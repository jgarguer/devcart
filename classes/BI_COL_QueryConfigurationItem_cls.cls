/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       20-May-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_QueryConfigurationItem_cls 
{
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static map<String,BI_COL_Homologacion_integraciones__c> getIDHomologacion(List<NE__OrderItem__c> lstNewOrdeItem)
	{
		map<String,String> mapIDHomologacion = new map<String,String>();
		List<String> lstMatrixIdentifie = new List<String>();
		List<String> lstLasPH = new List<String>();
		set<String> atributos = new set<String>();
		
		/*for( AggregateResult qry : [select count(ID), Name atributo from NE__DynamicPropertyDefinition__c
													where BI_COL_Id_propio__c<>null
													Group by Name] )
			atributos.add(String.valueOf(qry.get('atributo')));*/

		/*for( NE__OrderItem__c noit : lstNewOrdeItem )
		{
			lstMatrixIdentifie.add( noit.NE__CatalogItem__r.NE__Matrix_Item_Identifier__c );
		}*/
		System.debug('\n\n lstMatrixIdentifie: '+lstMatrixIdentifie+'\n\n');
		/*List<NE__Matrix_Adjustment_Row__c> lstMatrixRows = [
			select 	BI_COL_Codigo_P__c,Name, NE__Configuration_Code__c, NE__Matrix_Adjustment__c, NE__Matrix_Item_Identifier__c, 
					NE__Matrix_Name__c, NE__Priority__c, NE__Source_MatrixAdjRow_Id__c, ID
			from 	NE__Matrix_Adjustment_Row__c where NE__Matrix_Item_Identifier__c IN: lstMatrixIdentifie 
			order by NE__Matrix_Adjustment__c];*/

		for( NE__OrderItem__c noit : lstNewOrdeItem )
		{
			if(noit.NE__ProdId__r.BI_COL_CodigoP__c==null || noit.NE__ProdId__r.BI_COL_CodigoP__c=='')
			{
				/*set<String> valor = new set<String>();
				for( NE__Order_Item_Attribute__c neorAtr : noit.NE__Order_Item_Attributes__r )
				{
					//if( atributos.contains( neorAtr.Name )){
						valor.add( neorAtr.Name + '=' + neorAtr.NE__Value__c );
						//System.debug('\n'+neorAtr.Name + '=' + neorAtr.NE__Value__c );}
				}
				System.debug('\n\n valor= '+valor+' \n\n');
				for( NE__Matrix_Adjustment_Row__c ma : lstMatrixRows )
				{
					if( ma.NE__Matrix_Item_Identifier__c == noit.NE__CatalogItem__r.NE__Matrix_Item_Identifier__c )
					{
						String configurationCode = ma.NE__Configuration_Code__c;
						//Elimina el punto y coma que esta al final de la cadena
						configurationCode = configurationCode!=null?configurationCode.substring(0,configurationCode.length()-1):'';
						//Convierte la cadena a una lista
						List<String> stringSplit = configurationCode.split(';');
						Integer conta = 0;
						for( String va : stringSplit )
						{
							//Se quita la primera parte del string :
							va = va.substringAfter(':');
							//System.debug('\n\n va: '+va+'\n\n');
							if( valor.contains( va ) )
								conta = conta + 1;

						}
						//System.debug('\n\n conta: '+conta+'  ----'+valor.size()+'\n\n');
						if(conta>valor.size()-2)
						{
							System.debug(' \n\n Cuenta='+conta+'  ma.id= '+ma.id+ ' ## ma.NE__Configuration_Code__c: '+ma.NE__Configuration_Code__c+'\n\n');
						}
						if( conta == valor.size() && conta != 0 )
						{
							mapIDHomologacion.put( noit.id, ma.BI_COL_Codigo_P__c );
							System.debug(' \n\n query sesultados----'+ma.BI_COL_Codigo_P__c +noit.id);
							lstLasPH.add( ma.BI_COL_Codigo_P__c);
							break;
						}
					}
				}*/
				mapIDHomologacion.put( noit.id, noit.NE__SmartPartNumber__c );
				lstLasPH.add(noit.NE__SmartPartNumber__c);
			}
			else
			{
				mapIDHomologacion.put(noit.id,noit.NE__ProdId__r.BI_COL_CodigoP__c);
				lstLasPH.add(noit.NE__ProdId__r.BI_COL_CodigoP__c);
			}
		}
		System.debug('\n\n lstLasPH: '+lstLasPH+'\n\n');
		map<String,BI_COL_Homologacion_integraciones__c> mapHomologacion = new map<String,BI_COL_Homologacion_integraciones__c>();
		map<String,BI_COL_Homologacion_integraciones__c> mapIDOrderItem = new map<String,BI_COL_Homologacion_integraciones__c>();

		List<BI_COL_Homologacion_integraciones__c> lstHomologacion=[
			select 	Name, ID, BI_COL_Tipo_registro_DS__c, BI_COL_Plan__c
			from 	BI_COL_Homologacion_integraciones__c 
			where 	Name in:lstLasPH ];
		System.debug('\n\n lstHomologacion: '+lstHomologacion+'\n\n');

		for( BI_COL_Homologacion_integraciones__c hi : lstHomologacion )
			mapHomologacion.put( hi.Name, hi);
		System.debug('\n\n mapIDHomologacion: '+mapIDHomologacion);
		for( String mavar : mapIDHomologacion.keyset() )
			mapIDOrderItem.put( mavar, mapHomologacion.get( mapIDHomologacion.get( mavar ) ) );
		System.debug('\n\n mapIDOrderItem '+mapIDOrderItem+'\n\n');
		return mapIDOrderItem;
	}

	/*public  static void GenerarReporteMatrix()
	{
		Document d = new Document(); 
		d.Name = 'ReporteMatrix.csv';
		d.FolderId='00lw0000002JsCAAA0';
		String myContent ;
			map<String,NE__Catalog_Item__c> mcatalogo=new map <String,NE__Catalog_Item__c>();
			for(NE__Catalog_Item__c nc:[select NE__Matrix_Item_Identifier__c,Name, SubFamilia_CdG__c, Subfamilia_Local__c, SystemModstamp, Temp_Ext_Id_Categ__c, 
								Temp_Ext_Id_Product__c 
								from NE__Catalog_Item__c
								where NE__Matrix_Item_Identifier__c<>null and BI_COL_Id_Propio__c<>null])
			{
				mcatalogo.put(nc.NE__Matrix_Item_Identifier__c,nc);
			}

			for(NE__Matrix_Adjustment_Row__c mar:[select BI_COL_Codigo_P__c, BI_COL_ID__c, 
			ConnectionSentId, CreatedById, CreatedDate, CurrencyIsoCode, Id, IsDeleted, LastModifiedById, LastModifiedDate, 
			Name, NE__Account_Type__c, NE__Channel_Parameter__c, NE__Configuration_Code__c, NE__Configuration_Family__c, 
			NE__Configuration_Family_Property__c, NE__Configuration_Value__c, NE__Currency_Extension_1__c, 
			NE__Currency_Extension_2__c, NE__Currency_Extension_3__c, NE__Currency_Extension_4__c, NE__Currency_Extension_5__c, 
			NE__Matrix_Adjustment__c, NE__Matrix_Item_Identifier__c, NE__Matrix_Name__c, NE__Order_Type__c, NE__Payment_Mode__c, 
			NE__Priority__c, NE__Set_Base_One_Time_Fee__c, NE__Set_Base_Recurring_Charge__c, NE__Set_OneTime_Fee_Code__c, 
			NE__Set_Recurring_Charge_Code__c, NE__Set_SmartPartNumber__c, NE__Set_Unit_Credit_Code__c, NE__Set_Usage_Code__c, 
			NE__Source_MatrixAdjRow_Id__c, NE__Text_Extension_1__c, NE__Text_Extension_2__c, NE__Text_Extension_3__c, 
			NE__Text_Extension_4__c, NE__Text_Extension_5__c 
			from NE__Matrix_Adjustment_Row__c 
			where BI_COL_Codigo_P__c<>null])
			{
				myContent+=mar.BI_COL_Codigo_P__c;//+' - '+mcatalogo.get(mar.NE__Matrix_Item_Identifier__c)!=null?mcatalogo.get(mar.NE__Matrix_Item_Identifier__c).id:'';
				if(Limits.getHeapSize()> Limits.getLimitHeapSize() - 990000)
				{
					break;
				}
			}
		d.Body = Blob.valueOf(myContent); 
		d.ContentType = 'text/plain';
		d.Type = 'txt';
		insert d;
		System.debug(d.id);

	}
	/*public static void generarReporte()
	{
		List<NE__DynamicPropertyDefinition__c> lstNdynamy=[select
		(SELECT NE__FamilyId__r.Name,ID, NAME, NE__FamilyId__c FROM NE__ProductFamilyProperties__r WHERE BI_COL_Id_Propio__c<> NULL),
		(select ID,Name,BI_COL_Id_Propio__c from NE__PropertyDomains__r), 
		 Name ,ID
		from NE__DynamicPropertyDefinition__c WHERE BI_COL_Id_Propio__c like 'v%' ];
		List<String> idpfp=new List<String>();

		for(NE__DynamicPropertyDefinition__c ned:lstNdynamy)
		{
			for(NE__ProductFamilyProperty__c pfp:ned.NE__ProductFamilyProperties__r)
			{
				idpfp.add(pfp.NE__FamilyId__c);
			}
		}

		map<id,NE__ProductFamily__c> mpPro=new map<id,NE__ProductFamily__c>();

		for(NE__ProductFamily__c pf:[select NE__FamilyId__c,Name, NE__ProdId__c,NE__ProdId__r.Name,BI_COL_Id_propio__c,NE__FamilyId__r.Name 
		from NE__ProductFamily__c where BI_COL_Id_propio__c<> null and NE__FamilyId__c in:idpfp])
		{
			mpPro.put(pf.NE__FamilyId__c,pf);
		}
		Document d = new Document(); 
		d.Name = 'ReporteProductos';
		d.FolderId='00lw0000002JsCAAA0';
		String myContent ; 
		

		map<String,String> mapValores=new map<String,String>();
		//Recorre toda la consulta
		for(NE__DynamicPropertyDefinition__c ned:lstNdynamy)
		{
			for(NE__ProductFamilyProperty__c pfp:ned.NE__ProductFamilyProperties__r)
			{
				//mpPro.get(pfp.NE__FamilyId__c);
				//System.debug(pfp.NE__FamilyId__c+';'+ned.Name+';'+pfp.Name);
				if(mpPro.containsKey(pfp.NE__FamilyId__c))
				{
					NE__ProductFamily__c pf=mpPro.get(pfp.NE__FamilyId__c);
					//System.debug(pf.NE__ProdId__r.Name+';'+ned.Name+';'+pfp.Name);
					if(ned.NE__PropertyDomains__r!=null)
					{
						for(NE__PropertyDomain__c pd:ned.NE__PropertyDomains__r)
						{
							//mapValores.put(ned.Id+'-'+pd.ID,pf.NE__ProdId__r.Name+';'+ned.Name+';'+pd.Name);
							myContent+=pf.NE__ProdId__r.Name+';'+ned.Name+';'+pfp.Name+';'+pd.Name+'\n';
						}
					}
				
				}	
			}
		}
		List<String> val=new List<string>();
		for(Integer i=0;i<mapValores.size();i++)
		{
			//mapValores.keset
			//System.debug(mapValores.keySet(i));
		}
		/*for(String valor:mapValores.keySet())
		{
			String agrupador=mapValores.get(valor);
			//String valorStrin='';
			if()
			//for()
		}*/
		/*d.Body = Blob.valueOf(myContent); 
		d.ContentType = 'text/plain';
		d.Type = 'txt';
		insert d;
	}*/
}