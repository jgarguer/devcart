/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_OrderMethods class 
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016          Fernando Arteaga             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_OrderMethods
{
	final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
	
    static {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'NE__Order__c']){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Sets opportunity field BI_Fecha_de_entrega_de_la_oferta__c to current Datetime, when quote status has changed from In progress to Delivered

    In:            newOrders: orders from Trigger.new
                   mapOldOrders: orders from Trigger.oldMap
    Out:           
    
    History: 
    
    <Date>          <Author>                <Change Description>
    22/09/2016      Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setOpptyFieldsOnQuoteDelivery(List<NE__Order__c> newOrders, Map<Id, NE__Order__c> mapOldOrders)
	{
		try
		{
			Set<Id> setOppId = new Set<Id>();
			Set<Id> setProposalId = new Set<Id>();
			
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
			
			for (NE__Order__c ord: newOrders)
			{
				System.debug(LoggingLevel.ERROR, 'New recordTypeId:' + ord.RecordTypeId);
				if (ord.RecordTypeId == MAP_NAME_RT.get('Quote') && ord.NE__OrderStatus__c == 'Delivered' && mapOldOrders.get(ord.Id).NE__OrderStatus__c == 'In progress')
				{
					if (ord.NE__OptyId__c != null)
						setOppId.add(ord.NE__OptyId__c);
				}
			}
			
			if (!setOppId.isEmpty())
			{
				List<Opportunity> listOpp = [SELECT Id, BI_Fecha_de_entrega_de_la_oferta__c
                                             FROM Opportunity
                                             WHERE Id IN :setOppId];
                                             
				for (Opportunity o: listOpp)
				{
					o.BI_Fecha_de_entrega_de_la_oferta__c = Datetime.now();
				}
					
				BI_O4_QueueableUpdateParentOpps newQ = new BI_O4_QueueableUpdateParentOpps(listOpp);
				System.enqueueJob(newQ);
			}
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.setOpptyFieldsOnQuoteDelivery', 'BI_EN', Exc, 'Trigger');
		}
	}
	
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   When quotes are updated from Delivered to Active, gets the orders and the current active quotes and enqueues a queueable job

    In:            newOrders: orders from Trigger.new
                   mapOldOrders: orders from Trigger.oldMap
    Out:           
    
    History: 
    
    <Date>          <Author>                <Change Description>
    15/09/2016      Fernando Arteaga        Initial Version
    15/09/2016      Alvaro García        	DEPRECATED
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*public static void activateQuote(List<NE__Order__c> newOrders, Map<Id, NE__Order__c> mapOldOrders)
	{
		try
		{
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
			
			List<NE__Order__c> listOrders = new List<NE__Order__c>();
			List<Opportunity> listOpps = new List<Opportunity>();
			Set<Id> setOrderId = new Set<Id>();
			
			Set<Id> setOppId = new Set<Id>();
			for (NE__Order__c ord: newOrders)
			{
				System.debug(LoggingLevel.ERROR, 'New NE__OrderStatus__c:' + ord.NE__OrderStatus__c);
				System.debug(LoggingLevel.ERROR, 'Old NE__OrderStatus__c:' + mapOldOrders.get(ord.Id).NE__OrderStatus__c);
				System.debug(LoggingLevel.ERROR, 'RecordTypeId:' + ord.RecordTypeId);
				if (ord.RecordTypeId == MAP_NAME_RT.get('Quote') && ord.NE__OrderStatus__c == Label.BI_Active && mapOldOrders.get(ord.Id).NE__OrderStatus__c == 'Delivered')
				{
					listOrders.add(ord);
					setOppId.add(ord.NE__OptyId__c);
					setOrderId.add(ord.Id);
				}
			}
			
			if (!listOrders.isEmpty())
			{
		    	List<NE__Order__c> listCurrentActiveQtes = [SELECT Id, NE__OrderStatus__c
															FROM NE__Order__c
							 								WHERE RecordType.DeveloperName = 'Quote'
							 								AND NE__OrderStatus__c = :Label.BI_Active
							 								AND NE__OptyId__c IN :setOppId
							 								AND Id NOT IN :setOrderId];
				
				for (NE__Order__c ord: newOrders)
				{
					if (setOppId.contains(ord.NE__OptyId__c))
					{
						Opportunity opp = new Opportunity(Id = ord.NE__OptyId__c,
						                                  BI_Oferta_tecnica__c = 'Oferta técnica aprobada',
						                                  BI_ARG_CAPEX_total__c = ord.BI_O4_CAPEX_total__c,
	                                                      BI_ARG_OPEX_recurrente_total__c = ord.BI_O4_OPEX_recurrente_total__c,
	                                                      BI_ARG_OPEX_no_recurrente_total__c = ord.BI_O4_OPEX_no_recurrente_total__c,
	                                                      BI_Fecha_de_vigencia__c = ord.BI_O4_Fecha_entrega_oferta_ingenieria__c);
						listOpps.add(opp);
					}
				}
				
				if (!listOpps.isEmpty())
				{
					BI_O4_QueueableUpdateParentOpps newQ1 = new BI_O4_QueueableUpdateParentOpps(listOpps);
					System.enqueueJob(newQ1);
				}
				
				if (!listOrders.isEmpty())
				{
					BI_O4_QueueablePutQuoteIntoOppty newQ2 = new BI_O4_QueueablePutQuoteIntoOppty(listOrders, listCurrentActiveQtes);
					System.enqueueJob(newQ2);
				}
			}
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.activateQuote', 'BI_EN', Exc, 'Trigger');
		}
	}*/

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       New Energy Aborda
    Description:   When an order change from quote to Opty change the status of the old opty to "revised" if they are not already in "revised"

    In:            newOrders: orders from Trigger.new
                   mapOldOrders: orders from Trigger.oldMap
    Out:           
    
    History: 
    
    <Date>          <Author>                <Change Description>
    08/11/2016      Alvaro García	       Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setStatus(List<NE__Order__c> newOrders, Map<Id, NE__Order__c> mapOldOrders)
	{
		try
		{
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');

			Set<Id> idOpp = new Set<Id>();
			Set<Id> idOrd = new Set<Id>();
			
			for(NE__Order__c ord : newOrders) {
				if(ord.RecordTypeId == MAP_NAME_RT.get('Opty') && mapOldOrders.get(ord.Id).RecordTypeId == MAP_NAME_RT.get('Quote')) {
					idOpp.add(ord.NE__OptyId__c);
					idOrd.add(ord.Id);
				}
			}

			if (!idOpp.isEmpty()) {
				List<NE__Order__c> toUpdate = [SELECT Id, NE__OrderStatus__c FROM NE__Order__c WHERE Id NOT IN : idOrd AND NE__OptyId__c IN : idOpp AND RecordType.DeveloperName = 'Opty' AND NE__OrderStatus__c != 'Revised'];
				
				if (!toUpdate.isEmpty()) {
					for (NE__Order__c currentOrder : toUpdate) {
						currentOrder.NE__OrderStatus__c = 'Revised';
					}

					update toUpdate;
				}
			}
			
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.setStatus', 'BI_EN', Exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Alvaro García
	    Company:       Aborda
	    Description:   Method that change the status to Delivered when the actual status is active
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/11/2016                  Alvaro García       	      changeDescription
	    05/06/2017					Gawron, Julián				  fixing query
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void desactiveQuote(List <NE__Order__c> news){

		try	{
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');

			List <Id> lst_idOpp = new List <Id>();

			for(NE__Order__c ord : news){

				if(ord.RecordTypeId == MAP_NAME_RT.get('Opty')){

					lst_idOpp.add(ord.NE__OptyId__c);
				}
			}

			if(!lst_idOpp.isEmpty()){

				List <NE__Order__c> lst_toUpdate = [SELECT Id, NE__OrderStatus__c FROM NE__Order__c WHERE RecordType.DeveloperName = 'Quote' AND NE__OrderStatus__c =: Label.BI_Active AND NE__OptyId__c in : lst_idOpp];

				if(!lst_toUpdate.isEmpty()){

					for(NE__Order__c ord : lst_toUpdate){

						ord.NE__OrderStatus__c = 'Delivered';
					}

					update lst_toUpdate;
				}
			}

		} catch (Exception exc) {
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.desactiveQuote', 'BI_EN', Exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Alvaro García
	    Company:       Aborda
	    Description:   Method that fill the offer date with today date
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/11/2016                  Alvaro García       	      changeDescription
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setQuoteVersion(List <NE__Order__c> news){

		try {
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');

			List <NE__Order__c> toProcess = new List <NE__Order__c>();
			List <Id> lst_idOpp = new List <Id>();

			for (NE__Order__c ord : news) {
				if(ord.RecordTypeId == MAP_NAME_RT.get('Quote')) {
					lst_idOpp.add(ord.NE__OptyId__c);
					toProcess.add(ord);
				}
			}

			List <Opportunity> lst_opp = [SELECT Id , (SELECT NE__Version__c FROM NE__Orders__r WHERE RecordType.DeveloperName = 'Quote' ORDER BY NE__Version__c DESC LIMIT 1) FROM Opportunity WHERE Id IN : lst_idOpp];

			Map <Id,Decimal> map_versions = new Map <Id,Decimal>();
			
			for (Opportunity opp : lst_opp) {
				
				if (!map_versions.containsKey(opp.Id)) {
				
					if (!opp.NE__Orders__r.isEmpty()) {
						map_versions.put(opp.Id,opp.NE__Orders__r[0].NE__Version__c);
					}
					else {
						map_versions.put(opp.Id,0);
					}
				}
			}

			for (NE__Order__c ord : toProcess) {

				if (map_versions.containsKey(ord.NE__OptyId__c)) {
					ord.NE__Version__c = map_versions.get(ord.NE__OptyId__c) + 1;
					map_versions.put(ord.NE__OptyId__c, ord.NE__Version__c);
				}

			}

		} catch (Exception exc) {
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.setQuoteVersion', 'BI_EN', Exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Alvaro García
	    Company:       Aborda
	    Description:   Method that fill the offer date with today date
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/11/2016                  	Alvaro García       	    Initial version
	    18/08/2017              		Guillermo Muñoz         	Changed to use new BI_MigrationHelper functionality
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setNumberProduct(List <NE__Order__c> news, Map<Id, NE__Order__c> mapOldOrders){

		try {
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');

			Set<Id> idOpp = new Set<Id>();
			Set<Id> idOrd = new Set<Id>();
			
			for(NE__Order__c ord : news) {
				if(ord.RecordTypeId == MAP_NAME_RT.get('Opty') && mapOldOrders.get(ord.Id).RecordTypeId == MAP_NAME_RT.get('Quote')) {
					idOpp.add(ord.NE__OptyId__c);
					idOrd.add(ord.Id);
				}
			}

			if (!idOpp.isEmpty()) {

				List <NE__Order__c> toProcess = [SELECT Id, NE__OptyId__c, (SELECT Id FROM NE__Order_Items__r) FROM NE__Order__c WHERE Id IN :idOrd AND NE__OrderStatus__c = 'Active'];
				
				List <Opportunity> toUpdate = new List<Opportunity>();
				
				if (!toProcess.isEmpty()) {
				
					for (NE__Order__c ord : toProcess) {
				
						Opportunity opp = new Opportunity(Id = ord.NE__OptyId__c, BI_Productos_numero__c = ord.NE__Order_Items__r.size());
						toUpdate.add(opp);
					}

					//Hacemos que no salten reglas de validacion ni los triggers.
					Map <Integer, BI_bypass__c> mapa;
	                try{
	                    mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
	                    BI_MigrationHelper.skipAllTriggers();
	                    
	                    if (!toUpdate.isEmpty()) {
							update toUpdate;
						}

	                }catch(Exception e){
	                	System.debug(e.getMessage() + '\n' + e.getLineNumber());
	                }
	                finally{
	                    if(mapa != null){
	                        BI_MigrationHelper.disableBypass(mapa);
	                    }
	                    BI_MigrationHelper.cleanSkippedTriggers();
            		}					 
				}
			}

		} catch (Exception exc) {
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.setNumberProduct', 'BI_EN', Exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Javier Almirón García
	    Company:       Aborda
	    Description:   Fill the NBAV field in the Opportunity with the NBAV field from the OR
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    16/10/2017                  	Javier Almirón      	    Initial version
	    28/11/2017			            Javier Almirón 				Added field "BI_O4_TGS_NBAV_Budget__c "
		23/02/2018						Manuel Ochoa				Added check for amount==null
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateNBAV (List <NE__Order__c> news, Map<Id, NE__Order__c> mapOld){

      
		System.debug('BBNN - ENTRO EN MÉTODO UPDATENBAV');

      try{
		Set <Id> oppUpdateId = new Set <Id>();
    	List <Opportunity> oppUpdate = new List <Opportunity>();
    	Set <Id> setIdPad = new Set <Id>();
    	List <Opportunity> oppOrdUpdate = new List <Opportunity>();
    	Opportunity opp;
    	List <Opportunity> oppList = new List <Opportunity>();
    	Double fcvAuxiliar;

		if (mapOld == null){

			if (news != null){

				for (NE__Order__c ord : news){

					opp = new Opportunity();
                    if(opp.Id   != null){opp.Id = ord.NE__OptyId__c;}
                    if(opp.BI_O4_TGS_NBAV_Presup__c != null){opp.BI_O4_TGS_NBAV_Presup__c = ord.BI_O4_TGS_NBAV_Presup__c;}
                    if(opp.BI_O4_TGS_NBAV_Budget__c != null){opp.BI_O4_TGS_NBAV_Budget__c  =  ord.BI_O4_TGS_NBAV_Presup__c;}
                    if(opp.BI_O4_RAV_Budget__c != null){opp.BI_O4_RAV_Budget__c =  ord.BI_O4_TGS_RAV_Presup__c;}
                    if(opp.BI_O4_TGS_FCV_Budget__c != null){opp.BI_O4_TGS_FCV_Budget__c  =  ord.BI_FCV_Presupuesto__c;}
                
					oppUpdate.add(opp);
					oppUpdateId.add(ord.NE__OptyId__c);
				}
			}
		}
		else if (news != null){

			System.debug('BBNN - FLAG 1');

			for (NE__Order__c ord : news){

				System.debug('BBNN - FLAG 2 - ord.BI_O4_TGS_NBAV_Presup__c = ' + ord.BI_O4_TGS_NBAV_Presup__c);
				System.debug('BBNN - FLAG 2 - mapOld.get(ord.ID).BI_O4_TGS_NBAV_Presup__c = ' + mapOld.get(ord.ID).BI_O4_TGS_NBAV_Presup__c);
				System.debug('BBNN - FLAG 2 - ord.BI_O4_TGS_RAV_Presup__c = ' + ord.BI_O4_TGS_RAV_Presup__c );
				System.debug('BBNN - FLAG 2 - mapOld.get(ord.ID).BI_O4_TGS_RAV_Presup__c = ' + mapOld.get(ord.ID).BI_O4_TGS_RAV_Presup__c);
				System.debug('BBNN - FLAG 2 - ord.BI_FCV_Presupuesto__c = ' +  ord.BI_FCV_Presupuesto__c);
				System.debug('BBNN - FLAG 2 - mapOld.get(ord.ID).BI_FCV_Presupuesto__c = ' + mapOld.get(ord.ID).BI_FCV_Presupuesto__c);

				if (ord.BI_O4_TGS_NBAV_Presup__c != mapOld.get(ord.ID).BI_O4_TGS_NBAV_Presup__c || ord.BI_O4_TGS_RAV_Presup__c != mapOld.get(ord.ID).BI_O4_TGS_RAV_Presup__c ||  ord.BI_FCV_Presupuesto__c != mapOld.get(ord.ID).BI_FCV_Presupuesto__c){
					System.debug('BBNN - FLAG 2');

					opp = new Opportunity(
                                Id                                         =   ord.NE__OptyId__c,
                                BI_O4_TGS_NBAV_Presup__c                   =   ord.BI_O4_TGS_NBAV_Presup__c,
                                BI_O4_TGS_NBAV_Budget__c                   =   ord.BI_O4_TGS_NBAV_Presup__c,
                                BI_O4_RAV_Budget__c                        =   ord.BI_O4_TGS_RAV_Presup__c,
                                BI_O4_TGS_FCV_Budget__c                    =   ord.BI_FCV_Presupuesto__c

                );
					oppUpdate.add(opp);
					oppUpdateId.add(ord.NE__OptyId__c);
					System.debug('BBNN - FLAG 3 - oppUpdate.SIZE() = ' + oppUpdate.size() );
					System.debug('BBNN - FLAG 3 - ord.NE__OptyId__c' + ord.NE__OptyId__c);
				}
			}

			if (!oppUpdate.isEmpty()){
				System.debug('BBNN - FLAG 3');
				update oppUpdate;
			}

			if (!oppUpdateId.isEmpty()){
				System.debug('BBNN - FLAG 0');
				oppList = [SELECT Id, BI_O4_TGS_FCV_Budget__c, Amount,  BI_O4_TdCP__c, BI_Oportunidad_Padre__c FROM Opportunity WHERE Id IN :oppUpdate];
				System.debug('BBNN - FLAG 0 - oppList.size() = ' + oppList.size());
			}

			if (!oppList.isEmpty()){
				System.debug('BBNN - FLAG 4');
				//for (Opportunity op : oppList){
				  for (Opportunity op : oppList){
				  	System.debug('BBNN - FLAG 5 - op.Id = ' + op.Id);
				  	System.debug('BBNN - FLAG 5 - oop.BI_Oportunidad_Padre__c = ' + op.BI_Oportunidad_Padre__c);
					//fcvAuxiliar = 0;
					// 23/02/2018	- Manuel Ochoa - Added check for amount==null
                    if(op.Amount==null){
                        op.amount=0;
                    }
                    // 23/02/2018	- Manuel Ochoa

					//if (op.BI_O4_TdCP__c == 0) {fcvAuxiliar = 0;}

					//else if(op.Amount != null && op.BI_O4_TdCP__c != null){
					//	System.debug('BBNN - FLAG 2');
					//	System.debug('BBNN - FLAG 2 - op.Amount = ' + op.Amount);
					//	System.debug('BBNN - FLAG 3 - op.BI_O4_TdCP__c = ' + op.BI_O4_TdCP__c);
					//	fcvAuxiliar = op.Amount/op.BI_O4_TdCP__c;}
					
					//op.BI_O4_TGS_FCV_Budget__c = fcvAuxiliar;
					//oppOrdUpdate.add(op);
					if (op.BI_Oportunidad_Padre__c != null){
						System.debug('BBNN - FLAG 6');
						setIdPad.add(op.BI_Oportunidad_Padre__c);
					}
				}
			}

			//if (!oppOrdUpdate.isEmpty()){

				//update oppOrdUpdate;
				if (!setIdPad.isEmpty()){
					BI_O4_OpportunityMethods.wonOpenSumFields(setIdPad,null);
				}
			//}
		}
		} catch (Exception exc) {
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderMethods.updateNBAV', 'BI_EN', Exc, 'Trigger');
		}
	}
}