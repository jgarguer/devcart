public without sharing class BI_LineaDeVentaMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Methods executed by the BI_Linea_de_Venta__c trigger.
   
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for Insert BI_Linea_de_Venta__c
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertLineaDeVentaIMP(list<BI_Linea_de_Venta__c> lNew){
        
        if (BI_GlobalVariables.stopTriggerIMPExcel == false){

            set<string> auxIdNameSol = new set<string>{};
            set<string> auxIdNameMod = new set<string>{};
            set<string> auxIdNameDesMod = new set<string>{};
            set<string> auxIdNameDesSIM = new set<string>{};
            set<string> auxIdSIMTEM = new set<string>{};
            Set <Id> set_sols = new Set <Id>();
            for (BI_Linea_de_Venta__c aux: lNew){
                
                //Validaciones
                
                    system.debug(aux);
                    
                    if(aux.BI_Tipo_de_Contrato__c != null){
                        aux.BI_Tipo_de_Contrato__c = aux.BI_Tipo_de_Contrato__c.split(' - ')[0];
                    }
                    
                    if(aux.BI_Modalidad_de_venta__c != null){
                        aux.BI_Modalidad_de_venta__c = aux.BI_Modalidad_de_venta__c.split(' - ')[0];
                    }
                    
                    if(aux.BI_Catalogo_de_Servicio_1_TEM__c != null){
                        aux.BI_Catalogo_de_Servicio_1_TEM__c = aux.BI_Catalogo_de_Servicio_1_TEM__c.split(' - ')[0];
                    }
                    
                    if(aux.BI_Catalogo_de_Servicio_2_TEM__c != null){
                        aux.BI_Catalogo_de_Servicio_2_TEM__c = aux.BI_Catalogo_de_Servicio_2_TEM__c.split(' - ')[0];
                    }
                    
                    if(aux.BI_Catalogo_de_Servicio_3_TEM__c != null){
                        aux.BI_Catalogo_de_Servicio_3_TEM__c = aux.BI_Catalogo_de_Servicio_3_TEM__c.split(' - ')[0];
                    }
                    
                    if(aux.BI_Catalogo_de_Servicio_4_TEM__c != null){
                        aux.BI_Catalogo_de_Servicio_4_TEM__c = aux.BI_Catalogo_de_Servicio_4_TEM__c.split(' - ')[0];
                    }
                    
                    if(aux.BI_Catalogo_de_Servicio_5_TEM__c != null){
                        aux.BI_Catalogo_de_Servicio_5_TEM__c = aux.BI_Catalogo_de_Servicio_5_TEM__c.split(' - ')[0];
                    }
                    
                    system.debug('auxx: '+aux);
                            
                    aux.BI_cantidad_TEM__c = aux.BI_Cantidad_de_equipos__c;
                    aux.BI_Cantidad_de_equipos__c = 1;
                    
                    if ((aux.BI_Solicitud__c == null) && aux.BI_Solicitud_TEMP__c != '' && aux.BI_Solicitud_TEMP__c != null){
                        auxIdNameSol.add(aux.BI_Solicitud_TEMP__c);
                    }
                    if ((aux.BI_Modelo__c == null) && aux.BI_Modelo_TEM__c != '' && aux.BI_Modelo_TEM__c != null){
                        auxIdNameMod.add(aux.BI_Modelo_TEM__c);
                    }
                    if ((aux.BI_Codigo_de_descuento_de_modelo__c == null) && aux.BI_Codigo_de_Descuento_TEM__c != '' && aux.BI_Codigo_de_Descuento_TEM__c != null){
                        auxIdNameDesMod.add(aux.BI_Codigo_de_Descuento_TEM__c);
                    } 
                    if ((aux.BI_Codigo_de_descuento_de_SIM__c == null) && aux.BI_Descuento_SIM_TEM__c != '' && aux.BI_Descuento_SIM_TEM__c != null){
                        auxIdNameDesSIM.add(aux.BI_Descuento_SIM_TEM__c);
                    }
                    if (aux.BI_ID_de_simcard_TEM__c != '' && aux.BI_ID_de_simcard_TEM__c != null){
                        auxIdSIMTEM.add(aux.BI_ID_de_simcard_TEM__c);
                    }
                    
                    if(aux.BI_Codigo_de_cliente_final__c != null && aux.BI_Codigo_de_cliente_final__c != ''){
                            
                        if (aux.BI_Codigo_de_cliente_final__c.toLowerCase() == 'nuevo') {aux.BI_Nuevo_codigo_de_cliente__c = true;}                         //Cliente Nuevo
                        if (aux.BI_Codigo_de_cliente_final__c.toLowerCase() == 'nuevo con traspaso') {aux.BI_Nuevo_codigo_con_traspaso__c = true;} 
                        if (aux.BI_Codigo_de_cliente_final__c.toLowerCase() == 'creación de cuenta') {aux.BI_Creacion_de_cuenta__c = true;}
                        
                    }
                         
                    if (aux.BI_Precio_final_de_modelo__c != null){aux.BI_Precio_base_de_modelo__c = aux.BI_Precio_final_de_modelo__c; aux.BI_Precio_del_modelo_definido_manualmen__c = 'Sí';}   //Precio Base Modelo== Precio Final Modelo, y definido manualmente.
                    if (aux.BI_Precio_final_de_SIM__c != null){aux.BI_Precio_base_de_SIM__c = aux.BI_Precio_final_de_SIM__c; aux.BI_Precio_de_SIM_definido_manualmente__c = 'Sí';}              //Precio Base SIM == Precio Final SIM, y definido manualmente.
                                        
            }
            
            //Buscar Id Solicitud para asociar las Lineas de Venta
            if (auxIdNameSol.size()>0){ 
                list<BI_Solicitud_envio_productos_y_servicios__c> lauxSol = [SELECT Id,Name FROM BI_Solicitud_envio_productos_y_servicios__c  WHERE Id IN :auxIdNameSol OR Name IN :auxIdNameSol ORDER BY Id, Name];
                if (lauxSol.size()>0){
                    for (BI_Solicitud_envio_productos_y_servicios__c auxSol: lauxSol){
                        for (BI_Linea_de_Venta__c aux: lNew){
                            if ((aux.BI_Solicitud_TEMP__c == String.valueOf(auxSol.Id)) || (aux.BI_Solicitud_TEMP__c == auxSol.Name)){
                                 aux.BI_Solicitud__c = auxSol.Id;
                            }
                        }
                    }
                }
            }
            //****************************************************
            //Buscar Id Modelo para asociar las Líneas de Venta
            if (auxIdNameMod.size()>0){ 
                list<RecordType> lRT = new list<RecordType>([SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'BI_Modelo__c' AND DeveloperName = 'BI_Modelo' ORDER BY Id]);
                if (lRT.size()>0){
                    list<BI_Modelo__c> lauxMod = [SELECT Id,Name,BI_Tipo_de_SIM__c,BI_Familia_Local__c FROM BI_Modelo__c  WHERE RecordTypeId=:lRT[0].Id AND ( Id IN :auxIdNameMod OR Name IN :auxIdNameMod ) ORDER BY Id, Name];
                    list<BI_Modelo__c> lauxSIM = [SELECT Id,Name,BI_Tipo_de_SIM__c,BI_Familia_Local__c FROM BI_Modelo__c  WHERE RecordTypeId=:lRT[0].Id AND ( Id IN :auxIdSIMTEM OR Name IN :auxIdSIMTEM ) ORDER BY Id, Name];
                    if (lauxMod.size()>0){
                        for (BI_Modelo__c auxMod: lauxMod){
                            for (BI_Linea_de_Venta__c aux: lNew){
                                set_sols.add(aux.BI_Solicitud__c);    
                                if ((aux.BI_Modelo_TEM__c == String.valueOf(auxMod.Id)) || (aux.BI_Modelo_TEM__c == auxMod.Name)){
                                     aux.BI_Modelo__c = auxMod.Id;
                                     if(auxMod.BI_Tipo_de_SIM__c != null && aux.BI_ID_de_simcard_TEM__c == null){
                                        aux.BI_ID_de_simcard__c = auxMod.BI_Tipo_de_SIM__c;
                                     }
                                }
                            }
                        }   
                        if (lauxSIM.size()>0){
                            for (BI_Modelo__c auxMod: lauxSIM){
                                for (BI_Linea_de_Venta__c aux: lNew){                               
                                    if ((aux.BI_ID_de_simcard_TEM__c == String.valueOf(auxMod.Id)) || (aux.BI_ID_de_simcard_TEM__c == auxMod.Name)){                                     
                                         if(aux.BI_ID_de_simcard_TEM__c != null && aux.BI_ID_de_simcard__c == null){
                                            aux.BI_ID_de_simcard__c = auxMod.Id;
                                         }
                                    }
                                }
                            }    
                        }       
                    }
                }
            }
            //****************************************************          
            //Buscar Id Descuento Modelo para asociar las Líneas de Venta
            if (auxIdNameDesMod.size()>0){ 
                list<BI_Descuento_para_modelo__c> lauxDes = [SELECT Id,Name FROM BI_Descuento_para_modelo__c WHERE Id IN :auxIdNameDesMod OR Name IN :auxIdNameDesMod ORDER BY Id, Name];
                if (lauxDes.size()>0){
                    for (BI_Descuento_para_modelo__c auxDes: lauxDes){
                        for (BI_Linea_de_Venta__c aux: lNew){
                            if ((aux.BI_Codigo_de_Descuento_TEM__c == String.valueOf(auxDes.Id)) || (aux.BI_Codigo_de_Descuento_TEM__c == auxDes.Name)){
                                 aux.BI_Codigo_de_descuento_de_modelo__c = auxDes.Id;
                            }
                        }
                    }               
                }
            }
            //****************************************************  
            //Buscar Id Descuento SIM para asociar las Líneas de Venta
            if (auxIdNameDesSIM.size()>0){ 
                list<BI_Descuento_para_modelo__c> lauxDes = [SELECT Id,Name FROM BI_Descuento_para_modelo__c WHERE Id IN :auxIdNameDesSIM OR Name IN :auxIdNameDesSIM ORDER BY Id, Name];
                if (lauxDes.size()>0){
                    for (BI_Descuento_para_modelo__c auxDes: lauxDes){
                        for (BI_Linea_de_Venta__c aux: lNew){
                            if ((aux.BI_Descuento_SIM_TEM__c == String.valueOf(auxDes.Id)) || (aux.BI_Descuento_SIM_TEM__c == auxDes.Name)){
                                 aux.BI_Codigo_de_descuento_de_SIM__c = auxDes.Id;
                            }
                        }
                    }               
                }
            }   
            Map <Id, BI_Solicitud_envio_productos_y_servicios__c> map_sols = new Map<Id, BI_Solicitud_envio_productos_y_servicios__c>();  
             system.debug('#Debug set_sols '+ set_sols);
            if(!set_sols.isEmpty()) 
                map_sols = new Map<Id, BI_Solicitud_envio_productos_y_servicios__c>([SELECT Id, BI_Tipo_solicitud__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id IN :set_sols]);
            
            system.debug('#Debug map_sols '+ map_sols);

            for (BI_Linea_de_Venta__c aux: lNew){
                system.debug('#Debug aux.BI_Solicitud__c '+ aux.BI_Solicitud__c);
                system.debug('#Debug aux.BI_Modelo__c '+ aux.BI_Modelo__c);
                system.debug('#Debug aux.BI_Cantidad_de_equipos__c '+ aux.BI_Cantidad_de_equipos__c);
                system.debug('#Debug aux.BI_Codigo_de_cliente_final__c '+ aux.BI_Codigo_de_cliente_final__c);
                system.debug('#Debug aux.BI_Tipo_de_Contrato__c '+ aux.BI_Tipo_de_Contrato__c);
                system.debug('#Debug aux.BI_Ciclo_de_facturacion__c '+ aux.BI_Ciclo_de_facturacion__c);
                system.debug('#Debug aux.BI_Modalidad_de_venta__c '+ aux.BI_Modalidad_de_venta__c);

                if(aux.BI_Modelo__c ==null || 
                    aux.BI_Cantidad_de_equipos__c == null ||
                    aux.BI_Codigo_de_cliente_final__c == null
                    || aux.BI_Tipo_de_Contrato__c == null /*|| aux.BI_Ciclo_de_facturacion__c == null*/ ||
                    aux.BI_Modalidad_de_venta__c == null || aux.BI_Solicitud__c == null){
                        
                        aux.addError('Por favor, rellene los campos obligatorios');
                        
                        
                }else{
                    if (!map_sols.isEmpty() && map_sols.get(aux.BI_Solicitud__c).BI_Tipo_solicitud__c == 'Split billing'){
                        if(aux.BI_RUT_hijo__c == null || aux.BI_Nombre_persona__c == null){
                            aux.addError('Por favor, rellene los campos obligatorios');
                        }
                    }else if(!map_sols.isEmpty() && map_sols.get(aux.BI_Solicitud__c).BI_Tipo_solicitud__c != 'Split billing'){
                        if(aux.BI_RUT_hijo__c != null || aux.BI_Nombre_persona__c != null || aux.BI_Codigo_cliente_hijo__c != null){
                            aux.addError('Campos erróneos para solicitud de tipo Normal');
                        }
                        if(aux.BI_Otro_plan__c == null  && aux.BI_Plan__c == null){
                            aux.addError('Por favor, rellene los campos obligatorios');
                        }
                    }
                }
                if((aux.BI_Precio_final_de_modelo__c != null && (aux.BI_Codigo_de_descuento_de_modelo__c != null ||
                    aux.BI_Bolsa_de_Dinero_Modelo_TEM__c != null)) ||
                    ((aux.BI_Bolsa_de_Dinero_Modelo_TEM__c != null || aux.BI_Codigo_de_descuento_de_SIM__c != null) && aux.BI_Precio_final_de_SIM__c != null) ){
                    aux.addError('No puede asignar bolsas ni descuentos si el precio es manual');
                }
            }
            //****************************************************          
            
            //Insertar una línea por cantidad.
            /*list<BI_Linea_de_Venta__c> lauxNewLin = new list<BI_Linea_de_Venta__c>{};
            for (BI_Linea_de_Venta__c aux: lNew){
                decimal cantidad = aux.BI_Cantidad_de_equipos__c;
                if  (cantidad < 1) aux.addError('No se puede insertar lineas de Venta con cantidad 0'); 
                else if (cantidad > 1){
                    BI_Linea_de_Venta__c auxLin = aux.clone();
                    auxLin.BI_Cantidad_de_equipos__c = 1;
                    for (integer i = 0; i < cantidad-1; i++){
                        lauxNewLin.add(auxLin.clone());
                    }
                    aux.BI_Cantidad_de_equipos__c = 1;
                    BI_GlobalVariables.stopTriggerIMPExcel = true; //Detenemos la recursividad
                } 
            }
            
            try{
                if (lauxNewLin.size()>0) insert lauxNewLin;
            }catch (exception exc){
                BI_LogHelper.generate_BILog('BI_LineaDeServicioHelper.insertLineaDeVentaIMP', 'BI_EN', exc, 'Trigger');
            }*/
            
            
                            
        }
    } 

            /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*public static void validateBolsaDinero(list<BI_Linea_de_Venta__c> news){
        Set <Id> set_acc = new Set <Id>();
        Set <Id> set_sol = new Set <Id>();
        Set <Id> set_hierarchy = new Set <Id>();
        for(BI_Linea_de_Venta__c venta:news){
            set_sol.add(venta.BI_Solicitud__c);
        }

        for(BI_Solicitud_envio_productos_y_servicios__c sol:[SELECT BI_Cliente__c FROM BI_Solicitud_envio_productos_y_servicios__c  WHERE Id IN :set_sol]){
            set_acc.add(sol.BI_Cliente__c);
        }
        List <Account> AccParent = [select Id, ParentId, Parent.ParentId, Parent.Parent.ParentId, Parent.Parent.Parent.ParentId, 
                                         Parent.Parent.Parent.Parent.ParentId                                     
                                         from Account where Id IN :set_acc]; 
        Map <Id, Id> map_acc_parent = new Map <Id, Id>();          
        for(Account acc:AccParent){                         
            Id id_bol;              
            if(acc.ParentId == null){
                id_bol = acc.Id;
                set_hierarchy.add(acc.Id);
            }else if(acc.Parent.ParentId == null){
                id_bol = acc.ParentId;
                set_hierarchy.add(acc.ParentId);
            }else if(acc.Parent.Parent.ParentId == null){
                id_bol = acc.Parent.ParentId;
                set_hierarchy.add(acc.Parent.ParentId);
            }else if(acc.Parent.Parent.Parent.ParentId == null){
                id_bol = acc.Parent.Parent.ParentId;
                set_hierarchy.add(acc.Parent.Parent.ParentId);
            }else if(acc.Parent.Parent.Parent.Parent.ParentId == null){
                id_bol = acc.Parent.Parent.Parent.ParentId;
                set_hierarchy.add(acc.Parent.Parent.Parent.ParentId);
            }else{
                id_bol = acc.Parent.Parent.Parent.Parent.ParentId;
                set_hierarchy.add(acc.Parent.Parent.Parent.Parent.ParentId);
            }
            if(id_bol!=null)
                map_acc_parent.put(acc.Id, id_bol);
        }
        
        List <BI_Bolsa_de_dinero__c> lst_bolsa = [SELECT Id FROM BI_Bolsa_de_dinero__c WHERE Id IN :set_hierarchy OR Id IN :map_acc_parent.keySet()];

    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void deleteLineaDeServicio(list<BI_Linea_de_Venta__c> lOld){
        
        list<Id> auxList = new list<Id>{};
        for (BI_Linea_de_Venta__c aux: lOld){
             if (aux.BI_Estado_de_linea_de_venta__c == 'Pendiente') auxList.add(aux.Id);
             else   aux.addError('No se puede Eliminar porque no se encuentra en estado Pendiente');        
        }
        list<BI_Linea_de_Servicio__c> lLineaServicio=[SELECT Id FROM BI_Linea_de_Servicio__c WHERE BI_Linea_de_Venta__c IN :auxList ORDER BY Id];
        
        if (lLineaServicio.size()>0){
            BI_LineaDeServicioHelper.eliminarLineaDeServicio(lLineaServicio);
            
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void callBatchWS(list<Id> lst_linea, List <Id> lst_users){
        //Database.executeBatch(new BI_LineaDeVenta_JOB(lst_linea,lst_users), 50);
        
    }

             /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarLineaDeServicio(list<BI_Linea_de_Venta__c> news){
        Map <String, String> map_cat_act = new Map <String, String>();
        Map <String, String> map_cat_stat = new Map <String, String>();
        Set <Id> set_ven = new Set <Id>();
        for (BI_Linea_de_Venta__c aux: news){
                    
            if(aux.BI_Catalogo_de_Servicio_1_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_1_TEM__c, aux.BI_Accion_1_TEM__c);   
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_1_TEM__c, aux.BI_Estado_1_TEM__c); 
                set_ven.add(aux.Id);        
            }
            if(aux.BI_Catalogo_de_Servicio_2_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_2_TEM__c, aux.BI_Accion_2_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_2_TEM__c, aux.BI_Estado_2_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_3_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_3_TEM__c, aux.BI_Accion_3_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_3_TEM__c, aux.BI_Estado_3_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_4_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_4_TEM__c, aux.BI_Accion_4_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_4_TEM__c, aux.BI_Estado_4_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_5_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_5_TEM__c, aux.BI_Accion_5_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_5_TEM__c, aux.BI_Estado_5_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_6_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_6_TEM__c, aux.BI_Accion_6_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_6_TEM__c, aux.BI_Estado_6_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_7_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_7_TEM__c, aux.BI_Accion_7_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_7_TEM__c, aux.BI_Estado_7_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_8_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_8_TEM__c, aux.BI_Accion_8_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_8_TEM__c, aux.BI_Estado_8_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_9_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_9_TEM__c, aux.BI_Accion_9_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_9_TEM__c, aux.BI_Estado_9_TEM__c); 
                set_ven.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_10_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_10_TEM__c, aux.BI_Accion_10_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_10_TEM__c, aux.BI_Estado_10_TEM__c);
                set_ven.add(aux.Id); 
            }   
        }
        if(!map_cat_act.keySet().isEmpty()){
            List <BI_Linea_de_Servicio__c> lst_serv =[SELECT Id, BI_Accion__c, BI_Estado_de_linea_de_servicio__c, Name, BI_Modelo__r.Name FROM BI_Linea_de_Servicio__c WHERE BI_Linea_de_venta__c IN :set_ven];
            if(!lst_serv.isEmpty()){
                for(BI_Linea_de_Servicio__c serv:lst_serv){
                    serv.BI_Accion__c = map_cat_act.get(serv.BI_Modelo__r.Name);
                    serv.BI_Estado_de_linea_de_servicio__c = map_cat_stat.get(serv.BI_Modelo__r.Name);
                }
            }
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for updating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarLineaDeServicioStatus(List<BI_Linea_de_Venta__c> news, List <BI_Linea_de_Venta__c> olds){        
        
        Map <String, String> map_cat_stat = new Map <String, String>();
        Integer i = 0;
        Set <Id> set_ven = new Set <Id>();
        for (BI_Linea_de_Venta__c aux: news){   

            if(aux.BI_Estado_1_TEM__c != olds[i].BI_Estado_1_TEM__c){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_1_TEM__c, aux.BI_Estado_1_TEM__c); 
                set_ven.add(aux.Id);    
            }else if(aux.BI_Estado_2_TEM__c != olds[i].BI_Estado_2_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_2_TEM__c, aux.BI_Estado_2_TEM__c);
                set_ven.add(aux.Id);     
            }else if(aux.BI_Estado_3_TEM__c != olds[i].BI_Estado_3_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_3_TEM__c, aux.BI_Estado_3_TEM__c);
                set_ven.add(aux.Id);    
            }else if(aux.BI_Estado_4_TEM__c != olds[i].BI_Estado_4_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_4_TEM__c, aux.BI_Estado_4_TEM__c); 
                set_ven.add(aux.Id);    
            }else if(aux.BI_Estado_5_TEM__c != olds[i].BI_Estado_5_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_5_TEM__c, aux.BI_Estado_5_TEM__c);
                set_ven.add(aux.Id);     
            }else if(aux.BI_Estado_6_TEM__c != olds[i].BI_Estado_6_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_6_TEM__c, aux.BI_Estado_6_TEM__c); 
                set_ven.add(aux.Id);
            }else if(aux.BI_Estado_7_TEM__c != olds[i].BI_Estado_7_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_7_TEM__c, aux.BI_Estado_7_TEM__c); 
                set_ven.add(aux.Id);    
            }else if(aux.BI_Estado_8_TEM__c != olds[i].BI_Estado_8_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_8_TEM__c, aux.BI_Estado_8_TEM__c); 
                set_ven.add(aux.Id);    
            }else if(aux.BI_Estado_9_TEM__c != olds[i].BI_Estado_9_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_9_TEM__c, aux.BI_Estado_9_TEM__c); 
                set_ven.add(aux.Id);    
            }else if(aux.BI_Estado_10_TEM__c != olds[i].BI_Estado_10_TEM__c ){
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_10_TEM__c, aux.BI_Estado_10_TEM__c); 
                set_ven.add(aux.Id);    
            }
            i++;
        }  

        if(!map_cat_stat.keySet().isEmpty()){
            List <BI_Linea_de_Servicio__c> lst_serv =[SELECT Id, BI_Accion__c, BI_Estado_de_linea_de_servicio__c, Name, BI_Modelo__r.Name FROM BI_Linea_de_Servicio__c WHERE BI_Linea_de_venta__c IN :set_ven];
            if(!lst_serv.isEmpty()){
                for(BI_Linea_de_Servicio__c serv:lst_serv){
                    serv.BI_Estado_de_linea_de_servicio__c = map_cat_stat.get(serv.BI_Modelo__r.Name);
                }
                update lst_serv;
            }
        }

        
    }
    
    
    public static void insertLineaDeVentaSplitAndValidate(Map<Id, BI_Linea_de_Venta__c> news){
        
        if (BI_GlobalVariables.stopTriggerIMPExcel == false){
        
            List<BI_Linea_de_Venta__c> lauxNewLin = new List<BI_Linea_de_Venta__c>();
            
            for(BI_Linea_de_Venta__c aux:news.values()){
                for(Integer i = 0; i < aux.BI_cantidad_TEM__c - 1; i++){    
                    BI_Linea_de_Venta__c auxLin = aux.clone();
                    auxLin.BI_Linea_origen_masivo__c = aux.Id;
                    lauxNewLin.add(auxLin.clone());
                }
            } 
            
            BI_GlobalVariables.stopTriggerIMPExcel = true;
                
            try{
                insert lauxNewLin;
                
                lauxNewLin.addAll(news.values());
                
                BI_LineaDeServicioHelper.insertLineaDeServicioIMP(lauxNewLin);
                
                List<Id> lst_ven = new List<Id>();
                lst_ven.addAll(news.keySet());
                
                //FUTURE
                BI_Siscel_Helper.LineaVenta_Mass(UserInfo.getUserEmail(), lst_ven, 0, null, null);
                
            }catch (Exception exc){
                BI_LogHelper.generate_BILog('BI_LineaDeServicioHelper.insertLineaDeVentaIMP', 'BI_EN', exc, 'Trigger');
            }
            
        }
        
    }
    
    
    public static void validateBackOffice(List<BI_Linea_de_Venta__c> news, List<BI_Linea_de_Venta__c> olds){
        
        List<Id> lst_ven = new List<Id>();
        
        Integer i = 0;
        for(BI_Linea_de_Venta__c lven:news){
            if(lven.BI_Estado_de_linea_de_venta__c == 'Activado' && olds[i].BI_Estado_de_linea_de_venta__c != 'Activado')
                lst_ven.add(lven.Id);
            i++;
        }
        
        if(!lst_ven.isEmpty())
            BI_Siscel_Helper.BackOfficeVenta_Mass(UserInfo.getUserEmail(), lst_ven, 0, null);
        
    }
    
    
    public static void updateSol(List<BI_Linea_de_Venta__c> news, List<BI_Linea_de_Venta__c> olds){
        
        Set<Id> set_sol = new Set<Id>();
        
        Integer i = 0;
        for(BI_Linea_de_Venta__c ven:news){
            if(ven.BI_Estado_de_linea_de_venta__c != olds[i].BI_Estado_de_linea_de_venta__c || 
               ven.BI_Solicitud_descargada_BackOffice__c != olds[i].BI_Solicitud_descargada_BackOffice__c)
            {
                set_sol.add(ven.BI_Solicitud__c);
            }   
            i++;
        }
        
        if(!set_sol.isEmpty()){
            
            List<BI_Solicitud_envio_productos_y_servicios__c> lst_sol = new List<BI_Solicitud_envio_productos_y_servicios__c>();
            
            Set<String> set_status = new Set<String>{'Activado', 'Desactivado'};
            Set<String> set_status_back = new Set<String>{'Entregado', 'No entregado'};
            
            for(BI_Solicitud_envio_productos_y_servicios__c sol:[select Id, (select Id, BI_Solicitud_descargada_BackOffice__c, BI_Estado_de_linea_de_venta__c from 
                                                                 Linea_de_Venta__r) from BI_Solicitud_envio_productos_y_servicios__c where Id IN :set_sol])
            {
                
                if(!sol.Linea_de_Venta__r.isEmpty()){
                    
                    Boolean sol_desc = true;
                    Boolean sol_closed = true;
                    Boolean sol_incomplete = true;
                    
                    for(BI_Linea_de_Venta__c ven:sol.Linea_de_Venta__r){
                        if(!ven.BI_Solicitud_descargada_BackOffice__c || (ven.BI_Solicitud_descargada_BackOffice__c 
                            && !set_status_back.contains(ven.BI_Estado_de_linea_de_venta__c)))
                        {
                            sol_desc = false;
                        }
                        if(!set_status.contains(ven.BI_Estado_de_linea_de_venta__c))
                            sol_closed = false;

                        if(ven.BI_Estado_de_linea_de_venta__c == 'Enviando'){
                            sol_desc = true;
                            sol_closed = false;
                            sol_incomplete = false;
                        } 

                        if(ven.BI_Estado_de_linea_de_venta__c != 'No ejecutado' && ven.BI_Estado_de_linea_de_venta__c != 'Enviando')
                            sol_incomplete = false;
                            
                        //else if(ven.BI_Estado_de_linea_de_venta__c != 'No ejecutado')
                        //    sol_incomplete = false;
                    }
                    
                    if(sol_incomplete){
                        sol.BI_Estado_de_ventas__c = 'Incompleto';
                        lst_sol.add(sol);
                    }else if(sol_closed){
                        sol.BI_Estado_de_ventas__c = 'Cerrado';
                        lst_sol.add(sol);
                    }else if(sol_desc){
                        sol.BI_Estado_de_ventas__c = 'Activación';
                        lst_sol.add(sol);
                    }
                    
                }
                
            }
            
            try{
                update lst_sol;
            }catch(Exception exc){
                BI_LogHelper.generate_BILog('BI_LineaDeVentaMethods.updateSol', 'BI_EN', exc, 'Trigger');
            }
            
        }
        
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for updating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarHoraSolicitud(list<BI_Linea_de_venta__c> news){
        
       
        for (BI_Linea_de_venta__c aux: news){ 
              aux.BI_Hora_de_solicitud__c = string.valueof(datetime.now().time());     
              
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Aborda.es
    Description:   Method for updating associated discounts
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/05/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateDesc(list<BI_Linea_de_venta__c> olds){
    	
    	Map<Id, Integer> map_desc = new Map<Id, Integer>();
    	
    	for(BI_Linea_de_venta__c ven:olds){
    		
    		if(ven.BI_Codigo_de_descuento_de_modelo__c != null){
    			
    			if(map_desc.containsKey(ven.BI_Codigo_de_descuento_de_modelo__c))
    				map_desc.put(ven.BI_Codigo_de_descuento_de_modelo__c, map_desc.get(ven.BI_Codigo_de_descuento_de_modelo__c) + 1);
    			else
    				map_desc.put(ven.BI_Codigo_de_descuento_de_modelo__c, 1);
    			
    		}
    		
    		if(ven.BI_Codigo_de_descuento_de_SIM__c != null){
    			
    			if(map_desc.containsKey(ven.BI_Codigo_de_descuento_de_SIM__c))
    				map_desc.put(ven.BI_Codigo_de_descuento_de_SIM__c, map_desc.get(ven.BI_Codigo_de_descuento_de_SIM__c) + 1);
    			else
    				map_desc.put(ven.BI_Codigo_de_descuento_de_SIM__c, 1);
    			
    		}
    		
    	}
    	
    	if(!map_desc.isEmpty()){
    		List<BI_Descuento_para_modelo__c> lst_desc = [select Id, BI_Cantidad_disponible__c from BI_Descuento_para_modelo__c where Id IN :map_desc.keySet()];
    		for(BI_Descuento_para_modelo__c descMod:lst_desc){
    			if(descMod.BI_Cantidad_disponible__c != null)
    				descMod.BI_Cantidad_disponible__c += map_desc.get(descMod.Id);
    		}
    		
    		
    		BI_GlobalVariables.stopTriggerDesc = true;
    	
    		update lst_desc;
    	}
    	
    }
}