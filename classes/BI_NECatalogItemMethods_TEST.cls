@isTest
private class BI_NECatalogItemMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_NECatalogItemMethods class 
    
    History:
    
    <Date> 					<Author> 				<Change Description>
    05/05/2014				Pablo Oliva				Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Pablo Oliva
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_NECatalogItemMethods.inactivateCampaign
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	05/05/2014				Pablo Oliva				Initial version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void inactivateCampaignTest() {
		NE__Catalog__c cat = new NE__Catalog__c (Name = 'test catalog');
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		insert cat;
    	
    	List<NE__Promotion__c> lst_promotions = BI_DataLoad.loadPromotions(200, cat.Id);
    	
    	List<NE__Catalog_Item__c> lst_nci = BI_DataLoad.loadNECatalogItems(200, cat.Id, lst_promotions);
    	
    	List<Campaign> lst_campaigns = BI_DataLoad.loadCampaignsforPromo(lst_promotions);
    	
    	Set<Id> set_campaigns = new Set<Id>();
    	for(Campaign cmp:lst_campaigns){
    		cmp.isActive = true;
        	cmp.Status = Label.BI_EnCurso;
        	set_campaigns.add(cmp.Id);
    	}
    	
    	update lst_campaigns;
    	
    	Test.startTest();
    	
    	for(NE__Catalog_Item__c nci:lst_nci)
    		nci.Not_available_for_campaigns__c = true;
    	
    	update lst_nci;
    	
    	for(Campaign cmp2:[select Id, isActive from Campaign where Id IN :set_campaigns])
    		system.assertEquals(cmp2.isActive, false);
    	
    	Test.stopTest();
    	
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}