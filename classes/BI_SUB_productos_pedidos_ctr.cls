/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Alvaro Sevilla
 Company:       Accenture LTDA
 Project:       Subsidios ARG
 Description:

 History:

 <Date>               <Author>             <Change Description>
 23/02/2018         Alvaro Sevilla          Initial Version
 ------------------------------------------------------------------------------------------------*/
public class BI_SUB_productos_pedidos_ctr
{
    public Id IdPed {get;set;}
    public BI_SUB_Pedido_Subsidio__c CamposPedidos {get;set;}
    public List<NE__OrderItem__c> lstOIProd{get;set;}
    public List<NE__Order__c> lstobjOR{get;set;}
    public List<WrpAtributos> lstAtributos {get; set;}
    public String PopUp {get; set;}
    public String TipoPopUp {get; set;}
    public String TituloPopUp {get; set;}
    public String BEnviar {get; set;}
    public Boolean Redireccionar {get; set;}
    public String Estado {get; set;}
    public Boolean Desactivar {get; set;}
    public map<String,Integer> mphijos {get; set;}
    public List<BI_SUB_ProductosOI_PedidoSubsidio__c> lstOIPedido;
    public Map<String, Decimal> mapOfCurrsConversion = new Map<String, Decimal>();
    public list<sObject> listOfCurrencies;

    public BI_SUB_productos_pedidos_ctr(ApexPages.StandardController controller) {
      IdPed = ApexPages.currentPage().getParameters().get('Id');
      if (!Test.isRunningTest()){
        controller.addFields(new List<String>{'BI_SUB_Oportunidad__c','BI_SUB_Estado__c'});
      }
      PopUp = 'none';
      redireccionar = false;

      CamposPedidos = (BI_SUB_Pedido_Subsidio__c)controller.getRecord();
      Estado = CamposPedidos.BI_SUB_Estado__c;
      system.debug('>IdPed< ' +IdPed + '\n >opp< ' + CamposPedidos.BI_SUB_Oportunidad__c+'-Estado->'+Estado);

      lstobjOR =  [select Id, NE__OptyId__c,NE__OrderStatus__c From NE__Order__c where NE__OptyId__c =: CamposPedidos.BI_SUB_Oportunidad__c AND (NE__OrderStatus__c = 'Active' OR NE__OrderStatus__c = 'F1 - Closed Won')];
      system.debug('>lstobjOR< ' +lstobjOR);
      lstOIProd = [select Id,Name, BI_SUB_Disponible__c, BI_SUB_EnDespacho__c, BI_SUB_Enviados__c, BI_SUB_AEnviar__c, NE__Status__c,NE__ProdId__r.Payback_Family__c, NE__Qty__c,
                          NE__One_Time_Cost__c,NE__OneTimeFeeOv__c,NE__Recurring_Cost__c,NE__RecurringChargeOv__c,NE__ProdId__r.Name,NE__OrderId__r.NE__OptyId__r.CurrencyIsoCode,
                          CurrencyIsoCode , NE__CatalogItem__r.NE__Item_Header__r.NE__Catalog_Item__c,NE__CatalogItem__r.NE__Parent_Catalog_Item__c,NE__Account__c,
                          NE__ProdId__c,  NE__CatalogItem__r.NE__One_Time_Cost__c,NE__CatalogItem__r.NE__Recurring_Cost__c, BI_SUB_Grupo__c,NE__CatalogItem__r.NE__Type__c,
                          NE__CatalogItem__r.NE__Recurring_Charge_Frequency__c,NE__CatalogItem__r.NE__Item_Header__r.Name,NE__Parent_Order_Item__c,NE__Root_Order_Item__c
                    from NE__OrderItem__c
                    where NE__OrderId__c =: lstobjOR[0].Id AND (NE__ProdId__r.Payback_Family__c = 'Terminales' OR NE__ProdId__r.Payback_Family__c = 'Planes' OR NE__ProdId__r.Payback_Family__c = 'Servicios') AND NE__ProdId__r.Name != 'Sede' order by NE__CatalogItem__r.NE__Item_Header__r.Name desc];

      lstOIPedido = [select Id,BI_SUB_Junction_Configuration_Item__c,BI_SUB_Junction_Pedido_Subsidio__c from BI_SUB_ProductosOI_PedidoSubsidio__c where BI_SUB_Junction_Pedido_Subsidio__c =: IdPed];
//BI_SUB_Pedido_Subsidio__r.BI_SUB_Oportunidad__c,BI_SUB_Pedido_Subsidio__r.BI_SUB_Oportunidad__r.AccountId,BI_SUB_Pedido_Subsidio__r.BI_SUB_Oportunidad__r.BI_SUB_Subsidio__c
        if(!lstOIProd.isEmpty()){
            TazadeCambio();
            lstAtributos = new List<WrpAtributos>();
            System.debug('--lstOIProd-->'+lstOIProd);
            System.debug('--lstOIPedido-->'+lstOIPedido);
            mphijos = new map<String,Integer>();
            List<WrpAtributos> lstPreorder = new List<WrpAtributos>();
            map<String,List<WrpAtributos>> mpgrupOrden = new map<String, List<WrpAtributos>>();
            List<WrpAtributos> lstOrdenada = new List<WrpAtributos>();
            map<String,List<WrpAtributos>> mpOrden = new map<String,List<WrpAtributos>>();

           for(NE__OrderItem__c item : lstOIProd){

              WrpAtributos producto = new WrpAtributos();
              producto.Id = item.Id;
              producto.Tipo = item.NE__ProdId__r.Payback_Family__c;
              producto.NombreProducto = item.NE__ProdId__r.Name;
              producto.Cantidad = item.NE__Qty__c;
              producto.Disponible = item.BI_SUB_Disponible__c;
              producto.EnDespacho = item.BI_SUB_EnDespacho__c;
              producto.Enviados = item.BI_SUB_Enviados__c;
              producto.AEnviar = String.valueOf(item.BI_SUB_AEnviar__c);
              if(item.NE__OneTimeFeeOv__c != null){
                producto.IUVEditado = ((item.NE__OneTimeFeeOv__c/mapOfCurrsConversion.get(item.CurrencyIsoCode))*mapOfCurrsConversion.get(item.NE__OrderId__r.NE__OptyId__r.CurrencyIsoCode)).setscale(2);
              }else{producto.IUVEditado = 0.00;}

              if(item.NE__RecurringChargeOv__c != null){
                producto.RBMEditado = ((item.NE__RecurringChargeOv__c/mapOfCurrsConversion.get(item.CurrencyIsoCode))*mapOfCurrsConversion.get(item.NE__OrderId__r.NE__OptyId__r.CurrencyIsoCode)).setscale(2);
              }else{
                producto.RBMEditado = 0.00;
              }
              producto.Estado = item.NE__Status__c;
              producto.CabeceraArticulo = item.NE__CatalogItem__r.NE__Item_Header__c;
              producto.TipoLinea = item.NE__CatalogItem__r.NE__Type__c;
              producto.OIPadre = item.NE__Root_Order_Item__c;//item.NE__Parent_Order_Item__c;
              producto.Grupo = item.BI_SUB_Grupo__c;
              producto.DivisaOpty = item.NE__OrderId__r.NE__OptyId__r.CurrencyIsoCode;

              if(!lstOIPedido.isEmpty()){
                for(BI_SUB_ProductosOI_PedidoSubsidio__c elem : lstOIPedido){
                    if(item.Id == elem.BI_SUB_Junction_Configuration_Item__c){
                        producto.Elemento = 'Activo';
                    }
                }
              }
              lstAtributos.add(producto);

              if(item.NE__CatalogItem__r.NE__Item_Header__c != null){
                  if(mphijos.containsKey(item.NE__CatalogItem__r.NE__Item_Header__c)){
                      mphijos.put(item.NE__CatalogItem__r.NE__Item_Header__c, mphijos.get(item.NE__CatalogItem__r.NE__Item_Header__c) + 1);
                  }else{
                      mphijos.put(item.NE__CatalogItem__r.NE__Item_Header__c,1);
                  }
              }
            }
           for(WrpAtributos ord : lstAtributos){
              if(ord.Tipo == 'Terminales'){
                  lstOrdenada.add(ord);

              }else if(ord.Tipo == 'Planes'){

                  if(mpOrden.containsKey(ord.Id)){
                      mpOrden.get(ord.Id).add(ord);
                  }else{
                      mpOrden.put(ord.Id, new List<WrpAtributos>{ord});
                  }
              }else{
                  if(mpOrden.containsKey(ord.OIPadre)){
                      mpOrden.get(ord.OIPadre).add(ord);
                  }else{
                      mpOrden.put(ord.OIPadre, new List<WrpAtributos>{ord});
                  }
              }
          }

          for(String clave : mpOrden.keySet()){
              for(WrpAtributos item : mpOrden.get(clave)){
                  lstOrdenada.add(item);
              }
          }
          lstAtributos = lstOrdenada;

           for(WrpAtributos orden : lstAtributos){
                if(mpgrupOrden.containsKey(orden.Grupo)){
                  mpgrupOrden.get(orden.Grupo).add(orden);
                }else{
                  mpgrupOrden.put(orden.Grupo, new List<WrpAtributos>{orden});
                }
            }
            for(String clave : mpgrupOrden.keySet()){
                for(WrpAtributos item : mpgrupOrden.get(clave)){
                    lstPreorder.add(item);
                }
            }
            lstAtributos = lstPreorder;

            if(lstobjOR[0].NE__OrderStatus__c == 'F1 - Closed Won' && (Estado == 'Enviado' || Estado == 'Completo' || Estado == 'Rechazado' || Estado == 'Cancelado')){
              Desactivar = true;
              BEnviar = 'true';

            }else {
              Desactivar = false;
              BEnviar = 'false';
            }
        }
    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Alvaro Sevilla
 Company:       Accenture LTDA
 Project:       Subsidios ARG
 Description:   Metodo que realiza los calculos y actualizad los OI y crea los registros del objeto junction que los relacion con Pedidos

 History:

 <Date>               <Author>             <Change Description>
 23/02/2018         Alvaro Sevilla          Initial Vertion
 ------------------------------------------------------------------------------------------------*/
    public void Enviar(){
      try{
       Integer temp = 0;
       Boolean error = false;
       Boolean exito = false;
       List<NE__OrderItem__c> lstProductosOItems = new List<NE__OrderItem__c>();
       List<BI_SUB_Pedido_Subsidio__c> lstpedido = new List<BI_SUB_Pedido_Subsidio__c>();
       List<BI_SUB_ProductosOI_PedidoSubsidio__c> lstPxPS = new List<BI_SUB_ProductosOI_PedidoSubsidio__c>();

       for(WrpAtributos elem : lstAtributos){
            if(elem.Tipo == 'Terminales'){
                if(elem.AEnviar != '' && elem.AEnviar != null){
                    if(elem.AEnviar.isNumeric()){
                        if(Integer.valueOf(elem.Disponible) >= 0 && Integer.valueOf(elem.AEnviar) >= 0 ){
                            if(Integer.valueOf(elem.Disponible) < Integer.valueOf(elem.AEnviar)){
                                temp++;
                            }
                        }else{
                            temp++;
                        }
                    }else{
                         temp++;
                    }
                }else{
                  elem.AEnviar = '0';
                }
            }
         }

      if(temp == 0){

          for(WrpAtributos prod : lstAtributos){

              if(prod.Tipo == 'Terminales' && (prod.AEnviar != '' && Integer.valueOf(prod.AEnviar) > 0)){
                  System.debug('-->'+prod.AEnviar);
                  //prod.EnDespacho = Integer.valueOf(prod.EnDespacho) + Integer.valueOf(prod.AEnviar);
                  prod.EnDespacho = Integer.valueOf(prod.AEnviar);
                  prod.Disponible = Integer.valueOf(prod.Disponible) - Integer.valueOf(prod.AEnviar);

                  for(WrpAtributos plan : lstAtributos){
                    if(prod.Grupo == plan.Grupo && (plan.Tipo == 'Planes' || plan.Tipo == 'Servicios')){
                        plan.AEnviar = prod.AEnviar;
                        //plan.EnDespacho = Integer.valueOf(plan.EnDespacho) + Integer.valueOf(prod.AEnviar);
                        plan.EnDespacho = Integer.valueOf(prod.AEnviar);
                        plan.Disponible = Integer.valueOf(plan.Disponible) - Integer.valueOf(prod.AEnviar);
                        System.debug('--Tipo->'+plan.Tipo+'--Grupo-->'+plan.Grupo+'---->'+plan.AEnviar);
                    }
                  }
              }
          }
          System.debug('-lstAtributos-->>>'+lstAtributos);

          for(WrpAtributos prod2 : lstAtributos){
              if(prod2.AEnviar != '' && Integer.valueOf(prod2.AEnviar) > 0){
                for(NE__OrderItem__c oi : lstOIProd){
                    if(prod2.Id == oi.Id){
                        NE__OrderItem__c ProdOI = new NE__OrderItem__c();
                        ProdOI.Id = prod2.Id;
                        ProdOI.BI_SUB_Disponible__c = prod2.Disponible;
                        ProdOI.BI_SUB_EnDespacho__c = oi.BI_SUB_EnDespacho__c + Integer.valueOf(prod2.AEnviar);
                        lstProductosOItems.add(ProdOI);
                    }
                }
              }
          }
          System.debug('-lstProductosOItems-->>>'+lstProductosOItems);
          if(!lstProductosOItems.isEmpty()){

            Database.SaveResult[] lstSub = Database.update(lstProductosOItems, true);
            for (Database.SaveResult sub : lstSub) {
                if (sub.isSuccess()) {
                    System.debug('Exitoso' + sub.getId());
                    exito = true;

                }else {
                    for(Database.Error err : sub.getErrors()) {
                        System.debug('---->'+err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('---->Que produjo el error: ' + err.getFields());
                    }
                }
            }

             if(exito){
                BI_SUB_Pedido_Subsidio__c pedido = new BI_SUB_Pedido_Subsidio__c();
                pedido.Id = IdPed;
                pedido.BI_SUB_Estado__c = 'Enviado';

               // for(NE__OrderItem__c elem : lstProductosOItems){
                    for(WrpAtributos prod3 : lstAtributos){
                      if(prod3.AEnviar != '' && Integer.valueOf(prod3.AEnviar) > 0){
                          BI_SUB_ProductosOI_PedidoSubsidio__c pxps = new BI_SUB_ProductosOI_PedidoSubsidio__c();
                          pxps.BI_SUB_Junction_Pedido_Subsidio__c = IdPed;
                          pxps.BI_SUB_Junction_Configuration_Item__c = prod3.Id;
                          pxps.BI_SUB_Junction_Cantidad_Enviada__c = Integer.valueOf(prod3.AEnviar);
                          lstPxPS.add(pxps);
                      }
                    }
                //}

                insert lstPxPS;
                update pedido;
                redireccionar = true;
              }

          }

      }else{
        ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,' Por favor revisar:'+'\n los campos no puede contener letras ni números negativos ni decimales y la cantidad a enviar no puede ser mayor a la disponible');
        ApexPages.addMessage(mensaje);
        PopUp = 'block';
        TipoPopUp = 'slds-theme_warning';
        TituloPopUp = 'Advertencia';
        BEnviar = 'false';

      }
      }catch(Exception e){

            ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.ERROR,'Ocurrio un error mientras se guardaba: '+e);
            ApexPages.addMessage(mensaje);
            PopUp = 'block';
            TipoPopUp = 'slds-theme_error';
            TituloPopUp = 'Error';
            BEnviar = 'false';
      }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Alvaro Sevilla
 Company:       Accenture LTDA
 Project:       Subsidios ARG
 Description:   Metodo que permite encontrar las tasas de cambio definidas en la instancia para realizar los calculos segun el currencyIsoCode

 History:

 <Date>               <Author>             <Change Description>
 18/05/2018         Alvaro Sevilla          Initial vertion
 ------------------------------------------------------------------------------------------------*/
    public void TazadeCambio(){
        listOfCurrencies = new list<sObject>();

        list<sObject> listOfCurrencies  = [SELECT ConversionRate,CreatedById,CreatedDate,DecimalPlaces,Id,IsActive,IsCorporate,IsoCode,LastModifiedById,LastModifiedDate,SystemModstamp FROM CurrencyType];
        if(!listOfCurrencies.isEmpty()){
            for(sObject currObj:listOfCurrencies){
                Decimal conversionRate = (Decimal)currObj.get('ConversionRate');
                String isoCode     = (String)currObj.get('IsoCode');
                mapOfCurrsConversion.put(isoCode,conversionRate);
            }
        }

        system.debug('mapOfCurrsConversion: '+mapOfCurrsConversion);
    }

    public void Cancelar(){
        PopUp = 'none';
    }

    public class WrpAtributos{

      public String Id {get; set;}
      public String Tipo {get; set;}
      public String NombreProducto {get; set;}
      public Decimal Cantidad {get; set;}
      public Decimal Disponible {get; set;}
      public Decimal EnDespacho {get; set;}
      public Decimal Enviados {get; set;}
      public String AEnviar {get; set;}
      public String AEnviarSVA {get; set;}
      public Decimal IUVEditado {get; set;}
      public Decimal RBMEditado {get; set;}
      public String Estado {get; set;}
      public String CabeceraArticulo {get; set;}
      public String TipoLinea {get; set;}
      public String OIPadre {get; set;}
      public String Grupo {get; set;}
      public String Elemento {get; set;}
      public String DivisaOpty {get;set;}

    }


}