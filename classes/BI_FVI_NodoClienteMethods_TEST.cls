@isTest
private class BI_FVI_NodoClienteMethods_TEST {

	static List<Account> lst_acc = new List<Account>();
    static List<Opportunity> lst_opp =  new List<Opportunity>();
    static List<NE__OrderItem__c> lst_orderItem = new List<NE__OrderItem__c>();


	/*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método de creación de data

    History: 

    <Date>                  <Author>                <Change Description>
    09/06/2016              Geraldine Pérez Montes     Edit method    
    ----------------------------------------------------------------------*/ 
	
	@isTest static void validaNodo_TEST() {
	
		User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

		System.runAs(me){

			NE__Lov__c lov = new NE__Lov__c(
				Name = 'N_CLIENTES_VOLATIL_TEMPORAL_ADQ',
				NE__Active__c = true,
				Country__c = Label.BI_Peru,
				NE__Type__c = 'NODOS',
				NE__Value1__c = '1',
				NE__Value2__c = '1',
				NE__Value3__c = '2'
			);
			insert lov;

			//List<Account> lst_acc = new List<Account>();

			Account cuenta1 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431271525',
			    CurrencyIsoCode = 'MXN',
  			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta1);

			Account cuenta2 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20538273525',
			    CurrencyIsoCode = 'MXN',
			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta2);

			Account cuenta3 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20331176525',
			    CurrencyIsoCode = 'MXN',
  			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta3);

			Account cuenta4 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '57019465G',
			    CurrencyIsoCode = 'MXN',
			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta4);

			Account cuenta5 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '48209965X',
			    CurrencyIsoCode = 'MXN',
   			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta5);


			insert lst_acc;

			List<RecordType> lst_rt = [SELECT Id,DeveloperName 
			                           FROM RecordType 
			                           WHERE DeveloperName = 'BI_FVI_NAV'
			                           OR DeveloperName = 'BI_FVI_NAG'
			                           OR DeveloperName = 'BI_FVI_NAT'
			                           OR DeveloperName = 'BI_FVI_NAB'
			                           OR DeveloperName = 'BI_FVI_NCP'
			                           OR DeveloperName = 'BI_FVI_NCO'];


	       	Id rt_NCP,rt_NAT,rt_NAV,rt_NAB,rt_NAG,rt_NCO;

	       
	        for(RecordType rt : lst_rt)
        	{
	            if(rt.DeveloperName == 'BI_FVI_NCP'){rt_NCP = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAV'){rt_NAV = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAT'){rt_NAT = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAG'){rt_NAG = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NCO'){rt_NCO = rt.Id;}
	            else{rt_NAB = rt.Id;}
       		}
	        
	        BI_FVI_Nodos__c nodo_ncp = new BI_FVI_Nodos__c(
	            Name = 'NCP_TEST', 
	            RecordTypeId = rt_NCP,
	            BI_FVI_Pais__c = Label.BI_Peru
        	);
        	insert nodo_ncp;

        	BI_FVI_Nodos__c ncp = [SELECT Id FROM BI_FVI_Nodos__c WHERE Name = 'NCP_TEST'];

	        List<BI_FVI_Nodos__c> lst_nodosInsert = new List<BI_FVI_Nodos__c>();
	        
	        BI_FVI_Nodos__c	nodo_NAG = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAG_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAG
	        	//BI_FVI_NodoPadre__c = ncp.Id
	        );
	        BI_FVI_Nodos__c	nodo_NCO = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NOC_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NCO
	        	//BI_FVI_NodoPadre__c = ncp.Id
	        );

	        BI_FVI_Nodos__c	nodo_NAT = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAT_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAT,
	        	BI_FVI_NodoPadre__c = ncp.Id

	        );

	        BI_FVI_Nodos__c	nodo_NAV = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAV_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAV,
	        	BI_FVI_NodoPadre__c = ncp.Id

	        );

	        BI_FVI_Nodos__c	nodo_NAB = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAB_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAB
	        	//BI_FVI_NodoPadre__c = ncp.Id

	        );

	        lst_nodosInsert.add(nodo_NAG);
	        lst_nodosInsert.add(nodo_NCO);
	        lst_nodosInsert.add(nodo_NAT);
	        lst_nodosInsert.add(nodo_NAV);
	        lst_nodosInsert.add(nodo_NAB);

	        insert lst_nodosInsert;

	        List<Account> lst_clientes = [SELECT Id FROM Account WHERE Name LIKE '%_TEST'];
	        List<BI_FVI_Nodos__c> lst_nodos = [SELECT Id, Name FROM BI_FVI_Nodos__c WHERE Name LIKE '%_TEST'];

	        Id nag, nat, nav, nab, nco;

	        for(BI_FVI_Nodos__c nodo : lst_nodos){
	        	if(nodo.Name == 'NAG_TEST'){
	        		nag = nodo.Id;
	        	}else if(nodo.Name == 'NAT_TEST'){
	        		nat = nodo.Id;
	        	}else if(nodo.Name == 'NAB_TEST'){
	        		nab = nodo.Id;
	        	}else if(nodo.Name == 'NOC_TEST'){
	        		nco = nodo.Id;
        		}else{
        			nav = nodo.Id;
        		}
	        system.debug('nodo----> ' + nodo);
	        }

	        Test.startTest();

	        List<BI_FVI_Nodo_Cliente__c> lst_nodoClienteInsert = new List<BI_FVI_Nodo_Cliente__c>();

		        BI_FVI_Nodo_Cliente__c nc1 = new BI_FVI_Nodo_Cliente__c(
		        	BI_FVI_IdCliente__c = cuenta1.Id,
		        	BI_FVI_IdNodo__c  = nat
		        );
		        lst_nodoClienteInsert.add(nc1);
		        // insert nc1;
		        BI_FVI_Nodo_Cliente__c nc2 = new BI_FVI_Nodo_Cliente__c(
		        	BI_FVI_IdCliente__c = cuenta2.Id,
		        	BI_FVI_IdNodo__c  = nav
		        );
		        lst_nodoClienteInsert.add(nc2);
		        // insert nc2;
		        BI_FVI_Nodo_Cliente__c nc3 = new BI_FVI_Nodo_Cliente__c(
		        	BI_FVI_IdCliente__c = cuenta3.Id,
		        	BI_FVI_IdNodo__c  = nag
		        );
		        lst_nodoClienteInsert.add(nc3);
		        //insert nc3;
		        BI_FVI_Nodo_Cliente__c nc4 = new BI_FVI_Nodo_Cliente__c(
		        	BI_FVI_IdCliente__c = cuenta4.Id,
		        	BI_FVI_IdNodo__c  = nab
		        );
		        lst_nodoClienteInsert.add(nc4);
		        // insert nc4;
		         BI_FVI_Nodo_Cliente__c nc5 = new BI_FVI_Nodo_Cliente__c(
		        	BI_FVI_IdCliente__c = cuenta5.Id,
		        	BI_FVI_IdNodo__c  = nco
		        );
		        lst_nodoClienteInsert.add(nc5); 
		        // insert nc5;

		        insert lst_nodoClienteInsert;

		    Test.stopTest();
		}
	}


	/*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método de creación de data

    History: 

    <Date>                  <Author>                			<Change Description>
    09/06/2016              Geraldine Pérez Montes     			Edit method    
    03/08/2016				Humberto Nunes						Se omitio el System.AssertEquals(ex_esperada, true); en el Catch
    25/08/2016				Humberto Nunes						Se realizaron cambios para asignar tipos de registros, afecto todo el metodo.
    ----------------------------------------------------------------------*/ 
	@isTest static void actualizaNodoEnCliente_TEST(){
		
		User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

		System.runAs(me)
		{
			//dataLoad();
			List<RecordType> lst_rt = [SELECT Id,DeveloperName 
			                           FROM RecordType 
			                           WHERE DeveloperName = 'BI_FVI_NAV'
			                           OR DeveloperName = 'BI_FVI_NAG'
			                           OR DeveloperName = 'BI_FVI_NAT'
			                           OR DeveloperName = 'BI_FVI_NCP'
			                           OR DeveloperName = 'BI_FVI_Ciclo_completo'
			                           OR DeveloperName = 'BI_FVI_Contrato'];


	       	Id rt_NCP,rt_NAT,rt_NAV,rt_NAB,rt_NAG,rt_NCO,rt_opp,rt_Cont;

	       
	        for(RecordType rt : lst_rt)
        	{
	            if(rt.DeveloperName == 'BI_FVI_NCP'){rt_NCP = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAV'){rt_NAV = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAT'){rt_NAT = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAG'){rt_NAG = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_Ciclo_completo'){rt_opp = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_Contrato'){rt_Cont = rt.Id;}
       		}

			BI_FVI_Nodos__c nodo_NCP = new BI_FVI_Nodos__c(
	            Name = 'NCP_TEST', 
	            RecordTypeId = rt_NCP,
	            BI_FVI_Pais__c = Label.BI_Peru
        	);
        	insert nodo_ncp;

        	BI_FVI_Nodos__c ncp = [SELECT Id FROM BI_FVI_Nodos__c WHERE Name = 'NCP_TEST'];
Test.startTest();
	        List<BI_FVI_Nodos__c> lst_nodosInsert = new List<BI_FVI_Nodos__c>();
	        
	        BI_FVI_Nodos__c	nodo_NAG = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAG_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAG
	        );

	        BI_FVI_Nodos__c	nodo_NAT = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAT_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAT,
	        	BI_FVI_NodoPadre__c = ncp.Id
	        );

	        BI_FVI_Nodos__c	nodo_NAV = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAV_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAV,
	        	BI_FVI_NodoPadre__c = ncp.Id
	        );

	        lst_nodosInsert.add(nodo_NAG);
	        lst_nodosInsert.add(nodo_NAT);
	        lst_nodosInsert.add(nodo_NAV);

	        insert lst_nodosInsert;
	        system.debug('list nodos ultima ====>>>' + lst_nodosInsert);


			NE__Lov__c lov = new NE__Lov__c(
				Name = 'N_CLIENTES_VOLATIL_TEMPORAL_ADQ',
				NE__Active__c = true,
				Country__c = Label.BI_Peru,
				NE__Type__c = 'NODOS',
				NE__Value1__c = '2',
				NE__Value2__c = '2',
				NE__Value3__c = '2'
			);
			insert lov;

			//List<Account> lst_acc = new List<Account>();

			Account cuenta1 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431271525',
			    CurrencyIsoCode = 'MXN',
   			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    // BI_FVI_Nodo__c = lst_nodosInsert[3].Id,
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta1);

			Account cuenta2 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20538273525',
			    CurrencyIsoCode = 'MXN',
			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    // BI_FVI_Nodo__c = lst_nodosInsert[3].Id,
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta2);

			Account cuenta3 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20331176525',
			    CurrencyIsoCode = 'MXN',
   			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    // BI_FVI_Nodo__c = lst_nodosInsert[3].Id,
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta3);

			Account cuenta4 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20221176525',
			    CurrencyIsoCode = 'MXN',
   			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    // BI_FVI_Nodo__c = lst_nodosInsert[3].Id,
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta4);

			Account cuenta5 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20111176525',
			    CurrencyIsoCode = 'MXN',
			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',			    
			    // BI_FVI_Nodo__c = lst_nodosInsert[3].Id,
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta5);

			insert lst_acc;

			  BI_Col_Ciudades__c objCiudad        = new BI_Col_Ciudades__c ();
                  objCiudad.Name                      = 'Test City';
                  objCiudad.BI_COL_Pais__c            = 'Test Country';
                  objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
                  insert objCiudad;

                   BI_Sede__c objSede = new BI_Sede__c();
                  objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
                  objSede.BI_Direccion__c         = 'Test Street 123 Number 321';
                  objSede.BI_Localidad__c         = 'Test Local';
                  objSede.BI_COL_Estado_callejero__c    = System.Label.BI_COL_LbValor3EstadoCallejero;
                  objSede.BI_COL_Sucursal_en_uso__c     = 'Libre';
                  objSede.BI_Country__c           = 'Colombia';
                  objSede.Name              = 'Test Street 123 Number 321, Test Local Colombia';
                  objSede.BI_Codigo_postal__c       = '12356';
                  insert objSede;

                  //Sede
                  BI_Punto_de_instalacion__c objPuntosInsta = new BI_Punto_de_instalacion__c ();
                  objPuntosInsta.BI_Cliente__c       = lst_acc[0].Id;
                  objPuntosInsta.BI_Sede__c          = objSede.Id;
                  //objPuntosInsta.BI_Contacto__c      = objContacto.id;
                  objPuntosInsta.Name         = 'Nombre sede prueba';
                  insert objPuntosInsta;

			 //List<Contact> lst_cont = new List<Contact>();
    //              for(Integer i = 0; i < 3; i++){
    //                    Contact con = new Contact(
    //                          LastName = 'test'+ String.valueOf(i),
    //                          AccountId = lst_acc[i].Id,
    //                          Email = 'test'+ String.valueOf(i)+'@gmail.com',
    //                          HasOptedOutOfEmail = false,
    //                          BI_Activo__c = true,
    //                          BI_Tipo_de_documento__c = 'NIF',
    //                          BI_Country__c = Label.BI_Peru,
    //                          BI_Numero_de_documento__c = '0000000' + i,
    //                          Phone = '12345678',
    //                          MobilePhone='1234567890',
    //                          BI_COL_Direccion_oficina__c='bdxjabsdgas',
    //                          BI_COL_Ciudad_Depto_contacto__c = objCiudad.Id,
    //                          BI_Tipo_de_contacto__c='Cartera'
    //                    );
    //                    lst_cont.add(con);
    //              }

    //              insert lst_cont;
                // RecordType rt_cc= [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_Ciclo_completo'];//BI_Ciclo_completo

			//List<Opportunity> lst_opp = new List<Opportunity>();

            Opportunity oppCaso1 = new Opportunity(
                Name = 'XXX OppZytrust Caso 1',
                BI_Country__c = 'Peru',
                        AccountId = lst_acc[0].Id,
                RecordTypeId = rt_opp,
                BI_Opportunity_Type__c = 'Móvil',
                BI_Duracion_del_contrato_Meses__c = 14,
                BI_Plazo_estimado_de_provision_dias__c = 22,
                CloseDate = Date.today() + 15,
                BI_Licitacion__c = 'No',
                BI_Probabilidad_de_exito__c = '95',
                CurrencyIsoCode = 'MXN',
                StageName = 'F6 - Prospecting',
                BI_PER_Fecha_max_registro_participante__c = Date.today(),
                        BI_Requiere_contrato__c = true
                        //BI_FVI_Contacto__c = lst_cont[0].Id
            );
            lst_opp.add(oppCaso1);

             Opportunity oppCaso2 = new Opportunity(
                Name = 'XXX OppZytrust Caso 2',
                BI_Country__c = 'Peru',
                        AccountId = lst_acc[1].Id,
                RecordTypeId = rt_opp,
                BI_Opportunity_Type__c = 'Fijo',
                BI_Duracion_del_contrato_Meses__c = 14,
                BI_Plazo_estimado_de_provision_dias__c = 22,
                CloseDate = Date.today() + 15,
                BI_Licitacion__c = 'Si',
                BI_Probabilidad_de_exito__c = '95',
                CurrencyIsoCode = 'MXN',
                StageName = 'F6 - Prospecting',
                BI_PER_Fecha_max_registro_participante__c = Date.today(),
                        BI_Requiere_contrato__c = true
                       //BI_FVI_Contacto__c = lst_cont[1].Id
            );
            lst_opp.add(oppCaso2);

             Opportunity oppCaso3 = new Opportunity(
                Name = 'XXX OppZytrust Caso 3',
                BI_Country__c = 'Peru',
                        AccountId = lst_acc[3].Id,
                RecordTypeId = rt_opp,
                BI_Opportunity_Type__c = 'Móvil',
                BI_Duracion_del_contrato_Meses__c = 14,
                BI_Plazo_estimado_de_provision_dias__c = 22,
                CloseDate = Date.today() + 15,
                BI_Licitacion__c = 'No',
                BI_Probabilidad_de_exito__c = '95',
                CurrencyIsoCode = 'MXN',
                StageName = 'F6 - Prospecting',
                BI_PER_Fecha_max_registro_participante__c = Date.today(),
                        BI_Requiere_contrato__c = true
                        //BI_FVI_Contacto__c = lst_cont[2].Id
            );
            lst_opp.add(oppCaso3);

            Insert lst_opp;

             NE__Catalog_Header__c catalogheaderobj = new NE__Catalog_Header__c(
                    CurrencyIsoCode = 'MXN',
                    Name = 'Catalog Test WS Zytrust',
                    NE__Active__c = true,
                    NE__Enable_Global_Cart__c = false,
                    NE__Name__c = 'Catalog WS Zytrust',
                    NE__Source_Catalog_Header_Id__c = 'a1R11000000H0tc',
                    NE__Start_Date__c = system.today()-1
                  ); 
                  insert catalogheaderobj;
            
                  NE__Catalog__c catalogobj = new NE__Catalog__c(
                    CurrencyIsoCode = 'MXN',
                    Name = 'Catalog Test WS Zytrust x',
                    NE__Active__c = true,
                    NE__Catalog_Header__c = catalogheaderobj.Id,
                    NE__Enable_for_oppty__c = false,
                    NE__Engine_Code__c = 'a1U110000002812',
                    NE__Source_Catalog_Id__c = 'a1U110000002812',
                    NE__Version__c = 1.0,
                    NE__Visible_web__c = false
                  ); 
                  insert catalogobj;

                  NE__Catalog_Category__c catalogCategory = new NE__Catalog_Category__c(
                        Name = 'Movil',
                        NE__CatalogId__c = catalogobj.Id
                  );

                  List<NE__Product__c> lst_prod = new List<NE__Product__c>();
                  NE__Product__c prod1 = new NE__Product__c(
                        Name = 'Plan vuela 1',
                        Payback_Family__c = 'Planes'
                  );
                  NE__Product__c prod2 = new NE__Product__c(
                        Name = 'Plan vuela 2',
                        Payback_Family__c = 'Planes'
                  );
                  NE__Product__c prod3 = new NE__Product__c(
                        Name = 'Iphone 5',
                        Payback_Family__c = 'Planes'
                  );
  

                  /////---nuevo despues de 75%

                  lst_prod.add(prod1);
                  lst_prod.add(prod2);
                  lst_prod.add(prod3);

                  insert lst_prod;                
                    
                  List<NE__Catalog_Item__c> lst_ci = new List<NE__Catalog_Item__c>();
                  NE__Catalog_Item__c catalogItem1 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[0].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );
                  NE__Catalog_Item__c catalogItem2 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[1].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );
                  NE__Catalog_Item__c catalogItem3 = new NE__Catalog_Item__c(
                        NE__BaseRecurringCharge__c = 16,
                        BI_FVI_Valor_Acto_Adq__c = 1024,
                        BI_FVI_Valor_Acto_Des__c = 64,
                        BI_FVI_Valor_Acto_Fid__c= 32,
                        NE__ProductId__c = lst_prod[2].Id,
                        NE__Catalog_Category_Name__c = catalogCategory.Id,
                        NE__Catalog_Id__c = catalogobj.Id
                  );

 
                  lst_ci.add(catalogItem1);
                  lst_ci.add(catalogItem2);
                  lst_ci.add(catalogItem3);

                  insert lst_ci;

                  List<NE__Order__c> lst_order = new List<NE__Order__c>();
                  NE__Order__c order1 = new NE__Order__c(
                        NE__OptyId__c = lst_opp[0].Id,//oppCaso1.Id,
                        NE__OrderStatus__c   =   'Pending',
                        NE__AccountId__c = lst_acc[0].Id,
                        NE__CatalogId__c = catalogobj.Id
                  );
                  NE__Order__c order2 = new NE__Order__c(
                        NE__OptyId__c = lst_opp[1].Id,//oppCaso2.Id,
                        NE__OrderStatus__c   =   'Sent',
                        NE__AccountId__c = lst_acc[1].Id,
                        NE__CatalogId__c = catalogobj.Id
                  );

                  lst_order.add(order1);
                  lst_order.add(order2);
                  insert lst_order;

                 
                  //CASO 1
                  NE__OrderItem__c orderItem1 = new NE__OrderItem__c(
                        NE__OrderId__c = order1.Id, 
                        Configuration_SubType__c = 'Alta',
                        NE__Status__c = 'Pendiente de envío',
                        NE__Account__c = lst_acc[0].Id,
                        NE__Qty__c = 1,
                        NE__ProdId__c = lst_prod[0].Id,//lst_prod[4].Id,
                        NE__BaseOneTimeFee__c =  25,
                        NE__CatalogItem__c = lst_ci[0].Id//catalogItem1.Id
                  );
                  lst_orderItem.add(orderItem1);

                  //CASO 2
                  NE__OrderItem__c orderItem2 = new NE__OrderItem__c(
                        NE__OrderId__c = order1.Id, 
                        Configuration_SubType__c = 'Portabilidad',
                        NE__Status__c = 'Pending',
                        NE__Account__c = lst_acc[1].Id,
                        NE__Qty__c = 1,
                        NE__ProdId__c = lst_prod[1].Id,//lst_prod[4].Id,
                        NE__BaseOneTimeFee__c =  25,
                        NE__CatalogItem__c = lst_ci[1].Id//catalogItem2.Id
                  );

                  lst_orderItem.add(orderItem2);
                  NE__OrderItem__c orderItem3 = new NE__OrderItem__c(
                        NE__OrderId__c = order2.Id, 
                        Configuration_SubType__c = null,
                        NE__Status__c = 'Enviado', 
                        NE__Qty__c = 1,
                        NE__Account__c = lst_acc[2].Id,
                        NE__ProdId__c = lst_prod[2].Id,//lst_prod[4].Id,
                        NE__BaseOneTimeFee__c =  25,
                        NE__CatalogItem__c = lst_ci[2].Id//catalogItem3.Id
                  );
                  lst_orderItem.add(orderItem3);
                  

                  insert lst_orderItem;

            Contract contrato = new Contract(
                  AccountId = lst_acc[0].Id,
                  Name = 'Contrato2',
                  BI_FVI_Autoriza__c = true,
                  BI_FVI_Cobertura__c = false,
                  BI_FVI_Autoriza_representante__c = false,
                  BI_FVI_desconoce_CPF__c = false,
                  BI_FVI_Control_Parental__c = false,
                  BI_Oportunidad__c = lst_opp[0].Id,
                  BI_FVI_IdFileTxn__c = null,
                  RecordTypeId = rt_Cont,
                  BI_COL_Formato_Tipo__c = 'Tipo Cliente',
                  BI_COL_Tipo_contrato__c = 'Condiciones Uniformes',
                  ContractTerm = 12,
                  BI_COL_Presupuesto_contrato__c  =12000,
                  BI_FVI_No_de_linea__c = '10',
                  BI_Indefinido__c = 'No',
                  StartDate = system.Today()

            );

            insert contrato;

			BI_FVI_Nodo_Cliente__c nc1 = new BI_FVI_Nodo_Cliente__c(
	        	BI_FVI_IdCliente__c = lst_acc[0].Id,
	        	BI_FVI_IdNodo__c  = lst_nodosInsert[2].Id
	        );
	        insert nc1;
	        BI_FVI_Nodo_Cliente__c nc2 = new BI_FVI_Nodo_Cliente__c(
	        	BI_FVI_IdCliente__c = lst_acc[1].Id,
	        	BI_FVI_IdNodo__c  = lst_nodosInsert[2].Id
	        );
	        insert nc2;

	        Inventario_Competencia__c InventCom = new Inventario_Competencia__c(
	        	Cuenta__c = lst_acc[0].Id,
	        	Cantidad__c = 12
	        	);
	        insert InventCom;

	        BI_FVI_Historico_Parque_y_Competencia__c HistoPar = new BI_FVI_Historico_Parque_y_Competencia__c(
	        	BI_FVI_Cuenta__c  = lst_acc[0].Id,
	        	BI_FVI_Cantidad_Total__c = 10
	        );
	        insert HistoPar;

	        BI_FVI_Recomendaciones__c recom =  new BI_FVI_Recomendaciones__c(
	        	BI_FVI_Titulo_Recomendacion__c = 'prueba'
	        	);
	        insert recom;

	        BI_FVI_Recomendacion_Cliente__c recomCliente = new BI_FVI_Recomendacion_Cliente__c(
	        	BI_FVI_Cuenta_Junction__c = lst_acc[0].Id,
	        	BI_FVI_Recomendacion_Junction__c = recom.Id
	        	);
	         insert recomCliente;

	         NE__Asset__c parque = new NE__Asset__c(
	         	NE__AccountId__c = lst_acc[0].Id
	         	);
	         insert parque;

	         //Event evento = new Event(
	         //	WhatId = lst_acc[0].Id,
	         //	DurationInMinutes = 20,
	         //	Description = 'prueba',
	         //	ActivityDateTime = System.today(),
	         //	StartDateTime = System.today(),
	         //	EndDateTime = System.today()
	         //	//WhoId = lst_acc[0].Id

	         //	);
	         //insert evento;

			Test.stopTest();
			try{
				
		        BI_FVI_Nodo_Cliente__c nc3 = new BI_FVI_Nodo_Cliente__c(
		        	BI_FVI_IdCliente__c = lst_acc[3].Id,
		        	BI_FVI_IdNodo__c  = lst_nodosInsert[2].Id
		        );
		        insert nc3;

		        update nc1;


				throw new BI_Exception('Test'); 
			}catch(Exception e){

				Boolean ex_esperada =  e.getMessage().contains('" solo admite [') ? true : false;
				
				system.debug('Exception ====>>>' +  e.getMessage());

				 System.AssertEquals(ex_esperada, true);
			} 

	        delete nc1;
        }
	}
}