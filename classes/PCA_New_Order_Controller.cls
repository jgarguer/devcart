public without sharing class PCA_New_Order_Controller {
	public String mySite {get; set;}
    public NE__OrderItem__c confItem {get; set;}
    public String additionalCSSUrl {get; set;}
    
    public NE.NewConfigurationController configuration {get; set;}
    public Attachment myAttachment {get; set;}
    
    public Map<Id, Boolean> hiddenInCWPMap {get; set;}
    public String mapKeys {get; set;}
    
    /*public Attachment myAttachment {
      get {
          if (myAttachment == null)
            myAttachment = new Attachment();
          return myAttachment ;
        }
      set;
      }*/
    public String attDescription {get; set;}
    public String myAttachmentBody {get; set;}
    public String myAttachmentName {get; set;}
    
    public boolean mandatoryAttachment {get; set;}
    public NE__Product__c comProd {get; set;}
    public String cid {get; set;}
    
    public String quantity{get; set;}
    public boolean isRegistration {get; set;}

    public PCA_New_Order_Controller() {
        
        
      
        String errorAttachment = ApexPages.currentPage().getParameters().get('notAttached');
        if(errorAttachment!=null && errorAttachment!=''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachment is mandatory'));    
        }       
    
    
        //READING GET PARAMETERS
   
        cid = ApexPages.currentPage().getParameters().get('cId');
        //quantity = ApexPages.currentPage().getParameters().get('quantity');
        //
        mandatoryAttachment = true;
        myAttachment = new Attachment();
        
        NE__Catalog_Item__c catalogItem;
        NE__Catalog_Item__c[] subCatalogItems;
        Set<Id> products = new Set<Id>();
        
        if(cid!=''&& cid!=null){
            try {
                catalogItem = [SELECT Id, NE__ProductId__c, NE__Technical_Behaviour__c 
                                        FROM NE__Catalog_Item__c 
                                        WHERE Id = :cid];
                subCatalogItems = [SELECT Id, NE__ProductId__c, NE__Technical_Behaviour__c 
                                        FROM NE__Catalog_Item__c 
                                        WHERE NE__Root_Catalog_Item__c =: cid 
                                        OR Id = :cid];
                for (NE__Catalog_Item__c catItem : subCatalogItems){
                    products.add(catItem.NE__ProductId__c);
                }
                //subCatalogItems = [SELECT]
            } catch (Exception e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid catalog Item'));
                return;
            }
            
            mandatoryAttachment = catalogItem.NE__Technical_Behaviour__c != null && catalogItem.NE__Technical_Behaviour__c.contains('Attachment Required');
            
            /*        
            mandatoryAttachment  = 0<[SELECT Id 
                                        FROM NE__Catalog_Item__c 
                                        WHERE Id =: cid AND NE__Technical_Behaviour__c INCLUDES ('Attachment Required')].size();
            */
            System.debug('### mandatoryAttachment : ' + mandatoryAttachment  );
            System.debug('### Parámetros leídos');
        }
        //
        String suid = ApexPages.currentPage().getParameters().get('suId');
        String userId = UserInfo.getUserId();
        Account userAC = TGS_Portal_Utils.getUserAccount(userId);

        String company = ApexPages.currentPage().getParameters().get('company');
        String user = ApexPages.currentPage().getParameters().get('user');
        String payer = ApexPages.currentPage().getParameters().get('payer');
        mySite = ApexPages.currentPage().getParameters().get('site');
        
        
        
        System.debug('### Parámetros leídos ' );
        
     
        //HERE WE SELECT BETWEEN NEW ORDER AND NEW MODIFICATION
        if(cid!=''&& cid!=null){//NEW ORDER
            System.debug('############ CID not null: ' + userAC.Id + ', ' + user + ', ' + payer + '; CID: ' + cid);
            ApexPages.currentPage().getParameters().put('accId',userAC.Id);
            ApexPages.currentPage().getParameters().put('servAccId',user);
            ApexPages.currentPage().getParameters().put('billAccId',payer);
            ApexPages.currentPage().getParameters().put('cType','New');
			
            //INICIALIZING CID
            system.debug('@@@@@@@@@@Antes del comProd');
            comProd = [SELECT Name FROM NE__Product__c WHERE Id IN (SELECT NE__ProductId__c FROM NE__Catalog_Item__c WHERE id =: cid)];            
            //system.debug('@@@@@@@@@@Despues del comProd: ' + comProd.Name);
            
            isRegistration = true;
            

        }else{//MODIFICATION
            System.debug('### Es una modificación');
            /* ApexPages.currentPage().getParameters().put('accId',userAC.Id);
            ApexPages.currentPage().getParameters().put('servAccId',user);
            ApexPages.currentPage().getParameters().put('billAccId',payer); */
            ApexPages.currentPage().getParameters().put('cType','Change');

            //INICIALIZING CID
            system.debug('@@@@@@@@@@Antes del comProd (modification)');
            //comProd = [SELECT Name FROM NE__Product__c WHERE Id IN (SELECT NE__ProdId__c FROM NE__OrderItem__c WHERE id =: suid)];
           // system.debug('@@@@@@@@@@Despues del comProd: ' + comProd.Name);
           
           isRegistration = false;
           
        }
        
        /*
        ApexPages.currentPage().getParameters().put('accId','001m000000AIqxFAAT');
        ApexPages.currentPage().getParameters().put('servAccId','001m000000AIqxFAAT');
        ApexPages.currentPage().getParameters().put('billAccId','001m000000AIqxFAAT');
        ApexPages.currentPage().getParameters().put('cType','New');
        */
        System.debug('### Antes de instanciar controller');
        configuration = new NE.NewConfigurationController();
        System.debug('### Después de instanciar controller');
        
        
        
        additionalCSSUrl = configuration.cartConfiguration.cartConfigurationObject.NE__Additional_CSS__c.substringAfterLast('/');
        System.debug('### additionalCSS: ' + additionalCSSUrl);
        
        //// Dar valor al mandatoryAttachment
        if(cid==''|| cid==null){
            String auxId;
            System.debug('### AUXID: ' + auxId);
            try{
                auxId = ApexPages.currentPage().getParameters().get('rootItemVId');
            }catch(Exception e){
                System.debug('$%&$%&$%& EL ERROR ES: ' + e);
            }            
            System.debug('### AUXID: ' + auxId);
            NE__OrderItem__c auxOrderItem = [SELECT Id, NE__CatalogItem__c, 
                                                  NE__CatalogItem__r.NE__Technical_Behaviour__c 
                                                  FROM NE__OrderItem__c 
                                                  WHERE Id = :auxId];
            System.debug('### SELECT: ' + auxOrderItem.NE__CatalogItem__r.NE__Technical_Behaviour__c);
            mandatoryAttachment = auxOrderItem.NE__CatalogItem__r.NE__Technical_Behaviour__c != null && auxOrderItem.NE__CatalogItem__r.NE__Technical_Behaviour__c.contains('Attachment Required');            
        }
        
        
        System.debug('### FIN');
        
        if(cid!=''&& cid!=null){//NEW ORDER
            
            //NE__Catalog_Item__c myCatItem = [SELECT Id FROM NE__Catalog_Item__c WHERE NE__ProductId__c = :sid LIMIT 1][0];     //myCatItem.Id
            
            
            //String XML =  '<Workspace action="add"><ListOfPromotions /><ListOfProducts><Item id="a0Am0000000SGVDEA4" vid="" quantity="1" /></ListOfProducts></Workspace>';
            String XML = '<Workspace action="add" isAddAndConfigure="false"><ListOfPromotions /><ListOfProducts><Item id="' + cId + '" vid="" quantity="1" /></ListOfProducts></Workspace>';                       
            //String XML = '<Workspace action="add" isAddAndConfigure="false"><ListOfPromotions /><ListOfProducts><Item id="' + cId + '" vid="" quantity="' + quantity + '" /></ListOfProducts></Workspace>';                       
            
            system.debug('@@@@@@@@@@List/String');        

            configuration.selectedItemsXml = XML;   
            system.debug('@@@@@@@@@@XML ' + configuration.selectedItemsXml);            
            configuration.addItemsToCart();
            system.debug('@@@@@@@@@@AddItem');            
            system.debug('@@@@@@@@@@ Cart: ' + configuration.cartQty);  
            
           for(NE.Item it:configuration.Cart){
                configuration.itemToConfigure = it.id;
           }
           
           
           /*if(){
               myAttachment = new Attachment();
               // myAttachment.folderid = UserInfo.getUserId(); //this puts it in My Personal Documents

           }*/
           

           
           configuration.configureProduct();
           
           additionalCSSUrl = configuration.cartConfiguration.cartConfigurationObject.NE__Additional_CSS__c.substringAfterLast('/');
           System.debug('### additionalCSS: ' + additionalCSSUrl);
           
            /* -- JARA --
             * Explanation:
             * We cannot access to TGS_Hidden_in_CWP__c field from the B2W list, because the query that is used in the controller
             * is not visible and we cannot add that field to it.
             * Instead, we have repeated the query to get the TGS_Hidden_in_CWP__c field for each Product Family Property.
             * Then, we fill a Map<Id, Boolean> with the Id of the Product Family Property as the key and TGS_Hidden_in_CWP__c
             * as the value.
             * We use the map in the visualforce page to get the value of the field based on the Id of the property.
             * THIS ONLY WORKS WITH SIMPLE PRODUCTS. TO-DO: QUERY FOR SUB-SERVICES IN COMPLEX PRODUCTS
             */
           
           /* NE__ProductFamilyProperty__c[] prodFamProps = [SELECT Id, TGS_Hidden_in_CWP__c FROM NE__ProductFamilyProperty__c WHERE NE__FamilyId__c IN 
               (SELECT NE__FamilyId__c FROM NE__ProductFamily__c WHERE NE__ProdId__c IN :products)];
           
            hiddenInCWPMap = new Map<Id, Boolean>();
            mapKeys = '';
            for (NE__ProductFamilyProperty__c prop : prodFamProps) {
                hiddenInCWPMap.put(prop.Id, prop.TGS_Hidden_in_CWP__c);
                mapKeys += prop.Id + ';';
            }
            */
        }

    }
   
    
///////// METODOS //////////    
/*
    public PageReference saveConfiguration(){
        configuration.complexProdController.saveConfiguration();
        System.debug('### save configuration: ' + configuration.ord.Id);

        return null;
    }    */
    //ADDED BY GABRIELE
    public void TGSStartConfigurator()
    {
      String rootCatItemVId                   =   configuration.pageParameters.get('rootItemVId'); //otherwise get the rootItemVID page parameter
      configuration.itemToConfigure          =   rootCatItemvId;  
         if(rootCatItemvId != null && rootCatItemVId != ''){        
            configuration.configureProduct();
        } 

        //Alternative
        /* if(rootCatItemvId != null && rootCatItemVId != ''){
         configuration.complexProdController = new NE.ProductConfiguratorController(configuration,rootCatItemVId);              
        } */
        

    }

    
    public PageReference saveAndCloseConfiguration(){
    //System.debug('### myAttachment.Description: ' + attDescription);
        System.debug('Save and Close Configuration STARTS');
        if (mandatoryAttachment && isRegistration && (myAttachment == null || myAttachment.body == null)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachment is mandatory'));
            //return ApexPages.currentPage();
            PageReference pageRef = ApexPages.currentPage();
            pageRef.getParameters().put('notAttached','true');
            pageRef.setRedirect(true);
            return pageRef;
            
           // return null;
        } 
        System.debug('Attachments body');
        /*
        if (mandatoryAttachment && (myAttachment.description == null || myAttachment.description == '')) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'File description is mandatory'));
            return ApexPages.currentPage();
        }*/
        System.debug('Attachments description');
        //try {
            System.debug('Save and Close Configuration');
            configuration.complexProdController.saveAndCloseConfiguration();
        /*} catch (Exception e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating configuration'));
            System.debug(e);
            return ApexPages.currentPage();
        }*/
            //configuration.configureProduct();
            //returnPage = configuration.configureProduct();
        System.debug('### save and close configuration: ' + configuration.ord.Id);
        List<NE__Order__c> configs = new List<NE__Order__c>();
        if (configuration.ord.Id != null) { 
            configs = [SELECT case__c,case__r.caseNumber, Id  FROM NE__Order__c WHERE Id = :configuration.ord.Id];
            if (mySite!=null && mySite!=''){
                confItem=[SELECT Id,Installation_point__c FROM NE__OrderItem__c WHERE NE__OrderId__c IN (SELECT Id FROM NE__Order__c WHERE Id = :configs[0].Id )]; 
           		confItem.Installation_point__c=mySite;
                update confItem;
            }
            if(mandatoryAttachment && myAttachment!=null && myAttachment.body != null) {
                try{              
                    String pid = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: configuration.ord.Id][0].Id; 
                    // AND NE__CatalogItem__c IN :catItems];

                    myAttachment.ParentId = pid;
                    myattachment.OwnerId = UserInfo.getUserId();
                    myattachment.IsPrivate = false;
                    System.debug('##################### Antes del insert: ' + myAttachment.ParentId);
                    insert myAttachment;
                    
                    System.debug('### Insert: ' + myAttachment.Id);
                }catch(exception e){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                    System.debug(e);
                    return ApexPages.currentPage();
                }
                                   
            } 
            
           
        }
        System.debug('Redirecting...');
         if (configs.size() > 0) {
             //PageReference pageRef = new PageReference('/PCA_Address_B2W?caseId=' + configs[0].case__r.Id);
             PageReference pageRef = new PageReference('/PCA_Cases?orderNumber=' + configs[0].case__r.caseNumber);
             return pageRef;
         }
        return null;
    } 
    
    
    
    public PageReference cancel(){
        if (cid != null && cid != ''){
            return new PageReference('/PCA_External_B2W?cId=' + cid);      
        }else{
            String suId = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: configuration.ord.Id][0].Id;
            return new PageReference('/PCA_Installed_Services?suId=' + suId);
        }
        
    }
  

//////////////
    /*
    public PageReference loadAttachment() {
        System.debug('### load attachment');
        return null;
    }
    
    public PageReference upload(String orderItemId) {

        myattachment.OwnerId = UserInfo.getUserId();
        myattachment.ParentId = orderItemId; // the record the file is attached to
        myattachment.IsPrivate = true;
     
        try {
          insert myattachment;
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
          return null;
        } finally {
          myattachment = new Attachment(); 
        }
    
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        return null;
    }
    
    
        <apex:actionFunction action="{!configuration.complexProdController.saveConfiguration}" name="saveConfiguration" status="actStatusLoad">
        </apex:actionFunction>

        <apex:actionFunction action="{!configuration.complexProdController.saveAndCloseConfiguration}" name="saveAndCloseConfiguration" status="actStatusLoad">
        </apex:actionFunction>
    */
    
    
}