/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Alejandro Labrin
Company:		Everis Centers Temuco
Description:	Mock para simular la llamada al metodo 

History:      Creación de operación

<Date>                      <Author>                        <Change Description>
05/11/2016                  Alejandro Labrin				Initial Version
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

global class FS_CHI_Risk_Management_Mock implements HttpCalloutMock{
    /*SALESFORCE VARIABLE DECLARATION*/
    /*No Entries*/
    
    global static HTTPResponse respond (HttpRequest request) {
        System.debug(request.getEndpoint());
        /*DATA DEFINITION*/
        HttpResponse response = new HttpResponse();	
        
        try{//(request.getEndpoint() == 'callout:FS_CHL_Fullstack/customerinfo/v1/customers/20552003609/creditInfo?ad_accreditationType=Otras&ad_companySize=Test')
            //if(request.getEndpoint() == 'https://api-test.movistar.cl/customerinfo/v1/customers/20552003609/creditInfo?ad_accreditationType=Otras&ad_companySize=Test'){
            if(request.getEndpoint().contains('FS_CHI_DataPower/customerinfo/v1/customers/20552003609')){
                //response.setBody('{"creditProfiles":[{"creditProfileDate":"15/12/1990","creditScore":"0"}],"creditLimitInfo":{"creditLimit":"1"},"additionalData":[{"key":"portabilityLimit","value":"2"},{"key":"creditClass","value":"A"},{"key":"consumeLimit","value":"3"}]}');
                response.setBody('{"creditnfo":{"creditProfiles":[{"creditProfileDate":"2017-11-22 00:00:00","creditScore":"0"}],"creditLimitInfo":{"creditLimit":"1"},"additionalData":[{"key":"portabilityLimit","value":"2"},{"key":"creditClass","value":"A"},{"key":"consumeLimit","value":"3"}]}}');
                response.setStatusCode(200);
            }else if(request.getEndpoint().contains('FS_CHI_DataPower/customerinfo/v1/customers/120047787')){
                //response.setBody('{"creditProfiles":[{"creditProfileDate":"15/12/1990","creditScore":"0"}],"creditLimitInfo":{"creditLimit":"1"},"additionalData":[{"key":"portabilityLimit","value":"2"},{"key":"creditClass","value":"A"},{"key":"consumeLimit","value":"3"}]}');
                response.setBody('{"creditInfo":{"creditProfiles":[{"creditProfileDate":"2017-11-22 00:00:00","creditScore":"0"}],"creditLimitInfo":{"creditLimit":"1"},"additionalData":[{"key":"portabilityLimit","value":"2"},{"key":"creditClass","value":"A"},{"key":"consumeLimit","value":"3"}]}}');
                response.setStatusCode(200);
            }else response.setStatusCode(400);
            
        } catch(Exception exc){response.setStatusCode(500);}
        
        return response;
    }
}