@isTest
private class BI_PortalUsersController_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   This class manage coverage for PortalUsersController class.

    History: 
    
     <Date>                     <Author>                <Change Description>
    11/11/2014                  Micah Burgos             Initial Version
    03/02/2017					Pedro Párraga			 Increase coverage
    20/09/2017                 	Jaime Regidor		 	 Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void portalUsersController() {
      
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		Id idProfile =BI_DataLoad.searchAdminProfile();
	        
	    List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
	    
	    system.runAs(lst_user[0]){
	    	
	       	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	              
	       	List<Account> lst_acc = new List<Account>();
       		Account acc = new Account(Name = 'testAcc',
            						  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Country__c = lst_pais[0],
                                      BI_Segment__c = 'test', //28/09/2017
                             		  BI_Subsegment_Regional__c = 'test', //28/09/2017
                             		  BI_Territory__c = 'test' //28/09/2017
                                      );
            
            lst_acc.add(acc);

	       	insert lst_acc;
		
			List<Contact> lst_contacts = BI_DataLoad.loadContacts(2, lst_acc);

            BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(
            						  BI_Activo__c = true,
            						  BI_Contacto__c = lst_contacts[0].Id,
                                      BI_Cliente__c = lst_contacts[0].AccountId,
                                      BI_User__c = UserInfo.getUserId(),
                                      BI_Perfil__c = Label.BI_Apoderado,
                                      BI_Eliminar__c = true);
	            
	
			insert ccp;
			
			Test.startTest();

			ApexPages.StandardController sc = new ApexPages.StandardController(lst_user[0]);
			PortalUsersController crt = new PortalUsersController(sc);

			//Try test
			crt.addItem();

			crt.listCCP[crt.listCCP.size() - 1 ].BI_Activo__c = true;
			crt.listCCP[crt.listCCP.size() - 1 ].BI_Contacto__c = lst_contacts[1].Id;
			
			crt.listCCP[crt.listCCP.size() - 1 ].BI_User__c = UserInfo.getUserId();
			crt.listCCP[crt.listCCP.size() - 1 ].BI_Perfil__c = Label.BI_Apoderado;

			crt.refreshAcc();
			system.assertEquals(crt.listCCP[crt.listCCP.size() - 1 ].BI_Cliente__c, lst_contacts[1].AccountId);
			crt.save();

			System.debug('Catch test');
			//Catch test
			crt.addItem();
			crt.save();

			Test.stopTest();
			 
    	}


	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}