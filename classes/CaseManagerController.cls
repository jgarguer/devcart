public with sharing class CaseManagerController
{
    /* Main Page */
    public list<CaseItem> listOfCases {get;set;}                // List of Cases of current page
    public ApexPages.StandardSetController listCaseSSC;         // List of all Cases saved into StandardSetController
    public set<String> listOfSelectedCaseId {get;set;}          // List of Id of all the Cases selected
    list<String> listOpenedCaseStatus;                          // Only Cases with an opened status are managed
    set<String> listOfServices;                                 // List of Service Name of all the Cases selected
    String caseQuery;                                           // Query string used to retrieve Cases
    public Boolean inConfigurator {get;set;}                    // Boolean that defines if the change status section is opened
    public Integer pageSize = 50;
    public Integer nmaxpage {get;set;}
    public Integer npage {get;set;}
    
    /* Search Section */
    public Case filterObject {get;set;}                         // Object used to get different objects lookup field for filtered search
    public String filterStatus {get;set;}                       // Status value for filtered search
    public String filterReason {get;set;}                       // Status Reason value for filtered search
    public String filterRequestId {get;set;}                    // Request Id value for filtered search
    public String filterCaseNumber {get;set;}                   // Case Number value for filtered search
    
    /* Update Status Section */
    public list<SelectOption> selectedStatusOptions {get;set;}          // Picklist with all the selected Status
    public list<SelectOption> selectedStatusReasonOptions {get;set;}    // Picklist with all the selected Status Reason
    public list<SelectOption> allStatusOptions {get;set;}               // Picklist with all the Status available to choose for update
    public list<SelectOption> allStatusReasonOptions {get;set;}         // Picklist with all the Status Reason available to choose for update
    public list<UpdateStatus> listStatusToChange {get;set;}             // List of all Prior/New Status and Status Reason selected for update
    
    /* Update Request */
    public Boolean updateProcess {get;set;}                     // Boolean that defines if the update request is sent
    public Task taskRequest {get;set;}                          // Task generated from an update request
    public Attachment completedUpdAttachment {get;set;}         // Csv attachment with the list of all completed updates
    public Attachment failedUpdAttachment {get;set;}            // Csv attachment with the list of all failed updates
    public Integer numOfCaseToUpd {get;set;}                    // Number of Cases to update
    public Integer numOfCompletedUpdate {get;set;}              // Number of completed updates
    public Integer numOfFailedUpdate {get;set;}                 // Number of failed updates
    
    
    public CaseManagerController()
    {
        listOfCases = new list<CaseItem>();
        listOfSelectedCaseId = new set<String>();
        filterObject = new Case();
        filterStatus = '';
        filterReason = '';
        filterRequestId = '';
        filterCaseNumber = '';
        inConfigurator = false;
        listStatusToChange = new list<UpdateStatus>();
        updateProcess = false;
        
        try
        {
            Id caseOrderManagementRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId();
            
            // Retrieve only Cases with an opened status
            listOpenedCaseStatus = new list<String>();
            for(CaseStatus caseStatus : [SELECT Id, MasterLabel FROM CaseStatus WHERE IsClosed = false OR MasterLabel = 'Resolved'])
                listOpenedCaseStatus.add(caseStatus.MasterLabel);
            
            caseQuery = 'SELECT Id, Account.Name, CaseNumber, Status, Order__c, Order__r.Bit2WinHUB__BulkConfigurationRequest__r.Bit2WinHUB__Request_Id__c, TGS_Service__c, TGS_Status_reason__c ';
            //caseQuery += 'FROM Case WHERE RecordTypeId = \''+caseOrderManagementRecordTypeId+'\' AND Status IN: listOpenedCaseStatus ';
            caseQuery += 'FROM Case WHERE RecordTypeId = \''+caseOrderManagementRecordTypeId+'\' AND Status IN: listOpenedCaseStatus AND Order__r.Bit2WinHUB__BulkConfigurationRequest__r.Bit2WinHUB__Request_Id__c != null ';
            
            listCaseSSC = new ApexPages.StandardSetController(Database.getQueryLocator(caseQuery + 'ORDER BY CaseNumber ASC'));
            listCaseSSC.setPageSize(pageSize);
            listCaseSSC.last();
            nmaxpage = listCaseSSC.getPageNumber();
            listCaseSSC.first();
            npage = listCaseSSC.getPageNumber();
            
            list<Case> cases = (list<Case>) listCaseSSC.getRecords();
            for(Case c : cases)
            {
                CaseItem caseIt = new CaseItem(c);
                listOfCases.add(caseIt);
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
            system.debug('[CaseManagerController] Exception found: '+e+' at line: '+e.getLineNumber());
        }
    }
    
    
    /* Returns true if there are pages next to the current one */
    public Boolean thereIsNextPage
    {
        get {
            if(listCaseSSC != null)             
                return listCaseSSC.getHasNext();
            else
                return false;
        }
        set;
    }
   
   
    /* Returns true if there are pages previous to the current one */
    public Boolean thereIsPreviousPage
    {
        get { 
          if(listCaseSSC != null)
              return listCaseSSC.getHasPrevious();  
            else
              return false;
        }  
        set;  
    }
    
    
    /* Update results with items of the next page */
    public void goToNextPage()
    {
        try
        {
            for(CaseItem c : listOfCases)
            {
                if(c.selected == true)
                    listOfSelectedCaseId.add(c.caseToUpd.Id);
                else
                    listOfSelectedCaseId.remove(c.caseToUpd.Id);
            }
            
            listCaseSSC.next();
            npage = listCaseSSC.getPageNumber();
            list<Case> cases = (list<Case>) listCaseSSC.getRecords();
            
            listOfCases = new list<CaseItem>();
            for(Case c : cases)
            {
                CaseItem caseIt = new CaseItem(c);
                if(listOfSelectedCaseId.contains(c.Id))
                    caseIt.selected = true;
                listOfCases.add(caseIt);
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
            system.debug('[goToNextPage] Exception found: '+e+' at line: '+e.getLineNumber());
        }
    }
    
    
    /* Update results with items of the previous page */
    public void goToPreviousPage()
    {
        try
        {
            for(CaseItem c : listOfCases)
            {
                if(c.selected == true)
                    listOfSelectedCaseId.add(c.caseToUpd.Id);
                else
                    listOfSelectedCaseId.remove(c.caseToUpd.Id);
            }
            
            listCaseSSC.previous();
            npage = listCaseSSC.getPageNumber();
            list<Case> cases = (list<Case>) listCaseSSC.getRecords();
            
            listOfCases = new list<CaseItem>();
            for(Case c : cases)
            {
                CaseItem caseIt = new CaseItem(c);
                if(listOfSelectedCaseId.contains(c.Id))
                    caseIt.selected = true;
                listOfCases.add(caseIt);
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
            system.debug('[goToPreviousPage] Exception found: '+e+' at line: '+e.getLineNumber());
        }
    }
    
    
    /* Return all the cases with a filter */
    public void addFilter()
    {
        try
        {
            for(CaseItem c : listOfCases)
            {
                if(c.selected == true)
                    listOfSelectedCaseId.add(c.caseToUpd.Id);
                else
                    listOfSelectedCaseId.remove(c.caseToUpd.Id);
            }
            
            String filterAccount = filterObject.AccountId;
            String filterOrder = filterObject.Order__c;
            String queryToDo = caseQuery;
            
            if (filterRequestId != null && filterRequestId != ''){
                // Filter on Account field
                if(filterAccount != null)
                    queryToDo += 'AND AccountId =: filterAccount ';
                
                // Filter on Order field
                if(filterOrder != null)
                    queryToDo += 'AND Order__c =: filterOrder ';
                
                // Filter on Status field
                if(filterStatus != null && filterStatus != '')
                    queryToDo += 'AND Status LIKE \'%' + filterStatus + '%\' ';
                
                // Filter on Status Reason field
                if(filterReason != null && filterReason != '')
                    queryToDo += 'AND TGS_Status_reason__c LIKE \'%' + filterReason + '%\' ';
                
                // Filter on Request Id field
                if(filterRequestId != null && filterRequestId != '')
                    queryToDo += 'AND Order__r.Bit2WinHUB__BulkConfigurationRequest__r.Bit2WinHUB__Request_Id__c LIKE \'%' + filterRequestId + '%\' ';
                
                // Filter on Case Number field
                if(filterCaseNumber != null && filterCaseNumber != ''){
                    //queryToDo += 'AND CaseNumber LIKE \'%' + filterCaseNumber + '%\' ';
                    List<String> filterCaseList = filterCaseNumber.split(',');                   
                    queryToDo += 'AND CaseNumber IN (\'' + String.join(filterCaseList, '\',\'') + '\') ';
                }
            }
            
            
            // Retrieve Case records with or without filters
            queryToDo += 'ORDER BY CaseNumber ASC';                             
            listCaseSSC = new ApexPages.StandardSetController(Database.getQueryLocator(queryToDo));
            
            listCaseSSC.setPageSize(pageSize);
            listCaseSSC.last();
            nmaxpage = listCaseSSC.getPageNumber();
            listCaseSSC.first();
            npage = listCaseSSC.getPageNumber();
            
            list<Case> cases = (list<Case>) listCaseSSC.getRecords();
            listOfCases = new list<CaseItem>();
            for(Case c : cases)
            {
                CaseItem caseIt = new CaseItem(c);
                if(listOfSelectedCaseId.contains(c.Id))
                    caseIt.selected = true;
                listOfCases.add(caseIt);
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
            system.debug('[addFilter] Exception found: '+e+' at line: '+e.getLineNumber());
        }
    }
    
    
    /* Set inConfigurator to true to open the change status section */
    public void goToChangeStatusSection()
    {
        if(!inConfigurator)
        {
            if(!Test.isRunningTest())
            {
                for(CaseItem c : listOfCases)
                {
                    if(c.selected == true)
                        listOfSelectedCaseId.add(c.caseToUpd.Id);
                    else
                        listOfSelectedCaseId.remove(c.caseToUpd.Id);
                }
            }
            else
            {
                list<Case> cases = (list<Case>) listCaseSSC.getRecords();
                for(Case c : cases)
                    listOfSelectedCaseId.add(c.Id); 
            }
            
            if(listOfSelectedCaseId.size() == 0)
            {
                // Display an error message
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.NE_Select_at_least_one_case));
            }
            else
            {
                inConfigurator = true;
                buildSelectOptions();
                addStatusToChange();
            }
                
            system.debug('[goToChangeStatusSection] N. of selected cases: '+listOfSelectedCaseId.size());
            
            if(listOfSelectedCaseId.size() > 80)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.NE_Number_selected_cases+': '+listOfSelectedCaseId.size()+'. '+Label.NE_Warning_number_cases));
        }
        else
        {
            // Return to main page and clear all select options
            inConfigurator = false;
            listStatusToChange = new list<UpdateStatus>();
            selectedStatusOptions = new list<SelectOption>();
            selectedStatusReasonOptions = new list<SelectOption>();
            allStatusOptions = new list<SelectOption>();
            allStatusReasonOptions = new list<SelectOption>();
        }
    }
    
    
    /* Create picklists with the list of status and status reasons */
    public void buildSelectOptions()
    {
        try
        {
            list<Case> selectedCases = [SELECT Id, Status, TGS_Service__c, TGS_Status_reason__c FROM Case WHERE Id IN: listOfSelectedCaseId];
            listOfServices = new set<String>();
            
            // Status and Status Reason of selected cases
            selectedStatusOptions = new list<SelectOption>();
            selectedStatusReasonOptions = new list<SelectOption>();
            
            set<SelectOption> setStatusOptions = new set<SelectOption>();
            set<SelectOption> setStatusReasonOptions = new set<SelectOption>();
            setStatusOptions.add(new SelectOption('', ''));
            setStatusReasonOptions.add(new SelectOption('', ''));
            
            for(Case item : selectedCases)
            {
                setStatusOptions.add(new SelectOption(item.Status, item.Status));
                setStatusReasonOptions.add(new SelectOption(ifNull(item.TGS_Status_reason__c), ifNull(item.TGS_Status_reason__c)));
                listOfServices.add(item.TGS_Service__c);
            }
            
            selectedStatusOptions.addAll(setStatusOptions);
            selectedStatusOptions.sort();           
            selectedStatusReasonOptions.addAll(setStatusReasonOptions);
            selectedStatusReasonOptions.sort();
            
            // All Status and Status Reason
            allStatusOptions = new list<SelectOption>();
            allStatusReasonOptions = new list<SelectOption>();
        
            setStatusOptions = new set<SelectOption>();
            setStatusReasonOptions = new set<SelectOption>();
            setStatusOptions.add(new SelectOption('', ''));
            setStatusReasonOptions.add(new SelectOption('', ''));
            
            for(TGS_Status_machine__c statusMachine : [SELECT TGS_Status__c, TGS_Status_reason__c FROM TGS_Status_machine__c WHERE TGS_Service__r.Name IN: listOfServices])
            {
                setStatusOptions.add(new SelectOption(statusMachine.TGS_Status__c, statusMachine.TGS_Status__c));
                setStatusReasonOptions.add(new SelectOption(ifNull(statusMachine.TGS_Status_reason__c), ifNull(statusMachine.TGS_Status_reason__c)));
            }
            
            allStatusOptions.addAll(setStatusOptions);
            allStatusOptions.sort();
            allStatusReasonOptions.addAll(setStatusReasonOptions);
            allStatusReasonOptions.sort();
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
            system.debug('[buildSelectOptions] Exception found: '+e+' at line: '+e.getLineNumber());
        }
    }
    
    
    /* Add new row of status to change */
    public void addStatusToChange()
    {
        UpdateStatus updStatus = new UpdateStatus();
        listStatusToChange.add(updStatus);
    }
    
    
    /* Update Cases status */
    public void updateCases()
    {
        try
        {   
            // mapStatusToChange - key: prior status and status reason, value: new status and status reason
            map<String, String> mapStatusToChange = new map<String, String>();
            
            for(UpdateStatus updStatus : listStatusToChange)
            {
                if(updStatus.priorStatus != '' && updStatus.priorStatus != null && updStatus.newStatus != '' && updStatus.newStatus != null)
                {
                    String key = updStatus.priorStatus + ':' + ifNull(updStatus.priorStatusReason);
                    String value = updStatus.newStatus + ':' + ifNull(updStatus.newStatusReason);
                    mapStatusToChange.put(key, value);
                }
            }
                        
            if(mapStatusToChange.size() > 0)
            {
                updateProcess = true;
                
                // statusMachinesOfSelectedService - key: service name + prior status + prior status reason + profile; value: list of available new status and status reason
                map<String, set<String>> statusMachinesOfSelectedService = new map<String, set<String>>();
                
                for(TGS_Status_machine__c statusMachine : [SELECT TGS_Prior_Status__c, TGS_Prior_Status_reason__c, TGS_Profile__c, TGS_Service__r.Name, TGS_Status__c, TGS_Status_reason__c FROM TGS_Status_machine__c WHERE TGS_Service__r.Name IN: listOfServices])
                {
                    String key = statusMachine.TGS_Service__r.Name + ':' + statusMachine.TGS_Prior_Status__c + ':' + statusMachine.TGS_Prior_Status_reason__c + ':' + ifNull(statusMachine.TGS_Profile__c);
                    set<String> values = new set<String>();
                    if(statusMachinesOfSelectedService.get(key) != null)
                        values = statusMachinesOfSelectedService.get(key);
                    String value = statusMachine.TGS_Status__c + ':' + ifNull(statusMachine.TGS_Status_reason__c);
                    values.add(value);
                    statusMachinesOfSelectedService.put(key, values);
                }
                
                
                String csvCompletedUpdate = 'CASE ID;CASE NUMBER;SERVICE;PRIOR STATUS;PRIOR STATUS REASON;NEW STATUS;NEW STATUS REASON;PROFILE\n';
                String csvFailedUpdate = csvCompletedUpdate.substring(0, csvCompletedUpdate.length()-1) + ';ERROR\n';
                
                numOfCaseToUpd = listOfSelectedCaseId.size();
                numOfCompletedUpdate = 0;
                numOfFailedUpdate = 0;
                
                list<String> rowsCompleted = new list<String>();
                list<Case> listCaseToUpdate = new list<Case>();
                
                
                for(Case caseToUpd : [SELECT Id, CaseNumber, CreatedBy.Profile.Name, Status, TGS_Service__c, TGS_Status_reason__c FROM Case WHERE Id IN: listOfSelectedCaseId])
                {
                    String thisCaseStatus = caseToUpd.Status + ':' + ifNull(caseToUpd.TGS_Status_reason__c);
                    String newCaseStatus = mapStatusToChange.get(thisCaseStatus);
                    
                    if(newCaseStatus != null)
                    {
                        try
                        {
                            String statusMachineKey = caseToUpd.TGS_Service__c+':'+caseToUpd.Status+':'+caseToUpd.TGS_Status_reason__c+':'+caseToUpd.CreatedBy.Profile.Name;
                            set<String> listStatusMachine = statusMachinesOfSelectedService.get(statusMachineKey);
                            
                            // Three cases: 
                            // 1 - There are status machines for a specific profile
                            // 2 - There are status machines without profile
                            // 3 - There are not status machines available
                            
                            // Case 1: there are status machines for a specific profile.
                            if(listStatusMachine != null && listStatusMachine.contains(newCaseStatus))
                            {
                                // New Case Status permitted
                                list<String> newStatus = newCaseStatus.split(':');
                                if(newStatus.size() == 1)
                                    newStatus.add('');
                                
                                String csvRow = caseToUpd.Id+';'+caseToUpd.CaseNumber+';'+caseToUpd.TGS_Service__c+';';
                                csvRow += caseToUpd.Status+';'+ifNull(caseToUpd.TGS_Status_reason__c)+';';
                                csvRow += newStatus[0]+';'+ifNull(newStatus[1])+';'+caseToUpd.CreatedBy.Profile.Name+'\n';
                                
                                rowsCompleted.add(csvRow);
                                
                                caseToUpd.Status = newStatus[0];
                                caseToUpd.TGS_Status_reason__c = newStatus[1];
                                listCaseToUpdate.add(caseToUpd);
                                
                                numOfCompletedUpdate++;
                            }
                            else
                            {
                                statusMachineKey = caseToUpd.TGS_Service__c+':'+caseToUpd.Status+':'+caseToUpd.TGS_Status_reason__c+':';
                                listStatusMachine = statusMachinesOfSelectedService.get(statusMachineKey);
                                
                                // Case 2: there are status machines without profile
                                if(listStatusMachine != null && listStatusMachine.contains(newCaseStatus))
                                {
                                    // New Case Status permitted
                                    list<String> newStatus = newCaseStatus.split(':');
                                    if(newStatus.size() == 1)
                                        newStatus.add('');
                                    
                                    String csvRow = caseToUpd.Id+';'+caseToUpd.CaseNumber+';'+caseToUpd.TGS_Service__c+';';
                                    csvRow += caseToUpd.Status+';'+ifNull(caseToUpd.TGS_Status_reason__c)+';';
                                    csvRow += newStatus[0]+';'+ifNull(newStatus[1])+';'+caseToUpd.CreatedBy.Profile.Name+'\n';
                                    
                                    rowsCompleted.add(csvRow);
                                                                        
                                    caseToUpd.Status = newStatus[0];
                                    caseToUpd.TGS_Status_reason__c = newStatus[1];
                                    listCaseToUpdate.add(caseToUpd);
                                    
                                    numOfCompletedUpdate++;
                                }
                                // Case 3: there are not status machines available
                                else
                                {
                                    // New Case Status not permitted
                                    list<String> newStatus = newCaseStatus.split(':');
                                    if(newStatus.size() == 1)
                                        newStatus.add('');
                                    
                                    csvFailedUpdate += caseToUpd.Id+';'+caseToUpd.CaseNumber+';'+caseToUpd.TGS_Service__c+';';
                                    csvFailedUpdate += caseToUpd.Status+';'+ifNull(caseToUpd.TGS_Status_reason__c)+';';
                                    csvFailedUpdate += newStatus[0]+';'+ifNull(newStatus[1])+';';
                                    csvFailedUpdate += caseToUpd.CreatedBy.Profile.Name+';'+Label.NE_Case_update_error+'\n';
                                    
                                    numOfFailedUpdate++;
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            system.debug('[UpdateCases] Exception found: '+e+' at line: '+e.getLineNumber());
                            
                            list<String> newStatus = newCaseStatus.split(':');
                            if(newStatus.size() == 1)
                                newStatus.add('');
                            
                            csvFailedUpdate += caseToUpd.Id+';'+caseToUpd.CaseNumber+';'+caseToUpd.TGS_Service__c+';';
                            csvFailedUpdate += caseToUpd.Status+';'+ifNull(caseToUpd.TGS_Status_reason__c)+';';
                            csvFailedUpdate += newStatus[0]+';'+ifNull(newStatus[1])+';';
                            csvFailedUpdate += caseToUpd.CreatedBy.Profile.Name+';'+e+'\n';
                            
                            numOfFailedUpdate++;
                        }
                    }
                    else
                    {
                        system.debug('[UpdateCases] Case '+caseToUpd.Id+' not changed.');
                        numOfCaseToUpd--;
                    }
                }
                
                
                Database.SaveResult[] SR = Database.update(listCaseToUpdate, false);
                
                for(Integer i=0 ; i<SR.size(); i++)
                {
                    if(!SR[i].isSuccess())
                    {
                        String csvRow = rowsCompleted[i].substring(0, rowsCompleted[i].length()-1) + ';';
                        for(Database.Error err : SR[i].getErrors())
                            csvRow += err.getMessage() + ' - ';
                        csvRow = csvRow.subString(0, csvRow.length()-3) + '\n';
                        csvFailedUpdate += csvRow;
                        
                        numOfCompletedUpdate--;
                        numOfFailedUpdate++;
                    }
                    else
                        csvCompletedUpdate += rowsCompleted[i];
                }
                
                
                String requestId = generateIdentifier('req');
                taskRequest = new Task();
                taskRequest.Subject = 'Update Request: '+requestId;
                taskRequest.Status = 'Completed';
                insert taskRequest;
                
                if(numOfCompletedUpdate > 0)
                {
                    completedUpdAttachment = new Attachment();
                    completedUpdAttachment.Body = Blob.valueOf(csvCompletedUpdate);
                    completedUpdAttachment.Name = 'Completed Updates '+requestId+'.csv';
                    completedUpdAttachment.Description = 'Completed updates details';
                    completedUpdAttachment.ParentId = taskRequest.Id;
                    insert completedUpdAttachment;
                }
                
                if(numOfFailedUpdate > 0)
                {
                    failedUpdAttachment = new Attachment();
                    failedUpdAttachment.Body = Blob.valueOf(csvFailedUpdate);
                    failedUpdAttachment.Name = 'Failed Updates '+requestId+'.csv';
                    failedUpdAttachment.Description = 'Failed updates details';
                    failedUpdAttachment.ParentId = taskRequest.Id;
                    insert failedUpdAttachment;
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.NE_Select_prior_and_new_status));
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
            system.debug('[UpdateCases] Exception found: '+e+' at line: '+e.getLineNumber());
        }
    }
    
    
    /* Return to Case Manager main page */
    public PageReference goBack()
    {
        PageReference pageBack = ApexPages.currentPage();
        pageBack.setRedirect(true);
        return pageBack;
    }
    
    
    
    /* Generate random codes methods */
    public static String generateIdentifier(String prefix)
    {
        return prefix + '-' + generateRandomNumber();
    }
    
    public static String generateRandomNumber() {
        String randomNumber = generate();

        if (randomNumber.length() < 10) {
            String randomNumber2 = generate();
            randomNumber = randomNumber + randomNumber2.substring(0, 10 - randomNumber.length());
        }
        
        return randomNumber;
    }
    
    public static String generate()
    {
        return String.valueOf(Math.abs(Crypto.getRandomInteger()));
    }
    
    public String ifNull(String text)
    {
        if(text == null)
            return '';
        else
            return text;
    }
    
    
    
    //-------------- WRAPPER CLASSES --------------
    
    public class CaseItem
    {
        public Case caseToUpd {get;set;}
        public Boolean selected {get;set;}
        
        public CaseItem(Case caseToUpd)
        {
            this.caseToUpd = caseToUpd;
            this.selected = false;
        }
    }
    
    
    public class UpdateStatus
    {
        public String priorStatus {get;set;}
        public String priorStatusReason {get;set;}
        public String newStatus {get;set;}
        public String newStatusReason {get;set;}
        
        public UpdateStatus() {}
    }
    
}