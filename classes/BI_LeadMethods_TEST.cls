@isTest
private class BI_LeadMethods_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_LeadMethods class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    09/09/2014              Micah Burgos            Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LeadMethods.validateId.
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    09/09/2014              Micah Burgos            Initial version
    19/03/2015              Juan Santisi            Refactored BI_Paises_Ref__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    static testMethod void validateId_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_DataLoad.set_ByPass(false, false);
        /////////////////////////////////////
                    Integer K = 10;
        ////////////////////////////////////            

        List<BI_Dependencia_Territorio__c> lst_deps = new List<BI_Dependencia_Territorio__c>();
        for(Integer i = 0; i < K; i++) {
            BI_Dependencia_Territorio__c dep = new BI_Dependencia_Territorio__c(Name='Dep'+i, BI_Segmento_regional__c='Empresas', BI_Pais__c= Label.BI_peru ,BI_Territorio__c='Moquegua');
            lst_deps.add(dep);
        }
        insert lst_deps;

    /*  List <BI_Pickup_Option__c> lst_op = new List <BI_Pickup_Option__c>();
            BI_Pickup_Option__c op1 = new BI_Pickup_Option__c (Name = 'RFC',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'México');
            lst_op.add(op1);
            
            BI_Pickup_Option__c op2 = new BI_Pickup_Option__c (Name = 'CUIT',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Argentina');
            lst_op.add(op2); 
            BI_Pickup_Option__c op3 = new BI_Pickup_Option__c (Name = 'NIT',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Colombia');
            lst_op.add(op3);                                                   
            BI_Pickup_Option__c op4 = new BI_Pickup_Option__c (Name = 'RUT',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Chile');
            lst_op.add(op4);                                                   
            BI_Pickup_Option__c op5 = new BI_Pickup_Option__c (Name = 'NITE',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Costa Rica');
            lst_op.add(op5);                                                   
            BI_Pickup_Option__c op6 = new BI_Pickup_Option__c (Name = 'RIF',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Venezuela');
            lst_op.add(op6);                                                   
            BI_Pickup_Option__c op7 = new BI_Pickup_Option__c (Name = 'RTN',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c ='Honduras');
            lst_op.add(op7);                                                   
            BI_Pickup_Option__c op8 = new BI_Pickup_Option__c (Name = 'CPF',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Brasil');
            lst_op.add(op8);                                                   
            BI_Pickup_Option__c op9 = new BI_Pickup_Option__c (Name = 'RUC',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Ecuador');
            lst_op.add(op9);                                                   
         //[ECUADOR]
        BI_Pickup_Option__c op10 = new BI_Pickup_Option__c (Name = 'PASAPORTE',
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        BI_Pickup_Option__c op11 = new BI_Pickup_Option__c (Name = 'RUC ESPECIAL',
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        BI_Pickup_Option__c op12 = new BI_Pickup_Option__c (Name = Label.BI_Cedula,
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        BI_Pickup_Option__c op13 = new BI_Pickup_Option__c (Name = 'RUC',
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        //[MEXICO]
        BI_Pickup_Option__c op14 = new BI_Pickup_Option__c (Name = Label.BI_RFC_Moral,
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Mexico);
                                                   
        BI_Pickup_Option__c op15 = new BI_Pickup_Option__c (Name = Label.BI_RFC_Fisica,
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Mexico);
     
        lst_op.add(op10); //PASAPORTE
        lst_op.add(op11); //RUC ESPECIAL
        lst_op.add(op12); //CEDULA
        lst_op.add(op13); //RUC ECUADOR
                
        insert lst_op;
        */
        list<Lead> lst_lead = new list<Lead>();
        
        for (Integer i = 0; i< K ; i++){
            
            //'CUIT' -> 11 Numbers
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'CUIT', 
                                          BI_Numero_identificador_fiscal__c = '23218867669',
                                          BI_Country__c = 'Argentina');
                
                lst_lead.add(led);
                i++;
        }
        
        for (Integer i = 0; i< K ; i++){
            
            //'RIF' -> // 1 letter - 9 numbers A123456789
             Lead led = new Lead(LastName = 'testRIF'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RIF', 
                                          BI_Numero_identificador_fiscal__c = ('A12345678' + String.valueOf(i)),
                                          BI_Country__c = 'Venezuela'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        for (Integer i = 0; i< K ; i++){
            
            //'NIT'// 9 numbers 123456789
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'NIT', 
                                          BI_Numero_identificador_fiscal__c = ('12345678' + String.valueOf(i)),
                                          BI_Country__c = 'Colombia'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        for (Integer i = 0; i< K ; i++){
            
            //'NITE') { //10 or 12 numbers 1234567891 OR 123456789101
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'NITE', 
                                          BI_Numero_identificador_fiscal__c = ('12345678900' + String.valueOf(i)),
                                          BI_Country__c = 'Costa Rica' 

                                          );
                
                lst_lead.add(led);
                i++;
        }
        
       /* for (Integer i = 0; i< K ; i++){
            
            //'RUT') { // 8 numbers - 1 letter 12345678A
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RUT', 
                                          BI_Numero_identificador_fiscal__c = ('1234567' + String.valueOf(i) + 'A'),
                                          BI_Country__c = 'Chile'
                                          );
                
                lst_lead.add(led);
                i++;
        }*/
        
        for (Integer i = 0; i< K ; i++){
            
            //'CPF') { //11 Numbers 12345678910
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'CPF', 
                                          BI_Numero_identificador_fiscal__c = ('1234567891' + String.valueOf(i)),
                                          BI_Country__c = 'Brasil'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        
        
            
            
        for (Integer i = 0; i< K ; i++){
            
            //'RUC') { //11 Numbers 12345678910
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RUC', 
                                          //BI_Numero_identificador_fiscal__c = ('1234567891' + String.valueOf(i)),
                                          BI_Numero_identificador_fiscal__c = ('123456789012' + String.valueOf(i)), /* Modified 26/05/2015 Raul Aguera*/
                                          BI_Country__c = 'Ecuador'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        
        for (Integer i = 0; i< K ; i++){
            
            //'RTN') { //14 Numbers 12345678910112
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RTN', 
                                          BI_Numero_identificador_fiscal__c = ('1234567891011' + String.valueOf(i)),
                                          BI_Country__c = 'Honduras'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        
        
        for (Integer i = 0; i< K ; i++){
            
            //'RFC') { //4 letters 6 numbers 3 characters VECB380326XXX
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RFC', 
                                          BI_Numero_identificador_fiscal__c = 'VECB380326XXX',
                                          BI_Country__c = Label.BI_Mexico
                                          );
                
                lst_lead.add(led);
                i++;
        }

        //[ECUADOR]
         Lead led_20 = new Lead(LastName = 'test_ECU_1',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 1',
                                      Status = 'Abierto',
                                      Email = 'testARG_1_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = 'PASAPORTE', //PASAPORTE
                                      BI_Numero_identificador_fiscal__c = '123456721891A',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
 
            
            lst_lead.add(led_20);
  
         Lead led_21 = new Lead(LastName = 'test_ECU_2',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 2',
                                      Status = 'Abierto',
                                      Email = 'testARG_2_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = 'RUC ESPECIAL', //RUC ESPECIAL
                                      BI_Numero_identificador_fiscal__c = '1',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
            
            lst_lead.add(led_21);
  
         Lead led_22 = new Lead(LastName = 'test_ECU_3',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 3',
                                      Status = 'Abierto',
                                      Email = 'testARG_3_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = Label.BI_Cedula, //CEDULA
                                      BI_Numero_identificador_fiscal__c = '1234567891',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
            
            lst_lead.add(led_22);
  
         Lead led_23 = new Lead(LastName = 'test_ECU_4',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 4',
                                      Status = 'Abierto',
                                      Email = 'testARG_4_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = 'RUC', //RUC
                                      BI_Numero_identificador_fiscal__c = '1234567890123',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
            
            lst_lead.add(led_23);

         //[MEXICO]
         Lead led_24 = new Lead(LastName = 'test_MEX_1',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 1',
                                      Status = 'Abierto',
                                      Email = 'testMEX_1_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = Label.BI_RFC_Moral, //MORAL
                                      BI_Numero_identificador_fiscal__c = 'ABC123456A1A',
                                      BI_Country__c = Label.BI_Mexico

                                      );
 
            
            lst_lead.add(led_24);
  
         Lead led_25 = new Lead(LastName = 'test_MEX_2',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 2',
                                      Status = 'Abierto',
                                      Email = 'testMEX_2_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = Label.BI_RFC_Fisica, //FISICA
                                      BI_Numero_identificador_fiscal__c = 'ABCD123456A1A',
                                      BI_Country__c = Label.BI_Mexico
                                      );
            
            lst_lead.add(led_25);

        
        Test.startTest();
        
        insert lst_lead;

     ///Only to search bugs 
    //for(Lead ld :lst_lead){
    //    system.debug('Lead TO insert: ' +ld);
    //    insert ld;
    //  }
        
        system.assertEquals(lst_lead.size(), [SELECT Id FROM Lead].size());
        
        Test.stopTest();
    }
    
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LeadMethods.validateId.
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    09/09/2014              Micah Burgos            Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void validateIdwrong_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
              
      /////////////////////////////////////
            Integer K = 1;
    ////////////////////////////////////        
        BI_DataLoad.set_ByPass(false, false);

   /*   List <BI_Pickup_Option__c> lst_op = new List <BI_Pickup_Option__c>();
        
        BI_Pickup_Option__c op1 = new BI_Pickup_Option__c (Name = 'RFC',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'México');
            lst_op.add(op1);
            
            BI_Pickup_Option__c op2 = new BI_Pickup_Option__c (Name = 'CUIT',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Argentina');
            lst_op.add(op2); 
            BI_Pickup_Option__c op3 = new BI_Pickup_Option__c (Name = 'NIT',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Colombia');
            lst_op.add(op3);                                                   
            BI_Pickup_Option__c op4 = new BI_Pickup_Option__c (Name = 'RUT',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Chile');
            lst_op.add(op4);                                                   
            BI_Pickup_Option__c op5 = new BI_Pickup_Option__c (Name = 'NITE',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Costa Rica');
            lst_op.add(op5);                                                   
            BI_Pickup_Option__c op6 = new BI_Pickup_Option__c (Name = 'RIF',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Venezuela');
            lst_op.add(op6);                                                   
            BI_Pickup_Option__c op7 = new BI_Pickup_Option__c (Name = 'RTN',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c ='Honduras');
            lst_op.add(op7);                                                   
            BI_Pickup_Option__c op8 = new BI_Pickup_Option__c (Name = 'CPF',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Brasil');
            lst_op.add(op8);                                                   
            BI_Pickup_Option__c op9 = new BI_Pickup_Option__c (Name = 'RUC',
                                                               BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                               BI_Country__c = 'Ecuador');
            lst_op.add(op9);                                                   

        //[ECUADOR]
        BI_Pickup_Option__c op10 = new BI_Pickup_Option__c (Name = 'PASAPORTE',
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        BI_Pickup_Option__c op11 = new BI_Pickup_Option__c (Name = 'RUC ESPECIAL',
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        BI_Pickup_Option__c op12 = new BI_Pickup_Option__c (Name = Label.BI_Cedula,
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador);
        BI_Pickup_Option__c op13 = new BI_Pickup_Option__c (Name = 'RUC',
                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal',
                                                   BI_Country__c = Label.BI_Ecuador); 
        lst_op.add(op10); //PASAPORTE
        lst_op.add(op11); //RUC ESPECIAL
        lst_op.add(op12); //CEDULA
        lst_op.add(op13); //RUC ECUADOR
                
        insert lst_op;*/
        
        list<Lead> lst_lead = new list<Lead>();
        
        for (Integer i = 0; i < K ; i++){
            
            //'CUIT' -> 11 Numbers
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'CUIT',  
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Argentina'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        for (Integer i = 0; i < K ; i++){
            
            //'RIF' -> // 1 letter - 9 numbers A123456789
             Lead led = new Lead(LastName = 'testRIF'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RIF', 
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Venezuela'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        for (Integer i = 0; i < K ; i++){
            
            //'NIT'// 9 numbers 123456789
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'NIT',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Colombia'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        for (Integer i = 0; i < K ; i++){
            
            //'NITE') { //10 or 12 numbers 1234567891 OR 123456789101
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'NITE',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Costa Rica'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        for (Integer i = 0; i < K ; i++){
            
            //'RUT') { // 8 numbers - 1 letter 12345678A
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RUT',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i) + 'A'),
                                          BI_Country__c = 'Chile'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        for (Integer i = 0; i < K ; i++){
            
            //'CPF') { //11 Numbers 12345678910
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'CPF',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Brasil'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        
        
            
            
        for (Integer i = 0; i < K ; i++){
            
            //'RUC') { //11 Numbers 12345678910
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RUC',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Ecuador'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        
        for (Integer i = 0; i < K ; i++){
            
            //'RTN') { //14 Numbers 12345678910112
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RTN',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i)),
                                          BI_Country__c = 'Honduras'
                                          );
                
                lst_lead.add(led);
                i++;
        }
        
        
        
        for (Integer i = 0; i < K ; i++){
            
            //'RFC') { //4 letters 6 numbers 3 characters VECB380326XXX
             Lead led = new Lead(LastName = 'test'+ String.valueOf(i),
                                          CurrencyIsoCode = 'USD',
                                          Company = 'Company Test'+ String.valueOf(i),
                                          Status = 'Abierto',
                                          Email = 'testARG'+ String.valueOf(i)+'@gmail.com',
                                          BI_Tipo_de_Identificador_Fiscal__c = 'RFC',
                                          BI_Numero_identificador_fiscal__c = ('BAD_ID' + String.valueOf(i) +'XXX'),
                                          BI_Country__c = Label.BI_Mexico
                                          );
                
                lst_lead.add(led);
                i++;
        }

      //[ECUADOR]
         Lead led_20 = new Lead(LastName = 'test_ECU_1',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 1',
                                      Status = 'Abierto',
                                      Email = 'testARG_1_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = 'PASAPORTE', //PASAPORTE
                                      BI_Numero_identificador_fiscal__c = 'BAD_ID',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
 
            
            lst_lead.add(led_20);
  
         Lead led_21 = new Lead(LastName = 'test_ECU_2',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 2',
                                      Status = 'Abierto',
                                      Email = 'testARG_2_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = 'RUC ESPECIAL', //RUC ESPECIAL
                                      BI_Numero_identificador_fiscal__c = 'BAD_ID',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
            
            lst_lead.add(led_21);
  
         Lead led_22 = new Lead(LastName = 'test_ECU_3',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 3',
                                      Status = 'Abierto',
                                      Email = 'testARG_3_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = Label.BI_Cedula, //CEDULA
                                      BI_Numero_identificador_fiscal__c = 'BAD_ID',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
            
            lst_lead.add(led_22);
  
         Lead led_23 = new Lead(LastName = 'test_ECU_4',
                                      CurrencyIsoCode = 'USD',
                                      Company = 'Company Test 4',
                                      Status = 'Abierto',
                                      Email = 'testARG_4_@gmail.com',
                                      BI_Tipo_de_Identificador_Fiscal__c = 'RUC', //RUC
                                      BI_Numero_identificador_fiscal__c = 'BAD_ID',
                                      BI_Country__c = Label.BI_Ecuador
                                      );
            
            lst_lead.add(led_23);
        
        try{
            insert lst_lead;
            system.assert([SELECT Id, Name FROM Lead].size() == 0);
        }catch(exception e){
            system.assert(true);
        }
    }
    
    static testMethod void validateId_Exception_Test()
    {
    	try
    	{
    		BI_LeadMethods.validateId(null, null);
    	}
    	catch (Exception e)
    	{
    		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
    	}
    	
    	Boolean b = BI_LeadMethods.CUITisValid('123456789xx');
    	System.assertEquals(b, false);
    }
    
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}