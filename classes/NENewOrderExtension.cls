public with sharing class NENewOrderExtension
{   
    public NE__Order__c ord {get;set;}
    ApexPages.standardController cntr;
    private Account oAccount = new Account();
    private Account billingAcc = new Account();
    private Account serviceAcc = new Account();
    private User authUser = new User();
    public Boolean bacc {get;set;}
    private String accId;
    public Id caseId;
    
    //START DELOITTE
    public String selectedUser {get; set;}
    public Boolean selectedUserError {get; set;}
    public List<SelectOption> usersList {get; set;}
    public Boolean isTGS {get; set;}
    private Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
    private List<SelectOption> costCenterOptions = new List<SelectOption>();
    private List<SelectOption> siteOptions = new List<SelectOption>();    
    private Id businessUserId;
    private Account businessUser;
    //END DELOITTE
    
    
    public NENewOrderExtension(ApexPages.standardController c)
    {               
        cntr = c;
        ord = (NE__Order__c)cntr.getRecord();      
        
        Id orderRTypeId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, Constants.RECORD_TYPE_ORDER);
        
        ord.RecordTypeId = orderRTypeId;
        
        accId = ApexPages.currentPage().getParameters().get('accId');
        caseId = ApexPages.currentPage().getParameters().get('caseId');
        
        if(accId != null)
        {
            bacc = true;
            Account acc = [SELECT Id, HoldingId__c, TGS_Aux_Holding__r.Name, Name, RecordTypeId FROM Account WHERE Id =: accId];
            ord.NE__AccountId__c = accId;
            ord.HoldingId__c = acc.HoldingId__c;
            if(acc.RecordTypeId == TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING)) {
                ord.TGS_Holding_Name__c = acc.Name;
            } else {
                ord.TGS_Holding_Name__c = acc.TGS_Aux_Holding__r.Name;
            }
        }
        else
            bacc=false;
        
        ord.NE__Configuration_Type__c = 'New';        
        ord.NE__OrderStatus__c = 'Pending';
        ord.NE__Type__c = 'InOrder';
        ord.NE__ConfigurationStatus__c = 'In progress';
        ord.NE__Version__c = 1;
        ord.NE__One_Time_Fee_Total__c = 0;
        ord.NE__Recurring_Charge_Total__c = 0;
        ord.Case__c = caseId;
       
        //START DELOITTE
        
        //Select isTGS
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
        isTGS = userTGS.TGS_Is_TGS__c;
        //isTGS = false; Deloitte (GAB) 15/05/2015 - Comment this line and uncomment previous one
        getAuthorizedUsersAndContract();
        //END DELOITTE
    }
    
    /**
    * Method:  getAuthorizedUsers
    * Author:  Jara Minguillon
    * Company: Deloitte
    */
    public void getAuthorizedUsersAndContract() 
    {
        System.debug('test');
        usersList = new List<SelectOption>();
        if (ord != null && ord.NE__AccountId__c != null) 
        {
            //DTT (GAB) 06/07/2015 - Only active users will be allowed
            //DTT (GAB) 08/07/2015 - Users ordered by name
            List<User> resultList = [SELECT Id, Name 
                                     FROM User
                                     WHERE Contact.AccountId = :ord.NE__AccountId__c
                                        AND IsActive = true
                                     ORDER BY Name];
            if (resultList.size() == 0) {
                usersList.add(new SelectOption('','-No users available-'));
            } else {
                usersList.add(new SelectOption('','-Select a user-'));
                for (User u : resultList) {
                    usersList.add(new SelectOption(u.Id, u.Name));
                }
            }
        } else {
            usersList.add(new SelectOption('','-No users available-'));
        }
        
        //Get Contract
        if (isTGS != null && isTGS) {
            List<NE__Contract_Account_Association__c> contractAccounts = [SELECT Id FROM NE__Contract_Account_Association__c WHERE NE__Account__c = :ord.NE__AccountId__c];
            if (contractAccounts.size() > 0) {
                ord.NE__Contract_Account__c = contractAccounts[0].Id;
            } else {
                ord.NE__Contract_Account__c = null;
            }
        }
    }
    
    /**
    * Method:  clearAuthorizedUsers
    * Author:  Jara Minguillon
    * Company: Deloitte
    */
    public PageReference clearAuthorizedUsers() {
        usersList = new List<SelectOption>();
        usersList.add(new SelectOption('','-No users available-'));
        System.debug('### Estoy en clearAuthorizedUsers');
        selectedUserError = true;
        
        return null;
    }
    
    public PageReference cancel()
    {
        // prefix
        String prefix = Site.getPathPrefix();    //DELOITTE: Replaced deprecated method
        if(prefix == null || prefix == '')
            prefix = '';
        
        if(bacc)
            return new PageReference(prefix+'/'+accId);
        else
            return cntr.cancel();
    }
    
    public PageReference next()
    {
        System.debug(ord);

        if (selectedUser == null || selectedUser == '') {
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Authorized User field is mandatory'));
            selectedUserError = true;
            return null;
        }
        // prefix
        String prefix = Site.getpathPrefix();    //DELOITTE: Replaced deprecated method
        if(prefix == null || prefix == '')
            prefix = '';
        
        list<NE__Contract_Account_Association__c> cHead = [SELECT id,NE__Contract_Header__c FROM NE__Contract_Account_Association__c WHERE id =: ord.NE__Contract_Account__c];
        
        PageReference cartPage = Page.NE__NewConfiguration;
        
        if (ord.Case__c ==null){

            if(cHead.size() > 0)
                cartPage.getParameters().put('contractHeaderId',cHead.get(0).NE__Contract_Header__c);   
            
            cartPage.getParameters().put('servAccId',ord.Business_Unit__c); 
            cartPage.getParameters().put('billAccId',ord.Cost_Center__c);   
            cartPage.getParameters().put('accId',ord.NE__AccountId__c); 
            cartPage.getParameters().put('billProfId',ord.NE__BillingProfId__c);    
            cartPage.getParameters().put('cType',ord.NE__Configuration_Type__c);
            cartPage.getParameters().put('site',ord.Site__c);
            cartPage.getParameters().put('holdingId',ord.HoldingId__c);
       
            // START DELOITTE COMMENT        cartPage.getParameters().put('authUser',ord.Authorized_User__c);   //END DELOITTE COMMENT
            //START DELOITTE
            cartPage.getParameters().put('authUser', selectedUser);
            //END DELOITTE

        } else{

            ord.Authorized_User__c = selectedUser; 
            ord.NE__Configuration_SubType__c = 'Completo';
            ord.NE__BillAccId__c=   ord.Cost_Center__c; // CC Acme SPN 1
            ord.NE__ServAccId__c =  ord.Business_Unit__c; // BU Acme SPN 1
            ord.NE__TotalRecurringFrequency__c = 'Monthly';
                
            //ord.NE__Parameters__c= 'ordId='+ord.id;

            insert ord;
            System.debug('NENEwOrderExtension.next :: ord ' +ord);
            cartPage.getParameters().put('ordId', ord.Id);

        }
        

        
        //ET v3.5 prefix removed for Page.
        return new PageReference(/*prefix+*/cartPage.getUrl());
    }
    
    public void setAcc()
    {
        accId=ord.NE__AccountId__c;
        oAccount = [select id, name,ShippingStreet, ShippingState, ShippingPostalCode, ShippingCountry, ShippingCity, NE__ShippingRegion__c,NE__Name__c,NE__Surname__c, NE__BillingAddressComplete__c, NE__ShippingAddressComplete__c,NE__Type__c,Industry,NE__Sub_sector__c, BillingStreet, NE__BillingRegion__c,BillingState, BillingPostalCode, BillingCountry, BillingCity  from Account where id =: accId];
        ord.NE__AccountId__c=accId;
        
        ord.NE__BillAccId__c = oAccount.Id;
        setPayerAcc();
        
        ord.NE__ServAccId__c = oAccount.Id;
        setUserAcc();
    }
    
    public void setUserAcc()
    {
        serviceAcc = [Select Id,ShippingStreet, ShippingState, ShippingPostalCode, ShippingCountry, ShippingCity, NE__ShippingRegion__c From Account Where Id=: ord.Business_Unit__c];
    }
    
    public void setPayerAcc()
    {
        billingAcc = [Select Id,NE__BillingRegion__c,BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity From Account Where Id=: ord.Cost_Center__c];
    }
    
    public void setAuthUser()
    {
        authUser = [SELECT Id FROM User WHERE Id =: ord.Authorized_User__c];
    }
    
    /**
    * Method:  getSites
    * Author:  Marta García 
    * Company: Deloitte
    */
    public void updateBusinessUserId() {
        // Dummy method
    }

    /**
    * Method:  getCostCenters
    * Author:  Marta García 
    * Company: Deloitte
    */
    //GMN 12/07/2017 - Changed cost centers picklist when a Ferrovial account is selected
    public List<SelectOption> getCostCenters() {
        
        Id buId = (Id) Apexpages.currentPage().getParameters().get('businessUnitId'); 
        
        if(buId != null){        
            costCenterOptions = new List<SelectOption>();
            
            Account bu = [SELECT TGS_Aux_Holding__c, TGS_Aux_Holding__r.Name FROM Account WHERE Id = :buId];
            List<Account> listCostCenter;

            if(bu.TGS_Aux_Holding__r.Name != Label.TGS_Ferrovial){

                listCostCenter= [SELECT Id, Name, Parent.Parent.Parent.BI_Country__c 
                                               FROM Account 
                                               WHERE TGS_Aux_Holding__c= :bu.TGS_Aux_Holding__c AND 
                                                     RecordTypeId= :rtId];
            }
            else{

                listCostCenter= [SELECT Id, Name, Parent.Parent.Parent.BI_Country__c 
                                               FROM Account 
                                               WHERE ParentId =: buId AND 
                                                     RecordTypeId= :rtId];
            }

            costCenterOptions.add(new SelectOption('', '-- Select a Cost Center --'));
            for (Account costCenter : listCostCenter){
                costCenterOptions.add(new SelectOption(costCenter.Id, costCenter.Parent.Parent.Parent.BI_Country__c +' - ' +costCenter.Name));
            }
        }
        
        return costCenterOptions;       
        
    }
    
    /**
     * Method:  getSites
     * Author:  Marta García 
     * Company: Deloitte
     */
    /*public List<SelectOption> getSites() {      
        Id buId = (Id) Apexpages.currentPage().getParameters().get('businessUnitId');
        
        if(buId != null){        
            siteOptions = new List<SelectOption>();
                        
            List<BI_Punto_de_instalacion__c > listSite = [SELECT Id, BI_Cliente__c, Name, TGS_ISO_Code__c, Pais__c
                                           FROM BI_Punto_de_instalacion__c
                                           WHERE BI_Cliente__c= :buId]; // Order by is ignored?
            
            List<Pto_Instalac> lstSort = new List<Pto_Instalac>();

            for (BI_Punto_de_instalacion__c site : listSite) {
                //siteOptions.add(new SelectOption(site.Id, site.Pais__c + ' - ' + site.TGS_ISO_Code__c + ' - ' +site.Name));
                lstSort.add(new Pto_Instalac(site));
            }
            lstSort.sort();
            for(Pto_Instalac pto : lstSort) {
                siteOptions.add(new SelectOption(pto.site.Id, pto.site.TGS_ISO_Code__c + ' -    ' + pto.site.Pais__c + '    - ' +pto.site.Name));
            }

            siteOptions.add(0, new SelectOption('', '-- Select a Site --'));
        }
        
        return siteOptions;
    }

    @testVisible
    private class Pto_Instalac implements Comparable {
        public BI_Punto_de_instalacion__c site;

        public Pto_Instalac(BI_Punto_de_instalacion__c me) {
            this.site = me;
        }
        public Integer compareTo(Object comp) {
            BI_Punto_de_instalacion__c a = this.site;
            BI_Punto_de_instalacion__c b = ((Pto_Instalac)comp).site;

            // Sort first by ISO Codes
            if(a.TGS_ISO_Code__c == null || b.TGS_ISO_Code__c == null)
                return a.TGS_ISO_Code__c != null ? -1 : b.TGS_ISO_Code__c != null ? 1 : 0; // nulls last
            if(a.TGS_ISO_Code__c != b.TGS_ISO_Code__c)
                return a.TGS_ISO_Code__c.compareTo(b.TGS_ISO_Code__c);

            // ISO Codes are equal, sort by Country
            if(a.Pais__c == null || b.Pais__c == null)
                return a.Pais__c != null ? -1 : b.Pais__c != null ? 1 : 0; // nulls last
            if(a.Pais__c != b.Pais__c)
                return a.Pais__c.compareTo(b.Pais__c);

            // Both ISO Codes _AND_ Countries are equal, sort by Site Name
            if(a.Name == null || b.Name == null)
                return a.Name != null ? -1 : b.Name != null ? 1 : 0; // nulls last
            if(a.Name != b.Name)
                return a.Name.compareTo(b.Name);

            return 0; // Everything is equal
        }
    }*/
    
}