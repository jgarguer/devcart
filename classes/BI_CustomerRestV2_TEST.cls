@isTest
private class BI_CustomerRestV2_TEST {
    
    @isTest static void test_createCustomer() {
            
            //Create data
            Account acc = createData();

            FS_TGS_RestWrapperFullStack.CustomerRequestType custReqType = createCustomerRequestType(acc);

            RestRequest req = new RestRequest();
            RestContext.response = new RestResponse();
            System.debug(LoggingLevel.ERROR, 'ELJSON:' + JSON.serialize(custReqType));
            req.requestURI ='/customerinfo/v2/customers/23908';
            req.requestBody = Blob.valueOf(JSON.serialize(custReqType));
            RestContext.request = req;

            BI_CustomerRestV2.createCustomer();

            List<Account> queryAccount = [SELECT Id FROM Account];

            System.assert(queryAccount.size()==1);

           
    }


    @isTest static void test_exception_createCustomer() {

        
        try{

                Account acc = createData();

                FS_TGS_RestWrapperFullStack.CustomerRequestType custReqType = createCustomerRequestType(acc);

                RestRequest req = new RestRequest();
                RestContext.response = new RestResponse();

                req.requestURI ='/customerinfo/v2/customers/';
                req.requestBody = Blob.valueOf(JSON.serialize(custReqType));
                RestContext.request = req;

                BI_CustomerRestV2.createCustomer();

                FS_TGS_RestWrapperFullStack.GenericResponse oGenericResponse = (FS_TGS_RestWrapperFullStack.GenericResponse)Json.deserialize(RestContext.response.responseBody.tostring(), FS_TGS_RestWrapperFullStack.GenericResponse.Class);
                System.assertEquals(500, RestContext.response.statusCode);

        }catch(Exception e){

            System.assert(String.isNotBlank(e.getMessage()));
        }

        
        
    }

    @isTest static void test_updateCustomer(){

            //Create data
            Account acc = createData();

            insert acc;

            acc.SAP_ID__c = 'E99999';

            FS_TGS_RestWrapperFullStack.CustomerRequestType custReqType = createCustomerRequestType(acc);

            RestRequest req = new RestRequest();
            RestContext.response = new RestResponse();

            req.requestURI ='/customerinfo/v2/customers/23908';
            req.requestBody = Blob.valueOf(JSON.serialize(custReqType));
            RestContext.request = req;

            BI_CustomerRestV2.updateCustomer();

            list<Account> queryAccount = [SELECT Id, SAP_ID__c FROM Account];

            System.assertEquals('E99999', queryAccount.get(0).SAP_ID__c);

            req.requestURI ='/customerinfo/v2/customers/23907';
            req.requestBody = Blob.valueOf(JSON.serialize(custReqType));
            RestContext.request = req;

            BI_CustomerRestV2.updateCustomer();
            System.assertEquals(404, RestContext.response.statusCode);

    }

    @isTest static void test_exception_updateCustomer(){

            
        try{

            //Create data
            Account acc = createData();

            insert acc;

            acc.SAP_ID__c = 'E99999';

            FS_TGS_RestWrapperFullStack.CustomerRequestType custReqType = createCustomerRequestType(acc);

            RestRequest req = new RestRequest();
            RestContext.response = new RestResponse();
            

            req.requestURI ='/customerinfo/v2/customers/';
            req.requestBody = Blob.valueOf(JSON.serialize(custReqType));
            RestContext.request = req;

            BI_CustomerRestV2.updateCustomer();


        }catch(Exception e){

                System.assert(String.isNotBlank(e.getMessage()));
        }
            



    }

    private static Account createData(){
   // 20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
     // BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
            Account acc = new Account();

            acc.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
            acc.Name = 'Test Account';
            acc.BI_Identificador_Externo__c = '23908';
            acc.Fax = '916755532';
            acc.Phone = '654337899';
            acc.NumberOfEmployees = 467; 
            acc.BI_No_Identificador_fiscal__c = '654358756'; 
            acc.AnnualRevenue = 2897600;
            acc.BI_Country__c = 'Spain';  
            acc.BI_Tipo_de_identificador_fiscal__c = 'NIF';
            acc.Website = 'test@accenture.com';
            acc.BI_Inicio_actividad_del_cliente__c = Date.today();
            acc.Rating = 'Cold';
            acc.BI_Riesgo__c = '1 - Sin problema';
            acc.BI_Activo__c = 'Sí';
            acc.BI_Denominacion_comercial__c = 'Test';
            acc.TGS_Account_Mnemonic__c = 'Test';
            acc.TGS_Account_Category__c = 'Corporate';
            acc.TGS_Es_MNC__c = true;
            acc.TGS_Account_Leading_MNC_Unit__c = 'SPAIN';
            acc.SAP_ID__c = 'E34622';
            acc.TGS_Invoice_Language__c = false;
            acc.BI_Segment__c                         = 'test';
            acc.BI_Subsegment_Regional__c             = 'test';
            acc.BI_Territory__c                       = 'test';
            return acc;

    }

    private static FS_TGS_RestWrapperFullStack.CustomerRequestType createCustomerRequestType(Account acc){
        
        FS_TGS_RestWrapperFullStack.CustomerRequestType custType = new FS_TGS_RestWrapperFullStack.CustomerRequestType(); 
        
        custType.name = acc.Name;
        custType.type = acc.RecordTypeId;
        custType.segment = FS_TGS_RestWrapperFullStack.SegmentType.corporate;
             
        //legalId
        FS_TGS_RestWrapperFullStack.IdentificationType legal = new FS_TGS_RestWrapperFullStack.IdentificationType();
        list<FS_TGS_RestWrapperFullStack.IdentificationType> legalId = new list<FS_TGS_RestWrapperFullStack.IdentificationType>();
        legal.nationalID = acc.BI_No_Identificador_fiscal__c!=null?acc.BI_No_Identificador_fiscal__c:'';
        legal.nationalIDType = acc.BI_Tipo_de_identificador_fiscal__c!=null?acc.BI_Tipo_de_identificador_fiscal__c:'';
        legalId.add(legal);
        custType.legalId = legalId;

        //customerCreditProfile, mapeo entre el valor del Rating con el numero de creditRiskRating ?????
        FS_TGS_RestWrapperFullStack.CreditProfileType creditProfile = new FS_TGS_RestWrapperFullStack.CreditProfileType();
        list<FS_TGS_RestWrapperFullStack.CreditProfileType> listaCred = new list<FS_TGS_RestWrapperFullStack.CreditProfileType>();
        creditProfile.creditRiskRating = acc.Rating!=null?acc.Rating == 'Cold'?0:0:0;
        listaCred.add(creditProfile);
        custType.customerCreditProfile = listaCred;

       
        //customerStatus
        if(acc.BI_Activo__c != null){
            if(acc.BI_Activo__c == Label.BI_Si)
                custType.status = FS_TGS_RestWrapperFullStack.CustomerStatusType.active;
            else if(acc.BI_Activo__c == Label.BI_No)
                 custType.status = FS_TGS_RestWrapperFullStack.CustomerStatusType.inactive;
            else if(acc.BI_Activo__c == 'Pendiente de aprobación')
                custType.status = FS_TGS_RestWrapperFullStack.CustomerStatusType.prospective;
        }


        //characteristic
        list<FS_TGS_RestWrapperFullStack.NameValueType> listachar = new list<FS_TGS_RestWrapperFullStack.NameValueType>();
        FS_TGS_RestWrapperFullStack.NameValueType charact = new FS_TGS_RestWrapperFullStack.NameValueType();
        charact.name = 'country';
        charact.value = acc.BI_Country__c!=null? acc.BI_Country__c:'';
        listachar.add(charact);
        custType.characteristic = listachar;
        
        //contactingModes
        custType.contactMedium = new List<FS_TGS_RestWrapperFullStack.ContactingModeType>();
        
        if(acc.Phone != null){
            FS_TGS_RestWrapperFullStack.ContactingModeType phone = new FS_TGS_RestWrapperFullStack.ContactingModeType();
            List<FS_TGS_RestWrapperFullStack.KeyvalueType> phones = new List<FS_TGS_RestWrapperFullStack.KeyvalueType>();
            FS_TGS_RestWrapperFullStack.KeyvalueType tipo = new FS_TGS_RestWrapperFullStack.KeyvalueType();
            phone.type = 'phone';
            tipo.value = acc.Phone;
            phones.add(tipo); 
            phone.medium = phones;           
            custType.contactMedium.add(phone);
        }
        
        if(acc.Fax != null){
            FS_TGS_RestWrapperFullStack.ContactingModeType fax = new FS_TGS_RestWrapperFullStack.ContactingModeType();
            List<FS_TGS_RestWrapperFullStack.KeyvalueType> faxes = new List<FS_TGS_RestWrapperFullStack.KeyvalueType>();
            FS_TGS_RestWrapperFullStack.KeyvalueType tipo = new FS_TGS_RestWrapperFullStack.KeyvalueType();
            fax.type = 'fax';
            tipo.value = acc.Fax;
            faxes.add(tipo); 
            fax.medium = faxes;    
            custType.contactMedium.add(fax);
        }
        
        if(acc.Website != null){
            FS_TGS_RestWrapperFullStack.ContactingModeType web = new FS_TGS_RestWrapperFullStack.ContactingModeType();
            List<FS_TGS_RestWrapperFullStack.KeyvalueType> webs = new List<FS_TGS_RestWrapperFullStack.KeyvalueType>();
            FS_TGS_RestWrapperFullStack.KeyvalueType tipo = new FS_TGS_RestWrapperFullStack.KeyvalueType();
            web.type = 'website';
            tipo.value = acc.Website;
            webs.add(tipo); 
            web.medium = webs;    
            custType.contactMedium.add(web);
        }
        
        //ParentCustomer
        custType.parentCustomer = acc.Parent.BI_Identificador_Externo__c!=null?acc.Parent.BI_Identificador_Externo__c:'';

        //Additional data
        list<FS_TGS_RestWrapperFullStack.KeyValueType> additionalData = new list<FS_TGS_RestWrapperFullStack.KeyValueType>();

        FS_TGS_RestWrapperFullStack.KeyValueType data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        //data.key = 'segment';
        //data.value = acc.TGS_Account_Category__c != null? acc.TGS_Account_Category__c:'';
        //additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'AnnualRevenue';
        data.value = acc.AnnualRevenue !=null?String.valueOf(acc.AnnualRevenue):'';
        additionalData.add(data);
       
        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'NumberOfEmployees';
        data.value = acc.NumberOfEmployees != null?String.valueOf(acc.NumberOfEmployees):'';
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'website';
        data.value = acc.Website !=null?acc.Website:'';
        additionalData.add(data);

        //Cambiar el formato de fecha
        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'customerSince';
        data.value = acc.BI_Inicio_actividad_del_cliente__c!=null?String.valueOf(acc.BI_Inicio_actividad_del_cliente__c):'';
        additionalData.add(data);
        
        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'creditStatus';
        if(acc.BI_Riesgo__c!=null){
            if (acc.BI_Riesgo__c =='Sin problema'){
                data.value = '1';
            }else if(acc.BI_Riesgo__c =='Requiere autorización'){
                    data.value = '2';
                }else if(acc.BI_Riesgo__c == 'Bloquea al cliente'){
                    data.value = '3';
                }
        }else{
            data.value = '0';
        }
        additionalData.add(data);
        

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'legalName';
        data.value = acc.BI_Denominacion_comercial__c != null?acc.BI_Denominacion_comercial__c:'';
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'acronym';
        data.value = acc.TGS_Account_Mnemonic__c != null?acc.TGS_Account_Mnemonic__c:'';
        additionalData.add(data);

        //campo nuevo
        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'customerClassification';
        data.value = '';
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'mnc';
        data.value = String.valueOf(acc.TGS_Es_MNC__c); //Siempre va a ser true
        additionalData.add(data);

        //campo nuevo
        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'billingType';
        data.value = '';
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'accountLeadingMncUnit';
        data.value = acc.TGS_Account_Leading_MNC_Unit__c != null?acc.TGS_Account_Leading_MNC_Unit__c:'';
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'sapCode';
        data.value = acc.SAP_ID__c != null?acc.SAP_ID__c:'';
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'language';
        data.value = String.valueOf(acc.TGS_Invoice_Language__c);
        additionalData.add(data);

        data = new FS_TGS_RestWrapperFullStack.KeyValueType();
        data.key = 'revenueCodeAggrupation';
        data.value = '';
        additionalData.add(data);
                            
        custType.additionalData = additionalData;
    
     
        
        return custType;
        
    }
    

    
    
}