@IsTest
public class VE_venta_express_trg_code_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Test class for VE_venta_express_trg_code
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void testMoveOppty(){
        List<Opportunity> ops = new List<Opportunity>();
        Opportunity o = new Opportunity();
        o.stagename='F5 - Solution Definition';
        o.ve_express__c=true;
        o.BI_Productos_numero__c=3;
        
        ops.add(o);
        
        Test.startTest();
        
        VE_venta_express_trg_code.moveOppty(ops);
        
        Test.stopTest();
        
        System.assertEquals(ops.get(0).stagename, 'F4 - Offer Development');
    }
    
}