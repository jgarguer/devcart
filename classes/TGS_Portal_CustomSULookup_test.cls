@isTest
public class TGS_Portal_CustomSULookup_test {

	static testMethod void callConstructorTier1() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		
        System.runAs(userTest){
            //JV
            /*Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;*/
            //JV
            Pagereference p = Page.TGS_Portal_CustomSULookup;
        	p.getParameters().put('tier1', '%tier1');
        	Test.setCurrentPageReference(p);
            Test.startTest();
			TGS_Portal_CustomSULookup pCSUL = new TGS_Portal_CustomSULookup();
            Test.stopTest();
			pCSUL.getFormTag();
			pCSUL.getTextBox();
			pCSUL.search();
        }
	}
    
	static testMethod void callConstructorTier2() {
		/*User userTest = [SELECT Id, Name, ContactId, Contact.Id 
						FROM User 
						WHERE profileId IN (
											SELECT Id 
											FROM Profile 
											WHERE Name = 'TGS Customer Community Plus') 
						AND isActive = true LIMIT 1];*/
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);        
        System.runAs(userTest){
            //JV
            /*Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;*/
            //JV
            Pagereference p = Page.TGS_Portal_CustomSULookup;
        	p.getParameters().put('tier2', '%tier2');
        	Test.setCurrentPageReference(p);
			TGS_Portal_CustomSULookup pCSUL = new TGS_Portal_CustomSULookup();
			pCSUL.getFormTag();
			pCSUL.getTextBox();
			pCSUL.search();
        }
	}
    
    static testMethod void callConstructorTier3() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);

		/*User userTest = [SELECT Id, Name, ContactId, Contact.Id 
						FROM User 
						WHERE profileId IN (
											SELECT Id 
											FROM Profile 
											WHERE Name = 'TGS Customer Community Plus') 
						AND isActive = true LIMIT 1];*/
        System.runAs(userTest){
            //JV
            /*Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;*/
            //JV
            Pagereference p = Page.TGS_Portal_CustomSULookup;
        	p.getParameters().put('tier3', '%tier3');
        	Test.setCurrentPageReference(p);
			TGS_Portal_CustomSULookup pCSUL = new TGS_Portal_CustomSULookup();
			pCSUL.getFormTag();
			pCSUL.getTextBox();
			pCSUL.search();
        }
	}
    
    @isTest static void test_paging() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Pagereference p = Page.TGS_Portal_CustomSULookup;
        	Test.setCurrentPageReference(p);
            Test.startTest();
			TGS_Portal_CustomSULookup pCSUL = new TGS_Portal_CustomSULookup();
            Test.stopTest();
            pCSUL.gethasPrev();
            pCSUL.gethasNext();
            pCSUL.next(); // next before prev!
            pCSUL.prev();
        }
    }
}