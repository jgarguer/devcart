public class BI_ClientesIndicadoresJOB_ChatterFeed {
       
    public static void createChatterFeedsReport(Map<String, String> group_msg, String idInforme){
       
        List<FeedItem> lst_feedItem = new List<FeedItem>();
        
        for(String key : group_msg.keySet()){
            
            FeedItem post = new FeedItem();
            
           	post.ParentId = key ;
        	post.Body = group_msg.get(key).contains('#informe#') ? group_msg.get(key).replace('#informe#', System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + idInforme) : group_msg.get(key);
            lst_feedItem.add(post);
        }

         
        insert lst_feedItem;
    }									


}