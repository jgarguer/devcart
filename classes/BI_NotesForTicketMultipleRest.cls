@RestResource(urlMapping='/ticketing/v1/tickets/*/notes')
global class BI_NotesForTicketMultipleRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Notes Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for the notes related to a specific ticket.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.TicketNotesListType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.TicketNotesListType getNotesForTicket() {
		
		BI_RestWrapper.TicketNotesListType res;
		
		try{
		
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
			
				//OBTAIN NOTES
				res = BI_RestHelper.getNotesForTicket(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND, 200 OK
					
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_NotesForTicketMultipleRest.getNotesForTicket', 'BI_EN', exc, 'Web Service');
			
		}
		
		return res;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a new note related to a ticket in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void createNote() {
		
		try{
			
			if(RestContext.request.headers.get('countryISO') == null){
								
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);			
								
			}else{
		
				RestRequest req = RestContext.request;
			
				Blob blob_json = req.requestBody; 
			
				String string_json = blob_json.toString(); 
				
				BI_RestWrapper.TicketNoteRequestType ticNoteRequestType = (BI_RestWrapper.TicketNoteRequestType) JSON.deserialize(string_json, BI_RestWrapper.TicketNoteRequestType.class);
				
				String noteExtId;
			
				if(ticNoteRequestType.additionalData != null){
				
					for(BI_RestWrapper.KeyvalueType keyValue:ticNoteRequestType.additionalData){
						
						if(keyValue.Key == 'noteId'){
							noteExtId = keyValue.value;
							break;
						}
						
					}
				
					if(noteExtId == null){
						
						RestContext.response.statuscode = 400;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' noteId');
						
					}else{
						
						Boolean author_error = false;
						List<User> lst_user = new List<User>();
						
						if(ticNoteRequestType.author != null){
							
							lst_user = [select Id from User where BI_CodigoUsuario__c = :ticNoteRequestType.author limit 1];
						
							if(lst_user.isEmpty()){
								
								//ERROR
								RestContext.response.statuscode = 404;//NOT_FOUND
								RestContext.response.headers.put('errorMessage', Label.BI_UserNotFound);
								author_error = true;
								
							}
							
						}
						
						if(!author_error){
							
							Id idAuthor = (lst_user.isEmpty())?null:lst_user[0].Id;
								
							CaseComment ccomment = new CaseComment();
				
							ccomment = BI_RestHelper.generateNote(ccomment, ticNoteRequestType, RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'), RestContext.request.requestURI.split('/')[4]);
							
							if(ccomment == null){
								
								RestContext.response.statuscode = 404;//NOT_FOUND
								RestContext.response.headers.put('errorMessage', Label.BI_TicketNotFound);
								
							}else{
								
								insert ccomment;
								
								BI_Comentario__c ccomment_custom = new BI_Comentario__c();
								
								ccomment_custom = BI_RestHelper.generateNoteCustom(ccomment_custom, ccomment, ticNoteRequestType, RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'), noteExtId, idAuthor);
							
								insert ccomment_custom;
							
								RestContext.response.statuscode = 201;
								RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/services/apexrest/tickets/'+RestContext.request.requestURI.split('/')[4]+'/notes/'+ccomment_custom.BI_Id_del_comentario_Legado__c);
								
							}
							
						}
						
					}
					
				}else{
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' noteId');
					
				}
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL SERVER ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_NotesForTicketMultipleRest.createNote', 'BI_EN', exc, 'Web Service');
			
		}
		
	}
	
}