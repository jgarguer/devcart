@isTest(seeAllData = false)
public class BIIN_UNICA_AccountHandler_TEST 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González Beltrán
  Company:       HPE
  Description:   Test method to manage the code coverage for BIIN_UNICA_AccountHandler
  
  <Date>             <Author>                        			<Change Description>
  20/06/2016         José Luis González Beltrán           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
  private final static String LE_NAME = 'TestAccount_AccountHandler_TEST';

  @testSetup static void dataSetup() 
  {
      Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
      insert account;
  }

  @isTest static void test_BIIN_UNICA_AccountHandler_TEST()
  {
  	Account account = [SELECT Id, Name, BI_Activo__c, BI_Country__c, BI_Segment__c, BI_Tipo_de_identificador_fiscal__c, 
  														BI_No_Identificador_fiscal__c, BI_Validador_Fiscal__c, RecordTypeId, BI_RecordType_DevName__c 
  											FROM Account WHERE Name =: LE_NAME LIMIT 1];

    String accountStr = JSON.serialize(account);

  	Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));
    Test.startTest(); 

    BIIN_UNICA_AccountHandler accountHandler = new BIIN_UNICA_AccountHandler();
    
    accountHandler.bulkBefore();
    accountHandler.bulkAfter();
    accountHandler.beforeInsert('');
    accountHandler.beforeUpdate('', '');
    accountHandler.beforeDelete('');
    BIIN_UNICA_AccountHandler.afterInsert(accountStr);
    String accountModified = accountStr.replace('TestAccount_AccountHandler_TEST', 'TestAccount_AccountHandler_TEST_UPDATE');
    BIIN_UNICA_AccountHandler.afterUpdate(accountStr, accountModified);
    accountHandler.afterDelete('');
    accountHandler.andFinally();
        
    Test.stopTest();
  }
}