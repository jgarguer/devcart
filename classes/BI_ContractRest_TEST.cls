@isTest
private class BI_ContractRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ContractRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    17/09/2014              Pablo Oliva             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContractRest.getContract()
    History:
    
    <Date>              <Author>                <Description>
    17/09/2014          Pablo Oliva             Initial version
    05/10/2017          Guillermo Muñoz         Added BI_MigrationHelper.skipAllTriggers to avoid unnecesary executions
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getContractSFTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_MigrationHelper.skipAllTriggers();
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                               
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        lst_acc = [select Id, OwnerId, BI_Id_del_cliente__c from Account where Id = :lst_acc[0].Id];
        
        List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null);
        
        List <Contract> lst_con = BI_DataLoadRest.loadContracts(1, lst_acc[0], lst_opp);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contracts/test'; 
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        BI_MigrationHelper.cleanSkippedTriggers();

        Test.startTest();
        
        //INTERNAL_SERVER_ERROR
        BI_ContractRest.getContract();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c WHERE BI_Tipo_de_componente__c = 'Web Service'];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //NOT FOUND
        BI_TestUtils.throw_exception = false;
        BI_ContractRest.getContract();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/accountresources/v1/contracts/'+lst_con[0].Id;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO',  region.BI_Country_ISO_Code__c);
        BI_ContractRest.getContract();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContractRest.getContract()
    History:
    
    <Date>              <Author>                <Description>
    17/09/2014          Pablo Oliva             Initial version
    05/10/2017          Guillermo Muñoz         Added BI_MigrationHelper.skipAllTriggers to avoid unnecesary executions
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getContractNETest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_MigrationHelper.skipAllTriggers();
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List<NE__Contract__c> lst_conNE = BI_DataLoadRest.loadContractsNE(10, lst_acc[0]);
        
        List<NE__Contract_Account_Association__c> lst_caa = BI_DataLoadRest.loadContractAccountAssociations(lst_acc[0], lst_conNE);
        
        lst_acc = [select Id, OwnerId, BI_Id_del_cliente__c from Account where Id = :lst_acc[0].Id];
        
        List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], lst_caa);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contracts/test'; 
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        BI_MigrationHelper.cleanSkippedTriggers();

        Test.startTest();
        
        //INTERNAL_SERVER_ERROR
        BI_ContractRest.getContract();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c WHERE BI_Tipo_de_componente__c = 'Web Service'];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //NOT FOUND
        BI_TestUtils.throw_exception = false;
        BI_ContractRest.getContract();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/accountresources/v1/contracts/'+lst_conNE[0].Id;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO',  region.BI_Country_ISO_Code__c);
        BI_ContractRest.getContract();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}