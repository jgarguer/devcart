@isTest
private class BI_TicketRest_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_TicketRest class 
    
    History: 
    
    <Date>              <Author>                <Change Description>
    18/09/2014          Pablo Oliva             Initial Version
    18/05/2016          José Luis González      Adapt test to UNICA interface class modifications
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_TicketRest.getTicket()
    History:
    
    <Date>              <Author>                <Description>
    18/09/2014          Pablo Oliva             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static
    {
        BI_TestUtils.throw_exception = false;
    }

    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_TicketRest_TEST';

    @testSetup static void dataSetup() 
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }
    
    static testMethod void getTicketTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                              
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Case> lst_tickets = BI_DataLoadRest.loadTickets(1, lst_acc[0].Id, lst_region[0]);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/ticketing/v1/tickets/'+lst_tickets[0].BI_Id_del_caso_Legado__c;  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        //INTERNAL_SERVER_ERROR
        BI_TestUtils.throw_exception = true;
        BI_TicketRest.getTicket();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c where BI_Asunto__c = 'BI_TicketRest.getOneTicket'];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST
        BI_TestUtils.throw_exception = false;
        BI_TicketRest.getTicket();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        req.requestURI = '/ticketing/v1/tickets/idTicket';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
        
        BI_TicketRest.getTicket();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/ticketing/v1/tickets/'+lst_tickets[0].BI_Id_del_caso_Legado__c;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_TicketRest.getTicket();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();

    }
    
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_TicketRest.doPut()
    History:
    
    <Date>              <Author>                <Description>
    19/09/2014          Pablo Oliva             Initial version
    18/05/2016          José Luis González      Adapt test to UNICA interface class modifications
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void doPutTest()
    {
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, CaseNumber FROM Case WHERE AccountId =: account.Id];

        String ticketRequestMissingFieldsStr = '{"correlatorId":"' + ticketIncidenciaTecnica.BI_Id_del_caso_Legado__c + '","subject":"","description":"Description Test - UPDATED","country":"Peru","customerId":"' + account.BI_Validador_Fiscal__c + '","reportedDate":"1462871973","perceivedSeverity":"1000","perceivedPriority":1,"ticketType":"Incident","source":"Phone","parentTicket":"","additionalData":[{"key":"First Name","value":"Edgard Alberto"},{"key":"Last Name","value":"Anaya Gonzalez"},{"key":"Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Direct Contact First Name","value":"Edgard Alberto"},{"key":"Direct Contact Last Name","value":"Anaya Gonzalez"},{"key":"Direct Contact Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Urgency","value":"2000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"IMPLANTACION  DEMANDA"},{"key":"Categorization Tier 2","value":"GOLDEN JUMPER"},{"key":"Categorization Tier 3","value":"CAMBIO"},{"key":"Product Categorization Tier 1","value":"DOCUMENTACION"},{"key":"Product Categorization Tier 2","value":"DOCUMENTO"},{"key":"Product Categorization Tier 3","value":"DOCUMENTO"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZUW9FMNL0P03GM"},{"key":"Name","value":"AST_OB_BS_IN_CI_Name"},{"key":"Case Number","value":"00299913"},{"key":"Created Date","value":"1462871973"},{"key":"Status","value":"1"},{"key":"Status Reason","value":null},{"key":"Assigned Group","value":""},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1462871973"},{"key":"Close Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":"1462871973"},{"key":"Resolution Category Tier 1","value":""},{"key":"Resolution Category Tier 2","value":""},{"key":"Resolution Category Tier 3","value":""}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestMissingFields = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestMissingFieldsStr, BIIN_UNICA_Pojos.TicketRequestType.class);
        //Created Date set to null
        String ticketRequestBadRequestStr = '{"correlatorId":"' + ticketIncidenciaTecnica.BI_Id_del_caso_Legado__c + '","subject":"Subject Test - UPDATED","description":"Description Test - UPDATED","country":"Peru","customerId":"' + account.BI_Validador_Fiscal__c + '","reportedDate":"1462871973","perceivedSeverity":"minor","perceivedPriority":1,"ticketType":"Incident","source":"Phone","parentTicket":"","additionalData":[{"key":"First Name","value":"Edgard Alberto"},{"key":"Last Name","value":"Anaya Gonzalez"},{"key":"Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Direct Contact First Name","value":"Edgard Alberto"},{"key":"Direct Contact Last Name","value":"Anaya Gonzalez"},{"key":"Direct Contact Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Urgency","value":"2000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"IMPLANTACION  DEMANDA"},{"key":"Categorization Tier 2","value":"GOLDEN JUMPER"},{"key":"Categorization Tier 3","value":"CAMBIO"},{"key":"Product Categorization Tier 1","value":"DOCUMENTACION"},{"key":"Product Categorization Tier 2","value":"DOCUMENTO"},{"key":"Product Categorization Tier 3","value":"DOCUMENTO"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZUW9FMNL0P03GM"},{"key":"Name","value":"AST_OB_BS_IN_CI_Name"},{"key":"Case Number","value":"' + ticketIncidenciaTecnica.CaseNumber + '"},{"key":"Created Date","value":null},{"key":"Status","value":"1"},{"key":"Status Reason","value":null},{"key":"Assigned Group","value":""},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1462871973"},{"key":"Close Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":"1462871973"},{"key":"Resolution Category Tier 1","value":""},{"key":"Resolution Category Tier 2","value":""},{"key":"Resolution Category Tier 3","value":""}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestBadRequest = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestBadRequestStr, BIIN_UNICA_Pojos.TicketRequestType.class);

        String ticketRequestOkStr = '{"correlatorId":"' + ticketIncidenciaTecnica.BI_Id_del_caso_Legado__c + '","subject":"Subject Test - UPDATED","description":"Description Test - UPDATED","country":"Peru","customerId":"' + account.BI_Validador_Fiscal__c + '","reportedDate":"1464843122","perceivedSeverity":"1000","perceivedPriority":1,"ticketType":"Incident","source":"6000","parentTicket":"","additionalData":[{"key":"First Name","value":"PRUEBA ACME"},{"key":"Last Name","value":"PRUEBA ACME"},{"key":"Internet Email","value":"PRUEBAS_ACME@acme.org"},{"key":"Direct Contact First Name","value":" "},{"key":"Direct Contact Last Name","value":""},{"key":"Direct Contact Internet Email","value":""},{"key":"Urgency","value":"3000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"GESTION"},{"key":"Categorization Tier 2","value":"PORTAL DE CLIENTES"},{"key":"Categorization Tier 3","value":"FALLA  SERVICIO"},{"key":"Product Categorization Tier 1","value":"BASE DE DATOS"},{"key":"Product Categorization Tier 2","value":"BASE DE DATOS"},{"key":"Product Categorization Tier 3","value":"BASE DE DATOS"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZU6LFM10CQ0JNC"},{"key":"Name","value":"AFPHBTDCMON01"},{"key":"Case Number","value":"' + ticketIncidenciaTecnica.CaseNumber + '"},{"key":"Created Date","value":"1464858995"},{"key":"Status","value":"1"},{"key":"Status Reason","value":""},{"key":"Assigned Group","value":"ACME GROUP 1"},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1464859000"},{"key":"Closed Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":""},{"key":"Resolution Category","value":"BUZON DE CORREO"},{"key":"Resolution Category Tier 2","value":"CORREO"},{"key":"Resolution Category Tier 3","value":"ARCHIVO OST CORRUPTO"}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestOk = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestOkStr, BIIN_UNICA_Pojos.TicketRequestType.class);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        Test.startTest();

        req.httpMethod = 'PUT';
        req.addHeader('UNICA-ServiceId', 'Peru');
        req.requestURI = '/ticketing/v1/tickets/' + ticketIncidenciaTecnica.BI_Id_del_caso_Legado__c;

        RestContext.request = req;
        RestContext.response = res;
        
        // BAD REQUEST missing mandatory fields
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestMissingFields));
        RestContext.request = req;
        BI_TicketRest.doPut(ticketRequestMissingFields);
        system.assertEquals(400, RestContext.response.statuscode);
        
        // OK
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestOk));
        RestContext.request = req;
        BI_TicketRest.doPut(ticketRequestOk);
        system.assertEquals(200, RestContext.response.statuscode);

        // INTERNAL_SERVER_ERROR
        req.requestURI = '/ticketing/v1/tickets/00000000';
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestBadRequest));
        RestContext.request = req;
        BI_TicketRest.doPut(ticketRequestBadRequest);
        system.assertEquals(500, RestContext.response.statuscode);

        Test.stopTest();
        
    }
}