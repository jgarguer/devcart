public with sharing class PCA_IdeasFAQCtrl extends PCA_HomeController {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    10/07/2014        Micah Burgos         v2
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public String IdFAQ {get;set;}
	public List<Solution> solutionsPlatinoTotal {get;set;}
	public String Title {get;set;}
	
	public String Detail {get;Set;}
	private map<Id,Solution> faqMap;
	
	//Search
	public String searchText {get;set;}
	public boolean searchResult {get;set;}
	public Id idOtherFaq {get;set;}
	
	//Language
	public List<SelectOption> LanguageFields		{get; set;}
	public String Language {get; set;}
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Check permissions of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			PageReference page = enviarALoginComm();
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.checkPermissions', 'Portal Platino',Exc, 'Class');
		   return null;
		}
	}
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:  Constructor 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public PCA_IdeasFAQCtrl(){}
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   load info of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
	public void loadInfo (){
	
		try{
		//searchResult= false;
		if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
		recoverySolutions();
		
		IdFAQ = (Apexpages.currentPage().getParameters().get('Id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
		if(IdFAQ != null){
			if((IdFAQ.length() == 15 || IdFAQ.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', IdFAQ)) {
				if(!faqMap.isEmpty()){
					Title = faqMap.get(IdFAQ).SolutionName;
					Detail = faqMap.get(IdFAQ).SolutionNote;
				}
			}
		}		
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.loadInfo', 'Portal Platino',Exc, 'Class');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   recovery Solutions of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
	public void recoverySolutions(){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			this.numberAllFaqsShow = 10;
			solutionsPlatinoTotal = [Select SolutionNote, SolutionName, Id From Solution];
			faqMap = new map<Id,Solution>(solutionsPlatinoTotal);
			
			System.debug('faqMap: ' + faqMap);
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.recoverySolutions', 'Portal Platino',Exc, 'Class');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show more FAQs of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/			
	public void showOtherFaq(){ 
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			searchResult = false;	
			if(!faqMap.isEmpty() && (idOtherFaq != null)){
				Title = faqMap.get(idOtherFaq).SolutionName;
				Detail = faqMap.get(idOtherFaq).SolutionNote;
				//Adjunto = solutionsPlatino[0].SolutionName;
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.showOtherFaq', 'Portal Platino',Exc, 'Class');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   search some FAQ of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/			
	public void searchFAQS(){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			this.numberAllFaqsShow = 20;
		
			solutionsPlatinoTotal = Database.query('Select SolutionNote, SolutionName, Id From Solution WHERE SolutionName like \'%' + searchText + '%\'');
													//[Select SolutionNote, SolutionName, Id From Solution WHERE SolutionName like '%' :searchText '%' limit 100];
			if(solutionsPlatinoTotal.isEmpty()){
				Title = null;
				Detail = null;
				searchResult = false;
			}else{
				Title = solutionsPlatinoTotal[0].SolutionName;
				Detail = solutionsPlatinoTotal[0].SolutionNote;
				searchResult = true;				
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.searchFAQS', 'Portal Platino',Exc, 'Class');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method to go back in ideasFAQ 
    
    History:
    
    <Date>            <Author>          	<Description>
    08/10/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
	public void BackFaqs(){		
			searchText='';
			searchResult = false;
			searchFAQS();

	}

	public Integer numberAllFaqsShow { get; set;}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Increment number of FAQ for each view of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/			
	public void incrementNumberAllFaqsShow() {
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			this.numberAllFaqsShow += 5;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.incrementNumberAllFaqsShow', 'Portal Platino',Exc, 'Class');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show all FAQs of FAQ page 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/			
	public boolean getshowMoreNumberAllFaqs() {
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			boolean result = false;
			
			if(solutionsPlatinoTotal != null){
				if((!this.solutionsPlatinoTotal.isEmpty()) && (this.numberAllFaqsShow < this.solutionsPlatinoTotal.size())){
					result = true;
				}
			}
			
			return  result;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.getshowMoreNumberAllFaqs', 'Portal Platino',Exc, 'Class');
		   return null;
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Select Language of faqs 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
	public void selectLanguage(){
		
	}
	
	public Id dato{		
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Select Actual Acc of faqs 
    
    History:
    
    <Date>            <Author>          	<Description>
    04/07/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/				
		get{
			try{
				return BI_AccountHelper.getCurrentAccountId();
			}catch (exception Exc){ 
		  	 	BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.dato', 'Portal Platino',Exc, 'Class');
		   		return null;
		}
			
		}
		set{
			
		}
	}
		
	
}