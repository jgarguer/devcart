/** 
 * Clase de Utilidad para manipular fechas/tiempo
 * @author Juan Carlos Elizalde Ayala <asuej28@gmail.com>
 * @version 1.0
 */
public class UtilidadesFechas {

	/** 
	 * Obtiene la diferencia de dias habiles entre 2 fechas
	 * @param fechaInicio Fecha limite inferior
	 * @param fechaFin Fecha limite superior
	 * @return Numero de dias habiles que existe entre las 2 fechas
	 */
	public static Integer obtenerDiasHabiles(DateTime fechaInicio, DateTime fechaFin) {
	
		Integer diasHabiles = 0;
		
		Integer diasSemana = 7;
		Integer sabado = 5;
		Integer domingo = 6;
		Integer diasSabadoDomingo = 2;

		Date FI = fechaInicio.date();
		Date FF = fechaFin.date();
		Date inicioSemanaFI = FI.toStartOfWeek();
		Integer diferenciaInicioSemanaFI = inicioSemanaFI.daysBetween(FI);
		Integer diferenciaInicioSemanaFF = inicioSemanaFI.daysBetween(FF);
		Integer numSemanas = 0;
		
		// Si la diferencia es mas de una semana
		if ( diferenciaInicioSemanaFF >= diasSemana ) {
			
			numSemanas = diferenciaInicioSemanaFF / diasSemana;
			diasHabiles = (diferenciaInicioSemanaFF - (numSemanas * diasSabadoDomingo)) - diferenciaInicioSemanaFI;
			
			// Si fecha de fin cae en sabado restar 1 dia, si cae en domingo restar 2 dias
			if (Math.mod(diferenciaInicioSemanaFF, diasSemana) == sabado) {
				diasHabiles -= 1;
			} else if (Math.mod(diferenciaInicioSemanaFF, diasSemana) == domingo) {
				diasHabiles -= 2;
			}
		
		// Si la diferencia es menor a una semana
		} else {
		
			// Si cae entre dias habiles (0,1,2,3,4)
			if ( diferenciaInicioSemanaFI <= 4 ) {
				
				// Si fecha de fin cae en sabado restar 1 dia, si cae en domingo restar 2 dias
				if (diferenciaInicioSemanaFF == sabado) {
					diasHabiles = FI.daysBetween(FF) - 1;
				} else if (diferenciaInicioSemanaFF == domingo) {
					diasHabiles = FI.daysBetween(FF) - 2;
				} else {
					diasHabiles = FI.daysBetween(FF);
				}
							
			}
		
		}
		
		return diasHabiles;
	}
	
	/**
	 * Metodo para probar la funcionalidad de la Clase UtilidadesFechas
	 */
	 /*
	 static testmethod void probarUtilidadesFechas() {
	 	
	 	System.debug('DIAS HABILES =====> ' + UtilidadesFechas.obtenerDiasHabiles(
	 			DateTime.newInstance(2010, 3, 24),
	 			DateTime.newInstance(2010, 3, 26)));
	 	
	 	System.debug('DIAS HABILES =====> ' + UtilidadesFechas.obtenerDiasHabiles(
	 			DateTime.newInstance(2010, 3, 24),
	 			DateTime.newInstance(2010, 3, 27)));
	 			
	 	System.debug('DIAS HABILES =====> ' + UtilidadesFechas.obtenerDiasHabiles(
	 			DateTime.newInstance(2010, 3, 24),
	 			DateTime.newInstance(2010, 3, 28)));
	 			
	 	System.debug('DIAS HABILES =====> ' + UtilidadesFechas.obtenerDiasHabiles(
	 			DateTime.newInstance(2010, 3, 24),
	 			DateTime.newInstance(2010, 4, 10)));
	 			
	 	System.debug('DIAS HABILES =====> ' + UtilidadesFechas.obtenerDiasHabiles(
	 			DateTime.newInstance(2010, 3, 24),
	 			DateTime.newInstance(2010, 4, 11)));
	 
	 }
	 */

}