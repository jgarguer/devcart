@isTest
private class ADQ_Reasignacion_Test {
	
	@isTest static void  ADQ_ReasignacionSitiosController_test  (){
		List<NE__OrderItem__c> lisinertNEorder = new List<NE__OrderItem__c> ();
		List<NE__OrderItem__c> listaOriginal = new List<NE__OrderItem__c> ();
		Map<String,Programacion__c> mapProgramacion  = new Map<String,Programacion__c> ();
		String programacionSeleccionada = '';
		  ADQ_ReasignacionSitiosController clas01 =  new ADQ_ReasignacionSitiosController ();
			//BI_Pickup_Option__c tipoOption = [SELECT Id FROM BI_Pickup_Option__c where BI_Tipo__c = 'Tipo de oportunidad' and BI_Formato_clave__c = 'Tipo de oportunidad-M?xico---Fijo'];
			
			//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
			Account acc01  = new Account ();
				acc01.Name = 'Cuenta de prueba JMO';
				acc01.BI_Segment__c = 'test';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				//acc01.Raz_n_social__c = 'Cuenta de prueba SA CV';
				acc01.BI_Country__c = Label.BI_Mexico;
			insert acc01;
			

			BI_Punto_de_instalacion__c puntoInstall = new BI_Punto_de_instalacion__c ();
				puntoInstall.BI_Cliente__c = acc01.Id;
				
			//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
			Account accEliminadas  = new Account ();
				accEliminadas.Name = 'CUENTAS ELIMINADAS';
				accEliminadas.BI_Segment__c = 'test';
				accEliminadas.BI_Subsegment_Regional__c = 'test';
				accEliminadas.BI_Territory__c = 'test';
				//accEliminadas.Raz_n_social__c = 'CUENTAS ELIMINADAS';
			insert accEliminadas;  
					
			Factura__c Factura = new Factura__c ();
				Factura.Activo__c = true;
				Factura.Cliente__c = acc01.Id;
				Factura.Fecha_Inicio__c = Date.today();
				Factura.Fecha_Fin__c = Date.today() + 20;
				Factura.Inicio_de_Facturaci_n__c = Date.today();
				Factura.IVA__c = '16%';
				Factura.Sociedad_Facturadora__c = 'GTM';
				Factura.Requiere_Anexo__c = 'SI';
				RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente'];
				Factura.RecordTypeId = tiporegistro.Id;
			insert Factura;
			
				Factura.Sociedad_Facturadora__c = 'PCS';
			update  Factura;
			
			Programacion__c programacion = new Programacion__c ();
				programacion.Factura__c = Factura.Id;
				programacion.CurrencyIsoCode = 'MXN';
				programacion.Sociedad_Facturadora_Real__c = 'GTM';
				programacion.Facturado__c = false;
				programacion.Fecha__c = Date.today();
				programacion.Inicio_del_per_odo__c = Date.today();
				programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacionSeleccionada = programacion.Id;
				mapProgramacion.put(programacion.Id,programacion);
				
			insert programacion;
			
				programacion.Facturado__c =  true;
				programacion.Exportado__c =  true;
				programacion.Folio_Fiscal__c = 'FGTMD00001';
				programacion.Sociedad_Facturadora_Real__c = 'PCS';
				update programacion;
				
			Test.setCurrentPageReference(new PageReference('Page.ADQ_BajaProductos'));
			System.currentPageReference().getParameters().put('fId', factura.Id);
			
			
			Factura__c FacturaDestino = new Factura__c ();
				FacturaDestino.Activo__c = true;
				FacturaDestino.Cliente__c = accEliminadas.Id;
				FacturaDestino.Fecha_Inicio__c = Date.today();
				FacturaDestino.Fecha_Fin__c = Date.today() + 20;
				FacturaDestino.Inicio_de_Facturaci_n__c = Date.today();
				
				FacturaDestino.IVA__c = '16%';
				FacturaDestino.Sociedad_Facturadora__c = 'GTM';
				FacturaDestino.Requiere_Anexo__c = 'SI';
				RecordType tiporegistro2 = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente'];
				FacturaDestino.RecordTypeId = tiporegistro2.Id;
			insert FacturaDestino;
			
			Test.setCurrentPageReference(new PageReference('Page.ADQ_ReasinacionSitios'));
			System.currentPageReference().getParameters().put('fid', Factura.Id);
		
			Programacion__c programacionDestino = new Programacion__c ();
				programacionDestino.Factura__c = FacturaDestino.Id;
				programacionDestino.Sociedad_Facturadora_Real__c = 'GTM';
				programacionDestino.Facturado__c = false;
				programacionDestino.Fecha__c = Date.today();
				programacionDestino.Inicio_del_per_odo__c = Date.today();
				programacionDestino.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacionDestino.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				
				
			insert programacionDestino;
			
			
			List<NE__OrderItem__c> listaProductosReasignar = new List<NE__OrderItem__c>();
			List<Programacion__c> listaProgramaciones = new List<Programacion__c>();
			List<NE__OrderItem__c> listaProductosUnicosReasignar = new List<NE__OrderItem__c>();
			
			NE__Order__c NEORDER =  new NE__Order__c(); 
				insert NEORDER;
			
			NE__OrderItem__c NEInsert  = new NE__OrderItem__c ();
				NEInsert.NE__OrderId__c = NEORDER.id; 
				NEInsert.Factura__c = Factura.Id;
				NEInsert.NE__Qty__c = 1;
				insert NEInsert;
			
			NE__OrderItem__c NEInsert2  = new NE__OrderItem__c ();
				NEInsert2.NE__OrderId__c = NEORDER.id; 
				NEInsert2.Factura__c = Factura.Id;
				NEInsert2.CurrencyIsoCode = 'USD';
				NEInsert2.NE__OneTimeFeeOv__c = 100;
				NEInsert2.NE__RecurringChargeOv__c = 100;
				NEInsert2.Fecha_de_reasignaci_n_a_factura__c = date.today();
				NEInsert2.NE__Qty__c = 1;
				insert NEInsert2;
				
				
				listaProductosReasignar.add(NEInsert);
				listaProductosUnicosReasignar.add(NEInsert);
				listaProductosUnicosReasignar.add(NEInsert2);
			
			Pedido_PE__c  p = new Pedido_PE__c ();
                p.Producto_en_Sitio__c = NEInsert2.Id;
                p.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p.Precio_original__c = 3999.98;
                p.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p.Precio_convertido__c = 8990.998;
                p.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                p.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			p.Lleva_IEPS__c = false; 
                p.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
	        insert p;
	        
	         Factura_OLI__c f = new Factura_OLI__c();
                f.Factura__c = Factura.Id;
               // f.Commercial_Product__c = ne01.NE__ProdId__c; // relacionamos con el producto comercial
                f.Cantidad__c = 1;
                f.CurrencyIsoCode = 'USD';
                f.Descuento__c = 0.2 * 100;
                f.Plazo__c = 36;  
                f.Precio_Unitario__c = 800.80;
                f.Configuration_Item__c = NEInsert2.Id;
                
          	insert f;
		  	clas01.periodoSeleccionado =  '2015-02-1 00:00:00';
			
			 ADQ_ReasignacionSitiosController.WrapperProductos wrapper01= new ADQ_ReasignacionSitiosController.WrapperProductos (NEInsert2,false); 
			clas01.cubre();
			//programacionSeleccionclas01adaclas01.programacionSeleccionada = ;
		  	clas01.cargaEncabezados();
		  	clas01.generaEncabezadoOrigen();
		  	clas01.generaEncabezadoDestino();
		  	clas01.filtroSitios();
		  	clas01.refresh();
		  	clas01.reasignaEquipo();
		  	clas01.guardaSeleccion();
		  	clas01.reasignaProductosRecurrentes();
		  	clas01.validaEncabezadoRecurrente();
		  	clas01.guardaCargosUnicos();
		  	clas01.validaEncabezadoDestino();
		  	clas01.existenCargosUnicos();
		  	clas01.cancelar();
		  	clas01.getEncabezadoOrigen();
		  	clas01.getEncabezadoDestino();
		  	clas01.getListaEncabezadosCuentas();
		  	clas01.getListaSitios();
		  	clas01.getListaProgramaciones();
		  	clas01.getListaPeriodos();
		  	clas01.getListaWrapperProductosOrigen();
		  	clas01.getListaWrapperProductosDestino ();
		  	 
		  	clas01.getshowComboProgramaciones();
		  	
	}

}