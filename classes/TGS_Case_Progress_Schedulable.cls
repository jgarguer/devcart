/*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Schedulable class to wait enough time to asset orders and to prevent queueable chaining limits
    Test Class:
    IN:
    OUT:        
    
    History
    <Date>      <Author>        <Description>
    27/09/2017  Manuel Ochoa    Initial version
    ------------------------------------------------------------*/
    public class TGS_Case_Progress_Schedulable implements Schedulable{
    
        public Case caseToProgress {get;set;}
        
        public void execute(SchedulableContext sc) {
            System.debug(LoggingLevel.ERROR,'Entering schedulable class');
            //TGS_Salesforce_Case_Progress_FAST caseProgressor = new TGS_Salesforce_Case_Progress_FAST(caseToProgress);
            //ID jobID = System.enqueueJob(caseProgressor);
            
            try{
                System.debug(LoggingLevel.ERROR,'Closing CASE');
                Constants.firstRunBilling = true;
                NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
                caseToProgress.Status = 'Closed';
                update caseToProgress;
           	}catch (Exception exc) {// in case of error abort job
                System.debug(LoggingLevel.ERROR,'Aborting schedulable job');
            	System.debug(LoggingLevel.ERROR,sc.getTriggerId());
            	System.abortJob(sc.getTriggerId());            
            }
            
            // Abort the job once the job is queued
            //String scheduledJobName= 'ScheduledJob' + caseToProgress.id;
            //CronTrigger t = [select Id, CronJobDetail.Name from CronTrigger where CronJobDetail.Name = :scheduledJobName LIMIT 1];
            System.debug(LoggingLevel.ERROR,'Aborting schedulable job');
            System.debug(LoggingLevel.ERROR,sc.getTriggerId());
            System.abortJob(sc.getTriggerId());
        }   
    }