/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       20-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public class BI_COL_Generate_OT_ctr 
{
	public BI_COL_Anexos__c fun = new BI_COL_Anexos__c(); 

	public boolean todosMarcados {get;set;}
	public boolean habilitarEnvio {get;set;}
	public boolean showPage{get; set;}
	public Boolean blnpageBlock1{get; set;}
	public Boolean blnpageBuscar{get; set;}

	public String msEnvio {get;set;}
	public String idFUN {get;set;}
	public String Busqueda{get; set;}
	
	public static List<Id> lstIDMsDepuradas{get;set;}
	
	public List<WrapperMS> lstMS;    
	public List<BI_COL_Modificacion_de_Servicio__c> lstMsDepuradas{get;set;}
	/** Paramétros del paginador */
	public Integer pageSize;
	private List<WrapperMS> pageMS;
	private Integer pageNumber;
	private Integer totalPageNumber;
	/***
	* @Author: 	
	* @Company: Avanxo Colombia
	* @Description: En esta variable estan los campos que no se deben duplicar
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public BI_COL_Generate_OT_ctr( ApexPages.StandardController controller )
	{
			idFUN = controller.getId();
			blnpageBlock1 = true;
			pageNumber = 0;
			pageSize = 0;
			totalPageNumber = 0;
			//blnpageBuscar=true;		
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: En esta variable estan los campos que no se deben duplicar
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public void validarFraude()
	{
		list<BI_COL_Anexos__c> ListFraude = [
			SELECT  BI_COL_Contrato__r.Account.BI_Fraude__c, BI_COL_Contrato__r.Account.Id, 
					BI_COL_Contrato__r.Account.BI_No_Identificador_fiscal__c, BI_COL_Estado__c 
			FROM    BI_COL_Anexos__c  
			WHERE   Id =: idFUN];
		
		list<ID> lstID = new list<ID>();
		set<String> permiSet=BI_COL_ManagementMashup_ctr.getPermisionSetUsuario(userinfo.getuserid());
		System.debug('\n\n'+permiSet+'\n\n');

		for( BI_COL_Anexos__c f : ListFraude )
			lstID.add( f.BI_COL_Contrato__r.Account.Id );

		map<String, list<String> > mpConsulta = BI_COL_validationFraud_cls.Consultar_Fraude( lstID );

		list<String> lstRespuesta = mpConsulta.get( ListFraude[0].BI_COL_Contrato__r.Account.BI_No_Identificador_fiscal__c );

		if( !ListFraude.isEmpty() && lstRespuesta[0] == '1' )
		{
			blnpageBlock1 = false;
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, Label.BI_COL_lblValidacionFraude + '  ' + lstRespuesta[1] ) );
		}
		else if(!permiSet.contains('BI_COL_Jefe_de_Ventas') && !permiSet.contains('BI_COL_Ejecutivo_de_Cobros'))
		{
			blnpageBuscar=true;
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'No tiene permisos para enviar Ordenes de trabajo' ) );
		}
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Acción para enviar ordenes de trabajo
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public PageReference action_envioOT()
	{
		List<String> lstIdMS = new List<String>();
		List<BI_COL_Modificacion_de_Servicio__c> lstMSUpdate = new List<BI_COL_Modificacion_de_Servicio__c>(); 
		List<BI_COL_Modificacion_de_Servicio__c> lstMSUpdateNoViajanTRS = new List<BI_COL_Modificacion_de_Servicio__c>(); 
		List<BI_COL_Modificacion_de_Servicio__c> lstMSSeleccionadaAlta = new List<BI_COL_Modificacion_de_Servicio__c>(); 
		String Mensaje = '';
		String strMensajeContrato = '';
		Boolean band = false;
		Boolean control = true;
		Decimal decTotalServiciosMS = 0;
		msEnvio = '';
		//Calculamos el total de las MS seleccionadas
		set<ID> dsNoviajanTrs = new set<ID>();

		if( this.lstMS != null )
		{ 
			for( WrapperMS wMS : this.lstMS )
			{
				if( wMS.seleccionado )
				{
					if( wMS.ms.BI_COL_Medio_Preferido__c != null )
					{
						set<String> noViajanTrs = new set<String>();
						//JATC pilas!!!!!!!!!! Campo de producto que falta crear
						System.debug('\n ===> !noViajanTrs.contains( wMS.ms.BI_COL_Clasificacion_Servicio__c )'+!noViajanTrs.contains( wMS.ms.BI_COL_Clasificacion_Servicio__c ));
						if( wMS.ms.BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c /*&& !noViajanTrs.contains( wMS.ms.BI_COL_Clasificacion_Servicio__c ) */)
						{
							lstIdMS.add( wMS.ms.Id );
							wMS.ms.BI_COL_Estado_orden_trabajo__c = 'Solicitado';
							lstMSUpdate.add( wMS.ms );
							System.debug('\n lstMSUpdate=====>>>'+lstMSUpdate);
						}
						else
						{
							//Cambiamos los campos necesarios a los tipos de clacificacion que no viajan a trs
							wMS.ms.BI_COL_Estado_orden_trabajo__c = 'En Desarrollo';
							wMS.ms.BI_COL_Fecha_liberacion_OT__c = System.now();
							wMS.ms.BI_COL_Estado__c = 'Enviado';
							dsNoviajanTrs.add( wMS.ms.BI_COL_Codigo_unico_servicio__c );
							lstMSUpdateNoViajanTRS.add( wMS.ms );
						}
						msEnvio += wMS.ms.Name + ', ';
						//Valida que sea de ALTA para crea las DS y Asocia la MS a DS creada
						if( wMS.ms.BI_COL_Clasificacion_Servicio__c == 'ALTA' )
							lstMSSeleccionadaAlta.add( wMS.ms );
					}
					else
					{
						//Mensaje += 'La ' + wMS.ms.BI_COL_Codigo_unico_servicio__r.Name + ' - ' + wMS.ms.Name + Label.BI_COL_lblMSSinMedioSucursal;
						//band = true;
					}
					system.debug('Montos a validar : ===========\n\n'+wMS.ms.BI_COL_Monto_ejecutado__c +'\n\n\n' +wMS.ms.BI_COL_TRM__c);
					decTotalServiciosMs +=wMS.ms.BI_COL_TRM__c!=null?wMS.ms.BI_COL_Monto_ejecutado__c / wMS.ms.BI_COL_TRM__c:wMS.ms.BI_COL_Monto_ejecutado__c;
					System.debug('\n =======================decTotalServiciosMs=====>>>'+decTotalServiciosMs);
				}
			}
			
			System.debug('\n dsNoviajanTrs======>>>'+dsNoviajanTrs);
			if( !dsNoviajanTrs.isEmpty() )
			{
				ActualizarDS( dsNoviajanTrs );
			System.debug('\n dsNoviajanTrs11======>>>'+dsNoviajanTrs);
			}
			else if(!lstMSUpdate.isEmpty())
			{
				lstMSDepuradas = validarMsEnvio(lstMSUpdate);
				System.debug('\n dsNoviajanTrs22======>>>'+lstMSDepuradas);
			}
			
			if( this.fun != null )
			{
				if( this.fun.BI_COL_Contrato__c != null )
				{
					//if( this.fun.BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c != null  && this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c != null && this.fun.BI_COL_Contrato__r.BI_Indefinido__c!='No')
					//{
						Decimal decSaldoContrato =(this.fun.BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c != null  && this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c != null && this.fun.BI_COL_Contrato__r.BI_Indefinido__c!='No')?this.fun.BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c - this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c:0;
						//Validamos que el valor de las MS seleccionadas esté cubierto con el saldo del contrato
						System.debug('\n\n=================== decSaldoContrato: '+decSaldoContrato+' decTotalServiciosMs: '+decTotalServiciosMs+'\n\n');
						if( ( decTotalServiciosMs <= decSaldoContrato && control ) || this.fun.BI_COL_Contrato__r.BI_Indefinido__c!='Si')
						{
							if( band )
							{
								ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, Mensaje ) );
								return null;
							}
							//Cantidad de bloqueos
							Integer iBloqueo = 0;
							map<Integer, list<String> > mapBloques = new map<Integer, list<String>>();
							Integer intercalaBlq = 0;
							Integer sizeLst = 0;
							list<String> regMS = new list<String>();
					
							for( String idMS : lstIdMS )
							{
								regMS.add( idMS );
								intercalaBlq++;
								sizeLst++;
								if( intercalaBlq == 10 || sizeLst == lstIdMS.size() )
								{
									mapBloques.put( iBloqueo, regMS );
									iBloqueo++;
									intercalaBlq = 0;
									regMS = new List<String>();
								}
							}
							
							try
							{
								if( lstMSDepuradas != null && lstMSDepuradas.size()>0 && !Test.isRunningTest() )
									update lstMSDepuradas;
								System.debug('\n\n lstMSUpdateNoViajanTRS '+lstMSUpdateNoViajanTRS+'\n\n');
								if( lstMSUpdateNoViajanTRS.size() > 0 && !Test.isRunningTest() )
									update lstMSUpdateNoViajanTRS;
								System.debug('=============decTotalServiciosMs  '+decTotalServiciosMs);
								this.actualizarPresupuesto( decTotalServiciosMs );
							}
							catch( DMLException e )
							{
								System.debug('\n\n Error dml Exception');
								ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblErrorActualizacionBD + ' ' + e.getDmlMessage(0) ) );
								return null;  
							}

							List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = getMSSolicitudServicio( this.fun.Id );
				
							if( lstMSConsulta == null || lstMSConsulta.size() == 0 )
							{
								msEnvio = Label.BI_COL_lblMSEnviadas + ' ' + msEnvio;
								lstMS = new List<WrapperMS>();
								pageMS = new List<WrapperMS>();
								ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO, msEnvio ) );
								return null;
							}
							else
							{
								//Llenar listado de MS Wrapper que se muestran en pantalla
								lstMS = new List<WrapperMS>(); 
								for( BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta )
								{
									WrapperMS wMS = new WrapperMS( ms, false );
									this.lstMS.add( wMS );
									system.debug('\n wMS============>>>>'+wMS);
								}
									system.debug('\n lstMS============>>>>'+lstMS);
								//Inicializar valores del paginador
								pageNumber = 0;
								totalPageNumber = 0;
								pageSize = 10;
								ViewData();
							}    
							//Actualizamos los valores del presupuesto del contrato
							ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, Label.BI_COL_lblActualizacionBD ) );
						}
						else
							ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblContratoPresupuestoExcedido ) );
					//}
					//else
					//	ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblContratoSinPresupuestoMontoEjecutado ) );
				}
				else
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblFUNSinContrato ) );
			}
			else
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblMSSinFUN ) );
		}
		return null;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public PageReference action_Regresar()
	{
		PageReference pageRef = new PageReference( '/' + idFUN );
		pageRef.setRedirect( true );
		return pageRef;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	@Future (callout=true)
	public static void ActualizarDS( set<ID> activarDs )
	{
		list<BI_COL_Descripcion_de_servicio__c> actDS = [
			SELECT  Id, BI_COL_Estado_de_servicio__c 
			FROM    BI_COL_Descripcion_de_servicio__c 
			where   Id =: activarDs ];

		list<BI_COL_Descripcion_de_servicio__c> mydsDS = new list<BI_COL_Descripcion_de_servicio__c>(); 

		for( BI_COL_Descripcion_de_servicio__c newds : actDS )
		{
			newds.BI_COL_Estado_de_servicio__c = 'ACTIVA';
			mydsDS.add( newds );
		}

		update mydsDS;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: método encargado de validar si las ms a enviar tienen alguna ms de la misma oportunidad en estado de enviado, en caso afirmativo, esta ms no se envia al SW
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static List<BI_COL_Modificacion_de_Servicio__c> validarMsEnvio(List<BI_COL_Modificacion_de_Servicio__c> msEnviar)
	{
		set<Id> idDRSInvolucradas = new set<Id>();
		List<Id> idMsUpdate = new List<Id>();
		String msm = '';
		/**** Identificar las oportunidades involucradas en las ms a enviar ******/
		System.debug('\n msEnviar============>>>'+msEnviar);
		for( BI_COL_Modificacion_de_Servicio__c ms : msEnviar )
		{
			if( !idDRSInvolucradas.contains( ms.BI_COL_Codigo_unico_servicio__c ) && ms.BI_COL_Clasificacion_Servicio__c != 'SERVICIO INGENIERIA')
				idDRSInvolucradas.add( ms.BI_COL_Codigo_unico_servicio__c );
		}

		List<BI_COL_Modificacion_de_Servicio__c> msDepuradas = new List<BI_COL_Modificacion_de_Servicio__c>();
		set<Id> idDRSVetadas = new set<Id>();

		for( BI_COL_Modificacion_de_Servicio__c ms : msEnviar )
		{
			msDepuradas.add( ms );
		System.debug('\n ms.Id============>>>'+ms.Id);
			idMsUpdate.add( ms.Id );
		}
		System.debug('\n idMsUpdate============>>>'+idMsUpdate);
		envioDatosSISGOT( idMsUpdate );

		return msDepuradas; //msEnviar
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public void actualizarPresupuesto( Decimal decTotalServiciosMs )
	{  
		System.debug('\n\n==================> decTotalServiciosMs '+decTotalServiciosMs);
			
			

		System.debug('\n\n==================> fun '+fun);
		//if(Test.isRunningTest()){
		//		 fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c = 5.00;
		//	}	
		Decimal montoEjecutado = fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c == null ? 0 : fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c; 
		System.debug('\n\n==================> montoEjecutado '+montoEjecutado);
		System.debug('\n\n==================>>>>>>>>>> fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c '+FUN.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c);
		fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c = montoEjecutado + decTotalServiciosMs;
		System.debug('\n\n==================>>>>>>>>>> fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c '+fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c);

		Contract contr = [
			select  BI_COL_Monto_ejecutado__c , BI_COL_Saldo_contrato__c 
			from    Contract 
			where   Id =: fun.BI_COL_Contrato__c ];
		
	
		contr.BI_COL_Monto_ejecutado__c = montoEjecutado + decTotalServiciosMs;
		
		//
		if( fun.BI_COL_Estado__c != 'Liberado' )
		{
			fun.BI_COL_Estado__c = 'Liberado';
			update fun;
		}
		
		Integer porcentaje = this.fun.BI_COL_Contrato__r.BI_Indefinido__c!='Si'&&fun.BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c!=null?Math.round( fun.BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c * .1 ):0;
		
		if( contr.BI_COL_Monto_ejecutado__c > porcentaje )
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, Label.BI_COL_MenorOIgual10 ) );     
		update contr;   

	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Clase Wrapper que administra la selección de registros
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	@Future( callout = true )
	public static void envioDatosSISGOT( List<String> lstIdMS )
	{
		List<BI_COL_Modificacion_de_Servicio__c> lstMS = [
			select  Id, Name, BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Estado_orden_trabajo__c, BI_COL_Oportunidad__c
			from    BI_COL_Modificacion_de_Servicio__c 
			where   Id in :lstIdMS];

			System.debug('\n lstMS==========>>>'+lstMS);
		try
		{
			BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS( lstMS );
		}
		catch(Exception e)
		{
			System.debug('\n\n##Error: Se presento un error generando las ordenes de trabajo: ' + e.getMessage() + '\n\n');
		}
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Obtiene el listado de MS asociadas a la Solicitud de Servicio
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String idFUN)
	{
		// Obtenemos la lista de MS asociadas al FUN identificado con el id recibido como parámetro
		// Scardona 08-03-2012 Se Agregan campos en la consulta para el Requerimiento Backup testing.
		String consultaSOQL = 'SELECT Id, Name, BI_COL_Oportunidad__c, BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c, BI_COL_Clasificacion_Servicio__c , '+
			' BI_COL_Producto__r.Name, BI_COL_Fecha_Instalacion_Comprometida__c, '+
			' BI_COL_Codigo_unico_servicio__r.Name, '+
			' BI_COL_Estado__c, BI_COL_Estado_orden_trabajo__c,BI_COL_Medio_Preferido__c, BI_COL_Sucursal_Origen__r.BI_COL_Codigo_sisgot__c,'+
			' CurrencyIsoCode, BI_COL_Total_servicio__c, BI_COL_TRM__c, BI_COL_Monto_ejecutado__c, '+
			' BI_COL_Altura_donde_instalara__c, BI_COL_Ancho_banda_canal_cliente__c, ' +
			' BI_COL_Autoconsumo__c, BI_COL_Bloqueado__c, BI_COL_Bloqueo__c, ' +
			' BI_COL_Cantidad__c, BI_COL_Cliente__c, BI_COL_Codigo_de_acceso__c, ' +
			' BI_COL_DS_Medio_Acceso__c, BI_COL_Enrutamiento_por_Tiempo__c, ' +
			' BI_COL_Fecha_Final_Servicio__c, BI_COL_Fecha_Inicio_Servicio__c, ' +
			' BI_COL_Linea_Piloto__c, '+//BI_COL_Nombre_del_dominio__c,  +
			' BI_COL_Numero_telefonico__c, BI_COL_Rack__c,  ' +
			' BI_COL_Site_survey__c, BI_COL_Producto__c, BI_COL_Sucursal_Origen__c, ' +
			' BI_COL_Producto__r.NE__ProdId__r.Name, '+
			' BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c' +
			' FROM BI_COL_Modificacion_de_Servicio__c '+
			' WHERE BI_COL_Estado__c = \'Pendiente\''+
			' AND BI_COL_FUN__c = \''+idFUN+'\''+
			//' AND BI_COL_Estado_orden_trabajo__c IN (\'Fallido\',\'\',\'Fallido Conexión\')'+
			' AND BI_COL_Estado_orden_trabajo__c IN (\'Fallido\',\'Fallido Conexion\',\'\')'+
			' AND BI_COL_Oportunidad__r.StageName =\'F1 - Closed Won\'';
			//' AND BI_COL_Oportunidad__r.StageName =\'F1 - Ganada\'';

		if( Busqueda != null && Busqueda != '' )
		{
			String dsBuscar='';
			
			if( Busqueda.contains(',') )
			{
				List<String> lstDSB=Busqueda.split(',');
				
				for(String ls:lstDSB)
					dsBuscar+='\''+ls+'\',';
				
				dsBuscar = dsBuscar.subString( 0, dsBuscar.length() - 1 );
			}
			else
				dsBuscar='\''+Busqueda+'\'';
			
			consultaSOQL+=' AND Name IN ('+dsBuscar+')'; 
		}
	  
	  
		consultaSOQL += ' ORDER BY Name ASC limit 200';
		List<BI_COL_Modificacion_de_Servicio__c> lstMS = new List<BI_COL_Modificacion_de_Servicio__c>(); 
		system.debug('\n\n##consultaSOQL: '+ consultaSOQL +'\n\n');
		lstMS = Database.query( consultaSOQL );

		return lstMS;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public PageReference ViewData()
	{
		totalPageNumber = 0;
		BindData(1);
		return null;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Posiciona el registro segun el numero de pagina
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public void BindData(Integer newPageIndex)
	{
		try
		{
			pageMS = new List<WrapperMS>();
			Transient Integer counter = 0;
			Transient Integer min = 0;
			Transient Integer max = 0;

			if (newPageIndex > pageNumber)
			{
				min = pageNumber * pageSize;
				max = newPageIndex * pageSize;
			}
			else
			{
				max = newPageIndex * pageSize;
				min = max - pageSize;
			}

			for(WrapperMS wMS : lstMS)
			{
				counter++;
				if( counter > min && counter <= max)
					pageMS.add( wMS );
			}

			pageNumber = newPageIndex;

			if( pageMS == null || pageMS.size() <= 0 )
				ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO, Label.BI_COL_lblDatosNoDisponiblesVista ) );
		}
		catch( Exception ex )
		{
			ApexPages.addmessage( new ApexPages.message( ApexPages.severity.FATAL, ex.getMessage() ) );
		}  
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Clase Wrapper que administra la selección de registros
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public class WrapperMS
	{
		public boolean seleccionado {get;set;}
		public BI_COL_Modificacion_de_Servicio__c ms {get;set;}

		public WrapperMS( BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado)
		{
			this.ms = ms;
			this.seleccionado = seleccionado;
		}
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Clase Wrapper que administra la selección de registros
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public void Buscar()
	{
		this.showPage = false;
		//Obtenemos el id del FUN enviado como parámetro
		if( idFUN != null )
		{
			this.showPage = true;
			//Obtenemos la lista de FUN asociados al id recibido como parámetro
			List<BI_COL_Anexos__c> lstFUN =
			[   SELECT  BI_COL_Contrato__c, Id, Name, 
						BI_COL_Contrato__r.ContractNumber, 
						BI_COL_Contrato__r.Account.Id,
						BI_COL_Contrato__r.Account.AccountNumber, 
						BI_COL_Contrato__r.Account.Name, 
//20150918 PLL 			BI_COL_Contrato__r.Account.BillingAddress, 
						BI_COL_Contrato__r.Account.Phone, 
						BI_COL_Contrato__r.ContractTerm,
						BI_COL_Contrato__r.StartDate, 
						BI_COL_Contrato__r.Status, 
						BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c,
						BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c, 
						BI_COL_Contrato__r.BI_COL_Saldo_contrato__c,
						BI_COL_Contrato__r.BI_COL_Duracion_Indefinida__c,
						BI_COL_Contrato__r.Name, 
						BI_COL_Contrato__r.BI_COL_Cuantia_Indeterminada__c,
						BI_COL_Contrato__r.BI_Indefinido__c,
						BI_COL_Estado__c
				FROM    BI_COL_Anexos__c
				WHERE   Id =: idFUN
				LIMIT 1 ];
			//Validamos que la lista tenga al menos un FUN
			if( lstFUN.size() > 0 )
			{
				//Obtenemos el objeto FUN que se obtuvo en el query
				this.fun = lstFUN.get(0);
				if( this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c == null )
					this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c = 0;
				//Obtenemos la fecha de hoy
				Datetime dtNow = System.now();
				//Guardamos la fecha en la que se inicia el contrato
				Datetime dtStartContract = this.fun.BI_COL_Contrato__r.StartDate;
				if( dtStartContract != null )
				{
					//Creamos una fecha para guardar el término del contrato
					Datetime dtContractEnd = Datetime.newInstanceGmt( dtStartContract.yearGmt(),
						dtStartContract.monthGmt(), dtStartContract.dayGmt(), dtStartContract.hourGmt(),
						dtStartContract.minuteGmt(), dtStartContract.secondGmt() );

					//if( this.fun.BI_COL_Contrato__r.ContractTerm != null )
					//{
						//Determinamos la fecha en la que se termina el contrato (sumamos los meses)
						dtContractEnd = this.fun.BI_COL_Contrato__r.ContractTerm!=null && this.fun.BI_COL_Contrato__r.BI_Indefinido__c!='No'?dtContractEnd.addMonths( this.fun.BI_COL_Contrato__r.ContractTerm ):System.today()+180;                  
						//Validamos que el estado del contrato sea Firmado Activo y no se haya vencido
						if( dtContractEnd >= dtNow )
						{	

							System.debug('===========\n\n '+this.fun.BI_COL_Contrato__r.Status + '\n\n\n====================');
							if( this.fun.BI_COL_Contrato__r.Status.equals( 'Firmado Activo' ) || this.fun.BI_COL_Contrato__r.Status.equals('Activated') )
							{
							System.debug('===========\n\n Entro\n\n\n====================');
								//Obtenemos las MS asociadas al FUN
								List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta =   getMSSolicitudServicio( this.fun.Id );
								System.debug('\n lstMSConsulta========>>>>'+lstMSConsulta);
								if( lstMSConsulta == null || lstMSConsulta.size() == 0 )
									ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblEstadoOportunidadMSLiberar ) );
								else
								{
									//Llenar listado de MS Wrapper que se muestran en pantalla
									lstMS = new List<WrapperMS>(); 
									for( BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta )
									{
										WrapperMS wMS = new WrapperMS( ms, false );
										this.lstMS.add(wMS);
									}
									//Inicializar valores del paginador
									pageNumber = 0;
									totalPageNumber = 0;
									pageSize = 50;
									ViewData();
								}
							}
							else
								ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblContratoEstadoNoFirmadoActivo ) );
						}
						else
							ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblContratoCaducado ) );    
					//}
					//else
					//	ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblContratoSinDuracion ) );
				}
				else
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblContratoSinFechaInicio ) );
			}
			else
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblIdInvalido ) );
		}
		else
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblEspecificarFUN ) );
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Acción para seleccionar todos los registros de MS
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public PageReference action_seleccionarTodos()
	{
		if( this.lstMS != null )
		{
			for( WrapperMS wMS : this.lstMS )
				wMS.seleccionado = this.todosMarcados;
		}
		return null;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Acción para seleccionar todos los registros de MS
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public List<WrapperMS> getLstMS()
	{
		if( pageMS != null && pageMS.size() > 0 )
			habilitarEnvio = false;
		else
			habilitarEnvio = true;
		return pageMS;
	}
	//IMPLEMENTACION PAGINADOR
	public Integer getPageNumber()
	{
		return pageNumber;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}

	public Boolean getPreviousButtonEnabled()
	{
		return !(pageNumber > 1);
	}

	public Boolean getNextButtonDisabled()
	{
		if( this.lstMS == null )
			return true;
		else
			return ( ( pageNumber * pageSize ) >= lstMS.size() );
	}

	public Integer getTotalPageNumber()
	{
		if( totalPageNumber == 0 && this.lstMS != null )
		{
			totalPageNumber = this.lstMS.size() / pageSize;
			Integer mod = this.lstMS.size() - ( totalPageNumber * pageSize );
			if( mod > 0 )
				totalPageNumber++;
		}
		return totalPageNumber;
	}

	public PageReference nextBtnClick() 
	{
		if( pageNumber != null )
			BindData( pageNumber + 1 );
		return null;
	}

	public PageReference previousBtnClick()
	{
		if( pageNumber != null )
			BindData( pageNumber - 1 );
		return null;
	}
}