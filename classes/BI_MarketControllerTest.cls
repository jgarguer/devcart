/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class BI_MarketControllerTest {

    @isTest(SeeAllData=true) static void myUnitTest() {

      BI_TestUtils.throw_exception = false;

      TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
      if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

          userTGS.TGS_Is_BI_EN__c = true;
          userTGS.TGS_Is_TGS__c = false;
          
          if(userTGS.Id != null){
              update userTGS;
          }
          else{
              insert userTGS;
          }
      }

      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);


        // TO DO: implement unit test
        // 
        // Constructor Catch
         try{
            BI_MarketController mc0 = new BI_MarketController(null);
        }catch(Exception e){}
        String compareUrl;
        Boolean lvvar;
        String                                   redSum;             
        Boolean                                  mobile;      
        String                                   catName;
        String                                   psess;
        

       
        NE.NewConfigurationController ncc;
          ncc= new NE.NewConfigurationController();
 
        try{
            Datetime myT = datetime.newInstance(2012, 5, 1, 12, 30, 2);
            
            NE__Catalog_Header__c ch=new NE__Catalog_Header__c(NE__Name__c='myUnitTest');
            insert ch;
            
            NE__Catalog__c ct = new NE__Catalog__c(Name='myUnitTest', NE__StartDate__c=myT, NE__Catalog_Header__c=ch.id);
            insert ct;
            
            system.debug(System.LoggingLevel.WARN,'MRIG ct.Id: '+ct.Id);
            
            NE__Catalog_Category__c categoria = new NE__Catalog_Category__c(Name = ct.Name,NE__CatalogId__c = ct.Id);
            insert categoria;
            
            NE__Commercial_Model__c commodel = new NE__Commercial_Model__c(Name = ct.Name, NE__Catalog_Header__c = ct.NE__Catalog_Header__c,
            NE__ProgramName__c = NE.JS_RemoteMethods.SFDC_HTMLENCODE(ct.Name.replaceAll(' ','_')));
            insert commodel;
            
            NE__Catalog_Category__c cc=new NE__Catalog_Category__c(Name='testcc',NE__CatalogId__c=ct.id, NE__Parent_Category_Name__c = categoria.Id);
            insert cc;
            
            NE__Product__c prod1=new NE__Product__c(Name='prod1');
            insert prod1;
            NE__Product__c prod2=new NE__Product__c(Name='prod2');
            insert prod2;
            NE__Product__c prod3=new NE__Product__c(Name='prod3');
            insert prod3;
            NE__Product__c prod4=new NE__Product__c(Name='prod4');
            insert prod4;       
            NE__Product__c prod5=new NE__Product__c(Name='prod5');
            insert prod5;
            
            List<NE__Catalog_Item__c> lisct = new List<NE__Catalog_Item__c>();  
            NE__Catalog_Item__c item= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item;
            lisct.add(item);
            NE__Catalog_Item__c item2= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item2;
            lisct.add(item2);
            NE__Catalog_Item__c item3= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item3;
            lisct.add(item3);
            NE__Catalog_Item__c item4= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item4;
            lisct.add(item4);
            NE__Catalog_Item__c item5= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod5.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item5;
            lisct.add(item5);
            
            NE__Promotion__c prom= new NE__Promotion__c (Name = 'Prova', NE__Start_Date__c = myT, NE__Catalog_Id__c = ct.id);
            insert prom;
            
      // 20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
      //                                             BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c   
            Account acc = new Account (Name = 'Account Classe di Test', BI_Subsector__c = 'Banca',
            BI_Sector__c = 'Industria',
            BI_Territory__c = 'test');
            insert acc;
            
            NE__Contract_Header__c contractHeader = new NE__Contract_Header__c(NE__name__c='TestCCloneHeader');
            insert contractHeader;
            NE__Contract__c contractToUse   =   new NE__Contract__c(NE__Contract_Header__c = contractHeader.id, NE__Version__c = 1, NE__Start_Date__c = DateTime.Now()-5000, NE__Status__c = 'Active');
            insert contractToUse;
            NE__Contract_Line_Item__c cLine =   new NE__Contract_Line_Item__c(NE__Base_OneTime_Fee__c = 10, NE__Base_Recurring_Charge__c = 10, NE__Commercial_Product__c = prod5.id, NE__Contract__c = contractToUse.id);
            insert cLine;
            NE__Contract_Account_Association__c cAssoc  =   new NE__Contract_Account_Association__c(NE__Contract_Header__c = contractHeader.id, NE__Account__c = acc.id);
            insert cAssoc;
            
            RecordType recordTypeId= [SELECT CreatedById,DeveloperName,Id,Name,NamespacePrefix,SobjectType 
            FROM RecordType 
            WHERE (SobjectType=:'NE__Order__c' or SobjectType=:'Order__c') AND Name='Opty' LIMIT 1];
            
            String probab = Opportunity.BI_Probabilidad_de_exito__c.getDescribe().getPicklistValues().get(0).getValue();
            String curr= Opportunity.CurrencyIsoCode.getDescribe().getPicklistValues().get(0).getValue();
            String stage= Opportunity.stageName.getDescribe().getPicklistValues().get(0).getValue();
            String country= Opportunity.BI_Country__c.getDescribe().getPicklistValues().get(0).getValue();
            
            try{

              Opportunity optyTest = new Opportunity(Name='OpportunityTest',AccountId=acc.id, CloseDate =Date.today(),StageName=stage, BI_Probabilidad_de_exito__c=probab,CurrencyIsoCode=curr,BI_Country__c=country,  BI_Duracion_del_contrato_Meses__c=12,BI_Plazo_estimado_de_provision_dias__c=12);
              insert optyTest;
              System.debug('opty created');
              NE__Order__c ord = new NE__Order__c (NE__AccountId__c = acc.Id,  RecordType=recordTypeId, NE__OptyId__c=optyTest.Id, NE__Contract_Account__c=cAssoc.id, NE__Contract_Header__c=contractHeader.id);
              insert ord;
              System.debug('ord.Id: '+ord.Id);
              System.debug('ord.NE__AssetEnterpriseId__c: '+ord.NE__AssetEnterpriseId__c);
              System.debug('ord.LastModifiedDate: '+ord.LastModifiedDate);
              ord=[SELECT id,LastModifiedDate,NE__AssetEnterpriseId__c FROM NE__Order__c LIMIT 1]; 
              ord.NE__AssetEnterpriseId__c=ord.Id;
              
              System.debug('ord.LastModifiedDate: '+ord.LastModifiedDate);

              System.debug('ord.NE__AssetEnterpriseId__c: '+ord.NE__AssetEnterpriseId__c);
              ncc.ord.LastModifiedDate=ord.LastModifiedDate;
              update ord;
              ncc.ord.LastModifiedDate=ord.LastModifiedDate;
              
              NE__Item_Header__c IH = new NE__Item_Header__c(Name = 'IHTPCmut3', NE__Catalog__c = ct.Id);
              insert IH;
              NE__Catalog_Item__c catit=new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=categoria.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Base_OneTime_Fee__c=15,NE__BaseRecurringCharge__c=30, NE__Type__c = 'Product', NE__Item_Header__c = IH.Id);
              insert catit;
              //catit = [SELECT Catalog_Id__c, Catalog_Category_Name__c, ProductId__c, Max_Qty__c,Base_OneTime_Fee__c, BaseRecurringCharge__c, Type__c, Item_Header__c, Catalog_Id__c FROM Catalog_Item__c WHERE Id:= catit.Id]
              NE__Catalog_Item__c catitCompl=new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=categoria.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Base_OneTime_Fee__c=15,NE__BaseRecurringCharge__c=30, NE__Type__c = 'Root', NE__Item_Header__c = IH.Id, NE__Parent_Catalog_Item__c = catit.Id,NE__Version__c=1);
              insert catitCompl;
              NE__Catalog_Item__c catitCat = new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=categoria.id, NE__Root_Catalog_Item__c = catitCompl.Id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Base_OneTime_Fee__c=15,NE__BaseRecurringCharge__c=30, NE__Type__c = 'Category', NE__Item_Header__c = IH.Id, NE__Parent_Catalog_Item__c = catitCompl.Id);
              insert catitCat;
              NE__Catalog_Item__c catitCat2 = new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=categoria.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Base_OneTime_Fee__c=15,NE__BaseRecurringCharge__c=30, NE__Type__c = 'Child-Product', NE__Item_Header__c = IH.Id, NE__Parent_Catalog_Item__c = catitCat.Id);
              insert catitCat2;
              IH.NE__Catalog_Item__c = catit.Id;
              update IH;
      
              NE__Promotion_Item__c promItTest = new NE__Promotion_Item__c(NE__Promotion_Id__c=prom.Id, NE__Catalog_Item__c=catit.Id);
              insert promItTest;
              
              NE__OrderItem__c oit=new NE__OrderItem__c(NE__CatalogItem__c=catit.Id,NE__OrderId__c=ord.id,NE__PromotionId__c=prom.id,NE__isPromo__c=true,NE__ProdId__c=prod1.id,NE__Qty__c=1,NE__BaseOneTimeFee__c=5,NE__BaseRecurringCharge__c=10,NE__OneTimeFeeOv__c=15,NE__RecurringChargeOv__c=20);
              oit.NE__AssetItemEnterpriseId__c=ord.Id;
              insert oit;
              NE__OrderItem__c oitchild=new NE__OrderItem__c(NE__CatalogItem__c=catitCat2.Id,NE__Root_Order_Item__c=oit.Id,NE__OrderId__c=ord.id,NE__ProdId__c=prod2.id,NE__Qty__c=1,NE__BaseOneTimeFee__c=5,NE__BaseRecurringCharge__c=10,NE__OneTimeFeeOv__c=15,NE__RecurringChargeOv__c=20);
              oitchild.NE__AssetItemEnterpriseId__c=ord.Id;
              insert oitchild;
              
              NE.Item myCartItem;
              NE.PromotionItem pItemTest;
              NE.Promotion promoTest;
              
              BI_MigrationHelper.disableBypass(mapa);

              Pagereference p = Page.BI_Market;
              p.getParameters().put('ordId', ord.Id);
              p.getParameters().put('simulator','true');
              p.getParameters().put('checkadv','true');
              //AP change to create Patch
              Test.setCurrentPageReference(p);
              
              Test.startTest();
              
              List<NE__DocumentationCharacterist__c>   listOfCharacteristics;
              
              
              for (NE__Catalog_Item__c ctl : lisct){
                  ncc.catalogItems.put(ctl.NE__productId__c, ctl);
              }
              
              try
              {
                  myCartItem = new NE.Item(item,null,null,null,null);
                  myCartItem.catalogItemToOrderItem();
                  pItemTest = new NE.PromotionItem (promItTest, myCartItem);
                  promoTest = new NE.Promotion(prom, new NE.PromotionItem[]{pItemTest});
                  ncc.cartPromotions.add(promoTest);
                  ncc.promoCartMap.put(prom.Id, promoTest);
                  p.getParameters().put('ordId', myCartItem.orderItem.Id);
              }
              catch(Exception e){system.debug(System.LoggingLevel.ERROR,'MRIG: '+e);}
            }catch(Exception e){}
            BI_MarketController mc1 = new BI_MarketController(ncc);
            try{
                compareUrl = mc1.marketCompareUrl;
            }catch(Exception e){}
            lvvar=mc1.listViewVariable;
            redSum=mc1.redirectFromSummary;
            mobile=mc1.isMobileAgent;
            catName=mc1.catName;
            psess=mc1.paramsess;
         
            mc1.SelectCatalogCustomItem();
            mc1.retrieveCharacteristics();
            mc1.newComplexMarketSave();
            mc1.startConfigurator();
            mc1.configureComplexProduct();
            mc1.addItemsToCampareCustom();
            mc1.addItemsToCompareComplex();
            mc1.startConfigurator();
            mc1.configureComplexProduct();
            mc1.newCheckout();
            
            mc1.commProdContext=prod1.id;
            mc1.retrieveCharacteristics();
            
            mc1.contextCommercialProduct=null;
            mc1.retrieveCharacteristics();
            
            mc1.wannaSelectCatalog='true';
            mc1.reloadCatalogVars();
            
            mc1.wannaSelectCatalog='false';
            mc1.newConf.selectedCategory=categoria.id;
            mc1.reloadCatalogVars();
            
            try{
                mc1.complexMarketSave();
            }catch(Exception e){}
            try{
                mc1.complexMarketCancel();
            }catch(Exception e){}
            
            try{
                mc1.newConf.catXmlAttachment=null;
                mc1.configureComplexProduct();
            }catch(Exception e){}
            
            
            
            
            mc1.newConf.accId=null;
            mc1.newConf.servAccId=null;
            mc1.newConf.billAccId=null;
            mc1.newConf.orderId=null;
            mc1.retrieveAccountData();

            Test.stopTest();
        }
        catch(Exception e){}
    }
    
}