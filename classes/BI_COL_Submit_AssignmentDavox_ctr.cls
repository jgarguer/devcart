/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-04-14      Manuel Esthiben Mendez Devia (MEMD)     Cloned Class
*************************************************************************************************************/
public with sharing class BI_COL_Submit_AssignmentDavox_ctr  
{

	public string id{get;set;}
	public list<BI_COL_Modificacion_de_Servicio__c> ms{get;set;}
	public list<wrapperCesionDavox> listwr{get;set;}
	public Boolean siSeEnvia{get;set;}
	public string Dsr='';
	public string fechaI='';
	public string fechaF='';
	public string cuenta='';
	public string duracion='';
	public string moneda='';
	public String strIdsucursaFact='';
	public String strDirSinEspacio='';
	public String strBarrio='';
	public String strCodDane='';
	public String strNombreContacto='';
	public String strIdentifContacto='';
	public String strCorreoContacto='';
	public String strMovilContacto='';
	public String strTelContacto='';
	public String strCargoContacto='';
	public String strNrodeContratoAnterior='';
	public string strNrodeContratoNuevo='';
	
	public BI_COL_Submit_AssignmentDavox_ctr(){
		listwr=new list<wrapperCesionDavox>();
		siSeEnvia=true;
		id = ApexPages.currentPage().getParameters().get('id');
		
		system.debug('Contrato ID'+id);
		
		//---------------------->>>> Aun no se ha creado el campo Duraci_n_en_meses__c y Fecha_Instalacion_Camara__c
		ms=new list<BI_COL_Modificacion_de_Servicio__c>([select id, name,BI_COL_Codigo_unico_servicio__r.name, BI_COL_FUN__r.BI_COL_Contrato_Anexos__r.name,BI_COL_Estado_orden_trabajo__c,BI_COL_Estado__c,BI_COL_Fecha_liberacion_OT__c,/*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c, 
		BI_COL_Fecha_inicio_de_cobro_RFB__c/*Fecha_Instalacion_Camara__c,Duraci_n_en_meses__c*/ ,BI_COL_Fecha_fin__c,BI_COL_Tipo_de_Moneda__c,CurrencyIsoCode, BI_COL_Sucursal_de_Facturacion__c,BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_COL_Direccion_sin_espacio__c, 
		BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_Localidad__c, BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Name, BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.ID,
		BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Email, BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.MobilePhone, BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Phone,
		BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Title,BI_COL_FUN__r.BI_COL_FUN__r.BI_COL_Contrato_Anexos__r.BI_COL_Numero_de_documento__c,BI_COL_FUN__r.BI_COL_Contrato_Anexos__r.BI_COL_Numero_de_documento__c,
		BI_COL_FUN__r.BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c, BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c
		from BI_COL_Modificacion_de_Servicio__c
		where BI_COL_FUN__r.BI_COL_Contrato__c=:id and BI_COL_Clasificacion_Servicio__c='CESION CONTRATO'
		]); //10/11/2016 ANRC: Se comentó el campo antiguo y se adiciono nuevo campo davox1
		
		system.debug('\n@-->>datos ms'+ms);
		if(ms.isEmpty()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No se encontraron MS con cesión de contratos'));
			siSeEnvia=false;
			return;
		}
		
		for(BI_COL_Modificacion_de_Servicio__c m: ms){
			if(m.BI_COL_Estado_orden_trabajo__c=='Ejecutado' && m.BI_COL_Estado__c=='Activa' && m.BI_COL_Fecha_liberacion_OT__c!=null){
				listwr.add(new wrapperCesionDavox(m,true));
				Dsr+=m.BI_COL_Codigo_unico_servicio__r.name+',';

			// ------------------->>> clonar clase BI_COL_GenerateNf_cls	
				BI_COL_GenerateNf_cls.estructuraV limpiarFi=new BI_COL_GenerateNf_cls.estructuraV('BI_COL_Fecha_inicio_de_cobro_RFB__c',31,'MM/dd/yyyy','Fecha',0,'',31.00);
				fechaI=limpiarFi.getLimpiar(m.BI_COL_Fecha_inicio_de_cobro_RFB__c);
				
				BI_COL_GenerateNf_cls.estructuraV limpiarFf=new BI_COL_GenerateNf_cls.estructuraV('BI_COL_Fecha_fin__c',33,'MM/dd/yyyy','Fecha',0,'',33.00);
				fechaf=limpiarFf.getLimpiar(m.BI_COL_Fecha_fin__c);
			
			//---------------------->>>> Aun no se ha creado el campo Duraci_n_en_meses__c	
				//BI_COL_GenerateNf_cls.estructuraV limpiarDu=new BI_COL_GenerateNf_cls.estructuraV('Duraci_n_en_meses__c',32,'###','Numérico',0,'',32.00);
				//duracion=limpiarDu.getLimpiar(m.Duraci_n_en_meses__c);
				
				//cuenta=m.BI_COL_Cuenta_facturar_davox__c; //10/11/2016 ANRC: Se comentó el campo antiguo
				cuenta=m.BI_COL_Cuenta_facturar_davox1__c; //10/11/2016 ANRC: Se adiciono nuevo campo davox1
				
				if(m.BI_COL_Tipo_de_Moneda__c!=null){
					moneda=m.BI_COL_Tipo_de_Moneda__c.toUpperCase();
				}
				
				strIdsucursaFact=m.BI_COL_Sucursal_de_Facturacion__c;
				strDirSinEspacio=m.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_COL_Direccion_sin_espacio__c;
				if(m.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_Localidad__c!=null)
				{
					strBarrio=m.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_Localidad__c;
				}
				strCodDane=m.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c;
				strNombreContacto=m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Name;
				strNombreContacto=strNombreContacto.toUpperCase();
				strIdentifContacto=m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.ID;
				strCorreoContacto=m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Email;
				strMovilContacto=m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.MobilePhone;
				strTelContacto=m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Phone;
				if(m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Title!=null)
				{
					strCargoContacto=m.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__r.Title;
				}
				strNrodeContratoAnterior=m.BI_COL_FUN__r.BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c;
				strNrodeContratoNuevo=m.BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c;
				
			}
			else
			{
				listwr.add(new wrapperCesionDavox(m,false));
				siSeEnvia=false;
			}
		
		}
		Dsr=Dsr.removeEnd(',');
		//fechaI=fechaI.removeEnd(',');
		//fechaf=fechaF.removeEnd(',');
		cuenta=cuenta.removeEnd(',');
		//duracion=duracion.removeEnd(',');
		
		system.debug('\n\n\n\nFIN');
	
	}
	
	public class wrapperCesionDavox{
		
		//115
		
		public BI_COL_Modificacion_de_Servicio__c ms{get;set;}
		public boolean esCorrecto{get;set;}
		
		public wrapperCesionDavox(BI_COL_Modificacion_de_Servicio__c ms,Boolean esCorrecto){
			this.esCorrecto=esCorrecto;
			this.ms=ms;
		}	
	
	
	}
	
	public Pagereference enviar(){
		
		contratoCadenaInterfaz();
		System.debug('\n\n Ingresa po linea 122\n\n');
		PageReference pr = new PageReference('/'+id);
        pr.setRedirect(true);
        return pr;
		
	}
	
	public Pagereference cancelar(){
		
		PageReference pr = new PageReference('/'+id);
        pr.setRedirect(true);
        return pr;
		
	}
	
	
	
	
	public void contratoCadenaInterfaz(){
		
		string idC=id;
		
		BI_COL_Davox_wsdl.BasicServiceWSSOAP ws = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
		ws.timeout_x=12000;

		//Informacion para homologar los campos del contrato contra la interfas de MS
		set<string> homologacion=new set<string>{'BI_COL_Oportunidad__r.','BI_COL_FUN__r.BI_COL_Contrato__r.'};
		
		//orden interfaz

		map<integer,BI_COL_GenerateNf_cls.estructuraV> estructuraList=new map<integer,BI_COL_GenerateNf_cls.estructuraV>();
		
		list<BI_COL_Configuracion_campos_interfases__c> confIntf= new list<BI_COL_Configuracion_campos_interfases__c>([SELECT BI_COL_Nombre_campo__c,BI_COL_Orden__c,BI_COL_Formato_Campo__c,BI_COL_Tipo_campo__c,BI_COL_Decimales__c,BI_COL_Separador_Interno__c,BI_COL_Longitud__c,BI_COL_Convertir_mayuscula__c
					FROM BI_COL_Configuracion_campos_interfases__c
					WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE' and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c='DAVOXPLUS' and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c='SERV. CORPORATIVO - NOVEDADES' AND 
					(BI_COL_Nombre_campo__c LIKE 'Oportunidad__r.Account.%' OR BI_COL_Nombre_campo__c LIKE 'FUN__r.Contrato__r.%')]);
		
		
		//Limpia la relacion para que se pueda buscar en el contrato
		set<string> camposContrato=new set<string>();
		for(BI_COL_Configuracion_campos_interfases__c ci:confIntf){
			string campo=ci.BI_COL_Nombre_campo__c.toLowerCase() ;
			for(string si:homologacion){
				System.debug('Valor:\n'+campo+'para limpiar'+si);
				campo=campo.remove(si);
				System.debug('FINAL:\n'+campo);	
			}
			camposContrato.add(campo);
			estructuraList.put((integer)ci.BI_COL_Orden__c,new BI_COL_GenerateNf_cls.estructuraV(campo,ci.BI_COL_Orden__c,ci.BI_COL_Formato_Campo__c,ci.BI_COL_Tipo_campo__c,ci.BI_COL_Decimales__c,ci.BI_COL_Separador_Interno__c,ci.BI_COL_Longitud__c));
		}
		//campos adicionales
		estructuraList.put(17,new BI_COL_GenerateNf_cls.estructuraV('Owner.BI_DNI_Per__c',17.00,'','Texto',0,'',20.00));
		estructuraList.put(27,new BI_COL_GenerateNf_cls.estructuraV('Id',27.00,'','Texto',0,'',20.00));
		estructuraList.put(90,new BI_COL_GenerateNf_cls.estructuraV('LastModifiedBy.Username',90.00,'','Texto',0,'',80.00));
		estructuraList.put(9,new BI_COL_GenerateNf_cls.estructuraV('Account.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c',9.00,'','Texto',0,'',9.00));
		
		string camposContratoString='';
		
		for (integer i:estructuraList.keySet()){
			//ignora duplicados
			if(!camposContratoString.containsIgnoreCase(estructuraList.get(i).nombre)){
			camposContratoString+=estructuraList.get(i).nombre +', ';
			system.debug(estructuraList.get(i).nombre);
			}
		}
		
		
		camposContratoString =camposContratoString.removeEnd(', ');
		
		camposContratoString='SELECT '+camposContratoString+' FROM Contract WHERE id = :idC';
		
		System.debug('\ncamposContratoString'+camposContratoString);
		
		list<Contract> ctr=Database.query(camposContratoString);
		
		AggregateResult totalPosiciones=[SELECT MAX(BI_COL_Orden__c) total
			FROM BI_COL_Configuracion_campos_interfases__c
			WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE' and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c='DAVOXPLUS' and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c='SERV. CORPORATIVO - NOVEDADES'];
			
		Integer total =Integer.valueOf(totalPosiciones.get('total'));			
		
		list<string>listCadena=new list<string>();
		
		//Se recorren los registros y los campos de los mismos para ser limpiados y formar una cadena para davox
		for(Contract c:ctr){
		string cadenaInterfaz='';

			for(Integer i=1;total>=i;i++){
				string valorC='';
				if(estructuraList.get(i)!=null){
					if(i==98)
					{
						estructuraList.get(i).nombre='Numero_de_documento__c';
					}
					
					if(estructuraList.get(i).nombre.contains('.')){
						if(estructuraList.get(i).nombre.split('\\x2E').size()<=2 && !estructuraList.get(i).nombre.contains(',')){
							list<string> relacion=estructuraList.get(i).nombre.split('\\x2E');
							valorC=estructuraList.get(i).getLimpiar(c.getSObject(relacion[0]).get(relacion[1]));
						}			
					}else{
						if(!estructuraList.get(i).nombre.contains(',')){
							valorC=estructuraList.get(i).getLimpiar(c.get(estructuraList.get(i).nombre));
						}
					}
				}
				
				
				if(i==9 && c.Account.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c!=null){
					valorC=c.Account.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c;
				}
				
				
				
				if(i==11){
					valorC=cuenta;
				}
				//Datos de la sucursal de facturacion
				if(i==13)
				{
					valorC=strIdsucursaFact;
				}
				if(i==14)
				{
					valorC=strDirSinEspacio;
				}
				if(i==15)
				{
					valorC=strBarrio;
				}
				if(i==16)
				{
					valorC=strCodDane;
				}
				//Activida Economica
				if(i==20)
				{
					valorC='NO APLICA';
				}
				
				// Datos del cointacto de facturacion
				if(i==21)
				{
					valorC=strNombreContacto;
				}
				if(i==22)
				{
					valorC=strIdentifContacto;
				}
				if(i==23)
				{
					valorC=strCorreoContacto;
				}
				if(i==24)
				{
					valorC=strMovilContacto;
				}
				if(i==25)
				{
					valorC=strTelContacto;
				}
				if(i==26)
				{
					valorC=strCargoContacto;
				}
				if(i==28)
				{
					valorC=strNrodeContratoAnterior;
				}
				
				if(i==30){
					valorC='CESION CONTRATO';
				}
				
				if(i==31){
					valorC=fechai;
				}
				/*
				if(i==32){
					valorC=duracion;
				}
				if(i==33){
					valorC=fechaf;
				}
				*/
				if(i==40){
					valorC=moneda;
				}
				if(i==98)
				{
					valorC=strNrodeContratoNuevo;
				}
				
				if(i==115){
					valorC=Dsr;
				}
				
				cadenaInterfaz+=valorC+'Õ';				
			}
			cadenaInterfaz=cadenaInterfaz.removeEnd('Õ');
			System.debug('cadenaInterfaz'+cadenaInterfaz);
			listCadena.add(cadenaInterfaz);
		}
		
		string pck=Math.floor((Math.random()*10000)+1)+'-ASD654AS'+System.now();
		string xml =BI_COL_CustomerAccount_ctr.armadoXMLMultiple(listCadena,'SERV. CORPORATIVO - NOVEDADES',pck);
		
		try
		{
			system.debug('\nxml'+xml);
			string strRespuestaServicio = ws.add(xml);
			system.debug(strRespuestaServicio);
			BI_COL_CustomerAccount_ctr cut=new BI_COL_CustomerAccount_ctr(); 
			strRespuestaServicio=cut.retornaCadena(strRespuestaServicio);
			
			list<string>logInfo=strRespuestaServicio.split('Õ');
			
			insert new BI_Log__c( BI_COL_Contrato__c=logInfo[0],BI_Descripcion__c=xml,BI_COL_Informacion_recibida__c=strRespuestaServicio,BI_COL_Estado__c='EXITOSO');

		}catch(exception e){
			
			insert new BI_Log__c( BI_COL_Contrato__c=ctr[0].id,BI_Descripcion__c=xml,BI_COL_Informacion_recibida__c='Error:\n'+e.getmessage(),BI_COL_Estado__c='FALLIDO');
		
		}
		
	
		
	}

		public void creaLogTransaccionError(String error, List<String> logInfo)
     {
          List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
          BI_Log__c log=null; 
          system.debug('métod de log de transacciones::Exception::'+error+'\n\n'+logInfo);
        
          for(String suc:logInfo)
          {
              log=new BI_Log__c(BI_COL_Informacion_recibida__c=error,BI_COL_Sede__c=suc,BI_COL_Estado__c='FALLIDO');
              lstLogTran.add(log);

        //log=new BI_Log__c(BI_COL_Informacion_recibida__c=error,BI_COL_Contacto__c=suc,BI_COL_Estado__c='FALLIDO',BI_COL_Interfaz__c='NOTIFICACIONES'); //,BI_Numero_de_documento__c='FALLIDO', Name='Error Servicio Web: '+servicio,
        //lstLogTran.add(log);
         
         }
          
          system.debug('Insertando log de transacciones::::'+lstLogTran);
          insert lstLogTran;
     }

    
     /**** OA: 04-09-2012 Se agrega método de creación de log de transacciones para el objeto Cuenta ****/ 
    public void creaLogTransaccion(ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] arrRespuesta, List<String> logInfo){
        List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
        BI_Log__c log=null; 
        system.debug('---->métod de log de transacciones::::'+arrRespuesta+'\n\n'+logInfo);
        system.debug('Iteracion For 1');
        lstLogTran.clear();
        if(!Test.isRunningTest())
            for(String suc:logInfo){ 
                if(arrRespuesta[0].idError.equals('0'))
                    log=new BI_Log__c(BI_COL_Informacion_recibida__c='OK, Respuesta Procesada',BI_COL_Sede__c=suc,BI_COL_Estado__c='EXITOSO');
                else
                    log=new BI_Log__c(BI_COL_Informacion_recibida__c='('+arrRespuesta[0].idError+')::'+arrRespuesta[0].descripcionError,BI_COL_Sede__c=suc,BI_COL_Estado__c='FALLIDO');
                lstLogTran.add(log);
            }
        
        system.debug('------->Insertando log de transacciones::::'+lstLogTran);
        insert lstLogTran;
    }
	
}