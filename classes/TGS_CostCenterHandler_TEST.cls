@isTest
public class TGS_CostCenterHandler_TEST {
    static testmethod void fillCostCenter_Test(){
        
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                             FROM Profile
                             WHERE Name = :profile
                             LIMIT 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.id;
            insert uO;
        }
        
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
            NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'InOrder');
            Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            insert CaseContact;
            Account testLegalEntity1 = [SELECT id, Name, Parentid FROM Account WHERE id =: CaseContact.AccountId LIMIT 1];
            Account testCustomerCountry1 = [SELECT id, Name, Parentid FROM Account WHERE id =: testLegalEntity1.Parentid LIMIT 1];
            Account testHolding1 = [SELECT id, Name, Parentid FROM Account WHERE id =: testCustomerCountry1.Parentid LIMIT 1];
            Account testCostCenter1 = [SELECT id,Name,Parentid FROM Account WHERE Parent.Parentid =: testLegalEntity1.id LIMIT 1];
            Account testBusinessUnit1 = [SELECT id,Name FROM Account WHERE Parentid =: testLegalEntity1.id LIMIT 1];
            testOrder.NE__AccountId__c = testLegalEntity1.Id; // JMF 03/04/2016
            testOrder.HoldingId__c = testHolding1.id; 
            testOrder.NE__ServAccId__c = testBusinessUnit1.id;
            //testOrder.NE__BillAccId__c = testCostCenter1.id;
            insert testOrder;
            NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
            insert testCatalog;
            NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
            insert testCatalogCategory;
            NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(Name='Test SubCategory', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCatalogCategory.Id);
            insert testCatalogSubCategory;
            
            NE__Product__c testProduct = new NE__Product__c(Name='Test Product');
            insert testProduct;
            /* Family property*/
            NE__Family__c family = new NE__Family__c(Name = 'Product Family');
            insert family;
            NE__DynamicPropertyDefinition__c property = new NE__DynamicPropertyDefinition__c(Name = 'Attribute 1', NE__Type__c = 'String');
            insert property;
            System.debug(property.RecordTypeId);
            NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__FamilyId__c = family.Id, NE__PropId__c = property.Id, NE__Required__c = 'No', TGS_Is_key_attribute__c = true);
            insert familyProp;
            NE__ProductFamily__c productFamily = new NE__ProductFamily__c(NE__ProdId__c = testProduct.Id, NE__FamilyId__c = family.Id);
            insert productFamily;
            
            NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Type__c = 'Product', NE__Catalog_Category_Name__c = testCatalogSubCategory.Id, NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
            insert testCatalogItem;
            NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1);
            insert testOrderItem;
            /* Attribute of the OrderItem*/
            List<NE__Order_Item_Attribute__c> ListAttributes =  new List<NE__Order_Item_Attribute__c>();
            
            NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Attribute 1', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attribute);
            NE__Order_Item_Attribute__c attributeID = new NE__Order_Item_Attribute__c(Name = 'Attribute 1_id', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attributeID);
            NE__Order_Item_Attribute__c attributeClass = new NE__Order_Item_Attribute__c(Name = 'Attribute 1_class', NE__Value__c = '', NE__Order_Item__c = testOrderItem.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attributeClass);
            insert ListAttributes;
            
            /* Case*/ 
            //rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Order Management Case').getRecordTypeId();
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
            Case testCase = new Case(RecordTypeId = rtId, Subject = 'Test Case', Status = 'Assigned', Order__c = testOrder.Id, Type = 'New', TGS_Customer_Services__c = testOrderItem.Id, TGS_Service__c = testProduct.Name, TGS_Invoice_Date__c = date.today(),ContactId = CaseContact.id, AccountId = CaseContact.AccountId);
            insert testCase;
            testOrder.Case__c = testCase.Id;
            update testOrder;
        }
        
        
    }
   
   // OI 01-03-2016
   static testmethod void deleteCase_Test()
   {
       String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                             FROM Profile
                             WHERE Name = :profile
                             LIMIT 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.id;
            insert uO;
        }
        
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
            NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'InOrder');
            Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            insert CaseContact;
            Account testLegalEntity1 = [SELECT id, Name, Parentid FROM Account WHERE id =: CaseContact.AccountId LIMIT 1];
            Account testCustomerCountry1 = [SELECT id, Name, Parentid FROM Account WHERE id =: testLegalEntity1.Parentid LIMIT 1];
            Account testHolding1 = [SELECT id, Name, Parentid FROM Account WHERE id =: testCustomerCountry1.Parentid LIMIT 1];
            Account testCostCenter1 = [SELECT id,Name,Parentid FROM Account WHERE Parent.Parentid =: testLegalEntity1.id LIMIT 1];
            Account testBusinessUnit1 = [SELECT id,Name FROM Account WHERE Parentid =: testLegalEntity1.id LIMIT 1];
            testOrder.NE__AccountId__c = testLegalEntity1.Id; // JMF 03/04/2016
            testOrder.HoldingId__c = testHolding1.id; 
            testOrder.NE__ServAccId__c = testBusinessUnit1.id;
            //testOrder.NE__BillAccId__c = testCostCenter1.id;
            insert testOrder;
            NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
            insert testCatalog;
            NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
            insert testCatalogCategory;
            NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(Name='Test SubCategory', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCatalogCategory.Id);
            insert testCatalogSubCategory;
            
            NE__Product__c testProduct = new NE__Product__c(Name='Test Product');
            insert testProduct;
            /* Family property*/
            NE__Family__c family = new NE__Family__c(Name = 'Product Family');
            insert family;
            NE__DynamicPropertyDefinition__c property = new NE__DynamicPropertyDefinition__c(Name = 'Attribute 1', NE__Type__c = 'String');
            insert property;
            System.debug(property.RecordTypeId);
            NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__FamilyId__c = family.Id, NE__PropId__c = property.Id, NE__Required__c = 'No', TGS_Is_key_attribute__c = true);
            insert familyProp;
            NE__ProductFamily__c productFamily = new NE__ProductFamily__c(NE__ProdId__c = testProduct.Id, NE__FamilyId__c = family.Id);
            insert productFamily;
            
            NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Type__c = 'Product', NE__Catalog_Category_Name__c = testCatalogSubCategory.Id, NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
            insert testCatalogItem;
            NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1);
            insert testOrderItem;
            /* Attribute of the OrderItem*/
            List<NE__Order_Item_Attribute__c> ListAttributes =  new List<NE__Order_Item_Attribute__c>();
            
            NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Attribute 1', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attribute);
            NE__Order_Item_Attribute__c attributeID = new NE__Order_Item_Attribute__c(Name = 'Attribute 1_id', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attributeID);
            NE__Order_Item_Attribute__c attributeClass = new NE__Order_Item_Attribute__c(Name = 'Attribute 1_class', NE__Value__c = '', NE__Order_Item__c = testOrderItem.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attributeClass);
            insert ListAttributes;
            
            /* Case*/ 
            //rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Order Management Case').getRecordTypeId();
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
            Case testCase = new Case(RecordTypeId = rtId, Subject = 'Test Case', Status = 'Assigned', Order__c = testOrder.Id, Type = 'New', TGS_Customer_Services__c = testOrderItem.Id, TGS_Service__c = testProduct.Name, TGS_Invoice_Date__c = date.today(),ContactId = CaseContact.id, AccountId = CaseContact.AccountId);
            insert testCase;
            testOrder.Case__c = testCase.Id;
            delete testOrder;
         }
   }
    
    
     
}