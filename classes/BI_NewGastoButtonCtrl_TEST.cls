@isTest
private class BI_NewGastoButtonCtrl_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_NewGastoButtonCtrl class 
    
    History: 
    
    <Date> 					<Author> 				<Change Description>
    12/08/2014    			Micah Burgos			Initial Version
    31/05/2017              Cristina Rodriguez      Commented BI_segment__c(Campaign)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the coverage code for BI_NewGastoButtonCtrl class 
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
    12/08/2014    			Micah Burgos			Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BI_NewGastoButton_TEST() {  			
		
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		Profile p = [select Id from Profile where Name = 'System Administrator' OR Name = :Label.BI_Administrador limit 1];
		User u = new User(alias = 'testu',
						  email = 'tu@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'Test',
						  languagelocalekey = 'en_US',
						  localesidkey = 'en_US',
						  ProfileId = p.Id,
						  BI_Permisos__c = Label.BI_Administrador,
						  timezonesidkey = 'America/Los_Angeles',
						  username = 'usertestuser@testorg.com',
						  Pais__c = Label.BI_Argentina);
		
		insert u;

		system.runAs(u){
	    	String region = Label.BI_Argentina;
			
			Campaign camp = new Campaign(Name = 'testCampaign',
		                                      CurrencyIsoCode = 'USD',
//		                                      BI_Segment__c = 'Empresas',
		                                      EndDate = date.TODAY(),
		                                      BI_Country__c = region,
		                                      BI_Crear_oportunidad_contactos__c = Label.BI_No
		                                      );
			
			insert camp;
			
		    set<String> setFieldToId  = new set<String>();
	       		 setFieldToId.add('BI_Campana__c');
	       		 setFieldToId.add('BI_Country__c');
	       		 
	   		map<string,string> dynamicIds = BI_DynamicFieldId.getDynamicField('BI_Gastos__c', setFieldToId);
			
			 PageReference pageRef = new PageReference('BI_NewGastoButton');
				 Test.setCurrentPage(pageRef);
				 
				 ApexPages.currentPage().getParameters().put(dynamicIds.get('BI_Campana__c'), String.valueOf(camp.Name) );
				 ApexPages.currentPage().getParameters().put(dynamicIds.get('BI_Country__c'), region);
				 ApexPages.currentPage().getParameters().put(dynamicIds.get('BI_Campana__c') + '_lkid', String.valueOf(camp.Id));
				 
				 BI_Gastos__c gasto = new BI_Gastos__c();

				 ApexPages.StandardController sc = new ApexPages.StandardController(gasto);
			 BI_NewGastoButtonCtrl controller = new BI_NewGastoButtonCtrl(sc);
			 controller.actionFunction();

			system.assert(controller.regionName != null);
			system.assert(controller.currencyType != null);
			system.assert(controller.camp_Name != null);
			system.assert(controller.camp_Id != null);
		}
   	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}