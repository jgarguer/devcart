@isTest(seeAllData = false)
public class BIIN_Obtener_Token_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Obtener_Token_BAO
    
    <Date>                 <Author>               <Change Description>
    11/2015              José María Martín           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void test_BIIN_Obtener_Token_TEST() {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];

        System.runAs(ActualUser)
        {               
            Test.startTest(); 
            
            BIIN_Obtener_Token_BAO ceController = new BIIN_Obtener_Token_BAO();
            ceController.obtencionToken();
            
            Test.stopTest();
        }
    }
}