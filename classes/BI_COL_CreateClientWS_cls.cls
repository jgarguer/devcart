/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-03-24      Daniel ALexander Lopez (DL)     Cloned Trigger 
*					2018-01-12		Gawron, Julián					lstClientes.isEmpty()) return     
*************************************************************************************/

public with sharing class BI_COL_CreateClientWS_cls 
{
	@future (callout=true)
	
	public static void crearClienteTelefonica (List<Id> lstIdClientes, String accion)
	{
		map<ID,ws_wwwTelefonicaComNotificacionessalesfo.cliente> infClienteEnviad=new map<ID,ws_wwwTelefonicaComNotificacionessalesfo.cliente>();
		BI_COL_FlagforTGRS_cls.flagTriggers = true;
		// Array para enviar al Web Service
		ws_wwwTelefonicaComNotificacionessalesfo.cliente[] arrayCliente;
		//public static boolean bandera = false;
		
		   System.debug('=======TESTE 1 ==== \n'+lstIdClientes+' \n '+accion);

		if(lstIdClientes!=null && lstIdClientes.size()>0)
		{
			//Query para traer los datos del objeto Account dependiendo del ID q llega del trigger
			Account objCliente = new Account();
			System.debug('=======TESTE 2 ==== ');
		   List<Account> lstClientes = [Select  Id,
												Name,
												BI_Denominacion_comercial__c,
												BI_Tipo_de_identificador_fiscal__c,
												BI_No_Identificador_fiscal__c,
												NE__E_mail__c,
												BI_COL_Naturaleza__c,
												BI_Subsegment_Local__c,
												BI_COL_Actividad_economica__c,
												Phone,
												BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c,
												BI_Fecha_de_ultima_carga_deuda__c,
												Fax,
												LastModifiedById,
												OwnerId,
												BI_Country__c,
												BI_Sector__c,
												BI_Subsector__c,
												BI_Segment__c,
													(Select Id,
													 AccountId,
													 UserId,
													 TeamMemberRole,
													 AccountAccessLevel,
													 CreatedDate,
													 CreatedById,
													 LastModifiedDate,
													 LastModifiedById,
													 SystemModstamp,
													 IsDeleted From AccountTeamMembers WHERE TeamMemberRole ='Jefe de servicio')
												 From Account Where Id in :lstIdClientes AND BI_Country__c='Colombia' ALL ROWS];

			// 12/01/2018
			//Condicion para salir si no tiene valores de Colombia. //JEG 
			if(lstClientes.isEmpty()) return;

			System.debug('=======TESTE 3 ====  lst clientes '+lstClientes);                                                 
			ws_wwwTelefonicaComNotificacionessalesfo.cliente cl = new ws_wwwTelefonicaComNotificacionessalesfo.cliente();
			arrayCliente = new ws_wwwTelefonicaComNotificacionessalesfo.cliente[]{};
			System.debug('=======TESTE 4 ==== ');
			if(lstClientes.size()>0)
			{
				//Llamando a funcion en el Ws que espera los datos para Contacto.
			   System.debug('=======TESTE 5 ==== ');
				for(Account ac : lstClientes)
				{
					System.debug('=======TESTE 6 ==== ');
					//system.debug('..==.. objeto result cliente.. '+ac.BillingAddress);
					cl = new ws_wwwTelefonicaComNotificacionessalesfo.cliente();
					cl.name = ac.Name;
					cl.sigla = ac.BI_Denominacion_comercial__c;
					cl.tipoId = ac.BI_Tipo_de_identificador_fiscal__c;
					cl.identificacion = ac.BI_No_Identificador_fiscal__c;
					cl.emailCliente = ac.NE__E_mail__c;
					cl.telefono = ac.Phone;
					cl.celular = ac.Phone;
					cl.fax = ac.Fax;
					cl.naturaleza = ac.BI_COL_Naturaleza__c;
					cl.codigoDane = ac.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c;
					cl.segmentoTelefonica = ac.BI_Segment__c==null?'':String.valueOf(ac.BI_Segment__c).toUpperCase();
					cl.subsegmentoTelefonica = ac.BI_Subsegment_Local__c==null?'':String.valueOf(ac.BI_Subsegment_Local__c).toUpperCase();
					cl.sectorTelefonica=ac.BI_Sector__c==null?'':String.valueOf(ac.BI_Sector__c).toUpperCase();
					cl.subsectorTelefonica = ac.BI_Subsector__c==null?'':String.valueOf(ac.BI_Subsector__c).toUpperCase();
					cl.actividadEconomica = ac.BI_COL_Actividad_economica__c;
					cl.identificacionPadre = ac.BI_No_Identificador_fiscal__c;
					cl.tipoIdentificacionPadre = ac.BI_Tipo_de_identificador_fiscal__c;
					cl.idCliente = ac.Id;
					cl.asesorVentas = ac.OwnerId;
					cl.pais = ac.BI_Country__c;
					cl.idCuadrante='';
					cl.idSector='';
					cl.accion=accion;
					cl.idUsuarioModificacion=ac.LastModifiedById;
					cl.marcacionCliente='1';
					arrayCliente.add(cl);
					infClienteEnviad.put(ac.Id,cl);
				}
			}else if(accion=='Eliminar')
			{              
			  for(Account ac:lstClientes)
			  {
				System.debug('=======TESTE 7 ==== ');
				cl = new ws_wwwTelefonicaComNotificacionessalesfo.cliente();
				cl.idCliente = ac.Id;
				cl.accion=accion;
				cl.idUsuarioModificacion=Userinfo.getUserId();
				arrayCliente.add(cl);
				
			  }
			}

			ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] rtaWSClientes;
			System.debug('=======TESTE 8 ==== ');
			ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP xy = new ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP();
			System.debug('..==.. request Web Service RegistrarClientes ..==.. '+arrayCliente);
			try
			{	
				rtaWSClientes = xy.RegistrarClientes(arrayCliente);
				if(accion!='Eliminar')
				{
					creaLogTransaccion(rtaWSClientes,lstClientes,infClienteEnviad);
					System.debug('..==.. Rta Web Service RegistrarClientes ..==.. '+rtaWSClientes);
				}
			}
			catch(Exception ex)
			{ 

				creaLogTransaccionError(''+ex,lstClientes,String.valueof(arrayCliente));

			}
			
			
		}
	}

	//Metodo creado para crear el log de transacciones cuando falla el consumo del servicio web

	 public static void creaLogTransaccionError(String error, List<Account> lstIdClientes,String infEnviada)
	 {
		  List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
		  BI_Log__c log=null; 
		  system.debug('métod de log de transacciones::Exception::'+error+'\n\n'+lstIdClientes);
		
		  for(Account acc:lstIdClientes)
		  {
			  log=new BI_Log__c(BI_COL_Informacion_recibida__c=error,BI_COL_Cuenta__c=acc.id,BI_COL_Estado__c='FALLIDO',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=infEnviada);
			  lstLogTran.add(log);
		  }
		  
		  system.debug('Insertando log de transacciones::::'+lstLogTran);
		  insert lstLogTran;
	 }
	
   


	/**** OA: 04-09-2012 Se agrega método de creación de log de transacciones para el objeto Cuenta ****/
	public static void creaLogTransaccion(ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] arrRespuesta, List<Account> lstClientes,map<ID,ws_wwwTelefonicaComNotificacionessalesfo.cliente> infClienteEnviad){
		System.debug('=======REspuesta ==== '+arrRespuesta);
		
		System.debug('=======REspuesta lstClientes ==== '+lstClientes);
		List<BI_Log__c> lstLogTran = new List<BI_Log__c>();
		BI_Log__c log=null; 
		  for(Account cuenta:lstClientes)
		  {
			if(arrRespuesta[0].idError.equals('0'))
			{
				system.debug('\n\n Entro al log if '+infClienteEnviad.get(cuenta.id));
				log=new BI_Log__c(BI_COL_Informacion_recibida__c=String.valueOF(arrRespuesta),BI_COL_Cuenta__c=cuenta.id,BI_COL_Tipo_Transaccion__c='SALIDA NOTIFICACIONES',BI_COL_Estado__c ='Exitoso',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=String.valueof(infClienteEnviad.get(cuenta.id)));
			}
			else
			{
				system.debug('Entro al log else');
				log=new BI_Log__c(BI_COL_Informacion_recibida__c='('+arrRespuesta[0].idError+')::'+arrRespuesta[0].descripcionError,BI_COL_Cuenta__c=cuenta.id,BI_COL_Tipo_Transaccion__c='SALIDA NOTIFICACIONES',BI_COL_Estado__c='Fallido',BI_COL_Informacion_Enviada__c=String.valueOf(infClienteEnviad.get(cuenta.id)));
			}
			lstLogTran.add(log);
		  }
		insert lstLogTran;
	}

		  
	
}