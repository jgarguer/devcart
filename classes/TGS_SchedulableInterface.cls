/*------------------------------------------------------------
Author:         Everis
Company:        Everis
Description:    Clase Schedule/Batch para crear Orders de Billable Mobile Line.
History
<Date>          <Author>                <Change Description>
14/02/2017      Everis          		Initial Version.  
17/03/2017	 	Everis					Añadimos el campo NE__OrderID__r.NE__AccountID__r.BI_Country__c a los SELECT.
22/03/2017	 	Everis					Reducimos la carga de Select al quitar campos que no eran necesarios.
17/04/2017		Everis					Cambiamos de Schedulable a Batcheable
------------------------------------------------------------*/
global class TGS_SchedulableInterface implements Schedulable, Database.batchable<ID>{
    
    global static Boolean TGS_BATCH_BILLABLE_MOBILE_LINE = false; 
    
    global List<ID> start(Database.BatchableContext info){ 
        List<NE__OrderItem__c> OIs = new List<NE__OrderItem__c>();
        If(!Test.isRunningTest())
            OIs = [SELECT NE__Billing_Account__c, TGS_CSUID1__c FROM NE__OrderItem__c Where NE__OrderId__r.Case__r.TGS_Service__c = :Constants.MOBILE_LINE 
                   AND NE__OrderId__r.NE__rTypeName__c = :Constants.RECORD_TYPE_ASSET AND NE__OrderId__r.NE__OrderStatus__c = :Constants.CI_STATUS_ACTIVE 
                   AND TGS_Product_Name__c = :Constants.MOBILE_LINE AND TGS_Service_status__c = :Constants.CI_TGS_SERVICE_STATUS_DEPLOYED AND isDeleted=false AND NE__Billing_Account__c != null];
        Else{
            OIs = [SELECT NE__Billing_Account__c, TGS_CSUID1__c FROM NE__OrderItem__c ]; //Where NE__OrderId__r.Case__r.TGS_Service__c = :Constants.MOBILE_LINE];   
        }
        SET<ID> SET_AccountIDs = new SET<ID>();
        for (NE__OrderItem__c OI :OIs){
            if (OI.TGS_CSUID1__c != null) SET_AccountIDs.add(OI.NE__Billing_Account__c);
        }
        List<ID> accountsID = new List<ID>();
        for(ID acct :SET_AccountIDs){
            accountsID.add(acct);
        }
        System.debug('Everis: El Batch de Billable Mobile Line ha encontrado las siguientes cuentas: ' + accountsID);
        return accountsID; 
    } 
    
    // Schedule deletion of logs
    global void execute(SchedulableContext sc) {
        System.debug('Id de batch: ' + Database.executeBatch(this, 1));
    }
    
    global void execute(Database.BatchableContext info, List<ID> scope){
        TGS_BATCH_BILLABLE_MOBILE_LINE = true;
        System.debug('Everis: Entro al Batch de Billable Mobile Line con la lista de cuentas: ' + scope);
        TGS_SchedulableInterface_Helper.generateBillingMobileLine(scope);
        TGS_BATCH_BILLABLE_MOBILE_LINE = false;
    }   
    
    global void finish(Database.BatchableContext info){     
    } 
}