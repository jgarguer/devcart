public with sharing class BI_Tasa_de_cambio_presupuestariaMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Methods executed by BI_Tasa_de_cambio_presupuestaria Triggers 
    Test Class:    BI_Tasa_de_cambio_presupuestaria_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    08/11/2016              Gawron, Julián          Initial Version D317
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

 static Map<String, Decimal> MAP_TASAS = new Map<String, Decimal>();

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Method that allows the chronological order of the BI_Tasa_de_cambio_presupuestaria 
    			   records in each CurrencyIsoCode, 
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    08/11/2016              Gawron, Julián          Initial Version D317
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	public static void BI_actualiza_fecha_fin_anterior(List<BI_Tasa_de_cambio_presupuestaria__c> news, 
		List<BI_Tasa_de_cambio_presupuestaria__c> olds){
		Boolean isInsert = olds == null;

		try{

		 if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
                
		if(isInsert){
			System.debug('BI_actualiza_fecha_fin_anterior en Insert');
			System.debug('BI_actualiza_fecha_fin_anterior en news:' + news);
			System.debug('BI_actualiza_fecha_fin_anterior en olds:' + olds);
		 Map<String, BI_Tasa_de_cambio_presupuestaria__c> map_tasas_toprocess = 
		     new Map<String, BI_Tasa_de_cambio_presupuestaria__c>();

		 //Recuperamos las tasas activas (que no tienen fecha de fin)
		 for(BI_Tasa_de_cambio_presupuestaria__c tasa :
	 		[Select Id, BI_Ratio_divisa_a_euro__c, BI_Fecha_inicio__c, BI_Fecha_fin__c, CurrencyIsoCode 
	 		from BI_Tasa_de_cambio_presupuestaria__c where BI_Fecha_fin__c = NULL AND Id != NULL]){
	 		map_tasas_toprocess.put(tasa.CurrencyIsoCode, tasa);
	 		System.debug('BI_actualiza_fecha_fin_anterior cada anterior:' + tasa.BI_Fecha_inicio__c);
	 	 }
		 
		 for(BI_Tasa_de_cambio_presupuestaria__c tasaNueva : news){
			if(map_tasas_toprocess.containsKey(tasaNueva.CurrencyIsoCode)){
				if(tasaNueva.BI_Fecha_inicio__c < map_tasas_toprocess.get(tasaNueva.CurrencyIsoCode).BI_Fecha_inicio__c){
					tasaNueva.BI_Fecha_inicio__c.addError('La fecha ingresada ya tiene valor asignado. Verifique los datos anteriores');
				}
				//Actualizamos la fecha fin con la nueva tasa
				map_tasas_toprocess.get(tasaNueva.CurrencyIsoCode).BI_Fecha_fin__c = tasaNueva.BI_Fecha_inicio__c;
			}
		  }
		  NETriggerHelper.setTriggerFired('BI_Tasa_de_cambio_presupuestaria');
		  update map_tasas_toprocess.values();
		}//Is insert

		else{ //IsUpdate

		 List<Id> lst_Id_en_trigger = new List<Id>();
		 for(BI_Tasa_de_cambio_presupuestaria__c t : olds){
		 	lst_Id_en_trigger.add(t.Id);
		 }
		 List<BI_Tasa_de_cambio_presupuestaria__c> lst_aActualizar = new List<BI_Tasa_de_cambio_presupuestaria__c>();
		 for(BI_Tasa_de_cambio_presupuestaria__c tasa :
			 [Select Id, BI_Ratio_divisa_a_euro__c, BI_Fecha_inicio__c, BI_Fecha_fin__c, CurrencyIsoCode 
			 from BI_Tasa_de_cambio_presupuestaria__c where BI_Fecha_fin__c != NULL AND Id NOT IN:lst_Id_en_trigger]){
		 	for(Integer i = 0; i < olds.size(); i++){
		 		//Si el campo antiguo de fecha es igual a la fecha que estaba guardada
		 		if( olds[i].CurrencyIsoCode == tasa.CurrencyIsoCode 
		 			&& tasa.BI_Fecha_fin__c == olds[i].BI_Fecha_inicio__c
		 			){
		 			//cambiamos ese campo con la fecha nueva.
		 			
		 			tasa.BI_Fecha_fin__c = news[i].BI_Fecha_inicio__c;
		 			lst_aActualizar.add(tasa);
		 		}
		 	}

		 }//fin del for
		 System.debug('BI_actualiza_fecha_fin_anterior lista aActualizar' + lst_aActualizar);
		 NETriggerHelper.setTriggerFired('BI_Tasa_de_cambio_presupuestaria');
		 update lst_aActualizar;

	    }

	 } catch (Exception ex) {
			System.debug(LoggingLevel.ERROR, 'BI_Tasa_de_cambio_presupuestaria.BI_actualiza_fecha_fin_anterior Type: ' + ex.getTypeName() + ' Reason: ' + ex.getMessage());
			BI_LogHelper.generate_BILog('BI_Tasa_de_cambio_presupuestaria.BI_actualiza_fecha_fin_anterior', 'BI_EN', ex, 'Trigger');
	 }
	}//fin BI_actualiza_fecha_fin_anterior

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:    
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    08/11/2016              Gawron, Julián          Initial Version D317
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
    public static Map<String, Decimal> getTasasActuales(){
        if(MAP_TASAS.size() > 0 && !Test.isRunningTest()){
            system.debug('MAP_TASAS Utilizado :::' + MAP_TASAS);
            return MAP_TASAS;
        }else{  //Si no tiene datos o es un test  
		    for(BI_Tasa_de_cambio_presupuestaria__c tdcp :
		            [Select BI_Fecha_inicio__c, BI_Fecha_fin__c, CurrencyIsoCode, BI_Ratio_divisa_a_euro__c
		              from BI_Tasa_de_cambio_presupuestaria__c 
		              where BI_Fecha_inicio__c <= TODAY 
		              AND (BI_Fecha_fin__c > TODAY OR BI_Fecha_fin__c = NULL)
		            ]){
		            MAP_TASAS.put(tdcp.CurrencyIsoCode, tdcp.BI_Ratio_divisa_a_euro__c);
		        }
            system.debug('MAP_TASAS Cargado :::' + MAP_TASAS);
        	return MAP_TASAS;
        }
        
    }//fin de getTasasActuales

}