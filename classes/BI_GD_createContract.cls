/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:				Jesús Martínez
Company:			Everis España
Description:    	Migración de Contract: Operación POST.

History:

<Date>							<Author>							<Change Description>
26/10/2016						Jesús Martínez						Versión Inicial.
31/10/2016						Miguel Girón						Modificación para poner el body en XML.
02/11/2016						Miguel Girón						Cambio en la response: setBody por setStatus.
15/03/2017						Eduardo Ventura						Normalización del desarrollo y sustitución XML por JSON.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class BI_GD_createContract {
    
    /* AUTH */
    //private static final String username = 'julio.alberto.asenjo.garcia@everis.com.prod.fsarg2';
    //private static final String password = 'Ja15022016-07';
    
    /* Rest Generic Configuration */
    //private static final String endpoint22 = 'callout:BI_GD_Argentina'+ '/telefonica/api/contractinfo/v1/contracts?client_id=2ac34a39-fa4e-4c6c-a79a-b008daa8e698' ;
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:				Jesús Martínez
	Company:			Everis España
	Description:    	Migración de Contract: Operación POST.

	History:

	<Date>							<Author>							<Change Description>
	26/10/2016						Jesús Martínez						Versión Inicial.
	31/10/2016						Miguel Girón						Modificación para poner el body en XML.
	02/11/2016						Miguel Girón						Cambio en la response: setBody por setStatus.
	15/03/2017						Eduardo Ventura						Normalización del desarrollo y sustitución XML por JSON.
	15/03/2017						Eduardo Ventura						Eliminación de métodos inecesarios.
	02/11/2017															Ever 01: Try catch de logs.
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global static HttpResponse createContract(BI_RestWrapper.ContractRequestType contract, String method){
        /* Endpoint Configuration */
        //Ever01-INI
        HttpResponse response;
        try{
        //Ever01-FIN
        String endpoint = 'callout:Gestor_Argentina';
        BI_GD_Config__c mc = BI_GD_Config__c.getOrgDefaults();
        
        if(method=='POST') {
            endpoint += mc.BI_GD_endPoint__c;
        }
        else if(method=='PUT' && contract.correlationId != null){
            endpoint += mc.BI_GD_endPoint__c+'/'+contract.correlationId;
        }
        /* Variable Configuration */
        Http http = new Http();
        HttpRequest request = generateARGHeader();
            //Ever01-INI
        //HttpResponse response = new HttpResponse();
        response = new HttpResponse();
        	//Ever01-FIN
        /* Rest Configuration */
        request.setMethod(method);
        request.setEndpoint(endpoint);
       
        
        request.setTimeout(20000);
        
        request.setBody(JSON.serialize(contract, true));
        system.debug('\n\n-->@@@@@El contrato es'+ JSON.serialize(contract, true)+'\n');
        /* Start Process */
        //Ever01-INI
        //try{
            //Ever01-FIN
            if(Test.isRunningTest()){			 
                BI_GD_createContract_Mock mock = new BI_GD_createContract_Mock();
                response = mock.respond(request);
            } else 
                response = http.send(request);                                
            
        }catch(Exception except){
            //Ever01-INI
            BI_LogHelper.generate_BILog('BI_GD_createContract.createContract', 'BI_EN', except, 'Web Service');
            //response.setStatusCode(404);
            //response.setBody('The requested resource couldn’t be found. Check the URI for errors, and verify that there are no sharing issues.');
            //Ever01-FIN
        } 
        return response;
    }
    
    public static HttpRequest generateARGHeader (){
        /* Variable Configuration */
        //Ever01-INI
        HttpRequest request;
        try{
        //HttpRequest request = new HttpRequest();
        request = new HttpRequest();
        //Ever01-FIN
        BI_GD_Config__c mc = BI_GD_Config__c.getOrgDefaults();


          request.setHeader('lang', mc.BI_GD_lang__c);
        request.setHeader('entity', mc.BI_GD_entity__c);
        request.setHeader('system', mc.BI_GD_system__c);
        request.setHeader('subsystem', mc.BI_GD_subsystem__c);
        request.setHeader('userId', mc.BI_GD_userId__c);
        request.setHeader('execId', mc.BI_GD_execId__c);
        request.setHeader('msgId', mc.BI_GD_msgId__c);
        request.setHeader('x-ibm-client-id', mc.BI_GD_clientid__c);
      
        //request.setHeader('Content-Type','application/xml; charset=UTF-8');
        //request.setClientCertificateName(mc.BI_GD_nameCertif__c);
        //Ever01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_GD_createContract.generateARGHeader', 'BI_EN', exc, 'Web Service');
        }
        //Ever01-FIN
        return request;
    }
    
   /* private static String timestamp(){
        /* Variable Declaration */
       /* String result = '';
        
        try{
            DateTime dt = DateTime.newInstance(system.currentTimeMillis());
            result = dt.format('yyyy-MM-dd\'T\'hh:mm:ss');  
        }
        catch (Exception exc) {}
        
        return result;
    }*/
    
    /*private static String UUID(){
        /* Variable Declaration */
       /* String result = '';
        
        try{
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            result = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        }catch(Exception exc){}
        
        return result;
    }*/
}