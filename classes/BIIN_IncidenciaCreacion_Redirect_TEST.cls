/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales
    Company:       Deloitte
    Description:   Test class to manage the coverage code for BIIN_Detalle_Incidencia class
    
    History: 
    <Date>                  <Author>                <Change Description>
    02/11/2015              Ignacio Morales         Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest(seeAllData = true)

public class BIIN_IncidenciaCreacion_Redirect_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Detalle_Incidencia
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    01/11/2015              Ignacio Morales         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BIIN_IncidenciaCreacion_Redirect(){
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];
        
        System.runAs(ActualUser){     
            Case caso = new Case(Status='Assigned', Description='Descripcion test');
        	insert caso;
            Test.startTest();
            
            PageReference p = Page.BIIN_Detalle_Incidencia;
            p.getParameters().put('retURL', caso.Id);
            Test.setCurrentPageReference(p);
            
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BIIN_IncidenciaCreacion_Redirect_Ctrl ceController = new BIIN_IncidenciaCreacion_Redirect_Ctrl(controller);
            PageReference redirect = ceController.redirect();
            
            Test.stopTest();
        }
    }
}