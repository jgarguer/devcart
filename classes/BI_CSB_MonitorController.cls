/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Class containing methods to control Transaction CSB Monitor
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/12/2015              Fernando Arteaga        Initial Version
    30/05/2016				Fernando Arteaga		Use indexed field in query
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_CSB_MonitorController
{
	public ApexPages.StandardSetController setCon {get; set;}
    public Integer size {get;set;} 
    public Integer noOfRecords {get; set;} 
    public List<SelectOption> paginationSizeOptions {get;set;}
    public String delId {get; set;}
    public List<BI_Log__c> listTransactions {get; set;}
         
    public BI_CSB_MonitorController(Apexpages.Standardcontroller controller)
    {
        this.size=10;
        this.paginationSizeOptions = new List<SelectOption>();
        this.paginationSizeOptions.add(new SelectOption('5','5'));
        this.paginationSizeOptions.add(new SelectOption('10','10'));
        this.paginationSizeOptions.add(new SelectOption('20','20'));
        this.paginationSizeOptions.add(new SelectOption('50','50'));
        this.paginationSizeOptions.add(new SelectOption('100','100'));
        this.listTransactions = [SELECT Id, Name, BI_Asunto__c, BI_Descripcion__c, Descripcion__c, BI_Tipo_Error__c, BI_COL_Estado__c
		 	                     FROM BI_Log__c
			                     //WHERE BI_Asunto__c = 'Transacción CSB'
			                     WHERE BI_CSB_Record_Id__c != null	// FAR 30/05/2016
        			             ORDER BY Name DESC];
        			             
        this.setCon = new ApexPages.StandardSetController(this.listTransactions);
        this.setCon.setPageSize(this.size);  
        this.noOfRecords = this.setCon.getResultSize();
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Refresh pagination size
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/12/2015              Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference refreshPageSize()
    {
         this.setCon.setPageSize(this.size);
         return null;
    }
 

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Gets CSB transactions 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/12/2015              Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<BI_Log__c> getTransactions()
    {
    	System.debug('listTransactions.size:' + this.listTransactions.size());
		return this.setCon.getRecords();
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Deletes a transaction 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/12/2015              Fernando Arteaga        Initial Version
    30/05/2016				Fernando Arteaga		Use indexed field in query
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference deleteTransaction()
    {
    	System.debug('delId:' + this.delId);
    	List<BI_Log__c> listToDelete = new List<BI_Log__c>();
    	
    	for (BI_Log__c t: this.listTransactions)
    	{
    		if (t.Id == delId)
    		{
    			listToDelete.add(t);
    			break;
    		}
    	}
    	
    	delete listToDelete;

		this.listTransactions = [SELECT Id, Name, BI_Asunto__c, BI_Descripcion__c, Descripcion__c, BI_Tipo_Error__c, BI_COL_Estado__c
		 	                     FROM BI_Log__c
			                     //WHERE BI_Asunto__c = 'Transacción CSB'
			                     WHERE BI_CSB_Record_Id__c != null	// FAR 30/05/2016
        			             ORDER BY Name DESC];
        			             
        this.setCon = new ApexPages.StandardSetController(this.listTransactions);
        this.setCon.setPageSize(this.size);  
        this.noOfRecords = this.setCon.getResultSize();
    	
    	return null;
    }
}