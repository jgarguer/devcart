@isTest
private class BI_ContractMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for CampaignMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    23/09/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_ContractMethods.assignCurrency.
   
    History:
    
    <Date>                   <Author>                <Change Description>
    23/09/2014               Ignacio Llorca          Initial Version       
    11/05/2016               Guillermo Muñoz         BI_TestUtils.throw_exception = false added 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void assignCurrencyTest() {
        BI_TestUtils.throw_exception = false;
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
                  
            List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
            
            Opportunity opp = new Opportunity(Name = 'Test',
                                                  CloseDate = Date.today(),
                                                  BI_Requiere_contrato__c = true,
                                                  Generate_Acuerdo_Marco__c = false,
                                                  AccountId = lst_acc[0].id,
                                                  StageName = Label.BI_OpportunityStageF1Ganada,
                                                  BI_Ultrarapida__c = true,
                                                  BI_Duracion_del_contrato_Meses__c = 3,
                                                  BI_Recurrente_bruto_mensual__c = 100.12,
                                                  BI_Ingreso_por_unica_vez__c = 12.1,
                                                  BI_Opportunity_Type__c = 'Producto Standard',
                                                  Amount = 23,
                                                  BI_Country__c = lst_pais[0]);
                                            
            insert opp;
            
            NE__Product__c pord = new NE__Product__c();
            pord.Offer_SubFamily__c = 'a-test;';
            pord.Offer_Family__c = 'b-test';
            
            insert pord;        
            
            
            NE__Catalog__c cat = new NE__Catalog__c();
            insert cat;
            NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = pord.Id, NE__Change_Subtype__c = 'test', NE__Disconnect_Subtype__c = 'test');
            insert catIt;
            
            NE__Order__c ord = new NE__Order__c(NE__AccountId__c = lst_acc[0].Id,
                                                NE__BillAccId__c = lst_acc[0].Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = lst_acc[0].Id,
                                                NE__OrderStatus__c = Label.BI_OpportunityStageF1Ganada);
                                                
                
            insert ord;
    
    
            List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();                                                  
            for(Integer j = 0; j < 50; j++){
                NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = ord.Id,
                                                           NE__Qty__c = 2,
                                                           NE__Asset_Item_Account__c = lst_acc[0].id,
                                                           NE__Account__c = lst_acc[0].id,
                                                           NE__Status__c = Label.BI_LabelActive,
                                                           NE__ProdId__c = pord.Id,
                                                           CurrencyIsoCode = 'ARS',
                                                           NE__CatalogItem__c = catIt.Id);
                lst_oi.add(oit);
            }                                                  
            for(Integer j = 0; j < 50; j++){
                NE__OrderItem__c oit2 = new NE__OrderItem__c(NE__OrderId__c = ord.Id,
                                                           NE__Qty__c = 2,
                                                           NE__Asset_Item_Account__c = lst_acc[0].id,
                                                           NE__Account__c = lst_acc[0].id,
                                                           NE__Status__c = Label.BI_LabelActive,
                                                           NE__ProdId__c = pord.Id,
                                                           CurrencyIsoCode = 'EUR',
                                                           NE__CatalogItem__c = catIt.Id);
                lst_oi.add(oit2);
            }
            
            insert lst_oi;
            
            Test.StartTest();
            
            List <Contract> lst_cont = BI_DataLoad.loadContracts(200,lst_acc[0].Id, opp.Id);
            
            Set <Id> set_cont = new Set <Id>();
            
            for (Contract item:lst_cont){
                set_cont.add(item.Id);
            }
            
            List <Contract> lst_cont2 = [SELECT Id, BI_Divisas_del_contrato__c FROM Contract WHERE Id IN :set_cont];
            system.assert(!lst_cont2.isEmpty());
            
            for (Contract cont:lst_cont2){          
                system.assertequals(cont.BI_Divisas_del_contrato__c, 'ARS - Peso Argentino;EUR - Euro');
            }
            Test.StopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }

*/ 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda
    Description:   Test method to manage the code coverage for BI_ContractMethods.moverNCP.
   
    History:
    
    <Date>                   <Author>                <Change Description>
    24/05/2016               Antonio Masferrer         Initial Version  
    03/08/2016               Humberto Nunes           Se omitio el system.assertEquals    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void moverNCP_TEST() {
        
        
        User user = [select Id from User where Id = :UserInfo.getUserId()];
        
        system.runAs(user){

            List<RecordType> lst_rt = [SELECT Id,DeveloperName 
                                   FROM RecordType 
                                   WHERE DeveloperName = 'BI_FVI_NCP'
                                   OR DeveloperName = 'BI_FVI_NAV'
                                   OR DeveloperName = 'BI_FVI_NAB'
                                   OR DeveloperName = 'BI_FVI_NAT'];

        
            Id rt_NCP,rt_NAT,rt_NAV,rt_NAB;
            
            for(RecordType rt : lst_rt){
                if(rt.DeveloperName == 'BI_FVI_NCP'){
                    rt_NCP = rt.Id;
                }else if(rt.DeveloperName == 'BI_FVI_NAV'){
                    rt_NAV = rt.Id;
                }else if(rt.DeveloperName == 'BI_FVI_NAT'){
                    rt_NAT = rt.Id;
                }else{
                    rt_NAB = rt.Id;
                }
            }

            insert new BI_FVI_Nodos__c(Name = 'NCP_TEST', RecordTypeId = rt_NCP, BI_FVI_Pais__c = Label.BI_Peru);

            BI_FVI_Nodos__c ncp = [SELECT Id, RecordType.DeveloperName, RecordTypeId FROM BI_FVI_Nodos__c WHERE Name = 'NCP_TEST' LIMIT 1];

            insert new BI_FVI_Nodos__c(Name = 'NAV_TEST', RecordTypeId = rt_NAV, BI_FVI_Pais__c = Label.BI_Peru, BI_FVI_NodoPadre__c = ncp.Id);

            BI_FVI_Nodos__c nav = [SELECT Id, BI_FVI_NodoPadre__r.RecordTypeId FROM BI_FVI_Nodos__c WHERE Name = 'NAV_TEST' LIMIT 1];
            
            system.assertEquals(nav.BI_FVI_NodoPadre__r.RecordTypeId, rt_NCP);

            Account acc = new Account(
                Name = 'TestNodos1',
                BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 95,
                BI_Country__c = Label.BI_Peru,
                BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'Negocios',
                BI_Subsegment_Regional__c = 'test',
                BI_Territory__c = 'test'
            );

            insert acc;

            Contract con = new Contract(
                AccountId = acc.Id,
                Status = 'Draft'
            );

            insert con;

            List<Account> lst_accQ = [SELECT Id, Name, OwnerId, BI_Country__c, BI_No_Identificador_fiscal__c, BI_Tipo_de_identificador_fiscal__c, CurrencyIsoCode FROM Account]; 

            BI_FVI_Nodo_Cliente__c nc = new BI_FVI_Nodo_Cliente__c(
                BI_FVI_IdCliente__c = lst_accQ[0].Id,
                BI_FVI_IdNodo__c = nav.Id
            );

            insert nc;
            
            List<Contract> lst_conQ = [SELECT Id, Name, Account.Name, BI_Oportunidad__r.Name, Status, OwnerId FROM Contract]; 

            lst_conQ[0].Status = 'Signed Activated';
            update lst_conQ[0];

            List<Contract> assertContr = [SELECT Id, Name, Account.BI_FVI_Nodo__r.RecordType.DeveloperName FROM Contract WHERE Id =: lst_conQ[0].Id LIMIT 1];

            // system.assertEquals(assertContr[0].Account.BI_FVI_Nodo__r.RecordType.DeveloperName , 'BI_FVI_NCP');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda
    Description:   Test method to manage the code coverage for BI_ContractMethods.FiltroOpp.
   
    History:
    
    <Date>                   <Author>                <Change Description>
    07/06/2016               Antonio Masferrer        Initial Version       
    03/08/2016               Humberto Nunes           Se agrego el Campo 'BI_Fecha_de_cierre_real__c' y 'BI_Licitacion__c'
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void FiltroOpp_TEST() {
      
      Account acc = new Account(
        Name = 'Test1',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 95,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
        BI_Segment__c = 'Negocios',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test'
      );
      insert acc;

      Opportunity opp = new Opportunity(
        Name = 'OppTest1',
        BI_Requiere_contrato__c = true,
        CloseDate = Date.today(),
        StageName = 'F1 - Closed Won',
        AccountId = acc.Id,
        BI_Ciclo_ventas__c = Label.BI_Completo,
        BI_Country__c = Label.BI_Peru,
        CurrencyIsoCode = 'ARS',
        BI_Fecha_de_cierre_real__c = Date.today(),
        BI_Licitacion__c = 'No'
      );
      insert opp;

      List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName IN ('BI_FVI_Contrato')];
      Contract con = new Contract(
        AccountId = acc.Id,
        Status = 'Draft',
        BI_Oportunidad__c = opp.Id,
        RecordTypeId = lst_rt[0].Id
      );
      insert con;



    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda
    Description:   Test method to manage the code coverage for BI_ContractMethods.moverNCP.
   
    History:
    
    <Date>                   <Author>                <Change Description>
    07/06/2016               Antonio Masferrer        Initial Version  
    03/08/2016               Humberto Nunes           Se agrego el Campo 'BI_Fecha_de_cierre_real__c' y 'BI_Licitacion__c'
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void CallZytrust_TEST() {

      Account acc = new Account(
        Name = 'Test1',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 95,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
        BI_Segment__c = 'Negocios',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test'
      );
      insert acc;

      Opportunity opp = new Opportunity(
        Name = 'OppTest1',
        CloseDate = Date.today(),
        BI_Requiere_contrato__c = true,
        StageName = 'F1 - Closed Won',
        AccountId = acc.Id,
        BI_Ciclo_ventas__c = Label.BI_Completo,
        BI_Country__c = Label.BI_Peru,
        CurrencyIsoCode = 'ARS',
        BI_Fecha_de_cierre_real__c = Date.today(),
        BI_Licitacion__c = 'No'
      );
      insert opp;

      List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName IN ('BI_FVI_Contrato')];
      Contract con = new Contract(
        AccountId = acc.Id,
        Status = 'Draft',
        BI_Oportunidad__c = opp.Id,
        RecordTypeId = lst_rt[0].Id
      );
      insert con;

      con.BI_FVI_Solicitar_Contrato__c = true;
      update con;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda
    Description:   Test method to manage the code coverage for BI_ContractMethods.CreateRegistroDesemp.
   
    History:
    
    <Date>                   <Author>                <Change Description>
    07/06/2016               Antonio Masferrer        Initial Version       
    03/08/2016               Humberto Nunes           Se agrego el Campo 'BI_Fecha_de_cierre_real__c' y 'BI_Licitacion__c'
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void CreateRegistroDesemp_TEST() {
      BI_TestUtils.throw_exception = false;
      Account acc = new Account(
        Name = 'Test1',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 95,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
        BI_Segment__c = 'Negocios',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test'
      );
      insert acc;

      Opportunity opp = new Opportunity(
        Name = 'OppTest1',
        CloseDate = Date.today(),
        BI_Requiere_contrato__c = true,
        StageName = 'F1 - Closed Won',
        AccountId = acc.Id,
        BI_Ciclo_ventas__c = Label.BI_Completo,
        BI_Country__c = Label.BI_Peru,
        CurrencyIsoCode = 'ARS',
        BI_Fecha_de_cierre_real__c = Date.today(),
        BI_Licitacion__c = 'No'
      );
      insert opp;

      List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName IN ('BI_FVI_Contrato')];
      Contract con = new Contract(
        AccountId = acc.Id,
        Status = 'Draft',
        // BI_FVI_Solicitar_Contrato__c = true,
        BI_Oportunidad__c = opp.Id,
        RecordTypeId = lst_rt[0].Id
      );
      insert con;

      con.Status = 'Signed Activated';
      update con;
      
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda
    Description:   Test method to manage the code coverage for the BI_ContractMethods exceptions
   
    History:
    
    <Date>                   <Author>                <Change Description>
    07/06/2016               Antonio Masferrer         Initial Version       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void exception_TEST() {
      BI_TestUtils.throw_exception = true; 

      BI_ContractMethods.FiltroOpp(null);
      BI_ContractMethods.moverNCP(null, null);
      BI_ContractMethods.CreateRegistroDesemp(null, null);

      BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Patricia Castillo
    Company:       NEAborda
    Description:   Test method to manage the code coverage for the validateUserModifierARG method
   
    History:
    
    <Date>                   <Author>                <Change Description>
    11/10/2016               Patricia Castillo       Initial Version     
    26/05/2017               Gawron, Julián E.       Adding fixes and migrationHelper 
    18/08/2017               Guillermo Muñoz         Changed to use new BI_MigrationHelper functionality
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void validateUserModifierARG_TEST() {
      BI_TestUtils.throw_exception = false;
      TGS_User_Org__c userTGS = new TGS_User_Org__c();
      userTGS.TGS_Is_BI_EN__c = true;
      insert userTGS;

      Contract con = new Contract();
      List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName IN ('BI_Contrato')];
      List<Profile> id_ProfARG = new List<Profile>([SELECT Id from Profile where name='BI_Standard_ARG' limit 1]);
      User me = [SELECT Id from User where id=:UserInfo.getUserId() LIMIT 1];
   
    
       Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, false, false, false);
       BI_MigrationHelper.skipAllTriggers();
      //Argentina User
      User usARG = new User(
        Username = 'd321prueba@test.com',
        Email = 'd321prueba@test.com',
        Alias = 'd321',
        CommunityNickname = 'd321.prueba',
        IsActive = true,
        ProfileId = id_ProfARG[0].Id,
        BI_Permisos__c = 'Ejecutivo de Cliente',
        Pais__c = 'Argentina',
        TimeZoneSidKey='Europe/Paris', 
        LocaleSidKey='es_ES', 
        EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='es', 
        LastName='prueba'
      );
      System.runAs(me){
        insert usARG;
      }
      

      //Arg Account
        Account acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_No_Identificador_fiscal__c = '12345678', 
          BI_Validador_Fiscal__c = 'ARG_DNI_12345678', 
          BI_Country__c = Label.BI_Argentina, 
          BI_Sector__c = 'Comercio', 
          BI_Segment__c = 'Empresas', 
          BI_Subsector__c = 'Alimentos y Bebidas', 
          BI_Subsegment_Regional__c = 'Corporate', 
          BI_Tipo_de_identificador_fiscal__c = 'DNI', 
          TGS_Es_MNC__c = false, 
          Sector__c = 'Private',
          OwnerId = usARG.Id,
          BI_Territory__c = 'test'
        );
        insert acc;

        AccountTeamMember atm = new AccountTeamMember(
          AccountId = acc.Id, 
          UserId = usARG.Id, 
          TeamMemberRole = 'Sales Engineer'
        );
        insert atm;
        
        Opportunity OPP = new Opportunity(
          Name = 'OppTest1',
          CloseDate = Date.today(),
          StageName = Label.BI_F6Preoportunidad,//'F1 - Closed Won',
          AccountId = acc.Id,
          BI_Ciclo_ventas__c = Label.BI_Completo,
          BI_Country__c = Label.BI_Argentina,
          CurrencyIsoCode = 'ARS',
         // BI_Fecha_de_cierre_real__c = Date.today(),
          BI_Licitacion__c = 'NO',
          BI_ARG_Proveedor_Tercero__c = '123456abc', 
          BI_ARG_Proveedor_Internacional__c = 'Tiws', 
          BI_ARG_Servicios_Internacionales__c = 'SI', 
          BI_ARG_Fecha_Sol_Elaboracion_Rta_Pliego__c = Date.today(), 
          BI_ARG_Fecha_presentacion_de_sobres__c = Date.today(), 
          BI_ARG_Tipo_de_Contratacion__c = 'Concurso Privado', 
          BI_ARG_Nro_de_Licitacion__c = '12ab34cd'
        );
        System.runAs(usARG){
            insert opp;
        }        
        

        BI_MigrationHelper.disableBypass(mapa);
        BI_MigrationHelper.cleanSkippedTriggers();

        con.AccountId = acc.Id;
        con.Status = 'Draft';
        con.OwnerId = usARG.Id;
        con.BI_Oportunidad__c = opp.Id;
        con.RecordTypeId = lst_rt[0].Id;
        con.BI_ARG_Monetario__c = 'SI';
        con.BI_ARG_Recargo_por_Mora__c = 'SI';
        con.BI_ARG_Rescision_Anticipada__c = 'NO';
        con.BI_ARG_Valor_Tope__c =100.6;
        con.BI_ARG_Tipo_de_Cambio_Aplicable__c ='Otro';
        con.BI_ARG_SLA__c ='Especial';
        con.BI_ARG_Venta_de_Equipamiento__c ='Contado';
        con.BI_ARG_Empresa_Local__c ='TASA';
        con.BI_ARG_Moneda_Impuesto__c ='ARS';
        con.BI_ARG_Impuesto_a_los_Sellos__c ='NO';
        con.BI_ARG_Monto_Impuesto__c =0;
        con.BI_Fecha_fin_de_implementacion__c=Date.today()+60;
        con.BI_Fecha_inicio_servicio__c=Date.today();
        con.BI_ARG_Tope_Tipo_de_Cambio__c='NO';
        con.BI_ARG_Id_Legado__c = '123abc';
       

      System.runAs(usARG){
        
        
        try{
         insert con;
          
          throw new BI_Exception('La validación no se realizó correctamente');
        } catch (DMLException e) {

        }
      }

     
      mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, false, false, false);
      BI_MigrationHelper.skipAllTriggers();
     
          opp.StageName = 'F1 - Closed Won';
          opp.BI_Fecha_de_cierre_real__c = Date.today();
          update opp;

      BI_MigrationHelper.disableBypass(mapa);
      BI_MigrationHelper.cleanSkippedTriggers();

      User usARG_CA = new User(
        Username = 'ca_d321prueba@test.com',
        Email = 'ca_d321prueba@test.com',
        Alias = 'cad321',
        CommunityNickname = 'ca_d321.prueba',
        IsActive = true,
        ProfileId = id_ProfARG[0].Id,
        BI_Permisos__c = 'Ingeniería/Preventa',
        Pais__c = 'Argentina',
        TimeZoneSidKey='Europe/Paris', 
        LocaleSidKey='es_ES', 
        EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='es', 
        LastName='prueba'
      );
      System.runAs(me){
        insert usARG_CA;

      }
      acc.OwnerId = usARG_CA.Id;
      update acc;

      AccountTeamMember atm_CA = new AccountTeamMember(
        AccountId = con.AccountId, 
        UserId = usARG_CA.Id, 
        TeamMemberRole = 'Contract Administrator'
      );
      insert atm_CA;

      OpportunityShare opps = new OpportunityShare(
        OpportunityId = opp.Id,
        UserOrGroupId = usARG_CA.Id,
        OpportunityAccessLevel = 'Edit'
        
      );
      Test.startTest();
      System.runAs(me){
        insert opps;
      }
      con.OwnerId = usARG_CA.Id;
      System.runAs(usARG_CA){
        //Arg Account
        
          insert con;
        
      }

      con.BI_ARG_Monetario__c = 'NO';
      con.BI_ARG_Recargo_por_Mora__c = 'NO';
      con.BI_ARG_Rescision_Anticipada__c = 'SI';
      con.BI_ARG_Valor_Tope__c =100.2;
      con.BI_ARG_Tipo_de_Cambio_Aplicable__c ='Efectivo Pago';
      con.BI_ARG_SLA__c ='No';
      con.BI_ARG_Venta_de_Equipamiento__c ='Leasing';
      con.BI_ARG_Empresa_Local__c ='MVS';
      con.BI_ARG_Moneda_Impuesto__c ='USD';
      con.BI_ARG_Impuesto_a_los_Sellos__c ='SI';
      con.BI_ARG_Monto_Impuesto__c =0.5;
      con.BI_Fecha_fin_de_implementacion__c=Date.today()+30;
      con.BI_Fecha_inicio_servicio__c=Date.today()-1;
      con.BI_ARG_Tope_Tipo_de_Cambio__c='SI';
      con.BI_ARG_Id_Legado__c = '123abcD';
      //AccountShare accs = [Select id from AccountShare where UserOrGroupId =:usARG.Id limit 1 ];
      atm_CA.TeamMemberRole='Sales Manager';
      usARG_CA.BI_Permisos__c = 'Ejecutivo de Cliente';
      update usARG_CA;
      update atm_CA;
      System.runAs(usARG_CA){
        try{
          update con;
        } catch(DmlException e) {
          System.assertEquals(e!=null, true);
        }
      }
      Test.stopTest();

        
    }

}