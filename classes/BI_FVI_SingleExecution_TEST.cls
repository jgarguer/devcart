@isTest
private class BI_FVI_SingleExecution_TEST 
{	
	static testMethod void testSingleExecution()
	{
	    //Hasn't already run
	    System.assertEquals(false,BI_FVI_SingleExecution.hasAlreadyExecuted('testSingleExecution'));

	    BI_FVI_SingleExecution.setAlreadyExecuted('testSingleExecution');
	    //Has just been run
	    System.assertEquals(true,BI_FVI_SingleExecution.hasAlreadyExecuted('testSingleExecution'));
	}
	
}