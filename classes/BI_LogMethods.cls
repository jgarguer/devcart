public without sharing class BI_LogMethods {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that separate the logs in diferent types and sets the 'BI_Delete__c' field to true of the logs that will be deleted in the next
	    			   job execution of BI_DeleteLogs_JOB
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void checkLogsToDelete(List <BI_Log__c> news){

		try{
			//condición añadida para evitar recursividad en el test.
			if(news.size() == 1 && !(news[0].BI_Tipo_Error__c == 'BI_Exception')){
				if(BI_TestUtils.isRunningTest()){
					throw new BI_Exception('Test');
				}
			}
	        List <BI_Log__c> toProcess = new List <BI_Log__c>();
	        String query;
	        Set <Id> set_idMs = new Set <Id> ();
	        Set <Id> set_idOpp = new Set <Id> ();
	        Set <Id> set_idCon = new Set <Id> ();
	        Set <Id> set_idSede = new Set <Id> ();
	        Set <Id> set_idVia = new Set <Id> ();

	        //Separación de logs por objeto al que referencian.
	        for(BI_log__c log : news){
	            if(log.BI_COL_Modificacion_Servicio__c != null){
	                set_idMs.add(log.BI_COL_Modificacion_Servicio__c);
	            }
	            else if(log.BI_COL_Oportunidad__c != null){
	                set_idOpp.add(log.BI_COL_Oportunidad__c);
	            }
	            else if(log.BI_COL_Contacto__c != null){
	                set_idCon.add(log.BI_COL_Contacto__c);
	            }
	            else if(log.BI_COL_Sede__c != null){
	                set_idSede.add(log.BI_COL_Sede__c);
	            }
	            else if(log.BI_COL_Solicitud_viabilidad__c != null){
	                set_idVia.add(log.BI_COL_Solicitud_viabilidad__c);
	            }
	        }

	        Boolean ms = !set_idMs.isEmpty();
	        Boolean opp = !set_idOpp.isEmpty();
	        Boolean con = !set_idCon.isEmpty();
	        Boolean sede = !set_idSede.isEmpty();
	        Boolean via = !set_idVia.isEmpty();

	        if(ms || opp || con || sede || via){

	            query = 'SELECT BI_COL_Estado__c, CreatedDate,';
	            String whereSentence = '';
	            
	            //Creación de la query dinámica para solo consultar los registros que se van a procesar.
	            if(ms){
	                query += ' BI_COL_Modificacion_Servicio__c, BI_COL_Modificacion_Servicio__r.BI_COL_Oportunidad__r.Account.BI_Segment__c,';
	                whereSentence += ' BI_COL_Modificacion_Servicio__c IN: set_idMs';
	            }
	            if(opp){
	                query += ' BI_COL_Oportunidad__c, BI_COL_Oportunidad__r.Account.BI_Segment__c,';
	                if(whereSentence.length() > 0){
	                    whereSentence += ' OR';
	                }
	                whereSentence += ' BI_COL_Oportunidad__c IN: set_idOpp';
	            }
	            if(con){
	                query += ' BI_COL_Contacto__c, BI_COL_Contacto__r.Account.BI_Segment__c,';
	                if(whereSentence.length() > 0){
	                    whereSentence += ' OR';
	                }
	                whereSentence += ' BI_COL_Contacto__c IN: set_idCon';
	            }
	            if(sede){
	                query += ' BI_COL_Sede__c, BI_COL_Sede__r.BI_Cliente__r.BI_Segment__c,';
	                if(whereSentence.length() > 0){
	                    whereSentence += ' OR';
	                }
	                whereSentence += ' BI_COL_Sede__c IN: set_idSede';
	            }
	            if(via){
	                query += ' BI_COL_Solicitud_viabilidad__c, BI_COL_Solicitud_viabilidad__r.BI_COL_Modificacion_de_servicio__r.BI_COL_Oportunidad__r.Account.BI_Segment__c,';
	                if(whereSentence.length() > 0){
	                    whereSentence += ' OR';
	                }
	                whereSentence += ' BI_COL_Solicitud_viabilidad__c IN: set_idVia';
	            }

	            query = query.removeEnd(',');
	            query += ' FROM BI_Log__c WHERE (' + whereSentence + ') AND BI_Delete__c = false';
	            System.debug('#####query---->' +query);

	            toProcess = Database.query(query);

	            System.debug('#####lista---->' + toProcess);

	            //Creación del mapa con los registros separados por objeto referenciado
	            if(!toProcess.isEmpty()){
	                Map <String, List<BI_Log__c>> map_logs = new  Map <String, List<BI_Log__c>>();
	                List <BI_log__c> aux;
	                for(BI_Log__c log : toProcess){
	                    if(ms && log.BI_COL_Modificacion_Servicio__c != null){
	                        if(map_logs.containsKey('MS')){
	                            map_logs.get('MS').add(log);
	                        }
	                        else{
	                            aux = new List <BI_Log__c>();
	                            aux.add(log);
	                            map_logs.put('MS', aux);
	                        }
	                    }
	                    else if(opp && log.BI_COL_Oportunidad__c != null){
	                        if(map_logs.containsKey('Opp')){
	                            map_logs.get('Opp').add(log);
	                        }
	                        else{
	                            aux = new List <BI_Log__c>();
	                            aux.add(log);
	                            map_logs.put('Opp', aux);
	                        }
	                    }
	                    else if(con && log.BI_COL_Contacto__c != null){
	                        if(map_logs.containsKey('Con')){
	                            map_logs.get('Con').add(log);
	                        }
	                        else{
	                            aux = new List <BI_Log__c>();
	                            aux.add(log);
	                            map_logs.put('Con', aux);
	                        }
	                    }
	                    else if(sede && log.BI_COL_Sede__c != null){
	                        if(map_logs.containsKey('Sede')){
	                            map_logs.get('Sede').add(log);
	                        }
	                        else{
	                            aux = new List <BI_Log__c>();
	                            aux.add(log);
	                            map_logs.put('Sede', aux);
	                        }
	                    }
	                    else if(via && log.BI_COL_Solicitud_viabilidad__c != null){
	                        if(map_logs.containsKey('Via')){
	                            map_logs.get('Via').add(log);
	                        }
	                        else{
	                            aux = new List <BI_Log__c>();
	                            aux.add(log);
	                            map_logs.put('Via', aux);
	                        }
	                    }
	                }
	                System.debug('#####Mapa---->' + map_logs);

	                //Envío de la lista de logs a cada método para su procesado.
	                if(map_logs.containsKey('MS')){
	                    checkMSLogs(map_logs.get('MS'));
	                }

	                if(map_logs.containsKey('Opp')){
	                    checkOpportunityLogs(map_logs.get('Opp'));
	                }

	                if(map_logs.containsKey('Con')){
	                    checkContactLogs(map_logs.get('Con'));
	                }

	                if(map_logs.containsKey('Sede')){
	                    checkSedeLogs(map_logs.get('Sede'));
	                }

	                if(map_logs.containsKey('Via')){
	                    checkViabilidadLogs(map_logs.get('Via'));
	                }
	            }
	        }
    	}catch(Exception exc){
    		BI_LogHelper.generate_BILog('BI_LogMethods.checkLogsToDelete', 'BI_EN', exc, 'Trigger');
    	}
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that filter the MS logs
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testVisible
    private static void checkMSLogs(List <BI_Log__c> toProcess){

        List <BI_Log__c> lst_toUpdate = new List <BI_Log__c>();

        //Solo se marcan los logs referenciados a clientes con segmento mayoristas fallidos
        for(BI_Log__c log : toProcess){
            if(log.BI_COL_Modificacion_Servicio__r.BI_COL_Oportunidad__r.Account.BI_Segment__c == Label.BI_COL_lblBI_COL_lblMayoristas && log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                log.BI_Delete__c = true;
                lst_toUpdate.add(log);
            }
        }

        if(!lst_toUpdate.isEmpty()){
            update lst_toUpdate;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that filter the Opportunity logs
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testVisible
    private static void checkOpportunityLogs(List <BI_Log__c> toProcess){
        
        List <BI_Log__c> lst_toUpdate = new List <BI_Log__c>();

        //Solo se marcan los logs referenciados a clientes con segmento mayoristas fallidos
        for(BI_Log__c log : toProcess){
            if(log.BI_COL_Oportunidad__r.Account.BI_Segment__c == Label.BI_COL_lblBI_COL_lblMayoristas && log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                log.BI_Delete__c = true;
                lst_toUpdate.add(log);
            }
        }

        if(!lst_toUpdate.isEmpty()){
            update lst_toUpdate;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that filter the Contact logs
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testVisible
    private static void checkContactLogs(List <BI_Log__c> toProcess){
        
        Map <Id, List <BI_Log__c>> map_conLogs = new Map <Id, List <BI_Log__c>>();
        List <BI_Log__c> aux;
        List <BI_Log__c> lst_toUpdate = new List <BI_Log__c>();

        //Se marcan los logs referenciados a clientes con segmento mayoristas fallidos y todos (exitosos y fallidos) de los logs
        //referenciados a clientes con segmento diferente a mayoristas excepto el últmi exitoso
        for(BI_Log__c log : toProcess){

            if(log.BI_COL_Contacto__r.Account.BI_Segment__c != Label.BI_COL_lblBI_COL_lblMayoristas){
                if(map_conLogs.containsKey(log.BI_COL_Contacto__c)){
                    map_conLogs.get(log.BI_COL_Contacto__c).add(log);
                }
                else{
                    aux = new List <BI_Log__c>();
                    aux.add(log);
                    map_conLogs.put(log.BI_COL_Contacto__c, aux);
                }
            }
            else if(log.BI_COL_Contacto__r.Account.BI_Segment__c != null && log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                log.BI_Delete__c = true;
                lst_toUpdate.add(log);
            }
        }

        BI_Log__c notChecked;

        for(Id idLog : map_conLogs.KeySet()){
            notChecked = null;
            for(BI_Log__c log : map_conLogs.get(idLog)){
                if(notChecked == null && log.BI_COL_Estado__c == Label.BI_Exitoso){
                    notChecked = log;
                }
                else if(log.BI_COL_Estado__c == Label.BI_Exitoso){
                    if(log.CreatedDate > notChecked.CreatedDate){
                        notChecked.BI_Delete__c = true;
                        lst_toUpdate.add(notChecked);
                        notChecked = log;
                    }
                    else{
                        log.BI_Delete__c = true;
                        lst_toUpdate.add(log);
                    }
                }
                else if(log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                    log.BI_Delete__c = true;
                    lst_toUpdate.add(log);
                }
            }
        }
        if(!lst_toUpdate.isEmpty()){
            update lst_toUpdate;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that filter the Sede logs
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testVisible
    private static void checkSedeLogs(List <BI_Log__c> toProcess){
        
        Map <Id, List <BI_Log__c>> map_sedeLogs = new Map <Id, List <BI_Log__c>>();
        List <BI_Log__c> aux;
        List <BI_Log__c> lst_toUpdate = new List <BI_Log__c>();

        //Se marcan los logs referenciados a clientes con segmento mayoristas fallidos y todos (exitosos y fallidos) de los logs
        //referenciados a clientes con segmento diferente a mayoristas excepto el últmi exitoso
        for(BI_Log__c log : toProcess){

            if(log.BI_COL_Sede__r.BI_Cliente__r.BI_Segment__c != Label.BI_COL_lblBI_COL_lblMayoristas){
                if(map_sedeLogs.containsKey(log.BI_COL_Sede__c)){
                    map_sedeLogs.get(log.BI_COL_Sede__c).add(log);
                }
                else{
                    aux = new List <BI_Log__c>();
                    aux.add(log);
                    map_sedeLogs.put(log.BI_COL_Sede__c, aux);
                }
            }
            else if(log.BI_COL_Sede__r.BI_Cliente__r.BI_Segment__c != null && log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                log.BI_Delete__c = true;
                lst_toUpdate.add(log);
            }
        }

        BI_Log__c notChecked;

        for(Id idLog : map_sedeLogs.KeySet()){
            notChecked = null;
            for(BI_Log__c log : map_sedeLogs.get(idLog)){
                if(notChecked == null && log.BI_COL_Estado__c == Label.BI_Exitoso){
                    notChecked = log;
                }
                else if(log.BI_COL_Estado__c == Label.BI_Exitoso){
                    if(log.CreatedDate > notChecked.CreatedDate){
                        notChecked.BI_Delete__c = true;
                        lst_toUpdate.add(notChecked);
                        notChecked = log;
                    }
                    else{
                        log.BI_Delete__c = true;
                        lst_toUpdate.add(log);
                    }
                }
                else if(log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                    log.BI_Delete__c = true;
                    lst_toUpdate.add(log);
                }
            }
        }
        if(!lst_toUpdate.isEmpty()){
            update lst_toUpdate;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that filter the Solicitud de viabilidad logs
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testVisible
    private static void checkViabilidadLogs(List <BI_Log__c> toProcess){
        
        List <BI_Log__c> lst_toUpdate = new List <BI_Log__c>();

        //Solo se marcan los logs referenciados a clientes con segmento mayoristas fallidos
        for(BI_Log__c log : toProcess){
            if(log.BI_COL_Solicitud_viabilidad__r.BI_COL_Modificacion_de_servicio__r.BI_COL_Oportunidad__r.Account.BI_Segment__c == Label.BI_COL_lblBI_COL_lblMayoristas && log.BI_COL_Estado__c == Label.BI_COL_lblFallido){
                log.BI_Delete__c = true;
                lst_toUpdate.add(log);
            }
        }

        if(!lst_toUpdate.isEmpty()){
            update lst_toUpdate;
        }
    }

}