/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CWP_reclamosPedidosController_TEST {
    
    @testSetup 
    private static void dataModelSetup() {  
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
            set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
           //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
            
          
         //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        contactTest.BI_Cuenta_activa_en_portal__c = accLegalEntity.id;
        insert contactTest;
        
        User usuario;
              
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            DateTime date1= DateTime.newInstance(2017, 11, 18, 3, 3, 3);
            Date date2= Date.newInstance(2018, 09, 18);
            insert usuario;
            User usuario1 = [SELECT id,AccountId FROM User WHERE  FirstName =: 'nombre1']; 
            system.debug('TEST: ' + usuario1.AccountId);
            NE__Order__c neorderTest= new NE__Order__c (NE__Type__c = 'ChangeOrder',
                                          NE__Order_date__c = date1,
                                          NE__OrderStatus__c = 'Pending',
                                          NE__AccountId__c = usuario1.AccountId                                                                                    
                                          ); 
                                          
           insert neorderTest; 
            
            
            //CATALOG
            NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
            insert catalogo;
            NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
            insert catalogCategoryParent;
            NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
            insert catalogCategoryChildren;
            NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
            producto.TGS_CWP_Tier_1__c = 'Catalogo padre';
            producto.TGS_CWP_Tier_2__c = 'Catalogo hijo';
            insert producto;
            NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Product', catalogCategoryChildren.id, catalogo.id, producto.id);
            insert catalogoItem;
            
           
            //CASO     
            list <Case> listaDeCasos = new list <Case>();                
            Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
            newCase.contactId = contactTest.id;        
            listaDeCasos.add(newCase);
            newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
            newCase.contactId = contactTest.id;
            listaDeCasos.add(newCase);
            newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
            newCase.contactId = contactTest.id;
            listaDeCasos.add(newCase);
            newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
            newCase.contactId = contactTest.id;
            listaDeCasos.add(newCase);
            newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
            newCase.contactId = contactTest.id;
            listaDeCasos.add(newCase);
            newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
            newCase.contactId = contactTest.id;
            listaDeCasos.add(newCase);
            newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
            newCase.contactId = contactTest.id;                
            listaDeCasos.add(newCase);
            insert listaDeCasos;        
            
            
            // ORDER   
            NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
            testOrder1.Case__c = listaDeCasos[1].id;
            insert testOrder1;
            
            NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
            testOrder2.Case__c = listaDeCasos[0].id;
            insert testOrder2;        
            
            // ORDER ITEM
            NE__OrderItem__c newOI;        
            newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
            insert newOI; 
           
        }       
      
    }
    
    static testMethod void getRecords(){
        //SETUP TESTS
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        system.runAs(usuario){
                CWP_reclamosPedidosController.getRecords('init');
                String jsonTest = '{        "table": "table",       "iniIndex": 1,      "finIndex": 1,      "numRecords": 1,        "viewSize": 1   }   ';
                CWP_reclamosPedidosController.getRecords(jsonTest);   
        } 
    }
    static testMethod void getFieldsValues(){
        //SETUP TESTS
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.runAs(usuario){

                CWP_reclamosPedidosController.getFieldsValues('init');
                
        } 
     }
     
     static testMethod void getFilteredRecords(){
        //SETUP TESTS
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id,accountId FROM User WHERE FirstName =: 'nombre1'];        
        system.runAs(usuario){

                CWP_reclamosPedidosController.getFilteredRecords('Order_Code_Pedido__c',usuario.accountId);
                
        } 
     }
     
     static testMethod void getSortRecords(){
        //SETUP TESTS
         BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.runAs(usuario){

                CWP_reclamosPedidosController.getSortRecords('Order_Code_Pedido__c','ASC');
                
        } 
     }
     
     static testMethod void getRecorDetail(){
         //SETUP TESTS
         BI_TestUtils.throw_exception=false;
         User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];      
        system.runAs(usuario){

                NE__OrderItem__c idorderitem= [SELECT id FROM NE__OrderItem__c LIMIT 1];         
            
                CWP_reclamosPedidosController.getRecorDetail(idorderitem.Id);
                
        } 
     }
     
     
    
    
}