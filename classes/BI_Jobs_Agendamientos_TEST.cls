@isTest
private class BI_Jobs_Agendamientos_TEST {

	static{
    	
    	BI_TestUtils.throw_exception = false;
    }
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for generateStrProg method
            
    History:  
    
    <Date>                  <Author>                <Change Description>
    29/09/2014              Ignacio Llorca            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
	static testMethod void testJobLineaVenta() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

    	List <String> lst_pais =new List <String>();
        lst_pais.add('Chile');
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);	

        list <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c'];
		
		BI_Modelo__c modelo = new BI_Modelo__c(Name = 'test modelo' ,RecordTypeId = lst_rt[0].Id);
		insert modelo;

		List <BI_Descuento__c> lst_desc = new List <BI_Descuento__c>();     

        for (Integer i=0;i<200;i++){
	        BI_Descuento__c disc = new BI_Descuento__c(BI_Cliente__c = lst_acc[0].Id,
	        										   BI_Descuento_aplica__c = 'Ambos');
	        lst_desc.add(disc);
    	}    	
    	insert lst_desc;
    

    	List <BI_Descuento_para_modelo__c> lst_mod = new List <BI_Descuento_para_modelo__c>();
    	for (Integer i=0;i<200;i++){
    		BI_Descuento_para_modelo__c mod = new BI_Descuento_para_modelo__c(BI_Descuento__c = lst_desc[0].Id,
    																		  BI_Estado__c = 'Activo',
    																		  BI_ID_de_Modelo__c = modelo.Id);
    		lst_mod.add(mod);
    	}
    	insert lst_mod; 
    	
		List <Id>lst_ven = new List <Id>();
		Map<Id,String> result = new Map <Id, String>();
		Map<Id,String> mline = new Map <Id, String>();
		String email = 'BIEN@aborda.es';
		List <Id>lst_rec = new List <Id>();
		List <Id>lst_serv = new List <Id>();
		list<BI_Linea_de_Venta__c> lstNewLineaVenta;
		System.runAs(usu){
			lstNewLineaVenta = BI_DataLoadAgendamientos.loadLineaVentaWithSolicitud(3);
		}

		for(BI_Linea_de_Venta__c ven:lstNewLineaVenta){
			ven.BI_Modelo__c = modelo.Id;
			ven.BI_Codigo_de_descuento_de_modelo__c = lst_mod[0].Id;
			ven.BI_Nuevo_codigo_de_cliente__c = false;

			ven.BI_Codigo_de_descuento_de_SIM__c = null;
			ven.BI_Bolsa_de_dinero_modelo_TEM__c = 'test';
			ven.BI_Bolsa_de_dinero_SIM_TEM__c = 'test';	
		}	
		update lstNewLineaVenta;


		for(BI_Linea_de_Venta__c ven:lstNewLineaVenta){
			lst_ven.add(ven.Id);
			mline.put(ven.Id, ven.Name);

		}		
		
		BI_Jobs.Linea_JOB(email, lst_ven, 2, result, mline, 0);
		BI_Jobs.BackOffice_JOB(email, lst_ven, 2, result, 0);
		mline = new Map<Id, String>();		
		
		
	}
	static testMethod void testJobLineaServicio() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

    	
		List <Id>lst_ven = new List <Id>();
		Map<Id,String> result = new Map <Id, String>();
		Map<Id,String> mline = new Map <Id, String>();
		String email = 'BIEN@aborda.es';
		List <Id>lst_rec = new List <Id>();
		List <Id>lst_serv = new List <Id>();		

		list<BI_Linea_de_Servicio__c> lstNewLineaServicio = BI_DataLoadAgendamientos.loadLineaServicio(1);
		mline = new Map<Id, String>();

		BI_Jobs.Linea_JOB(email, lst_ven, 0, result, mline, 2);
		BI_Jobs.BackOffice_JOB(email, lst_ven, 0, result, 2);

		for(BI_Linea_de_Servicio__c ven:lstNewLineaServicio){
			lst_serv.add(ven.Id);
			mline.put(ven.Id, ven.Name);

		}
		
		BI_Jobs.Linea_JOB(email, lst_serv, 2, result, mline, 2);
		BI_Jobs.BackOffice_JOB(email, lst_serv, 2, result, 2);

		
	}

	/*static testMethod void testJobLineaRecambio() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;


		List <String> lst_pais =new List <String>();
        lst_pais.add('Chile');
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);	

        list <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c'];
		
		BI_Modelo__c modelo = new BI_Modelo__c(Name = 'test modelo' ,RecordTypeId = lst_rt[0].Id);
		insert modelo;

		List <BI_Descuento__c> lst_desc = new List <BI_Descuento__c>();     

        for (Integer i=0;i<200;i++){
	        BI_Descuento__c disc = new BI_Descuento__c(
	        											BI_Cliente__c = lst_acc[0].Id,
	        											BI_Descuento_aplica__c = 'Ambos'
	        										  );
	        lst_desc.add(disc);
    	}    	
    	insert lst_desc;
    
    	List <BI_Descuento_para_modelo__c> lst_mod = new List <BI_Descuento_para_modelo__c>();	
    	for (Integer i=0;i<200;i++){
    		BI_Descuento_para_modelo__c mod = new BI_Descuento_para_modelo__c(BI_Descuento__c = lst_desc[0].Id,
    																		  BI_Estado__c = 'Activo',
    																		  BI_ID_de_Modelo__c = modelo.Id);
    		lst_mod.add(mod);
    	}
    	insert lst_mod; 

			list<BI_Linea_de_Recambio__c> lstRecambio = new list<BI_Linea_de_Recambio__c>();
			list<BI_Linea_de_Servicio__c> lstLineaServicio = new list<BI_Linea_de_Servicio__c>();


			String op1 =  'RFC';

			Account acc = new Account(Name = 'test Account',
								BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
								BI_Activo__c = Label.BI_Si,
								BI_Holding__c = 'No',
								BI_Tipo_de_identificador_fiscal__c = op1,
								BI_No_Identificador_fiscal__c = 'VECB380326XXX');
			        
			insert acc;


			BI_Solicitud_envio_productos_y_servicios__c solicitud = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'test solicitud 1',
		    															BI_Cliente__c = acc.Id);
			insert solicitud;
			
			BI_Linea_de_Recambio__c lineaCDescuento = new BI_Linea_de_Recambio__c();
			lineaCDescuento.BI_Solicitud__c =solicitud.Id;
			lineaCDescuento.BI_Modelo__c =modelo.Id;
			//lineaCDescuento.BI_Total_descuento_por_modelo__c = 210;
			//lineaCDescuento.BI_Total_descuento_por_SIM__c = 100 ;
			lineaCDescuento.BI_Estado__c = 'Activado';
			lineaCDescuento.BI_Razon_para_recambio__c = 'test razon';
			lineaCDescuento.BI_Telefono_de_contacto__c = '666563221';
			lineaCDescuento.BI_Modalidad_de_venta__c = 'test modalidad';
			lineaCDescuento.BI_Tipo_de_contrato__c = 'test tipo contrato';
			lineaCDescuento.BI_Plan__c = 'test plan';
			
			insert lineaCDescuento;

			List <BI_Descuento_para_modelo__c> lst_desc_mod  = [ SELECT Id, BI_Estado__c, BI_ID_de_Modelo__c, BI_Descuento__c, 
															BI_Descuento__r.BI_Descuento_aplica__c 
															FROM BI_Descuento_para_modelo__c 
															WHERE Id = :lst_mod[0].Id 
													  	   ];
		
			for (integer j = 0 ; j < 3 ; j++ ){
				
				BI_Linea_de_Recambio__c linea = new BI_Linea_de_Recambio__c();
				linea.BI_Solicitud__c = null ;
				linea.BI_ID_de_modelo__c  = modelo.Id;
				linea.BI_Modelo__c = modelo.Id;//modelo.Id;
				linea.BI_Solicitud_TEMP__c = string.valueOf( solicitud.Id );
				linea.BI_Modelo_TEM__c = string.valueOf( modelo.Id );
				linea.BI_Codigo_de_descuento_de_modelo__c = lst_desc_mod.get(0).Id;
				linea.BI_Codigo_de_Descuento_TEM__c = string.valueOf(lineaCDescuento.Id);
				linea.BI_Codigo_de_descuento_de_SIM__c = null;
				linea.BI_Descuento_SIM_TEM__c = string.valueOf(lineaCDescuento.Id);
				//linea.BI_Total_descuento_por_modelo__c = 210;
				//linea.BI_Total_descuento_por_SIM__c = 100 ;
				linea.BI_Estado__c = 'test estado';
				linea.BI_Razon_para_recambio__c = 'test razon';
				linea.BI_Telefono_de_contacto__c = '666563221';
				linea.BI_Modalidad_de_venta__c = 'test modalidad';
				linea.BI_Tipo_de_contrato__c = 'test tipo contrato';
				linea.BI_Plan__c = 'test plan';
				linea.BI_Catalogo_de_Servicio_1_TEM__c = 'test servicio 1';
				linea.BI_Accion_1_TEM__c = ' test Action 1';
				linea.BI_Estado_1_TEM__c = 'test estado 1';
				linea.BI_Catalogo_de_Servicio_2_TEM__c = 'test servicio 2';
				linea.BI_Accion_2_TEM__c = ' test Action 2';
				linea.BI_Estado_2_TEM__c = 'test estado 2';					
				linea.BI_Catalogo_de_Servicio_3_TEM__c = 'test servicio 3';
				linea.BI_Accion_3_TEM__c = ' test Action 3';
				linea.BI_Estado_3_TEM__c = 'test estado 3';
				linea.BI_Catalogo_de_Servicio_4_TEM__c = 'test servicio 4';
				linea.BI_Accion_4_TEM__c = ' test Action 4';
				linea.BI_Estado_4_TEM__c = 'test estado 4';
				linea.BI_Catalogo_de_Servicio_5_TEM__c = 'test servicio 5';
				linea.BI_Accion_5_TEM__c = ' test Action 5';
				linea.BI_Estado_5_TEM__c = 'test estado 5';
				linea.BI_Catalogo_de_Servicio_6_TEM__c = 'test servicio 6';
				linea.BI_Accion_6_TEM__c = ' test Action 6';
				linea.BI_Estado_6_TEM__c = 'test estado 6';		
				linea.BI_Catalogo_de_Servicio_7_TEM__c = 'test servicio 7';
				linea.BI_Accion_7_TEM__c = ' test Action 7';
				linea.BI_Estado_7_TEM__c = 'test estado 7';		
				linea.BI_Catalogo_de_Servicio_8_TEM__c = 'test servicio 8';
				linea.BI_Accion_8_TEM__c = ' test Action 8';
				linea.BI_Estado_8_TEM__c = 'test estado 8';		
				linea.BI_Catalogo_de_Servicio_9_TEM__c = 'test servicio 9';
				linea.BI_Accion_9_TEM__c = ' test Action 9';
				linea.BI_Estado_9_TEM__c = 'test estado 9';		
				linea.BI_Catalogo_de_Servicio_10_TEM__c = 'test servicio 10';
				linea.BI_Accion_10_TEM__c = ' test Action 10';
				linea.BI_Estado_10_TEM__c = 'test estado 10';
				linea.BI_Estado_de_linea_de_recambio__c = 'test estado 1';		
																						
				lstRecambio.Add(linea);
				
				system.debug( '## linea' + j + ': ' + linea);
		        	
			}
			
			insert lstRecambio;

		List <Id>lst_ven = new List <Id>();
		Map<Id,String> result = new Map <Id, String>();
		Map<Id,String> mline = new Map <Id, String>();
		String email = 'BIEN@aborda.es';
		List <Id>lst_rec = new List <Id>();
		List <Id>lst_serv = new List <Id>();	
	

		
		BI_Jobs.Linea_JOB(email, lst_ven, 2, result, mline, 0);
		BI_Jobs.BackOffice_JOB(email, lst_ven, 2, result, 0);
		mline = new Map<Id, String>();
		for(BI_Linea_de_Recambio__c ven:lstRecambio){
			lst_rec.add(ven.Id);
			mline.put(ven.Id, ven.Name);

		}
		BI_Jobs.Linea_JOB(email, lst_rec, 2, result, mline, 1);
		BI_Jobs.BackOffice_JOB(email, lst_rec, 2, result, 1);
	}*/
}