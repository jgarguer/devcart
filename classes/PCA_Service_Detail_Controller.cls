public class PCA_Service_Detail_Controller {
    public NE__OrderItem__c configurationItem{get;set;}
    public String numberName{get;set;}
    public String site{get;set;}
    public String productName{get;set;}
    public String categories{get;set;}
    public String status{get; set;}
    public String RFBdate {get; set;}
    public String RFSdate {get; set;}
    public String billingEndDate {get; set;}
    public String serviceAccount {get; set;}
    public String billingAccount {get; set;}
    public String esEordering {get; set;}
    public String  techBehaviour {get; set;}
    public String returnPage {get; set;}
    public Service service {get; set;}
    public String attributesHTML {get; set;}
    public String ccHierarchy {get; set;}
    public boolean hasOrders {get; set;}
    public Attachment myAttachment {get; set;}
    public boolean hasSub {get; set;}
    public String myCurrencyCode {get;set;}
    public String availableChanges {get;set;}
    public String suId{get; set;}
    public Boolean hasAttachment {get; set;}
    public List<Attachment> myAttachmentList {get; set;}
    public String serviceStatus {get; set;}
    public Map<Id, NE__OrderItem__c> map_OrderItems {get;set;}
    //public Map<Id, NE__Order_Item_Attribute__c> map_OrderItems {get;set;}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:  Controller of the service detail view
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_Service_Detail_Controller () {
        suId = ApexPages.currentPage().getParameters().get('suId');


        this.map_OrderItems = new Map<Id, NE__OrderItem__c>(TGS_Portal_Utils.getChildOrderItems(suId));
        //map_OrderItemsAttributes = new Map<Id,NE__Order_Item_Attribute__c>(TGS_Portal_Utils.getAttributesbyOrderItem(map_OrderItems.keySet()));


        NE__OrderItem__c[] configurationItems = TGS_Portal_Utils.getConfigurationItemsBySuId2(suId);
        List<Attachment> listAttachment = TGS_Portal_Utils.getServiceUnitAtt(suId);
        
        System.debug('[PCA_Service_Detail_Controller Constructor] listAttachment: '+listAttachment);
        
        hasAttachment=false;
        myCurrencyCode=  getUserCurrencyCode(UserInfo.getUserId());
        
        System.debug('[PCA_Service_Detail_Controller Constructor] myCurrencyCode: '+myCurrencyCode);
        
        if (listAttachment.size()>0){
            hasAttachment=true;
            myAttachmentList=listAttachment;
        }
        
        if (configurationItems.size()>0){
            configurationItem = configurationItems[0];
            serviceStatus=configurationItem.TGS_Service_status__c;
            if (!Constants.CI_STATUS_ACTIVE.equalsIgnoreCase(configurationItem.NE__Status__c)){
                hasOrders = true;
            }
            productName = configurationItem.NE__ProdId__r.Name;
            categories = '';
            
            System.debug('[PCA_Service_Detail_Controller Constructor] configurationItem: '+configurationItem);
            
            if (configurationItem.NE__ProdId__r.TGS_CWP_Tier_1__c != null 
                && configurationItem.NE__ProdId__r.TGS_CWP_Tier_1__c != 'TGSOL Catalog'){
                    categories = configurationItem.NE__ProdId__r.TGS_CWP_Tier_1__c + ' > ';
            }
            categories += configurationItem.NE__ProdId__r.TGS_CWP_Tier_2__c;
            
            System.debug('[PCA_Service_Detail_Controller Constructor] categories: '+categories);
            
            status = configurationItem.TGS_Service_Status__c;
            
            System.debug('[PCA_Service_Detail_Controller Constructor] Status: '+status);
            
            if (configurationItem.NE__OrderId__r.Case__r.TGS_RFB_date__c != null) {
                RFBdate = configurationItem.NE__OrderId__r.Case__r.TGS_RFB_date__c.format();
                
                System.debug('[PCA_Service_Detail_Controller Constructor] RFBdate: '+RFBdate);
            }
            
            if (configurationItem.NE__OrderId__r.Case__r.TGS_RFS_date__c != null) {
                RFSdate = configurationItem.NE__OrderId__r.Case__r.TGS_RFS_date__c.format();
                
                System.debug('[PCA_Service_Detail_Controller Constructor] RFSdate: '+RFSdate);
            }
            
            if (configurationItem.TGS_Billing_end_date__c != null) {
                billingEndDate= configurationItem.TGS_Billing_end_date__c.format();
                
                System.debug('[PCA_Service_Detail_Controller Constructor] billingEndDate: '+billingEndDate);
            }
            
            //numberName
            numberName = configurationItem.Name;
            
            System.debug('[PCA_Service_Detail_Controller Constructor] numberName: '+numberName);
            
            //site
            if (configurationItem.Installation_point__c != null){
                BI_Punto_de_instalacion__c nameSite = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Id =: configurationItem.Installation_point__c][0];
                site = nameSite.Name;
                
                System.debug('[PCA_Service_Detail_Controller Constructor] site: '+site);
            }
            
NE__Order__c configuration= [SELECT Name, NE__ServAccId__c,NE__ServAccId__r.Name, NE__BillAccId__r.Name, NE__BillAccId__c FROM NE__Order__c WHERE Id IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE Id=:configurationItem.Id) LIMIT 1];
            
            System.debug('[PCA_Service_Detail_Controller Constructor] configuration.Name '+configuration.Name);            
            
            
            
            /*
            serviceAccount = configurationItem.NE__Service_Account_Asset_Item__r.Name;
            billingAccount = configurationItem.NE__Billing_Account_Asset_Item__r.Name;
            
            System.debug('[PCA_Service_Detail_Controller Constructor] serviceAccount: '+serviceAccount);
            System.debug('[PCA_Service_Detail_Controller Constructor] billingAccount: '+billingAccount);
            */

            /*JCT 28/07/2016 Commented the two lines below, added NE__ServAccId__r.Name, NE__BillAccId__r.Name, on the upper query.
            serviceAccount = [SELECT Name FROM Account WHERE Id=: configuration.NE__ServAccId__c LIMIT 1].Name;
            
            billingAccount = [SELECT Name FROM Account WHERE Id=: configuration.NE__BillAccId__c LIMIT 1].Name;*/

            
            service = new Service(suId, productName,configurationItem.NE__OneTimeFeeOv__c, configurationItem.NE__RecurringChargeOv__c, map_OrderItems, true);
            
            System.debug('[PCA_Service_Detail_Controller Constructor] Service: '+service);
            
            attributesHTML =  '';
            getHTML(service, myCurrencyCode);
            hasSub=false;
            List<Service> mysubservices = service.subservices;
            if (mysubservices.size()>0){
                hasSub=true;
            }
            ccHierarchy = '';
            //JCT 28/07/2016 Wrapped the ccHierachy making sentences the (configuration.NE__ServAccId__c!=null) if sentence.
            if(configuration.NE__ServAccId__c!=null){
                    if(configuration.NE__ServAccId__r.Name != null){
                        serviceAccount = configuration.NE__ServAccId__r.Name;
                    }else{
                        serviceAccount = 'Service Account';
                    }
                    if(configuration.NE__BillAccId__r.Name != null){
                        billingAccount = configuration.NE__BillAccId__r.Name;
                    }else{
                        billingAccount = 'Billing Account';
                    }
                    
                   
                    System.debug('[PCA_Service_Detail_Controller Constructor] billingAccount: '+billingAccount);
                    System.debug('[PCA_Service_Detail_Controller Constructor] serviceAccount: '+serviceAccount);
                    
                    ccHierarchy = '';
                    //serviceLevel = TGS_Portal_Utils.getAccountLevelForAccount(configurationItem.NE__Service_Account_Asset_Item__r.Id);
                    Integer serviceLevel = TGS_Portal_Utils.getAccountLevelForAccount(configuration.NE__ServAccId__c);   
                    //billingLevel = TGS_Portal_Utils.getAccountLevelForAccount(configurationItem.NE__Billing_Account_Asset_Item__r.Id);
                    Integer billingLevel = TGS_Portal_Utils.getAccountLevelForAccount(configuration.NE__BillAccId__c);
                    
                    System.debug('[PCA_Service_Detail_Controller Constructor] serviceLevel: '+serviceLevel);
                    System.debug('[PCA_Service_Detail_Controller Constructor] billingLevel: '+billingLevel);
                    
                    


                    Id auxId;
                    integer auxLevel;
                   if(serviceLevel > billingLevel){
                            //auxId = configurationItem.NE__Service_Account_Asset_Item__r.Id;
                            auxId = configuration.NE__ServAccId__c;
                            auxLevel = serviceLevel;
                   }
                    else{
                            //auxId = configurationItem.NE__Billing_Account_Asset_Item__r.Id;
                            auxId = configuration.NE__BillAccId__c;
                            auxLevel = billingLevel;
                   }
                        

                   if(auxLevel>=1){
                            ccHierarchy += (TGS_Portal_Utils.getLevel1(auxId,1)[0]).Name;
                   }
                   if(auxLevel>=2){
                            ccHierarchy += '  >  ' + (TGS_Portal_Utils.getLevel2(auxId,1)[0]).Name;
                    }
                   if(auxLevel>=3){
                            ccHierarchy += '  >  ' + (TGS_Portal_Utils.getLevel3(auxId,1)[0]).Name;
                   }
                   if(auxLevel>=4){
                            ccHierarchy += '  >  ' + (TGS_Portal_Utils.getLevel4(auxId,1)[0]).Name;
                   }
                   if(auxLevel>=5){
                            ccHierarchy += '  >  ' + (TGS_Portal_Utils.getLevel5(auxId,1)[0]).Name;
                   }
                }
    }

    //END JCT 28/07/2016
        System.debug('[PCA_Service_Detail_Controller Constructor] ccHierarchy: '+ccHierarchy);
        
        eOrderingItemAndChanges (configurationItem.NE__CatalogItem__c);
    }
    
    /**************************
     * PUBLIC METHODS
     * **********************/
    /*
     * Method:         eOrderingItemAndChanges
     * Description:    Detecta si el CatalogItem es eOrdering y se se le permite hacer un ticket tipo change.
     */
    public void eOrderingItemAndChanges (Id myCatIt) {
        List<NE__Catalog_Item__c> listCatIt= new List<NE__Catalog_Item__c>();
        listCatIt=[SELECT NE__Technical_Behaviour__c FROM NE__Catalog_Item__c WHERE Id= :myCatIt LIMIT 1];
        techBehaviour=listCatIt[0].NE__Technical_Behaviour__c;
        if (techBehaviour.contains('Service not available for e-Ordering')){
            esEordering='False';
        }
        else {
           esEordering='True';
        }
        if (techBehaviour.contains('Service with pre-approved changes')){
            availableChanges='True';
        }
        else {
            availableChanges='False';
        }
        System.debug('[PCA_Service_Detail_Controller.eOrderingItemAndChanges] esEordering: '+esEordering);
        System.debug('[PCA_Service_Detail_Controller.eOrderingItemAndChanges] availableChanges '+availableChanges);
    }
   
    /*
     * Method:         getHTML
     * Description:    Crea el HTML correspondiente a los atributos del servicio.
     */
    public void getHTML(Service s, String currCod){
        System.debug('ASDFGHJKL134'+s);
        if(s.hasAttributes){
            
            System.debug('[PCA_Service_Detail_Controller.getHTML] s.listAttributes: '+s.listAttributes);
            
            for( ItemAttribute attribute : s.listAttributes){
                String attributeWithSpaces = insertSpacesInAttribute(attribute.name);
                attributesHTML +=  '<div class=\"halfWrapper\" style=\"margin-bottom:5px;\">';
                attributesHTML +=      '<label style=\"margin-right: 5%;width: 33%; float: left; word-wrap: break-word;\">'+attributeWithSpaces+': </label>';
                attributesHTML +=      '<div style= \"word-wrap: break-word; margin-right:5%; width:57%; float: left;display:block;\">';
                if(attribute.value != null){
                    attributesHTML += attribute.value;
                }
                else{
                    attributesHTML += '-';
                }
                attributesHTML +=  '</div>';
                if(!String.isBlank(attribute.description)) {
                    attributesHTML += '<div style="float:left; font-size: .9em; padding-left: 10px; padding-bottom: 10px;">' + attribute.description + '</div>';
                }
                attributesHTML +=  '</div>';
            }

            
            for(Service subs : s.subservices){
                if(s.id == service.id){
                    system.debug('[PCA_Service_Detail_Controller.getHTML] Base on time fee: '+subs.BaseOneTimeList[0]);
                    system.debug('[PCA_Service_Detail_Controller.getHTML] Base recurring charge: '+subs.BaseRecurringList[0]);
                    
                    if((subs.BaseOneTimeList[0]!=0.00) && (subs.BaseRecurringList[0]!=0.00)){
                        attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                        '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:33%; float:left;\">'+subs.name+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:33%; float:left;\">'+ 'Base One Time Fee: '+currCod+' '+subs.BaseOneTimeList[0]+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:33%; float:left;\">'+'Base Recurring Charge: '+currCod+' '+subs.BaseRecurringList[0]+'</h2><div class="clear"></div>';
                    }
                    else{ 
                        if(subs.BaseOneTimeList[0]==0.00){
                            if(subs.BaseRecurringList[0]==0.00){
                                attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                                '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:100%; float:left;\">'+subs.name+'</h2><div class="clear"></div>';
                            } 
                            else{
                                attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                                '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+subs.name+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+ 'Base Recurring Charge: '+currCod+' '+ subs.BaseRecurringList[0]+'</h2><div class="clear"></div>';
                            }
                        
                        }
                        else {
                            if(subs.BaseRecurringList[0]==0.00){
                                attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                                 '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+subs.name+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+ 'Base One Time Fee: '+currCod+' '+subs.BaseOneTimeList[0]+'</h2><div class="clear"></div>';
                            }

                        }
                    }
                }
                else{
                    attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+'\" style=\"margin-top: 10px;\"><h2>'+subs.name+'</h2><div class="clear"></div>';
                }
                getHTML(subs, myCurrencyCode);
                attributesHTML +=  '</div>';
            }
        }
        else{
            
            System.debug('[PCA_Service_Detail_Controller.getHTML] s.subservices: '+s.subservices);
            
            for(Service subs : s.subservices){
                if(s.id == service.id){
                    system.debug('[PCA_Service_Detail_Controller.getHTML] Base on time fee: '+subs.BaseOneTimeList[0]);
                    system.debug('[PCA_Service_Detail_Controller.getHTML] Base recurring charge: '+subs.BaseRecurringList[0]);
                    if((subs.BaseOneTimeList[0]!= 0.00) && (subs.BaseRecurringList[0]!= 0.00)){
                        attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                        '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:33%; float:left;\">'+subs.name+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:33%; float:left;\">'+ 'Base One Time Fee: '+currCod+' '+subs.BaseOneTimeList[0]+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:33%; float:left;\">'+'Base Recurring Charge: '+currCod+' '+subs.BaseRecurringList[0]+'</h2><div class="clear"></div>';
                    }
                    else{
                        if(subs.BaseOneTimeList[0]==0.00){
                            if(subs.BaseRecurringList[0]==0.00){
                                attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                                '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:100%; float:left;\">'+subs.name+'</h2><div class="clear"></div>';
                            } 
                            else{
                                attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                                '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+subs.name+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+ 'Base Recurring Charge: '+currCod+' '+ subs.BaseRecurringList[0]+'</h2><div class="clear"></div>';
                            }
                        
                        }
                        else {
                            if(subs.BaseRecurringList[0]==0.00){
                                attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+
                                 '\" style=\"margin-top: 15px;\"><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+subs.name+'</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;\">'+ 'Base One Time Fee: '+currCod+' '+subs.BaseOneTimeList[0]+'</h2><div class="clear"></div>';
                            }  
                        }
                    }
                }
                    
                else{
                    attributesHTML += '<div class="clear"></div><div id=\"'+subs.id+'\" style=\"margin-top: 10px;\"><h2>'+subs.name+'</h2><div class="clear"></div>';
                }
                getHTML(subs, myCurrencyCode);
                attributesHTML +=  '</div>';
            }
        }
    }
    
    public String insertSpacesInAttribute(String attributeName){
        List<Integer> attributeNameChars = attributeName.getChars();
        for (Integer i=2; i< attributeNameChars.size(); i++){
            integer previousSize = attributeNameChars.size();
            if(attributeNameChars.get(i) >= 97 && attributeNameChars.get(i) <= 122 && attributeNameChars.get(i-1) >= 65 && attributeNameChars.get(i-1) <= 90){
                attributeNameChars.add(i-1, 32);
            }
            if(attributeNameChars.get(i-1) >= 97 && attributeNameChars.get(i-1) <= 122 && attributeNameChars.get(i) >= 65 && attributeNameChars.get(i) <= 90){
                attributeNameChars.add(i, 32);
            }
            if(previousSize < attributeNameChars.size()){
                i++;
            }
        }
        return String.fromCharArray(attributeNameChars);
    }
    
    /*
     * Method:         getUserCurrencyCode
     * Description:    Devuelve el tipo de moneda del usuario.
     */
    public static String getUserCurrencyCode (Id actualUserId){
        List<User> users= [SELECT DefaultCurrencyIsoCode FROM User WHERE Id= : actualUserId];
        String currencyCode = '';
        if (users.size() > 0){
            currencyCode = users.get(0).DefaultCurrencyIsoCode;
        }
        System.debug('[PCA_Service_Detail_Controller.getUserCurrencyCode] currencyCode: '+currencyCode);
        return currencyCode;
    }
    
    /** Buttons **/
    /*
     * Method:         newModification
     * Description:    Método que se lanza cuando se pulsa el botón "New Order"-->"Modification".
     */
    public PageReference newModification() {
        NE__OrderItem__c currentCI;
        
        //Calling JS_RemoteMethods
        if(!Test.isRunningTest()){
            returnPage = NE.JS_RemoteMethods.requestChangeAssetItem(suId, 'ChangeOrder');
        }else{
            if(suId == null){
                returnPage = 'KO';    
            }else{
                returnPage = 'OK';    
            }
        }
        
        System.debug('[PCA_Service_Detail_Controller.newModification] returnPage: '+returnPage);
        if (returnPage != 'KO') {
            NE__OrderItem__c[] selectedCIS = TGS_Portal_Utils.getConfigurationItemsBySuId(suId); 
            if (selectedCIS.size() > 0) {
                currentCI = selectedCIS[0];
                currentCI.NE__Status__c = Constants.CI_STATUS_IN_PROGRESS;
                
                System.debug('[PCA_Service_Detail_Controller.newModification] status: '+currentCI.NE__Status__c);
                update currentCI;
            }
            String parameters = returnPage.substringAfter('?');
            
            System.debug('[PCA_Service_Detail_Controller.newModification] parameters: ' + parameters);  
            
            return new PageReference('/PCA_Order_Redirect?' + parameters + '&isExtBw=true');
        }
        
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('suId', suId);
        pr.getParameters().put('error', 'The modification order has not been created');
        return pr;
    }
    
    /*
     * Method:         newTermination
     * Description:    Método que se lanza cuando se pulsa el botón "New Order"-->"Termination".
     */    
    public PageReference newTermination(){
        if(!Test.isRunningTest()){
            returnPage = NE.JS_RemoteMethods.requestChangeAssetItem(suId, 'Disconnection');
        }else{
            if(suId == null){
                returnPage = 'KO';    
            }else{
                returnPage = 'OK/' + configurationItem.NE__OrderId__c;    
                
            }
        }
        if (returnPage != 'KO') {
            String orderId= returnPage.substringAfterLast('/');
            
            System.debug('[PCA_Service_Detail_Controller.newTermination] orderId: ' + orderId); 
            
            if (orderId != null && orderId!= '') {
                NE__Order__c configuration = TGS_Portal_Utils.getOrder(orderId);
                NE__OrderItem__c[] selectedCIS = TGS_Portal_Utils.getConfigurationItemsBySuId(suId); 
                if (selectedCIS.size() > 0) {
                    NE__OrderItem__c currentCI = selectedCIS[0];
                    currentCI.NE__Status__c = Constants.CI_STATUS_IN_PROGRESS;
                    
                    System.debug('[PCA_Service_Detail_Controller.newTermination] currentCI: '+currentCI);
                    update currentCI;
                    return new PageReference('/PCA_Cases?successOrder=true&orderNumber=' + configuration.Case__r.caseNumber);
                }
            }
        }
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('suId', suId);
        pr.getParameters().put('error', 'The termination order has not been created');
        return pr;
    }
    
    /*************************************
     *  INNER CLASSES
     * *********************************/
    /*
     * Class:         ItemAttribute
     * Description:   Contiene la información de un attributo (nombre, valor y CSUID).
     */
    private Class ItemAttribute {
        public String name {get; set;}
        public String value {get; set;}
        public String description {get; set;}
        public String csuid {get; set;}
        public ItemAttribute(String name, String value, String csuid, String description) {
            this.name = name;
            this.value = value;
            this.csuid = csuid;
            this.description = description;
        }
    }
    
    /*
     * Class:         Service
     * Description:   Contiene la información de un servicio.
     */
    private Class Service{
        public String id {get; set;}
        public String name {get; set;}
        public List<Decimal> BaseOneTimeList {get;set;}
        public List<Decimal> BaseRecurringList {get;set;}
        public List<Service> subservices {get; set;}
        public boolean hasAttributes {get; set;} //not direcly;maybe its subservices
        public List<ItemAttribute> listAttributes {get; set;}
        public Map<Id,NE__OrderItem__c> map_OrderItems {get;set;}
        
        /*
        * Method:        Service
        * Description:   Crea un servicio a partir de unos datos de entrada.
        */
        public Service(String id, String name, Decimal BaseOneTime, Decimal BaseRecurring) {
            this.id = id;
            this.name = name;
            this.hasAttributes =false;
            this.subservices = getSubservices(id);
            //this.hasAttributes = false;
            
            this.listAttributes = getAttributes(id);
                if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
                }
                else{this.hasAttributes = false;}
            
            this.BaseOneTimeList=new List<Decimal>();
            this.BaseOneTimeList.add(BaseOneTime);
            this.BaseRecurringList=new List<Decimal>();
            this.BaseRecurringList.add(BaseRecurring);
            if(this.subservices.size()==0){
                this.listAttributes = getAttributes(id);
                if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
                }
                else{this.hasAttributes = false;}
            }
        }
        
        public Service(String id, String name, Decimal BaseOneTime, Decimal BaseRecurring, Map<Id, NE__OrderItem__c> map_Children, boolean isFirstParent) {
            this.map_OrderItems = map_Children;
            this.id = id;
            this.name = name;
            this.subservices = new List<Service>();
            this.hasAttributes =false;
            if(isFirstParent) this.subservices = getSubservices(map_Children);

            this.listAttributes = getParentAttributes(id);
                if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
                }
                else{this.hasAttributes = false;}
            
            this.BaseOneTimeList=new List<Decimal>();
            this.BaseOneTimeList.add(BaseOneTime);
            this.BaseRecurringList=new List<Decimal>();
            this.BaseRecurringList.add(BaseRecurring);

            //this.hasAttributes = false;
            
           if(!this.subservices.isEmpty()){
             
            if(this.subservices.size()==0){
                this.listAttributes = getParentAttributes(id);
                if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
                }
                else{this.hasAttributes = false;}
            }
           }
        }

        public Service(NE__OrderItem__c param_orderItem, Map<Id, NE__OrderItem__c> map_OrderItems) {
            this.map_OrderItems = map_OrderItems;
            this.id = param_orderItem.Id;
            this.name = param_orderItem.Name+'  '+param_orderItem.NE__ProdName__c ;
            this.subservices = new List<Service>();
            this.hasAttributes =false;
            this.BaseOneTimeList=new List<Decimal>();
            this.BaseOneTimeList.add(param_orderItem.NE__OneTimeFeeOv__c);
            this.BaseRecurringList=new List<Decimal>();
            this.BaseRecurringList.add(param_orderItem.NE__RecurringChargeOv__c);
            //this.hasAttributes = false;
            
            if(map_OrderItems.get(this.Id) != null){
                if(map_OrderItems.get(this.id).NE__Order_Item_Attributes__r!=null){
                    this.hasAttributes = true;
                    this.listAttributes = this.getAttributes(this.id);
                }else{
                    this.hasAttributes = false;
                }
            }else{
                this.hasAttributes = false;
            }
           /*if(!this.subservices.isEmpty()){
             this.listAttributes = getAttributes(id);
                if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
                }
                else{this.hasAttributes = false;}
            if(this.subservices.size()==0){
                this.listAttributes = getAttributes(id);
                if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
                }
                else{this.hasAttributes = false;}
            }
           }*/
        }
        
        /*
        * Method:        getSubservices
        * Description:   Obtiene los subservicios de un servicio.
        */
        public List<Service> getSubservices(String id){
            List<NE__OrderItem__c> subservices = TGS_Portal_Utils.getConfItemSubser(id);
            List<Service> listSubservices = new List<Service>();
            Integer i=0;
            for(NE__OrderItem__c subservice : subservices) {
                Service auxSub = new Service(subservice.Id, subservice.NE__ProdId__r.Name,subservice.NE__OneTimeFeeOv__c,subservice.NE__RecurringChargeOv__c);
                listSubservices.add(auxSub); 

                if (auxSub.hasAttributes){this.hasAttributes = true;}
                if(i==4){
                    break;
                }

                i++;

            }
            
            System.debug('[PCA_Service_Detail_Controller.Service.getSubservices] listSubservices: '+listSubservices);
            return listSubservices;
        }
        
        public List<Service> getSubservices(Map<Id, NE__OrderItem__c> map_Children){
            List<NE__OrderItem__c> subservices = map_Children.values();
            List<Service> listSubservices = new List<Service>();
            Integer i=0;
            for(NE__OrderItem__c subservice : subservices) {
                Service auxSub = new Service(subservice, map_Children);
                listSubservices.add(auxSub); 

                if (auxSub.hasAttributes){this.hasAttributes = true;}
               /* if(i==10){
                    break;
                }

                i++;
*/
            }
            
            System.debug('[PCA_Service_Detail_Controller.Service.getSubservices] listSubservices: '+listSubservices);
            return listSubservices;
        }
        
        /*
        * Method:        getAttributes
        * Description:   Obtiene los atributos de un servicio.
        */
        public List<ItemAttribute> getAttributes(String id){
            Id aux = (Id) id;
            System.debug('Proactividad'+map_OrderItems.get(aux));
            List<NE__Order_Item_Attribute__c> attributes = this.map_OrderItems.get(id).NE__Order_Item_Attributes__r;
            List<ItemAttribute> listaAtributos = new List<ItemAttribute>();
            for(NE__Order_Item_Attribute__c attr : attributes) {
                 //validamos que no vengan caracteres especiales
                String value = attr.NE__Value__c;
                System.debug('Topdecked'+value);
                if (value != null && !''.equals(value)){
                    //Where:
                    //\w : A word character, short for [a-zA-Z_0-9]
                    //\W : A non-word character
                    //value = value.replaceAll('\\W', ' '); 
                    //Escapa los caracteres html
                    value = value.escapeHtml4();
                    //Escapa los caracteres unicode
                    value = value.escapeUnicode();
                }
                listaAtributos.add(new ItemAttribute(attr.Name, value, attr.TGS_CSUID__c, attr.NE__FamPropId__r.NE__PropId__r.NE__Description__c));
            }
            System.debug('[PCA_Service_Detail_Controller.Service.getAttributes]  listaAtributos: '+listaAtributos);
            return listaAtributos;
        }

        public List<ItemAttribute> getParentAttributes(String id){
            System.debug('Proactividad Padre'+map_OrderItems.get(id));
            List<NE__Order_Item_Attribute__c> attributes = [select Id, Name,NE__Value__c,TGS_CSUID__c, NE__FamPropId__r.NE__PropId__r.NE__Description__c from NE__Order_Item_Attribute__c where NE__Order_Item__c = :id];
            List<ItemAttribute> listaAtributos = new List<ItemAttribute>();
            for(NE__Order_Item_Attribute__c attr : attributes) {
                 //validamos que no vengan caracteres especiales
                String value = attr.NE__Value__c;
                if (value != null && !''.equals(value)){
                    //Where:
                    //\w : A word character, short for [a-zA-Z_0-9]
                    //\W : A non-word character
                    //value = value.replaceAll('\\W', ' '); 
                    //Escapa los caracteres html
                    value = value.escapeHtml4();
                    //Escapa los caracteres unicode
                    value = value.escapeUnicode();
                }
                listaAtributos.add(new ItemAttribute(attr.Name, value, attr.TGS_CSUID__c, attr.NE__FamPropId__r.NE__PropId__r.NE__Description__c));
            }
            System.debug('[PCA_Service_Detail_Controller.Service.getAttributes]  listaAtributos: '+listaAtributos);
            return listaAtributos;
        }
    }
}