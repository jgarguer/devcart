@isTest
private class PCA_EquipoTelefonicaCtrl_TEST {

    static testMethod void getloadInfo() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(2, accList);
            system.debug('con: '+con);                                          
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            User user2 = BI_DataLoad.loadSeveralPortalUsers(con[1].Id, BI_DataLoad.searchPortalProfile());
            Hierarchy__c hierar1 = new Hierarchy__c(ShowPersonalData__c=false,User__c=user1.Id,Contact__c=con[0].Id,Nivel_de_escalado__c='1º contacto');
            insert hierar1;
            Hierarchy__c hierar2 = new Hierarchy__c(ShowPersonalData__c=true,User__c=user2.Id,Contact__c=con[1].Id,Nivel_de_escalado__c='2º contacto');
            insert hierar2;
            List <BI_Contact_Customer_Portal__c> lst_ccp = new List <BI_Contact_Customer_Portal__c> ();
            BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(
                BI_User__c = user1.Id,
                BI_Cliente__c = acc[0].Id,
                BI_Contacto__c = con[0].Id,
                BI_Activo__c = true,
                BI_Perfil__c = Label.BI_Apoderado
            );
            lst_ccp.add(ccp);
            BI_Contact_Customer_Portal__c ccp2 = new BI_Contact_Customer_Portal__c(
                BI_User__c = user1.Id,
                BI_Cliente__c = acc[0].Id,
                BI_Contacto__c = con[1].Id,
                BI_Activo__c = true,
                BI_Perfil__c = Label.BI_Apoderado
            );
            lst_ccp.add(ccp2);
            Test.startTest();
            insert lst_ccp;
            Test.stopTest();
            AccountTeamMember AccMem = new AccountTeamMember(UserId=usr.Id,TeamMemberRole='Apoderado',AccountId=acc[0].Id);
            insert AccMem;
            
            
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = false;
	        	constructor.checkPermissions();
	        	constructor.loadInfo();
	        	constructor.defineRecords();
	        	constructor.defineViews();
	        	constructor.getPermissionTo_EquipoTelefonica();
	        	constructor.getPermissionTo_Escalamiento();
	        	constructor.getPermissionTo_EquipoCliente();
	        	constructor.defineSearch();
	        	constructor.searchRecords();
	        	constructor.getHasPrevious();
	        	constructor.getHasNext();
	        	constructor.Next();
	        	constructor.Previous();
	        	constructor.getItemPage();
	        	constructor.valuesInPage();
	        	constructor.Order();
	        	constructor.Order();
                
            }
             system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	        	constructor.checkPermissions();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.loadInfo();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.getPermissionTo_EquipoTelefonica();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.getPermissionTo_Escalamiento();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.getPermissionTo_EquipoCliente();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.defineSearch();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.searchRecords();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.getHasPrevious();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.getHasNext();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.Next();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.Previous();
            }
            system.runAs(user1){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = true;
	            constructor.Order();
            }
            system.runAs(user2){
            	PCA_EquipoTelefonicaCtrl constructor = new PCA_EquipoTelefonicaCtrl();
	            BI_TestUtils.throw_exception = false;	        	
	        	constructor.loadInfo();
            }

        }
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
	
    static testMethod void variableCalls() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PCA_EquipoTelefonicaCtrl telef= new PCA_EquipoTelefonicaCtrl();
        telef.ajaxParamValue = '';
        
        telef.detailsUser = new PCA_EquipoTelefonicaCtrl.detailsUserWrapper();
        telef.detailsUser.Role= '';
        telef.detailsUser.photo= '';
        telef.detailsUser.phone= '';
        telef.detailsUser.email= 'a@a.e';
        telef.detailsUser.AboutMe= '';
        telef.detailsUser.UserName='';
        telef.detailsUser.Bigphoto= '';
        telef.detailsUser.UserNameId='';
        telef.detailsUser.Description = '';
        telef.detailsUser.phoneMobile= '';
        telef.detailsUser.showPersonalData= false;
        
        PCA_EquipoTelefonicaCtrl.hierarchyWrapper hierarchy = new PCA_EquipoTelefonicaCtrl.hierarchyWrapper();
        hierarchy.Role= '';
        hierarchy.phone= '';
        hierarchy.photo= '';
        hierarchy.email= 'a@a.e';
        hierarchy.AboutMe= '';
        hierarchy.Bigphoto= '';
        hierarchy.UserName='';
        hierarchy.UserNameId='';
        hierarchy.Description= '';
        hierarchy.phoneMobile= '';
        hierarchy.NivelEscalado= '';
        hierarchy.showPersonalData= false;
        telef.listHierarchyWrapper = new List<PCA_EquipoTelefonicaCtrl.hierarchyWrapper>();
        telef.listHierarchyWrapper.add(hierarchy);
        
        telef.emailTo= '';
        telef.emailFrom= '';
        telef.emailBody= '';
        telef.emailPhone= '';
        telef.emailSubject= '';
        
        telef.haveError = false;
    }
}