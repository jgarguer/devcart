/* 
Copyright (c) 2011, salesforce.com, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer. 
    * Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution.
    * Neither the name of the salesforce.com, Inc. nor the names of its contributors 
    may be used to endorse or promote products derived from this software 
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.

*/
/******************************************************** Cambios ****************************************************************/
/* Razón del Cambio: Adaptaciones proyecto Milestone PM
Descripción del Cambio: Se modifica el proceso de clonado, para que en el nuevo elemento generado el propietario sea el usuario que clona.
Se añade una lógica de fechas al clonar, en la que se calcula un valor delta con la fecha del sistema menos la de kickoff del proyeto, el 
cuál es sumado al resto de fechas de los diferentes elemntos clonados.
Se añade un flag que identifica cuando se empieza el clonado y cuando finaliza para que no salte durante el proceso la validación de que el
proyecto debe de tener una oportunidad.
Autor: Capgemini
Fecha: Mayo 2016 
TAG: N0001 (OLA 4 y 5) */
/******************************************************** Cambios ****************************************************************/

public with sharing class Milestone1_Clone_Project_Controller {
    
    public Boolean showButton {get;set;} //JaimeRM
    public Milestone1_Project__c rec {get; set;}
    public Milestone1_Project__c dummyProj {get; set;}
    private List<Milestone1_Milestone__c> milestones {get; set;}
    private List<Milestone1_Task__c> tasks {get; set;}
    private List<User> statusList{get; set;}
    public Boolean isProjOwnerActive{get;set;}
    public boolean isInactiveUserToBeReplaced{get;set;}
    
    public Milestone1_Clone_Project_Controller(ApexPages.StandardController stc){
        //query project record
        rec = [SELECT Id,
                      Name,
                      Deadline__c,
                      Description__c,
                      Status__c,
                      Total_Expense_Budget__c,
                      Total_Hours_Budget__c,
                      KickOff__c,
                      BI_Project_Type__c,
                      BI_Adherence_HO_model__c,
                      //N0001 OwnerId,
                      BI_Country__c,
                      BI_O4_Leading_Channel__c
               FROM Milestone1_Project__c
               WHERE Id = :stc.getId()
              ];
        
        //query milestone records
        milestones = [SELECT Id,
                             Name,
                             Parent_Milestone__c,
                             Complete__c,
                             Deadline__c,
                             Description__c,
                             Expense_Budget__c,
                             Hours_Budget__c,
                             //N0001 OwnerId,
                             KickOff__c,
                            BI_country__c
                      FROM Milestone1_Milestone__c
                      WHERE Project__c = :rec.Id
                     ];
        
        //query task records 
        tasks = [SELECT Id,
                        Name,
                        Project_Milestone__c,
                        //N0001 Assigned_To__c,
                        Complete__c,
                        Description__c,
                        Start_Date__c,
                        Assigned_To__c,
                        Due_Date__c,
                        Priority__c,
                        Task_Stage__c,
                        Class__c,
                        BI_Assigned_Role__c,
                        Blocked__c,
                        Blocked_Reason__c,
                        Last_Email_Received__c,
                        Estimated_Hours__c,
                        Estimated_Expense__c,
                        //N0001
                 		BI_O4_Activity__c
                 		//N0001
                 FROM Milestone1_Task__c
                 WHERE Project_Milestone__r.Project__c = :rec.Id
                ];
                
        dummyProj = new Milestone1_Project__c();
        dummyProj.Name = 'Copy of ' + rec.Name;
        //N0001
        rec.OwnerId= UserInfo.getUserId();
        //N0001
        
        statusList = [SELECT IsActive from User where id = :rec.OwnerId];
        if(statusList  != null && statusList.size() > 0 )
            isProjOwnerActive = statusList[0].IsActive;
          //JaimeRM Demanda 357
        List<user> lst_users = [SELECT Profile.name from user where id =: UserInfo.getUserId()];
        if(lst_users.size() > 0  && lst_users[0].Profile.name == 'BI_HandOver')
            showButton = true;
        else 
            showButton = false;
    }
    
   public PageReference divisor_createClone(){
    return createClone();
   }


    public PageReference createClone(){      
        Savepoint preSave = Database.setSavepoint(); //set savepoint so we can rollback the whole save if there are errors
        PageReference pageReference = null;
        try{
            
          if(isProjOwnerActive == false){
                if(dummyProj.OwnerId == null){
                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Milestone1_InactiveUsersError);
                   ApexPages.addMessage(myMsg);
                   isInactiveUserToBeReplaced = true;
                   return pageReference;
                }if(dummyProj.OwnerId != null){
                    //N0001 rec.ownerId = dummyProj.OwnerId; 
                   //N0001
                   rec.ownerId = userInfo.getUserId(); 
                   //N0001
                }
            }             
            
            //create new project record
            rec.Name = dummyProj.Name;
            //N0001
            rec.BI_O4_Bypass__c = 1;
            
            integer deltaday = 0;
            if(rec.Kickoff__c != null && rec.BI_O4_Project_Customer__c != 'passclasstest'){
                deltaday = rec.Kickoff__c.daysbetween(system.today());
            }
            
            if (deltaday != 0 && rec.Kickoff__c != null){
                rec.Kickoff__c = rec.Kickoff__c.addDays(deltaday);
            }
            	            
            if (deltaday != 0 && rec.Deadline__c != null){
            	rec.Deadline__c = rec.Deadline__c.addDays(deltaday);
            }
            
            rec.OwnerId =userInfo.getUserId();
            //N0001
            
            Milestone1_Project__c newProj = Milestone1_Clone_Utility.cloneProject(rec);
            //newProj.BI_O4_Cloning_Bypass__c = true;
            insert newProj;
            //separate milestone records into parents and subs
            List<Milestone1_Milestone__c> topMilestones = new List<Milestone1_Milestone__c>();
            List<Milestone1_Milestone__c> bottomMilestones = new List<Milestone1_Milestone__c>();
            for(Milestone1_Milestone__c oldMS : milestones){
                // N0001
               	 if(deltaday != 0 && oldMS.Kickoff__c != null){
                    oldMS.Kickoff__c = oldMS.Kickoff__c.addDays(deltaday);
                 }
                 if(deltaday != 0 && oldMs.Deadline__c != null){
                   oldMs.Deadline__c = oldMs.Deadline__c.addDays(deltaday);
                 }
            	// N0001
                if(oldMS.Parent_Milestone__c == null){
                    oldMS.OwnerId =userInfo.getUserId(); /*Añadido para Demanda 357*/
                    topMilestones.add(oldMS);
                } else {
                    oldMS.OwnerId =userInfo.getUserId(); /*Añadido para Demanda 357*/
                    bottomMilestones.add(oldMS);
                }
            }            
            //clone and insert top milestone records
            Map<String, Milestone1_Milestone__c> newTopMilestoneMap = Milestone1_Clone_Utility.cloneMilestonesIntoMap(topMilestones);
            for(Milestone1_Milestone__c newMS : newTopMilestoneMap.values()){
                newMS.Project__c = newProj.Id;
                newMS.Parent_Milestone__c = null;
                newMS.Complete__c = false;
                newMS.Description__c = null;
            }
            insert newTopMilestoneMap.values();
            
            //clone and insert sub milestone records
            Map<String, Milestone1_Milestone__c> newBottomMilestoneMap = Milestone1_Clone_Utility.cloneMilestonesIntoMap(bottomMilestones);
            for(Milestone1_Milestone__c newMS : newBottomMilestoneMap.values()){
                newMS.Project__c = newProj.Id;
                newMS.Parent_Milestone__c = newTopMilestoneMap.get(newMS.Parent_Milestone__c).Id;
                newMS.Complete__c = false;
                newMS.Description__c = null;
            }
            insert newBottomMilestoneMap.values();
            
            //collect all milestones into one map
            Map<String, Milestone1_Milestone__c> allNewMilestoneMap = new Map<String, Milestone1_Milestone__c>();
            allNewMilestoneMap.putAll(newTopMilestoneMap);
            allNewMilestoneMap.putAll(newBottomMilestoneMap);
            
            //clone and insert task records
            for(Milestone1_Task__c task : tasks) {
              task.Assigned_To__c = userInfo.getUserId();  /*Añadido para Demanda 357*/
            }
            List<Milestone1_Task__c> newTasks = Milestone1_Clone_Utility.cloneTasksIntoList(tasks);
            for(Milestone1_Task__c newTask : newTasks){
				// N0001
                if(deltaday != 0 && newTask.Start_Date__c != null){
                    newTask.Start_Date__c = newTask.Start_Date__c.addDays(deltaday);
                }
                
                 if(deltaday != 0 && newTask.Due_Date__c != null){
                    newTask.Due_Date__c = newTask.Due_Date__c.addDays(deltaday);
                }
                newTask.Complete__c = false;
                newTask.Description__c = null;
                // N0001
                newTask.Project_Milestone__c = allNewMilestoneMap.get(newTask.Project_Milestone__c).Id;
            }
            insert newTasks;

            //N0001
            newProj.BI_O4_Bypass__c = 2;
            //newProj.BI_O4_Cloning_Bypass__c = false;
            update newProj;
            //N0001
            
            return new ApexPages.StandardController(newProj).view();
         
        }catch(Exception e){
            ApexPages.addMessages(e); //show save error(s) on the visualforce page
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Line number: ' + e.getLineNumber()));
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Stack Trace: ' + e.getStackTraceString()));
            Database.rollback(preSave); //rollback any completed saves if the transaction has errors
        }
        return pageReference;
    }

     public PageReference createCloneHandover(){      
        Savepoint preSave = Database.setSavepoint(); //set savepoint so we can rollback the whole save if there are errors
        PageReference pageReference = null;
        try{
            
          if(isProjOwnerActive == false){
                if(dummyProj.OwnerId == null){
                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Milestone1_InactiveUsersError);
                   ApexPages.addMessage(myMsg);
                   isInactiveUserToBeReplaced = true;
                   return pageReference;
                }if(dummyProj.OwnerId != null){
                    //N0001 rec.ownerId = dummyProj.OwnerId; 
                   //N0001
                   rec.ownerId = userInfo.getUserId(); 
                   //N0001
                }
            }             
            
            //create new project record
            rec.Name = dummyProj.Name;
            //N0001
            rec.BI_O4_Bypass__c = 1;
            
            integer deltaday = 0;
            if(rec.Kickoff__c != null && rec.BI_O4_Project_Customer__c != 'passclasstest'){
                deltaday = rec.Kickoff__c.daysbetween(system.today());
            }
            
            if (deltaday != 0 && rec.Kickoff__c != null){
                rec.Kickoff__c = rec.Kickoff__c.addDays(deltaday);
            }
                          
            if (deltaday != 0 && rec.Deadline__c != null){
              rec.Deadline__c = rec.Deadline__c.addDays(deltaday);
            }
            
            rec.OwnerId =userInfo.getUserId();
            //N0001
            
            Milestone1_Project__c newProj = Milestone1_Clone_Utility.cloneProject(rec);
            //newProj.BI_O4_Cloning_Bypass__c = true;
            insert newProj;
            //separate milestone records into parents and subs
            List<Milestone1_Milestone__c> topMilestones = new List<Milestone1_Milestone__c>();
            List<Milestone1_Milestone__c> bottomMilestones = new List<Milestone1_Milestone__c>();
            for(Milestone1_Milestone__c oldMS : milestones){
                // N0001
                 if(deltaday != 0 && oldMS.Kickoff__c != null){
                    oldMS.Kickoff__c = oldMS.Kickoff__c.addDays(deltaday);
                 }
                 if(deltaday != 0 && oldMs.Deadline__c != null){
                   oldMs.Deadline__c = oldMs.Deadline__c.addDays(deltaday);
                 }
              // N0001
                if(oldMS.Parent_Milestone__c == null){
                    oldMS.OwnerId =userInfo.getUserId(); /*Añadido para Demanda 357*/
                    topMilestones.add(oldMS);
                } else {
                    oldMS.OwnerId =userInfo.getUserId(); /*Añadido para Demanda 357*/
                    bottomMilestones.add(oldMS);
                }
            }            
            //clone and insert top milestone records
            Map<String, Milestone1_Milestone__c> newTopMilestoneMap = Milestone1_Clone_Utility.cloneMilestonesIntoMap(topMilestones);
            for(Milestone1_Milestone__c newMS : newTopMilestoneMap.values()){
                newMS.Project__c = newProj.Id;
                newMS.Parent_Milestone__c = null;
                newMS.Complete__c = false;
                newMS.Description__c = null;
            }
            insert newTopMilestoneMap.values();
            
            //clone and insert sub milestone records
            Map<String, Milestone1_Milestone__c> newBottomMilestoneMap = Milestone1_Clone_Utility.cloneMilestonesIntoMap(bottomMilestones);
            for(Milestone1_Milestone__c newMS : newBottomMilestoneMap.values()){
                newMS.Project__c = newProj.Id;
                newMS.Parent_Milestone__c = newTopMilestoneMap.get(newMS.Parent_Milestone__c).Id;
                newMS.Complete__c = false;
                newMS.Description__c = null;
            }
            insert newBottomMilestoneMap.values();
            
            //collect all milestones into one map
            Map<String, Milestone1_Milestone__c> allNewMilestoneMap = new Map<String, Milestone1_Milestone__c>();
            allNewMilestoneMap.putAll(newTopMilestoneMap);
            allNewMilestoneMap.putAll(newBottomMilestoneMap);
            
            //clone and insert task records
            List<Milestone1_Task__c> newTasks = Milestone1_Clone_Utility.cloneTasksIntoList(tasks);
            for(Milestone1_Task__c newTask : newTasks){
        // N0001
                if(deltaday != 0 && newTask.Start_Date__c != null){
                    newTask.Start_Date__c = newTask.Start_Date__c.addDays(deltaday);
                }
                
                 if(deltaday != 0 && newTask.Due_Date__c != null){
                    newTask.Due_Date__c = newTask.Due_Date__c.addDays(deltaday);
                }
                newTask.Complete__c = false;
                newTask.Description__c = null;
                // N0001
                newTask.Project_Milestone__c = allNewMilestoneMap.get(newTask.Project_Milestone__c).Id;
            }
            insert newTasks;

            //N0001
            newProj.BI_O4_Bypass__c = 2;
            //newProj.BI_O4_Cloning_Bypass__c = false;
            update newProj;
            //N0001
            
            return new ApexPages.StandardController(newProj).view();
         
        }catch(Exception e){
            ApexPages.addMessages(e); //show save error(s) on the visualforce page
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Line number: ' + e.getLineNumber()));
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Stack Trace: ' + e.getStackTraceString()));
            Database.rollback(preSave); //rollback any completed saves if the transaction has errors
        }
        return pageReference;
    }

}