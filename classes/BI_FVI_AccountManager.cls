/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alvaro Sevilla
Company:       New Energy Group
Description:   Methods called as web service methods by external App (UpToCloud) in order
               to retrieve an Account by RUC and to create a Vinculo de Nodo between
               an Account and a node NAV
Test Class:    BI_FVI_AccountManager_TEST

History:

<Date>                  <Author>                <Change Description>
30/09/2016              Alvaro Sevilla          Initial version
05/10/2016              Alvaro Sevilla          Cambio en salida del WS
--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
@RestResource(urlMapping='/AccountManager/*')
global with sharing class BI_FVI_AccountManager {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro Sevilla
    Company:       New Energy Group
    Description:   Method that retrieves an Account by RUC and verifies if it's
                   already assigned to a node
    
    IN:            RUC (String)
    OUT:           strMsg (String)
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    30/09/2016              Alvaro Sevilla          Initial Version
    08/03/2017              Humberto Nunes          Se Agrego Pais en el Query Usuario, Variable RUC cambio a NFISCAL, 
                                                    Se uso el campo Pais del Usuario como parte de la consulta del cliente. 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    @HttpGet
    global static WrpRespuesta retrieveAccountNode() {
        
        RestRequest req = RestContext.request;       
        String NFISCAL = req.params.get('ruc');
        String IDUSER = req.params.get('uid');
        String Respuesta;
        String CommercialId = '';
        System.debug('-----NFISCAL ->'+NFISCAL+'--->'+IDUSER);
        WrpRespuesta wrpR = new WrpRespuesta();

        if(NFISCAL != null && IDUSER != null){    

        if(IDUSER.length() == 18 && Pattern.matches('^[a-zA-Z0-9]*$', IDUSER)) {

            list<User> lstusuarioApp = [SELECT Id, Pais__c FROM User WHERE Id =: IDUSER];

            if(lstusuarioApp != null && lstusuarioApp.size() > 0){
        
                //Search for NFISCAL
                Account[] Acnt = [SELECT Id, BI_FVI_Tipo_Planta__c,OwnerId,BI_Subsegment_Regional__c,BI_FVI_Nodo__c,BI_FVI_Nodo__r.OwnerId,BI_FVI_Nodo__r.RecordType.DeveloperName,Owner.Contact.Account.RecordType.DeveloperName
                                  FROM Account
                                  WHERE BI_No_Identificador_fiscal__c = :NFISCAL
                                  AND BI_Country__c =: lstusuarioApp[0].Pais__c];        
                System.debug('-----Acnt ->'+Acnt);

                if (Acnt.size() > 0) {

                    if(Acnt[0].BI_Subsegment_Regional__c != null){

                    if(Acnt[0].BI_Subsegment_Regional__c.contains('Premium')){

                        if(Acnt[0].BI_FVI_Nodo__c != null && (Acnt[0].BI_FVI_Nodo__r.RecordType.DeveloperName == 'BI_FVI_NCP' || Acnt[0].BI_FVI_Nodo__r.RecordType.DeveloperName == 'BI_FVI_NAV' || Acnt[0].BI_FVI_Nodo__r.RecordType.DeveloperName == 'BI_FVI_NAT')){

                            if(Acnt[0].BI_FVI_Tipo_Planta__c != null){

                                if(Acnt[0].BI_FVI_Tipo_Planta__c == 'No Cliente'){

                                    if(Acnt[0].BI_FVI_Nodo__r.OwnerId == IDUSER){

                                        wrpR.mensaje = 'Este No-Cliente ya se encuentra asignado a su nodo';
                                        wrpR.codigo = '0';

                                    }else{

                                        wrpR.mensaje = 'No-Cliente de Segmento PREMIUM ya asignado. Solicitar mediante caso su Asignación';
                                        wrpR.codigo = '1';

                                    }

                                }else{

                                    if(Acnt[0].BI_FVI_Nodo__r.OwnerId == IDUSER){

                                        wrpR.mensaje = 'Este Cliente ya se encuentra asignado a su nodo';
                                        wrpR.codigo = '0';

                                    }else{

                                        wrpR.mensaje = 'Cliente de Segmento PREMIUM ya asignado. Solicitar mediante caso su Asignación';
                                        wrpR.codigo = '1';
                                    }

                                }
                            }else{

                                wrpR.mensaje = 'Sin clasificar, abra un caso';
                                wrpR.codigo = '2';

                            }

                        }else if(Acnt[0].BI_FVI_Tipo_Planta__c != null){

                            if(Acnt[0].BI_FVI_Tipo_Planta__c == 'No Cliente') {

                                    wrpR.mensaje = 'No Cliente de Segmento PREMIUM libre. Solicitar mediante caso su Asignación';
                                    wrpR.codigo = '2';
                            }else{

                                    wrpR.mensaje = 'Cliente de Segmento PREMIUM libre. Solicitar mediante caso su Asignación';
                                    wrpR.codigo = '2';
                            }
                            
                        }else{

                                wrpR.mensaje = 'Sin clasificar, abra un caso';
                                wrpR.codigo = '2';

                            }

                    }else{

                        wrpR.mensaje = 'Cliente fuera de segmento PREMIUM. No se puede asignar';
                        wrpR.codigo = '3';
                    }


                    }else{

                        wrpR.mensaje = 'Sin segmento definido, abra  un caso';
                        wrpR.codigo = '2';
                    }


                }else { //Account no exists
                    wrpR.mensaje =  'No existe Cliente. Solicitar mediante caso su Creación';
                    wrpR.codigo = '2';
                }
                return wrpR;
            }else{

                wrpR.mensaje =  'El Id de usuario suministrador No existe en este entorno';
                wrpR.codigo = '3';
                return wrpR;
            }


        }else{

            wrpR.mensaje =  'El Id de usuario suministrador no es correcto';
            wrpR.codigo = '3';
            return wrpR;
        }


        }else{

            wrpR.mensaje = 'Parametros no validos, alguno o ambos nulos, verifique la URL';
            wrpR.codigo = '3';
            return wrpR;
        }
    }

    global class WrpRespuesta{

        public string mensaje;
        public string codigo;

    }


    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro Sevilla
    Company:       New Energy Group
    Description:   Method that creates a Vinculo de Nodo between an Account and a node NAV
    
    IN:            RUC (String), UserId (String)
    OUT:           strMsg (String)
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    30/09/2016              Alvaro Sevilla          Initial Version  
    09/03/2017              Humberto Nunes          Se cambio SubSegmento Local por Regional y se comparo con "%Premium" 
                                                    El país se cogio del usuario para hacerlo generico para todos los paises.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpPost
    global static String createAccountNode() {
        
        RestRequest req = RestContext.request;
        Blob bBody = req.requestBody;
        String sserialize =  bBody.toString();
        system.debug('##### deserialize: ' + sserialize);
        system.debug(LoggingLevel.FINE,'## deserialize: ' + sserialize);
        WrpAsingAccount AssigClient = (WrpAsingAccount)Json.deserialize(sserialize, WrpAsingAccount.Class);
        System.debug('-----AssigClient.RUC ->'+AssigClient.RUC);
        String strMsg = '';
        //Search for RUC

        list<User> lstusuarioApp = [SELECT Id, Pais__c FROM User WHERE Id =: AssigClient.UserId];

        Account[] Acnt = [SELECT Id,BI_FVI_Tipo_Planta__c 
                         FROM Account
                         WHERE BI_No_Identificador_fiscal__c = :AssigClient.RUC
                         AND BI_Subsegment_Regional__c lIKE '%Premium%'
                         AND BI_Country__c =: (lstusuarioApp.size()>0)?lstusuarioApp[0].Pais__c:null];    
        
        //Account exists
        if (Acnt.size() > 0) {

            if(Acnt[0].BI_FVI_Tipo_Planta__c == 'No Cliente'){
            
            //Search for User's Node NAV
            BI_FVI_Nodos__c[] UserNode = [SELECT Id, Name
                                            FROM BI_FVI_Nodos__c
                                            WHERE OwnerId = :AssigClient.UserId
                                            AND RecordTypeId in(SELECT Id FROM RecordType WHERE Name='Nodo de Adquisición Volatil')];
            
            //If User's node NAV exists, old Vinculo de Nodo is deleted and a new Vinculo de Nodo is created
            if (UserNode.size() > 0) {
                
                try {

                    list<BI_FVI_Nodo_Cliente__c> lstNodClient = [SELECT Id, BI_FVI_IdCliente__c,BI_FVI_IdNodo__c FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdCliente__c = :Acnt[0].Id];
                    System.debug('---lstNodClient->'+lstNodClient);
                    if(lstNodClient != null && lstNodClient.size() > 0){

                        lstNodClient[0].BI_FVI_IdCliente__c = Acnt[0].Id;
                        lstNodClient[0].BI_FVI_IdNodo__c = UserNode[0].Id;
                        update lstNodClient;

                    }else{

                        BI_FVI_Nodo_Cliente__c new_nc = new BI_FVI_Nodo_Cliente__c(
                        BI_FVI_IdCliente__c = Acnt[0].Id,
                        BI_FVI_IdNodo__c = UserNode[0].Id);
                        insert new_nc;
                    }
                                                       
                }catch (Exception e) {
                    //strCode = 5;
                    strMsg = 'Ha ocurrido una excepción: '+e.getMessage();
                    return(strMsg);
                }

                strMsg = 'Se ha asignado correctamente la cuenta al nodo: ' + UserNode[0].Name;
               // strCode = 6;

            }else {
                strMsg = 'No existe un nodo NAV para usuario';
               // strCode = 7;
            }

        }else{

            strMsg = 'Solo pueden incluirse Clientes con el campo "Clasificación por Tipo de Planta" IGUAL A "No Cliente" a los Nodos NAG, NAT, NAV y NAB';
        }

        }else { //Account no exists

            strMsg = 'No se ha encontrado un cliente para el RUC: ' + AssigClient.RUC;
            //strCode = 8;
        }
        
        return strMsg;
    }
    //clase wrapper para manejo de parametros
    global class WrpAsingAccount{

        public string RUC;
        public string UserId;

    }
}