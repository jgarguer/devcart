global class BI_COL_BatchDeleteDuplicateDirection_bch implements Database.Batchable<SObject>,Database.Stateful,Database.AllowsCallouts{
	
	global String consulta;  
	global List<sObject> lstIdDireccion=new List<sObject>();
	global String termino;
	global String termino2;   
     
    global BI_COL_BatchDeleteDuplicateDirection_bch(){ 
    	termino = label.BI_COL_LbValor4EstadoCallejero;
    	termino2 = label.BI_COL_LbValor1SucursalEnUso;
    	
    	consulta='select Id from BI_Sede__c where BI_COL_Estado_callejero__c='+'\''+termino+'\''+' and BI_COL_Sucursal_en_uso__c != '+'\''+termino2+'\''; 
    }
    
    /******este método se ejecuta al inicio y sirve para obtener los registros que se le pasan al metodo execute********/
    global Database.QueryLocator start(Database.BatchableContext DB) 
    {        
    	System.debug('Ejecutando sheduler (start)\n\n'+consulta+'\n\n');
        return Database.getQueryLocator(consulta);
    } 
    /*******************************************************************************************************************/
    
    global void execute(Database.BatchableContext DB, List<sObject> scope)
    {
    	System.debug('Ejecutando sheduler (Excecute)---------->'+scope);
    		lstIdDireccion = scope;
    		    		
    		Database.DeleteResult[] ResultDupl = database.delete(scope,false); //se pone el false para indicar que se debe hacer la inserción de los registros obviando los que tienen errores para permitir la inserción de los demás
			
			for(Database.DeleteResult sr : ResultDupl){
				
				if(!sr.isSuccess()){Database.Error err = sr.getErrors()[0];string errorDetalles='';
					for(Database.Error errt : sr.getErrors()){errorDetalles+=errt.getMessage();}
					System.debug('Error: '+errorDetalles);
				}
			}
    	
    }
    /*****este método sirve para mandar a ejecutar otro batch****************/
    global void finish(Database.BatchableContext DB){
    	
    	/* activar cuando se cree otro batch y necesite ejecutar desde el maestro
    	BI_COL_MasterScheduler_sch gam = new BI_COL_MasterScheduler_sch(1);
        DateTime fechaActual= System.now().addMinutes(1);
        Integer minutos=fechaActual.minute();
        Integer hora=fechaActual.hour();
        Integer dia=fechaActual.day();
        integer mes=fechaActual.month();
        Integer anio=fechaActual.year();
        String sch = '0 '+minutos+' '+hora+' '+dia+' '+mes+' ? '+anio;
        System.schedule('BI_COL_BatchDeleteDuplicateDirection_bch'+System.now(),sch,gam);	
    	*/
    	System.debug('Proceso de eliminación terminado; se han eliminado: '+lstIdDireccion.size()+' registros');
    }

}