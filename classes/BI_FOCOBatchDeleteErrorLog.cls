/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Batchable class that mass-deletes records from the Registro Datos FOCO Error Log object
	History
	<Date>          <Author>           <Change Description>
	20-Feb-2015     Virgilio Utrera    Initial version
-------------------------------------------------------------------*/

global class BI_FOCOBatchDeleteErrorLog implements Database.Batchable<sObject>, Database.Stateful {

	global Integer failed;
	global Integer succeeded;
	global String errorLogId;
	global String query;

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Class constructor
		IN:				String: FOCO data load Id to be deleted
		OUT:
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	global BI_FOCOBatchDeleteErrorLog(String focoErrorLogId) {
		System.debug('=====> Executing BI_FOCOBatchDeleteErrorLog constructor for FOCO Data Load ' + focoErrorLogId);
		errorLogId = focoErrorLogId;
		succeeded = 0;
		failed = 0;
		query = 'SELECT Id FROM BI_Registro_Datos_FOCO_Error_Log__c WHERE BI_Identificador_Log__c = :errorLogId';
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Returns the record set as a QueryLocator object that will be batched for execution
		IN:				Database.BatchableContext: tracks the progress of the batch job
		OUT:            Database.QueryLocator: object or Iterable that contains the records or objects being passed into the job
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('=====> BI_FOCOBatchDeleteErrorLog.start()');
		return Database.getQueryLocator(query);
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Contains or calls the main execution logic for the batch job
		IN:				Database.BatchableContext: tracks the progress of the batch job
						List<sObject>: objects to be processed
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Database.DeleteResult> drList = new List<Database.DeleteResult>();
		List<BI_Registro_Datos_FOCO_Error_Log__c> errorsList = new List<BI_Registro_Datos_FOCO_Error_Log__c>();

		System.debug('=====> BI_FOCOBatchDeleteErrorLog.execute(): start batch');

		// Get records for current batch
		errorsList = (List<BI_Registro_Datos_FOCO_Error_Log__c>)scope;

		// Delete records
		try {
			drList = Database.Delete(errorsList, false);
		}
		catch(Exception e) {
			throw new BI_FOCO_Exception('Error while batch deleting FOCO Error Log records', e);
		}

		for(Database.DeleteResult dr : drList) {
			if(!dr.isSuccess())
				failed++;
			else
				succeeded++;
		}

		// Empty recycle bin
		Database.emptyRecycleBin(scope);

		System.debug('=====> BI_FOCOBatchDeleteErrorLog.execute(): end batch');
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Executes post-processing operations
		IN:				Database.BatchableContext: tracks the progress of the batch job
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	global void finish(Database.BatchableContext BC) {
		try {
			// Remove error log Id from pending for deletion list
			BI_FOCOUtil.removeLogIdFromPendingForDeletionList(errorLogId);

			// Remove error log Id from list of available error log Ids, if no records failed
			if(failed == 0) {
				BI_FOCOUtil.removeLogIdFromAvailableLogsList(errorLogId);
			}

			// Send confirmation email to running user
			sendConfirmationEmail();
		}
		catch(Exception e) {
			throw new BI_FOCO_Exception('Error while finishing the error log deletion process for FOCO Error Log "' + errorLogId + '"', e);
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    
		IN:				
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		23-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	private void sendConfirmationEmail() {
		Messaging.SingleEmailMessage mail;
		String body;
		String subject;
		String userEmailAddress;
		String userId;
		String userName;
		User activeUser;

		userId = UserInfo.getUserId();
		activeUser = [ SELECT Name, Email FROM User WHERE Id = :userId LIMIT 1 ];

		// Send confirmation email to running user
    	subject = String.format('Telefónica BI_EN: Resultados Eliminación Registros Datos FOCO Error Log: {0}', new List<String>{ errorLogId });
		body = String.format('Estimado {0}: el proceso de eliminación de Registros Datos FOCO Error Log ha finalizado:\n\nLog de Errores eliminado: {1}\nNúmero de registros eliminados: {2}\nNúmero de registros no eliminados: {3}', new List<String>{ activeUser.Name, errorLogId, String.valueOf(succeeded), String.valueOf(failed) });
    	mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[]{ activeUser.Email });
		mail.setSubject(subject);
		mail.setUseSignature(false);
		mail.setPlainTextBody(body);

		try {
   			//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); //Check límits and target to userId 
			BI_SendMails_Helper.sendMails(new Messaging.SingleEmailMessage[] { mail }, false, 'FOCO');

   		}
   		catch(Exception e) {
   			throw new BI_FOCO_Exception('Error while sending confirmation email after deleting FOCO Error Log "' + errorLogId + '"', e);
   		}
	}
}