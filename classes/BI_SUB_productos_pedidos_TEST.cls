/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Alvaro Sevilla
 Company:       Accenture LTDA
 Project:       Subsidios ARG
 Description:

 History:

 <Date>               <Author>             <Change Description>
 07/03/2018         Alvaro Sevilla          Initial Version
 ------------------------------------------------------------------------------------------------*/
@isTest
private class BI_SUB_productos_pedidos_TEST {

    @isTest static void test_method_one() {

          Account acc = new Account(
          Name = 'Test1ARG',
          Industry = 'Comercio',
          CurrencyIsoCode = 'ARS',
          BI_Activo__c = 'Sí',
          BI_Cabecera__c = 'Sí',
          BI_Estado__c = true,
          BI_Country__c = Label.BI_Argentina,
          BI_Segment__c = 'Empresas',
          TGS_Es_MNC__c = false,
          BI_No_Identificador_fiscal__c = '30111111118'
        );
        insert acc;

        BI_MigrationHelper.cleanSkippedTriggers();
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            BI_SUB_Subsidio__c = 'Alta'
        );
        insert opp;
        BI_MigrationHelper.skipAllTriggers();

        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;

        List<NE__Product__c> lst_prod = new List<NE__Product__c>();

        NE__Product__c pord = new NE__Product__c();
         pord.Name = 'Plan vuela 1';
         pord.Payback_Family__c = 'Planes';
         lst_prod.add(pord);

         NE__Product__c pord2 = new NE__Product__c();
         pord2.Name = 'Sansumg Galaxy S8';
         pord2.Payback_Family__c = 'Terminales';
         lst_prod.add(pord2);

        insert lst_prod;

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;

        NE__Item_Header__c cabecera = new NE__Item_Header__c(
            Name = 'Planes y Servicios',
            NE__Catalog__c = cat.Id
            );
        insert cabecera;
        List<NE__Catalog_Item__c> lstLineasCat = new List<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__ProductId__c = lst_prod[0].Id,
            NE__Currency__c = 'ARS',
            NE__Item_Header__c = cabecera.Id
            );
        lstLineasCat.add(catIt);

        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__ProductId__c = lst_prod[1].Id,
            NE__Currency__c = 'ARG'
            );
        lstLineasCat.add(catIt2);

        insert lstLineasCat;
        System.debug('agregaTasa mapaCatalogItems catIt' + catIt);

        BI_MigrationHelper.cleanSkippedTriggers();

        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();

        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = lst_prod[0].Id,
            NE__CatalogItem__c = lstLineasCat[0].Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            NE__One_Time_Cost__c = 3000,
            NE__RecurringChargeOv__c = 3000,
            BI_SUB_Descuento__c = 5,
            BI_SUB_Grupo__c = '1'
        );
        lst_itemToInsert.add(oi);

        NE__OrderItem__c oi2 = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = lst_prod[1].Id,
            NE__CatalogItem__c = lstLineasCat[1].Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 2000,
            NE__One_Time_Cost__c = 3000,
            NE__RecurringChargeOv__c = 6000,
            BI_O4_TdCP__c = 11.0,
            BI_SUB_Descuento__c = 5,
            BI_SUB_Grupo__c = '1'
        );
        lst_itemToInsert.add(oi2);

        insert lst_itemToInsert;

         BI_SUB_Pedido_Subsidio__c pedido = new BI_SUB_Pedido_Subsidio__c(
              BI_SUB_Estado__c = 'Abierto',
              BI_SUB_Oportunidad__c = opp.Id,
              BI_SUB_Cliente__c = acc.Id,
              BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio'
            );
        insert pedido;
        Test.startTest();
        apexpages.currentpage().getparameters().put('Id' , pedido.Id);
        ApexPages.StandardController ssc = new ApexPages.StandardController(pedido);

        BI_SUB_productos_pedidos_ctr param = new BI_SUB_productos_pedidos_ctr(ssc);
        BI_SUB_productos_pedidos_ctr.WrpAtributos atr = new BI_SUB_productos_pedidos_ctr.WrpAtributos();
        for(BI_SUB_productos_pedidos_ctr.WrpAtributos elem : param.lstAtributos){
            elem.AEnviar = '1';
        }
        param.Enviar();
        param.Cancelar();
        Test.stopTest();
    }

    @isTest static void test_method_two() {

          Account acc = new Account(
          Name = 'Test1ARG',
          Industry = 'Comercio',
          CurrencyIsoCode = 'ARS',
          BI_Activo__c = 'Sí',
          BI_Cabecera__c = 'Sí',
          BI_Estado__c = true,
          BI_Country__c = Label.BI_Argentina,
          BI_Segment__c = 'Empresas',
          TGS_Es_MNC__c = false,
          BI_No_Identificador_fiscal__c = '30111111118'
        );
        insert acc;

        BI_MigrationHelper.cleanSkippedTriggers();
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            BI_SUB_Subsidio__c = 'Alta'
        );
        insert opp;
        BI_MigrationHelper.skipAllTriggers();

        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;

        List<NE__Product__c> lst_prod = new List<NE__Product__c>();

        NE__Product__c pord = new NE__Product__c();
         pord.Name = 'Plan vuela 1';
         pord.Payback_Family__c = 'Planes';
         lst_prod.add(pord);

         NE__Product__c pord2 = new NE__Product__c();
         pord2.Name = 'Sansumg Galaxy S8';
         pord2.Payback_Family__c = 'Terminales';
         lst_prod.add(pord2);

        insert lst_prod;

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;

        NE__Item_Header__c cabecera = new NE__Item_Header__c(
            Name = 'Planes y Servicios',
            NE__Catalog__c = cat.Id
            );
        insert cabecera;
        List<NE__Catalog_Item__c> lstLineasCat = new List<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__ProductId__c = lst_prod[0].Id,
            NE__Currency__c = 'ARS',
            NE__Item_Header__c = cabecera.Id
            );
        lstLineasCat.add(catIt);

        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__ProductId__c = lst_prod[1].Id,
            NE__Currency__c = 'ARG'
            );
        lstLineasCat.add(catIt2);

        insert lstLineasCat;
        System.debug('agregaTasa mapaCatalogItems catIt' + catIt);

        BI_MigrationHelper.cleanSkippedTriggers();

        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();

        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = lst_prod[0].Id,
            NE__CatalogItem__c = lstLineasCat[0].Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            NE__One_Time_Cost__c = 3000,
            NE__RecurringChargeOv__c = 3000,
            BI_SUB_Descuento__c = 5,
            BI_SUB_Grupo__c = '1'
        );
        lst_itemToInsert.add(oi);

        NE__OrderItem__c oi2 = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = lst_prod[1].Id,
            NE__CatalogItem__c = lstLineasCat[1].Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 2000,
            NE__One_Time_Cost__c = 3000,
            NE__RecurringChargeOv__c = 6000,
            BI_O4_TdCP__c = 11.0,
            BI_SUB_Descuento__c = 5,
            BI_SUB_Grupo__c = '1'
        );
        lst_itemToInsert.add(oi2);

        insert lst_itemToInsert;

         BI_SUB_Pedido_Subsidio__c pedido = new BI_SUB_Pedido_Subsidio__c(
              BI_SUB_Estado__c = 'Abierto',
              BI_SUB_Oportunidad__c = opp.Id,
              BI_SUB_Cliente__c = acc.Id,
              BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio'
            );
        insert pedido;
        Test.startTest();
        apexpages.currentpage().getparameters().put('Id' , pedido.Id);
        ApexPages.StandardController ssc = new ApexPages.StandardController(pedido);

        BI_SUB_productos_pedidos_ctr param = new BI_SUB_productos_pedidos_ctr(ssc);
        BI_SUB_productos_pedidos_ctr.WrpAtributos atr = new BI_SUB_productos_pedidos_ctr.WrpAtributos();
        for(BI_SUB_productos_pedidos_ctr.WrpAtributos elem : param.lstAtributos){
            elem.AEnviar = 'A';
        }
        param.Enviar();
        param.Cancelar();
        Test.stopTest();
    }

}