/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Address_Standard_Redirection

History:
<Date>							<Author>						<Change Description>
12/06/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_CHI_Address_Standard_Redirection_Test {
    
    @testSetup 
    private static void init() {
        /* Sede */
        BI_Sede__c sede = new BI_Sede__c(Estado__c = 'Validado', Name = 'Test-Address', BI_Direccion__c ='Manoteras',
                                         BI_Numero__c ='13', BI_Provincia__c ='Santiago', BI_Country__c ='Chile',
                                         BI_Codigo_postal__c = '28050', BI_Distrito__c = 'Chile',
                                         BI_Localidad__c = 'Santiago', BI_Piso__c = '2', BI_Apartamento__c ='A',
                                         FS_CHI_Bloque__c = '10', FS_CHI_Tipo_Complejo_Habitacional__c = 'A', FS_CHI_Nombre_Complejo_Habitacional__c = 'A',
                                         FS_CHI_Numero_de_Casilla__c = '10');
        insert sede;
    }
    
    @isTest private static void controller() {
        /* Test */
        System.Test.startTest();
        
        try{
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller([SELECT Id FROM BI_Sede__c LIMIT 1][0]);
            FS_CHI_Address_Standard_Redirection_ctr page = new FS_CHI_Address_Standard_Redirection_ctr(controller);
            
            page.redirect();
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
}