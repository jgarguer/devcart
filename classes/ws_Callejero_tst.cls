/**
* Avanxo Colombia
* @author           Oscar Alejandro Jimenez Forero
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-26      Oscar Alejandro Jimenez Forero (OAJF)   Class Created
*************************************************************************************************************/
@isTest
global class ws_Callejero_tst implements WebServiceMock{
	
	global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		ws_Callejero_dir.Interfaz1Response_element objResp = new ws_Callejero_dir.Interfaz1Response_element();
    	objResp.Interfaz1Result = 'resp';

		response.put( 'response_x', objResp);
	}

	@isTest
	static void test_method_1() {
		
		ws_Callejero_dir.WebServiceSoap webser = new ws_Callejero_dir.WebServiceSoap();
		ws_Callejero_dir.ArrayOfParametrosGeo misParametrosGeo = new ws_Callejero_dir.ArrayOfParametrosGeo();		
		ws_Callejero_dir.ParametrosGeo par = new ws_Callejero_dir.ParametrosGeo();
		Test.startTest();		
		Test.setMock( WebServiceMock.class, new ws_Callejero_tst() );
		webser.Interfaz1(misParametrosGeo);
		Test.stopTest();		
	}

}