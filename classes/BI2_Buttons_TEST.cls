@isTest
private class BI2_Buttons_TEST {
	
	@isTest static void test_getBit2PubDoc4Case() {
		BI_TestUtils.throw_exception = false;

		System.assertEquals(null, BI2_Buttons.getBit2PubDoc4Case(null));

		BI2_Asignacion_Plantilla__c plant = new BI2_Asignacion_Plantilla__c(BI2_Pais__c ='Peru', BI2_Tipologia__c='Tipol1', BI2_Motivo_del_Contacto__c='Solicitud Prueba', BI2_Nombre_de_Plantilla__c='Plantilla1');
		insert plant;

		Case cas2 = new Case(BI2_per_tipologia__c=plant.BI2_Tipologia__c, BI_Country__c=plant.BI2_Pais__c, Reason=plant.BI2_Motivo_del_Contacto__c);
		Case cas1 = new Case(BI2_per_tipologia__c='Tipol2', BI_Country__c=plant.BI2_Pais__c, Reason='Inconformidad de prueba');
		insert new List<Case>{cas1, cas2};

		Test.startTest();
		System.assertEquals(null, BI2_Buttons.getBit2PubDoc4Case(null));
		System.assertEquals('', BI2_Buttons.getBit2PubDoc4Case(cas1.Id));

		String res = BI2_Buttons.getBit2PubDoc4Case(cas2.Id);
		System.assertNotEquals('', res);
		System.assertNotEquals(null, res);

		Test.stopTest();
	}
	
	@isTest static void test_exceptions() {
		BI_TestUtils.throw_exception = true;

		BI2_Buttons.getBit2PubDoc4Case(null);
	}
	
}