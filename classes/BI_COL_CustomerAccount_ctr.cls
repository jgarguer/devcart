/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-04-14      Manuel Esthiben Mendez Devia (MEMD)     Cloned Class
*************************************************************************************************************/
public with sharing class BI_COL_CustomerAccount_ctr 
{
	
	public String NombreCliente { get; set; }
	public String idMS{get;set;}
	public String cuentas{get;set;}
	public String respuestaxml{get;set;}
	public String separador='';
	
	public List<SelectOption> options {get;set;}
	public List<String> lDireccion=new List<String>();
	public String CuentasDavox='';
	public String CuentaDx = null;
	public BI_COL_Modificacion_de_Servicio__c dsto;
	public Map<String,String> mapCuenta=new Map<String,String>();
	public string cuentaNueva='';
	public Map<String,String> mapCuentaOption=new Map<String,String>();
	public List<String> lstCuentas{ get; set; }
	public List<String> lstLabelCuenta=new List<String>();
	
	public string tipo=Label.BI_COL_LbConsultaCuenta;
	public string packcode=Label.BI_COL_LbPackcode;
	
	/** Paramétros del paginador */
	private Integer pageNumber;
	public Integer pageSize;
	public Integer totalPageNumber;
	
	
	public void cuentaCesionContr()
	{
		pageNumber=0;
		pageSize=50;
		totalPageNumber=0;
		string id=ApexPages.currentPage().getParameters().get('id');
		Account acc =new Account();
		acc=[select id,BI_No_Identificador_fiscal__c from Account where id=:id];
		
		cuentas=counsultarCuentasWS(acc.BI_No_Identificador_fiscal__c);
		
		XmlStreamReader xml=new XmlStreamReader(cuentas);
			try
			{
				Dom.Document doc = new Dom.Document();
				doc.load(cuentas);
				Dom.XMLNode WS = doc.getRootElement();
				List<dom.XMLNode> lsxmlValues=WS.getChildElements(); 
				for(dom.XMLNode dxm:lsxmlValues)
				{
					if(dxm.getName()==Label.BI_COL_LbSeparator)
					{
						separador=dxm.getText();
						System.debug('\nSEPARATOR'+separador);
					}
					if(dxm.getName()==Label.BI_COL_LbValues)
					{
						List<dom.XMLNode> values=dxm.getChildElements();
						for(dom.XMLNode dxmv: values)
						{
							respuestaxml=dxmv.getText();
							System.debug('\nrespuestaxml'+respuestaxml);
							
						}
					}
				}
				armarOpciones();
			} 
			catch(Exception ex)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Error al tratar de conectarse con DAVOX, por favor comuníquese con el administrador: '+cuentas+ex.getMessage()));
			}
	
	}

	public void cargarDatos()
	{
		pageNumber=0;
		pageSize=50;
		totalPageNumber=0;
		idMS=ApexPages.currentPage().getParameters().get('idMS');
		dsto=new BI_COL_Modificacion_de_Servicio__c(id=idMS);
		
		//dsto.Id=idDS;
		List<BI_COL_Modificacion_de_Servicio__c> dst=[Select BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c, BI_COL_Oportunidad__r.Account.Name 
										from BI_COL_Modificacion_de_Servicio__c d where d.id=:dsto.Id limit 1];
		if(dst.size()>0)
		{
			NombreCliente=dst[0].BI_COL_Oportunidad__r.Account.Name;
			cuentas=dst[0].BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c;
			if(cuentas!=null)
			{	
				cuentas=counsultarCuentasWS(cuentas);
				
				XmlStreamReader xml=new XmlStreamReader(cuentas);
				try
				{
					Dom.Document doc = new Dom.Document();
					doc.load(cuentas);
					Dom.XMLNode WS = doc.getRootElement();
					List<dom.XMLNode> lsxmlValues=WS.getChildElements(); 
					for(dom.XMLNode dxm:lsxmlValues)
					{
						if(dxm.getName()==Label.BI_COL_LbSeparator)
						{
							separador=dxm.getText();
						}
						if(dxm.getName()==Label.BI_COL_LbValues)
						{
							List<dom.XMLNode> values=dxm.getChildElements();
							for(dom.XMLNode dxmv: values)
							{
								respuestaxml=dxmv.getText();
							}
						}
					}
					armarOpciones();
				}
				catch(Exception ex)
				{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Error al tratar de conectarse con DAVOX, por favor comuníquese con el administrador: '+ex.getMessage()+ ex.getStackTraceString() +' Linea'+ex.getLineNumber()));
				} 
				//System.debug('\n\n lstCuentas.size()= '+lstCuentas.size()+'\n\n');
			}
			else
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'La modificacion de servicio no tiene Oportunidad Asociada'));
			}
		}
		else
		{
			NombreCliente='Cliente no existe';
		}
	}
	
	public String counsultarCuentasWS(String nroIdentificacion)
	{
		String cuentas='';
		try
		{
			BI_COL_Davox_wsdl.BasicServiceWSSOAP wsDavox=new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
			wsDavox.timeout_x=120000;
			String xml=armadoXML(nroIdentificacion);
			System.debug('\n\n xml:'+xml+'\n\n');
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,xml));
			//xml=xml.replace('<', '&lt;');
			//xml=xml.replace('>','&gt;');
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,xml));
			cuentas=wsDavox.find(xml);
			System.debug('\n\n xml CONSULTA:'+cuentas+'\n\n');
			//String xmlParaenvio=xml.replace('<', '&lt;');
			//xmlParaenvio=xmlParaenvio.replace('>','&gt;');
			//System.debug('\n\n xml xmlParaenvio: '+xmlParaenvio+'\n\n');
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,cuentas));
			
		} 
		catch(Exception ex) 
		{
			cuentas=ex.getMessage();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,cuentas));
		}
		
		return cuentas;
	}
	@Future(callout=true)
	public static void reservaCuenta(list<string>ids, string cuentaNueva){
		
		list<BI_Log__c> logs=new list<BI_Log__c>();
		BI_COL_Davox_wsdl.BasicServiceWSSOAP wsDavox=new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
		wsDavox.timeout_x=120000;
		
		List<BI_COL_Modificacion_de_Servicio__c> ms=[select id, BI_COL_Clasificacion_Servicio__c,
															/*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c,
															BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c 
													from BI_COL_Modificacion_de_Servicio__c 
												    where id IN :ids limit 1]; //10/11/2016 ANRC: Se comentó el campo antiguo y se adiciono nuevo campo davox1
		
		BI_COL_CustomerAccount_ctr ctCli=new BI_COL_CustomerAccount_ctr();  
		
		try{
			
			for(BI_COL_Modificacion_de_Servicio__c ms_r:ms){
				//if(ms_r.BI_COL_Cuenta_facturar_davox__c==cuentaNueva){ //10/11/2016 ANRC: Se comentó el campo antiguo
				if(ms_r.BI_COL_Cuenta_facturar_davox1__c==cuentaNueva){ //10/11/2016 ANRC: Se adiciono nuevo campo davox1

				ctCli.tipo=Label.BI_COL_LbReservasCuenta;
				//String xml=ctCli.armadoXML(ms_r.BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c+'Õ'+ms_r.BI_COL_Cuenta_facturar_davox__c+'ÕReservado'); //10/11/2016 ANRC: Se comentó el campo antiguo
				String xml=ctCli.armadoXML(ms_r.BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c+'Õ'+ms_r.BI_COL_Cuenta_facturar_davox1__c+'ÕReservado'); //10/11/2016 ANRC: Se adiciono nuevo campo davox1
				System.debug('\n\n xml:'+xml+'\n\n'); 
				string respuestaReserva=wsDavox.find(xml);
				logs.add(new BI_Log__c(BI_COL_Informacion_recibida__c=respuestaReserva,BI_COL_Tipo_Transaccion__c='SALIDA RESERVA CUENTA',BI_COL_Modificacion_Servicio__c=ms_r.id));
				system.debug(logs);
				}
			}
			insert logs;
			
		}catch(exception e){
			
		
		}
	
	}
	
	public String armadoXML(String nroIdentificacion)
	{
		XmlStreamWriter xmlWriter=new XmlStreamWriter();
		xmlWriter.writeStartDocument('utf-8','1.0');
		xmlWriter.writeStartElement(null, 'WS', null);

		xmlWriter.writeStartElement(null, 'USER', null);
		xmlWriter.writeCharacters('USER');
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'PASSWORD', null);
		xmlWriter.writeCharacters('PASSWORD');
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'DATEPROCESS', null);
		xmlWriter.writeCharacters(String.valueOf(System.Today()));
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'PACKCODE', null);
		xmlWriter.writeCharacters(packCode);
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'SEPARATOR', null);
		xmlWriter.writeCharacters('Õ');
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'TYPE', null);
		xmlWriter.writeCharacters(tipo);
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'COUNT', null);
		xmlWriter.writeCharacters('1');
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'VALUES', null);
			xmlWriter.writeStartElement(null, 'VALUE', null);
			xmlWriter.writeCharacters(nroIdentificacion);
			xmlWriter.writeEndElement();
		xmlWriter.writeEndElement();

		xmlWriter.writeEndElement();

		String tramaDavox = xmlWriter.getXmlString();
		tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
		
		xmlWriter = new XmlStreamWriter();
		return tramaDavox;
	}
	
	
	public static String armadoXMLMultiple(list<String> info,string tipo,string packcode)
	{
		
		XmlStreamWriter xmlWriter=new XmlStreamWriter();
		xmlWriter.writeStartDocument('utf-8','1.0');
		xmlWriter.writeStartElement(null, 'WS', null);

		xmlWriter.writeStartElement(null, 'USER', null);
		xmlWriter.writeCharacters('USER');
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'PASSWORD', null);
		xmlWriter.writeCharacters('PASSWORD');
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'DATEPROCESS', null);
		xmlWriter.writeCharacters(String.valueOf(System.Today()));
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'PACKCODE', null);
		xmlWriter.writeCharacters(packcode);
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'SEPARATOR', null);
		xmlWriter.writeCharacters('Õ');
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'TYPE', null);
		xmlWriter.writeCharacters(tipo);
		xmlWriter.writeEndElement();
			
		xmlWriter.writeStartElement(null, 'COUNT', null);
		xmlWriter.writeCharacters('1');
		xmlWriter.writeEndElement();
		
		
		xmlWriter.writeStartElement(null, 'VALUES', null);
		for(string inf:info){	
			xmlWriter.writeStartElement(null, 'VALUE', null);
			xmlWriter.writeCharacters(inf);
			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();
		

		xmlWriter.writeEndElement();

		String tramaDavox = xmlWriter.getXmlString();
		tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
		
		xmlWriter = new XmlStreamWriter();
		
		
		return tramaDavox;
		
	}
	
	
	
	public void armarOpciones()
	{
		if(respuestaxml!=null)
		{
			List<String> valores=respuestaxml.split(separador);
			System.debug('\n\n respuestaxml:'+respuestaxml+'\n\n');
			System.debug('\n\n valores.size():'+valores.size()+'\n\n');
			lstCuentas=new List<String>();
			if(valores.size()>6)
			{
				//options.add(new SelectOption(valores[1],'Nueva Cuenta '+valores[1]));
				lstCuentas.add(valores[1]);
				lstLabelCuenta.add('Nueva Cuenta '+valores[1]);
				cuentaNueva=valores[1];
				List<String> lvalCuenta=valores[2].split(',');
				List<String> lvalCiudad=valores[3].split(',');
				lDireccion=valores[4].split(',');
				List<String> lTipoNegocio=valores[5].split(',');
				List<String> lstCicloFact=valores[6].split(',');
				List<String> lstTapaDetalle=valores[7].split(',');
				List<String> lstCiclo=valores[8].split(',');
				List<String> lstNombreCiclo=valores[9].split(',');
				Integer cont=0;
				System.debug('\n\n Lista Clico de fact'+ lstCicloFact+'\n\n');
				String cicloFact='';
				for(String vCuenta: lvalCuenta)
				{
					String tipoNegocio;
					if(lTipoNegocio[cont]=='SC')
					{
						tipoNegocio='SERVICIOS CORPORATIVO';
					}
					else
					{
						tipoNegocio='VOZ CORPORATIVO';
					}
					
					if(lstCicloFact[cont]=='VE')
					{
						cicloFact='VENCIDA';
					}
					else
					{
						cicloFact='ANTICIPADA';
					}
					
					//options.add(new SelectOption(vCuenta,'Cuenta '+vCuenta + '  -  ' +lvalCiudad[cont]+ '  -  ' +lDireccion[cont]+ '  -  '+ tipoNegocio+' - '+cicloFact+' - '+lstTapaDetalle[cont]+
					//' - CICLO:'+lstCiclo[cont]+' '+lstNombreCiclo[cont]));
					lstCuentas.add(vCuenta);
					lstLabelCuenta.add('Cuenta '+vCuenta + '  -  ' +lvalCiudad[cont]+ '  -  ' +lDireccion[cont]+ '  -  '+ tipoNegocio+' - '+cicloFact+' - '+lstTapaDetalle[cont]+
					' - CICLO:'+lstCiclo[cont]+' '+lstNombreCiclo[cont]);
					mapCuenta.put(vCuenta,lvalCiudad[cont]+ 'Ç' +lDireccion[cont]);
					cont++;
				}
				ViewData();
			}
			else if(valores.size()>1)
			{
				lstCuentas.add(valores[1]);
				lstLabelCuenta.add('Nueva Cuenta '+valores[1]);
				cuentaNueva=valores[1];
				ViewData();
			}
			else
			{
				lstCuentas.add('0');
				lstLabelCuenta.add('Nueva Cuenta 0');
				cuentaNueva='0';
				ViewData();
			}
			
		}
	}
	
	public List<SelectOption> getItems(Integer desde, Integer hasta)
	{
		List<SelectOption> options = new List<SelectOption>();
		for(Integer i=desde;i<hasta;i++)
		{
			options.add(new SelectOption(lstCuentas[i],lstLabelCuenta[i]));
		} 
		return options;
	}
	
	public String getCuentaDx() {
		return CuentaDx;
	}
	
	public void setCuentaDx(String CuentaDx) { this.CuentaDx = CuentaDx; }
	
	public PageReference guardar()
	{
		PageReference pgRe;
		if(CuentaDx!=null)
		{
			List<BI_COL_Modificacion_de_Servicio__c> updste=[select id, BI_COL_Clasificacion_Servicio__c,
			/*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c,BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c 
													from BI_COL_Modificacion_de_Servicio__c 
												where id=:dsto.Id]; //10/11/2016 ANRC: Se comentó el campo antiguo y se adiciono nuevo campo davox1
			if(updste.size()>0)
			{
				//if(updste[0].BI_COL_Clasificacion_Servicio__c=='CAMBIO CUENTA' || updste[0].BI_COL_Clasificacion_Servicio__c=='ALTA' || updste[0].BI_COL_Clasificacion_Servicio__c=='ALTA POR NORMALIZACION' || updste[0].BI_COL_Clasificacion_Servicio__c=='TRASLADO')
				//{
					String data=mapCuenta.get(CuentaDx);
					//List<String> listaResult=data.split('Ç');
					//updste[0].direccion_envio_factura__c=listaResult[1];
					//String queryLike='%' + listaResult[0] + '%';
					//updste[0].Ciudad_envio_factura__c=qetIDCiudad(queryLike);
					//updste[0].BI_COL_Cuenta_facturar_davox__c=CuentaDx; //10/11/2016 ANRC: Se comentó el campo antiguo
					updste[0].BI_COL_Cuenta_facturar_davox1__c=CuentaDx; //10/11/2016 ANRC: Se adiciono nuevo campo davox1
					try 
					{
						update updste;
						//reservaCuenta(new list<string>{updste[0].id},cuentaNueva);						
						pgRe=new PageReference('/'+dsto.Id);
						pgRe.setRedirect(true);
						
					} 
					catch(Exception ex)
					{
						pgRe=null;
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
					}
					

				//}
				//else
				//{
					//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Solo se puede cambiar la cuenta para una Modificacion de Serevicio CAMBIO CUENTA O ALTA'));
				//}
			}
		}
		else
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Por Favor escoja una cuenta'));
		}
		
		return pgRe;
	}
	
	public String qetIDCiudad(String queryLike)
	{
		String idCiudad;
		List<BI_Col_Ciudades__c> listCiu=[Select c.Id, c.Name from BI_Col_Ciudades__c c where name like:queryLike limit 1];
		if(listCiu.size()>0)
		{
			idCiudad=listCiu[0].Id;
		}
		return idCiudad;
	}
	
	public string retornaCadena(string xml){
		
		string elstring='';
		
		Dom.Document doc = new Dom.Document();
		doc.load(xml);
		Dom.XMLNode WS = doc.getRootElement();
		List<dom.XMLNode> lsxmlValues=WS.getChildElements(); 
		for(dom.XMLNode dxm:lsxmlValues)
		{
			if(dxm.getName()=='VALUES')
			{
				List<dom.XMLNode> values=dxm.getChildElements();
				for(dom.XMLNode dxmv: values)
				{
							elstring+=dxmv.getText();
				}
			}
		}
		return elstring;   
	
	}
	
	//IMPLEMENTACION PAGINADOR
	public Integer getPageNumber()
	{
		return pageNumber;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}
	
	public Boolean getPreviousButtonEnabled()
	{
		return !(pageNumber > 1);
	}
	
	public Boolean getNextButtonDisabled()
	{
		if (this.lstCuentas == null)
		{
			return true;
		}
		else
		{
			return ((pageNumber * pageSize) >= lstCuentas.size());
		}
	
	}
	
	public Integer getTotalPageNumber()
	{
		if (totalPageNumber == 0 && this.lstCuentas !=null)
		{
			totalPageNumber = this.lstCuentas.size() / pageSize;
			Integer mod = this.lstCuentas.size() - (totalPageNumber * pageSize);
			if (mod > 0)
			totalPageNumber++;
		}
		return totalPageNumber;
	}

	public PageReference nextBtnClick() 
	{
		if(pageNumber != null)
			BindData(pageNumber + 1);
		return null;
	
	}
	
	public PageReference previousBtnClick() 
	{
		if(pageNumber != null)
			BindData(pageNumber - 1);
		return null;
	
	}
	
	public PageReference ViewData()
	{
		totalPageNumber = 0;
		BindData(1);
		return null;
	}
	
	/**
	* Posiciona el registro segun el numero de pagina
	* @param newPageIndex Indice de la pagina
	* @return 
	*/
	public void BindData(Integer newPageIndex)
	{
		try
		{
			//lstCuentas = new List<String>();
			Transient Integer counter = 0;
			Transient Integer min = 0;
			Transient Integer max = 0;
			system.debug('\n\n newPageIndex==='+newPageIndex+' pageNumber==='+pageNumber+' pageSize= '+pageSize+'\n\n' );
			if (newPageIndex > pageNumber)
			{
				min = pageNumber * pageSize;
				max = newPageIndex * pageSize;
			}
			else
			{
				max = newPageIndex * pageSize;
				min = max - pageSize;
				//min = (min <>);
			}
			 
			if(max>lstCuentas.size())
			{
				max=lstCuentas.size();
			}
			System.debug('\n\nmax= '+max+' min='+min+'\n\n');
			options=getItems(min,max);
			
			pageNumber = newPageIndex;
			
			if (lstCuentas == null || lstCuentas.size() <= 0)
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.BI_COL_lblDatosNoDisponiblesVista));
		}
		catch(Exception ex)
		{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()+' StackTrac '+ex.getStackTraceString() +' Linea'+ex.getLineNumber()));
		}	
	}
	

}