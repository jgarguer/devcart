global class BI_SumatoriaTdcpBatch implements Database.Batchable<sObject>, Database.Stateful {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Gawron, Julián
     Company:       Accenture
     Description:   Job to Update sumatoria on closed oppportunities
     Test Class:    BI_SumatoriaTdcpBatch_TEST
    
     History:
     
     <Date>                     <Author>                    <Change Description>
    21/03/2017                  Gawron, Julián              Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    public String query;
    Integer errores;
    Integer actualizados;
    Integer nulos = 0;
    Integer vueltas = 0;
    List<String> lst_errors = new List<String>();
    
    global BI_SumatoriaTdcpBatch() {

      query = 'select id, BI_O4_TdCP__c, CurrencyIsoCode, BI_Productos_numero__c from Opportunity '+
              'where (BI_Tasa_SUM_EAI_Presu__c = null '+
              '  OR BI_Tasa_SUM_FCV_Presu__c = null '+
              '  OR BI_Tasa_SUM_GANANCIA_Neta_Presu__c = null)'+
              ' and BI_O4_TdCP__c > 0 '+
              ' and BI_Productos_numero__c > 0';
    
        System.debug('### query--> ' + query);
        errores = 0;
        actualizados = 0;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc){
        BI_SumatoriaTdcpBatch obj = new BI_SumatoriaTdcpBatch();
        Database.executeBatch(obj,200);
    }


global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('### query--> ' + query);
        System.debug('### scope--> ' + scope);
        
   vueltas +=1; 
   Set<Id> oppsId = new Set<Id>();
  try{

    //Recolectamos los Id de las oportunidades
    for(sObject opp : scope){
        oppsId.add(((Opportunity)opp).Id);
    }

    List<AggregateResult> toProcess_obj = new List<sObject>();
    //Map<Id,NE__Order__c> map_ord = new Map<Id,NE__Order__c>(); 
    List<Id> lst_Oi_id = new List<Id>();
    Map <Integer, BI_bypass__c> mapa;

        //El método es llamado desde Opportunitys
        toProcess_obj = [
            Select 
             NE__OrderId__r.NE__OptyId__r.Id optyId,
             NE__OrderId__r.Id OrderId, 
            sum(BI_Tasa_GANANCIA_Neta_Presu__c) BI_Tasa_GANANCIA_Neta_Presu__c,
            sum(BI_Tasa_EAI_Presu__c) BI_Tasa_EAI_Presu__c,
            sum(BI_Tasa_NAV_Presu__c) BI_Tasa_NAV_Presu__c,
            sum(BI_Tasa_FCV_Presu__c) BI_Tasa_FCV_Presu__c,
            sum(BI_Tasa_OneTimeFeeOv__c) BI_Tasa_OneTimeFeeOv__c, 
            sum(BI_Tasa_RecurringChargeOv__c) BI_Tasa_RecurringChargeOv__c, 
            sum(BI_Tasa_Ingreso_Recurrente_Anterior_Prod__c) BI_Tasa_Ingreso_Recurrente_Anterior_Prod__c 
            
             from NE__OrderItem__c
            where NE__OrderId__r.NE__OptyId__r.Id in :oppsId


           and (
                (NE__OrderId__r.NE__OrderStatus__c like 'Activ%' AND  (NOT NE__OrderId__r.NE__OptyId__r.StageName like 'F1%')) OR 
                (NE__OrderId__r.NE__OrderStatus__c like 'F1%' AND NE__OrderId__r.NE__OptyId__r.StageName like 'F1%' )
            )  
            group by  NE__OrderId__r.NE__OptyId__r.Id, NE__OrderId__r.Id
        ];

        
        // Query sobre campos en la orden para actualizar la oportunidad
            Map<Id,NE__Order__c> map_ord = new Map<Id,NE__Order__c>([ Select 
                        Id,
                        BI_O4_TGS_NBAV_Presup__c,
                        BI_O4_TGS_RAV_Presup__c,
                        BI_FCV_Presupuesto__c,
                        NE__OptyId__r.BI_O4_TGS_NBAV_Presup__c,
                        NE__OptyId__r.BI_O4_TGS_NBAV_Budget__c,
                        NE__OptyId__r.BI_O4_RAV_Budget__c,
                        NE__OptyId__r.BI_O4_TGS_FCV_Budget__c

                         from NE__Order__c 
                        where NE__OptyId__r.Id in :oppsId ]);

                  System.debug('updateSumatoria '+toProcess_obj);
                
                  System.debug('updateSumatoria vueltas '+ vueltas );
                

// Modificamos todos los objetos necesarios
    if(!toProcess_obj.isEmpty()){
       System.debug('updateSumatoria empezar a procesar:' +toProcess_obj);
        List<Opportunity> lst_OppsToUpdate = new List<Opportunity>();
        Map<Id, Opportunity> map_OppsToUpdate = new Map<Id, Opportunity>();
        List<NE__Order__c> lst_OrdersToUpdate = new List<NE__Order__c>();
        for(AggregateResult obj: toProcess_obj){

            lst_OrdersToUpdate.add(
            new NE__Order__c(
                        Id                                           = (Id)obj.get('OrderId'),
                        BI_Tasa_OI_SUM_FCV_Presu__c                  = (Double)obj.get('BI_Tasa_FCV_Presu__c'),               
                        BI_Tasa_OI_SUM_GANANCIA_Neta_Presu__c        = (Double)obj.get('BI_Tasa_GANANCIA_Neta_Presu__c'),
                        BI_Tasa_OI_SUM_EAI_Presu__c                  = (Double)obj.get('BI_Tasa_EAI_Presu__c'),               
                        BI_Tasa_OI_SUM_OneTimeFeeOv__c               = (Double)obj.get('BI_Tasa_OneTimeFeeOv__c'),
                        BI_Tasa_OI_SUM_NAV_Presu__c                  = (Double)obj.get('BI_Tasa_NAV_Presu__c'),
                        BI_Tasa_OI_SUM_Ingreso_Recurrente_Anteri__c  = (Double)obj.get('BI_Tasa_Ingreso_Recurrente_Anterior_Prod__c'),
                        BI_Tasa_OI_SUM_RecurringChargeOv__c          = (Double)obj.get('BI_Tasa_RecurringChargeOv__c')
                        ));

                map_OppsToUpdate.put((Id)obj.get('optyId'),
                        new Opportunity(
                                   Id                                         =   (Id)obj.get('optyId'),
                                   BI_Tasa_SUM_GANANCIA_Neta_Presu__c         =   (Double)obj.get('BI_Tasa_GANANCIA_Neta_Presu__c'),
                                   BI_Tasa_SUM_EAI_Presu__c                   =   (Double)obj.get('BI_Tasa_EAI_Presu__c'),
                                   BI_Tasa_SUM_OneTimeFeeOv__c                =   (Double)obj.get('BI_Tasa_OneTimeFeeOv__c'),
                                   BI_Tasa_SUM_NAV_Presu__c                   =   (Double)obj.get('BI_Tasa_NAV_Presu__c'),
                                   BI_Tasa_SUM_RecurringChargeOv__c           =   (Double)obj.get('BI_Tasa_RecurringChargeOv__c'),
                                   BI_Tasa_SUM_FCV_Presu__c                   =   (Double)obj.get('BI_Tasa_FCV_Presu__c'),
                                   BI_Tasa_SUM_Ingreso_Recurrente_Anterior__c =   (Double)obj.get('BI_Tasa_Ingreso_Recurrente_Anterior_Prod__c'),
                                   BI_O4_TGS_NBAV_Presup__c                   =   map_ord.get((Id)obj.get('OrderId')).BI_O4_TGS_NBAV_Presup__c,
                                   BI_O4_TGS_NBAV_Budget__c                   =   map_ord.get((Id)obj.get('OrderId')).BI_O4_TGS_NBAV_Presup__c,
                                   BI_O4_RAV_Budget__c                        =   map_ord.get((Id)obj.get('OrderId')).BI_O4_TGS_RAV_Presup__c,
                                   BI_O4_TGS_FCV_Budget__c                    =   map_ord.get((Id)obj.get('OrderId')).BI_FCV_Presupuesto__c
                                    )
                );
            

              if((Double)obj.get('BI_Tasa_FCV_Presu__c')== null)
               nulos += 1;
            }
///////// Actualiza todos los campos
        
            System.debug('updateSumatoria lst_OppsToUpdate: ' + map_OppsToUpdate.values() );
            System.debug('Tamaño map ordenes: ' + map_ord.size());
            System.debug('Tamaño lista oportunidades: ' + toProcess_obj.size());

                             
    
             try{
                 //Desactivamos los triggers, porque solo se insertan datos sin otras relaciones. 
                mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true,true,false,false);


                Database.SaveResult[] lsr = database.update( map_OppsToUpdate.values(), false );
        
                 for( Database.SaveResult sr : lsr )
                   {
                       if(!sr.isSuccess() )
                       {
                           Database.Error err = sr.getErrors()[0];
                           string errorDetalles = '';

                           for( Database.Error errt:sr.getErrors() )
                               errorDetalles += errt.getMessage();
                           
                           errores += 1;
                           String textoMSG = 'Nro ID:' + sr.getId() + ' Detalles: ' + errorDetalles;    
                           lst_errors.add(textoMSG);
                       }else{
                           actualizados += 1;
                       }
                   }

                Database.SaveResult[] lsr1 = database.update( lst_OrdersToUpdate, false );
        
                 for( Database.SaveResult sr : lsr1 )
                   {
                       if(!sr.isSuccess() )
                       {
                           Database.Error err = sr.getErrors()[0];
                           string errorDetalles = '';

                           for( Database.Error errt:sr.getErrors() )
                               errorDetalles += errt.getMessage();
                          errores += 1;
                           String textoMSG = 'Nro ID:' + sr.getId() + ' Detalles: ' + errorDetalles;    
                           lst_errors.add(textoMSG);
                       }else{
                          actualizados += 1;
                       }          
                   }

                 System.debug('updateSumatoria lst_OppsToUpdate despues de update: ' + lst_OppsToUpdate );
                 System.debug('updateSumatoria lst_OrdersToUpdate despues de update: ' + lst_OrdersToUpdate );
                }catch(exception Exc){
                 System.debug('updateSumatoria error' + Exc);
                  errores += 1;
                }finally{
                    if(mapa != null){
                        BI_MigrationHelper.disableBypass(mapa);
                     }
                }

    }


    }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_SumatoriaTdcpBatch.updateSumatoria', 'BI_EN', Exc, 'Trigger');
    }
            System.debug('updateSumatoria fin');
  }
    
    global void finish(Database.BatchableContext BC) {
        
            Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();
                String body = '<h1>Actualización de sumatoria de tasas (BI_SumatoriaTdcpBatch) finalizada ';
                if(lst_errors.size() > 0 || errores > 0)
                    body += errores +' errores</h1>' + String.join(lst_errors,'<br>');
                else 
                    body +=  'sin errores</h1> Ordenes y oportunidades actualizadas: ' + actualizados + ' y ' + nulos * 2 + ' son nulos'+ '<p> en vueltas:' + vueltas + '</p>';
                mailOwner.setTargetObjectId(UserInfo.getUserId());
                mailOwner.setHtmlBody(body);
                mailOwner.setSubject('BatchDeActualizacion');

                mailOwner.setSenderDisplayName('BatchDeActualizacion');
                mailOwner.setSaveAsActivity(false);
            if(!Test.isRunningTest()){   
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailOwner});
            }


    }

}