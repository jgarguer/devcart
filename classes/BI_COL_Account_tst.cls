/*************************************************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-25-05      Daniel ALexander Lopez (DL)     New test Class
* @version	 1.1    2016-07-18		Manuel Medina					Changed record type name according to new label name.
*            1.2    2017-01-02      Gawron, Julián E.               Adding testSetup
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
                    19/09/2017     Angel F. Santaliestra             Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
**************************************************************************************************************************/

@isTest
private class BI_COL_Account_tst
{
 
    Public static list <UserRole>                       lstRoles;
    Public static User                          objUsuario = new User();
    public static BI_COL_Anexos__c                      objAnexos;
    public static BI_COL_Modificacion_de_Servicio__c    objModSer;
    public static BI_COL_Descripcion_de_servicio__c     objDesSer;
    public static Account                   objCuenta;
    public static Opportunity               objOpp;
    public static List<RecordType>                      rtBI_FUN;
    public static List<RecordType>                      rtOpp;
    public static Contact                               objContacto;
  public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;

    @testSetup static void crearData()
    {
         //List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
         //list<User> lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

         //lstRol
           //lstRoles = new list <UserRole>();
           //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

           //ObjUsuario
         //  objUsuario = new User();
         //  objUsuario.Alias = 'standt';
         //  objUsuario.Email ='pruebas@test.com';
         //  objUsuario.EmailEncodingKey = '';
         //  objUsuario.LastName ='Testasdfasdfing';
         //  objUsuario.LanguageLocaleKey ='en_US';
         //  objUsuario.LocaleSidKey ='en_US'; 
         //  objUsuario.ProfileId = lstPerfil.get(0).Id;
         //  objUsuario.TimeZoneSidKey ='America/Los_Angeles';
         //  objUsuario.UserName ='prasdfasdfbas@test.com';
         //  objUsuario.EmailEncodingKey ='UTF-8';
         //  objUsuario.UserRoleId = lstRoles.get(0).Id;
         //  objUsuario.BI_Permisos__c ='Sucursales';
         //  objUsuario.Pais__c='Colombia';
         
         //objUsuario = BI_COL_CreateData_tst.getCreateUSer();
          // list<User> lstUsuarios = new list<User>();
          // lstUsuarios.add(objUsuario);
          //insert lstUsuarios;

        // System.runAs(lstUsuarios[0])
         //{
          BI_bypass__c objBibypass = new BI_bypass__c();
          objBibypass.BI_migration__c=true;
          insert objBibypass;
      
          objCuenta                                     = new Account();
          objCuenta.Name                                = 'prueba';
          objCuenta.BI_Country__c                       = 'Colombia';
          objCuenta.TGS_Region__c                       = 'América'; 
          objCuenta.BI_Tipo_de_identificador_fiscal__c  = 'NIT';
          objCuenta.CurrencyIsoCode                     = 'GTQ';
          objCuenta.BI_No_Identificador_fiscal__c       = '1234567890';
          objCuenta.BI_Country__c                       = 'Colombia';
          objCuenta.TGS_Region__c                       = 'América';
          objCuenta.BI_Segment__c                       = 'test';
          objCuenta.BI_Subsegment_Regional__c           = 'test';
          objCuenta.BI_Territory__c                     = 'test';
          insert objCuenta;

          System.debug('\n\n\n Sosalida objCuenta '+ objCuenta );

          //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

          //objContacto
            objContacto                                     = new Contact();
            objContacto.LastName                          = 'Test';
            objContacto.BI_Country__c                     = 'Colombia';
            objContacto.CurrencyIsoCode                   = 'COP'; 
            objContacto.AccountId                         = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c        = 'Administrador Canal Online';
            objContacto.Phone                               = '57123456';
            objContacto.MobilePhone                 = '3104785925';
            objContacto.Email                   = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c       = 'Dirprueba';
            objContacto.BI_COL_Ciudad_Depto_contacto__c   = objCiudad.Id;
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            //REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.BI_Tipo_de_contacto__c          = 'Autorizado;General';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto;

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name                = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);

          objOpp                      = new Opportunity();
          objOpp.Name                   = 'prueba opp';
          objOpp.AccountId                = objCuenta.Id;
          //objOpp.TGS_Region__c              = 'América';
          objOpp.BI_Country__c              = 'Colombia';
          objOpp.CloseDate                = System.today().addDays(+5);
          objOpp.StageName                = 'F6 - Prospecting';
          objOpp.CurrencyIsoCode              = 'GTQ';
          objOpp.Certa_SCP__contract_duration_months__c   = 12;
          objOpp.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
          objOpp.BI_COL_Autoconsumo__c          = true;
          insert objOpp;
          System.debug('\n\n\n Sosalida objOpp '+ objOpp );

          rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];

          objAnexos               = new BI_COL_Anexos__c();
          objAnexos.Name          = 'FUN-0041414';
          objAnexos.RecordTypeId  = rtBI_FUN[0].Id;
          insert objAnexos;
          System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);

          objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
          objDesSer.BI_COL_Oportunidad__c                     = objOpp.Id;
          objDesSer.CurrencyIsoCode               = 'COP';
          insert objDesSer;
          System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);
          List<BI_COL_Descripcion_de_servicio__c> lstqry = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
          System.debug('\n\n\n Sosalida lstqry '+ lstqry);

          objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
            objModSer.BI_COL_FUN__c                         = objAnexos.Id;
            objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModSer.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModSer.BI_COL_Bloqueado__c                   = false;
            objModSer.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            insert objModSer;
          System.debug('\n\n\n Sosalida objModSer '+ objModSer);
      //} 

    }
    
    @isTest static void prueba1()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        objAnexos = [Select Id, Name, RecordTypeId from BI_COL_Anexos__c limit 1];
        System.runAs(objUsuario) 
        {
            //crearData();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('idFUN', objAnexos.Id);
            Test.stopTest();         
            // BI_COL_AutoconsumoFUN_ctr ctr = new BI_COL_AutoconsumoFUN_ctr();
            //BI_COL_AutoconsumoFUN_ctr.WrapperLst  ctrLst = new BI_COL_AutoconsumoFUN_ctr.WrapperLst(); 
            //System.debug('\n\n\n Sosalida ctr.lstGlobal '+ ctr.lstGlobal+' ctr.logoMovistar '+ctr.logoMovistar+' ctr.numOrdenServicio '+ ctr.numOrdenServicio );
        }
    }

    /* //Unused methods
    public static RecordType consultarTipoOportunidadPrueba()
    {
        return [SELECT 
                    Id 
                    FROM 
                    RecordType 
                    WHERE 
                    SObjectType = 'Opportunity' 
                    AND Name = 'Oportunidad Local' 
                    LIMIT 1];
    }

      public static RecordType consultarTipoRegistoCuentaPrueba()
      
      {
            return [SELECT 
                    Id 
                    FROM 
                    RecordType 
                    WHERE 
                    SObjectType = 'Account' 
                    AND Name = 'Cliente BIEN' 
                    LIMIT 1];
      }

      public static User consultarUsuarioPrueba()
      {
          return [SELECT 
                  Id 
                  FROM 
                  User 
                  WHERE 
                  IsActive = TRUE 
                  LIMIT 1];
      }
      
      public static Account consultarClientePrueba()
      {
            return [SELECT
                    Id
                    FROM
                    Account
                    LIMIT 1];
      }

       public static Contact consultarContactoPrueba()
      {
            return [SELECT
                    Id
                    FROM
                    Contact
                    LIMIT 1];
      }
      */
}