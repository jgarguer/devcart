/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Manuel Ochoa
 Company:       New Energy Aborda
 Description:   Webservice for SIGMA to FullStack integration
 Test Class:    
 History:
 
 <Date>            <Author>             <Description>
 24-08-2017			Manuel Ochoa		First version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class TGS_Salesforce_Ordering_Global_FAST {

	public enum Operation_StatusType {Processed, Cancelled, Idle}

	public static Date billingStartDate;
	public static Date billingEndDate;
    public static String use_case_aux;

	global with sharing class AttachmentReq{ 
		webservice String Attachment_ID;
		webservice String Attachment_Name;
		webservice Blob Attachment_Data;
	}

	global with sharing class AttachmentList{ 
		webservice List<AttachmentReq> attachment; 
	}

	// webservice response
	global with sharing class OutputMapping6_FAST{
		webservice String                Error_Code;
		webservice String                Error_Msg;
		webservice String                Operation_Instance_ID; // CaseNumber
		webservice Operation_StatusType  Operation_Status;
		webservice String                SRM_Request_ID;		// ParentOI.Name
		webservice String                Incident_ID;			// CaseNumber
	}

	// data received from webservice
	@testVisible class RequestInfo extends TGS_ErrorIntegrations_Helper.RequestInfo  {
		public String System_ID;
		public String Use_Case;
		public String Operation_Number;
		public String External_ID;
		public String Attributtes;
		public String ListAttributes;
		public AttachmentList Attachments;
	}

	
	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   webservice main method
    Test Class:
    IN:    			
    OUT:
    
	History
	<Date>      <Author>     <Description>
	------------------------------------------------------------*/
	webservice static OutputMapping6_FAST Creation(
		String System_ID,
		String Use_Case,
		String Operation_Number,
		String External_ID,
		String Attributtes,
		String ListAttributes,
		AttachmentList Attachments
	){
		OutputMapping6_FAST ret = new OutputMapping6_FAST();
		Savepoint svp = Database.setSavepoint();
		RequestInfo incomingXML = new RequestInfo();
		incomingXML.System_ID        = System_ID;        incomingXML.Use_Case       = Use_Case;
		incomingXML.Operation_Number = Operation_Number; incomingXML.External_ID    = External_ID;
		incomingXML.Attributtes      = Attributtes;      incomingXML.ListAttributes = ListAttributes;
		incomingXML.Attachments      = Attachments;
        use_case_aux = Use_Case;

		String recordIdentifier;
        try {
            try {
            	system.debug(LoggingLevel.ERROR,'Entering TGS_Salesforce_Ordering_Global_FAST creation');
            	
                TGS_RecordTypes_Util.loadRecordTypes(new List<Schema.SObjectType> { Account.SObjectType, Case.SObjectType, NE__Product__c.SObjectType, NE__Order__c.SObjectType, NE__OrderItem__c.SObjectType, NE__DynamicPropertyDefinition__c.SObjectType });
                TGS_Salesforce_Ordering_Global_Helper.processAttachments_FAST(Attachments.attachment);
                Map <String, List <TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI>> map_products = TGS_Salesforce_Ordering_Global_Helper.processListAttributes(ListAttributes);
                String service_name = TGS_Salesforce_Ordering_Global_Helper.processDataTemplate(map_products);
                String divisa = TGS_Salesforce_Ordering_Global_Helper.processCharges(service_name, map_products);
                
                if(divisa == null){
                    divisa = 'EUR';
                }
                
                
                system.debug(LoggingLevel.ERROR,'@@@ ********* Received data');
                system.debug(Use_Case);
                system.debug(Attributtes);
                system.debug(service_name);
                system.debug(map_products);
                system.debug(divisa);
                system.debug(External_ID);

                //add isFullStack flag
                Attributtes = Attributtes + 'isFullStack="true";';

                NE__Order__c order = TGS_Salesforce_Ordering_Global_Helper.generateOrder(Use_Case, Attributtes, service_name, map_products, divisa, External_ID);
                TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI parent = map_products.get('Parent')[0];
                TGS_Salesforce_Ordering_Global_Helper.translateProducts(map_products, divisa);

                system.debug('@@@ *** ORDER: ' + order);
    
                recordIdentifier = order.TGS_SIGMA_ID__c + ' | ' + map_products.get('Parent')[0].attributes.get('SIGMA WH Adminitrative Number');
    
                /*for(String k : map_products.keySet()) {
                    System.debug('.* ' + k);
                    for(TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI ci : map_products.get(k)){
                        for(String str : ci.attributes.keySet()){
                            System.debug('.*********' + str + '---->' + ci.attributes.get(str));
                        }
                        System.debug('***************************');
                    }
    
                }*/
    
                System.debug(LoggingLevel.ERROR, 'map_products: ' + JSON.serialize(map_products));
    
                // Prevent this from running; We do not need the case's service to be updated
                NETriggerHelper.setTriggerFired('NECheckOrderItems.TGS_NEOrderItemMethods.setCaseService');
    
                // Only one Use_case for FullStack integration            

                if(Use_Case == 'createOrder') {
                    System.debug('##*** createOrder');
                    System.debug(parent.attributes);
                    String stringAction= getAction(Attributtes);
                    //if(stringAction=='CREATE_SERVICE' || stringAction=='TERMINATION_SERVICE'){
                    if(stringAction=='CREATE_SERVICE'){
                        System.debug('***  CreateOrder: ' + stringAction);
			            // functional modifyContractual_FAST - deactivate in case you don't want to create orders
                        ret = translateOutputMapping(TGS_Salesforce_Ordering_Global_Helper.modifyContractual_FAST(map_products, order, divisa));
                        // mock response for webservice - activate for test with no insertion of data
                        //ret = mockResponse(stringAction);
                    }
                    if(stringAction=='TERMINATION_SERVICE'){
                        System.debug('***  CreateOrder: ' + stringAction);
                        // functional modifyContractual_FAST - deactivate in case you don't want to create orders
                        ret = translateOutputMapping(TGS_Salesforce_Ordering_Global_Helper.modifyContractual_FAST(map_products, order, divisa));
                        // mock response for webservice - activate for test with no insertion of data
                        //ret = mockResponse(stringAction);
                    }
			        if(stringAction=='MODIFY_SERVICE'){
                        System.debug('***  ModifyService: ' + stringAction);
			            // functional modifyData - deactivate in case you don't want to create orders
                        ret = translateOutputMapping(TGS_Salesforce_Ordering_Global_Helper.modifyContractual_FAST(map_products, order, divisa));
                        // mock response for webservice - activate for test with no insertion of data
                        //ret = mockResponse(stringAction);
			        }
                }else {			
                    inflateErrorResponse(ret, '11420', null);
                }

                 
                /*System.debug(LoggingLevel.FINE, '##** ' + order.Id);
                System.debug(LoggingLevel.FINE, '##*** ' + order);
    
                System.debug(LoggingLevel.FINE, '.** CIs **.');
                for(String prod : map_products.keyset()) {
                    System.debug(LoggingLevel.FINE, '.**** ' + prod);
                    for(TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI ci : map_products.get(prod)) {
                        System.debug(LoggingLevel.FINE, '.****** OI.Curr: ' + ci.oi.CurrencyISOCode + ' OI.NRC: ' + ci.oi.NE__OneTimeFeeOv__c + ' OI.MRC: ' + ci.oi.NE__RecurringChargeOv__c);
                        System.debug(LoggingLevel.FINE, '.****** Catalog Item: ' + ci.sourceId);
                        for(String k : ci.attributes.keySet()) {
                            System.debug(LoggingLevel.FINE, '.****** ' + k + ': ' + ci.attributes.get(k));
                        }
                        System.debug(LoggingLevel.FINE, '.**********.');
                    }
                }*/
    
                if(ret.Error_Code != null && ret.Error_Code != '0') {
                    Database.rollback(svp);
                    ret.Operation_Instance_ID = null;
                    ret.SRM_Request_ID = null;
                    ret.Incident_ID = null;
                    // This will not have a stacktrace
                    TGS_ErrorIntegrations_Helper.createExceptionLog('SigmaWH_FAST', Use_Case, new IntegrationException(ret.Error_Msg), incomingXML, recordIdentifier);
                } else {
                    ret.Operation_Status = Operation_StatusType.Processed;
                    recordIdentifier += ' | ' + ret.Incident_ID + ' | ' + 	ret.SRM_Request_ID;
                }
    
            } catch (Exception exc) {
                Database.rollback(svp);
                System.debug(LoggingLevel.ERROR, 'ERROR: ' + exc.getMessage() + '\nAt: ' + exc.getStackTraceString());
                if(inflateErrorResponse(ret, exc.getMessage(), null).Error_Code == null) {
                    inflateErrorResponse(ret, '11015', exc.getMessage());
                }
                ret.Operation_Instance_ID = null;
                ret.SRM_Request_ID = null;
                ret.Incident_ID = null;
                TGS_ErrorIntegrations_Helper.createExceptionLog('SigmaWH_FAST', Use_Case, exc, incomingXML, recordIdentifier);
            }
    
            //// Insert status
            
            if(ret.Error_Code == '0') {
                TGS_ErrorIntegrations_Helper.createSuccessLog('SigmaWH_FAST', Use_Case, incomingXML, recordIdentifier);
            }
        } catch(Exception exc) {
            Database.rollback(svp);
            inflateErrorResponse(ret, '11015', exc.getMessage());
            ret.Operation_Instance_ID = null;
            ret.SRM_Request_ID = null;
            ret.Incident_ID = null;
            TGS_ErrorIntegrations_Helper.createExceptionLog('SigmaWH_FAST', Use_Case, new IntegrationException('Unexpected error', exc), incomingXML, recordIdentifier);
            System.debug('Exception: ' + exc.getMessage());
            System.debug('Stacktrace: ' + exc.getStackTraceString());
        }
		

		return ret;
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Error documentation method
    Test Class:
    IN:    			
    OUT:
    
	History
	<Date>      <Author>     <Description>
	------------------------------------------------------------*/

	public static OutputMapping6_FAST inflateErrorResponse(OutputMapping6_FAST res, String ecode, String additionalInfo) {
		res.Operation_Status = Operation_StatusType.Cancelled;

		
        if(ecode == '11420'){////////////////////////done
            res.Error_Code = '11420';
            res.Error_Msg = 'Error(s) while checking the mandatory fields: Use_Case (invalid operation)';
        }else if (ecode == '11015'){
            res.Error_Code = '11015';
            res.Error_Msg = 'An unexpected error has occurred while parsing the information: '+additionalInfo;
        }
        return res;


        /*if(ecode == '11420'){////////////////////////done
            res.Error_Code = '11420';
            res.Error_Msg = 'Error(s) while checking the mandatory fields: Use_Case (invalid operation)';
        } else if(ecode == '54321'){
            res.Error_Code = '54321';
            res.Error_Msg = 'Invalid customer name';
        } else if(ecode == '54322'){
            res.Error_Code = '54322';
            res.Error_Msg = 'Invalid contact';
        } else if(ecode == '54323'){
            res.Error_Code = '54323';
            res.Error_Msg = 'The order\'s case was not created';
        } else if(ecode == '54324'){
            res.Error_Code = '54324';
            res.Error_Msg = 'Invalid external id';
        } else if(ecode == '54325'){
            res.Error_Code = '54325';
            res.Error_Msg = 'Invalid SIGMA WH Adminitrative Number';
        } else if(ecode == '54326'){
            res.Error_Code = '54326';
            res.Error_Msg = 'The order\'s configurations were not created';
        } else if(ecode == '54327'){
            res.Error_Code = '54327';
            res.Error_Msg = 'The order has no CIs';
        } else if(ecode == '54328'){ // FAST - added error for invalid cost center
            res.Error_Code = '54328';
            res.Error_Msg = 'Invalid cost center';
        } else if(ecode == '54329'){ // FAST - added error for invalid site
            res.Error_Code = '54329';
            res.Error_Msg = 'Invalid site';
        } else if(ecode == '54330'){ // FAST - added error for invalid RFS Date
            res.Error_Code = '54330';
            res.Error_Msg = 'Invalid RFS Date';
        } else if(ecode == '11004'){
            res.Error_Code = '11004';
            res.Error_Msg = 'CI class not found';
        } else if(ecode == ('12022')){
            res.Error_Code = '12022';
            res.Error_Msg = 'Unable to find the attachment '+additionalInfo+' in the system';
        } else if(ecode == '12021'){
            res.Error_Code = '12021';
            res.Error_Msg = 'Attachments size limit exceeded';
        } else if(ecode == '12007'){
            res.Error_Code = '12007';
            res.Error_Msg = 'The next attributes don\'t belong to this service: '+additionalInfo;
        } else if(ecode == '11009'){////////////////////////done
            res.Error_Code = '11009';
            res.Error_Msg = 'There is a request in progress with this CI';
        } else if(ecode == '54328'){////////////////////////done
            res.Error_Code = '54328';
            res.Error_Msg = 'Previous case asset missing';
        } else if(ecode == '54329'){
            res.Error_Code = '54329';
            res.Error_Msg = 'Case status invalid for this operation';
        } else if (ecode == '11015'){
            res.Error_Code = '11015';
            res.Error_Msg = 'An unexpected error has occurred while parsing the information: '+additionalInfo;
        }
		return res;*/
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Transalation from TGS_Salesforce_Ordering_Global.OutputMapping6 to this.OutputMapping6
    Test Class:
    IN:    	        TGS_Salesforce_Ordering_Global.OutputMapping6
    OUT:            OutputMapping6_FAST
    
	History
	<Date>      <Author>     <Description>
	------------------------------------------------------------*/

	@TestVisible private static OutputMapping6_FAST translateOutputMapping(TGS_Salesforce_Ordering_Global.OutputMapping6 ret){
		OutputMapping6_FAST ret_FAST = new OutputMapping6_FAST();
        ret_FAST.Error_Code=ret.Error_Code;
        ret_FAST.Error_Msg= ret.Error_Msg;
        ret_FAST.Operation_Instance_ID=ret.Operation_Instance_ID; // CaseNumber
        if(ret.Operation_Status==TGS_Salesforce_Ordering_Global.Operation_StatusType.Processed){
            ret_FAST.Operation_Status = TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Processed;
        }
        if(ret.Operation_Status==TGS_Salesforce_Ordering_Global.Operation_StatusType.Cancelled){
            ret_FAST.Operation_Status = TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Cancelled;
        }
        if(ret.Operation_Status==TGS_Salesforce_Ordering_Global.Operation_StatusType.Idle){
            ret_FAST.Operation_Status = TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Idle;
        }
        
        ret_FAST.SRM_Request_ID=ret.SRM_Request_ID;        // ParentOI.Name
        ret_FAST.Incident_ID=ret.Incident_ID;

        return ret_FAST;
	}

    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Get action from attributes
    Test Class:
    IN:             string          
    OUT:            string
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/

    private static String getAction(String attributes){
        List<String> first_split = attributes.split('";');
        String strAction='';
        for(String attr : first_split){
            List<String> second_split = attr.split('="');
            if(second_split[0]=='Action'){
                strAction = second_split[1];
                break;
            }
        }
        return strAction;
    }

    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   mock response for connectivity tests
    Test Class:
    IN:                   
    OUT:            OutputMapping6_FAST
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/

    private static OutputMapping6_FAST mockResponse(string action){
        OutputMapping6_FAST ret_FAST = new OutputMapping6_FAST();
        ret_FAST.Error_Code='0';
        ret_FAST.Error_Msg= 'Test received - action: ' + action;
        ret_FAST.Operation_Instance_ID='Operation_Instance_ID'; // CaseNumber
        ret_FAST.Operation_Status = TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Processed;
        ret_FAST.SRM_Request_ID='SRM_Request_ID';        // ParentOI.Name
        ret_FAST.Incident_ID='Incident_ID';

        return ret_FAST;
    }
}