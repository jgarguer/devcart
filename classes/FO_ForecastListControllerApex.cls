/*****************************************************
 * Author:Javier Lopez andradas
 * Company: gCTIO
 * Description: controller of the main page of Forecast editing or "Adjustment"
 * 
 * 
 * 
 * **************************************************/
public  class FO_ForecastListControllerApex {
    @AuraEnabled
    /************************************************
     * Author: Javier Lopez Andradas
     * Company:gCTIO
     * Description: Retrieve all the nodes that the user have access
     * 
     * ********************************************/
    public static List<BI_FVI_Nodos__c> getNodos(){
        return [Select Id,Name from BI_FVI_Nodos__c];
    }
    
    @AuraEnabled
    /************************************************
     * Author: Javier Lopez Andradas
     * Company:gCTIO
     * Description: Retrieve all the Periodes related to a Node
     * 
     * ********************************************/
    public static List<BI_Periodo__c> getPeriodos(String IdNodo){
        return[Select Id,FO_Periodo_Padre__c,BI_FVI_Fecha_Fin__c,BI_FVI_Fecha_Inicio__c,Name,FO_Type__c,BI_FVI_Activo__c,FO_Secuencia__c from BI_Periodo__c where Id in (select FO_Periodo__c from FO_Forecast__c where FO_Nodo__c =:idNodo ) ORDER by BI_FVI_Fecha_Inicio__c];
    }
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Retrieves allthe periods Of the weekly forecast
     * 
     *IN: Id of the chosen Period
     * OUT: List with all the Periods that matches with this forecast
     * 
     * 
     * ***************************************************/
    @AuraEnabled
    public static List<BI_Periodo__c> getPeriodosForecast(String idPeriodo){
        Set<BI_Periodo__c> result = new Set<BI_Periodo__C>();
        Map<ID,BI_Periodo__c> lst_per =new Map<Id,BI_periodo__c>( [Select ID,FO_Periodo_Padre__c,BI_FVI_Fecha_Fin__c,BI_FVI_Fecha_Inicio__c,Name,FO_Type__c,BI_FVI_Activo__c,FO_Secuencia__c from BI_Periodo__c where FO_Type__c!=null]);
        Map<String,List<String>> mp_perPa = new Map<String,List<String>>();
        for(BI_periodo__C per:lst_per.values()){
            if(per.FO_Periodo_Padre__c!=null){
                list<String> lst = mp_perPa.get(per.FO_Periodo_Padre__c);
                if(lst==null)
                    lst=new List<String>();
                lst.add(per.Id);    
                mp_perPa.put(per.FO_periodo_PAdre__c,lst);
            }
        }
        BI_PEriodo__c per = lst_per.get(idPeriodo);
        while(per!=null){
            result.add(per);
            if(per.FO_Periodo_Padre__c!=null){
                for(String perH : mp_perPa.get(per.FO_Periodo_Padre__c)){
                    result.add(lst_per.get(perH));
                }
                
                
            }
            per=lst_per.get(per.FO_Periodo_Padre__c);
        }        
        return new List<BI_Periodo__c>(result);
    }
    
    @AuraEnabled
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Retrieves the Forecast related with a PEriod and a node
     * 
     *IN: Id of the chosen Period,an the Id of the choseb Node
     * OUT: List with all Forecast[ Always must be one]
     * 
     * 
     * ***************************************************/
    public static List<FO_Forecast__c> getForecasts(String periodo,String nodo ){
        System.debug('Per'+periodo+'//nodo'+nodo);
        return [Select Id, RecordType.DeveloperName, FO_Periodo_Fecha_Inicio__c,FO_Total_Forecast__c,FO_Forecast_Padre__c,FO_Periodo__c,FO_Commit__c,FO_Upside__c,FO_Commit_Pipeline__c,FO_Total_Pipeline__c,FO_Upside_Pipeline__c, CurrencyIsoCode from FO_Forecast__c where FO_Nodo__c =: nodo AND FO_Periodo__c =:periodo];
        
        
    }
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Retrieves all adjustment Forecasts related with a single Forecast
     * 
     *IN: Id of the Forecast
     * OUT: List with all the asjustment forecasts
     * 
     * 
     * ***************************************************/
    @AuraEnabled
    public static List<FO_Forecast_Futuro__c> getAjustes(String IdFore){
        return[ Select Id,FO_PEriodo__c,FO_Forecast_Padre__c,   FO_Estado_Forecast__c,  FO_Total_Forecast_Ajustado__c,FO_Ajuste_Forecast_Manager__c,FO_Forecast_Final__c from FO_Forecast_Futuro__c where FO_Forecast_PAdre__c = :idFore];
    }
    
    @AuraEnabled
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Retrieves Opp related with a forecast
     * 
     *IN: Inital period,End of period, Id of the node related to the forecast
     * OUT: List with allthe Opps
     * 
     * <date>		<version>		<description>
     * 13/04/2018	1.1				Added Currency Tansformation
     * 11/05/2018	1.2				Added field extraction to de Opps Object: BI_Fecha_de_cierre_real__c
     * ***************************************************/
    //Para sacar las oportunidades que se van a cerrar, o estan cerradas en el año que engloba todo 
    public static List<OppsAux> getOpps (String fechIni,String fechFin,String idNodo,String idFore){
        System.debug('Hemos entrado en Opp vamos a ver si parsea correctamente las fechas');
        System.debug('FechaIni: '+fechIni+' fecha fin: '+fechFin);
        Date dtt_Ini = convertDate(fechIni);
        Date dtt_Fin = convertDate(fechFin);
        System.debug('Ha parseado');
        Schema.DescribeFieldResult stageNameField = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
        List<String> val_def = new List<String>();
        //AHora vamos a sacar la estructuraa de los nodos para ver los clientes a los que tiene acceso un nodo N cualquiera
        //AL ser de vista privada deberia solo ver los que estan en jerarquia, ahora hay que sacar los que seha de esta misma jerarquia
        Map<Id,BI_FVI_Nodos__c> mp_nodos = new Map<Id,BI_FVI_Nodos__c>([Select Id,BI_FVI_NodoPadre__c from BI_FVI_Nodos__c]);
        List<String> lst_idNodos = new List<String>();
        //{Id_Nodo,[Nodos Hijos]}
        Map<String,List<String>> mp_parent = new Map<String,List<String>>();
        for(BI_FVI_Nodos__c nd : mp_nodos.values()){
            if(nd.BI_FVI_NodoPadre__c!=null){
                List<String> lst = mp_parent.get(nd.BI_FVI_NodoPadre__c);
                if(lst==null)
                    lst=new List<String>();
                lst.add(nd.Id);
                mp_parent.put(nd.BI_FVI_NodoPadre__c,lst);
            }
        }
        List<CurrencyType> lst_cr = [Select IsoCode,ConversionRate from CurrencyType where isActive=true];
     	Map<String,double> mp_curr = new Map<String,double>();
     	//JLA_2018/04/13 
	    for(CurrencyType curr:lst_cr)
    	    mp_curr.put(curr.IsoCode,curr.ConversionRate);
        
        List<String> lst_node1 = mp_parent.get(IdNodo);
        if(lst_node1==null){
            lst_idNodos.add(idNodo);
        }else{
            lst_idNodos.addAll(estrucPath(lst_node1,mp_parent));
        }
        List<BI_FVI_Nodo_Cliente__c> lst_nodoCL = [Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c  where BI_FVI_IdNodo__c IN:lst_idNodos];
        List<String> lst_acc = new List<String>();
        for(BI_FVI_Nodo_Cliente__c ndc : lst_nodoCL)
            lst_acc.add(ndc.BI_FVI_IdCliente__C);
        for(Schema.PicklistEntry entry :values){
            if(entry.getValue().contains('F6')==true || entry.getValue().equals('F1 - Closed Lost')==true || entry.getValue().equals('F1 - Cancelled | Suspended')  )
                val_def.add(entry.getValue());
        }
        List<RecordType> lst_rt = [Select Id from RecordType where DeveloperName='BI_Ciclo_completo' AND SobjectType='Opportunity'];
        
        List<Opportunity> lst_opps=[Select BI_Net_annual_value_NAV__c,StageName,CloseDate,CurrencyIsoCode,BI_Fecha_de_cierre_real__c from Opportunity where CloseDate>=:dtt_Ini AND CloseDate<=:dtt_Fin AND  recordTypeId=:lst_rt[0].Id AND  StageName NOT IN :val_def AND AccountID in:lst_acc];
        String currFore = [Select CurrencyIsoCode From FO_Forecast__c where Id =: idFOre].CurrencyIsoCode;
        List<OppsAux> aux_opss=new List<OppsAux>();
        for(Opportunity opp :lst_opps){
            OppsAux aux = new OppsAux(opp.CloseDate,opp.StageName,opp.BI_Net_annual_value_NAV__c*mp_curr.get(currFore)/mp_curr.get(opp.CurrencyIsoCode),opp.BI_Fecha_de_cierre_real__c);
            //JLA_Formula_cannot be modidief opp.BI_Net_annual_value_NAV__c=opp.BI_Net_annual_value_NAV__c*mp_curr.get(currFore)/mp_curr.get(opp.CurrencyIsoCode);
            aux_opss.add(aux);
        }
        System.debug('JLA:Returned L:ist: '+ aux_opss);
        return aux_opss;
        
    }
    @AuraEnabled
  /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Retrieves all the objetives related to a forecast, Month quarter and annual
     * 
     *IN: Id of the Node, initial date,end date 
     * OUT: List with all the Periods that matches with this forecast
     * 
     * 
     * ***************************************************/
    public static List<BI_FVI_Nodos_Objetivos__c> getObjetivo(String nodo,String fechIni,String fechFin,List<String> periodos){
        Date dtt_Ini = convertDate(fechIni);
        Date dtt_Fin = convertDate(fechFin);
        
        return [Select BI_FVI_Objetivo__c,BI_FVI_IdPeriodo__c from BI_FVI_Nodos_Objetivos__c where BI_FVI_Id_Nodo__c=:nodo AND BI_FVI_Fecha_Inicio__c>=:dtt_Ini AND BI_FVI_Fecha_Fin__c<=:dtt_Fin AND BI_FVI_Objetivo_Comercial__r.BI_Tipo__c='Cartera'];
    } 
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Helper method that translate a String format(YYYY-MM-DD) to a Date
     * 
     *IN: the String of the date
     * OUT: The Date
     * 
     * 
     * ***************************************************/
    static Date convertDate(String fech){
        String[] split = fech.split('-');
        return Date.newInstance(Integer.valueOf(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]));
    }
    @AuraEnabled
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Retrieve the StageName of the picklist Opportunity.stageName
     * 
     * IN: 
     * OUT:List with the values
     * 
     * 
     * ***************************************************/
   public static Map<String,String> getOppStageName(){
        Schema.DescribeFieldResult stageNameField = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
        Map<String,String> val_def = new Map<String,String>();
        for(Schema.PicklistEntry entry :values){
            if(entry.getValue().contains('F6')==false && entry.getValue().contains('Closed Lost')==false && entry.getValue().contains('Cancelled | Suspended')==false && entry.getValue().contains('Won')==false && entry.getValue().equals('Open')==false){
                val_def.put(entry.getValue(),entry.getLabel());
            }
        }
        return val_def;
    }
    /**************************************************
     * Author Javier Lopez Andradas
     * Company .gCTIO
     * Description: Helper methos that retrieves all the children Nodes of a single Node
     * 
     * IN: List of nodes to eval,map of {Id node,{List children ndoes}}
     * OUT: List with all the Periods that matches with this forecast
     * 
     * 
     * ***************************************************/
    public static List<String> estrucPath(List<String> idsNodes,Map<String,List<String>> mp_parent){
        List<String> lst_result = new List<String>();
        for(String nd : idsNodes){
            List<String> lst  = mp_parent.get(nd);
            if(lst==null){
                lst_result.add(nd);
            }else{
                lst_result.addAll(estrucPath(lst,mp_parent));
            }
        }
        return lst_result;
    }
    @AuraEnabled
    public static FO_Forecast__c getForeInfo(String idFore){
        
        return [select FO_Periodo__c,FO_Nodo__c from FO_Forecast__c where Id=:idFore];
    }
    /**********************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description: Auxiliar class to carry all the Opp data to the page
     * 
     * <date>		<version>			<description>
     * ??????		1.0					Initial
     * 11/05/2018	1.1					Added field BI_Fecha_de_cierre_real__c			
     * 
     * ********************************************/
    public class OppsAux{
        @AuraEnabled public Date CloseDate;
        @AuraEnabled public String StageName;
        @AuraEnabled public Decimal BI_Net_annual_value_NAV;
        @AuraEnabled public Date BI_Fecha_de_cierre_real;
        OppsAux(Date CloseDate,String StageName,Decimal BI_Net_annual_value_NAV,Date BI_Fecha_de_cierre_real){
            this.CloseDate=CloseDate;
            this.StageName=StageName;
            this.BI_Net_annual_value_NAV=BI_Net_annual_value_NAV;
            this.BI_Fecha_de_cierre_real=BI_Fecha_de_cierre_real;
            
        }
            
    }
}