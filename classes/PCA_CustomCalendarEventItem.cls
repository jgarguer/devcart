public with sharing class PCA_CustomCalendarEventItem {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Class to manage formatted date of custom events 
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public eventWrapper event;
    public String formatedDate; 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Class to format date of an event
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_CustomCalendarEventItem(eventWrapper eWrapper) { 
            event= eWrapper;
            formatedDate = eWrapper.startTime.format('h:mm a') +' - ' + eWrapper.finishTime.format('h:mm a');
        
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Retrieves the event
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public eventWrapper getEv(){
        try{ 
            if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
            }  
            return event;
        }
        catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CustomCalendarEventItem.eventWrapper', 'Portal Platino', Exc, 'Class');return null;
        } 
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Retrieves formatted date of the event
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String getFormatedDate() {
        try{ 
            if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
            }  
            return formatedDate;
        }
        catch (exception Exc)
        {
            BI_LogHelper.generate_BILog('PCA_CustomCalendarEventItem.eventWrapper', 'Portal Platino', Exc, 'Class');return null;
        } 
    }
}