/*-------------------------------------------------------------------------------------------------------------------------------------------------------


Company:       everis

Description:   Auxiliar class for the manage attachments  for the new incident modal page. 
History:
<Date>                          <Author>                                <Code>              <Change Description>
23/02/2017                      Everis                                  Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class CWP_AttachmentComp_Controller {
    
    //Attachments
    
    //public Static Attachment newAttachment {get;set;}
    
    public Static String varPruebaCMP {get; set;}
    //public List<String> listCaseAttachmentsName {get; set;}
    
    //public static transient Map<String,List<String>> cachedStringsMap;
    public static transient List<String> cachedStringsList;
    public Static transient List<Attachment> listAttachmentsCMP {get;set;}    
   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------


Company:       everis

Description:  Method for store attahcments in salesforce Cache. 
History:
<Date>                          <Author>                                <Code>              <Change Description>
23/02/2017                      Everis                                  Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @RemoteAction
    public Static pagereference CWP_createAttachments (String pattachmentName1, String pattachmentDesc1, String pmyAttachedBody){        
        Cache.Session.put('local.CWPexcelExport.Origin', 'CWP');
        System.debug('CWP_createAttachments METHOD varPruebaCMP '+ varPruebaCMP);
        System.debug('CWP_createAttachments PARAMETERS: pattachmentName1 '+pattachmentName1+' pattachmentDesc1 '+pattachmentDesc1+' pmyAttachedBody '+ pmyAttachedBody+' attachmentDesc1 ');
        system.debug('&&&: ');
        return null;
    }
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------


Company:       everis

Description:  Method for store save attachments 
History:
<Date>                          <Author>                                <Code>              <Change Description>
23/02/2017                      Everis                                  Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/   
    @RemoteAction
    public Static void CWP_createAttach (String parentId_att, String contentType_att, String name_att, String body_att1, String body_att2){        
        System.debug('CWP_createAttach METHOD recibe: parentId_att'+ parentId_att);
        System.debug('CWP_createAttach METHOD recibe: contentType_att'+ contentType_att);
        System.debug('CWP_createAttach METHOD recibe: name_att'+ name_att);
        System.debug('CWP_createAttach METHOD recibe: body_att1'+ body_att1);
        System.debug('CWP_createAttach METHOD recibe: body_att2'+ body_att2);
        String body_att= body_att1+body_att2;
        Blob body_attDec=EncodingUtil.base64Decode(body_att);
        Attachment adjunto=new Attachment();
        adjunto.ParentId=parentId_att;
        adjunto.ContentType=contentType_att;
        adjunto.Name=name_att;
        adjunto.Body=body_attDec;
        System.debug('Voy a insertar '+ adjunto);
        insert adjunto;
    }
    /*
     @RemoteAction
    public Static void CWP_createAttachPart (String fileId, String parentId_att, String contentType_att, String name_att, String body_att){        
        System.debug('CWP_createAttachPart METHOD recibe: fileId'+ fileId);
        System.debug('CWP_createAttachPart METHOD recibe: parentId_att'+ parentId_att);
        System.debug('CWP_createAttachPart METHOD recibe: contentType_att'+ contentType_att);
        System.debug('CWP_createAttachPart METHOD recibe: name_att'+ name_att);
        System.debug('CWP_createAttachPart METHOD recibe: body_att'+ body_att);

        Blob body_attDec=EncodingUtil.base64Decode(body_att);
        Attachment adjunto=new Attachment();
        adjunto.ParentId=parentId_att;
        adjunto.ContentType=contentType_att;
        adjunto.Name=name_att;
        adjunto.Body=body_attDec;
 
    }*/
}