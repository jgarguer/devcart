public class CWP_HomeController {

    @auraEnabled
    public static list<CWP_LHelper.imageHomeWrapper> getImageList(){
        list<CWP_LHelper.imageHomeWrapper> retList = new list<CWP_LHelper.imageHomeWrapper>();
        User currentUser = CWP_LHelper.getCurrentUser(null);
        system.debug('ususario recogido ' + currentUSer);
        system.debug('cuenta del usuario    ' + currentUSer.AccountId);
        Account currentAccount = CWP_LHelper.getCurrentAccount(currentUser.AccountId);
        string accSeg = currentAccount.BI_Segment__c;
        if(currentAccount.BI_Segment__c == null){
            accSeg = 'Empresas';
        }
        Set<Id> IdsClientes = new Set<Id>();
        List<BI_Cliente_PP__c> ClientPP = [Select Id, Name, BI_Activo__c, BI_Cliente__c, BI_Imagen_inicio_PP__c, BI_Country_2__c, BI_Segment_2__c FROM BI_Cliente_PP__c where BI_Cliente__c =: currentUser.AccountId AND BI_Activo__c = true AND (BI_Country_2__c != null OR BI_Segment_2__c != null)];
        system.debug('clientes pp   ' + clientPP);
        for(BI_Cliente_PP__c Client :ClientPP){
            IdsClientes.add(Client.BI_Imagen_inicio_PP__c);
        }
        transient List<BI_ImageHome__c> lImageHome = new List<BI_ImageHome__c>();
        if(currentUser.Profile.Name.startsWithIgnoreCase('TGS')){
        lImageHome = [SELECT 
                      BI_Fecha_Fin__c,
                      BI_Fecha_Inicio__c,
                      BI_Segment__c,
                      BI_Country__c,
                      BI_Url__c 
                          FROM BI_ImageHome__c WHERE 
                      ((((BI_Country__c =: currentAccount.BI_Country__c OR BI_Country__c = null) AND (BI_Segment__c = :accSeg OR BI_Segment__c= null) AND (BI_Country__c <> null OR BI_Segment__c <> null)) OR Id =:IdsClientes) AND RecordType.Name='Slider' AND BI_Activo_c__c = true) AND CWP_ManagedByTGS__c = true ORDER BY BI_Fecha_Inicio__c ASC];
        }else{
             lImageHome = [SELECT 
                      BI_Fecha_Fin__c,
                      BI_Fecha_Inicio__c,
                      BI_Segment__c,
                      BI_Country__c,
                      BI_Url__c 
                          FROM BI_ImageHome__c WHERE 
                      ((((BI_Country__c =: currentAccount.BI_Country__c OR BI_Country__c = null) AND (BI_Segment__c = :accSeg OR BI_Segment__c= null) AND (BI_Country__c <> null OR BI_Segment__c <> null)) OR Id =:IdsClientes) AND RecordType.Name='Slider' AND BI_Activo_c__c = true) ORDER BY BI_Fecha_Inicio__c ASC];
        }
            system.debug('lista de imagenes de home ' + lImageHome);
        if(lImageHome.size() > 0){
            transient List<Id> idImageHome = new List<Id>();
            for (BI_ImageHome__c el : lImageHome){
                idImageHome.add(el.Id);
            }
            transient List<Attachment> lAttachments = [SELECT Id, parentId FROM Attachment WHERE parentId IN :idImageHome];
            system.debug('lista de adjuntos asociados   ' + lImageHome);
            transient Map<Id, List<String>> MAttachments = new Map<Id, List<String>>();
            for (Attachment item : lAttachments){
                transient List<String> lAux = new List<String>();
                
                if (MAttachments.containsKey(item.parentId)){
                    lAux = MAttachments.get(item.parentId);
                }
                
                lAux.add(item.Id);
                MAttachments.put(item.parentId, lAux);
            }
            
            for (BI_ImageHome__c bImage : lImageHome){
                if (MAttachments.containsKey(bImage.Id)){
                    if ((bImage.BI_Fecha_Inicio__c <= System.today())   &&
                    ((bImage.BI_Fecha_Fin__c == null) || (bImage.BI_Fecha_Fin__c >= System.today()))){
                        retList.add(new CWP_LHelper.ImageHomeWrapper(bImage, MAttachments.get(bImage.Id), '/servlet/servlet.FileDownload?file='));
                    }
                }
            }
            system.debug('lista que va de vuelta    ' + retList);
        }
        return retList;
    }
    
    @AuraEnabled
    public static Boolean getShowServTandMyS(){
        system.debug('First');
        List<PermissionSetAssignment> permissionsList = [select Assignee.Name, PermissionSet.Name from PermissionSetAssignment where Assignee.Id =: UserInfo.getUserId() order by Assignee.Name, PermissionSet.Name ];
        system.debug('Show buttons Permissions List '+permissionsList.size());
        if(permissionsList.size()>0){
            for (Integer a = 0; a < permissionsList.size(); a++){
            	system.debug('names 2'+permissionsList[a].PermissionSet.Name);
            	if (permissionsList[a].PermissionSet.Name == 'BI_CORE_Jefe_de_Ventas' || permissionsList[a].PermissionSet.Name == 'BI_CORE_Ejecutivo_de_Cliente' || permissionsList[a].PermissionSet.Name == 'ADM_BI_SuperUsuarios'){               	
                    system.debug('Show buttons false');
                    return false;
            	} 
        	}
        }        
        return true;
    }
    
    @AuraEnabled
    public static Boolean redirectToChatter(){
        List<PermissionSetAssignment> permissionsList = [select Assignee.Name, PermissionSet.Name from PermissionSetAssignment where Assignee.Id =: UserInfo.getUserId() order by Assignee.Name, PermissionSet.Name ];
        system.debug('Redirect Permissions List '+permissionsList.size());
        if(permissionsList.size()>0){
            for (Integer a = 0; a < permissionsList.size(); a++){
            	system.debug('names '+permissionsList[a].PermissionSet.Name);
            	if (permissionsList[a].PermissionSet.Name == 'BI_CORE_Jefe_de_Ventas' || permissionsList[a].PermissionSet.Name == 'BI_CORE_Ejecutivo_de_Cliente' || permissionsList[a].PermissionSet.Name == 'ADM_BI_SuperUsuarios'){
                	return true;
            	} 
        	}
        }
        
        return false;
    }
    
    @auraEnabled
    public static User getUserInfo(){
        User usuarioPortal = [Select id, FullPhotoUrl, name,Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c, ContactId, MobilePhone, Usuario_BO__c, pass_BO__c,Profile.Name  from User where Id = : userInfo.getUserId()];
        return usuarioPortal;
    }
    
    @auraEnabled
    public static map<string,string> getPermissionSet(){
    	map<string,string> mapVisibility = CWP_ProfileHelper.getPermissionSet();
    	return mapVisibility;
    }
}