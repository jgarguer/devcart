public without sharing class CWP_ServiceTrackingController {
  @AuraEnabled
    public static map<string, boolean> getButtonVisibility(){
        system.debug('llamada a la funcion que saca los permisos por cuenta');
        
        User currentUser = CWP_LHelper.getCurrentUser(null);
        
        system.debug('id del contacto  ' + currentUser.ContactId);
        list<BI_Contact_Customer_Portal__c> contactPermission = [Select
                                                                 id,
                                                                 BI_Perfil__c, 
                                                                 BI_Cliente__c, 
                                                                 BI_Permisos_PP__c,  
                                                                 BI_Permisos_PP__r.BI_Monitoreo__c,  
                                                                 BI_Permisos_PP__r.BI_Reportes__c,  
                                                                 BI_Permisos_PP__r.BI_Reclamos_y_pedidos__c,
                                                                 BI_Permisos_PP__r.BI_Documentacion__c,  
                                                                 BI_Permisos_PP__r.BI_Portales__c,  
                                                                 BI_Contacto__c, 
                                                                 BI_User__c, 
                                                                 BI_Activo__c from BI_Contact_Customer_Portal__c where BI_User__c = : currentUser.Id and BI_Cliente__c = :currentUser.Contact.BI_Cuenta_activa_en_portal__c limit 1];
        system.debug('permisos del contacto  ' + contactPermission);
        map<string, boolean> retMap = new map<string, boolean>{
            'showReports' => false,
            'showMonitoreo' => false,
            'showPedido' => false,
            'showPortales' => true
        };
        if(!contactPermission.isEmpty()){
          retMap = new map<string, boolean>{
                'showReports' => contactPermission[0].BI_Permisos_PP__r.BI_Reportes__c,
                'showMonitoreo' => contactPermission[0].BI_Permisos_PP__r.BI_Monitoreo__c,
                'showPedido' => contactPermission[0].BI_Permisos_PP__r.BI_Reclamos_y_pedidos__c,
                'showPortales' => contactPermission[0].BI_Permisos_PP__r.BI_Portales__c
            };
        } 
        system.debug('mapa de vuelta  ' + retMap);
        return retMap;
    }
    
    @AuraEnabled
    public static map<string, string> getMainImage(){
        User currentUser = CWP_LHelper.getCurrentUser(null);
        map<string, string> retMap = new map<string, string>{
            'title' => label.PCA_Tab_Service_Track_menu,
            'informationMenu' => label.PCA_ServiceTrackingText,
            'imgId' => null
        };
        Account currentAccount = CWP_LHelper.getCurrentAccount(currentUser.Contact.BI_Cuenta_activa_en_portal__c);
        system.debug('llamada a la funcion que recoge la imagen del fondo  ' + currentUser);
        map<string, string> rcvMap = CWP_LHelper.getPortalInfo('Service Tracking', currentUser, currentAccount);
        if(!rcvMap.isEmpty()){
            retMap.clear();
            retMap.putAll(rcvMap);
        }
        
        system.debug('mapa de datos que van de vuelta' + retMap);
        return retMap;
    }
    
    @AuraEnabled
    public static Boolean getUser(){
        Boolean esTGS=false; 
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            esTGS= pName.startsWith('TGS');
        }
        return esTGS;
    }
    
    @auraEnabled
    public static map<string,string> getPermissionSet(){
        map<string,string> mapVisibility = CWP_ProfileHelper.getPermissionSet();
        return mapVisibility;
    }
}