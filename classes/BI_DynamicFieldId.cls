global without sharing class BI_DynamicFieldId {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Obtain The Id for the field
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Diego Arenas		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   method to obtain the retrieve API field and Salesforce ID dynamically
    
    IN:            string objectName, set<string> setFieldToId
    OUT:           map<string,string> with a Apifield and Salesforce Id
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Diego Arenas		Initial version
    23/10/2014        Micah Burgos      Now use BI_DynamicFields__c custom Setting
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	global static map<string,string> getDynamicField (string objectName, set<string> setFieldToId){
		try{
			map<String, String> mapApiToId  = getMap_apiName_Id_CS(objectName, 0);

			boolean notFound = false;
			for(String fieldRequested :setFieldToId){
				if(!mapApiToId.containsKey(fieldRequested)){
					System.debug('****BI_DynamicFieldId.getDynamicField->NOT FOUND\'' + fieldRequested +'\' IN BI_DynamicFields__c');
					notFound = true;
					break;
				}
			}

			if(notFound){
				map<string,string> mapIdName = getMapLabelName(objectName);
				map<String, String> maplabelToId = getMapLabelId(objectName);
				map<String, String> mapApiToId_HTML = getMapApiId(mapIdName, maplabelToId, setFieldToId);

				String keyPrefix = Schema.getGlobalDescribe().get(ObjectName).getDescribe().getKeyPrefix();
				list<BI_DynamicFields__c> lst_dynamic = new list<BI_DynamicFields__c>();

				for(String NameAPI :mapApiToId_HTML.keySet()){
					System.debug('****BI_DynamicFieldId.getDynamicField->Dynamics->NameAPI: ' + NameAPI);
					System.debug('****BI_DynamicFieldId.getDynamicField->mapApiToId.containsKey(NameAPI): ' + mapApiToId.containsKey(NameAPI));

					if(!mapApiToId.containsKey(NameAPI)){

						if(NameAPI.length() <= 34){

							BI_DynamicFields__c dynamicVar = new BI_DynamicFields__c(
								Name = keyPrefix+'_'+NameAPI,
								BI_NombreAPI__c = NameAPI,
		        				BI_IdCampo__c = removeCF(mapApiToId_HTML.get(NameAPI)), //Remove prefix 'CF'
		        				BI_IdCampo_cf__c = mapApiToId_HTML.get(NameAPI),
		        				BI_sObject__c = objectName,
		        				BI_KeyPrefix__c = keyPrefix
							);

							lst_dynamic.add(dynamicVar);
						}
						
						mapApiToId.put(NameAPI, mapApiToId_HTML.get(NameAPI));
					}

				} 
				System.debug('****BI_DynamicFieldId.getDynamicField->Dynamics to insert: ' + lst_dynamic);
				insert lst_dynamic; 
			}

			return mapApiToId; //This map contains IDs with prefix 'CF'

		}catch(Exception Exc){
			BI_LogHelper.generate_BILog('BI_Buttons.getDynamicField', 'BI_EN', Exc, 'Trigger');
        	return  getMap_apiName_Id_CS(objectName, 0); //This map only contains saved records
		}

 
		
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   That method returns a map<NameAPI,Id whiout prefix 'CF'> 

    History: 
    
     <Date>                     <Author>                <Change Description>
    23/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	global static map<string,string> getDynamicFieldwithoutCF (string objectName, set<string> setFieldToId){
		try{

			map<String, String> mapApiToId  = getMap_apiName_Id_CS(objectName, 1);

			boolean notFound = false;
			for(String fieldRequested :setFieldToId){
				if(!mapApiToId.containsKey(fieldRequested)){
					System.debug('****BI_DynamicFieldId.getDynamicFieldwithoutCF->NOT FOUND\'' + fieldRequested +'\' IN BI_DynamicFields__c');
					notFound = true;
					break;
				}
			}

			if(notFound){
				

				map<string,string> mapIdName = getMapLabelName(objectName);
				map<String, String> maplabelToId = getMapLabelId(objectName);
				map<String, String> mapApiToId_HTML = getMapApiId(mapIdName, maplabelToId, setFieldToId);

				String keyPrefix = Schema.getGlobalDescribe().get(ObjectName).getDescribe().getKeyPrefix();
				list<BI_DynamicFields__c> lst_dynamic = new list<BI_DynamicFields__c>();

				for(String NameAPI :mapApiToId_HTML.keySet()){
					System.debug('****BI_DynamicFieldId.getDynamicFieldwithoutCF->Dynamics->NameAPI: ' + NameAPI);
					System.debug('****BI_DynamicFieldId.getDynamicFieldwithoutCF->mapApiToId.containsKey(NameAPI): ' + mapApiToId.containsKey(NameAPI));

					if(!mapApiToId.containsKey(NameAPI)){

						if(NameAPI.length() <= 34){

							BI_DynamicFields__c dynamicVar = new BI_DynamicFields__c(
								Name = keyPrefix+'_'+NameAPI,
								BI_NombreAPI__c = NameAPI,
		        				BI_IdCampo__c = removeCF(mapApiToId_HTML.get(NameAPI)), 
		        				BI_IdCampo_cf__c = mapApiToId_HTML.get(NameAPI),
		        				BI_sObject__c = objectName,
		        				BI_KeyPrefix__c = keyPrefix 
							); 

							lst_dynamic.add(dynamicVar); 
						}
						
						mapApiToId.put(NameAPI, removeCF(mapApiToId_HTML.get(NameAPI))); 
					}
				} 

				System.debug('****BI_DynamicFieldId.getDynamicFieldwithoutCF->Dynamics to insert: ' + lst_dynamic);
				insert lst_dynamic; 
			}

	 
			return mapApiToId; //This map contains IDs without prefix 'CF'

		}catch(Exception Exc){
			BI_LogHelper.generate_BILog('BI_Buttons.getDynamicField', 'BI_EN', Exc, 'Trigger');
        	return  getMap_apiName_Id_CS(objectName, 1); //This map only contains saved records
		}
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   method to obtain the retrieve API field and Salesforce ID dynamically
    
    IN:            map<String, String> mapIdName, map<String, String> maplabelToId, set<string> setFieldToId
    OUT:           map<string,string> with a Apifield and Salesforce Id
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Diego Arenas		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static map<string,string> getMapApiId (map<String, String> mapIdName, map<String, String> maplabelToId, set<string> setFieldToId){
		System.debug('****BI_DynamicFieldId.getMapApiId->mapIdName: '+mapIdName);
		System.debug('****BI_DynamicFieldId.getMapApiId->maplabelToId: '+maplabelToId);
		System.debug('****BI_DynamicFieldId.getMapApiId->setFieldToId: '+setFieldToId);
		
		map<string,string> mapApiToId = new map<string,string>();
		for(string item : mapIdName.keySet()){
			System.debug('****BI_DynamicFieldId.getMapApiId->item: '+item);
			string nameField = mapIdName.get(item);
			System.debug('****BI_DynamicFieldId.getMapApiId->nameField: '+nameField);
			if(nameField != null){
				string IdField = maplabelToId.get(nameField);
				System.debug('****BI_DynamicFieldId.getMapApiId->IdField: '+IdField); 
				if(IdField != null){
					mapApiToId.put(item, IdField); 
				}
			} 
		}
		System.debug('****BI_DynamicFieldId.getMapApiId->RETURN: mapApiToId: '+mapApiToId);
		return mapApiToId;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   method to obtain the label and the Id of fields
    
    IN:            string objectName, set<string> setFieldToId, integer mode : 1 return map without prefix 'CF' on ID Fields
    OUT:           map<string,string> with a label and Id fields
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Diego Arenas		Initial version
    12/08/2014		  Micah Burgos		Add if(Test.isRunningTest()) in line 88

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static map<string,string> getMapLabelId (string objectName){
		PageReference p = new PageReference('/' + Schema.getGlobalDescribe().get(objectName).getDescribe().getKeyPrefix() + '/e?nooverride=1');
		String html = '';
		if(Test.isRunningTest()){ 
        	html = '<label for="CF00N11000000gvSV">País</label><label for="CF00N11000000gvSV">Country</label><label for="CF00N11000000qTg5">Campaña</label>';
		}else{
			html = p.getContent().toString();
		}
		System.debug('****BI_DynamicFieldId.getMapLabelId->HTML: '+html);
		
        map<String, String> labelToId = new map<String, String>(); //gets label of field and the field id from the html of the selected request

        Matcher m = Pattern.compile('<label for="(\\w+?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
        while (m.find()) { 
            String label = m.group(3);
            String id = m.group(1);
            	system.debug(label + ' ' + id);
            	labelToId.put(label, id);
            //if(id.startsWith('CF')){
            	
            //}
        }
        System.debug('****BI_DynamicFieldId.getMapLabelId->RETURN: labelToId: '+labelToId);
        return labelToId;
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   method to obtain the name and the label of fields
    
    IN:            string objectName, set<string> setFieldToId
    OUT:           map<string,string> with a name and label fields
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Diego Arenas		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static map<string,string> getMapLabelName (string objectName){
		map<string,string> mapApiLabel = new map<string,string>();
		Map<String, Schema.SObjectField> fldObjMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
		List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
		System.debug('****BI_DynamicFieldId.getMapLabelName->fldObjMapValues: '+fldObjMapValues);
		for(Schema.SObjectField s : fldObjMapValues){
			//if(setFieldToId.contains(s.getDescribe().getName())){ 
				String theLabel = s.getDescribe().getLabel();
				String theName = s.getDescribe().getName();
				String theType = string.valueOf(s.getDescribe().getType());
				system.debug('****BI_DynamicFieldId.getMapLabelName->'+theLabel + '**' + theName + '**' + theType);
				mapApiLabel.put(theName, theLabel); 
			//}
		}
		return mapApiLabel;
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that get API name and Id from the custom setting BI_DynamicFields__c. If mode equals 1, return without prefix 'CF'.

    History: 
    
     <Date>                     <Author>                <Change Description>
    14/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	global static map<string,string> getMap_apiName_Id_CS (string objectName, integer mode){
		//Query sobre BI_DynamicFields__c para ver sobre cual tenemos datos y sobre cual hay que calcular.
		String keyPrefix = Schema.getGlobalDescribe().get(ObjectName).getDescribe().getKeyPrefix();

		list<BI_DynamicFields__c> lst_dynamic = [SELECT BI_NombreAPI__c, BI_IdCampo__c, BI_IdCampo_cf__c FROM BI_DynamicFields__c WHERE BI_sObject__c = :objectName];
		
		map<String,String> map_apiName_Id_CS = new map<String,String>();
		if(mode == 1){	
			for(BI_DynamicFields__c item :lst_dynamic){
				map_apiName_Id_CS.put(item.BI_NombreAPI__c , item.BI_IdCampo__c);
			}
		}else{
			for(BI_DynamicFields__c item :lst_dynamic){
				map_apiName_Id_CS.put(item.BI_NombreAPI__c , item.BI_IdCampo_cf__c);
			}
		}

		return map_apiName_Id_CS;
	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that remove prefix 'CF' of string Id.	

    History: 
    
    <Date>                     <Author>                <Change Description>
    23/10/2014                 Micah Burgos            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	private static String removeCF(String IdwithCF){
		if(IdwithCF.startsWith('CF')){
			IdwithCF = IdwithCF.substring(2);
		}
		return IdwithCF;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture
    Description:   Method getMapLabelId overloaded to enable recordtype layouts

    History: 
    
    <Date>                     <Author>                <Change Description>
    27/06/2017                 Guillermo Muñoz            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static map<string,string> getMapLabelId (string objectName, Id rtId){
		PageReference p = new PageReference('/' + Schema.getGlobalDescribe().get(objectName).getDescribe().getKeyPrefix() + '/e?RecordType=' + rtId + '&nooverride=1');
		
		String html = '';
		if(Test.isRunningTest()){ 
        	html = '<label for="CF00N11000000gvSV">País</label><label for="CF00N11000000gvSV">Country</label><label for="CF00N11000000qTg5">Campaña</label>';
		}else{
			html = p.getContent().toString();
		}
		System.debug('****BI_DynamicFieldId.getMapLabelId->HTML: '+html);
		
        map<String, String> labelToId = new map<String, String>(); //gets label of field and the field id from the html of the selected request

        Matcher m = Pattern.compile('<label for="(\\w+?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
        while (m.find()) { 
            String label = m.group(3);
            String id = m.group(1);
            	system.debug(label + ' ' + id);
            	labelToId.put(label, id);
            //if(id.startsWith('CF')){
            	
            //}
        }
        System.debug('****BI_DynamicFieldId.getMapLabelId->RETURN: labelToId: '+labelToId);
        return labelToId;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture
    Description:   Method getMapLabelId overloaded to enable recordtype layouts

    History: 
    
    <Date>                     <Author>                <Change Description>
    27/06/2017                 Guillermo Muñoz            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static map<string,string> getDynamicField (string objectName, set<string> setFieldToId, Id rtId){
		try{
			map<String, String> mapApiToId  = getMap_apiName_Id_CS(objectName, 0);

			boolean notFound = false;
			for(String fieldRequested :setFieldToId){
				if(!mapApiToId.containsKey(fieldRequested)){
					System.debug('****BI_DynamicFieldId.getDynamicField->NOT FOUND\'' + fieldRequested +'\' IN BI_DynamicFields__c');
					notFound = true;
					break;
				}
			}

			if(notFound){
				map<string,string> mapIdName = getMapLabelName(objectName);
				map<String, String> maplabelToId = getMapLabelId(objectName, rtId);
				map<String, String> mapApiToId_HTML = getMapApiId(mapIdName, maplabelToId, setFieldToId);

				String keyPrefix = Schema.getGlobalDescribe().get(ObjectName).getDescribe().getKeyPrefix();
				list<BI_DynamicFields__c> lst_dynamic = new list<BI_DynamicFields__c>();

				for(String NameAPI :mapApiToId_HTML.keySet()){
					System.debug('****BI_DynamicFieldId.getDynamicField->Dynamics->NameAPI: ' + NameAPI);
					System.debug('****BI_DynamicFieldId.getDynamicField->mapApiToId.containsKey(NameAPI): ' + mapApiToId.containsKey(NameAPI));

					if(!mapApiToId.containsKey(NameAPI)){

						if(NameAPI.length() <= 34){

							BI_DynamicFields__c dynamicVar = new BI_DynamicFields__c(
								Name = keyPrefix+'_'+NameAPI,
								BI_NombreAPI__c = NameAPI,
		        				BI_IdCampo__c = removeCF(mapApiToId_HTML.get(NameAPI)), //Remove prefix 'CF'
		        				BI_IdCampo_cf__c = mapApiToId_HTML.get(NameAPI),
		        				BI_sObject__c = objectName,
		        				BI_KeyPrefix__c = keyPrefix
							);

							lst_dynamic.add(dynamicVar);
						}
						
						mapApiToId.put(NameAPI, mapApiToId_HTML.get(NameAPI));
					}

				} 
				System.debug('****BI_DynamicFieldId.getDynamicField->Dynamics to insert: ' + lst_dynamic);
				insert lst_dynamic; 
			}

			return mapApiToId; //This map contains IDs with prefix 'CF'

		}catch(Exception Exc){
			BI_LogHelper.generate_BILog('BI_Buttons.getDynamicField', 'BI_EN', Exc, 'Trigger');
        	return  getMap_apiName_Id_CS(objectName, 0); //This map only contains saved records
		}

 
		
	}
}