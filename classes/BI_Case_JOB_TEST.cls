@isTest
private class BI_Case_JOB_TEST {

	
	static testMethod void CaseJobTest2() {
        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		String NombreProceso = 'Case Job';
			
		String fecha = '0 0 3 * * ?';	
		  
		RecordType rType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];

		list<Case> lst_casesNew = new list<Case>();
		
		String pais = Label.BI_Argentina;

		List <Account> lst_acc = BI_DataLoad.loadAccounts( 1, new list<String>{pais} );
		/*
		Account acc = new Account(Name = 'test',
								  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
								  BI_Activo__c = Label.BI_Si,
								  BI_Segment__c = 'Empresas',
	                              BI_Subsegment_Local__c = 'Multinacionales',
	                              BI_Country__c = pais
	                              );
								
		insert acc;
		*/

		BI_Facturas__c fact1 = new BI_Facturas__c();
		fact1.BI_Nombre_del_cliente__c = lst_acc.get(0).Id;
		
		insert fact1;
        
		List <Opportunity> lst_opp = BI_DataLoad.loadOpportunitiesWithStringPais(lst_acc.get(0).Id, pais);

		for(Integer i=0; i<2; i++){

			Case cas = new Case();
			cas.BI_Cabecera_de_factura__c = fact1.Id;
			cas.Type = 'Caso Interno';
			cas.Subject = 'Caso test' + i;
			cas.BI_Otro_Tipo__c = 'test';
			cas.RecordTypeId=rType.Id;
			cas.BI_Nombre_de_la_Oportunidad__c = lst_opp[0].Id;
			cas.BI_Country__c = pais;
			cas.AccountId = lst_acc[0].Id;
			//cas.ClosedDate = datetime.now();
            
			lst_casesNew.add(cas);
		}
        
        Test.startTest();
		system.debug(LoggingLevel.ERROR, '------1');
        try {
            insert lst_casesNew;
        } catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_Case_JOB_TEST.CaseJobTest2', 'BI_EN', Exc, 'Trigger');
        }

        system.debug(LoggingLevel.ERROR, '------2');

		BI_Jobs.updateCases();

		BI_Jobs.updateCases(lst_casesNew[0].Id, false);

		Test.stopTest();

		List<CronTrigger> lst_cronTrigg = [SELECT CreatedById,CreatedDate,CronExpression,CronJobDetail.Name,EndTime,Id,LastModifiedById,NextFireTime,OwnerId,PreviousFireTime,StartTime,State,TimesTriggered,TimeZoneSidKey FROM CronTrigger WHERE CronJobDetail.Name LIKE '%Update Cases%'];
		System.debug('lst_cronTrigg: ' + lst_cronTrigg);

		System.assert(!lst_cronTrigg.isEmpty());

		Decimal res_decimal = BI_Case_JOB.getBusinessHoursDiff(lst_casesNew[0].CreatedDate, Datetime.now(), null, null);
		System.assertEquals(res_decimal, null);

		BusinessHours bh_default = [SELECT CreatedById,CreatedDate,FridayEndTime,FridayStartTime,Id,IsActive,IsDefault,LastModifiedById,LastModifiedDate,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,SystemModstamp,ThursdayEndTime,ThursdayStartTime,TimeZoneSidKey,TuesdayEndTime,TuesdayStartTime,WednesdayEndTime,WednesdayStartTime
									 FROM BusinessHours WHERE IsDefault = TRUE LIMIT 1];

		res_decimal = BI_Case_JOB.getBusinessHoursDiff(lst_casesNew[0].CreatedDate, Datetime.now(), 'minutes', bh_default);
		system.assertEquals(res_decimal, null);

		res_decimal = BI_Case_JOB.getBusinessHoursDiff(Datetime.now()-1, Datetime.now(), null, bh_default);
		System.assertNotEquals(res_decimal, null);

		res_decimal = BI_Case_JOB.getBusinessHoursDiff(Datetime.now()-1, Datetime.now(), 'minutes', bh_default);
		system.assertNotEquals(res_decimal, null);
		
		BI_Case_JOB.getBusinessHoursDiff(Datetime.now()-1, Datetime.now(), 'seconds', bh_default);
		system.assertNotEquals(res_decimal, null);

		BI_Case_JOB.getBusinessHoursDiff(Datetime.now()-1, Datetime.now(), 'days', bh_default);
		system.assertNotEquals(res_decimal, null);

		BI_Case_JOB.getBusinessHoursDiff(Datetime.now()-1, Datetime.now(), 'miliseconds', bh_default);
		system.assertNotEquals(res_decimal, null);
	}
			
		/*String Idtrabajo = System.schedule(NombreProceso, fecha, new BI_Case_JOB(null));

		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :Idtrabajo];

		System.assertEquals(fecha, ct.CronExpression);

		System.assertEquals(0, ct.TimesTriggered);

		Test.stopTest();
	}*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Salesforce.com
	Description:   Method that cover BI_Case_JOB.daysWorkedJOB()    

	History: 

	 <Date>                     <Author>                <Change Description>
	29/10/2014                  Micah Burgos            Initial Version
	28/05/2015					Micah BUrgos 			Deprecated
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//@isTest static void daysWorkedJOB_TEST() {

 //   	List <BusinessHours> lst_hours = [SELECT FridayEndTime,FridayStartTime,Id,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,
 //                                     SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,
 //                                     TuesdayStartTime,WednesdayEndTime,WednesdayStartTime FROM BusinessHours WHERE Name like 'Horario Oficina%' limit 1];

	//	Datetime fecha = datetime.now() - 10;

	//	Integer  hours = BI_Case_JOB.daysWorkedJOB(fecha, datetime.now(), 'hours', lst_hours);

	//	Integer  days = BI_Case_JOB.daysWorkedJOB(fecha, datetime.now(), 'days', lst_hours);

	//	//List<CronTrigger> lst_cronTrigg = [SELECT CreatedById,CreatedDate,CronExpression,CronJobDetail.Name,EndTime,Id,LastModifiedById,NextFireTime,OwnerId,PreviousFireTime,StartTime,State,TimesTriggered,TimeZoneSidKey FROM CronTrigger WHERE CronJobDetail.Name LIKE '%Update Cases%'];
	//	//System.debug('lst_cronTrigg: ' + lst_cronTrigg);

	//	//System.assert(!lst_cronTrigg.isEmpty());
	
	//}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}