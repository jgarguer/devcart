/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-03-24      Daniel ALexander Lopez (DL)     Cloned Class      
*************************************************************************************/


/*  Clase que contiene un booleano para manejar los metodos @future de las diferentes clases sobre el mismo objeto
  esta variable fue llamada dentro de las siguientes clases:
  - CrearClienteWS_cls
  - ValidarCorreoContactame_cls
  y sobre los siguientes triggers:
  - CrearClienteWS_tgr
  - crearUsuarioTRS
  - Callejero_dir_cls
*/

public with sharing class BI_COL_FlagforTGRS_cls {
	
		public static boolean flagTriggers = false;
		public static boolean flagTriggerTRSCuenta = true;//variable de uso para la clase CrearClienteWS_cls y SucursalesValidacion_tgr
    public static boolean flagTriggerTRSContacto = true;//variable de uso para la clase CrearClienteWS_cls y SucursalesValidacion_tgr
		public static boolean flagMsTriggers = false;
		
}