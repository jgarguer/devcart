public with sharing class ParqueInstaladoController {    
    /**
    * @author Jara Minguillón
    * @date ?
    * @description  This is the controller for the VisualForce page "ParqueInstalado",
    *               which shows a list of installed services for an account in its layout    *
    */
    public List<TableRecord> listaResultados {get; set;}
    public Set<Id> setIdServices {get; set;}    
    public map<Id, List<NE__Order_Item_Attribute__c>> mapServiceAttributes {get; set;}
    public String recordType; //{get ; set ;}
    
    public ParqueInstaladoController(ApexPages.StandardController controller) 
    {
        Account thisAccount = (Account)controller.getRecord();
        
    	List<RecordType> recType = [SELECT Id, DeveloperName FROM RecordType WHERE Id = :thisAccount.RecordTypeId];
        recordType = recType[0].DeveloperName;
        
        setIdServices = new Set<Id>();
        
        List<NE__OrderItem__c> listaInstalledServices = getInstalledServices(thisAccount);
        
        mapServiceAttributes = TGS_Portal_Utils.getAttributesByCI(setIdServices);
        
        listaResultados = new List<TableRecord>();
        
        for (NE__OrderItem__c su : listaInstalledServices) 
        {            
            TableRecord tr = new TableRecord(su.Id, su.Name, su.NE__ProdId__r.Name, su.TGS_Service_Status__c, su.NE__OrderId__c, 
                                             su.NE__OrderId__r.Name, su.TGS_RFS_date__c, 
                                             su.NE__Billing_Account_Asset_Item__r.Name, su.NE__Asset_Item_Account__r.Name, su.Installation_point__r.Name);

            tr.backStatus = su.NE__Status__c;
            
            if(mapServiceAttributes.containsKey(su.Id))
            {
                List<NE__Order_Item_Attribute__c> listAttributes = mapServiceAttributes.get(su.Id);
                
                if(listAttributes.size() > 0)
                    tr.keyAttribute1 = listAttributes[0].NE__Value__c;
                if(listAttributes.size() > 1)
                    tr.keyAttribute2 = listAttributes[1].NE__Value__c;
                if(listAttributes.size() > 2)
                    tr.keyAttribute3 = listAttributes[2].NE__Value__c;    
            }                            
            
            listaResultados.add(tr);
        }
        
    }
    
    
    public List<NE__OrderItem__c> getInstalledServices(Account acc) {
        List<Account> accounts = getAccountsBelowHierarchy(acc);
        
        /*List<NE__OrderItem__c> listaInstalledServices = [SELECT Id, Name, NE__ProdId__r.Name,
                                                                TGS_Service_status__c,
                                                                NE__Status__c,
                                                                NE__OrderId__c,
                                                                NE__OrderId__r.Name,
                                                                TGS_RFS_date__c,
                                                                TGS_Provisional_RFS_date__c,
                                                                NE__Billing_Account_Asset_Item__r.Name,
                                                                NE__Asset_Item_Account__r.Name,
                                                                Installation_point__r.Name
                                                         FROM NE__OrderItem__c
                                                         WHERE NE__OrderId__r.RecordType.Name = 'Asset'
                                                             AND NE__OrderId__r.NE__BillAccId__c IN :accounts
                                                             //AND TGS_Service_status__c IN ('Deployed','Disposed','Received')
                                                             AND TGS_Service_status__c IN ('Deployed','Received')
                                                             AND NE__Status__c <> 'Disconnected'
                                                             AND NE__Root_Order_Item__c = NULL
                                                             AND NE__CatalogItem__r.NE__Type__c = 'Product'];*/
        String qry = 'SELECT Id, Name, NE__ProdId__r.Name,'
                            +'TGS_Service_status__c,'
                            +'NE__Status__c,'
                            +'NE__OrderId__c,'
                            +'NE__OrderId__r.Name,'
                            +'TGS_RFS_date__c,'
                            +'TGS_Provisional_RFS_date__c,'
                            +'NE__Billing_Account_Asset_Item__r.Name,'
                            +'NE__Asset_Item_Account__r.Name,'
                            +'Installation_point__r.Name '
                        +'FROM NE__OrderItem__c '
                        +'WHERE NE__OrderId__r.RecordType.Name =  \'Asset\' ';
        
        if(!String.isBlank(recordType) && recordType == 'TGS_Legal_Entity') {
            // Todos los CIs asociados a esta LE en su campo Account o a cualquiera de las BUs que tenga por debajo en el campo Service Account
            accounts = [SELECT Id, Name FROM Account WHERE ParentId = :acc.Id];
            qry += 'AND (NE__OrderId__r.NE__AccountId__c = \'' + acc.Id + '\' OR NE__OrderId__r.NE__ServAccId__c IN :accounts) ';
        }
        if(!String.isBlank(recordType) && recordType == 'TGS_Business_Unit') {
            // Todos los CIs asociados a esta BU en su campo Service Account
            qry += 'AND NE__OrderId__r.NE__ServAccId__c IN :accounts ';
        }
        if(!String.isBlank(recordType) && recordType == 'TGS_Cost_Center') {
            // Todos los CIs asociados a este CC en su campo Billing Account
            qry += 'AND NE__OrderId__r.NE__BillAccId__c IN :accounts ';
        }
        
        qry += 'AND TGS_Service_status__c IN (\'Deployed\',\'Disposed\',\'Received\')'
            +'AND TGS_Service_status__c IN (\'Deployed\',\'Received\') '
            +'AND NE__Status__c <> \'Disconnected\' '
            +'AND NE__Root_Order_Item__c = NULL '
            +'AND NE__CatalogItem__r.NE__Type__c = \'Product\'';
        List<NE__OrderItem__c> listaInstalledServices = (List<NE__OrderItem__c>)Database.query(qry);
        
        for(NE__OrderItem__c installedService : listaInstalledServices)
        {
            setIdServices.add(installedService.Id);                 
        }            
        
        return listaInstalledServices;
    }
    
    class TableRecord {
        public String identifier {get; set;}
        public String name {get; set;}
        public String productName {get; set;}
        public String status {get; set;}
        public String backStatus {get;set;}
        public String configurationId {get; set;}
        public String configurationName {get; set;}
        public String provisionalRFSDate {get; set;}
        public String RFSDate {get; set;}
        public String provisionalRFSDateSort {get; set;}
        public String RFSDateSort {get; set;}
        public String relatedCostCenter {get; set;}
        public String creationAccount {get; set;}
        public String site {get; set;}
        public String keyAttribute1 {get; set;}
        public String keyAttribute2 {get; set;}
        public String keyAttribute3 {get; set;}
        
        public TableRecord (String identifier, String name, String productName, String status, String configurationId, String configurationName, Date RFSDate,
                            String relatedCostCenter, String creationAccount, String site) 
        {
            this.identifier = identifier;
            this.name = name;
            this.productName = productName;
            this.status = status;
            this.configurationId = configurationId;
            this.configurationName = configurationName;
            this.relatedCostCenter = relatedCostCenter;
            this.creationAccount = creationAccount;
            this.site = site;
            
            if (RFSDate != null) 
            {
                DateTime dt = DateTime.newInstance(RFSDate.year(), RFSDate.Month(), RFSDate.day());
                this.RFSDate = dt.format('dd/MM/YYYY');
                RFSDateSort = dt.format('YYYYMMdd');
            }
        }

    }    
    
    public List<Account> getAccountsBelowHierarchy (Account acc) {
        List<Account> accountList = new List<Account>();
        Integer level = TGS_Portal_Utils.getAccountLevelForAccount(acc.Id);
        for (integer i = level; i <= 5; i++) {
            System.debug('### Obteniendo accounts de nivel ' + i);
            List<Account> levelAccounts = TGS_Portal_Utils.getLevelN(acc.Id, 1, i);
            //System.debug('### Accounts de nivel ' + i + ' obtenidas: ' + levelAccounts.size());
            accountList.addAll(levelAccounts);
        }
        return accountList;        
    }
    
    /*public integer getAccountLevelForAccount(Account AC){    
        System.debug('### account record type: ' + ac.recordType.DeveloperName);    
        
        if (AC.RecordType.DeveloperName == TGS_Portal_Utils.ACCOUNT_LEVEL_1_NAME){//El user está asociado a un HOLDING
            return TGS_Portal_Utils.ACCOUNT_LEVEL_1;            
        }else if (AC.RecordType.DeveloperName == TGS_Portal_Utils.ACCOUNT_LEVEL_2_NAME){//El user está asociado a una CUSTOMER COUNTRY
            return TGS_Portal_Utils.ACCOUNT_LEVEL_2;        
        }else if (AC.RecordType.DeveloperName == TGS_Portal_Utils.ACCOUNT_LEVEL_3_NAME){//El user está asociado a una LEGAL ENTITY
            return TGS_Portal_Utils.ACCOUNT_LEVEL_3;        
        }else if (AC.RecordType.DeveloperName == TGS_Portal_Utils.ACCOUNT_LEVEL_4_NAME){//El user está asociado a una BUSINESS UNIT
            return TGS_Portal_Utils.ACCOUNT_LEVEL_4;        
        }else if (AC.RecordType.DeveloperName == TGS_Portal_Utils.ACCOUNT_LEVEL_5_NAME){//El user está asociado a un COST CENTER
            return TGS_Portal_Utils.ACCOUNT_LEVEL_5;        
        }else{
            return 0;//GESTIÓN DE ERRORES/NO DEBERÍA PASAR           
        }
    }*/ 

}