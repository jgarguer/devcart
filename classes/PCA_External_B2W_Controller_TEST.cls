@isTest
public class PCA_External_B2W_Controller_TEST {
    
    static NE__Catalog_Category__c testCategory {get; set;}
    static NE__Catalog_Header__c testCatalogHeader {get; set;}
    static NE__Catalog__c testCatalog {get; set;}
    static NE__Order__c testOrder {get; set;}
    static Case testCase {get; set;}
    static NE__OrderItem__c testOrderItem {get; set;}
    static NE__Product__c testProduct {get; set;}
    static NE__Catalog_Item__c testCatalogItem {get; set;}
    static Account le {get; set;}
    static Contact co {get; set;}

    
    
    
    ////////////////Método bueno de verdad    
    enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    
    static testmethod void usertest() {
        User pu = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.assert([select isPortalEnabled 
                         from user 
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');       
        
        System.RunAs(pu) {
            System.assert([select isPortalEnabled 
                             from user 
                            where id = :UserInfo.getUserId()].isPortalEnabled, 
                          'User wasnt portal enabled within the runas block. ');
        }
    }
    
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {
    
        /* Make sure the running user has a role otherwise an exception 
           will be thrown. */
        if(userWithRole == null) {   
            
            if(UserInfo.getUserRoleId() == null) {

                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com',
                                    BI_Permisos__c = 'Empresas Platino', isActive = True);
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            System.assert(userWithRole.userRoleId != null, 
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }

        Account a;
        Contact c;
        System.runAs(userWithRole) {

            a = TGS_Dummy_Test_data.dummyHierarchy();
            //Database.insert(a);
            
            c = new Contact(AccountId = a.id,
                            lastname = 'lastname',
                            Email = 'contactTest@telefonica.com',
                            BI_Activo__c = true
                            );
            Database.insert(c);

        }
        
        /* Get any profile for the given type.*/
        Profile p = [select id 
                      from profile 
                     where usertype = :portalType.name() 
                     and name = 'TGS Customer Community Plus'
                     limit 1];   
        
        String nRandom = String.valueOf((Integer)(Math.random()*100));
        String nRandom2 = String.valueOf((Integer)(Math.random()*100));
        String testemail = 'telefonicaTest'+nRandom+'@tefonica.com';
        User pu = new User(profileId = p.id, username = testemail+nRandom2, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = c.id, BI_Permisos__c = 'Empresas Platino',
                          isActive = True);
        
        if(doInsert) {
            Database.insert(pu);
        }
        co=c;
        le=a;
        return pu;
    }
//////////////////// 
    
    

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Jara Minguillon
  Company:       Deloitte
  Description:   Test class to manage the coverage code for PCA_Cases_Controller class 

  <Date>           <Author>         <Change Description>
  02/03/2015          Jara Minguillon        Initial Version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   static testMethod void loadInfoFromCIdTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*/
            createDummyCatalogItem();
            Test.stopTest();
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
        }
    }
    
    static testMethod void nextStepTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
       	User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*/
            createDummyCatalogItem();
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
            controller.userSelect = le.Id;
            controller.payerSelect = le.Id;
            controller.nextStep();
            Test.stopTest();
        }
    }
    
    static testMethod void nextStepFailTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
       	User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*/
            createDummyCatalogItem();
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
            controller.userSelect = le.Id;
            controller.payerSelect = le.Id;
           	controller.quantity = '0';
            System.assertEquals(null, controller.nextStep());
            controller.quantity = 'a';
            System.assertEquals(null, controller.nextStep());
            Test.stopTest();
        }
    }
    
    /*static testMethod void loadSiteTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
       	User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*x/
            createDummyCatalogItem();
            List<Account> listAcc = TGS_Portal_Utils.getLevel4(le.Id, 1);
            if(listAcc.size()>0){
                Id siteId = TGS_Dummy_Test_Data.dummyPuntoInstalacion(listAcc[0].Id).Id;
                ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
                PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
                controller.userSelect = listAcc[0].Id;
                controller.payerSelect = listAcc[0].Id;
                controller.loadSite();
            Test.stopTest();
            }
        }
    }*/
    
    static testMethod void cancelTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
       	User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*/
            createDummyCatalogItem();
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
            controller.cancel();
            Test.stopTest();
        }
    }
    
    /* static testMethod void getOrderTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
       	User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        createDummyCatalogItem();
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*P
            
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
            controller.getOrder();
            Test.stopTest();
        }
    } */
    
    
    static testMethod void loadPayerTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Communitiy') AND isActive = true LIMIT 1];
       	User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            /*
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact contact = [SELECT ID, Account.Id FROM Contact WHERE Id = :userTest.Contact.Id];
           	contact.Account = le;
            update contact;
			*/
            createDummyCatalogItem();
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            PCA_External_B2W_Controller controller = new PCA_External_B2W_Controller();
            controller.userSelect = le.Id;
            controller.loadPayer();
            Test.stopTest();
        }
    }
    
    private static void createDummyConfigurationUWIFI(Id accountId) {
        List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Asset'];
        if(listRecordTypes.size()>0){
            testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id);
            insert testOrder;
            testProduct = new NE__Product__c(Name='Universal WIFI');
            insert testProduct;
            testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__Qty__c=1, NE__Account__c = accountId, TGS_Service_Status__c = 'Deployed');            
            insert testOrderItem;
            //Attributes
            NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Domain', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id);
            insert attribute;
            attribute = new NE__Order_Item_Attribute__c(Name = 'Service Modality', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id);
            insert attribute;
        }      
    }
    
    private static void createDummyCatalogItem() {
        testProduct = new NE__Product__c(Name='Universal WIFI');
        insert testProduct;
        testCatalogHeader = new NE__Catalog_Header__c(NE__Name__c='Test Catalog Header');
        insert testCatalogHeader;
        testCatalog = new NE__Catalog__c(Name='Test Catalog', NE__Catalog_Header__c = testCatalogHeader.Id);
        insert testCatalog;
        testCategory = new NE__Catalog_Category__c(Name = 'Test Category', NE__CatalogId__c = testCatalog.Id);
        insert testCategory;
        testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCategory.Id, NE__ProductId__c = testProduct.Id, NE__Catalog_Id__c = testCatalog.Id);
        insert testCatalogItem;
    }
}