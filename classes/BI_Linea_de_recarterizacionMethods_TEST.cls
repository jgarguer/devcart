@isTest
private class BI_Linea_de_recarterizacionMethods_TEST {
	
	static{BI_TestUtils.throw_exception = false;}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that load all information that will be used in test
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testSetup static void setup(){
		List <Account> lst_acc = BI_DataLoad.loadAccountsWithCountry(2,new List<String>{Label.BI_Chile});
		System.assertEquals(lst_acc.size(),2);
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that manage the code coverage of BI_Linea_de_recarterizacionMethods
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             changeDescription
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void insertIdFromExternalId_VF_TEST() {

		BI_TestUtils.throw_exception = false;

		BI_Recarterizacion__c recart = new BI_Recarterizacion__c(
			Name = 'test1'
		);
		insert recart;
		
		Account [] lst_acc = [SELECT Id, BI_Validador_Fiscal__c,BI_Id_del_cliente__c FROM Account];
		System.assertEquals(lst_acc.size(),2);

		lst_acc[1].BI_Id_del_cliente__c = 'Test2';
		update lst_acc[1];

		List <BI_Linea_de_Recarterizacion__c> lst_lineas = new List <BI_Linea_de_Recarterizacion__c>();
		
		BI_Linea_de_Recarterizacion__c linea1 = new BI_Linea_de_Recarterizacion__c(
			BI_Nombre_API_identificador_clientes__c = 'BI_Validador_Fiscal__c',
			BI_Account_TEMP__c = lst_acc[0].BI_Validador_Fiscal__c,
			BI_Recarterizacion__c = recart.Id,
			BI_NewOwner__c = UserInfo.getUserId()
		);
		lst_lineas.add(linea1);

		BI_Linea_de_Recarterizacion__c linea2 = new BI_Linea_de_Recarterizacion__c(
			BI_Nombre_API_identificador_clientes__c = 'BI_Id_del_cliente__c',
			BI_Account_TEMP__c = lst_acc[1].BI_Id_del_cliente__c,
			BI_Recarterizacion__c = recart.Id,
			BI_NewOwner__c = UserInfo.getUserId()
		);
		lst_lineas.add(linea2);

		insert lst_lineas;

		BI_Linea_de_Recarterizacion__c [] lineas = [SELECT Id, BI_AccountId__c FROM BI_Linea_de_Recarterizacion__c];
		System.assertEquals(lineas.size(),2);

		System.assertEquals(lineas[0].BI_AccountId__c, lst_acc[0].Id);
		System.assertEquals(lineas[1].BI_AccountId__c, lst_acc[1].Id);
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that manage the code coverage of all exceptions thrown in BI_Linea_de_recarterizacionMethods
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             changeDescription
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void exceptions(){

		BI_TestUtils.throw_exception = true;
		BI_Linea_de_recarterizacionMethods.insertIdFromExternalId(null);

	}

}