@isTest
public with sharing class TGS_SLA_Assigner_Handler_Test {

    static testMethod void slaAssigner() {
        
        String profile = 'TGS Integration User';
        
        Profile miProfile = [SELECT Id, Name
        FROM Profile
        Where Name = :profile
        Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
            //[Micah Burgos]Comentado por error en subida de PL. Al tener el (SeeAllData=true) y el valor en el sistema salta duplicado
        }
        
        
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        u.BI_Permisos__c = 'TGS';
        insert u;
        
        
        
        System.runAs(u) {
                
            Contact contact = TGS_Dummy_Test_Data.dummyContactTGS('Contact Test');
            insert contact;
    
            Entitlement enti = TGS_Dummy_Test_Data.dummyEntitlementTGS('Entitlement Test', contact.AccountId);
            
            /*SlaProcess entitlementProcess = [Select Id
                                                From slaprocess
                                                where name = 'SLAs UWIFI'
                                                Limit 1];*/
            SlaProcess entitlementProcess = new SlaProcess();
            //insert entitlementProcess;
            
            enti.SlaProcessId = entitlementProcess.Id;
            
            insert enti;
            
            /*NE__Product__c commercialProduct = [Select Id
                                                From NE__Product__c
                                                Where Name = 'Universal WIFI'];*/
            
            NE__Product__c commercialProduct = TGS_Dummy_Test_Data.dummyCommercialProduct();
            
            TGS_SLA_Assigner__c customSla = new TGS_SLA_Assigner__c();
            customSla.TGS_Entitlement__c = enti.Id;
            customSla.TGS_Account__c = contact.AccountId;
            customSla.TGS_Commercial_Product__c = commercialProduct.Id;
            
            insert customSla;
            
            Case myCase = TGS_Dummy_Test_Data.dummyCaseTGS('Order_Management_Case', 'Authorized User');
            myCase.ContactId = contact.Id;
            //myCase.OwnerId = '00Gm0000000PLSDEA4';
            myCase.TGS_Service__c = commercialProduct.Name;
            
            Test.startTest();
                insert myCase;
            	TGS_SLA_Assigner_Handler.loadSLAs();
            Test.stopTest();
        }       
    }

}