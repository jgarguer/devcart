public class BI_AttachmentMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fco.Javier Sanz
    Company:       Salesforce.com
    Description:   Methods executed by Attachment Triggers 
    Test Class:    BI_AttachmentMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    03/07/2014              Fco.Javier Sanz          Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fco.Javier Sanz
    Company:       Salesforce.com
    Description:   Method that check if there is already an Attachment in the “BI_ImageHome__c” or "BI_Videoteca" and if the type and size of the attachment is correct
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    
    History: 
    
    <Date>                  <Author>                <Change Description>
   03/07/2014              Fco.Javier Sanz           Initial Version
   04/02/2016              Guillermo Muñoz         Modify to increase the test coverage      
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void validateImageAttachment(List <Attachment> news){
        try {

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }
        
            integer PngSizeImageHome= (1024*1024);  
            integer PngSizeVideoteca= (200*1024); 
            
            Schema.DescribeSObjectResult descr= BI_Videoteca__c.sObjectType.getDescribe();
            String prefixV =  descr.getKeyPrefix();
            
            descr = BI_ImageHome__c.sObjectType.getDescribe();
            String prefixI=  descr.getKeyPrefix();
            //system.debug( prefijio);      
            string ParentSubS = news[0].parentid;
            
            system.debug(news);
            
            Integer size = null;
            if (ParentSubS.substring(0,3).equals(prefixI)){
                size = PngSizeImageHome;
            }else if (ParentSubS.substring(0,3).equals(prefixV)){
                size = PngSizeVideoteca;
            }
            if(size != null ){
                    List<Attachment> myAttachment = [select id from Attachment where parentId= :news[0].parentid limit 1];
                            
                    if( !myAttachment.isempty()){
                        news[0].adderror(label.BI_AttachmentExist);
                        BI_LogHelper.generateLog('BI_AttachmentMethods.BI_AttachmentExist 0', 'BI_EN', news[0].ParentId, 'Trigger');
                    }
                    else{
                        if (news[0].ContentType!='image/png' && news[0].ContentType!='image/jpeg'){      
                            news[0].adderror(label.BI_AttachmentIncompatible);
                            BI_LogHelper.generateLog('BI_AttachmentMethods.BI_AttachmentIncompatible 1 ', 'BI_EN', news[0].ParentId, 'Trigger');
                        }
                        else{
                            if(news[0].BodyLength > size){
                                news[0].adderror(label.BI_AttachmentIncompatible);
                                BI_LogHelper.generateLog('BI_AttachmentIncompatible 2', 'BI_EN', news[0].ParentId, 'Trigger');
                            }
                        }
                    }
                
            }
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_AttachmentMethods.validateImageAttachment', 'BI_EN', exc, 'Trigger');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Send email to case owner
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        19/08/2014              Ana Escrich Acero           Initial Version    
        04/02/2016              Guillermo Muñoz         Modify to increase the test coverage
        23/11/2016              Antonio Pardo           The email is sent to case owner and case contact 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void sendEmail(List <Attachment> news){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            List<Profile> lst_profile = [select Id from Profile where Name = 'BI_Customer_Community_Plus' limit 1]; //changed Name = 'BI_Customer Communities' To Customer Community Plus
            
            if(UserInfo.getProfileId() == lst_profile[0].Id){
                
                system.debug('news: '+news);
                List<Id> Attachments = new List<Id>();
                Map<Id, Attachment> casesMap = new Map<Id, Attachment>();
                List<Id> accountIdCases = new List<Id>();
                for(Attachment item: news){
                    String part = item.ParentId;
                    part = part.substring(0,3);
                    system.debug('part: '+part);
                    if(part=='500'){
                        Attachments.add(item.ParentId);
                        casesMap.put(item.ParentId, item);
                    }
                }
                
                if(!Attachments.isEmpty()){
                    
                    List<EmailTemplate> templates = [Select Id, Name from EmailTemplate where DeveloperName='BI_Anexos_portal_platino'];
                
                    List<Case> casesToSend = [SELECT Id, Origin, Owner.Email, OwnerId, AccountId, ParentId, ContactId from Case where Id in :Attachments];
                    Map<String, List<String>> emailsToSend = new Map<String, List<String>>();
                    //Owner and Contact = case creator in portal
                    Set<Id> set_caseCreator = new Set<Id>();

                    for(Case item :casesToSend){
                        
                        //Added case creator and case owner
                        set_caseCreator.add(item.OwnerId);
                        set_caseCreator.add(item.ContactId);

                        if(!emailsToSend.containsKey(item.AccountId)){
                            accountIdCases.add(item.AccountId);
                            emailsToSend.put(item.AccountId, new List<String>());       
                        }
                        //}
                    }
                    System.debug('aaaa set_caseCreator: ' + set_caseCreator);
                    List<BI_Contact_Customer_Portal__c> portalContacts = new List<BI_Contact_Customer_Portal__c>();
                    
                    if(!accountIdCases.isEmpty()){
                    
                        portalContacts = [SELECT BI_Activo__c,BI_Cliente__c, BI_Contacto__c, BI_User__c,BI_User__r.isActive, BI_Contacto__r.Email,BI_User__r.Email FROM BI_Contact_Customer_Portal__c where BI_Cliente__c in :accountIdCases];
                        for(BI_Contact_Customer_Portal__c item: portalContacts){
                            System.debug('aaaa item: ' + item.BI_User__c + 'Contacto: ' + item.BI_Contacto__c);
                            //Sending emails only to case creator and case owner
                            if(item.BI_Contacto__c!=null && item.BI_Contacto__r.Email!=null && set_caseCreator.contains(item.BI_Contacto__c)){
                                emailsToSend.get(item.BI_Cliente__c).add(item.BI_Contacto__r.Email);
                            }
                            if(item.BI_User__c!=null && item.BI_User__r.Email!=null && item.BI_User__r.isActive && set_caseCreator.contains(item.BI_User__c)){
                                emailsToSend.get(item.BI_Cliente__c).add(item.BI_User__r.Email);
                            }
                        }
                        
                    }
                    
                    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                    for(Case item :casesToSend){
                        Attachment currentAttach = casesMap.get(item.Id);
                        String[] toaddress = new String[]{};
                        for(String address : emailsToSend.get(item.AccountId)){
                            toaddress.add(address);
                        }
                        
                        system.debug('debug item: ' + item.OwnerId);
                        if(item.Owner.Email != null)
                            toaddress.add(item.Owner.Email);
                        Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        efa.setFileName(currentAttach.Name);
                        efa.setBody(currentAttach.Body);
                        singleMail.setToAddresses(toaddress);
                        system.debug('toaddress: '+ toaddress);
                        //singleMail.setSubject('prueba');
                        singleMail.setTemplateId(templates[0].Id);
                        if(item.ContactId!=null){
                            singleMail.setTargetObjectId(item.ContactId);
                        }
                        else if(!portalContacts.isEmpty()){
                            singleMail.setTargetObjectId(portalContacts[0].BI_Contacto__c);
                        }
                        //APC eHelp 02185827 - Fixed fill email template with current case
                        singleMail.setWhatId(item.Id);
                        singleMail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                        emails.add(singleMail); 
                        system.debug('emails: '+emails);
                    }
                    if(!Test.isRunningTest()){
                        Messaging.sendEmail(emails);
                    }
                    
                }
                
            }
        
        }
        catch(exception exc){
            BI_LogHelper.generate_BILog('BI_AttachmentMethods.sendEmail', 'Portal Platino', exc, 'Class');
        }
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that sync attachments with opportunity field (BI_Per_Cantidad_de_archivos__c)

    History: 
    
     <Date>                     <Author>                <Change Description>
    16/10/2014                  Micah Burgos            Initial Version
    04/02/2016                  Guillermo Muñoz         Modify to increase the test coverage 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void countAttachments(List <Attachment> news,List <Attachment> olds){
        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            if(news != null && olds == null){ //ADD
                set<Id> list_oppNewsAtt = new set<Id>();
                
                for(Attachment newAtt :news){
                    list_oppNewsAtt.add(newAtt.ParentId);
                }


                if(!list_oppNewsAtt.isEmpty()){ //ADD
                    list<Opportunity> lst_oppTo_Add = [SELECT Id, BI_Per_Cantidad_de_archivos__c FROM Opportunity WHERE Id IN :list_oppNewsAtt ];
                    if(!lst_oppTo_Add.isEmpty()){


                        for(Opportunity opp :lst_oppTo_Add){
                            
                            opp.BI_Casilla_desarrollo__c = true;

                            if(opp.BI_Per_Cantidad_de_archivos__c == null)
                                opp.BI_Per_Cantidad_de_archivos__c = 0;
                            
                            for(Attachment att :news){
                                if(att.ParentId == opp.Id){
                                    opp.BI_Per_Cantidad_de_archivos__c = opp.BI_Per_Cantidad_de_archivos__c + 1; 
                                }
                            }
                        }


                        update lst_oppTo_Add;
                    }
                }
            
            }else if(news == null && olds != null){ //DELETE
        
                set<Id> list_oppOldsAtt = new set<Id>();

                for(Attachment oldAtt :olds){
                    list_oppOldsAtt.add(oldAtt.ParentId); 
                }

                if(!list_oppOldsAtt.isEmpty()){ //REMOVE
                    list<Opportunity> lst_oppTo_Remove = [SELECT Id, BI_Per_Cantidad_de_archivos__c FROM Opportunity WHERE Id IN :list_oppOldsAtt];
                    if(!lst_oppTo_Remove.isEmpty()){


                        for(Opportunity opp :lst_oppTo_Remove){

                            opp.BI_Casilla_desarrollo__c = true;
                            
                            for(Attachment att :olds){
                                if(att.ParentId == opp.Id){
                                    opp.BI_Per_Cantidad_de_archivos__c = opp.BI_Per_Cantidad_de_archivos__c - 1; 
                                }
                            }
                        }

                        update lst_oppTo_Remove;

                    }
                }
            }

        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_AttachmentMethods.countAttachments', 'BI_EN', exc, 'Trigger');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Prevent "Ejecutivo de Cliente" from deleting attachments related to an opportunity.

    History: 
    
    <Date>                     <Author>                <Change Description>
    17/11/2014                 Pablo Oliva             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventDelete(List<Attachment> olds){
        
        List<Attachment> lst_atta = new List<Attachment>();
        Set<Id> set_opp = new Set<Id>();
        
        User user = [select Id, BI_Permisos__c, Pais__c from User where Id = :UserInfo.getUserId() limit 1];
        
        if(user.Pais__c == Label.BI_Ecuador && user.BI_Permisos__c == Label.BI_Ejecutivo_de_Cliente){
            for(Attachment atta:olds){
                if(String.valueOf(atta.ParentId).startsWith('006'))
                    atta.addError(Label.BI_PreventDeleteAttachment);
            }
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Salesforce.com
    Description:   Save all the modifications on opportunity and case attachments in the object 'Historial de adjuntos oportunidad' and 'Historial de adjuntos caso'

    History: 
    
    <Date>                     <Author>                <Change Description>
    14/10/2015                 Guillermo Muñoz          Initial Version
    04/02/2016                  Guillermo Muñoz         Modify to increase the test coverage  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createHistorialOppCase(List <Attachment> news, List <Attachment> olds, String action) {

        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            List <BI_Historial_de_adjuntos_oportunidad__c> lstHistOpp = new List <BI_Historial_de_adjuntos_oportunidad__c>();
            List <BI_Historial_de_adjuntos_caso__c> lstHistCase = new List <BI_Historial_de_adjuntos_caso__c>();
            BI_Historial_de_adjuntos_oportunidad__c attHistO;
            BI_Historial_de_adjuntos_caso__c attHistC;
            List <Attachment> toProcess = news;
            //Condition added to create only one function for the insert, update and delete DML operations
            if(action == 'Delete'){
                toProcess = olds;
            }
            

                for(Attachment att : toProcess){
                    if(att.ParentId != null){
                        //if the Attachment belong to opportunity or case
                        if(att.ParentId.getSobjectType().getDescribe().getName() == 'Opportunity' || att.ParentId.getSobjectType().getDescribe().getName() == 'Case'){
                            //transform the file size to KB and round to 2 decimals
                            Decimal size = (att.BodyLength/1024.00).setScale(2);
                            //if belong to an opportunity create a record in BI_Hitorial_de_adjuntos_oportunidad__c object
                            if(att.ParentId.getSobjectType().getDescribe().getName() == 'Opportunity'){
                                attHistO = new BI_Historial_de_adjuntos_oportunidad__c();
                                attHistO.BI_Fecha_de_accion__c = System.now();
                                attHistO.BI_Accion_realizada_por__c = UserInfo.getUserId();
                                attHistO.BI_Titulo__c = (att.Name.length() > 255)?att.Name.substring(0,255):att.Name;  //Max length of field BI_Titulo__c = 255
                                attHistO.BI_Tamano__c = size + ' KB';
                                attHistO.BI_Oportunidad__c = att.ParentId;                           
                                if(action == 'Insert'){
                                    attHistO.BI_Accion_realizada__c = 'Agregado';
                                }
                                else if(action == 'Update'){
                                    attHistO.BI_Accion_realizada__c = 'Modificado';
                                }
                                else{                                
                                    attHistO.BI_Accion_realizada__c = 'Eliminado';
                                }                                
                                    lstHistOpp.add(attHistO);
                            }
                            //if belong to a case create a record in BI_Hitorial_de_adjuntos_caso__c object
                            else{
                                attHistC = new BI_Historial_de_adjuntos_caso__c();
                                attHistC.BI_Fecha_de_accion__c = System.now();
                                attHistC.BI_Accion_realizada_por__c = UserInfo.getUserId();
                                attHistC.BI_Titulo__c = (att.Name.length() > 255)?att.Name.substring(0,255):att.Name;  //Max length of field BI_Titulo__c = 255
                                attHistC.BI_Tamano__c = size + ' KB';
                                attHistC.BI_Caso__c = att.ParentId;                           
                                if(action == 'Insert'){
                                    attHistC.BI_Accion_realizada__c = 'Agregado';
                                }
                                else if(action == 'Update'){
                                    attHistC.BI_Accion_realizada__c = 'Modificado';
                                }
                                else{                                
                                    attHistC.BI_Accion_realizada__c = 'Eliminado';
                                }                                
                                    lstHistCase.add(attHistC);
                            }
                        }
                    }  
                }

            if(!lstHistOpp.isEmpty()){
                insert lstHistOpp;
            }
            if(!lstHistCase.isEmpty()){
                insert lstHistCase;
            }
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_AttachmentMethods.createHistorialOppCase', 'BI_EN', exc, 'Trigger');
        }

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that prevent the attachment insert on BI_Solicitud_envio_productos_y_servicios__c
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        02/02/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventInsertCHI (List <Attachment> news){

        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            List <Attachment> toProcess = new List <Attachment>();
            List <Id> idToProcces = new List <Id>();

            for(Attachment att : news){

                if(att.ParentId.getSobjectType().getDescribe().getName() == 'BI_Solicitud_envio_productos_y_servicios__c'){

                    idToProcces.add(att.ParentId);
                    toProcess.add(att);
                }
            }

            if(!idToProcces.isEmpty()){

                Map <Id, BI_Solicitud_envio_productos_y_servicios__c> map_sol = new Map <Id, BI_Solicitud_envio_productos_y_servicios__c> ( [SELECT Id, BI_Estado_de_ventas__c, BI_Estado_de_servicios__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id in: idToProcces] );

                if(!map_sol.isEmpty()){

                    for(Attachment att : toProcess){

                        if(map_sol.containsKey(att.ParentId)){
                            if(map_sol.get(att.ParentId).BI_Estado_de_ventas__c == 'Aprobado' || map_sol.get(att.ParentId).BI_Estado_de_servicios__c == 'Aprobado'){

                                att.addError('No se pueden adjuntar archivos una vez que la solicitud ha pasado por visado');
                            }
                        }
                    }
                }
            }
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_AttachmentMethods.preventInsertCHI', 'BI_EN', exc, 'Trigger');
        }
    }




    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Validation to avoid modification on Attachment
    IN:           List <Attachment>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    31/10/2017        Gawron, Julian    Add toCheck
    02/11/2017        Gawron, Julián    Adding Label.BI_PER_SinPermisos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void verificarPermisosContract(List<Attachment> news , List<Attachment> olds)
    {
        try
        {

         if(BI_TestUtils.isRunningTest()){throw new BI_Exception('Test');}

        List <Attachment> toProcess = new List <Attachment>();
        List <Attachment> toCheck = news != null ? news : olds;
        for(Attachment att : toCheck){
            if(att.ParentId.getSobjectType().getDescribe().getName() == 'NE__Contract_Header__c' || 
                att.ParentId.getSobjectType().getDescribe().getName() == 'NE__Contract__c'){
                toProcess.add(att);
            }
        }
            
     if(!toProcess.isEmpty()){
        Set<String> set_creacion = new Set<String>{Label.BI_Administrador,'Administrador de Contrato', Label.BI_SuperUsuario};
        Set<String> set_modificacion = new Set<String>{Label.BI_Administrador,'Administrador de Contrato', Label.BI_SuperUsuario};
        Set<String> set_eliminacion = new Set<String>{Label.BI_Administrador, Label.BI_SuperUsuario};

          List<User> me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId() and Profile.name = 'BI_Standard_PER' limit 1];
          if(me.isEmpty()) return;

           if(olds == null){
                //Insert
                if(!set_creacion.contains(me[0].BI_Permisos__c)){
                    for(Attachment attch : toProcess){
                        attch.addError(Label.BI_PER_SinPermisos);
                    }
                }
            }else if(news == null){
                //delete
                if(!set_eliminacion.contains(me[0].BI_Permisos__c)){
                    for(Attachment attch : toProcess){
                        attch.addError(Label.BI_PER_SinPermisos);
                    }
                }
            }else{
                if(!set_modificacion.contains(me[0].BI_Permisos__c)){
                    for(Attachment attch : toProcess){
                        attch.addError(Label.BI_PER_SinPermisos);
                    }
                }
            }
        }
    }    
     catch (exception Exc)
        {
           BI_LogHelper.generate_BILog('BI_AttachmentMethods.verificarPermisos', 'BI_EN', Exc, 'Trigger');
        } 

   }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro López
    Company:       Accenture
    Description:   Validation to verify the extension of the file attached
    IN:            List <Attachment>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    03/04/2018        Álvaro López      Initial version
    06/04/2018        Álvaro López      Added attachmentFromBulk and validate map_error != null
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void validateExtension(List<Attachment> news){

        Boolean extFound = false;

        Attachment firstZip = new Attachment();

        List<Attachment> lst_zip =new List<Attachment>();

        Map<String, Blob> map_zip2 =new Map<String, Blob>();
        Map<Id, String> map_error = new Map<Id, String>();

        try{
            List<SEG_Allowed_Extensions__c> lst_allowedExt = [SELECT Id, SEG_Extension__c FROM SEG_Allowed_Extensions__c];

            for(Attachment att :news){
                String attName = att.Name.toLowerCase();
                if(!BI_AttachmentMethods.attachmentFromBulk(String.valueOf(att.ParentId))){
                    for(SEG_Allowed_Extensions__c ext :lst_allowedExt){

                        if(attName.endsWith(ext.SEG_Extension__c)){

                            if(attName.endsWith('.zip')){
                                lst_zip.add(att);
                                firstZip.Id = att.Id;
                                firstZip.Name = attName;
                            }
                            System.debug('file name: ' + attName);
                            extFound = true;
                        }
                    }
                    if(!extFound){
                        System.debug('Error 1');
                        map_error.put(att.Id, 'Extension not allowed: ' + att.Name);
                    }
                }
            }

            if(!lst_zip.isEmpty()){
                while(validateZip(lst_zip, map_zip2, map_error, lst_allowedExt, firstZip));
            }

            System.debug('#Debug map_error '+ map_error);
            if(map_error != null){
                for(Attachment att :news){
                    if(map_error.containsKey(att.Id)){
                        if(!Test.isRunningTest()){
                            att.addError(map_error.get(att.Id));
                        }
                    }
                }
            }
        }
        catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_AttachmentMethods.validateExtension', 'BI_EN', exc, 'Trigger');
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro López
    Company:       Accenture
    Description:   Validation to verify the extension of the files from zip
    IN:            List <Attachment>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    03/04/2018        Álvaro López      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Boolean validateZip(List<Attachment> lst_zip, Map<String, Blob> map_zip2, Map<Id, String> map_error, List<SEG_Allowed_Extensions__c> lst_allowedExt, Attachment firstZip){
        System.debug('Coprobación zip 1');
        Boolean zip = false;
        Boolean exten = false;

        Integer contExt = 0;
        System.debug('List zip2: ' + map_zip2);
        if(!lst_zip.isEmpty()){
            
            for(Attachment att :lst_zip){

                SEG_Zippex sampleZip = new SEG_Zippex(att.Body);

                Set<String> set_fileNames = sampleZip.getFileNames();
                
                for(String fileName :set_fileNames){
                    System.debug('file name zip: ' + fileName);
                    for(SEG_Allowed_Extensions__c ext :lst_allowedExt){
                        if(fileName.endsWith(ext.SEG_Extension__c)){
                            if(fileName.endsWith('.zip')){
                                Blob fileData = sampleZip.getFile(fileName);
                                map_zip2.put(fileName, fileData);
                                zip = true;
                            }
                            contExt++;
                        }
                    }
                }
                System.debug('comparación size: ' + contExt + ' - ' + set_fileNames.size());
                if(contExt<set_fileNames.size()){
                    System.debug('Error 2');
                    map_error.put(firstZip.Id, 'Extension not allowed: ' + firstZip.Name);
                }
                else{
                    exten = true;
                }           
            }
            lst_zip.clear();
        }
        else if(map_zip2 != null){

            for(String bodyName :map_zip2.keySet()){

                SEG_Zippex sampleZip = new SEG_Zippex(map_zip2.get(bodyName));

                Set<String> set_fileNames = sampleZip.getFileNames();
                
                for(String fileName :set_fileNames){
                    System.debug('file name zip: ' + fileName);
                    for(SEG_Allowed_Extensions__c ext :lst_allowedExt){
                        if(fileName.endsWith(ext.SEG_Extension__c)){
                            if(fileName.endsWith('.rar') || fileName.endsWith('.zip') || fileName.endsWith('.1.7z')){
                                Blob fileData = sampleZip.getFile(fileName);
                                map_zip2.put(fileName, fileData);
                                zip = true;
                            }
                            contExt++;
                        }
                    }
                }
                map_zip2.remove(bodyName);

                System.debug('comparación size: ' + contExt + ' - ' + set_fileNames.size());
                if(contExt<set_fileNames.size()){
                    System.debug('Error 2');
                    map_error.put(firstZip.Id, 'Extension not allowed: ' + firstZip.Name);
                    break;
                }
                else{
                    exten = true;
                }           
            }
        }
        if(exten == true && zip == true){
            return true;
        }
        else{
            return false;
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro López
    Company:       Accenture
    Description:   Validation to verify attachments from bulk process
    IN:            String
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    06/04/2018        Álvaro López      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Boolean attachmentFromBulk(String parentId){

        Boolean isBulk = false;
        
        Schema.DescribeSObjectResult object_desc = null;
        Set<String> set_prefix = new Set<String>();
        Set<String> set_bulkObjects = new Set<String>{'Bit2WinHUB__Bulk_Configuration_Request__c', 'Bit2WinHUB__Bulk_Configuration_Item_Request__c', 'Bit2WinHUB__Bulk_Import_Request__c'};

        // Search every object in the getGlobalDescribe() map to check key prefixes
        for(Schema.SObjectType t : Schema.getGlobalDescribe().values()){
            Schema.DescribeSObjectResult descr = t.getDescribe();
            
            if(set_bulkObjects.contains(descr.getName())){
                set_prefix.add(descr.getKeyPrefix());
            }
        }

        if(parentId != null && set_prefix != null){
            for(String prefix: set_prefix){
                if(parentId.startsWith(prefix)){
                    isBulk = true;
                    System.debug('Attachment from bulk.');
                }
            }
        }

        return isBulk;
    }
}