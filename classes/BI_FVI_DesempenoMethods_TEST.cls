@isTest
private class BI_FVI_DesempenoMethods_TEST {

	public static list<BI_Registro_Datos_Desempeno__c> lstNodosDesOld = new list<BI_Registro_Datos_Desempeno__c>();
	public static list<BI_Registro_Datos_Desempeno__c> lstNodosDesNew = new list<BI_Registro_Datos_Desempeno__c>();
	public static BI_FVI_Nodos_Objetivos__c objNObjetivo = new BI_FVI_Nodos_Objetivos__c();

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('BI_FVI_Nodos__c', 'BI_FVI_Nodos_Objetivos__c', 'Account', 'BI_Objetivo_Comercial__c', 'BI_Registro_Datos_Desempeno__c', 'BI_FVI_Nodo_Objetivo_Desempeno__c')])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

	/*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método de creación de data

    History: 

    <Date>                  <Author>                    <Change Description>
    26/05/2016              Geraldine Pérez Montes      Initial Version
    20/09/2017              Angel F. Santaliestra       Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    18/02/2018              Humberto Nunes              Se agrego el Mapa de RT y se les coloco RT a  los NO
    21/02/2018              Humberto Nunes              Se Agrego codigo para que entrara a los Desempeños asociados a NODOS
    21/02/2018              Humberto Nunes              Se agrego codigo para cubrir Nodos Jerarquicos.
    15/03/2018              Humberto Nunes              Se agrego el Tipo de Registro 
    ----------------------------------------------------------------------*/ 
	public static void GenerateData() {

        RecordType rtAcc = [SELECT Id FROM RecordType WHERE DeveloperName != 'TGS_Business_Unit' AND SObjectType = 'Account' limit 1];

        system.debug('BI_FVI_NCS' + MAP_NAME_RT.get('BI_FVI_NCS'));

		BI_FVI_Nodos__c objNodo1 = new BI_FVI_Nodos__c();
		objNodo1.name = 'NodoPadre1';
		objNodo1.BI_FVI_Pais__c = Label.BI_Peru;
		objNodo1.RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS');
		objNodo1.BI_FVI_Activo__c = true;

		insert objNodo1;

		BI_FVI_Nodos__c objNodo2 = new BI_FVI_Nodos__c();
		objNodo2.name = 'NodoPadre2';
		objNodo2.BI_FVI_Pais__c = Label.BI_Peru;
		objNodo2.RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS');
		objNodo2.BI_FVI_NodoPadre__c = objNodo1.Id;
		objNodo2.BI_FVI_Activo__c = true;

		insert objNodo2;

		BI_FVI_Nodos__c objNodo3 = new BI_FVI_Nodos__c();
		objNodo3.name = 'NodoPadre3';
		objNodo3.BI_FVI_Pais__c = Label.BI_Peru;
		objNodo3.RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS');
		objNodo3.BI_FVI_NodoPadre__c = objNodo2.Id;
		objNodo3.BI_FVI_Activo__c = true;

		insert objNodo3;

		BI_FVI_Nodos__c objNodo4 = new BI_FVI_Nodos__c();
		objNodo4.name = 'NodoPadre4';
		objNodo4.BI_FVI_Pais__c = Label.BI_Peru;
		objNodo4.RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS');
		objNodo4.BI_FVI_NodoPadre__c = objNodo3.Id;
		objNodo4.BI_FVI_Activo__c = true;

		insert objNodo4;

		BI_FVI_Nodos__c objNodo5 = new BI_FVI_Nodos__c();
		objNodo5.name = 'NodoPadre5';
		objNodo5.BI_FVI_Pais__c = Label.BI_Peru;
		objNodo5.RecordTypeId = MAP_NAME_RT.get('BI_FVI_NCS');
		objNodo5.BI_FVI_NodoPadre__c = objNodo4.Id;
		objNodo5.BI_FVI_Activo__c = true;

		insert objNodo5;

		BI_Periodo__c objPeriodo = new BI_Periodo__c();
		objPeriodo.name = 'Junio2017';
		objPeriodo.BI_FVI_Activo__c = true;
		objPeriodo.BI_FVI_Fecha_Inicio__c =  Date.today();
		objPeriodo.BI_FVI_Fecha_Fin__c = Date.today() + 10;

		insert objPeriodo;

		BI_Objetivo_Comercial__c objOComercial = new BI_Objetivo_Comercial__c();
        objOComercial.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos');
		objOComercial.BI_Tipo__c  = 'Actos Comerciales';
		objOComercial.BI_FVI_Unidad_Medida__c = 'Cantidad';
		objOComercial.BI_FVI_Descripcion__c = 'AC';
		objOComercial.BI_FVI_Activo__c = true;

		insert objOComercial;

        objNObjetivo.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos');
		objNObjetivo.BI_FVI_Id_Nodo__c = objNodo5.id;
		objNObjetivo.BI_FVI_IdPeriodo__c = objPeriodo.id;
		objNObjetivo.BI_FVI_Objetivo_Comercial__c = objOComercial.id;
		objNObjetivo.BI_FVI_Objetivo__c = 500;

		insert objNObjetivo;

 
		List<Account> lstAcc = new List<Account>();
        
        Account objAcc = new Account(
        Name = 'Acc-Nodos1',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today(),
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Nodo__c = objNodo5.id,
        BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test',
        RecordTypeId = rtAcc.Id);
        
        lstAcc.add(objAcc);

        Account objAcc2 = new Account(
        Name = 'Acc-Nodos2',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today()+1,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Nodo__c = objNodo4.id,
        BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test',
        RecordTypeId = rtAcc.Id);
        
        lstAcc.add(objAcc2);

        Account objAcc3 = new Account(
        Name = 'Acc-Nodos3',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today()-1,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Nodo__c = objNodo3.id,
        BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test',
        RecordTypeId = rtAcc.Id);
        
        lstAcc.add(objAcc3);

        insert lstAcc;

        BI_FVI_Nodo_Cliente__c ObjNCliente = new BI_FVI_Nodo_Cliente__c();
        ObjNCliente.BI_FVI_IdCliente__c = lstAcc[0].id;
        ObjNCliente.BI_FVI_IdNodo__c = objNodo5.Id;
        insert ObjNCliente;


        BI_Registro_Datos_Desempeno__c objNodosDesOld = new BI_Registro_Datos_Desempeno__c();
        objNodosDesOld.BI_FVI_Fecha__c = Date.today();
        objNodosDesOld.BI_FVI_Id_Cliente__c = lstAcc[0].id;
        objNodosDesOld.BI_Tipo__c = 'Visitas';
        objNodosDesOld.BI_FVI_Valor__c = 2;
        objNodosDesOld.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesOld.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018


        lstNodosDesOld.add(objNodosDesOld);

        BI_Registro_Datos_Desempeno__c objNodosDesOld2 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesOld2.BI_FVI_Fecha__c = Date.today();
        objNodosDesOld2.BI_FVI_Id_Cliente__c = lstAcc[1].id;
        objNodosDesOld2.BI_Tipo__c = 'Visitas';
        objNodosDesOld2.BI_FVI_Valor__c = 2;
        objNodosDesOld2.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesOld2.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018


        lstNodosDesOld.add(objNodosDesOld2);

        BI_Registro_Datos_Desempeno__c objNodosDesOld3 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesOld3.BI_FVI_Fecha__c = Date.today()-1;
        objNodosDesOld3.BI_FVI_Id_Cliente__c = lstAcc[2].id;
        objNodosDesOld3.BI_Tipo__c = 'Visitas';
        objNodosDesOld3.BI_FVI_Valor__c = 2;
        objNodosDesOld3.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesOld3.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018

        lstNodosDesOld.add(objNodosDesOld3);

        BI_Registro_Datos_Desempeno__c objNodosDesOld5 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesOld5.BI_FVI_Fecha__c = Date.today()+3;
        objNodosDesOld5.BI_FVI_Id_Cliente__c = lstAcc[2].id;
        objNodosDesOld5.BI_Tipo__c = 'Visitas';
        objNodosDesOld5.BI_FVI_Valor__c = 2;
        objNodosDesOld5.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesOld5.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018

        lstNodosDesOld.add(objNodosDesOld5);


        insert lstNodosDesOld;
        

        BI_Registro_Datos_Desempeno__c objNodosDesNew = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNew.BI_FVI_Fecha__c = Date.today();
        objNodosDesNew.BI_FVI_Id_Cliente__c = lstAcc[0].id;
        objNodosDesNew.BI_Tipo__c = 'Visitas';
        objNodosDesNew.BI_FVI_Valor__c = 4;
        objNodosDesNew.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesNew.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018

        lstNodosDesNew.add(objNodosDesNew);

        BI_Registro_Datos_Desempeno__c objNodosDesNew2 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNew2.BI_FVI_Fecha__c = Date.today()+2;
        objNodosDesNew2.BI_FVI_Id_Cliente__c = lstAcc[1].id;
        objNodosDesNew2.BI_Tipo__c = 'Visitas';
        objNodosDesNew2.BI_FVI_Valor__c = 4;
        objNodosDesNew2.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesNew2.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018

        lstNodosDesNew.add(objNodosDesNew2);

        BI_Registro_Datos_Desempeno__c objNodosDesNew3 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNew3.BI_FVI_Fecha__c = Date.today()-1;
        objNodosDesNew3.BI_FVI_Id_Cliente__c = lstAcc[2].id;
        objNodosDesNew3.BI_Tipo__c = 'Visitas';
        objNodosDesNew3.BI_FVI_Valor__c = 5;
        objNodosDesNew3.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesNew3.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018

        lstNodosDesNew.add(objNodosDesNew3);


        BI_Objetivo_Comercial__c objOComercial4 = new BI_Objetivo_Comercial__c();
        objOComercial4.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos');
        objOComercial4.BI_Tipo__c  = 'Visitas';
        objOComercial4.BI_FVI_Unidad_Medida__c = 'Cantidad';
        objOComercial4.BI_FVI_Descripcion__c = 'Visitas XXX';
        objOComercial4.BI_FVI_Activo__c = true;

        insert objOComercial4;

        BI_FVI_Nodos_Objetivos__c objNObjetivo4 = new BI_FVI_Nodos_Objetivos__c();
        objNObjetivo4.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos');
        objNObjetivo4.BI_FVI_Id_Nodo__c = objNodo3.id;
        objNObjetivo4.BI_FVI_IdPeriodo__c = objPeriodo.id;
        objNObjetivo4.BI_FVI_Objetivo_Comercial__c = objOComercial4.id;
        objNObjetivo4.BI_FVI_Objetivo__c = 500;
        insert objNObjetivo4;

        BI_Registro_Datos_Desempeno__c objNodosDesNew5 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNew5.BI_FVI_Fecha__c = Date.today()+1;
        objNodosDesNew5.BI_FVI_Id_Cliente__c = lstAcc[2].id;
        objNodosDesNew5.BI_Tipo__c = 'Visitas';
        objNodosDesNew5.BI_FVI_Valor__c = 5;
        objNodosDesNew5.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesNew5.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
        lstNodosDesNew.add(objNodosDesNew5);




        // ************** ASOCIADO A NODOS *****************
        BI_Objetivo_Comercial__c objOComercial2 = new BI_Objetivo_Comercial__c();
        objOComercial2.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Objetivos_Nodos');
        objOComercial2.BI_Tipo__c  = 'Visitas';
        objOComercial2.BI_FVI_Unidad_Medida__c = 'Cantidad';
        objOComercial2.BI_FVI_Descripcion__c = 'Visitas';
        objOComercial2.BI_FVI_Activo__c = true;
        insert objOComercial2;

        // OBJETIVO EN NODO1
        BI_FVI_Nodos_Objetivos__c objNObjetivo2 = new BI_FVI_Nodos_Objetivos__c();
        objNObjetivo2.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos');
        objNObjetivo2.BI_FVI_Id_Nodo__c = objNodo1.id;
        objNObjetivo2.BI_FVI_IdPeriodo__c = objPeriodo.id;
        objNObjetivo2.BI_FVI_Objetivo_Comercial__c = objOComercial2.id;
        objNObjetivo2.BI_FVI_Objetivo__c = 500;
        insert objNObjetivo2;

        // ASOCIADO A NODO1... 
        BI_Registro_Datos_Desempeno__c objNodosDesNew4 = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNew4.BI_FVI_Fecha__c = Date.today()+1;
        objNodosDesNew4.BI_FVI_Id_Nodo__c = objNodo1.Id;
        objNodosDesNew4.BI_Tipo__c = 'Visitas';
        objNodosDesNew4.BI_FVI_Valor__c = 5;
        objNodosDesNew4.BI_FVI_IdObjeto__c = '098765432123buh789';
        objNodosDesNew4.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
        lstNodosDesNew.add(objNodosDesNew4);
        // ************** ASOCIADO A NODOS *****************


        // ************** ASOCIADO A NODOS JERARQUICOS *****************

        BI_FVI_Nodos__c objNodoJerarquico = new BI_FVI_Nodos__c();
        objNodoJerarquico.name = 'NodoJERARQUICO';
        objNodoJerarquico.BI_FVI_Pais__c = Label.BI_Argentina;
        objNodoJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_NCJ');
        objNodoJerarquico.BI_FVI_Activo__c = true;
        insert objNodoJerarquico;

        BI_Objetivo_Comercial__c objOComercialJerarquico = new BI_Objetivo_Comercial__c();
        objOComercialJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Objetivos_Jerarquicos');
        objOComercialJerarquico.BI_Tipo__c  = 'Oportunidad';
        objOComercialJerarquico.BI_OBJ_Campo__c = 'Amount';
        objOComercialJerarquico.BI_FVI_Unidad_Medida__c = 'Moneda';
        objOComercialJerarquico.BI_FVI_Descripcion__c = 'FCV';
        objOComercialJerarquico.BI_FVI_Activo__c = true;
        insert objOComercialJerarquico;

        BI_FVI_Nodos_Objetivos__c objNObjetivoJerarquico = new BI_FVI_Nodos_Objetivos__c();
        objNObjetivoJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Jerarquicos');
        objNObjetivoJerarquico.BI_FVI_Id_Nodo__c = objNodoJerarquico.id;
        objNObjetivoJerarquico.BI_FVI_IdPeriodo__c = objPeriodo.id;
        objNObjetivoJerarquico.BI_FVI_Objetivo_Comercial__c = objOComercialJerarquico.id;
        objNObjetivoJerarquico.BI_FVI_Objetivo__c = 500;
        insert objNObjetivoJerarquico;
       
        BI_Registro_Datos_Desempeno__c objNodosDesNewJerarquico = new BI_Registro_Datos_Desempeno__c();
        objNodosDesNewJerarquico.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Desempeno_Nodos_Jerarquico');
        objNodosDesNewJerarquico.BI_OBJ_NodoObjetivo__c = objNObjetivoJerarquico.Id;
        objNodosDesNewJerarquico.BI_FVI_Fecha__c = Date.today()+1;
        objNodosDesNewJerarquico.BI_Tipo__c = 'Oportunidad';
        objNodosDesNewJerarquico.BI_FVI_Valor__c = 5;
        objNodosDesNewJerarquico.BI_FVI_IdObjeto__c = '098765432123buh789';
        lstNodosDesNew.add(objNodosDesNewJerarquico);


        // ************** ASOCIADO A NODOS JERARQUICOS *****************

        insert lstNodosDesNew;

        // EVALUA ACTUALIZAR VALOR 
        objNodosDesNewJerarquico.BI_FVI_Valor__c = 50;
        update objNodosDesNewJerarquico;

        lstNodosDesNew[2].BI_FVI_Fecha__c = Date.today()-8;

        update lstNodosDesNew;
	}

	/*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método para actualizaValor

    History: 

    <Date>                  <Author>                <Change Description>
    26/05/2016              Geraldine Pérez Montes       Initial Version     
    ----------------------------------------------------------------------*/ 
	public static testMethod void actualizaValor_TEST() {

		BI_TestUtils.throw_exception = false;

		GenerateData();
		List<BI_FVI_Nodo_Objetivo_Desempeno__c> lstNOD = new List<BI_FVI_Nodo_Objetivo_Desempeno__c>();

		BI_FVI_Nodo_Objetivo_Desempeno__c objNOD = new BI_FVI_Nodo_Objetivo_Desempeno__c();
		objNOD.BI_FVI_Registro_Datos_Desempeno__c = lstNodosDesNew[0].Id;
		objNOD.BI_FVI_Valor__c = 60;
		objNOD.BI_FVI_Nodo_Objetivo__c = objNObjetivo.Id;
        objNOD.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivo_Desempeno');
		lstNOD.add(objNOD);
		insert lstNOD;

		Test.startTest();
		

            BI_FVI_DesempenoMethods.actualizaValor(lstNodosDesNew, lstNodosDesOld );  
            
        Test.stopTest();  
	}
	

	/*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método para actualizaValor

    History: 

    <Date>                  <Author>                <Change Description>
    26/05/2016           Alvaro Sevilla       		Initial Version     
    ----------------------------------------------------------------------*/ 
	public static testMethod void generaNodo_Objetivo_Desempeno_TEST() {

		BI_TestUtils.throw_exception = false;

		Test.startTest();

		GenerateData();

        BI_FVI_DesempenoMethods.generaNodo_Objetivo_Desempeno(lstNodosDesNew, lstNodosDesOld);  
            
        Test.stopTest();  
	}

	//public static testMethod void generaNodo_Objetivo_Desempeno_TEST2() {

	//	BI_TestUtils.throw_exception = false;

	//	Test.startTest();

	//	GenerateData();
		
		
		

 //       BI_FVI_DesempenoMethods.generaNodo_Objetivo_Desempeno(lstNodosDesNew, lstNodosDesOld);  
            
 //       Test.stopTest();  
	//}

	/*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método para generar exceción

    History: 

    <Date>                  <Author>                <Change Description>
    26/05/2016              Geraldine Pérez Montes       Initial Version     
    ----------------------------------------------------------------------*/ 	
	@isTest static void GenerateException() {

		BI_TestUtils.throw_exception = true;
		BI_FVI_DesempenoMethods.actualizaValor(lstNodosDesNew, lstNodosDesOld );
		BI_FVI_DesempenoMethods.generaNodo_Objetivo_Desempeno(lstNodosDesNew, lstNodosDesOld); 

	}
	
    /*------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   recopilaDatosDesempenoParaOppYRevenues test

    History: 

    <Date>                  <Author>                <Change Description>
    19/09/2016              Miguel Cabrera       Initial Version 
    25/09/2017              Jaime Regidor        Fields BI_Subsector__c and BI_Sector__c added    
    ----------------------------------------------------------------------*/ 

    @isTest static void recopilaDatosDesempenoParaOppYRevenuesTST() {

        RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_O4_TGS'];
        String pais = 'Spain';

        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = 'Spain',
                          IsActive = true);
        insert usertest;

        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Country__c = 'Spain'
                  );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        Opportunity opp = new Opportunity(Name = 'Test'+system.now(),
                          CloseDate = Date.today(),
                          AccountId = acc1.Id,
                          BI_Ciclo_ventas__c = Label.BI_Completo,
                          BI_Country__c = 'Spain',
                          BI_Fecha_de_cierre_real__c = Date.today(),
                          BI_O4_Total_Calculated_NAV__c = 1000,
                          OwnerId = usertest.Id,
                          StageName = 'F1 - Closed Won'
                          );
        insert opp;
        

        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = acc1.Id;
        atm.TeamMemberRole = 'GSM';
        atm.UserId = usertest.Id;
        insert atm; 

        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.OpportunityId = opp.Id;
        otm.TeamMemberRole = 'GSM';
        otm.UserId = usertest.Id;
        insert otm; 

        BI_O4_Revenue__c rvv = new BI_O4_Revenue__c();
        rvv.BI_O4_Year__c = 2016;
        rvv.BI_O4_Value__c = 1000;
        rvv.BI_O4_Month__c = '09';
        rvv.BI_O4_Account__c = acc1.Id;

        insert rvv;

        List<BI_Registro_Datos_Desempeno__c> lstRdd = new List<BI_Registro_Datos_Desempeno__c>();
        BI_Registro_Datos_Desempeno__c rdd = new BI_Registro_Datos_Desempeno__c();
        rdd.RecordTypeId = rt.Id;
        rdd.BI_O4_Month__c = 9;
        rdd.BI_O4_Year__c = 2016;
        rdd.OwnerId = usertest.Id;
        rdd.BI_FVI_IdObjeto__c = '098765432123buh789';


        Test.startTest();
        lstRdd.add(rdd);
        insert lstRdd;
        BI_FVI_DesempenoMethods.recopilaDatosDesempenoParaOppYRevenues(lstRdd);
        Test.stopTest();


    }
}