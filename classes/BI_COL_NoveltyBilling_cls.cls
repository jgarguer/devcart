/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Carlos Carvajal
    Company:       Salesforce.com
    Description:   Class for Facturaction Colombia.
    
    History:
    
    <Date>            <Author>          <Description>
    26/03/2015        Carlos Carvajal     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


global class BI_COL_NoveltyBilling_cls 
{

    // Programa que permite procesar las DS y a partir de ellas generar las novedades de facturación 
    // para el sistema DavoxPlus.
    // Desarrollado por: AVANXO Sucursal Colombia (CFC)
    // Fecha: Abril/2015
    // Release notes:

	// PERMITE PROCESAR LAS DS para generar las Nobvedades de facturación
	webservice static void ProcesarServCorporativos_ws(BI_COL_Modificacion_de_Servicio__c[] DSNew, boolean esInsercion)
	{
		System.Debug('Entra al procesar serv corporativos');
		string strInterfaz = 'DAVOXPLUS', strTransaccion = 'SERV. CORPORATIVO - NOVEDADES', strNombreObjeto = 'BI_COL_Modificacion_de_Servicio__c', strDireccion = 'SALIENTE';
		boolean blnConfigCargada = false, blnProcesada = false, blnRegistrada = false;
		BI_COL_Configuracion_campos_interfases__c configuracionInterfaz = new BI_COL_Configuracion_campos_interfases__c(); //Configuracion Interfaz
		list<BI_COL_Configuracion_campos_interfases__c> Interfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>(); //Configuracion Interfaz
		list<BI_COL_Configuracion_campos_interfases__c> camposConfigInterfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>(); //Configuracion Campos Interrfaz
		list<id> Registros_list = new list<id>();
		list<id> NuevosClientes_list = new list<id>();
		map<id, id> NuevosClientes_map = new map<id, id>();
		map<id, string> cadenas_map = new map<id, string>();
		map<id, string> errores_map = new map<id, string>();
		map<id, string> novedades_map = new map<id, string>();
		map<id, string> novedadesEliminar_map = new map<id, string>();
		set<id> idsSet = new set<id>();
		map<id, BI_COL_Modificacion_de_Servicio__c> DS_map = new map<id, BI_COL_Modificacion_de_Servicio__c>();
		map<id,id> registrosAsociados = new map<id,id>();
		for(integer x = 0; x < DSNew.size(); x++)
		{
			//idsSet.add(DSNew[x].id);
			Registros_list.add(DSNew[x].id);
		}
		String tpTransaccion='';

		// Recorre todas las DS procesadas para validar si aplican y obtener sus Ids
		// =================================================================================
		System.debug('DSNew.size():'+DSNew.size());
		
		System.Debug('Cantidad de novedades: ' + Registros_list.size());
		// Si hay registros (DS) por procesar se continúa el programa
		if(Registros_list.size()>0)
		{
			strTransaccion = 'SERV. CORPORATIVO - NOVEDADES'; strNombreObjeto = 'BI_COL_Modificacion_de_Servicio__c';
			Interfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>(); //Configuracion Interfaz
			camposConfigInterfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>(); //Configuracion Campos Interfaz
			cadenas_map = new map<id, string>();
			errores_map = new map<id, string>();
			// Carga la parametrización de la Interfaz
			blnConfigCargada = BI_COL_Basic_pck_cls.cargarConfiguracion(Interfaz_list, camposConfigInterfaz_list, 
																		strInterfaz, strTransaccion, strDireccion);
			System.debug('\n\n blnConfigCargada=> '+blnConfigCargada+'\n\n');

			if(blnConfigCargada)
			{
				configuracionInterfaz = Interfaz_list.get(0);
				blnProcesada = BI_COL_Basic_pck_cls.procesarRegistros(configuracionInterfaz, camposConfigInterfaz_list, Registros_list, cadenas_map, errores_map, novedades_map,tpTransaccion);
				system.debug('\n\n Termina de procesar Registros \n\n');
				blnRegistrada = BI_COL_Basic_pck_cls.registrarLog(cadenas_map, errores_map, strInterfaz, strNombreObjeto, strTransaccion, strDireccion, novedades_map, registrosAsociados);
				system.debug('\n\n blnRegistrada ' +blnRegistrada);
			}
			else
			{
				system.debug('ERROR: No se encontró la configuración para la Interfaz con DavoxPlus. Transacción: '+strTransaccion); // Mostrar mensaje de error
			}
		}
	} // Fin del webservice RegistrarNovedadesDS_ws

	 // Permite registrar los cobros parciales
	webservice static void RegistrarCobrosParciales_ws(BI_COL_Cobro_Parcial__c[] CobrosNew, BI_COL_Cobro_Parcial__c[] CobrosOld, boolean esInsercion)
	{
		System.Debug('Ingresa a RegistrarCobrosParciales' +  CobrosNew);
		string strInterfaz = 'DAVOXPLUS'; 
		String strTransaccion = 'SERV. CORPORATIVO - COBROS PARCIALES'; 
		String strNombreObjeto = 'Cobro_Parcial__c'; 
		String strDireccion = 'SALIENTE'; 
		String strNovedad = 'COBRO PARCIAL';
		boolean blnConfigCargada = false, blnProcesada = false, blnRegistrada = false;
		BI_COL_Configuracion_campos_interfases__c configuracionInterfaz = new BI_COL_Configuracion_campos_interfases__c();
		list<BI_COL_Configuracion_campos_interfases__c> Interfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>();
		list<BI_COL_Configuracion_campos_interfases__c> camposConfigInterfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>();
		list<id> Registros_list = new list<id>();
		set<id> idsSet = new set<id>();
		map<id, BI_COL_Modificacion_de_Servicio__c> DS_map = new map<id, BI_COL_Modificacion_de_Servicio__c>(); 
		map<id, string> cadenas_map = new map<id, string>();
		map<id, string> errores_map = new map<id, string>();
		map<id, string> novedades_map = new map<id, string>();
		map<id, string> novedadesEliminar_map = new map<id, string>();
		map<id,id> registrosAsociados = new map<id,id>();
		integer TotalRegistros = 0;
		map<id, id> NuevosClientes_map = new map<id, id>();
		System.Debug('Finaliza declaración de variables');

		if(CobrosNew != null)
		{   
			System.Debug('CobrosNew tiene registros' + CobrosNew);
			TotalRegistros = CobrosNew.size();
			for(integer x = 0; x < CobrosNew.size(); x++)
			{
				idsSet.add(CobrosNew[x].BI_COL_MS__c);
			}
			System.Debug('131 ' + idsSet.Size());
		}
		else
		{
			TotalRegistros = CobrosOld.size();
			for(integer y = 0; y < CobrosOld.size(); y++)
			{
				idsSet.add(CobrosOld[y].BI_COL_MS__c);
			}
		}
		//system.debug('idsSet: '+idsSet);
		for(BI_COL_Modificacion_de_Servicio__c DS : [select id, Name,BI_COL_Oportunidad__r.AccountId 
								from BI_COL_Modificacion_de_Servicio__c where id in :idsSet])
		{
			DS_map.put(DS.id, DS);
			NuevosClientes_map.put(DS.BI_COL_Oportunidad__r.AccountId,DS.BI_COL_Oportunidad__r.AccountId);
		}

		// Recorre todos los Contactos procesados para validar si aplican y obtener sus Ids
		// =================================================================================
		for(integer i = 0; i < TotalRegistros; i++)
		{
			System.Debug('Recorre el for 146: ' + TotalRegistros);
			BI_COL_Cobro_Parcial__c CobroNew = CobrosNew[i];
			if(esInsercion)
			{
				System.Debug('Si es un insert');
				// Registrar cobro al Log para ser enviado a DavoxPlus
				Registros_list.add(CobrosNew[i].id);
				novedades_map.put(CobroNew.id, strNovedad);
				registrosAsociados.put(CobroNew.id, CobroNew.BI_COL_MS__c);
			}
		}
		idsSet.clear();
		DS_map.clear();
		if(Registros_list.size()>0)
		{
            // Carga la parametrización de la Interfaz
            //system.debug('Procesando Cobros Parciales...');
			blnConfigCargada = BI_COL_Basic_pck_cls.cargarConfiguracion(Interfaz_list, camposConfigInterfaz_list, strInterfaz, strTransaccion, strDireccion);
			if(blnConfigCargada)
			{
				configuracionInterfaz = Interfaz_list.get(0);
				blnProcesada = BI_COL_Basic_pck_cls.procesarRegistros(configuracionInterfaz, camposConfigInterfaz_list, Registros_list, cadenas_map, errores_map, novedades_map,strNovedad);
				blnRegistrada = BI_COL_Basic_pck_cls.registrarLog(cadenas_map, errores_map, strInterfaz, strNombreObjeto, strTransaccion, strDireccion, novedades_map, registrosAsociados);
			}
			else
			{
				system.debug('ERROR: No se encontró la configuración para la Interfaz con DavoxPlus. Transacción: '+strTransaccion); // Mostrar mensaje de error
			}
		}
	} // Fin del webservice RegistrarCobrosParciales_ws

}