@isTest
private class BI_O4_QuoteMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test class to manage the coverage code for BI_QuoteMethods class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    11/01/2017              Alvaro García          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method to manage the code coverage for BI_AccountMethods.activeProposalItem.
            
    History:
    
    Action:  
    
    <Date>                  <Author>                <Change Description>
    11/01/2017              Alvaro García           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    static testMethod void activeProposalItemTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Opportunity opp = new Opportunity(Name = 'Prueba Opp',
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );

	    insert opp;

 	    Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = opp.Id);
	   	insert quote;

	   	BI_O4_Proposal_Item__c pItem = new BI_O4_Proposal_Item__c (BI_O4_Quote__c = quote.Id, BI_O4_SubOpportunity__c = opp.Id);
        Insert pItem;

        System.debug('!!!sync: ' + [SELECT IsSyncing FROM Quote WHERE Id = :quote.Id].IsSyncing );

        Test.startTest();

	        opp.SyncedQuoteId = quote.Id;
	        update opp;

        Test.stopTest();

        Boolean sol = [SELECT BI_O4_Proposal_Activa__c FROM BI_O4_Proposal_Item__c WHERE Id = :pItem.Id].BI_O4_Proposal_Activa__c;
        System.assertEquals(true, sol);
  }

	static testMethod void exceptionsTest() {
		BI_TestUtils.throw_exception = true;
        
        Test.startTest();

	        BI_O4_Quote_Methods.activeProposalItem(null, null);
            BI_O4_Quote_Methods.selectQuoteSkipGates(null, null);
        Test.stopTest();
  }

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Almirón
    Company:       Aborda
    Description:   Test method to manage the code coverage for BI_O4_QuoteMethods.migAppOppQuote.
            
    History:
    
    Action:  
    
    <Date>                  <Author>                       <Change Description>
    10/07/2017              Javier Almirón García          Initial Version
    20/09/2017        Angel F. Santaliestra             Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
    28/09/2017              Javier Almirón García          Added method "BI_MigrationHelper.skipAllTriggers()" to avoid Too Many Queries
	11/10/2017			 	Oscar Bartolo		  		   Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    

    static testMethod void migAppOppQuoteTest() {

        Profile prof = [SELECT Id FROM Profile WHERE Name IN ('BI_O4_TGS_Standard') LIMIT 1];

        User u = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Ejecutivo de Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');

        insert u;


        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' Limit 1];
        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

        List <Recordtype> rType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_Oportunidad_Regional','BI_Ciclo_completo', 'BI_O4_Proposal_Ready', 
                                                                                  'BI_O4_Gate_3_Closed', 'BI_O4_Organic_Growth', 'TGS_Legal_Entity')];

        Map <String, Id> rT = new Map <String, Id>();

         for ( Recordtype r : rType){

            rT.put (r.DeveloperName , r.Id);
         }


        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'Test2',
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'TEST2',
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test',
                                  BI_Subsegment_local__c = 'Top 1',
                                  RecordTypeId = rT.get('TGS_Legal_Entity')
                                  );    
        lst_acc1.add(acc1);
        System.runAs(usu){

            TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
            if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
    
                userTGS.TGS_Is_BI_EN__c = true;
                userTGS.TGS_Is_TGS__c = false;
    
                if(userTGS.Id != null){
                    update userTGS;
                }
                else{
                    insert userTGS;
                }
            }

            BI_MigrationHelper.skipAllTriggers();
            insert lst_acc1;
            
    
            List<Account> lst_acc = new List<Account>();       
            Account acc = new Account(Name = 'Test2',
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Segment__c = 'Test',
                                      BI_Subsegment_Regional__c = 'test',
                                      BI_Territory__c = 'test',
                                      BI_Subsegment_local__c = 'Top 1',
                                      ParentId = lst_acc1[0].Id
                                      );    
            lst_acc.add(acc);
            insert lst_acc;
            
    
            Opportunity opp = new Opportunity(Name = 'Test2' + system.now(),
                                              CloseDate = Date.today(),
                                              StageName = Label.BI_F6Preoportunidad,
                                              AccountId = lst_acc[0].Id,
                                              RecordTypeId = rT.get('BI_Oportunidad_Regional')
                                              );    
            
            insert opp;
    
    
            OpportunityTeamMember teamMember = new OpportunityTeamMember ();
            teamMember.TeamMemberRole = 'Global Solutions Engineer Principal';
            teamMember.User = u;
            teamMember.Opportunity = opp;
            teamMember.OpportunityId = opp.ID;
            teamMember.UserId = u.Id;
    
            insert teamMember;
    
            Set <Id> oppSet = new Set <Id>();
            oppSet.add(opp.Id);
    
    
            List <Quote> proposalSync = new List <Quote> ();

            Quote proposalSyncZero = new Quote();
            proposalSyncZero.RecordTypeId = rT.get('BI_O4_Proposal_Ready');
            proposalSyncZero.Name = 'Test';
            proposalSyncZero.OpportunityId = opp.Id;
            proposalSyncZero.BI_O4_GSE__c = 'TestUser UserTest';
            proposalSyncZero.BI_O4_USER_GSE__c = UserInfo.getUserId();
            proposalSyncZero.BI_O4_Proposal_FCV_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_FCV_TGS__c = 10;
            proposalSyncZero.BI_O4_Proposal_iCOST_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_iCOST_TGS__c = 10;
            proposalSyncZero.BI_O4_Proposal_iCAPEX_TGS__c = 10;
            proposalSyncZero.BI_O4_Proposal_iCAPEX_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_NRR_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_NRR_TGS__c = 10;
            proposalSyncZero.BI_O4_Proposal_NRC_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_NRC_TGS__c = 10;
            proposalSyncZero.BI_O4_Proposal_MRR_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_MRR_TGS__c = 10;
            proposalSyncZero.BI_O4_Proposal_MRC_TEF__c = 10;
            proposalSyncZero.BI_O4_Proposal_MRC_TGS__c = 15;
            proposalSyncZero.BI_O4_URL_to_OneDrive__c = 'y';
    
            proposalSync.add(proposalSyncZero);
    
    
            Quote proposalSyncOne = new Quote();
            proposalSyncOne.RecordTypeId = rT.get('BI_O4_Proposal_Ready');
            proposalSyncOne.Name = 'Test';
            proposalSyncOne.OpportunityId = opp.Id;
            proposalSyncOne.BI_O4_GSE__c = 'TestUser UserTest';
            proposalSyncOne.BI_O4_USER_GSE__c = UserInfo.getUserId();
            proposalSyncOne.BI_O4_Proposal_FCV_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_FCV_TGS__c = 10;
            proposalSyncOne.BI_O4_Proposal_iCOST_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_iCOST_TGS__c = 10;
            proposalSyncOne.BI_O4_Proposal_iCAPEX_TGS__c = 10;
            proposalSyncOne.BI_O4_Proposal_iCAPEX_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_NRR_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_NRR_TGS__c = 10;
            proposalSyncOne.BI_O4_Proposal_NRC_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_NRC_TGS__c = 10;
            proposalSyncOne.BI_O4_Proposal_MRR_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_MRR_TGS__c = 10;
            proposalSyncOne.BI_O4_Proposal_MRC_TEF__c = 10;
            proposalSyncOne.BI_O4_Proposal_MRC_TGS__c = 15;
            proposalSyncOne.BI_O4_URL_to_OneDrive__c = 'y';
    
            proposalSync.add(proposalSyncOne);
    
            Quote proposalSyncTwo = new Quote();
            proposalSyncTwo.RecordTypeId = rT.get('BI_O4_Proposal_Ready');
            proposalSyncTwo.Name = 'Test2';
            proposalSyncTwo.OpportunityId = opp.Id;
            proposalSyncTwo.BI_O4_GSE__c = 'TestUser UserTest';
            proposalSyncTwo.BI_O4_Proposal_FCV_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_FCV_TGS__c = 20;
            proposalSyncTwo.BI_O4_Proposal_iCOST_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_iCOST_TGS__c = 20;
            proposalSyncTwo.BI_O4_Proposal_iCAPEX_TGS__c = 20;
            proposalSyncTwo.BI_O4_Proposal_iCAPEX_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_NRR_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_NRR_TGS__c = 20;
            proposalSyncTwo.BI_O4_Proposal_NRC_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_NRC_TGS__c = 20;
            proposalSyncTwo.BI_O4_Proposal_MRR_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_MRR_TGS__c = 20;
            proposalSyncTwo.BI_O4_Proposal_MRC_TEF__c = 20;
            proposalSyncTwo.BI_O4_Proposal_MRC_TGS__c = 25;
            proposalSyncTwo.BI_O4_URL_to_OneDrive__c = 'y';
    
            proposalSync.add(proposalSyncTwo);
    
            insert proposalSync;
    
    
            Set <Id> proposalSyncOneSet = new Set <Id>();
            proposalSyncOneSet.add (proposalSyncOne.ID);
    
            Set <Id> proposalSyncTwoSet = new Set <Id>();
            proposalSyncTwoSet.add (proposalSyncTwo.ID);
    

            List <BI_O4_Approvals__c> approvalInsert = new List <BI_O4_Approvals__c> ();
    
            BI_O4_Approvals__c approvalGo = new BI_O4_Approvals__c();
            approvalGo.RecordTypeId = rT.get('BI_O4_Gate_3_Closed');
            approvalGo.BI_O4_Opportunity__c = opp.ID;
            approvalGo.BI_O4_Proposal__c = proposalSyncOne.ID;
            approvalGo.BI_O4_Financial_Gate_Approved_by__c = 'DRB';
            approvalGo.BI_O4_Gate_status__c = 'GO';
            approvalGo.BI_O4_FCV__c = 5; 
            approvalGo.BI_O4_FCV_Off_Net__c = 5; 
            approvalGo.BI_O4_NRR__c = 5; 
            approvalGo.BI_O4_NRR_TGS__c = 5;
            approvalGo.BI_O4_MRR_TEF__c = 5; 
            approvalGo.BI_O4_MRR_TGS__c = 5;
            approvalGo.BI_O4_NRC_TEF__c = 5; 
            approvalGo.BI_O4_NRC_TGS__c = 5;
            approvalGo.BI_O4_MRC_TEF__c = 5; 
            approvalGo.BI_O4_MRC_TGS__c = 5;
            approvalGo.BI_O4_iCOST_TGS__c = 5; 
            approvalGo.BI_O4_Incremental_Cost__c = 5;
            approvalGo.BI_O4_CAPEX__c = 5; 
            approvalGo.BI_O4_CAPEX_TGS__c = 5;
    
            approvalInsert.add(approvalGo);
    
    
            BI_O4_Approvals__c approvalPending = new BI_O4_Approvals__c();
            approvalPending.RecordTypeId = rT.get('BI_O4_Gate_3_Closed');
            approvalPending.BI_O4_Opportunity__c = opp.ID;
            approvalPending.BI_O4_Proposal__c = proposalSyncTwo.ID;
            approvalPending.BI_O4_Financial_Gate_Approved_by__c = 'DRB';
            approvalPending.BI_O4_Gate_status__c = 'Decision pending';
            approvalPending.BI_O4_FCV__c = 9; 
            approvalPending.BI_O4_FCV_Off_Net__c = 9; 
            approvalPending.BI_O4_NRR__c = 9; 
            approvalPending.BI_O4_NRR_TGS__c = 9;
            approvalPending.BI_O4_MRR_TEF__c = 9; 
            approvalPending.BI_O4_MRR_TGS__c = 9;
            approvalPending.BI_O4_NRC_TEF__c = 9; 
            approvalPending.BI_O4_NRC_TGS__c = 9;
            approvalPending.BI_O4_MRC_TEF__c = 9; 
            approvalPending.BI_O4_MRC_TGS__c = 9;
            approvalPending.BI_O4_iCOST_TGS__c = 9; 
            approvalPending.BI_O4_Incremental_Cost__c = 9;
            approvalPending.BI_O4_CAPEX__c = 9; 
            approvalPending.BI_O4_CAPEX_TGS__c = 9;
    

            approvalInsert.add(approvalPending);
            
            insert approvalInsert;

            BI_MigrationHelper.cleanSkippedTriggers();
                
                opp.SyncedQuoteId = proposalSyncOne.ID;
                opp.StageName = 'F5 - Solution Definition';
                update  opp;
                List <Quote> proposalSyncConOne = [SELECT ID, IsSyncing, BI_O4_Opportunity__c
                                                FROM Quote
                                                Where ID =: proposalSyncOne.ID ];
                BI_O4_Quote_Methods.migAppOppQuote(proposalSyncConOne, new Map<Id, Quote>{proposalSyncOne.Id => proposalSyncOne} );
    
                List <BI_O4_Approvals__c> approvTest = [SELECT ID, BI_O4_Gate_status__c, BI_O4_Opportunity__c, BI_O4_FCV__c, BI_O4_FCV_Off_Net__c, 
                                                    BI_O4_NRR__c, BI_O4_NRR_TGS__c,
                                                    BI_O4_MRR_TEF__c, BI_O4_MRR_TGS__c,
                                                    BI_O4_NRC_TEF__c, BI_O4_NRC_TGS__c,
                                                    BI_O4_MRC_TEF__c, BI_O4_MRC_TGS__c,
                                                    BI_O4_iCOST_TGS__c, BI_O4_Incremental_Cost__c,
                                                    BI_O4_Incremental_OIBDA__c, BI_O4_oibda_tgs__c,
                                                    BI_O4_Incremental_OIBDA_Percent__c, BI_O4_Incremental_oibda_tgs__c,
                                                    BI_O4_CAPEX__c, BI_O4_CAPEX_TGS__c,
                                                    BI_O4_Incremental_OI__c, BI_O4_iOI_TGS__c,
                                                    BI_O4_Incremental_OI_Percent__c, BI_O4_Incremental_OI_tgs__c
                                                    FROM BI_O4_Approvals__c 
                                                    WHERE BI_O4_Proposal__c IN: proposalSyncOneSet
                                                    AND BI_O4_Opportunity__c IN: oppSet];
    
            Test.startTest();
    
    
                List <Opportunity> oppTest = [SELECT ID, 
                                          BI_O4_FCV_Business_Case__c, BI_O4_BC_FCV_TGS__c, 
                                          BI_O4_Opportunity_Won_BC_NRR_TEF__c, BI_O4_BC_NRR_TGS__c,
                                          BI_O4_Opportunity_Won_BC_MRR_TEF__c, BI_O4_BC_MRR_TGS__c,
                                          BI_O4_Opportunity_Won_BC_NRC_TEF__c, BI_O4_BC_NRC_TGS__c,
                                          BI_O4_Opportunity_Won_BC_MRC_TEF__c, BI_O4_BC_MRC_TGS__c,
                                          BI_O4_Opportunity_Won_BC_iCOST_TEF__c, BI_O4_BC_iCOST_TGS__c,
                                          BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c, BI_O4_BC_iCAPEX_TGS__c
                                          FROM Opportunity
                                          WHERE ID IN: oppSet];
    
    
               System.assertEquals(approvTest[0].BI_O4_FCV__c, oppTest[0].BI_O4_FCV_Business_Case__c);
               System.assertEquals(approvTest[0].BI_O4_FCV_Off_Net__c, oppTest[0].BI_O4_BC_FCV_TGS__c);
               System.assertEquals(approvTest[0].BI_O4_NRR__c, oppTest[0].BI_O4_FCV_Business_Case__c);
               System.assertEquals(approvTest[0].BI_O4_NRR_TGS__c, oppTest[0].BI_O4_BC_NRR_TGS__c);
               System.assertEquals(approvTest[0].BI_O4_MRR_TEF__c, oppTest[0].BI_O4_Opportunity_Won_BC_NRR_TEF__c);
               System.assertEquals(approvTest[0].BI_O4_MRR_TGS__c, oppTest[0].BI_O4_BC_MRR_TGS__c);
               System.assertEquals(approvTest[0].BI_O4_NRC_TEF__c, oppTest[0].BI_O4_Opportunity_Won_BC_NRC_TEF__c);
               System.assertEquals(approvTest[0].BI_O4_NRC_TGS__c, oppTest[0].BI_O4_BC_NRC_TGS__c);
               System.assertEquals(approvTest[0].BI_O4_MRC_TEF__c, oppTest[0].BI_O4_Opportunity_Won_BC_MRC_TEF__c);
               System.assertEquals(approvTest[0].BI_O4_MRC_TGS__c, oppTest[0].BI_O4_BC_iCOST_TGS__c);
               System.assertEquals(approvTest[0].BI_O4_iCOST_TGS__c, oppTest[0].BI_O4_FCV_Business_Case__c);
               System.assertEquals(approvTest[0].BI_O4_Incremental_Cost__c, oppTest[0].BI_O4_Opportunity_Won_BC_iCOST_TEF__c);
               System.assertEquals(approvTest[0].BI_O4_CAPEX__c, oppTest[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c);
               System.assertEquals(approvTest[0].BI_O4_CAPEX_TGS__c, oppTest[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c);
    
    
    
    
    
                opp.SyncedQuoteId = proposalSyncTwo.ID;
                update  opp;
    
    
                List <Quote> proposalSyncConTwo = [SELECT ID, IsSyncing, BI_O4_Opportunity__c
                                                   FROM Quote
                                                   Where ID =: proposalSyncTwo.ID ];
    
                BI_O4_Quote_Methods.migAppOppQuote(proposalSyncConTwo, new Map<Id, Quote>{proposalSyncTwo.Id => proposalSyncTwo} );
    
    
                List <Opportunity> oppTwo = [SELECT ID, 
                                             BI_O4_FCV_Business_Case__c, BI_O4_BC_FCV_TGS__c, 
                                             BI_O4_Opportunity_Won_BC_NRR_TEF__c, BI_O4_BC_NRR_TGS__c,
                                             BI_O4_Opportunity_Won_BC_MRR_TEF__c, BI_O4_BC_MRR_TGS__c,
                                             BI_O4_Opportunity_Won_BC_NRC_TEF__c, BI_O4_BC_NRC_TGS__c,
                                             BI_O4_Opportunity_Won_BC_MRC_TEF__c, BI_O4_BC_MRC_TGS__c,
                                             BI_O4_Opportunity_Won_BC_iCOST_TEF__c, BI_O4_BC_iCOST_TGS__c,
                                             BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c, BI_O4_BC_iCAPEX_TGS__c
                                             FROM Opportunity
                                             WHERE ID IN: oppSet];
    
                
               System.assertEquals(oppTwo[0].BI_O4_FCV_Business_Case__c,0);
               System.assertEquals(oppTwo[0].BI_O4_BC_FCV_TGS__c,0);
               System.assertEquals(oppTwo[0].BI_O4_FCV_Business_Case__c,0);
               System.assertEquals(oppTwo[0].BI_O4_BC_NRR_TGS__c,0);
               System.assertEquals(oppTwo[0].BI_O4_Opportunity_Won_BC_NRR_TEF__c,0);
               System.assertEquals(oppTwo[0].BI_O4_BC_MRR_TGS__c,0);
               System.assertEquals(oppTwo[0].BI_O4_Opportunity_Won_BC_NRC_TEF__c,0);
               System.assertEquals(oppTwo[0].BI_O4_BC_NRC_TGS__c,0);
               System.assertEquals(oppTwo[0].BI_O4_Opportunity_Won_BC_MRC_TEF__c,0);
               System.assertEquals(oppTwo[0].BI_O4_BC_iCOST_TGS__c,0);
               System.assertEquals(oppTwo[0].BI_O4_FCV_Business_Case__c,0);
               System.assertEquals(oppTwo[0].BI_O4_Opportunity_Won_BC_iCOST_TEF__c,0);
               System.assertEquals(oppTwo[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c,0);
               System.assertEquals(oppTwo[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c,0);
    
    
    
                opp.SyncedQuoteId = proposalSyncZero.ID;
                update  opp;
    
                List <Quote> proposalSyncConZero = [SELECT ID, IsSyncing, BI_O4_Opportunity__c
                                                    FROM Quote
                                                    Where ID =: proposalSyncZero.ID ];
                BI_O4_Quote_Methods.migAppOppQuote(proposalSyncConZero, new Map<Id, Quote>{proposalSyncZero.Id => proposalSyncZero} );
    
                 List <Opportunity> oppZero = [SELECT ID, 
                                               BI_O4_FCV_Business_Case__c, BI_O4_BC_FCV_TGS__c, 
                                               BI_O4_Opportunity_Won_BC_NRR_TEF__c, BI_O4_BC_NRR_TGS__c,
                                               BI_O4_Opportunity_Won_BC_MRR_TEF__c, BI_O4_BC_MRR_TGS__c,
                                               BI_O4_Opportunity_Won_BC_NRC_TEF__c, BI_O4_BC_NRC_TGS__c,
                                               BI_O4_Opportunity_Won_BC_MRC_TEF__c, BI_O4_BC_MRC_TGS__c,
                                               BI_O4_Opportunity_Won_BC_iCOST_TEF__c, BI_O4_BC_iCOST_TGS__c,
                                               BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c, BI_O4_BC_iCAPEX_TGS__c
                                               FROM Opportunity
                                               WHERE ID IN: oppSet];
    
               System.assertEquals(oppZero[0].BI_O4_FCV_Business_Case__c,0);
               System.assertEquals(oppZero[0].BI_O4_BC_FCV_TGS__c,0);
               System.assertEquals(oppZero[0].BI_O4_FCV_Business_Case__c,0);
               System.assertEquals(oppZero[0].BI_O4_BC_NRR_TGS__c,0);
               System.assertEquals(oppZero[0].BI_O4_Opportunity_Won_BC_NRR_TEF__c,0);
               System.assertEquals(oppZero[0].BI_O4_BC_MRR_TGS__c,0);
               System.assertEquals(oppZero[0].BI_O4_Opportunity_Won_BC_NRC_TEF__c,0);
               System.assertEquals(oppZero[0].BI_O4_BC_NRC_TGS__c,0);
               System.assertEquals(oppZero[0].BI_O4_Opportunity_Won_BC_MRC_TEF__c,0);
               System.assertEquals(oppZero[0].BI_O4_BC_iCOST_TGS__c,0);
               System.assertEquals(oppZero[0].BI_O4_FCV_Business_Case__c,0);
               System.assertEquals(oppZero[0].BI_O4_Opportunity_Won_BC_iCOST_TEF__c,0);
               System.assertEquals(oppZero[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c,0);
               System.assertEquals(oppZero[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c,0);
    
             Test.stopTest();
    
        }                                                  
    }
                                                           
                                                           
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Test method to manage the code coverage for BI_O4_Quote_Methods.selectQuoteSkipGatesPositivo_Test.   
    History:
    Action:  
    
    <Date>                  <Author>                <Change Description>
    03/10/2017             Gawron, Julián           Initial Version
    04/10/2017             Gawron, Julián           Adding DataLoad
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    static testMethod void selectQuoteSkipGatesPositivo_Test() {

       TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
       if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
    
           userTGS.TGS_Is_BI_EN__c = true;
           userTGS.TGS_Is_TGS__c = false;
    
           if(userTGS.Id != null){
               update userTGS;
           }
           else{
               insert userTGS;
           }
       }


       List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
              
       List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);

       List <Opportunity> lst_opp =BI_DataLoad.loadOpportunitiesforButton(1, lst_acc[0].Id);


        
       /* Account acc = new Account(Name = 'test',
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'test',
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test',
                                  BI_Subsegment_local__c = 'Top 1'
                                  );
        insert acc;

        Opportunity opp = new Opportunity(Name = 'Prueba Opp',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          BI_Ciclo_ventas__c = Label.BI_Completo  
                                          );

        insert opp;*/

        Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id);
        insert quote;


        quote.BI_O4_Skip_Gates__c = true;
        try{
            update quote;
            System.assert(false, 'No debe actualizar');   
        }catch (exception exc){}


        System.runAs(new User(Id = UserInfo.getUserId()))
        {
           PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'BI_O4_Skip_Gates_and_Approval'];
           PermissionSetAssignment perm = new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = ps.Id);
           insert perm;
        }

        Test.startTest();
            System.debug('selectQuoteSkipGates_Test userInfo' + UserInfo.getUserId());
            quote.BI_O4_Skip_Gates__c = true;          
            update quote;


        Quote quote2 = [SELECT Id, BI_O4_SkipGates_Comment__c FROM Quote WHERE Id = :quote.Id];
        System.assertNotEquals(quote.BI_O4_SkipGates_Comment__c, quote2.BI_O4_SkipGates_Comment__c);


        quote.BI_O4_Skip_Gates__c = false;
        try{
            update quote; 
        }catch (exception exc){}
            

        Test.stopTest();
    }                                               
                                                           
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Test method to manage the code coverage for BI_O4_QuoteMethods.fillBusinessFields.
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    24/10/2017                      Oscar Bartolo               Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void fillBusinessFieldsTEST() {
		Profile prof = [SELECT Id FROM Profile WHERE Name IN ('BI_O4_TGS_Standard') LIMIT 1];

        User u = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Ejecutivo de Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');

        insert u;

        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

        List <Recordtype> rType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_Oportunidad_Regional','BI_Ciclo_completo', 'BI_O4_Proposal_Ready', 
                                                                                  'BI_O4_Gate_3_Closed', 'BI_O4_Organic_Growth', 'TGS_Legal_Entity')];

        Map <String, Id> rT = new Map <String, Id>();

         for ( Recordtype r : rType){

            rT.put (r.DeveloperName , r.Id);
         }


        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'Test2',
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'TEST2',
                                  BI_Subsegment_Regional__c = 'Corporate',
                                  BI_Territory__c = 'TEST2',
                                  BI_Subsegment_local__c = 'Top 1',
                                  RecordTypeId = rT.get('TGS_Legal_Entity')
                                  );    
        lst_acc1.add(acc1);
        System.runAs(usu){

            TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
            if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
    
                userTGS.TGS_Is_BI_EN__c = true;
                userTGS.TGS_Is_TGS__c = false;
    
                if(userTGS.Id != null){
                    update userTGS;
                }
                else{
                    insert userTGS;
                }
            }

            insert lst_acc1;
            
    
            List<Account> lst_acc = new List<Account>();       
            Account acc = new Account(Name = 'Test2',
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Segment__c = 'Test',
                                      BI_Subsegment_Regional__c = 'Corporate',
                                      BI_Territory__c = 'TEST',
                                      BI_Subsegment_local__c = 'Top 1',
                                      ParentId = lst_acc1[0].Id
                                      );    
            lst_acc.add(acc);
            insert lst_acc;
            
            Opportunity opp = new Opportunity(Name = 'Test2' + system.now(),
                                  CloseDate = Date.today(),
                                  StageName = Label.BI_F6Preoportunidad,
                                  AccountId = lst_acc[0].Id,
                                  RecordTypeId = rT.get('BI_Oportunidad_Regional')
                                 );    
            
            insert opp;
            
            OpportunityTeamMember teamMember = new OpportunityTeamMember ();
            teamMember.TeamMemberRole = 'GSE';
            teamMember.User = usu;
            teamMember.Opportunity = opp;
            teamMember.OpportunityId = opp.ID;
            teamMember.UserId = u.Id;
    
            insert teamMember;
			
            Test.startTest();
            Quote proposal = new Quote();
            proposal.RecordTypeId = rT.get('BI_O4_Proposal_Ready');
            proposal.Name = 'Test';
            proposal.OpportunityId = opp.Id;
            proposal.BI_O4_GSE__c = 'TestUser UserTest';
            proposal.BI_O4_USER_GSE__c = UserInfo.getUserId();
            proposal.BI_O4_URL_to_OneDrive__c = 'y';
            proposal.BI_O4_Business_Case_Output__c = '23.25; 59.39; 98.21; 1,200.68; 99; 1.125; 9,863.59; 93.36; 63.96; 369.3; 11; 983.69; 5,840.956955; 853.5; 11/01/2016; Datos de test';


            insert proposal;

            Test.stopTest();
        }
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Almirón
    Company:       Aborda
    Description:   Test method to manage the code coverage for BI_O4_QuoteMethods.userSkipGates.
            
    History:
    
    Action:  
    
    <Date>                  <Author>                       <Change Description>
    10/07/2017              Javier Almirón García          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    static testMethod void userSkipGates() {      
        Profile prof = [SELECT Id FROM Profile WHERE Name IN ('BI_O4_TGS_Standard') LIMIT 1];

         User u = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Ejecutivo de Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
         insert u;

        BI_O4_Skip_Gates_Users__c cs = new BI_O4_Skip_Gates_Users__c();
        cs.Name = 'TestCustomSetting';
        cs.BI_O4_Id_Skip_Gates__c = u.Id;
        insert cs;

        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' Limit 1];

        List <Recordtype> rType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_Oportunidad_Regional','BI_Ciclo_completo', 'BI_O4_Working_on_proposal', 'BI_O4_Gate_3_Closed', 'BI_O4_Organic_Growth', 'TGS_Legal_Entity')];

        Map <String, Id> rT = new Map <String, Id>();

         for ( Recordtype r : rType){
            rT.put (r.DeveloperName , r.Id);
         }

        Account acc1 = new Account(Name = 'Test2',
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'TEST2',
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test',
                                  BI_Subsegment_local__c = 'Top 1',
                                  RecordTypeId = rT.get('TGS_Legal_Entity')
                                  );    

            //BI_MigrationHelper.skipAllTriggers();
            insert acc1;

            AccountShare atm = new AccountShare (
                AccountId = acc1.Id, 
                AccountAccessLevel = 'Edit', 
                OpportunityAccessLevel = 'Edit', 
                UserOrGroupId = u.Id
                );

            insert atm;

            Opportunity opp = new Opportunity(Name = 'Test2' + system.now(),
                                              CloseDate = Date.today(),
                                              StageName = Label.BI_F6Preoportunidad,
                                              AccountId = acc1.Id,
                                              RecordTypeId = rT.get('BI_Oportunidad_Regional')
                                              );    
            Test.startTest();
            insert opp;

            //BI_MigrationHelper.cleanSkippedTriggers();

        System.runAs(u){
            TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
            if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
                userTGS.TGS_Is_BI_EN__c = true;
                userTGS.TGS_Is_TGS__c = false;
                if(userTGS.Id != null){
                    update userTGS;
                }
                else{
                    insert userTGS;
                }
             }

            Quote proposalSyncZero = new Quote();
            proposalSyncZero.RecordTypeId = rT.get('BI_O4_Working_on_proposal');
            proposalSyncZero.Name = 'Test';
            proposalSyncZero.OpportunityId = opp.Id;
            insert proposalSyncZero;

            Quote quote = [SELECT Id, BI_O4_Skip_Gates__c FROM Quote WHERE Id = :proposalSyncZero.Id];

            System.assertEquals(quote.BI_O4_Skip_Gates__c , true);

            Test.stopTest();
        }

    }

}