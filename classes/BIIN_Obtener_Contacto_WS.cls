public class BIIN_Obtener_Contacto_WS extends BIIN_UNICA_Base
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jose María Martín Castaño
    Company:       Deloitte
    Description:   Clase para obtener Contacto Final de RoD.

    History:

    <Date>           <Author>                                       <Description>
    10/12/2015       Jose María Martín Castaño                      Initial version
    12/05/2016       José Luis González Beltrán                     Adapt to UNICA interface
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public ContactSalida getContact(String authorizationToken, String name, String lastName, String email, String accountId)
    {
        try {
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            // Get account
            Account account = getAccount(accountId);

            // Callout URL
            String url = 'callout:BIIN_WS_UNICA/accountresources/v1/contacts';
            url = BIIN_UNICA_Utils.addParameter(url, 'customerId', account.BI_Validador_Fiscal__c);
            url = BIIN_UNICA_Utils.addParameter(url, 'accountId', account.Name);
            url = BIIN_UNICA_Utils.addParameter(url, 'givenName', name);
            url = BIIN_UNICA_Utils.addParameter(url, 'familyName', lastName);
            url = BIIN_UNICA_Utils.addParameter(url, 'email', email);

            // Prepare Request
            HttpRequest req = getRequest(url, 'GET');

            // Get response
            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.ContactsListType.class);
            system.debug(' BIIN_Obtener_Contacto_WS.getContact | La salida del servicio es (response): ' + response);
            BIIN_UNICA_Pojos.ContactsListType contactList = (BIIN_UNICA_Pojos.ContactsListType) response.responseObject;
            system.debug(' BIIN_Obtener_Contacto_WS.getContact | Lista de Contactos disponibles (contactList): ' + contactList);

            return getSalida(contactList);

        } catch(Exception e) {
            system.debug ('SITE-ERROR: ' + e);
            return null;
        }
    }

    public ContactSalida getSalida(BIIN_UNICA_Pojos.ContactsListType contactList)
    {
        ContactSalida salida = new ContactSalida();
        Outputs outputs      = new Outputs();
        outputs.output       = new List<Output>();
        Output output;
        for (BIIN_UNICA_Pojos.ContactType contact : contactList.contacts) {
            // Read additional fields
            Map<String, String> additionalData = BIIN_UNICA_Utils.additionalDataToMap(contact.contactDetails.additionalData);
            output = new Output();
            output.remedyLoginId = contact.contactId;
            output.firstName     = contact.contactDetails.name.givenName;
            output.lastName      = contact.contactDetails.name.familyName;

            for (BIIN_UNICA_Pojos.ContactingModeType contactingMode : contact.contactDetails.contactingModes) {
                if(contactingMode.contactMode == 'email') {
                    output.internetEmail = contactingMode.details;
                } else if(contactingMode.contactMode == 'phone') {
                    output.phone = contactingMode.details;
                } else if(contactingMode.contactMode == 'mobile') {
                    output.mobile = contactingMode.details;
                }
            }

            output.Company       = additionalData.get('company');
            output.Country       = '';
            output.code          = null;
            output.message       = '';
            outputs.output.add(output);
        }

        salida.outputs = outputs;

        system.debug(' BIIN_Obtener_Contacto_WS.getSalida | Objeto salida final del servicio (salida): ' + salida);

        return salida;
    }

    //##############POJOS
    public class ContactSalida
    {
        public Outputs outputs;
    }

    public class Outputs
    {
        public List<Output> output;
    }

    public class Output
    {
        public String remedyLoginId;
        public String firstName;
        public String lastName;
        public String internetEmail;
        public String phone;
        public String mobile;
        public String Company;
        public String Country;
        public String code;
        public String message;
    }


    public String crearContacto(String strName, String strLastName, String strEmail, String strPhone, String strMobile, String strAccountId) {
        try {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'crearContacto' + '   <<<<<<<<<<\n-=#=-\n');

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            String strURL        = 'callout:BIIN_WS_UNICA/accountresources/v1/contacts';
            Account objAccount   = getAccount(strAccountId);

            //Armado del JSON
            BIIN_UNICA_Pojos.ContactDetailsType objContactDetailsType = new BIIN_UNICA_Pojos.ContactDetailsType();
            objContactDetailsType.name = new BIIN_UNICA_Pojos.NameDetailsType();
            objContactDetailsType.name.fullName = (strName + ' ' + strLastName).trim();
            objContactDetailsType.name.givenName = strName;
            objContactDetailsType.name.familyName = strLastName;
            objContactDetailsType.name.displayName = (strName + ' ' + strLastName).trim();
            
            objContactDetailsType.customers = new List<String>{objAccount.BI_Validador_Fiscal__c};
            objContactDetailsType.contactStatus = BIIN_UNICA_Pojos.ContactStatusType.active;
            objContactDetailsType.contactingModes = new List<BIIN_UNICA_Pojos.ContactingModeType>();
            
            BIIN_UNICA_Pojos.ContactingModeType objContactingModeTypeEmail = new BIIN_UNICA_Pojos.ContactingModeType();
            objContactingModeTypeEmail.contactMode = 'email';
            objContactingModeTypeEmail.details = strEmail;
            objContactDetailsType.contactingModes.add(objContactingModeTypeEmail);

            BIIN_UNICA_Pojos.ContactingModeType objContactingModeTypePhone = new BIIN_UNICA_Pojos.ContactingModeType();
            objContactingModeTypePhone.contactMode = 'phone';
            objContactingModeTypePhone.details = strPhone;
            objContactDetailsType.contactingModes.add(objContactingModeTypePhone);
            
            BIIN_UNICA_Pojos.ContactingModeType objContactingModeTypeMobile = new BIIN_UNICA_Pojos.ContactingModeType();
            objContactingModeTypeMobile.contactMode = 'mobile';
            objContactingModeTypeMobile.details = strMobile;
            objContactDetailsType.contactingModes.add(objContactingModeTypeMobile);
            
            String strBody = '{"create":{"contactDetails":' + JSON.serialize(objContactDetailsType) + '}}';
            System.debug('\n\n-=#=-\n' + 'crearContacto - strBody' + ': ' +strBody + '\n-=#=-\n');

            HttpRequest objHttpRequest = getRequest(strURL, 'POST');
            objHttpRequest.setHeader('Content-Type', 'application/json');
            objHttpRequest.setBody(strBody);

            System.debug('\n\n-=#=-\n' + 'crearContacto - objHttpRequest' + ': ' + objHttpRequest + '\n-=#=-\n');
            
            // Get response
            BIIN_UNICA_Pojos.ResponseObject objResponseObject = getResponse(objHttpRequest, null);
            System.debug('\n\n-=#=-\n' + 'crearContacto - objResponseObject' + ': ' + objResponseObject + '\n-=#=-\n');
            
            if(objResponseObject.isResponseOK()) {
                return null;
            } else {
                if(objResponseObject.exceptionObject != null) {
                    String strResponse = objResponseObject.exceptionObject.id + ' - ' + objResponseObject.exceptionObject.text;
                    System.debug('\n\n-=#=-\n' + 'strResponse' + ': ' + strResponse + '\n-=#=-\n');
                    return strResponse;
                } else {
                    return 'Error en la creación del contacto.';
                }
            }

            return JSON.serialize(objResponseObject);
        } catch(Exception objException) {
            System.debug('\n\n-=#=-\n' + 'crearContacto - objException.getMessage()' + ': ' + objException.getMessage() + '\n-=#=-\n');
            return objException.getMessage();
        }
    }
}