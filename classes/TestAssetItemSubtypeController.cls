@isTest (SeeAllData=true)
private class TestAssetItemSubtypeController {

    static testMethod void myUnitTest() {
        
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert catalog items
        NE__Product__c prod1 = new NE__Product__c();
        NE__Product__c prod2 = new NE__Product__c();
        insert prod1;
        insert prod2;
        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt1 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod1.Id, NE__Change_Subtype__c = 'Cambio de SIM;Modificaciones upgrade', NE__Disconnect_Subtype__c = 'Suspensión Voluntaria');
        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod2.Id, NE__Change_Subtype__c = 'Cambio de SIM;Modificaciones upgrade;Traslados', NE__Disconnect_Subtype__c = 'DSubT1;DSubT2');
        insert catIt1;
        insert catIt2;
        // Insert orders
        list<NE__Order__c> ordertoins = new list<NE__Order__c>();
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        ordertoins.add(ord1);
        ordertoins.add(ord2);
        
        insert ordertoins;
        // Insert order items
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__CatalogItem__c = catIt1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__CatalogItem__c = catIt2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        //NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__CatalogItem__c = catIt2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');        
        //insert ordIt1;
        //insert ordIt2;
        //insert ordIt3;
        
        oitoins.add(ordIt1);
        oitoins.add(ordIt2);
        //oitoins.add(ordIt3);
        
        insert oitoins;
        
        Test.startTest();
        // Order type = 'ChangeOrder'
        list<String> listOrdItemsId1 = new list<String>();
        listOrdItemsId1.add(ordIt1.Id);
        listOrdItemsId1.add(ordIt2.Id);
        String result = ChangeRequestConfItem.changeRequest(listOrdItemsId1,'ChangeOrder');
        /**/
        //insert ord2;
        result = ord2.Id;
        /**/
        
        PageReference pageRef = Page.AssetItemSubtype;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', result);
        NE__Order__c newOrd = [SELECT Id, NE__OptyId__c FROM NE__Order__c WHERE Id =: result];
        ApexPages.StandardController sc = new ApexPages.StandardController(newOrd);
        AssetItemSubtypeController controller = new AssetItemSubtypeController(sc);
        
        controller.stSelected = 'Cambio de SIM';
        controller.setAllChanges();
        
        String nextPage = controller.save().getUrl();
        String oppId = newOrd.NE__OptyId__c;
        
    }
    
    static testMethod void myUnitTest2() {
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert catalog items
        NE__Product__c prod1 = new NE__Product__c();
        NE__Product__c prod2 = new NE__Product__c();
        insert prod1;
        insert prod2;
        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt1 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod1.Id, NE__Change_Subtype__c = 'Cambio de SIM;Modificaciones upgrade', NE__Disconnect_Subtype__c = 'Suspensión Voluntaria');
        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod2.Id, NE__Change_Subtype__c = 'Cambio de SIM;Modificaciones upgrade;Traslados', NE__Disconnect_Subtype__c = 'DSubT1;DSubT2');
        insert catIt1;
        insert catIt2;
        // Insert orders
        list<NE__Order__c> ordertoins = new list<NE__Order__c>();
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        ordertoins.add(ord1);
        ordertoins.add(ord2);
        
        insert ordertoins;
        // Insert order items
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        //NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__CatalogItem__c = catIt1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        //NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__CatalogItem__c = catIt2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__CatalogItem__c = catIt2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');        
        //insert ordIt1;
        //insert ordIt2;
        //insert ordIt3;
        
        //oitoins.add(ordIt1);
        //oitoins.add(ordIt2);
        oitoins.add(ordIt3);
        
        insert oitoins;
        
        Test.startTest();
        
        // Order type = 'Disconnection'
        list<String> listOrdItemsId2 = new list<String>();
        listOrdItemsId2.add(ordIt3.Id);
        String result = ChangeRequestConfItem.changeRequest(listOrdItemsId2,'Disconnection');
        /**/
        //insert ord2;
        result = ord2.Id;
        /**/
        NE__Order__c newOrd2 = [SELECT Id FROM NE__Order__c WHERE Id =: result];
        ApexPages.currentPage().getParameters().put('id', result);
        ApexPages.currentPage().getParameters().put('adminmode', 'false');
        ApexPages.StandardController sc = new ApexPages.StandardController(newOrd2);
        AssetItemSubtypeController controller = new AssetItemSubtypeController(sc);
        
        controller.stSelected = 'DSubT1';
        controller.setAllChanges();
        
        String nextPage = controller.save().getUrl();
    }
    
     static testMethod void myUnitTest3() {
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert catalog items
        NE__Product__c prod1 = new NE__Product__c();
        NE__Product__c prod2 = new NE__Product__c();
        insert prod1;
        insert prod2;
        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt1 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod1.Id, NE__Change_Subtype__c = 'Cambio de SIM;Modificaciones upgrade', NE__Disconnect_Subtype__c = 'Suspensión Voluntaria');
        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod2.Id, NE__Change_Subtype__c = 'Cambio de SIM;Modificaciones upgrade;Traslados', NE__Disconnect_Subtype__c = 'DSubT1;DSubT2');
        insert catIt1;
        insert catIt2;
        // Insert orders
        list<NE__Order__c> ordertoins = new list<NE__Order__c>();
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        ordertoins.add(ord1);
        ordertoins.add(ord2);
        
        insert ordertoins;
        // Insert order items
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        //NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__CatalogItem__c = catIt1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__CatalogItem__c = catIt2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'RFS');        
        oitoins.add(ordIt3);      
        insert oitoins;
        
        NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__Root_Order_Item__c = ordIt3.id,NE__Parent_Order_Item__c = ordIt3.id, NE__OrderId__c = ord2.Id, NE__CatalogItem__c = catIt2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        insert ordIt2;
        
        Test.startTest();
        
        // Order type = 'Disconnection'
        list<String> listOrdItemsId2 = new list<String>();
        listOrdItemsId2.add(ordIt3.Id);
        //String result = ChangeRequestConfItem.changeRequest(listOrdItemsId2,'Disconnection');
        
        //NE__Order__c newOrd2 = [SELECT Id FROM NE__Order__c WHERE Id =: result];
        String result   =   ord2.id;
        ApexPages.currentPage().getParameters().put('id', result);
        ApexPages.currentPage().getParameters().put('adminmode', 'true');
        ApexPages.StandardController sc = new ApexPages.StandardController(ord2);
        AssetItemSubtypeController controller = new AssetItemSubtypeController(sc);
        
        controller.stSelected = 'DSubT1';
        controller.setAllChanges();
        
        String nextPage = controller.save().getUrl();
    }   
}