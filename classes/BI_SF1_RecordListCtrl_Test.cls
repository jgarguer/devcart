@isTest
private class BI_SF1_RecordListCtrl_Test {

    @isTest static void lookuprecord_test(){
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
        List<Object> return_obj = BI_SF1_RecordList_Ctrl.lookUpRecords('BI_Country__c;Name', 'Account', 'Argentina', 'BI_Country__c;Mail_Propietario__c');
        System.assert(return_obj!=null);

        List<Contact> lst_cnt = BI_DataLoad.loadContacts(1, lst_acc);
        return_obj = BI_SF1_RecordList_Ctrl.lookUpRecords('BI_Country__c', 'Contact', 'Argentina', 'BI_Country__c;Account.Name');
        System.assert(return_obj!=null);
        return_obj = BI_SF1_RecordList_Ctrl.lookUpRecords('BI_Country__c', 'Account', '', '');
        System.assert(return_obj==null);
        return_obj = BI_SF1_RecordList_Ctrl.lookUpRecords('BI_Country__c', 'Contact', 'Perús', 'Account.Name');
        System.assert(return_obj==null);
    }

    @isTest static void customViews_test(){
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
        List<Object> return_obj = BI_SF1_RecordList_Ctrl.customViews('Account', 'BI_Country__c', null);
        System.assert(return_obj!=null);

        List<Contact> lst_cnt = BI_DataLoad.loadContacts(1, lst_acc);
        return_obj = BI_SF1_RecordList_Ctrl.customViews('Contact', 'BI_Country__c', null);
        System.assert(return_obj!=null);
    }

}