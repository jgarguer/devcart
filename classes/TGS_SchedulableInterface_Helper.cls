global class TGS_SchedulableInterface_Helper{

    /*-------------------------------------------------------------------------------------
     Author:         Everis
     Company:        Everis
     Description:    Método para crear Orders automáticamente.
		
	 Return: 	Devuelve la lista de IDs de los Orders creados para poder ser usada en una SOQL y recuperar los casos que se han autogenerado.

     History
     <Date>      <Author>            <Description>
     08/02/2017   Everis  			  Initial Version
	 17/02/2017   Everis  			  Reducción de 3 SOQL.
	 10/03/2017	  Everis			  Incluida la generación de Assets.
     10/03/2017	  Everis			  Se añade el parámetro "create" al método que indica si queremos crear ordenes de registro o de terminación.
	 14/03/2017	  Everis			  Actualización del campo CSUID.
	 17/03/2017	  Everis			  Quitamos el código de creación de orders de terminación.
	 21/03/2017	  Everis			  Separación entre facturación centralizada y distribuida.
	 22/03/2017	  Everis			  Cambiamos el método para que cree orders por cada Cost center.
	 04/04/2017	  Everis			  Cambio de Schedule a Queueable. Cambia el parámetro de entrada de OI a Account.
 	 17/04/2017	  Everis			  Cambio de Queueable a Batchable.
    -------------------------------------------------------------------------------------*/
    public static List<ID> generateBillingMobileLine(List<ID> newsIDS) {
        
        List<Account> news = [SELECT id, name, ParentID, Parent.name, Parent.ParentID, Parent.Parent.name, Parent.Parent.Parent.ParentID  FROM Account Where id IN : newsIDs];
        system.debug('Everis: START-generateBillingMobileLine');
        
        //ID de los distintos record types de Order y Account.
        Id rtOrdOrder = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, Constants.RECORD_TYPE_ORDER);		
        Id rtOrdAsset = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, Constants.RECORD_TYPE_ASSET);
        
        //Almacena los Order Item de "Mobile Line" para cada cost center.
        Map<ID, List<NE__OrderItem__c>> mapCostCenterOrderItems = new Map<ID, List<NE__OrderItem__c>>();
        
        //Recuperamos la información que necesitaremos de los CostCenter para crear el order.
        Map<ID, Account> mapAccount = new Map<ID, Account>();
        for(Account a : news){
            mapAccount.put(a.id,a);
        }
        
        //Recorro todos los orderItem y creo un mapa donde para cada CostCenter se almacenará una lista con sus OrderItems.
        if (!Test.isRunningTest()){ 
            for(NE__OrderItem__c oi : [SELECT NE__Billing_Account__c, TGS_CSUID1__c, NE__OrderId__c, NE__OrderId__r.NE__AccountId__c, Installation_point__c  FROM NE__OrderItem__c 
                          Where NE__Billing_Account__c IN :mapAccount.keySet() AND NE__ProdName__c = :Constants.MOBILE_LINE AND NE__OrderId__r.NE__rTypeName__c = :Constants.RECORD_TYPE_ASSET
                          AND NE__OrderId__r.NE__OrderStatus__c = :Constants.CI_STATUS_ACTIVE AND TGS_Service_status__c = :Constants.CI_TGS_SERVICE_STATUS_DEPLOYED]){
                ID costCenterID = oi.NE__Billing_Account__c;
                if (mapCostCenterOrderItems.get(costCenterID) == null) mapCostCenterOrderItems.put(costCenterID, new List<NE__OrderItem__c>());
                List<NE__OrderItem__c> listAux = mapCostCenterOrderItems.get(costCenterID);
                listAux.add(oi);
                mapCostCenterOrderItems.put(oi.NE__Billing_Account__c, listAux);
            }
        }else {
            for(NE__OrderItem__c oi : [SELECT NE__Billing_Account__c, TGS_CSUID1__c, NE__OrderId__c, NE__OrderId__r.NE__AccountId__c, Installation_point__c  FROM NE__OrderItem__c 
                          Where NE__Billing_Account__c IN :mapAccount.keySet()]){
                ID costCenterID = oi.NE__Billing_Account__c;
                if (mapCostCenterOrderItems.get(costCenterID) == null) mapCostCenterOrderItems.put(costCenterID, new List<NE__OrderItem__c>());
                List<NE__OrderItem__c> listAux = mapCostCenterOrderItems.get(costCenterID);
                listAux.add(oi);
                mapCostCenterOrderItems.put(oi.NE__Billing_Account__c, listAux);
        	}
        }

        //Recuperamos el producto, site y otros valores necesarios para el Order
        NE__Product__c prod = [Select id,name,TGS_CWP_Tier_1__c,TGS_CWP_Tier_2__c from NE__Product__c Where name = :Constants.BILLABLE_MOBILE_LINE];        
        NE__Catalog_Item__c OI_catalogItem = [SELECT Id FROM NE__Catalog_Item__c WHERE NE__Product_Name__c = :Constants.BILLABLE_MOBILE_LINE AND NE__Type__c = :Constants.PRODUCT Limit 1];
      	NE__Catalog__c catalog = [SELECT Id FROM NE__Catalog__c WHERE Name = :Constants.TGSOL_CATALOG Limit 1];
        NE__ProductFamilyProperty__c familyProp = [SELECT id FROM NE__ProductFamilyProperty__c where NE__PropId__r.name = :Constants.BILLING_MOBILE_LINE_OIA];
       
        //ID de cuenta. Para cada cuenta guardamos el Order y OI a crear con su atributo y su attachment.
        List<NE__Order__c> orders = new List<NE__Order__c>();
        Map<ID, NE__OrderItem__c> OIS = new  Map<ID, NE__OrderItem__c>();
        Map<ID, NE__Order_Item_Attribute__c> OIAS = new Map<ID, NE__Order_Item_Attribute__c>();
        Map<ID, Attachment> attachments = new  Map<ID, Attachment>();
        
        for(ID account :mapCostCenterOrderItems.keySet()){
            System.debug('Everis: account ' + account);
            String MSISDN_list = '';
            ID site = null;
                        
            for(NE__OrderItem__c oi : mapCostCenterOrderItems.get(account)){
                if(oi.TGS_CSUID1__c != null) MSISDN_list += oi.TGS_CSUID1__c+';';
                //Usaremos el primer site que encontremos.
                if (site == null && oi.Installation_point__c != null) site = oi.Installation_point__c;
            }
            
            //Creamos el nuevo Order.
            NE__Order__c order = new NE__Order__c();

			//Recuperamos la información del CostCenter desde la que extraeremos los datos que necesitamos para crear el nuevo order.            
            Account acctAux = mapAccount.get(account);
            if (acctAux != null){
                String accountName = acctAux.Parent.name;
                order.NE__AccountId__c = acctAux.ParentID;
                order.NE__ServAccId__c = acctAux.ParentID;
                order.Business_Unit__c = acctAux.ParentID;
                order.Cost_Center__c = acctAux.id;
                order.NE__BillAccId__c = acctAux.id;
                order.NE__Channel__c = Constants.CONFIGURATION_CHANNEL_BULK;
                //Por problemas de validations rules del objeto BI_Punto_de_instalacion__c externo a este desarrollo
                if (!Test.IsRunningTest()) order.Site__c = site;            
                order.RecordTypeId = rtOrdOrder;
                order.HoldingId__c = acctAux.Parent.Parent.Parent.ParentID;
                if (catalog != null) order.NE__CatalogId__c = catalog.id;
                order.NE__Configuration_Type__c = Constants.TYPE_NEW;
                order.NE__OrderStatus__c = 'Completed';
                order.NE__Type__c = Constants.IN_ORDER;
                order.NE__TotalRecurringFrequency__c = Constants.MONTHLY;
                order.NE__One_Time_Fee_Total__c = 0.0;
                order.NE__Recurring_Charge_Total__c = 0.0;
                orders.add(order);
                
                //Creamos el OI.
                NE__OrderItem__c ordItem = new NE__OrderItem__c(NE__Qty__c = 1);
                ordItem.NE__Account__c = acctAux.ParentID;
                ordItem.NE__ProdId__c = prod.ID;
                ordItem.NE__Status__c = Constants.CONFIGURATION_STATUS_ACTIVE;
                //Por problemas de validations rules del objeto BI_Punto_de_instalacion__c externo a este desarrollo
                if (!Test.IsRunningTest()) ordItem.Installation_point__c = site;
                ordItem.NE__Billing_Account__c = acctAux.id;
                ordItem.NE__Service_Account__c = acctAux.ParentID;
                ordItem.NE__Qty__c = 1;
                ordItem.NE__RecurringChargeFrequency__c = Constants.MONTHLY;
                ordItem.NE__OneTimeFeeOv__c = 0.0;
                ordItem.NE__OneTimeFeePb__c = 0.0;
                ordItem.NE__RecurringChargeOv__c = 0.0;
                ordItem.NE__RecurringChargePb__c = 0.0;
                ordItem.TGS_billing_start_date__c = DATE.today();
                ordItem.NE__Action__c = Constants.CI_ACTION_ADD;
                if (OI_catalogItem != null) ordItem.NE__CatalogItem__c = OI_catalogItem.id;
                OIS.put(acctAux.ParentID, ordItem);
                
                //Creamos el OIA.
                NE__Order_Item_Attribute__c oia = new NE__Order_Item_Attribute__c();
                oia.Name = Constants.BILLING_MOBILE_LINE_OIA;
                oia.NE__FamPropId__c = familyProp.id;
                oia.NE__Value__c = String.ValueOf(MSISDN_list.countMatches(';'));
                OIAS.put(acctAux.ParentID, oia);
                
                //Preparar attachment. (El nombre se instancia más tarde ya que necesita que se inserte primero los order.)
                Attachment att = new Attachment();
                att.Body = Blob.valueOf(MSISDN_list);
                att.Description = Constants.ATTACHMENT_DESCRIPTION;
                Attachments.put(acctAux.ParentID, att);
            }            
        }
        
        //INSERT
        insert orders;
        
        //For solo para debugear.
        List<ID> orderID = new List<ID>();
        for (NE__Order__c order : orders){
            orderID.add(order.id);
        } System.debug('Everis: Orders insertados: ' + orderID);
        
        //Necesario para recuperar los name, ya que se autogeneran tras el insert.
        orders = [Select id, name, NE__AccountId__c, NE__BillAccId__c From NE__Order__c Where id in :orders];
        
        List<NE__OrderItem__c> auxOI = new List<NE__OrderItem__c>();
        List<Attachment> attchs = new List<Attachment>();
        for(NE__Order__c ord : orders){
            NE__OrderItem__c oi =  OIS.get(ord.NE__AccountId__c);
            Attachment att = Attachments.get(ord.NE__AccountId__c);
            oi.NE__OrderId__c = ord.Id;
            att.ParentId = ord.id;
            att.Name = Constants.ATTACHMENT_NAME+ord.name+Constants.ATTACHMENT_EXTENSION;
            auxOI.add(oi);
            attchs.add(att);
        }
        insert auxOI;
        insert attchs;
        List<NE__Order_Item_Attribute__c> auxOIA = new List<NE__Order_Item_Attribute__c>();
        for(NE__OrderItem__c ordIt : auxOI){
            NE__Order_Item_Attribute__c oia =  OIAS.get(ordIt.NE__Account__c);
            oia.NE__Order_Item__c = ordIt.Id;
            auxOIA.add(oia);
        }
        insert auxOIA;
        
        List<ID> newOrderIds = new List<ID>();
        for (NE__Order__c o : orders){
            newOrderIds.add(o.id);
        }       
        
        //Generación de Asset
        //Insert Asset
        Map<Id,NE__Asset__c> assets = new Map<Id,NE__Asset__c>();
        for (NE__Order__c forO: orders) {
            assets.put(forO.Id,New NE__Asset__c ());
            assets.get(forO.Id).NE__AccountId__c = forO.NE__AccountId__c;
            assets.get(forO.Id).NE__BillAccId__c = forO.NE__BillAccId__c;
            assets.get(forO.Id).NE__ServAccId__c = forO.NE__AccountId__c;
            assets.get(forO.Id).NE__Status__c  = Constants.CI_STATUS_ACTIVE;
        }
        insert assets.values();
        
        Map<Id,Case> casos = new Map<Id,Case>();
        //ordeno casos por Order
        for (case forC: [select id, CaseNumber, owner.Name, order__c, type, TGS_Service__c, Asset__c from Case where order__c in :newOrderIds]) {
            casos.put(forC.Order__c,forC);
        } 
        
        //Generación de AssetsOrders
        Map<Id,NE__Order__c> orderAssets = new Map<Id,NE__Order__c> ();
        for (NE__Order__c forO: orders) {            
            orderAssets.put(forO.Id,forO.clone());
            orderAssets.get(forO.Id).recordTypeId=rtOrdAsset;
			orderAssets.get(forO.Id).NE__OrderStatus__c = Constants.CI_STATUS_ACTIVE;            
            if(casos.get(forO.Id) != null) orderAssets.get(forO.Id).Case__c = casos.get(forO.Id).Id;
            if(assets.get(forO.Id) != null){
                orderAssets.get(forO.Id).NE__Asset_Configuration__c = assets.get(forO.ID).Id;
            	orderAssets.get(forO.Id).NE__Asset__c = assets.get(forO.ID).Id;
            }
        }
        insert orderAssets.values();
        
        //insert de asset order item
        Map<Id,NE__OrderItem__c> assetItemToInsert = new Map<Id,NE__OrderItem__c> ();
         for (NE__OrderItem__c forOI: auxOI) {
             assetItemToInsert.put(forOI.Id,forOI.clone());
             assetItemToInsert.get(forOI.Id).NE__OrderId__c = orderAssets.get(forOI.NE__OrderId__c).ID;
             assetItemToInsert.get(forOI.Id).NE__Status__c = Constants.CI_STATUS_ACTIVE;
             assetItemToInsert.get(forOI.Id).TGS_Service_Status__c = Constants.CI_TGS_SERVICE_STATUS_RECEIVED;
             assetItemToInsert.get(forOI.Id).NE__Asset_Item_Account__c = forOI.NE__Account__c;
             assetItemToInsert.get(forOI.Id).NE__Billing_Account_Asset_Item__c = forOI.NE__Billing_Account__c;
             assetItemToInsert.get(forOI.Id).NE__Service_Account_Asset_Item__c = forOI.NE__Service_Account__c;
             assetItemToInsert.get(forOI.Id).NE__AssetItemEnterpriseId__c = forOI.Id;
             assetItemToInsert.get(forOI.Id).NE__Action__c = Constants.CI_ACTION_ADD;
             assetItemToInsert.get(forOI.Id).TGS_billing_start_date__c = DATE.today();
         }
        
         insert assetItemToInsert.values();
          
        //insert asset configuration attributes
        List<NE__Order_Item_Attribute__c> assetattributesToInsert =  new List<NE__Order_Item_Attribute__c>();
        
        for (NE__Order_Item_Attribute__c forAA: auxOIA ) {
            NE__Order_Item_Attribute__c toAdd = forAA.clone();
            toAdd.NE__Order_Item__c = assetItemToInsert.get(forAA.NE__Order_Item__c).Id;
            assetattributesToInsert.add(toAdd);
        }
        
        insert assetattributesToInsert;
        
        //Necesario ya que no se autogeneran los casos desde los test.
        if (TEST.isRunningTest()){
            Case c = new Case(status = Constants.CASE_STATUS_ASSIGNED);
            insert c;
            casos.put(c.id, c);
        }
        
        Contact c;
        if (!TEST.isRunningTest()){
        	c = [Select id from Contact where Name='Dummy_Contact' Limit 1];
        }
        //Actualización de casos         
        for (Case forC: casos.values()) {
			forC.status = Constants.CASE_STATUS_CLOSED;
            if (!TEST.isRunningTest()) forC.contactID= c.id;
            forC.TGS_Product_Tier_1__c = Constants.ENTERPRISE_MANAGED_MOBILITY;
            forC.TGS_Product_Tier_2__c = Constants.GLOBAL_MANAGEMENT_ELITE;
            forC.TGS_Product_Tier_3__c = constants.BILLABLE_MOBILE_LINE;
            if (orderAssets.get(forC.order__c) != null) forC.Asset__c = orderAssets.get(forC.order__c).Id;
            forC.TGS_Resolve_Date__c = DATE.today();
        }
        //Necesario ya que no podemos cambiar el estado del caso debido a validations rules sin darle al botón de case edit.
        if(!Test.isRunningTest()) update casos.values();
        
        //Debe estar después para que el campo Asset_c esté informado.
        //No se ejecutará desde los test, ya que los casos no se autogeneran y faltaría información para el método.
        for (Case forC: casos.values()) {
            if (!Test.isRunningTest())
                TGS_fill_CSUID.csuid(forC, null);            
        }        
        system.debug('Everis: END-generateBillingMobileLine. Orders insertados: ' + newOrderIds);  
        return newOrderIds;
    }
}