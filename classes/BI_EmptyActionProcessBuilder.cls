public class BI_EmptyActionProcessBuilder {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro Hernando Gavilán
    Company:       Telefónica Global Technology
    Description:   Invocable Method for the Process Builder "BI Oportunidad FCV Elevado Email Alert". The method doesn't do anything, it's just an empty action. 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    15/06/2015                      Álvaro Hernando             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @InvocableMethod
    public static void emtpyAction(List<Id> opptyIds){


    }
    
}