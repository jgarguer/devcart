@isTest
private class BI_InvoicesTreeViewControllerTest {
	static Account testAccount;
	
	@isTest static void controllerTest() {
		generateData();
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		PageReference pg = Page.BI_InvoicesTreeView;
		pg.getParameters().put('id', testAccount.id);
		Test.setCurrentPage(pg); 
		
		BI_InvoicesTreeViewController cont = new BI_InvoicesTreeViewController();

		system.assertEquals('{0,number,########000}', cont.getDynamicFormatString());

		List<BI_InvoicesTreeViewController.Level4> levelList = BI_InvoicesTreeViewController.getDetails(testAccount.id, '201305', 'Rama2', 'Fam2', 'Fam22', 'FCIC');

		for (BI_InvoicesTreeViewController.Level4 l : levelList) {
			system.assertEquals('Conc111', l.codigoConcepto);
			system.assertEquals('Desc111', l.descripcionConcepto);
		}


	}

	private static void generateData() {
		// 20/09/2017        Angel F. Santaliestra Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
		testAccount = new Account(Name='TestAccount',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
		// END 20/09/2017        Angel F. Santaliestra
		insert testAccount;

		List<BI_Cobranza__c> items = new List<BI_Cobranza__c>();

		String year = '2013';

		for (Integer i = 0; i < 12; i++) {
			String month = ''+i;

			if (i < 9) {
				month = '0'+i;
			}

			BI_Cobranza__c bi = new BI_Cobranza__c();
			bi.BI_YearMonth__c = year+month;
			bi.BI_Clientes__c = testAccount.id;
			bi.Active__c = true;
			bi.Aggregation_Level__c = 'Level0';
			bi.BI_Tipo_Facturacion__c = 'FCIC';


			String aggType;
			if (i < 9) {
				aggType = '4-13M';
				bi.BI_Rama__c = 'Rama2';
				bi.BI_Familia_1__c = 'Fam2';
				bi.BI_Familia_2__c = 'Fam22';
			} else {
				aggType = '1-3M';
				bi.BI_Rama__c = 'Rama1';
				bi.BI_Familia_1__c = 'Fam1';
				bi.BI_Familia_2__c = 'Fam11';
			}
			bi.Aggregation_Type__c = aggType;
			items.add(bi);
		}

		for (Integer i = 0; i < 12; i++) {
			String month = ''+i;

			if (i < 9) {
				month = '0'+i;
			}

			BI_Cobranza__c bi = new BI_Cobranza__c();
			bi.BI_YearMonth__c = year+month;
			bi.BI_Clientes__c = testAccount.id;
			bi.Active__c = true;
			bi.Aggregation_Level__c = 'Level1';
			bi.BI_Tipo_Facturacion__c = 'FCIC';

			String aggType;
			if (i < 9) {
				aggType = '4-13M';
				bi.BI_Rama__c = 'Rama2';
				bi.BI_Familia_1__c = 'Fam2';
				bi.BI_Familia_2__c = 'Fam22';
			} else {
				aggType = '1-3M';
				bi.BI_Rama__c = 'Rama1';
				bi.BI_Familia_1__c = 'Fam1';
				bi.BI_Familia_2__c = 'Fam11';
			}
			bi.Aggregation_Type__c = aggType;
			items.add(bi);
		}

		for (Integer i = 0; i < 12; i++) {
			String month = ''+i;

			if (i < 9) {
				month = '0'+i;
			}

			BI_Cobranza__c bi = new BI_Cobranza__c();
			bi.BI_YearMonth__c = year+month;
			bi.BI_Clientes__c = testAccount.id;
			bi.Active__c = true;
			bi.Aggregation_Level__c = 'Level2';
			bi.BI_Tipo_Facturacion__c = 'FCIC';

			String aggType;
			if (i < 9) {
				aggType = '4-13M';
				bi.BI_Rama__c = 'Rama2';
				bi.BI_Familia_1__c = 'Fam2';
				bi.BI_Familia_2__c = 'Fam22';
			} else {
				aggType = '1-3M';
				bi.BI_Rama__c = 'Rama1';
				bi.BI_Familia_1__c = 'Fam1';
				bi.BI_Familia_2__c = 'Fam11';
			}
			bi.Aggregation_Type__c = aggType;
			items.add(bi);
		}

		for (Integer i = 0; i < 12; i++) {
			String month = ''+i;

			if (i < 9) {
				month = '0'+i;
			}

			BI_Cobranza__c bi = new BI_Cobranza__c();
			bi.BI_YearMonth__c = year+month;
			bi.BI_Clientes__c = testAccount.id;
			bi.Active__c = true;
			bi.Aggregation_Level__c = 'Level3';
			bi.BI_Tipo_Facturacion__c = 'FCIC';

			String aggType;
			if (i < 9) {
				aggType = '4-13M';
				bi.BI_Rama__c = 'Rama2';
				bi.BI_Familia_1__c = 'Fam2';
				bi.BI_Familia_2__c = 'Fam22';
			} else {
				aggType = '1-3M';
				bi.BI_Rama__c = 'Rama1';
				bi.BI_Familia_1__c = 'Fam1';
				bi.BI_Familia_2__c = 'Fam11';
			}
			bi.Aggregation_Type__c = aggType;
			items.add(bi);
		}

		for (Integer i = 0; i < 12; i++) {
			String month = ''+i;

			if (i < 9) {
				month = '0'+i;
			}

			BI_Cobranza__c bi = new BI_Cobranza__c();
			bi.BI_YearMonth__c = year+month;
			bi.BI_Clientes__c = testAccount.id;
			bi.Active__c = true;
			bi.Aggregation_Level__c = 'Level4';
			bi.BI_Tipo_Facturacion__c = 'FCIC';
			bi.BI_Codigo_concepto__c = 'Conc111';
			bi.BI_Descripcion_concepto__c = 'Desc111';

			String aggType;
			if (i < 9) {
				aggType = '4-13M';
				bi.BI_Rama__c = 'Rama2';
				bi.BI_Familia_1__c = 'Fam2';
				bi.BI_Familia_2__c = 'Fam22';
			} else {
				aggType = '1-3M';
				bi.BI_Rama__c = 'Rama1';
				bi.BI_Familia_1__c = 'Fam1';
				bi.BI_Familia_2__c = 'Fam11';
			}
			bi.Aggregation_Type__c = aggType;
			items.add(bi);
		}

		insert items;
	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}