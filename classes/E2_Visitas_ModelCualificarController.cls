public class E2_Visitas_ModelCualificarController {
    
    @AuraEnabled
    public static Event doGetEvent(Id recordId) {
        
        Event evento = [SELECT Id, BI_FVI_SurveyTaken__c,BI_FVI_Estado__c, BI_FVI_SurveyTaken__r.Name From Event Where Id = :RecordId];
        
        return evento;
    }
}