/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CORE_Fullstack_Manager

History:
<Date>							<Author>						<Change Description>
3/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_CORE_Fullstack_Manager_Test {
    
    @testSetup 
    private static void init() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id);
        
        insert account;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                         BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                         CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F6 - Prospecting',
                                                         BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                         BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                         RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        
        /* Contact */
        Contact contact = new Contact(FirstName = 'Integración', LastName = 'Perú', BI_Activo__c = true,
                                      BI_Tipo_de_contacto__c = 'Administrativo', BI_Representante_legal__c = true, Phone = '000000000',
                                      AssistantPhone = '000000000', HomePhone = '000000000', MobilePhone = '000000000',
                                      OtherPhone = '000000000', Fax = '000000000', email = 'integraciones@integraciones.test',
                                      BI_Country__c = 'Peru', BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '0000000X',
                                      Birthdate = Date.today(), BI_Genero__c = 'Masculino', accountId = account.Id,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Active_Contact' LIMIT 1][0].Id);
        
        insert contact;
        
        /* AccountTeamMembers */
        FS_CORE_TeamMemberToSincronize__c accountTeamMembers_insert = new FS_CORE_TeamMemberToSincronize__c(FS_CORE_FuncionUser__c = 'Service Manager', FS_CORE_IdAccount__c = account.Id, FS_CORE_IdUser__c = UserInfo.getUserId(),
                                                                                                    FS_CORE_SalesforceId__c = 'service manager', FS_CORE_TipoRegistro__c = 'Actual');
        
        FS_CORE_TeamMemberToSincronize__c accountTeamMembers_delete = new FS_CORE_TeamMemberToSincronize__c(FS_CORE_FuncionUser__c = 'Service Manager', FS_CORE_IdAccount__c = account.Id, FS_CORE_IdUser__c = UserInfo.getUserId(),
                                                                                                    FS_CORE_SalesforceId__c = 'service manager', FS_CORE_TipoRegistro__c = 'Anterior');
        insert accountTeamMembers_insert;
        insert accountTeamMembers_delete;
        
        /* Custom Settings */
        FS_CORE_Fullstack_Manager_Test.generateCustomSettings();
    }
    
    @isTest private static void createAccount_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_2_Status__c = true;		/* HTPP 200 */
        Account account = new Account(Name = 'Test 2 [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20352003609',
                                      Phone = '6815623147', Fax = '122356789', FS_CORE_Account_Massive__c = false,
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id);
        
        insert account;
        FS_CORE_Fullstack_Manager.createAccount(account);
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }

    @isTest private static void createAccount_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_2_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    @isTest private static void createAccount_errorParam() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_2_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            string CustomParamStr = [SELECT Id From Account LIMIT 1][0].Id+'';
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null,CustomParamStr);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    @isTest private static void updateAccount_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_3_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void updateAccount_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_3_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void updateAccount_errorParam() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_3_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        string CustomParamStr = [SELECT Id From Account LIMIT 1][0].Id+'';
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null,CustomParamStr);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void createContact_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_4_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 3, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void createContact_successParam() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_4_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        string CustomParamStr = [SELECT Id From Account LIMIT 1][0].Id+'';
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 3, false, null,CustomParamStr);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void createContact_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_4_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 3, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void updateContact_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_5_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 3, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void updateContact_successParam() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_5_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        string CustomParamStr = [SELECT Id From Account LIMIT 1][0].Id+'';
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 3, false, null,CustomParamStr);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void updateContact_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_5_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 3, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void AccountTeamMembers_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true; 	/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_6_Status__c = true;		/* HTPP 200 */
        ORGConfigurationMock.FS_CORE_Method_7_Status__c = true;		/* HTPP 200 */
        ORGConfigurationMock.FS_CORE_Method_1_Status__c =true;
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 2, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void AccountTeamMembers_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_6_Status__c = false;	/* HTPP 400 */
        ORGConfigurationMock.FS_CORE_Method_7_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 2, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void linkContactToAccount_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_8_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Account account = [SELECT FS_CORE_ID_Cliente_legado__c FROM Account LIMIT 1][0];
        account.FS_CORE_ID_Cliente_legado__c = '14785AAE1';
        update account;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 4, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void linkContactToAccount_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_8_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        Account account = [SELECT FS_CORE_ID_Cliente_legado__c FROM Account LIMIT 1][0];
        account.FS_CORE_ID_Cliente_legado__c = '14785AAE1';
        update account;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 4, false, null);
        }catch(Exception exc){
            System.assert(false);
            
            
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void unlinkContactToAccount_success() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_9_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Account account = [SELECT FS_CORE_ID_Cliente_legado__c FROM Account LIMIT 1][0];
        account.FS_CORE_ID_Cliente_legado__c = '14785AAE1';
        update account;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 5, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void unlinkContactToAccount_successExtra() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_9_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Account account = [SELECT FS_CORE_ID_Cliente_legado__c FROM Account LIMIT 1][0];
        account.FS_CORE_ID_Cliente_legado__c = '14785AAE1';
        update account;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 5, false, [SELECT Id From Account LIMIT 1][0].Id);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void unlinkContactToAccount_error() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = true;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_9_Status__c = false;	/* HTPP 400 */
        
        insert ORGConfigurationMock;
        
        Account account = [SELECT FS_CORE_ID_Cliente_legado__c FROM Account LIMIT 1][0];
        account.FS_CORE_ID_Cliente_legado__c = '14785AAE1';
        update account;
        
        Contact contact = [SELECT BI_Id_del_contacto__c FROM Contact LIMIT 1][0];
        contact.BI_Id_del_contacto__c = '44785AAQ1';
        update contact;
        
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 5, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void createAccount_successAccountParam() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_2_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Contact contact = [SELECT Id,BI_Id_del_contacto__c,AccountId FROM Contact LIMIT 1][0];
        contact.AccountId = [SELECT Id From Account LIMIT 1][0].Id;
        update contact;
        /* Test */
        System.Test.startTest();
        string CustomParamStr = [SELECT Id From Account LIMIT 1][0].Id+'';
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null,CustomParamStr);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void createAccount_successAccountParamStep2() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's not Fullstack */
        ORGConfigurationMock.FS_CORE_Method_2_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        Contact contact = [SELECT Id,BI_Id_del_contacto__c,AccountId FROM Contact LIMIT 1][0];
        contact.AccountId = [SELECT Id From Account LIMIT 1][0].Id;
        update contact;
        /* Test */
        System.Test.startTest();
        string CustomParamStr = [SELECT Id From Account LIMIT 1][0].Id+'';
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 2, false, null,CustomParamStr);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void updateAccount_internalMethod() {
        /* Custom Configuration */
        FS_CORE_Integracion_Mock__c ORGConfigurationMock = new FS_CORE_Integracion_Mock__c();
        ORGConfigurationMock.FS_CORE_Server_Status__c = false;
        ORGConfigurationMock.FS_CORE_Method_1_Exist__c = false;		/* It's Fullstack */
        ORGConfigurationMock.FS_CORE_Method_3_Status__c = true;		/* HTPP 200 */
        
        insert ORGConfigurationMock;
        
        /* Test */
        System.Test.startTest();
        try{
            FS_CORE_Fullstack_Manager.callout([SELECT Id From Account LIMIT 1][0].Id, 1, false, null);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }

    
    private static void generateCustomSettings(){
        /* Variable Definition */
        FS_CORE_Integracion__c ORGConfiguration = new FS_CORE_Integracion__c();
        
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1 Top 2 Top 3 Top 4 Platino';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';
        insert ORGConfiguration;
    }
}