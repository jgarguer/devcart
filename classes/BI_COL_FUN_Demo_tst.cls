/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-23      Daniel ALexander Lopez (DL)     Create Class   
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
                    20/09/2017      Angel F. Santaliestra               Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private with sharing class BI_COL_FUN_Demo_tst {

    Public static list <UserRole>                                 lstRoles;
    Public static User                                            objUsuario = new User();

    public static list<Opportunity>                                 lstOppAsociadas = new list<Opportunity>();
    public static Opportunity                                       objOppty;
    
    public static List<BI_COL_FUN_Demo_ctr.wrappMs>               lstWrapper = new List<BI_COL_FUN_Demo_ctr.wrappMs>();
  
    public static Account                                           objCuenta;

    public static BI_COL_Anexos__c                                  objAnexos;

    public static List<BI_COL_Modificacion_de_Servicio__c>          lstModSer;
    public static BI_COL_Modificacion_de_Servicio__c                objModSer;

    public static List<BI_COL_Descripcion_de_servicio__c>           lstDesSer;
    public static BI_COL_Descripcion_de_servicio__c                 objDesSer;

    public static List<RecordType>                                  rtBI_FUN;
    public static Contact                               objContacto;
    public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;
    
    public static void crearData()
    {
      /*List<Profile> lstPerfil       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
      //list<User> lstUsuarios        = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

      //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        list<User> lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);

      System.runAs(lstUsuarios[0])
      {*/
          BI_bypass__c objBibypass = new BI_bypass__c();
          objBibypass.BI_migration__c=true;
          insert objBibypass;
      
          objCuenta                                       = new Account();
          objCuenta.Name                                  = 'prueba';
          objCuenta.BI_Country__c                         = 'Colombia';
          objCuenta.TGS_Region__c                         = 'América';
          objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
          objCuenta.CurrencyIsoCode                       = 'GTQ';
          objCuenta.BI_Segment__c                         = 'test';
          objCuenta.BI_Subsegment_Regional__c             = 'test';
          objCuenta.BI_Territory__c                       = 'test';
          insert objCuenta;

          System.debug('\n\n\n ======= CUENTA ======\n '+ objCuenta );

             //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto; 
            
            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);



          for(Integer i = 0; i < 10; i++)
          {
            objOppty                                      = new Opportunity();
            objOppty.Name                                 = 'TEST AVANXO OPPTY'+i;
            objOppty.AccountId                            = objCuenta.Id;
            //objOppty.TGS_Region__c                      = 'América';
            objOppty.BI_Country__c                        = 'Colombia';
            objOppty.CloseDate                            = System.today().addDays(i);
            objOppty.StageName                            = 'F6 - Prospecting';
            objOppty.BI_Ciclo_ventas__c                   = Label.BI_Completo;
            lstOppAsociadas.add(objOppty);
          }
          
          System.debug('\n\n\n ======= lstOppyAsociadas ======\n '+ lstOppAsociadas );

          insert lstOppAsociadas;

          rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];

          objAnexos               = new BI_COL_Anexos__c();
          objAnexos.Name          = 'FUN-0041414';
          objAnexos.RecordTypeId  = rtBI_FUN[0].Id;
          insert objAnexos;

          System.debug('\n\n\n ======== objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);

          objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
          objDesSer.BI_COL_Oportunidad__c                     = lstOppAsociadas[0].Id;
          objDesSer.CurrencyIsoCode                           = 'COP';
          insert objDesSer;
          
          System.debug('\n\n\n ======= objDesSer '+ lstModSer);

          //for(Integer i = 0; i < 10; i++)
          //{

              objModSer                                        = new BI_COL_Modificacion_de_Servicio__c();
              objModSer.BI_COL_FUN__c                          = objAnexos.Id;
              objModSer.BI_COL_Codigo_unico_servicio__c        = objDesSer.Id;
              objModSer.BI_COL_Clasificacion_Servicio__c       = 'ALTA DEMO';
              objModSer.CurrencyIsoCode                     = 'COP';
              objModSer.BI_COL_Fecha_instalacion_servicio_RFS__c  = Date.today().addDays(30);
              //objModSer.BI_COL_Direccion_IP__c            = i+'';
              objModSer.BI_COL_Oportunidad__c                 = objOppty.Id;
              objModSer.BI_COL_Bloqueado__c                   = false;            
              objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
              objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
              //objModSer.BI_COL_Direccion_IP__c            = i+'';
              insert objModSer;
            //  lstModSer.add(objModSer);
          //}

         // insert lstModSer;

          System.debug('\n\n\n ======= lstModSer '+ lstModSer);
       // }
    }

        public static testMethod void test_method_one()
        {
            objUsuario = BI_COL_CreateData_tst.getCreateUSer();
            System.runAs(objUsuario) 
            {
                crearData();
                ApexPages.StandardController sc = new ApexPages.StandardController(objAnexos);
    
                Test.startTest();
                BI_COL_FUN_Demo_ctr classfun              = new BI_COL_FUN_Demo_ctr(sc);
                BI_COL_FUN_Demo_ctr.wrappMs   wrapper     = new BI_COL_FUN_Demo_ctr.wrappMs('Test',objModSer);
                lstWrapper.add(wrapper);
                classfun.lstMsWrap = lstWrapper;
                Test.stopTest();
            }

        }
}