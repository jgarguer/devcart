/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test Class BI_O4_NewProposalController_TEST
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    23/11/2016              Alvaro Garc�a           Fix and add test_method_two, test_method_three, test_method_four, test_method_five, exceptions
    20/09/2017              Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
    25/09/2017              Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_NewProposalController_TEST {
	
  static{
        BI_TestUtils.throw_exception = false;
  }
	
	@isTest static void test_method_one() {
		
		Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();
		for(RecordType rt11 : [SELECT Id, DeveloperName
							 FROM RecordType
							 WHERE SObjectType = 'Quote' OR DeveloperName = 'TGS_Holding' OR DeveloperName = 'BI_Oportunidad_Regional' OR DeveloperName = 'BI_O4_Not_Organic_Growth']) {
			mapRecordTypes.put(rt11.DeveloperName, rt11);
		}
		//RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Holding' Limit 1];

    User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
    
		List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                              BI_Activo__c = Label.BI_Si,
                              BI_Segment__c = 'Empresas',
                              BI_Subsector__c = 'test', //25/09/2017
                              BI_Sector__c = 'test', //25/09/2017
                              BI_Subsegment_Regional__c = 'test',
                              BI_Territory__c = 'test',
                              BI_Subsegment_local__c = 'Top 1',
                              RecordTypeId = mapRecordTypes.get('TGS_Holding').Id
                              );    
    lst_acc1.add(acc1);
    System.runAs(currentUser){
        insert lst_acc1;
    }
      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableByPass(UserInfo.getUserId(),true,false,false,false);

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',  
                                          BI_Productos_numero__c  = 1
                                          );    
        insert opp;

        Opportunity opp1 = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          /*StageName = Label.BI_DesarrolloOferta,*/
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',
                                          BI_Oportunidad_Padre__c = opp.Id,
                                          BI_Productos_numero__c = 1
                                          );    
        insert opp1;

        BI_MigrationHelper.disableByPass(mapa);

        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opp.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = opp.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opp.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
        quote.BI_O4_USER_GSE__c = UserInfo.getUserId();

        insert quote;


 		//RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

  //      Case testCase = new Case();    
        
  //      testCase.Subject = 'Test Subject Opp';
  //      testCase.AccountId = opp.AccountId;
  //      testCase.RecordTypeId = mapRecordTypes.get('BI_Caso_Interno').Id;
  //      testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
  //      testCase.BI_Country__c = opp.BI_Country__c;
  //      testCase.BI_Department__c = 'Ingenier�a de Preventa';
  //      testCase.BI_Type__c = 'Solicitar Cotizaci�n';
       	                                                           
		
		//insert testCase;

  //  System.debug('##RP## testCase: ' + testCase);

		Test.startTest();
		ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 
		BI_O4_NewProposalController controller = new BI_O4_NewProposalController(standardC);

     	controller.doValidations();
     	//controller.reValidate();
     	controller.cancelButton();
      controller.selectAllCheckbox = true;
      controller.selectAll();
     	controller.saveButton();

     	System.assertNotEquals(null, opp1.Id);
     	Test.stopTest();
	}

    @isTest static void test_method_two() {
    
      Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();
      for(RecordType rt11 : [SELECT Id, DeveloperName
                 FROM RecordType
                 WHERE SObjectType = 'Quote' OR DeveloperName = 'TGS_Holding' OR DeveloperName = 'BI_Oportunidad_Regional' OR DeveloperName = 'BI_O4_Not_Organic_Growth']) {
        mapRecordTypes.put(rt11.DeveloperName, rt11);
      }
      //RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Holding' Limit 1];

      User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();

		List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                              BI_Activo__c = Label.BI_Si,
                              BI_Segment__c = 'Empresas',
                              BI_Subsector__c = 'test', //25/09/2017
                              BI_Sector__c = 'test', //25/09/2017
                              BI_Subsegment_Regional__c = 'test',
                              BI_Territory__c = 'test',
                              BI_Subsegment_local__c = 'Top 1',
                              RecordTypeId = mapRecordTypes.get('TGS_Holding').Id
                            );    
    lst_acc1.add(acc1);
    System.runAs(currentUser){
        insert lst_acc1;
    }

      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableByPass(UserInfo.getUserId(),true,false,false,false);

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',  
                                          BI_Productos_numero__c  = 1
                                          );    
        insert opp;

        Opportunity opp1 = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          /*StageName = Label.BI_DesarrolloOferta,*/
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',
                                          BI_Oportunidad_Padre__c = opp.Id,
                                          BI_Productos_numero__c = 1
                                          );    
        insert opp1;

        BI_MigrationHelper.disableByPass(mapa);

        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opp.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = opp.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opp.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
        quote.BI_O4_USER_GSE__c = UserInfo.getUserId();

        insert quote;

        BI_O4_Opportunity_Qualify__c oppQualify = new BI_O4_Opportunity_Qualify__c(BI_O4_Opportunity__c = opp.Id,
                                                                                  RecordTypeId = mapRecordTypes.get('BI_O4_Not_Organic_Growth').Id);
        insert oppQualify;

    //RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

    //    Case testCase = new Case();    
        
    //    testCase.Subject = 'Test Subject Opp';
    //    testCase.AccountId = opp.AccountId;
    //    testCase.RecordTypeId = mapRecordTypes.get('BI_Caso_Interno').Id;
    //    testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    //    testCase.BI_Country__c = opp.BI_Country__c;
    //    testCase.BI_Department__c = 'Ingenier�a de Preventa';
    //    testCase.BI_Type__c = 'Solicitar Cotizaci�n';
                                                                   
    
    //insert testCase;

    //System.debug('##RP## testCase: ' + testCase);

    Test.startTest();
    ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 
    BI_O4_NewProposalController controller = new BI_O4_NewProposalController(standardC);

      controller.doValidations();
      //controller.reValidate();
      controller.cancelButton();
      controller.selectAllCheckbox = true;
      controller.selectAll();
      controller.saveButton();

      System.assertNotEquals(null, opp1.Id);
      Test.stopTest();
  }

  @isTest static void test_method_three() {
    
      Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();
      for(RecordType rt11 : [SELECT Id, DeveloperName
                 FROM RecordType
                 WHERE SObjectType = 'Quote' OR DeveloperName = 'TGS_Holding' OR DeveloperName = 'BI_Oportunidad_Regional' OR DeveloperName = 'BI_O4_Not_Organic_Growth']) {
        mapRecordTypes.put(rt11.DeveloperName, rt11);
      }
      //RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Holding' Limit 1];

      User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
      
      List<Account> lst_acc1 = new List<Account>();       
      Account acc1 = new Account(Name = 'test',
                                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                BI_Activo__c = Label.BI_Si,
                                BI_Segment__c = 'Empresas',
                                BI_Subsector__c = 'test', //25/09/2017
                                BI_Sector__c = 'test', //25/09/2017
                                BI_Subsegment_Regional__c = 'test',
                                BI_Territory__c = 'test',
                                BI_Subsegment_local__c = 'Top 1',
                                RecordTypeId = mapRecordTypes.get('TGS_Holding').Id
                                );    
      lst_acc1.add(acc1);
      System.runAs(currentUser){
          insert lst_acc1;
      }
      
      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableByPass(UserInfo.getUserId(),true,false,false,false);

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',  
                                          BI_Productos_numero__c  = 1
                                          );    
        insert opp;

        Opportunity opp1 = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          /*StageName = Label.BI_DesarrolloOferta,*/
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',
                                          BI_Oportunidad_Padre__c = opp.Id,
                                          BI_Productos_numero__c = 1
                                          );    
        insert opp1;

        BI_MigrationHelper.disableByPass(mapa);

        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opp.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = opp.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opp.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
      	quote.BI_O4_USER_GSE__c = UserInfo.getUserId();

        insert quote;

        BI_O4_Opportunity_Qualify__c oppQualify = new BI_O4_Opportunity_Qualify__c(BI_O4_Opportunity__c = opp.Id,
                                                                                  RecordTypeId = mapRecordTypes.get('BI_O4_Not_Organic_Growth').Id);
        insert oppQualify;

        BI_O4_Customer_Decision_Making_Model__c decision = new BI_O4_Customer_Decision_Making_Model__c(BI_O4_Opportunity_Qualify__c = oppQualify.Id);
        insert decision;

    //RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

    //    Case testCase = new Case();    
        
    //    testCase.Subject = 'Test Subject Opp';
    //    testCase.AccountId = opp.AccountId;
    //    testCase.RecordTypeId = mapRecordTypes.get('BI_Caso_Interno').Id;
    //    testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    //    testCase.BI_Country__c = opp.BI_Country__c;
    //    testCase.BI_Department__c = 'Ingenier�a de Preventa';
    //    testCase.BI_Type__c = 'Solicitar Cotizaci�n';
                                                                   
    
    //insert testCase;

    //System.debug('##RP## testCase: ' + testCase);

    Test.startTest();
    ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 
    BI_O4_NewProposalController controller = new BI_O4_NewProposalController(standardC);

      controller.doValidations();
      //controller.reValidate();
      controller.cancelButton();
      controller.selectAllCheckbox = true;
      controller.selectAll();
      controller.saveButton();

      System.assertNotEquals(null, opp1.Id);
      Test.stopTest();
  }

@isTest static void test_method_four() {
    
      Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();
      for(RecordType rt11 : [SELECT Id, DeveloperName
                 FROM RecordType
                 WHERE SObjectType = 'Quote' OR DeveloperName = 'TGS_Holding' OR DeveloperName = 'BI_Oportunidad_Regional' OR DeveloperName = 'BI_O4_Not_Organic_Growth']) {
        mapRecordTypes.put(rt11.DeveloperName, rt11);
      }
      //RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Holding' Limit 1];

      User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
      
      List<Account> lst_acc1 = new List<Account>();       
      Account acc1 = new Account(Name = 'test',
                                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                BI_Activo__c = Label.BI_Si,
                                BI_Segment__c = 'Empresas',
                                BI_Subsector__c = 'test', //25/09/2017
                                BI_Sector__c = 'test', //25/09/2017
                                BI_Subsegment_Regional__c = 'test',
                                BI_Territory__c = 'test',
                                BI_Subsegment_local__c = 'Top 1',
                                RecordTypeId = mapRecordTypes.get('TGS_Holding').Id
                                );    
      lst_acc1.add(acc1);
      System.runAs(currentUser){
          insert lst_acc1;
      }
      
      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableByPass(UserInfo.getUserId(),true,false,false,false);

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',  
                                          BI_Productos_numero__c  = 1
                                          );    
        insert opp;

        Opportunity opp1 = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          /*StageName = Label.BI_DesarrolloOferta,*/
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',
                                          BI_Oportunidad_Padre__c = opp.Id,
                                          BI_Productos_numero__c = 1
                                          );    
        insert opp1;

        BI_MigrationHelper.disableByPass(mapa);

        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opp.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = opp.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opp.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
		quote.BI_O4_USER_GSE__c = UserInfo.getUserId();
    
        insert quote;

        BI_O4_Opportunity_Qualify__c oppQualify = new BI_O4_Opportunity_Qualify__c(BI_O4_Opportunity__c = opp.Id,
                                                                                  RecordTypeId = mapRecordTypes.get('BI_O4_Not_Organic_Growth').Id);
        insert oppQualify;

        BI_O4_Risk_Assessment__c risk = new BI_O4_Risk_Assessment__c(BI_O4_Qualification_Tool__c = oppQualify.Id);
        insert risk;

    //RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

    //    Case testCase = new Case();    
        
    //    testCase.Subject = 'Test Subject Opp';
    //    testCase.AccountId = opp.AccountId;
    //    testCase.RecordTypeId = mapRecordTypes.get('BI_Caso_Interno').Id;
    //    testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    //    testCase.BI_Country__c = opp.BI_Country__c;
    //    testCase.BI_Department__c = 'Ingenier�a de Preventa';
    //    testCase.BI_Type__c = 'Solicitar Cotizaci�n';
                                                                   
    
    //insert testCase;

    //System.debug('##RP## testCase: ' + testCase);

    Test.startTest();
    ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 
    BI_O4_NewProposalController controller = new BI_O4_NewProposalController(standardC);

      controller.doValidations();
      //controller.reValidate();
      controller.cancelButton();
      controller.selectAllCheckbox = true;
      controller.selectAll();
      controller.saveButton();

      System.assertNotEquals(null, opp1.Id);
      Test.stopTest();
  }

@isTest static void test_method_five() {
    
      Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();
      for(RecordType rt11 : [SELECT Id, DeveloperName
                 FROM RecordType
                 WHERE SObjectType = 'Quote' OR DeveloperName = 'TGS_Holding' OR DeveloperName = 'BI_Oportunidad_Regional' OR DeveloperName = 'BI_O4_Not_Organic_Growth']) {
        mapRecordTypes.put(rt11.DeveloperName, rt11);
      }
      //RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Holding' Limit 1];

      User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
      
      List<Account> lst_acc1 = new List<Account>();       
      Account acc1 = new Account(Name = 'test',
                                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                BI_Activo__c = Label.BI_Si,
                                BI_Segment__c = 'Empresas',
                                BI_Subsector__c = 'test', //25/09/2017
                                BI_Sector__c = 'test', //25/09/2017
                                BI_Subsegment_Regional__c = 'test',
                                BI_Territory__c = 'test',
                                BI_Subsegment_local__c = 'Top 1',
                                RecordTypeId = mapRecordTypes.get('TGS_Holding').Id
                                  );    
        lst_acc1.add(acc1);
      System.runAs(currentUser){
        insert lst_acc1;
      }

      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableByPass(UserInfo.getUserId(),true,false,false,false);

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',  
                                          BI_Productos_numero__c  = 1
                                          );    
        insert opp;

        Opportunity opp1 = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          /*StageName = Label.BI_DesarrolloOferta,*/
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',
                                          BI_Oportunidad_Padre__c = opp.Id,
                                          BI_Productos_numero__c = 1
                                          );    
        insert opp1;

        BI_MigrationHelper.disableByPass(mapa);

        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opp.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = opp.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opp.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
    	quote.BI_O4_USER_GSE__c = UserInfo.getUserId();

        insert quote;

        BI_O4_Opportunity_Qualify__c oppQualify = new BI_O4_Opportunity_Qualify__c(BI_O4_Opportunity__c = opp.Id,
                                                                                  RecordTypeId = mapRecordTypes.get('BI_O4_Not_Organic_Growth').Id);
        insert oppQualify;

        BI_O4_Risk_Assessment__c risk = new BI_O4_Risk_Assessment__c(BI_O4_Qualification_Tool__c = oppQualify.Id);
        insert risk;

        BI_O4_Customer_Decision_Making_Model__c decision = new BI_O4_Customer_Decision_Making_Model__c(BI_O4_Opportunity_Qualify__c = oppQualify.Id);
        insert decision;

    //RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];
        
    //    Case testCase = new Case();    
        
    //    testCase.Subject = 'Test Subject Opp';
    //    testCase.AccountId = opp.AccountId;
    //    testCase.RecordTypeId = mapRecordTypes.get('BI_Caso_Interno').Id;
    //    testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    //    testCase.BI_Country__c = opp.BI_Country__c;
    //    testCase.BI_Department__c = 'Ingenier�a de Preventa';
    //    testCase.BI_Type__c = 'Solicitar Cotizaci�n';
       	                                                           
		
    //insert testCase;

    //System.debug('##RP## testCase: ' + testCase);

		Test.startTest();
		ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 
		BI_O4_NewProposalController controller = new BI_O4_NewProposalController(standardC);

     	controller.doValidations();
     	//controller.reValidate();
      controller.cancelButton();
      controller.selectAllCheckbox = true;
      controller.selectAll();
     	controller.saveButton();

     	System.assertNotEquals(null, opp1.Id);
     	Test.stopTest();
	}

  @isTest static void exceptions() {

    Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();
    for(RecordType rt11 : [SELECT Id, DeveloperName
               FROM RecordType
               WHERE SObjectType = 'Quote' OR DeveloperName = 'TGS_Holding' OR DeveloperName = 'BI_Oportunidad_Regional' OR DeveloperName = 'BI_Caso_Interno']) {
      mapRecordTypes.put(rt11.DeveloperName, rt11);
    }
    //RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Holding' Limit 1];

    User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
    
    List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                              BI_Activo__c = Label.BI_Si,
                              BI_Segment__c = 'Empresas',
                              BI_Subsector__c = 'test', //25/09/2017
                              BI_Sector__c = 'test', //25/09/2017
                              BI_Subsegment_Regional__c = 'test',
                              BI_Territory__c = 'test',
                              BI_Subsegment_local__c = 'Top 1',
                              RecordTypeId = mapRecordTypes.get('TGS_Holding').Id
                              );    
    lst_acc1.add(acc1);
    System.runAs(currentUser){
        insert lst_acc1;
    }
      Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableByPass(UserInfo.getUserId(),true,false,false,false);

        //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',  
                                          BI_Productos_numero__c  = 1
                                          );    
        insert opp;

        Opportunity opp1 = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          /*StageName = Label.BI_DesarrolloOferta,*/
                                          StageName = Label.BI_F5DefSolucion,
                                          AccountId = lst_acc1[0].Id,
                                          BI_Ciclo_ventas__c = 'Completo', 
                                          RecordTypeId = mapRecordTypes.get('BI_Oportunidad_Regional').Id,
                                          BI_O4_Opportunity_Type__c = 'Centralized',
                                          BI_Oportunidad_Padre__c = opp.Id,
                                          BI_Productos_numero__c = 1
                                          );    
        insert opp1;

        BI_MigrationHelper.disableByPass(mapa);

        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opp.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = opp.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opp.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
      	quote.BI_O4_USER_GSE__c = UserInfo.getUserId();

        insert quote;

    ////RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

    //    Case testCase = new Case();    
        
    //    testCase.Subject = 'Test Subject Opp';
    //    testCase.AccountId = opp.AccountId;
    //    testCase.RecordTypeId = mapRecordTypes.get('BI_Caso_Interno').Id;
    //    testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    //    testCase.BI_Country__c = opp.BI_Country__c;
    //    testCase.BI_Department__c = 'Ingenier�a de Preventa';
    //    testCase.BI_Type__c = 'Solicitar Cotizaci�n';
                                                                   
    
    //insert testCase;

    //System.debug('##RP## testCase: ' + testCase);

    Test.startTest();

    ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 

    BI_O4_NewProposalController controller = new BI_O4_NewProposalController(standardC);

      controller.doValidations();
      //controller.reValidate();
      controller.cancelButton();
      controller.selectAllCheckbox = true;
      controller.selectAll();
      BI_TestUtils.throw_exception = true;
      controller.saveButton();

      Test.stopTest();

  }
	
}