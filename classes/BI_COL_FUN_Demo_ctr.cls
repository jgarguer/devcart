public class BI_COL_FUN_Demo_ctr {
    
    public BI_COL_Anexos__c fun{get;set;}
    public List<BI_COL_Modificacion_de_Servicio__c> lstMsDemo{get;set;}
    public String salto{get;set;}
    public List<wrappMs> lstMsWrap{get;set;}
     
    public BI_COL_FUN_Demo_ctr(ApexPages.Standardcontroller ctr){
        
        if(ctr.getRecord()!=null && ctr.getRecord().Id!=null){
            
            fun=[select Id, Name from BI_COL_Anexos__c where Id=:ctr.getRecord().Id limit 1];
            lstMsDemo=getMSSolicitudServicio(fun.Id);
            
            if(lstMsDemo.size()>1){
                salto='salto';
            }else{  
                salto='';
            }
        }
        
    }
    
    
    public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String idFUN){
        List<BI_COL_Modificacion_de_Servicio__c> lstMS = new List<BI_COL_Modificacion_de_Servicio__c>();    
        lstMsWrap=new List<wrappMs>();
        lstMS = [Select Id, Name, BI_COL_Oportunidad__r.Account.Name,
                BI_COL_Producto_Anterior__r.BI_COL_Plan_TRS__c, BI_COL_Duracion_demo__c,
                BI_COL_Producto_Anterior__r.Name,  BI_COL_Producto_Anterior__r.BI_COL_Descripcion_referencia__c
                    //PENDIENTES POR CONFIRMAR CAMPOS
                    //Duracion_servicio_ocacional__c,
                    //Producto_Telefonica__r.Descripcion_Referencia__c,
                    //Producto_Telefonica__r.Anchos_de_banda__r.Name,Producto_Telefonica__r.Plan_TRS__c                 
                    from BI_COL_Modificacion_de_Servicio__c 
                    where BI_COL_FUN__c = :idFUN and BI_COL_Clasificacion_Servicio__c IN ('ALTA DEMO','DEMO UPGRADE')
                    and BI_COL_Estado__c != 'Cerrada perdida' //APC 03/08/2016 - modificado por la demanda 230
                    order by Name Asc];
                    
        wrappMs wrap=null;
        for(Integer i=0;i<lstms.size();i++){
                         
            if(i==(lstms.size()-1)){
                wrap=new wrappMs('',lstms.get(i));              
            }else{
                wrap=new wrappMs('salto',lstms.get(i));
            }
            
            lstMsWrap.add(wrap);
                
        }
                            
        return lstMS;
    }
    
    
    public class wrappMs{
        public String salto{get;set;}
        public BI_COL_Modificacion_de_Servicio__c ms{get;set;}
        
        public wrappMs(String sa, BI_COL_Modificacion_de_Servicio__c mod){
            salto=sa;
            ms=mod;
        }
    }
    

}