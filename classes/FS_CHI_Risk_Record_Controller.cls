/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Alejandro Labrin
Company:		Everis Centers Temuco
Description:	RFG-INT-CHI-02 / RFG-INT-CHI-04: Recuperación de información de riesgo.

History:

<Date>                      <Author>                        <Change Description>
05/11/2016                  Alejandro Labrin				Initial Version
24/10/2017					Daniel Leal		   				Ever01:Inclusion de logs
17/11/2017					Paloma Lastra					Ever02: Correccion en los valores de tipo de crédito 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public  with sharing class FS_CHI_Risk_Record_Controller {
    
    @AuraEnabled
    public static String consulta(String IdAccount){
        System.debug('Nombre de clase: FS_CHI_Risk_Record_Controller   Nombre de método: consulta   Comienzo del método');
        /*Variable Declaration*/
        String mensaje;
        boolean spinner = true;
        //Ever:01-INI
        Account act;
        try{
            //Ever:01-FIN
            act = [SELECT BI_No_Identificador_fiscal__c, FS_CHI_Tipo_Credito__c, BI_Subsegment_Regional__c, BI_Country__c FROM Account WHERE Id = :IdAccount];
            System.debug('@@@@RUT: '+act.BI_No_Identificador_fiscal__c+' TCred: '+act.FS_CHI_Tipo_Credito__c+' SUBseg: '+ act.BI_Subsegment_Regional__c+' Pais: '+act.BI_Country__c+' ID: '+act.Id);
            if(act.BI_Country__c == 'Chile'){
                String tipoCredito = act.FS_CHI_Tipo_Credito__c;
                if(tipoCredito != null){
                    String numeroIdentificador = String.valueOf(act.BI_No_Identificador_fiscal__c);
                    String subSegmento = act.BI_Subsegment_Regional__c;
                    BI_RestWrapper.CreditInfoType response = FS_CHI_Risk_Management.Sync(tipoCredito, subSegmento, numeroIdentificador);
                    System.debug('@@@@RUT: response: '+response+' tipoCredito: '+tipoCredito+' numeroIdentificador: '+ numeroIdentificador+' subSegmento: '+subSegmento);

                    
                    /*Proceess Data*/
                    if(response != null){
                        if(response.creditProfiles[0] != null){
                            if(response.creditProfiles[0].creditScore != null) 
                                act.FS_CHI_Credit_Score__c = response.creditProfiles[0].creditScore;
                            if(!String.isBlank(response.creditProfiles[0].creditProfileDate))
                                act.FS_CHI_Fecha_de_ultima_ev__c = Date.valueOf(response.creditProfiles[0].creditProfileDate);
                        }
                        if(response.CreditLimitInfo != null){
                            if(response.CreditLimitInfo.creditLimit != null) act.FS_CHI_Limite_de_credito__c = response.CreditLimitInfo.creditLimit; 
                        }
                        // Ever02 - INI  
                        if ( response.additionalData != null){
                            for(BI_RestWrapper.KeyValueType keyValue : response.additionalData){
                                if(keyValue.Key == 'portabilityLimit'){
                                    act.FS_CHI_Limite_credito_portabilidad__c = Double.valueOf(keyValue.value);                                 
                                } 
                                if(keyValue.Key == 'creditClass'){
                                    act.FS_CHI_CREDIT_CLASS__c = keyValue.value;
                                } 
                                if(keyValue.Key == 'consumeLimit'){
                                    act.FS_CHI_Limite_de_consumo__c = Double.valueOf(keyValue.value);
                                }
                            }
                        }
                        // Ever02 - FIN
                        act.FS_CHI_Estado_Validacion_Riesgo__c = 'Validado';
                        update act;
                        spinner = true;
                        mensaje = 'success';
                    } else {
                        act.FS_CHI_Estado_Validacion_Riesgo__c = 'No validado';
                        act.FS_CHI_Credit_Score__c = null;
                        act.FS_CHI_Fecha_de_ultima_ev__c = null;
                        act.FS_CHI_Limite_de_credito__c = null;
                        act.FS_CHI_Limite_credito_portabilidad__c = null;
                        act.FS_CHI_CREDIT_CLASS__c = null;
                        act.FS_CHI_Limite_de_consumo__c = null;
                        update act;
                        spinner = false;
                        mensaje = 'errorNull';
                    }
                } else {
                    spinner = false;
                    mensaje = 'errorCreditType';
                }
            } else {
                mensaje ='errorPais';
                spinner = false;
            }
            //Ever:01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog_Generic('FS_CHI_Risk_Record_Controller.consulta', 'BI_EN', exc, 'Trigger',act);
            mensaje=exc.getStackTraceString() + ' '+ exc.getTypeName()+' ' + exc.getMessage();
        }
        //Ever_01-FIN
        System.debug('Nombre de clase: FS_CHI_Risk_Record_Controller   Nombre de método: consulta   Finalización del método    Resultado: ' + mensaje);
        return mensaje;
    }
}