public class SEG_Puff {

    private final Integer MAXBITS = 15;             /* maximum bits in a code */
    private final Integer MAXLCODES = 286;           /* maximum number of literal/length codes */
    private final Integer MAXDCODES = 30;            /* maximum number of distance codes */
    private final Integer MAXCODES = (MAXLCODES+MAXDCODES);  /* maximum codes lengths to read */
    private final Integer FIXLCODES = 288;           /* number of fixed literal/length codes */

    private Integer[] getIntArray(Integer size){

        return (Integer[]) JSON.deserialize('['+'0'.repeat(',',size) +']', Integer[].class);
    }

    private Integer[] removeNElems(Integer[] arr, Integer n){
        Integer[] newArr = new List<Integer>(arr);

        for(Integer i = 0; i<n; i++){
            newArr.remove(0);
        }

        return newArr;
    }

    ///* input and output state */
    private class State {
        /* output state */
        public String out;         /* output buffer */
        public Integer outlen;       /* available space at out */
        public Integer outcnt;       /* bytes written to out so far */

        /* input state */
        public String inStr;    /* input buffer */
        public Integer inlen;        /* available input at in */
        public Integer incnt;        /* bytes read so far */
        public Integer bitbuf;                 /* bit buffer */
        public Integer bitcnt;                 /* number of bits in bit buffer */

    }


    private Integer bits(State s, Integer need)
    {
        Long val;           /* bit accumulator (can use up to 20 bits) */
        Integer incnt2;
        ///* load at least need bits into val */
        val = s.bitbuf;
        while (s.bitcnt < need) {
            incnt2 = s.incnt++ *2;
            val |= (((s.inStr.charAt(incnt2) & 15)+(s.inStr.charAt(incnt2)>>>6)*9) * 16
                | (s.inStr.charAt(incnt2+1) & 15)+(s.inStr.charAt(incnt2+1)>>>6) * 9) << s.bitcnt;
            
            s.bitcnt += 8;
        }

        ///* drop need bits and update buffer, always zero to seven bits left */
        s.bitbuf = (Integer)(val >>> need);
        s.bitcnt -= need;

        ///* return need bits, zeroing the bits above that */
        return (Integer)(val & ((1L << need) - 1));
    }

    private Long hexToLong (String hexStr){
        return ((hexStr.charAt(0) & 15)+(hexStr.charAt(0)>>>6)*9) * 16
              + (hexStr.charAt(1) & 15)+(hexStr.charAt(1)>>>6) * 9;
    }

    
    private Integer stored(State s)
    {
        Integer len;       /* length of stored block */

        /* discard leftover bits from current byte (assumes s->bitcnt < 8) */
        s.bitbuf = 0;
        s.bitcnt = 0;

        /* get length and check against its one's complement */
        if (s.incnt + 4 > s.inlen)
            return 2;                               /* not enough input */
        len = (Integer)hexToLong(s.inStr.mid(s.incnt++ *2, 2));
        len |= (Integer)hexToLong(s.inStr.mid(s.incnt++ *2, 2)) << 8;
        s.incnt++;
        s.incnt++;
        
        /* copy len bytes from in to out */
        while (len-- != 0) {
            s.out += s.inStr.mid(s.incnt++ *2, 2);
            s.outcnt++;
            //s.out[s.outcnt++] = s.inStr[s.incnt++];
        }

        /* done with a valid stored block */
        return 0;
    }

    
    public class Huffman {
        List<Integer> count;        //number of symbols of each length 
        List<Integer> symbol;      /* canonically ordered symbols */
    }

   
    public Integer decode(State s, Huffman h) //Fast
    {
        Integer len;            /* current number of bits in code */
        Integer code;           /* len bits being decoded */
        Integer first;          /* first code of length len */
        Integer count;          /* number of codes of length len */
        Integer index;          /* index of first code of length len in symbol table */
        Integer bitbuf;         /* bits from stream */
        Integer left;           /* bits left in next or left to process */
        Integer[] next;        /* next number of codes */

        bitbuf = s.bitbuf;
        left = s.bitcnt;
        code = first = index = 0;
        len = 1;
        next = h.count;
        while (true) {
            while (left-- != 0) {
                code |= bitbuf & 1;
                bitbuf >>>= 1;
                count = next[len];
                if (code - count < first) { /* if length len, return symbol */
                    s.bitbuf = bitbuf;
                    s.bitcnt = (s.bitcnt - len) & 7;
                    return h.symbol[index + (code - first)];
                }
                index += count;             /* else update for next length */
                first += count;
                first <<= 1;
                code <<= 1;
                len++;
            }
            left = (MAXBITS+1) - len;
            if (left == 0)
                break;
            bitbuf = (Integer)hexToLong(s.inStr.mid(s.incnt++ *2, 2));
            if (left > 8)
                left = 8;
        }
        System.assert(false, 'Ran out of codes while decompressing.');
        return -10;                         /* ran out of codes */
    }
    //#endif /* SLOW */

    
    private Integer construct(Huffman h, Integer[] length, Integer n)
    {
        Integer symbol;         /* current symbol when stepping through length[] */
        Integer len;            /* current length when stepping through h->count[] */
        Integer left;           /* number of possible codes left of current length */
        Integer[] offs = getIntArray(MAXBITS+1);      /* offsets in symbol table for each length */

        /* count number of codes of each length */
        for (len = 0; len <= MAXBITS; len++)
            h.count[len] = 0;
        for (symbol = 0; symbol < n; symbol++)
            (h.count[length[symbol]])++;   /* assumes lengths are within bounds */
        if (h.count[0] == n)               /* no codes! */
            return 0;                       /* complete, but decode() will fail */

        /* check for an over-subscribed or incomplete set of lengths */
        left = 1;                            //one possible code of zero length 
        for (len = 1; len <= MAXBITS; len++) {
            left <<= 1;                     /* one more bit, double codes left */
            left -= h.count[len];          /* deduct count from possible codes */
            if (left < 0)
                return left;                /* over-subscribed--return negative */
        }                                   /* left > 0 means incomplete */

        /* generate offsets into symbol table for each length for sorting */
        offs[1] = 0;
        for (len = 1; len < MAXBITS; len++)
            offs[len + 1] = offs[len] + h.count[len];

        /*
         * put symbols in table sorted by length, by symbol order within each
         * length
         */
        for (symbol = 0; symbol < n; symbol++)
            if (length[symbol] != 0) {
                h.symbol[offs[length[symbol]]] = symbol;
                offs[length[symbol]]++;
            }

        /* return zero for complete set, positive for incomplete set */
        return left;
    }

    
    private final Integer[] lens = new List<Integer>{ /* Size base for length codes 257..285 */
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
        35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258};
    private final Integer[] lext = new List<Integer>{ /* Extra bits for length codes 257..285 */
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2,
        3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0};
    private final Integer[] dists = new List<Integer>{ /* Offset base for distance codes 0..29 */
        1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193,
        257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145,
        8193, 12289, 16385, 24577};
    private final Integer[] dext = new List<Integer>{ /* Extra bits for distance codes 0..29 */
        0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
        7, 7, 8, 8, 9, 9, 10, 10, 11, 11,
        12, 12, 13, 13};

    public Integer codes(State s,
                    Huffman lencode,
                    Huffman distcode)
    {
        Integer symbol;         /* decoded symbol */
        Integer len;            /* length for copy */
        Integer dist;      /* distance for copy */
        

        /* decode literals and length/distance pairs */
        do {
            symbol = decode(s, lencode);
            if (symbol < 0)
                return symbol;              /* invalid symbol */
            if (symbol < 256) {             /* literal: symbol is the byte */
                /* write out the literal */
                s.out += SEG_HexUtils.intToHexLE(symbol, 1);
                s.outcnt++;
            }
            else if (symbol > 256) {        /* length */
                /* get and compute length */
                symbol -= 257;
                //if (symbol >= 29)
                //    return -10;             /* invalid fixed code */
                len = lens[symbol] + bits(s, lext[symbol]);

                /* get and check distance */
                symbol = decode(s, distcode);
                //if (symbol < 0)
                //    return symbol;          /* invalid symbol */
                dist = dists[symbol] + bits(s, dext[symbol]);
                /* copy length bytes from distance bytes back */
                //if (s.out != null) {
                    //if (s.outcnt + len > s.outlen)
                    //    return 101;
                    while (len-- != 0) {
                        //s.out[s.outcnt] = //////
                        s.out +=
                            dist > s.outcnt ?
                                '00' :
                                s.out.mid((s.outcnt - dist)*2, 2);
                        s.outcnt++;
                    }
                //}
                //else
                //    s.outcnt += len;
            }
        } while (symbol != 256);            /* end of block symbol */

        /* done with a valid fixed or dynamic block */
        return 0;
    }


    private Integer virgin = 1;
    private Integer[] lencnt = getIntArray(MAXBITS+1), lensym = getIntArray(FIXLCODES);
    private Integer[] distcnt = getIntArray(MAXBITS+1), distsym = getIntArray(MAXDCODES);
    private Huffman lencode = new Huffman(), distcode = new Huffman();

    private integer fixed(State s)
    {
        /* build fixed huffman tables if first call (may not be thread safe) */
        if (virgin == 1) {
            Integer symbol;
            Integer[] lengths = getIntArray(FIXLCODES);

            /* construct lencode and distcode */
            lencode.count = lencnt;
            lencode.symbol = lensym;
            distcode.count = distcnt;
            distcode.symbol = distsym;

            /* literal/length table */
            for (symbol = 0; symbol < 144; symbol++)
                lengths[symbol] = 8;
            for (; symbol < 256; symbol++)
                lengths[symbol] = 9;
            for (; symbol < 280; symbol++)
                lengths[symbol] = 7;
            for (; symbol < FIXLCODES; symbol++)
                lengths[symbol] = 8;
            construct(lencode, lengths, FIXLCODES);

            /* distance table */
            for (symbol = 0; symbol < MAXDCODES; symbol++)
                lengths[symbol] = 5;
            construct(distcode, lengths, MAXDCODES);

            /* do this just once */
            virgin = 0;
        }

        /* decode data until end-of-block code */
        return codes(s, lencode, distcode);
    }

   
    private Integer dynamic(State s)
    {
        Integer nlen, ndist, ncode;             /* number of lengths in descriptor */
        Integer index;                          /* index of lengths[] */
        Integer err;                            /* construct() return value */
        Integer[] lengths = getIntArray(MAXCODES);       /* descriptor code lengths */
        Integer[] lencnt = getIntArray(MAXBITS+1);
        Integer[] lensym = getIntArray(MAXLCODES);       /* lencode memory */
        Integer[] distcnt = getIntArray(MAXBITS+1);
        Integer[] distsym = getIntArray(MAXDCODES);       /* distcode memory */
        Huffman lencode = new Huffman();
        Huffman distcode = new Huffman();   /* length and distance codes */
        // permutation of code length codes
        final Integer[] order =  new List<Integer>    
            {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

        /* construct lencode and distcode */
        lencode.count = lencnt;
        lencode.symbol = lensym;
        distcode.count = distcnt;
        distcode.symbol = distsym;

        /* get number of lengths in each table, check lengths */
        nlen = bits(s, 5) + 257;
        ndist = bits(s, 5) + 1;
        ncode = bits(s, 4) + 4;
        if (nlen > MAXLCODES || ndist > MAXDCODES)
            return -3;                      /* bad counts */

        /* read code length code lengths (really), missing lengths are zero */
        for (index = 0; index < ncode; index++)
            lengths[order[index]] = bits(s, 3);
        for (; index < 19; index++)
            lengths[order[index]] = 0;

        /* build huffman table for code lengths codes (use lencode temporarily) */
        err = construct(lencode, lengths, 19);
        if (err != 0)               /* require complete code set here */
            return -4;

        /* read length/literal and distance code length tables */
        index = 0;
        while (index < nlen + ndist) {
            Integer symbol;             /* decoded value */
            Integer len;                /* last length to repeat */

            symbol = decode(s, lencode);
            if (symbol < 0)
                return symbol;          /* invalid symbol */
            if (symbol < 16)                /* length in 0..15 */
                lengths[index++] = symbol;
            else {                          /* repeat instruction */
                len = 0;                    /* assume repeating zeros */
                if (symbol == 16) {         /* repeat last length 3..6 times */
                    if (index == 0)
                        return -5;          /* no last length! */
                    len = lengths[index - 1];       /* last length */
                    symbol = 3 + bits(s, 2);
                }
                else if (symbol == 17)      /* repeat zero 3..10 times */
                    symbol = 3 + bits(s, 3);
                else                        /* == 18, repeat zero 11..138 times */
                    symbol = 11 + bits(s, 7);
                if (index + symbol > nlen + ndist)
                    return -6;              /* too many lengths! */
                while (symbol-- != 0)            /* repeat last or zero symbol times */
                    lengths[index++] = len;
            }
        }

        /* check for end-of-block code -- there better be one! */
        if (lengths[256] == 0)
            return -9;

        /* build huffman table for literal/length codes */
        err = construct(lencode, lengths, nlen);
        if (err!=0 && (err < 0 || nlen != lencode.count[0] + lencode.count[1]))
            return -7;      /* incomplete code ok only for single length 1 code */

        /* build huffman table for distance codes */
        err = construct(distcode, removeNElems(lengths, nlen), ndist);
        if (err!=0 && (err < 0 || ndist != distcode.count[0] + distcode.count[1]))
            return -8;      /* incomplete code ok only for single length 1 code */

        /* decode data until end-of-block code */
        return codes(s, lencode, distcode);
    }

    
    private State s = new State();             /* input/output state */
    
    public SEG_Puff(String source, Integer srclen, Integer destlen){
        /* initialize output state */
        s.out = '';
        s.outlen = destlen;                /* ignored if dest is NIL */
        s.outcnt = 0;

        /* initialize input state */
        s.inStr = source;
        s.inlen = srclen;
        s.incnt = 0;
        s.bitbuf = 0;
        s.bitcnt = 0;
    }

    public String inflate()
    {
        Integer blockNum = 0;
        Integer last, type;             /* block information */
        Integer err;                    /* return value */

        /* process blocks until last block or error */
        do {
            last = bits(s, 1);         /* one if last block */
            type = bits(s, 2);         /* block type 0..3 */
            err = type == 0 ?
                    stored(s) :
                    (type == 1 ?
                        fixed(s) :
                        (type == 2 ?
                            dynamic(s) :
                            -1));       /* type == 3, invalid */
            if (err != 0)
                break;                  /* return with error */
        } while (last == 0);
        
        System.assertEquals(0, err);
        return s.out;
    }
}