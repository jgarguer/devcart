/*------------------------------------------------------------
Author:         Ana Cirac 
Company:        Deloitte 
Description:    test class for DAO_HPD
                
<Date>          <Author>        <Change Description>
16-feb-2015     Ana Cirac        Initial Version
------------------------------------------------------------*/
@isTest()
private class TGS_DAO_HPD_Test {

    static testMethod void TestDAOHPD() {
       Test.startTest();
       
       Id incident = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Incident' LIMIT 1].Id;
       Contact caseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
       insert caseContact;
       Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket incident',
                                Description = 'Description test ticket incident',
                                RecordTypeId = incident,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                ContactId = caseContact.id
                                );      
        insert caseTest;
       String numberC = [SELECT CaseNumber FROM Case WHERE Subject = 'Test ticket incident'].CaseNumber;
       TGS_DAO_HPD.result result = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action="MODIFY";Incident_Number="'+numberC+'";Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
       //System.assertEquals(result.Operation_Status, 'OK');
       
       TGS_DAO_HPD.result resultAddWorkInfo = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action="MODIFY";Incident_Number="'+numberC+'";Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary"', 'file', Blob.valueOf('xxx'));
       //System.assertEquals(resultAddWorkInfo.Operation_Status, 'OK'); 
       
        /*
       NE__Order__c ci = new NE__Order__c(NE__AccountId__c='001m000000AIqxF');
       insert ci;
       NE__Order__c CI2 = [select id,case__c from NE__Order__c WHERE ID=:ci.id];
      System.assert(CI2.case__c!=null);
     
       Case ca = new Case(Subject = 'Subject',Description='Description',RecordTypeId='012m00000004N27',TGS_Service__c='Smart M2M');
       insert ca;
     
       ca.Status = 'Cancelled';
       update ca;
        ca.Status = 'Assigned';
       update ca;
      */
       Test.stopTest();
    }
    static testMethod void TestDAOHPDGenerateErrors() {
       Test.startTest();
       Id incident = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Incident' LIMIT 1].Id;
       Contact caseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
       insert caseContact;
       Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket incident',
                                Description = 'Description test ticket incident',
                                RecordTypeId = incident,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                ContactId = caseContact.id
                                );      

       insert caseTest;
       String numberC = [SELECT CaseNumber FROM Case WHERE  Subject = 'Test ticket incident'].CaseNumber;
       TGS_DAO_HPD.result result = TGS_DAO_HPD.modifyOrder('UDO', 'Action="MODIFY";Incident_Number="'+numberC+'";Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
           
       result = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action=MODIFY;Incident_Number="'+numberC+'";Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
      
       //Number Case!
       result = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action="MODIFY";Incident_Number=aaaa;Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
       
       result = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action="MODIFY";Incident_Number="'+numberC+'";Status="test";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
       
       //System_ID        
       result = TGS_DAO_HPD.modifyOrder('', 'Action="MODIFY";Incident_Number="'+numberC+'";Status="test";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
       
       //System_ID !
       result = TGS_DAO_HPD.modifyOrder('test', 'Action="MODIFY";Incident_Number="'+numberC+'";Status="test";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
       
       //Action  
       result = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action = "test" ;Incident_Number="'+numberC+'";Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null); 
       
       //Incident Number
       result = TGS_DAO_HPD.modifyOrder('"UDO"', 'Action=MODIFY;Incident_Number=;Status="Pending";Status_Reason="From Work In Progress";Work_Info_Summary="Summary";Work_Info_Notes="Notes"', '', null);
       
       Test.stopTest();
    }
}