@isTest
private class BI_Recalcular_NE_Orders_TEST {
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

    static testMethod void BI_Recalcular_NE_Orders_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        List<String> lst_pais = BI_DataLoad.loadPaisFromPickListNotBeing(1, new Set <String> {'Ecuador'});
        system.assert(!lst_pais.isEmpty());
                  
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
        system.assert(!lst_acc.isEmpty());

        List<Opportunity> lst_opp = BI_DataLoad.loadOpportunitiesforButton(2,lst_acc[0].Id);
        system.assert(!lst_opp.isEmpty());

        List<NE__Order__c> lst_ord = new List<NE__Order__c>();
        for(Opportunity opp :lst_opp){



            //Map<Id,NE__order__c> map_orders = new Map<Id,NE__order__c>([SELECT IdNE__One_Time_Fee_Total__cNE__Recurring_Charge_Total__cCurrencyISOCodeFROM NE__Order__c WHERE Id IN :set_ordID]);

             NE__Order__c order = new NE__Order__c();
                order.NE__OptyId__c = opp.Id;
                order.NE__One_Time_Fee_Total__c = 0;
                order.NE__Recurring_Charge_Total__c = 0;
                order.CurrencyISOCode = 'USD';
                order.NE__AccountId__c = opp.AccountId;

                lst_ord.add(order);
        }

        insert lst_ord;
        //Numero de OrderItems por Order:
        Integer K = 2;
        List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();
        for(NE__Order__c ord :lst_ord){
            for(Integer j = 0; j < K; j++){
                NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = ord.Id,
                                                             NE__Qty__c = 2,
                                                             NE__Asset_Item_Account__c = ord.NE__AccountId__c,
                                                             NE__Account__c = ord.NE__AccountId__c,
                                                             NE__Status__c = 'Active',
                                                             CurrencyISOCode = 'EUR',
                                                             NE__OneTimeFeeOv__c = 20,
                                                             NE__RecurringChargeOv__c = 10,
                                                             BI_Recalcular_valores_economicos__c = true);
                lst_oi.add(oit);
            }

        }

        insert lst_oi;
            

        List<NE__OrderItem__c> lst_oi_check = [select Id from NE__OrderItem__c WHERE BI_Recalcular_valores_economicos__c = true];
        system.assert(!lst_oi_check.isEmpty());

        Test.startTest();
    	System.enqueueJob(new BI_Recalcular_NE_Orders(null));
    	Test.stopTest();

        //Si el desarrollo ha pasado ha dejado el campo a true en NE__Order__c
        List<NE__Order__c> lst_order_check = [select Id from NE__Order__c WHERE BI_Recalcular_valores_economicos_Opp__c = true];
        system.assert(!lst_order_check.isEmpty());
    }
	
}