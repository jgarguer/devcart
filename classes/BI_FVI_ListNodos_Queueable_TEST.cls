@isTest
private class BI_FVI_ListNodos_Queueable_TEST {
		    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Adrián Caro             
    Company:       Accenture
    Description:   test to BI_FVI_ListNodos_Queueable class

    History: 

    <Date>                  <Author>                <Change Description>   
    06/02/2017              Adrián Caro             Initial Version
    20/09/2017              Antonio Mendivil        -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	
	@isTest 
	private static void BI_FVI_ListNodos_Queueable_TEST() {

		BI_TestUtils.throw_exception = true;

		Id idProfile =BI_DataLoad.searchAdminProfile();
		List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
		system.runAs(lst_user[0]){		
		User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
			//dataLoad();
			List<RecordType> lst_rt = [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName = 'BI_FVI_NAV' OR DeveloperName = 'BI_FVI_NAG' 
																												  OR DeveloperName = 'BI_FVI_NAT'
			                           																			  OR DeveloperName = 'BI_FVI_NCP'
			                          																			  OR DeveloperName = 'BI_FVI_Ciclo_completo'
			                           																			  OR DeveloperName = 'BI_FVI_Contrato'];


	       	Id rt_NCP,rt_NAT,rt_NAV,rt_NAB,rt_NAG,rt_NCO,rt_opp,rt_Cont;
	       
	        for(RecordType rt : lst_rt)
        	{
	            if(rt.DeveloperName == 'BI_FVI_NCP'){rt_NCP = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAV'){rt_NAV = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAT'){rt_NAT = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_NAG'){rt_NAG = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_Ciclo_completo'){rt_opp = rt.Id;}
	            else if(rt.DeveloperName == 'BI_FVI_Contrato'){rt_Cont = rt.Id;}
       		}

			BI_FVI_Nodos__c nodo_NCP = new BI_FVI_Nodos__c(
	            Name = 'NCP_TEST', 
	            RecordTypeId = rt_NCP,
	            BI_FVI_Pais__c = Label.BI_Peru
        	);
        	insert nodo_ncp;

        	BI_FVI_Nodos__c ncp = [SELECT Id FROM BI_FVI_Nodos__c WHERE Name = 'NCP_TEST'];
	        List<BI_FVI_Nodos__c> lst_nodosInsert = new List<BI_FVI_Nodos__c>();
	        
	        BI_FVI_Nodos__c	nodo_NAG = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAG_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAG
	        );

	        BI_FVI_Nodos__c	nodo_NAT = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAT_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAT,
	        	BI_FVI_NodoPadre__c = ncp.Id
	        );

	        BI_FVI_Nodos__c	nodo_NAV = new BI_FVI_Nodos__c(
	        	BI_FVI_Activo__c = true,
	        	Name = 'NAV_TEST',
	        	BI_FVI_Pais__c =  Label.BI_Peru,
	        	RecordTypeId = rt_NAV,
	        	BI_FVI_NodoPadre__c = ncp.Id
	        );

	        lst_nodosInsert.add(nodo_NAG);
	        lst_nodosInsert.add(nodo_NAT);
	        lst_nodosInsert.add(nodo_NAV);

	        insert lst_nodosInsert;
	        system.debug('list nodos ultima ====>>>' + lst_nodosInsert);


			NE__Lov__c lov = new NE__Lov__c(
				Name = 'N_CLIENTES_VOLATIL_TEMPORAL_ADQ',
				NE__Active__c = true,
				Country__c = Label.BI_Peru,
				NE__Type__c = 'NODOS',
				NE__Value1__c = '2',
				NE__Value2__c = '2',
				NE__Value3__c = '2'
			);
			insert lov;

			List <Account> lstacc = new List <Account>();

			Account cuenta1 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431271525',
			    CurrencyIsoCode = 'MXN',
			    BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
                BI_Territory__c = 'test'
			);

			Account cuenta2 = new Account(
			    Name = 'CLIENTE_TEST2',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431281525',
			    CurrencyIsoCode = 'MXN',
			    BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
			   BI_Subsegment_Regional__c = 'test',
			   BI_Territory__c = 'test'
			);

			Account cuenta3 = new Account(
			    Name = 'CLIENTE_TEST3',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431281526',
			    CurrencyIsoCode = 'MXN',
			    BI_FVI_Tipo_Planta__c = 'No Cliente',
			   BI_Segment__c = 'test',
               BI_Subsegment_Regional__c = 'test',
               BI_Territory__c = 'test'
			);
			lstacc.add(cuenta1);
			lstacc.add(cuenta2);
			lstacc.add(cuenta3);
			insert lstacc;


            List<BI_FVI_Nodo_Cliente__c> lst_noCl = new List<BI_FVI_Nodo_Cliente__c>();

			BI_FVI_Nodo_Cliente__c nc1 = new BI_FVI_Nodo_Cliente__c(
	        	BI_FVI_IdCliente__c = cuenta1.Id,
	        	BI_FVI_IdNodo__c  = lst_nodosInsert[0].Id
	        );
	        BI_FVI_Nodo_Cliente__c nc2 = new BI_FVI_Nodo_Cliente__c(
	        	BI_FVI_IdCliente__c = cuenta2.Id,
	        	BI_FVI_IdNodo__c  = lst_nodosInsert[1].Id
	        );
	        BI_FVI_Nodo_Cliente__c nc3 = new BI_FVI_Nodo_Cliente__c(
	        	BI_FVI_IdCliente__c = cuenta3.Id,
	        	BI_FVI_IdNodo__c  = lst_nodosInsert[2].Id
	        );
	        lst_noCl.add(nc1);
	        lst_noCl.add(nc2);
	        lst_noCl.add(nc3);
	        insert lst_noCl;
	        Integer Index = 0;
	        Set <String> setpais = new Set <String>();
	        setpais.add('Peru');

			List <BI_FVI_MailConfiguration__mdt> lstMail = [Select Label, Correo__c from BI_FVI_MailConfiguration__mdt];
			BI_FVI_MailConfiguration__mdt mailConfig = new BI_FVI_MailConfiguration__mdt();
				lstMail.add(mailConfig);

			//insert lstMail;

	        System.enqueueJob(new BI_FVI_ListNodos_Queueable(lst_noCl,Index,setpais));


	    }
	}
	
}