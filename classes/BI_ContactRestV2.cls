/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   Webservice for manage contacts.

History:

<Date>            <Author>          <Description>
25/05/2017        Oscar Ena        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@RestResource(urlMapping='/contactManagement/v1/customers/*/contacts/*')
global with sharing class BI_ContactRestV2  {

    public static final String API_VERSION = RestContext.request.requestURI.split('/')[2];
    public static final String SERVER_ROOT = System.URL.getSalesforceBaseURL().toExternalForm();
    public static final String API_ROOT = SERVER_ROOT+'/contactManagement/'+API_VERSION;
    public static final String TGS_ACTIVE_CONTACT = 'TGS_Active_Contact';
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that creates a contact asociated with a customer given in URL as parameter

    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:

    <Date>            <Author>          <Description>
    02/06/2017        Oscar Ena        Initial version
    29/11/2017        Gawron, Julián   Change Response Location SF Id
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpPost
    global static void createContact(){
        
        FS_TGS_RestWrapperFullStack.GenericResponse obj = new FS_TGS_RestWrapperFullStack.GenericResponse();
        
        try {
                        
            string sserialize = RestContext.request.requestBody.tostring();
            system.debug('BI_ContactRestV2.doPost ## deserialize: ' + sserialize);
            FS_TGS_RestWrapperFullStack.ContactRequestType wContact = (FS_TGS_RestWrapperFullStack.ContactRequestType)Json.deserialize(sserialize, FS_TGS_RestWrapperFullStack.ContactRequestType.Class);
            system.debug('BI_ContactRestV2.doPost ## wContact: ' + wContact);

            String idAccountFullStack = RestContext.request.requestURI.split('/')[4];
            String idContactFullStack = RestContext.request.requestURI.split('/')[6];
                       
            List<Account> queryAccount = [SELECT id, Name, BI_Identificador_Externo__c FROM Account WHERE BI_Identificador_Externo__c = : idAccountFullStack];
            System.debug('BI_ContactRestV2.doPost ## queryAccount '+queryAccount);
            
            Contact oContact = new Contact();

            if (!queryAccount.isEmpty()){

                oContact.FS_TGS_Id_Contacto_FullStack__c = idContactFullStack;
                oContact.AccountId = queryAccount.get(0).id;
                oContact.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Contact.SObjectType, TGS_ACTIVE_CONTACT); 
                
                BI_RestHelper.upsertContact(oContact, wContact);

//              RestContext.response.headers.put('Location', API_ROOT+'/contacts/'+idContactFullStack);
                RestContext.response.headers.put('Location', oContact.Id);  //JEG
                if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
                if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
                RestContext.response.statuscode = 201;

            } else {

                System.debug('BI_ContactRestV2.doPost :: ACCOUNT NOT FOUND');
                RestContext.response.statuscode = 404;
                obj.error = 'SVC 1006';
                obj.message = 'Resource '+idAccountFullStack+' does not exist';
            }
              
            
        } catch (Exception exc) {

            RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
            RestContext.response.headers.put('errorMessage',exc.getMessage());            
            System.debug('BI_ContactRestV2.doPost :: EXCEPTION ' +exc.getMessage());
            BI_LogHelper.generate_BILog('BI_ContactRestV2.createContact', 'BI_EN', Exc, 'Web Service');
            
        }

        RestContext.response.responseBody = Blob.valueof(Json.serializepretty(obj, true));
        System.debug('BI_ContactRestV2.doPost :: RestContext.response ' +RestContext.response);
        //return obj;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that modifies an existing contact 

    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:

    <Date>            <Author>          <Description>
    02/06/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpPut
    global static void updateContact() {

        FS_TGS_RestWrapperFullStack.GenericResponse obj = new FS_TGS_RestWrapperFullStack.GenericResponse();

        try {

            string sserialize = RestContext.request.requestBody.tostring();
            system.debug('BI_ContactRestV2.doPut ## deserialize: ' + sserialize);
            FS_TGS_RestWrapperFullStack.ContactRequestType wContact = (FS_TGS_RestWrapperFullStack.ContactRequestType)Json.deserialize(sserialize, FS_TGS_RestWrapperFullStack.ContactRequestType.Class);
            system.debug('BI_ContactRestV2.doPut ## wContact:' + wContact);

            String idAccountFullStack = RestContext.request.requestURI.split('/')[4];
            String idContactFullStack = RestContext.request.requestURI.split('/')[6];

            List<Contact> queryContact = [SELECT id, Birthdate, TGS_Language__c, Account.BI_Identificador_Externo__c, Email, Phone, Fax, Department, 
                                            Name, FirstName, LastName, Salutation, BI_Genero__c, BI_Activo__c 
                                            FROM Contact 
                                            WHERE FS_TGS_Id_Contacto_FullStack__c = : idContactFullStack];

            if (!queryContact.isEmpty()){

                Contact oContact = queryContact.get(0);

                if (oContact.Account.BI_Identificador_Externo__c == idAccountFullStack){

                    BI_RestHelper.upsertContact(oContact, wContact);

                    if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
                    if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
                    RestContext.response.statuscode = 200;

                } else {

                    System.debug('BI_ContactRestV2.doPut ## Customer With ID '+idAccountFullStack+' Is not the parent of Contact With ID '+idContactFullStack);
                    RestContext.response.statuscode = 400;
                    obj.error = 'SVC 0001';
                    obj.message = 'Generic Client Error: Customer With ID '+idAccountFullStack+' Is not the parent of Contact With ID '+idContactFullStack;
                }

            }else {

                System.debug('BI_ContactRestV2.doPut :: CONTACT NOT FOUND: Resource '+idContactFullStack+' does not exist');
                RestContext.response.statuscode = 404;
                obj.error = 'SVC 1006';
                obj.message = 'Resource '+idContactFullStack+' does not exist';
            }

        } catch (Exception exc){

            RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
            RestContext.response.headers.put('errorMessage',exc.getMessage());            
            System.debug('BI_ContactRestV2.doPut :: EXCEPTION ' +exc.getMessage());
            BI_LogHelper.generate_BILog('BI_ContactRestV2.updateContact', 'BI_EN', Exc, 'Web Service');

        }

        RestContext.response.responseBody = Blob.valueof(Json.serializepretty(obj, true));
        System.debug('BI_ContactRestV2.doPut :: RestContext.response ' +RestContext.response);
    }

    


}