public class TGS_CostCenterHandler {
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta García
    Company:       Deloitte
    Description:   Field Cost_Center__c fill
    
    History

    <Date>            <Author>                      <Description>
    16/12/2015        Marta García                  Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void  fillCostCenter (List<NE__Order__c> newOrder, List<NE__Order__c> oldOrder){
        
        
        Map<Id,Id> mapOldOrderCC = new Map<Id,Id>();
        Map<Id,Id> mapNewdOrderCC = new Map<Id,Id>();
        Map<Id,Id> mapOldOrderPayer = new Map<Id,Id>();
        Map<Id,Id> mapNewdOrderPayer = new Map<Id,Id>();
       
        for(NE__Order__c o: newOrder){
            
            mapNewdOrderPayer.put(o.id, o.NE__BillAccId__c);
           
            mapNewdOrderCC.put(o.id, o.Cost_Center__c);
            
        }
        
        for(NE__Order__c o: oldOrder){
            
            mapOldOrderPayer.put(o.id, o.NE__BillAccId__c);
           
            mapOldOrderCC.put(o.id, o.Cost_Center__c);
            
        }
        
        for(NE__Order__c order :  newOrder){
            system.debug('$$$$$$$$$$$$$$$$' + String.isnotblank(order.HoldingId__c));
            if(String.isnotblank(order.HoldingId__c)){
                
                system.debug(mapNewdOrderPayer.get(order.id));
                system.debug(mapOldOrderPayer.get(order.id));
                
                if((mapNewdOrderPayer.get(order.id) != mapOldOrderPayer.get(order.id))){ // Change NE__BillAccId__c
                    
                    order.Cost_Center__c =  mapNewdOrderPayer.get(order.id); // Fill Cost_Center__c
                    system.debug('Cost Center Value '+  order.Cost_Center__c);
                    break;
                }else if( mapNewdOrderCC.get(order.id) != mapOldOrderCC.get(order.id)){ //Change Cost_Center__c 
                    order.NE__BillAccId__c = mapNewdOrderCC.get(order.id); // Fill NE__BillAccId__c
                }else if(mapOldOrderCC.get(order.id) == null){ // Cost_Center__c is blank
                    order.Cost_Center__c =  mapNewdOrderPayer.get(order.id); // Fill Cost_Center__c
                }   
            
                         
            
            }
            
        }
    }
    
     /*OI 01-03-2016*/
    public static void deleteCase(List<NE__Order__c> oldOrder)
    {       
        List<String> IdCase    =    new List<String>();
        List<Case> nameCase    =    new List<Case>();
            
            for (NE__Order__c order : oldOrder)
            {    
                 system.debug('ORder: '+order );
                 if(order.Case__c != null)
                     IdCase.add(order.Case__c);
            }
    
            if(IdCase.size()>0)
            {
                nameCase = [Select id, Order__c, Asset__c From Case where id IN: IdCase];
            }
            
   
          
        delete nameCase; 
  
        
    }      
}