/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Job that return the 'Descuento para modelo' from a 'Linea de venta' and 'Linea de recambio' when a 'Solicitud' is canceled
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class BI_DevolverDescuentoModelo_JOB implements Queueable {
	
	Map <Id,Integer> toProcess;
    Map <Id,Integer> future;
    List <String> idSolicitud;

	public BI_DevolverDescuentoModelo_JOB(){}   
	 
    public BI_DevolverDescuentoModelo_JOB(Map <Id,Integer> mapa){
        System.debug('####constructor 2');
        Map <Integer, Map<Id,Integer>> aux = divide(mapa,200);
        toProcess= aux.get(0);
        future = aux.get(1);
    }

    public BI_DevolverDescuentoModelo_JOB(List<Id> idSol){
        System.debug('####Constructor 1');
        idSolicitud = idSol;
        Map <Integer, Map<Id,Integer>> aux = divide(searchDiscounts(idSol),200);
        toProcess = aux.get(0);
        future = aux.get(1);
    }

   	global void execute(QueueableContext context) {
		updateModelos(toProcess);
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that set the batch size at 'j' and return the values to process and the values to future process
    
    IN:            Map<Id, Integer>, Integer
    OUT:           map<Integer, map<Id, Integer>>
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public map<Integer, map<Id, Integer>> divide(Map<Id, Integer> mapa, Integer j){
    	Map<Integer, map<Id, Integer>> retorno = new Map<Integer, map<Id,Integer>>();
    	try{

    		if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

	        System.debug('####divido');	        
	          if(Test.isRunningTest()){
	          	j = 2;
	          }
	        if(mapa.size() > j){
	            Integer i = 0;
	          
	            for(Id ids : mapa.keySet()){
	                if(i == 0){
	                    retorno.put(0, new Map<Id, Integer>{ids => mapa.get(Ids)});         
	                }
	                else if(i < j ){
	                    retorno.get(0).put(ids, mapa.get(ids));                
	                }
	                
	                else if(i == j){
	                    retorno.put(1, new Map<Id, Integer>{ids => mapa.get(Ids)});         
	                }
	                else{                
	                     retorno.get(1).put(ids, mapa.get(ids));
	                }
	                i++;
	            }
	        }
	        
	        else{
	        
	            retorno.put(0, mapa);
	            retorno.put(1,null);  
	        }
    	}
    	catch(Exception e){
    		BI_LogHelper.generate_BILog('BI_DevolverDescuentoModelo_JOB.divide', 'BI_EN', e, 'BI_Slicitud_envio_CTRL.cancel_Sol');
    	}
        return retorno;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that search 'Descuentos para modelo' to process from 'Linea de venta' and 'Linea de recambio' of the canceled 'Solicitud'
    
    IN:            Id
    OUT:           map<Id, Integer>
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Map<Id,Integer> searchDiscounts(List <Id> idSol){
        System.debug('####Busco descuentos');
        Map <Id, Integer> modelos = new Map<Id, Integer>();
        try{

        	if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

	        List <BI_Linea_de_Venta__c> lst_LV = [SELECT Id, BI_Codigo_de_descuento_de_modelo__c, BI_Codigo_de_descuento_de_SIM__c FROM BI_Linea_de_Venta__c WHERE BI_Solicitud__c IN: idSol AND (BI_Codigo_de_descuento_de_modelo__c != null OR BI_Codigo_de_descuento_de_SIM__c != null)];
	        List <BI_Linea_de_Recambio__c> lst_LR = [SELECT Id, BI_Codigo_de_descuento_de_modelo__c, BI_Codigo_de_descuento_de_SIM__c FROM BI_Linea_de_Recambio__c WHERE BI_Solicitud__c IN: idSol AND (BI_Codigo_de_descuento_de_modelo__c != null OR BI_Codigo_de_descuento_de_SIM__c != null)];
	        System.debug('****Nº LR: ' +  lst_LR.size());
	        if(!lst_LV.isEmpty()){

	            for(BI_Linea_de_Venta__c lv : lst_LV){
	            	System.debug('****Cod DM: ' + lv.BI_Codigo_de_descuento_de_modelo__c);
	                if(lv.BI_Codigo_de_descuento_de_modelo__c!=null){
	                    if(modelos.containsKey(lv.BI_Codigo_de_descuento_de_modelo__c)){
	                        modelos.put(lv.BI_Codigo_de_descuento_de_modelo__c,modelos.get(lv.BI_Codigo_de_descuento_de_modelo__c) + 1);
	                    }
	                    else{
	                        modelos.put(lv.BI_Codigo_de_descuento_de_modelo__c, 1);
	                    }
	                }

	                if(lv.BI_Codigo_de_descuento_de_SIM__c!=null){
	                    if(modelos.containsKey(lv.BI_Codigo_de_descuento_de_SIM__c)){
	                        modelos.put(lv.BI_Codigo_de_descuento_de_SIM__c,modelos.get(lv.BI_Codigo_de_descuento_de_SIM__c) + 1);
	                    }
	                    else{
	                        modelos.put(lv.BI_Codigo_de_descuento_de_SIM__c, 1);
	                    }
	                }
	            }
	        }

	        if(!lst_LR.isEmpty()){

	            for(BI_Linea_de_Recambio__c lr : lst_LR){

	                if(lr.BI_Codigo_de_descuento_de_modelo__c!=null){
	                	System.debug('****Cod DM: ' + lr.BI_Codigo_de_descuento_de_modelo__c);
	                    if(modelos.containsKey(lr.BI_Codigo_de_descuento_de_modelo__c)){
	                        modelos.put(lr.BI_Codigo_de_descuento_de_modelo__c,modelos.get(lr.BI_Codigo_de_descuento_de_modelo__c) + 1);
	                    }
	                    else{
	                        modelos.put(lr.BI_Codigo_de_descuento_de_modelo__c, 1);
	                    }
	                }

	                if(lr.BI_Codigo_de_descuento_de_SIM__c!=null){
	                    if(modelos.containsKey(lr.BI_Codigo_de_descuento_de_SIM__c)){
	                        modelos.put(lr.BI_Codigo_de_descuento_de_SIM__c,modelos.get(lr.BI_Codigo_de_descuento_de_SIM__c) + 1);
	                    }
	                    else{
	                        modelos.put(lr.BI_Codigo_de_descuento_de_SIM__c, 1);
	                    }
	                }
	            }
	        }
    	}
    	catch(Exception e){
    		BI_LogHelper.generate_BILog('BI_DevolverDescuentoModelo_JOB.searchDiscounts', 'BI_EN', e, 'BI_Slicitud_envio_CTRL.cancel_Sol');
    	}

        return modelos;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that update 'Descuentos para modelo' quantity
    
    IN:            Map<Id, Integer>
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void updateModelos(Map<Id,Integer> modelos){
    	try{

    		if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

	        System.debug('####updateo cosas');
	        if(!modelos.isEmpty()){
	            
	            List <BI_Descuento_para_modelo__c> lst_DM = [SELECT Id, BI_Cantidad__c, BI_Cantidad_disponible__c FROM BI_Descuento_para_modelo__c WHERE Id in: modelos.keySet() ];

	            for(BI_Descuento_para_modelo__c dm : lst_DM){

	            	if(dm.BI_Cantidad_disponible__c == null){
	            		if(dm.BI_Cantidad__c <= modelos.get(dm.Id)){
	            			dm.BI_Cantidad_disponible__c=dm.BI_Cantidad__c;
	            		}
	            		else{
	            			dm.BI_Cantidad_disponible__c=modelos.get(dm.Id);
	            		}
	            	}
	                else if(dm.BI_Cantidad_disponible__c + modelos.get(dm.id) > dm.BI_Cantidad__c){

	                    dm.BI_Cantidad_disponible__c = dm.BI_Cantidad__c;
	                }
	                else{

	                    dm.BI_Cantidad_disponible__c += modelos.get(dm.Id);
	                }
	            }
	            BI_GlobalVariables.stopTriggerDesc=true;
	            update lst_DM;
	            BI_GlobalVariables.stopTriggerDesc=false;
	        }
	        nextExecute();
    	}
        catch(Exception e){
    		BI_LogHelper.generate_BILog('BI_DevolverDescuentoModelo_JOB.updateModelos', 'BI_EN', e, 'BI_Slicitud_envio_CTRL.cancel_Sol');
    	}
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that re run job
    
    IN:            
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void nextExecute(){        
        try{

        	if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

	        if(future != null){
	            System.enqueueJob(new BI_DevolverDescuentoModelo_JOB(future));
	        }
	        else{
	            System.debug('He terminado');
	        }
	    }
		catch(Exception e){
    		BI_LogHelper.generate_BILog('BI_DevolverDescuentoModelo_JOB.nextExecute', 'BI_EN', e, 'BI_Slicitud_envio_CTRL.cancel_Sol');
    	}
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that run the job for first time
    
    IN:            Id
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void executeJobDevolverDescuentos(List <Id> idsol){

    	try{

    		if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

    		System.enqueueJob(new BI_DevolverDescuentoModelo_JOB(idSol));
    	}
    	catch(Exception e){
    		BI_LogHelper.generate_BILog('BI_DevolverDescuentoModelo_JOB.executeJobDevolverDescuentos', 'BI_EN', e, 'BI_Slicitud_envio_CTRL.cancel_Sol');
    	}

    }

}