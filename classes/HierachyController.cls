public with sharing class HierachyController {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for manage hierarchies to Relationship management at Portal Platino 
    
    History:
    
    <Date>            <Author>          	<Description>
    24/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public list<Hierarchy__c> listHierarchy {get; set;}
	public Contact contactP {get; set;}
	public boolean saved {get; set;}
	public set<string> setUser {get; set;}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor Class 
    
    History:
    
    <Date>            <Author>          	<Description>
    24/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public HierachyController (ApexPages.StandardController Controller) {
		try{
			contactP = (Contact)Controller.getRecord();
			listHierarchy = [Select Id, ShowPersonalData__c, Description__c, User__c, Delete__c, Contact__c, Nivel_de_escalado__c From Hierarchy__c where Contact__c = : contactP.Id];
			if(listHierarchy.isEmpty()){
				listHierarchy = new list<Hierarchy__c>();
			}
			String ContactId = (ApexPages.currentPage().getParameters().get('id')!=null)?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('id')):null;
			system.debug('**********listHierarchy: ' +listHierarchy);
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('HierachyController.HierachyController', 'Portal Platino', Exc, 'Class');
	    }
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   add a new item for the list 
    
    History:
    
    <Date>            <Author>          	<Description>
    24/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void addItem(){
		try{
			Hierarchy__c item = new Hierarchy__c();
			item.Contact__c = contactP.Id;
			listHierarchy.add(item);
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('HierachyController.addItem', 'Portal Platino', Exc, 'Class');
	    }
	}
	
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method to save the new Hierarchy
    
    History:
    
    <Date>            <Author>          	<Description>
    24/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void save(){
		try{
			list<Hierarchy__c> listInsert = new list<Hierarchy__c>();
			list<Hierarchy__c> listDelete = new list<Hierarchy__c>();
			setUser = new set<string>();
			set<string> EscaldadeLevelSet = new set<string>();
			set<string> setUserToAbout = new set<string>();
			list<User> listUser;
			map<string, string> mapUserAbout = new map<string,string>();
			for(Hierarchy__c item : listHierarchy){
				setUserToAbout.add(item.User__c);
			}
			if(!setUserToAbout.isEmpty()){
				listUser = [Select Id, AboutMe From User where id IN : setUserToAbout];
			}
			for(User item : listUser){
				mapUserAbout.put(item.Id, item.AboutMe);
			}
			//Validate if have a other "nivel de escalado" existing
			for(Hierarchy__c item : listHierarchy){
				if(item.Delete__c && item.Id != null){
					listDelete.add(item);
				}else if (item.Delete__c == false){
					if(item.Nivel_de_escalado__c != null && EscaldadeLevelSet.contains(item.Nivel_de_escalado__c)){
						item.addError(Label.BI_NivelEscalado + ' ' + item.Nivel_de_escalado__c + ' ' + Label.BI_Duplicado);
						return;
					}
					if(item.Nivel_de_escalado__c != null){
						EscaldadeLevelSet.add(item.Nivel_de_escalado__c);
					}
					listInsert.add(item);
					if(setUser.contains(item.User__c)){
						User usr = [Select Id, Name From User where Id = : item.User__c];
						item.addError(Label.BI_UsuarioHierarchy + ' ' + usr.Name + ' ' + Label.BI_Duplicado);
						return;
					}
					setUser.add(item.User__c);
					item.AboutMe__c = mapUserAbout.get(item.User__c);
				}
			}
			
			if(!listDelete.isEmpty()){
				delete listDelete;
			}
			if(!listInsert.isEmpty()){
				upsert listInsert;
			}
			saved = true;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('HierachyController.save', 'Portal Platino', Exc, 'Class');
	    }
		
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method go to Contact page
    
    History:
    
    <Date>            <Author>          	<Description>
    24/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public pageReference goToContact (){
		try{
		PageReference page = new PageReference ('/'+ contactP.Id);
		return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('HierachyController.goToContact', 'Portal Platino', Exc, 'Class');
		   return null;
	    }
	}
}