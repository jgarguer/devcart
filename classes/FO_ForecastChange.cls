/*********************************************
 * Author: JAvier Lopez
 * Description:  to change the value of a  forecast in Adjustment Page
 * 
 * <version>	<date>		<description>
 * 1.0			2018/03/05	initial	 
 * *******************************************/
public class FO_ForecastChange {

    
    @AuraEnabled
    public static String changeFore(String Id,Double value,Boolean manager){
        
        FO_Forecast_Futuro__c fo = new FO_Forecast_Futuro__c();
        fo.Id =Id;
        if(manager==false)
        	fo.FO_Total_Forecast_Ajustado__c=value;
        else
            fo.FO_Ajuste_Forecast_Manager__c=value;
        try{
        	Database.SaveResult sv = Database.update(fo);    
            if(sv.isSuccess()==false){
                Database.Error err =sv.getErrors().get(0);
                return 'Error: '+err.getMessage();
            }
        }catch(Exception e){
            System.debug(e.getStackTraceString());
            return 'Error: '+e.getStackTraceString() ;
        }
        return 'OK';
    }
}