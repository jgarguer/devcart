global class BI_FOCO_CountryDateHelperBatch implements Database.Batchable<sObject>, Database.Stateful {
	Map<String, Date> minDateMap = new Map<String, Date>();
	Map<String, Date> maxDateMap = new Map<String, Date>();

	String query;
	private String country;
	private Map<String, BI_FOCO_Pais__c> mcs;

	global BI_FOCO_CountryDateHelperBatch(String c) {
		this.country = c;
		mcs =  BI_FOCO_Pais__c.getAll();
		query = 'SELECT BI_Fecha_Inicio_Periodo__c, BI_Cliente__r.BI_Country__c FROM BI_Registro_Datos_FOCO__c where BI_Cliente__r.BI_Country__c = :country';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<BI_Registro_Datos_FOCO__c> scope) {
		
		for (BI_Registro_Datos_FOCO__c b : scope) {
			//String country = b.BI_Cliente__r.BI_Pais_ref__r.Name;
			Date dt = b.BI_Fecha_Inicio_Periodo__c;
 
			if (minDateMap.get(country) == null) {
				minDateMap.put(country, dt);
			}

			if (maxDateMap.get(country) == null) {
				maxDateMap.put(country, dt);
			}

			if (minDateMap.get(country) > dt) {
				minDateMap.put(country, dt);
			}

			if (maxDateMap.get(country) < dt) {
				maxDateMap.put(country, dt);
			}
		}
	}

	global void finish(Database.BatchableContext BC) {
		List<BI_FOCO_Dates_by_Country__c> toDelete = [SELECT Id from BI_FOCO_Dates_by_Country__c WHERE Name = :country LIMIT 1];
		List<BI_FOCO_Dates_by_Country__c> toInsert = new List<BI_FOCO_Dates_by_Country__c>();

		for (String c : minDateMap.keySet()) {
			BI_FOCO_Dates_by_Country__c fd = new BI_FOCO_Dates_by_Country__c();
			fd.Name = c;
			fd.Min_Date__c = minDateMap.get(c);
			fd.Max_Date__c = maxDateMap.get(c);
			toInsert.add(fd);
		}

		delete toDelete;
		insert toInsert;
	

		BI_FOCO_BatchUpdFOCOFromOppty batchUp = new BI_FOCO_BatchUpdFOCOFromOppty(country);
		Id updBatchId = Database.executeBatch(batchUp, (Integer)mcs.get(country).Batch_Size__c);
	}
	
}