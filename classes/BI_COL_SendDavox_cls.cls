global class BI_COL_SendDavox_cls implements Database.Batchable<SObject>, Database.Stateful,Database.AllowsCallouts 
{
	public string TipoNovedad;
	public string Query;	
	public Boolean tst{get;set;}
	public Integer opc{get;set;}
	public String packagecode{get;set;}
	public String codPaquete;

	global BI_COL_SendDavox_cls(Boolean bndTst,Integer op)
	{
		tst=bndTst;
		opc=op+1;
		System.debug('En el constructor opc='+opc+' packagecode='+packagecode);
		//TipoNovedad='ADICIONAR_CUENTA';
		System.debug(Query);

	}
			
	global Database.Querylocator start(Database.Batchablecontext bc)
	{
		System.debug('Entra al start de davox_BatchEnviarADavox');
		System.Debug('Consulta del batch: ' + Query);
		return Database.getQueryLocator(Query);
	}

	global void execute(Database.Batchablecontext bc, List<sObject> scope)
	{
		System.debug('Entra al execute de davox_BatchEnviarADavox');
		System.Debug('Cantidad de novedades: ' + scope.Size()+ ' TipoNovedad --->'+TipoNovedad+ ' packagecode--->'+packagecode );
		codPaquete=packagecode;
		string strRespuestaServicio;
		if (scope.Size() > 0)
		{
			try
			{
				BI_COL_Davox_wsdl.BasicServiceWSSOAP ws = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
				ws.timeout_x=60000;
				String trama=armarXml(tipoNovedad,scope);
				System.Debug('\n\n EL XML ENVIADO AL SERVICIO WEB ES: ' + trama+' \n\n');
				strRespuestaServicio = ws.add(trama);
				System.Debug('\n\n ####### Respuesta del XML: ' + strRespuestaServicio+' ####### \n\n');
				ActualizaNovedades(scope, strRespuestaServicio);
				
			}
			catch(Exception ex)
			{
				List<BI_Log__c> novedades=scope;
				for(BI_Log__c n : novedades)
				{
					n.BI_COL_Causal_Error__c ='Error enviando la Novedad a DAVOX: '+ex.getMessage();
					n.BI_COL_Estado__c='Error Conexion';
				}
				update novedades;
				System.Debug('Error del XML: ' + ex.getMessage());
			}
		}
	}
	
	public String armarXml(String typeX,List<BI_Log__c> novedades)
	{
		System.debug('Entra a armarXml');
		XmlStreamWriter xmlWriter=new XmlStreamWriter();
		xmlWriter.writeStartDocument('utf-8','1.0');
		xmlWriter.writeStartElement(null, 'WS', null);

		xmlWriter.writeStartElement(null, 'USER', null);
		xmlWriter.writeCharacters('USER');
		xmlWriter.writeEndElement();
            
		xmlWriter.writeStartElement(null, 'PASSWORD', null);
		xmlWriter.writeCharacters('PASSWORD');
		xmlWriter.writeEndElement();
		xmlWriter.writeStartElement(null, 'DATEPROCESS', null);
		xmlWriter.writeCharacters(String.valueOf(System.Today()));
		xmlWriter.writeEndElement();
		xmlWriter.writeStartElement(null, 'PACKCODE', null);
		xmlWriter.writeCharacters(getPackageCode());
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'SEPARATOR', null);
		xmlWriter.writeCharacters('Õ');
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'TYPE', null);
		xmlWriter.writeCharacters(typeX);
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'VALUES', null);
		//ConstruirXML(novedades,typeX);
		for (BI_Log__c n : novedades)
		{
				xmlWriter.writeStartElement(null, 'VALUE', null);
				xmlWriter.writeCharacters(n.BI_COL_Informacion_Enviada__c);
				xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();
				
		xmlWriter.writeStartElement(null, 'COUNT', null);
		xmlWriter.writeCharacters(String.valueOf(novedades.size()));
		xmlWriter.writeEndElement();
		System.debug('\n\n Termina de armar el XML packagecode:'+ packagecode+'\n\n ');
		xmlWriter.writeEndElement();
		
		
		string tramaDavox = xmlWriter.getXmlString();
		tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
		xmlWriter = new XmlStreamWriter();
		
		return tramaDavox;
	}
	
	public static void ActualizaNovedades(List<BI_Log__c> novedades, string strXmlRespuesta)
	{
		System.debug('Entra a ActuaizarNovedades');
		Map<String,String> mapNovError=new Map<String,String>(); 
		System.debug('Entra a ActuaizarNovedades');
		if (strXmlRespuesta.length() != 0)
		{
			System.debug('La respuesta tiene una longitud superior a 0');
			XmlStreamReader reader = new XmlStreamReader(strXmlRespuesta);
			string strSeparador;
			string RespuestaDavox;
			Map<String, String> CatalogoCodigos = LeeCatalogoCodigos();
			
			
			while (reader.hasNext())
			{
				System.debug('Se recorre el XmlStreamReader');
				if (reader.getEventType() == XmlTag.START_ELEMENT)
				{
					if (reader.getLocalName() == 'SEPARATOR')
					{
						strSeparador = Parse(reader);
					}
					else if (reader.getLocalName() == 'Value')
					{
						RespuestaDavox = Parse(reader);
						string[] respuestaNovedad = RespuestaDavox.split(strSeparador);
						mapNovError.put(respuestaNovedad[0],respuestaNovedad[1]);
					}
				}
				reader.next();
			}
			if(mapNovError.size()>0)
			{
				System.debug('\n\n Ingreso al if MapNoveda \n\n');
				for(BI_Log__c n : novedades)
				{
					String rtaDavox='';
					
					if(mapNovError.containsKey(n.BI_COL_Cobro_Parcial__c))
					{
						rtaDavox=mapNovError.get(n.BI_COL_Cobro_Parcial__c);
					}
					else
					{
						rtaDavox=mapNovError.get(n.BI_COL_Modificacion_Servicio__c);
					}
					//System.debug('\n\n n.Cobro_Parcial__c'+n.BI_COL_Cobro_Parcial__c+'  n.Modificacion_servicio__c'+  n.Modificacion_servicio__c+ ' rtaDavox'+rtaDavox+' \n\n');
					n.BI_COL_Informacion_recibida__c=CatalogoCodigos.get(rtaDavox);
					if(rtaDavox!=null && rtaDavox.equals('100'))
					{
						n.BI_COL_Estado__c = 'Pendiente Sincronización';
					}
					else
					{
						n.BI_COL_Estado__c = 'Devuelto';
					}
				}
				update novedades;
			}
		}
		System.debug('Termina de actualizar novedades');
    }
    
	private static Map<String,String> LeeCatalogoCodigos()
	{
		Map<String, String> CatalogoCodigos = new Map<String, String>();
		
		for(BI_COL_RespuestasDavox__c c : [Select Name, BI_COL_Descripcion_Codigo__c from BI_COL_RespuestasDavox__c])
		{
			CatalogoCodigos.put(c.Name, c.BI_COL_Descripcion_Codigo__c);
		}
		
		return CatalogoCodigos;		
	}    
    
	private static String Parse(XmlStreamReader reader)
	{
		System.debug('Entra al Parse de xml');
		String strTextoDevuelve = '';
		
		while(reader.hasNext()){
			System.debug('Se recorre el reader en el Parse');
			if (reader.getEventType() == XmlTag.END_ELEMENT){
				break;
			} else if (reader.getEventType() == XmlTag.CHARACTERS){
				if (reader.getText() != null)
					strTextoDevuelve = reader.getText();
			}
			reader.next();
		}

		return strTextoDevuelve;
	}    
    
    
    global void finish(Database.BatchableContext bc)
    {
    	System.debug('Entra al finish de davox_BatchEnviarADavox');
		BI_COL_ScheduleSendDavox_cls ia=new BI_COL_ScheduleSendDavox_cls(opc);
		System.debug(codPaquete+' El codigo del paquete enviado --->'+packagecode);
		DateTime fechaActual= System.now().addMinutes(1);
		Integer minutos=fechaActual.minute();
		Integer hora=fechaActual.hour();
		Integer dia=fechaActual.day();
		integer mes=fechaActual.month();
		Integer anio=fechaActual.year();
		String sch = '0 '+minutos+' '+hora+' '+dia+' '+mes+' ? '+anio;
		if(!tst)
		{
			Id tarea=System.schedule('davox_BatchEnviarADavox: --->' +System.now(), sch, ia);
		}
    	System.debug('Finalizo y va para el siguiente paso '+ opc);
    }
    
    public Static String getPackageCode()
	{
		return Math.floor((Math.random()*10000)+1)+'-ASD654AS'+System.now();
	}
}