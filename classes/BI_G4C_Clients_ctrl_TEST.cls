@isTest
public with sharing class BI_G4C_Clients_ctrl_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Julio Alberto Asenjo García
Company:       everis
Description:   Test Class for the class BI_G4C_Clients_ctrl_TEST

History: 

<Date>                          <Author>                        <Change Description>
20/06/2016                      Julio Alberto Asenjo García     Initial version
31/05/2017                      Cristina Rodriguez              Commented BI_segment__c(Campaign)
20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c   
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static testMethod void testgetMyAccounts() {
        Test.startTest();
        NE__Catalog__c cat = new NE__Catalog__c (Name = 'test catalog', BI_Country__c = Label.BI_Argentina);
        insert cat;
        Campaign ca = new Campaign(Name = 'test',CurrencyIsoCode = 'USD', BI_SIMP_Opportunity_Type__c='Alta', EndDate = date.TODAY(),BI_Catalogo__c=cat.Id, IsActive=true, BI_Opportunity_Type__c = 'Convergente', BI_Country__c = Label.BI_Argentina);
        // BI_Segment__c = 'Mayoristas',
        
        ca.Status='In Progress';
        insert ca;
        Account a = new Account(Name='pruebaTest',OwnerId=userInfo.getUserId(),ShippingLatitude =3.5,ShippingLongitude =3.5, BI_Segment__c='Mayoristas', BI_Subsegment_Local__c='Mayoristas',BI_Territory__c = 'test');
        
        insert a;
        
        Contact c = new Contact(LastName='cpruebaTest', AccountId=a.Id);
        insert c;
        CampaignMember newMember = new CampaignMember(CampaignId = ca.Id, ContactId = c.Id);
        insert newMember;
        list<Account> lstAcc = new list<Account>();
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=Date.today(),StageName='Open',AccountId=a.Id,BI_Ciclo_ventas__c = 'Completo',OwnerId=UserInfo.getUserId(),BI_Oportunidad_suspendida_por_riesgo__c = true);
        opp.BI_No_Identificador_fiscal__c ='abc';
        insert opp;
        
        //   User usuarioTest = new User( FirstName = 'Miguel', LastName = 'Molina');
        //   insert usuarioTest;
        
        event ePr = BI_G4C_Test_Auxiliar.InsertEvent(a.Id);
        insert ePr;
        BI_G4C_Clients_ctrl apCl=new BI_G4C_Clients_ctrl();
        BI_G4C_Clients_ctrl.getMyAccounts();
        BI_G4C_Clients_ctrl.getFilterAccounts('','','','','misClientes');
        BI_G4C_Clients_ctrl.getFilterAccounts('CampaignId=\''+ca.Id+'\'','','','','misClientes');
        BI_G4C_Clients_ctrl.getFilterAccounts('OwnerId IN (\''+UserInfo.getUserId()+'\')','','','','miembrosEquipo');
        List<Id> hijos = new List<Id>();
        BI_G4C_Clients_ctrl.getFilterAccounts('OwnerId IN (\''+UserInfo.getUserId()+'\')  AND  StageName IN( \'F6 - Prospecting\')','','','','misClientesMiembrosEquipo');
        BI_G4C_Clients_ctrl.getFilterAccounts('StageName IN( \'Sin Estado \')','','','','todos');
        BI_G4C_Clients_ctrl.getFilterAccounts('OwnerId IN (\''+UserInfo.getUserId()+'\')  AND  StageName IN( \'Sin Estado\')','','','','');
        BI_G4C_Clients_ctrl.getFilterAccounts('Account.Name LIKE \'%a%\'','','','','');
        BI_G4C_Clients_ctrl.getState('CampaignId=\''+ca.Id+'\'', 'OwnerId IN (\''+UserInfo.getUserId()+'\')', 'StageName IN( \'F6 - Prospecting\')');
        BI_G4C_Clients_ctrl.getState('', '', '');
        BI_G4C_Clients_ctrl.getAuxFilter('CampaignId=\''+ca.Id+'\' AND  StageName IN( \'Sin estado\') AND OwnerId IN (\''+UserInfo.getUserId()+'\')AND Account.Name LIKE \'%a%\'');
        BI_G4C_Clients_ctrl.getAuxFilter('OwnerId IN (\''+UserInfo.getUserId()+'\')AND Account.Name LIKE \'%a%\'');
        BI_G4C_Clients_ctrl.getOptions(lstAcc);
        
        BI_G4C_Clients_ctrl.getEstadoOportunidades('', lstAcc);
        
        BI_G4C_Clients_ctrl.getEstadoOportunidades(ca.Id, lstAcc);
        lstAcc.add(a);
        
        BI_G4C_Clients_ctrl.getEstadoOportunidades('', lstAcc);
        BI_G4C_Clients_ctrl.setGeolocalization(ePr.Id,'');
        BI_G4C_Clients_ctrl.setGeolocalization('a','');
        BI_G4C_Clients_ctrl.getUserName();
        
        
        hijos.add(ca.Id);
        BI_G4C_Clients_ctrl.getListToString(hijos);
        BI_G4C_Clients_ctrl.getUsuarios('todos');
        BI_G4C_Clients_ctrl.getVisitados('["0012600000C44TBAAZ","0012600000CpCRxAAN","0012600000CpBTYAA3","0012600000CRyLrAAL"]','','','');
        BI_G4C_Clients_ctrl.getNoVisitados('["0012600000CmKdzAAF","0012600000CojCNAAZ","0012600000CSXNAAA5","0012600000CSX0wAAH","0012600000CSXMvAAP"]','','','');
        BI_G4C_Clients_ctrl.getFilterData();
        BI_G4C_Clients_ctrl.setFilterData('OwnerId IN (\'00526000000tmNsAAI\')');
        BI_G4C_Clients_ctrl.setFilterData('CampaignId=\''+ca.Id+'\'');
        BI_G4C_Clients_ctrl.setFilterData('CampaignId=\''+ca.Id+'\' AND  StageName IN( \'F6 - Prospecting\')');
        //  BI_G4C_Clients_ctrl.getAuxFilter('CampaignId=\'0012600000C44TBAAZ\' AND OwnerId = \'0012600000C44TBAAZ\'');
        BI_G4C_Clients_ctrl.getCountryPickList();
        
        BI_G4C_Clients_ctrl.getSaveCommercialAddress(''+a.Id, 'prueba', 'prueba', 'prueba', 'prueba', 'prueba', 'prueba');
        List<Account> lAccountEmpty = new List<Account>();
        lAccountEmpty.add(a);
        
        List<Account> listIdsNews= new List<Account>();
        listIdsNews.add(a);
        
        BI_G4C_Clients_ctrl.getOptions(lAccountEmpty);
        
        
        
        Test.stopTest();
    }
    
    static testMethod void testBIAccounts() {
        
          TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        list<User> lUs= BI_DataLoad.loadUsers(1,[SELECT Id FROM Profile WHERE Name = 'BI_Standard' LIMIT 1].Id,'Ejecutivo de Cliente');
        //   System.runAs(lUs.get(0)) {
        Test.startTest();
        Account a = new Account(Name='pruebaTest',OwnerId=userInfo.getUserId(),ShippingLatitude =3.5,ShippingLongitude =3.5, BI_Segment__c='Mayoristas', BI_Subsegment_Local__c='Mayoristas',BI_Territory__c = 'test');
        
        insert a;
        a.ShippingStreet= 'Avenida Manoteras 52';
        a.ShippingCity= 'Avenida Manoteras 52';
        a.ShippingCountry= 'Avenida Manoteras 52';
        a.ShippingPostalCode= 'Avenida Manoteras 52';
        update a;
        
        a.ShippingGeocodeAccuracy='city';
        update a;
        a.BI_G4C_Estado_Coordenadas__c= 'Pendiente Regla';
        a.ShippingGeocodeAccuracy='Address';
        update a;
        
        Test.stopTest();
        //}  
        
    }
}