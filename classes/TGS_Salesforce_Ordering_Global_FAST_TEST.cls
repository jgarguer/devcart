/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Manuel Ochoa Flórez
 Company:       New Energy Aborda
 Description:   Test class to manage the coverage code for TGS_Salesforce_Ordering_Global_FAST class
                Taken from Jose Miguel Fierro TGS_Salesforce_Ordering_Global_TEST class

 <Date>                  <Author>                <Change Description>
 12/09/2017              Manuel Ochoa Flórez      Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class TGS_Salesforce_Ordering_Global_FAST_TEST {
    static String attributesRegistration; 
    static String baseListAttributes;
    static String listAttributes_sla;
    static String strSiteForError;
    static TGS_Salesforce_Ordering_Global_FAST.AttachmentList attachmentList = new TGS_Salesforce_Ordering_Global_FAST.AttachmentList();
    static User usu;

    private static void clear_global_values() {
        if(!TGS_Salesforce_Ordering_Global_Helper.map_idParents.isEmpty()) {
            TGS_Salesforce_Ordering_Global_Helper.map_idParents.clear();
        }
        if(!TGS_Salesforce_Ordering_Global_Helper.map_access.isEmpty()) {
            TGS_Salesforce_Ordering_Global_Helper.map_access.clear();
        }
        if(!TGS_Salesforce_Ordering_Global_Helper.map_attachments.isEmpty()) {
            TGS_Salesforce_Ordering_Global_Helper.map_attachments.clear();
        }
    }

    private static String assetizar(String casnum) {
        Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
        BI_MigrationHelper.skipAllTriggers();

        Case c = [SELECT Id, Order__c, AccountId FROM Case WHERE CaseNumber = :casnum];

        NE__Order__c ord = new NE__Order__c(Id = c.Order__c);
        for(Account acc : [SELECT Id, RecordType.DeveloperName, (SELECT Id FROM Puntos_de_instalacion__r) FROM Account WHERE TGS_Aux_Holding__c = :c.AccountId]) {
            if(acc.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_LEGAL_ENTITY && ord.NE__AccountId__c == null) {
                ord.NE__AccountId__c = acc.Id;
            }
            if(acc.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_BUSINESS_UNIT && ord.NE__ServAccId__c == null && acc.Puntos_de_instalacion__r.size() > 1) {
                ord.NE__ServAccId__c = acc.Id;
                ord.Business_Unit__c = acc.Id;
                ord.Site__c = acc.Puntos_de_instalacion__r[0].Id;
            }
            if(acc.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_COST_CENTER && ord.NE__BillAccId__c == null) {
                ord.NE__BillAccId__c = acc.Id;
                ord.Cost_Center__c = acc.Id;
            }
        }

        NETriggerHelper.setTriggerFired('TGS_Order_Trigger');
        NETriggerHelper.setTriggerFired('TGS_Order_Handler.NeCheckOrder');
        NETriggerHelper.setTriggerFired('BI_FVI_AutoSplit');
        System.assertNotEquals(null, ord.Site__c);
        
        Id assetId =  NE.JS_RemoteMethods.order2asset(c.Order__c);

        NE__Asset__c comAss = new NE__Asset__c();
        insert comAss;

        ord.NE__AssetEnterpriseId__c = assetId;
        ord.NE__Asset__c = comAss.Id;
        update new NE__Order__c[]{ ord, new NE__Order__c(Id = assetId, NE__Asset__c = comAss.Id) };

        c.Status = 'Closed';
        c.TGS_Status_Reason__c = null;
        c.Asset__c = assetId;
        update c;

        List<NE__OrderItem__c> lst_oi = [SELECT Id, TGS_CSUID2__c FROM NE__OrderItem__c WHERE NE__OrderId__c = :assetId AND TGS_CSUID2__c = NULL];
        Integer i = 0;
        for(NE__OrderItem__c oi : lst_oi) {
            oi.TGS_CSUID2__c = oi.Id + '-' + i++;
        }
        update lst_oi;

        BI_MigrationHelper.disableBypass(mapa);
        BI_MigrationHelper.cleanSkippedTriggers();
        return assetId;
    }
    
    static {
        /* Added to avoid problems if queries have no results */
        try {
            BI_TestUtils.throw_exception = false;
            usu = [SELECT Id FROM User WHERE Profile.Name = 'TGS System Administrator' AND IsActive = true LIMIT 1];
            // Load Constants
            if(Constants.CASE_STATUS_ASSIGNED == Constants.CASE_STATUS_ASSIGNED) {}
            // Load RecordTypes
            TGS_RecordTypes_Util.loadRecordTypes(new List<Schema.SObjectType> { Account.SObjectType, Case.SObjectType, NE__Product__c.SObjectType, NE__Order__c.SObjectType, NE__OrderItem__c.SObjectType, NE__DynamicPropertyDefinition__c.SObjectType });

            baseListAttributes = 'VPN(Name="VPN ACME ACME";|)'+
                'Additional_Equipment(Brand="ACME";|)'+
                'Access(AL_ID="1";CPE_ID="1";IPSec="0";Access_Technology="Eth";Access_Connector="Und";Types_of_Interface="Eth";Access_Speed="1";IP_Bandwidth_Kb="1";International_IP_Bandwidth_Kb="1";National_IP_Bandwidth_Kb="1";Jumboframe="0";|)'+
                'CPE(CPE_ID="1";Name="Und";Manufacturer="1";Brand="1";Model="Cisco";Serial_Number="Und";|)'+
                'IP_Address(CPE_ID="1";IP_Address="0.0.0.0";Purpose="HSRP";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="Loopback";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="Customer";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="WAN";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="LAN";|)'+
                'CoS(BWPercentage="100";CosType="Data-Bronze";AL_ID="1";CAC=ATTACHMENT1;VPN="VPN ACME ACME";|)'+
                'Charges(CPE_Main_price="0.00 EUR";CPE_Backup_price="A.BC EUR";Access_1_price="1.00 EUR";Access_2_price="1.00 EUR";Access_3_price="1.00 EUR";Access_4_price="0.00 EUR";IP_Bandwidth_1_price="0.00 EUR";IP_Bandwidth_2_price="0.00 EUR";IP_Bandwidth_3_price="0.00 EUR";IP_Bandwidth_4_price="0.00 EUR";Additional_equipment_price="0.00 EUR";Additional_features_price="0.00 EUR";Multicast_price="0.00 EUR";Service_maintenance_price="0.00 EUR";Service_management_price="0.00 EUR";Activation_price="0.00 EUR";BC_ID="MWANCC0001";|CPE_Main_price="0.00 EUR";CPE_Backup_price="D.EF EUR";Access_1_price="100.00 EUR";Access_2_price="0.00 EUR";Access_3_price="0.00 EUR";Access_4_price="0.00 EUR";IP_Bandwidth_1_price="0.00 EUR";IP_Bandwidth_2_price="0.00 EUR";IP_Bandwidth_3_price="0.00 EUR";IP_Bandwidth_4_price="0.00 EUR";Additional_equipment_price="0.00 EUR";Additional_features_price="0.00 EUR";Multicast_price="0.00 EUR";Service_maintenance_price="0.00 EUR";Service_management_price="0.00 EUR";Monthly_fee="0.00 EUR";BC_ID="MWANCC0010";|)';
            listAttributes_sla = 'SLA(Compromished_Target="1";SLA_Name="D-Day";|Compromished_Target="2";SLA_Name="D-Day";|Compromished_Target="3";SLA_Name="D-Day";|)'+
                'SLA_per_CoS(Compromished_Target="1";SLA_Name="D-Day";CosType="COCO";|)';

            baseListAttributes += listAttributes_sla;

            attributesRegistration = 'Customer_Name="LE Acme SPN 1";Common_Name="test";';
            attributesRegistration += 'Delivery_Contact_Email="AZERT@QWERTY.COM";Delivery_Contact_FirstName="AZ";Delivery_Contact_Phone="123";';

        
            Account aCC=[SELECT ParentId,BI_Identificador_Externo__c,Parent.BI_Identificador_Externo__c FROM Account WHERE BI_Identificador_Externo__c!=null AND Parent.BI_Identificador_Externo__c!=null AND RecordType.DeveloperName=:Constants.RECORD_TYPE_TGS_COST_CENTER AND TGS_Es_MNC__c = true LIMIT 1];
            String strCCExtId = aCC.BI_Identificador_Externo__c;
            String strBUExtId = aCC.Parent.BI_Identificador_Externo__c;
    
            BI_Sede__c address = [Select Id, BI_Country__c, BI_ID_de_la_sede__c From BI_Sede__c Where BI_ID_de_la_sede__c!=null Limit 1];
            String strCountry=address.BI_Country__c;
            String strExtId  = address.BI_ID_de_la_sede__c;
            
            BI_Punto_de_instalacion__c objSite = new BI_Punto_de_instalacion__c();
            objSite.BI_Cliente__c = aCC.ParentId;
            objSite.BI_Sede__c = address.Id;
            objSite.Name = 'Test Fast';
            insert objSite;
        

            attributesRegistration += 'Action="CREATE_SERVICE";Country="' + strCountry + '";Sigma_WH_Site_ID="-TEST-";Resiliency="Basic";Incident_Number="0000001";Comments="AComment";';
            attributesRegistration +='Site="' + strExtId + '";RFB_Date="09/09/17";Business_Unit="' + strBUExtId + '";Cost_Center="' + strCCExtId + '";NRC_TOTAL="201.00 Billing Unit";MRC_TOTAL="2010.00 Billing Unit";';
            strSiteForError='Site="' + strExtId + '"';

            TGS_Salesforce_Ordering_Global_FAST.AttachmentReq att = new TGS_Salesforce_Ordering_Global_FAST.AttachmentReq();
            att.Attachment_ID = 'ATTACHMENT1';
            att.Attachment_Name = 'File1.txt';
            att.Attachment_Data = Blob.valueof('QXJjaGl2byBkZSBwcnVlYmEuDQpUZXN0IGZpbGUu');
            attachmentList.attachment = new List<TGS_Salesforce_Ordering_Global_FAST.AttachmentReq>();
            attachmentList.attachment.add(att);
        } catch (Exception e) {
            System.debug('Exception at setup. ' + e.getStackTraceString());
            System.debug(e.getMessage());
        }
    }

    @isTest(SeeAllData=true) static void test_Creation_createOrder_new_mWan() {
        System.runAs(usu){
            System.assertNotEquals(null, attributesRegistration);
            Test.startTest();
            TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST out = 
            TGS_Salesforce_Ordering_Global_FAST.Creation('SigmaWH', 'createOrder', '1', '970200', attributesRegistration, baseListAttributes, attachmentList);
            System.assertEquals(TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Processed, out.Operation_Status);
            Test.stopTest();
        }
    }

    @isTest(SeeAllData=true) static void test_Modify_createOrder_new_mWan() {
        System.runAs(usu){
            System.assertNotEquals(null, attributesRegistration);
            attributesRegistration = attributesRegistration.replace('Action="CREATE_SERVICE"', 'Action="MODIFY_SERVICE"');
            Test.startTest();
            TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST out = 
            TGS_Salesforce_Ordering_Global_FAST.Creation('SigmaWH', 'createOrder', '1', '970200', attributesRegistration, baseListAttributes, attachmentList);
            System.assertEquals(TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Cancelled, out.Operation_Status);
            Test.stopTest();
            attributesRegistration = attributesRegistration.replace('Action="MODIFY_SERVICE"','Action="CREATE_SERVICE"' );
        }
    }

    @isTest(SeeAllData=true) static void test_Termination_createOrder_new_mWan() {
        System.runAs(usu){
            System.assertNotEquals(null, attributesRegistration);
            attributesRegistration = attributesRegistration.replace('Action="CREATE_SERVICE"', 'Action="TERMINATION_SERVICE"');
            Test.startTest();
            TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST out = 
            TGS_Salesforce_Ordering_Global_FAST.Creation('SigmaWH', 'createOrder', '1', '970200', attributesRegistration, baseListAttributes, attachmentList);
            System.assertEquals(TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Cancelled, out.Operation_Status);
            Test.stopTest();
            attributesRegistration = attributesRegistration.replace('Action="TERMINATION_SERVICE"', 'Action="CREATE_SERVICE"');
        }
    }

    @isTest(SeeAllData=true) static void test_Creation_createOrder_forced_error() {
        System.runAs(usu){
            System.assertNotEquals(null, attributesRegistration);
            attributesRegistration = attributesRegistration.replace(strSiteForError, 'Site=""');
            Test.startTest();
            TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST out = 
            TGS_Salesforce_Ordering_Global_FAST.Creation('SigmaWH', 'createOrder', '1', '970200', attributesRegistration, baseListAttributes, attachmentList);
            System.assertEquals(TGS_Salesforce_Ordering_Global_FAST.Operation_StatusType.Cancelled, out.Operation_Status);
            Test.stopTest();
            attributesRegistration = attributesRegistration.replace('Site=""', 'Site="ARG-000078423"');
        }
    }

    @isTest(SeeAllData=true) static void test_inflateErrors() {
        System.runAs(usu){
            TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST om = new TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST();
            TGS_Salesforce_Ordering_Global_FAST.inflateErrorResponse(om, '11420', 'An Exception');
            TGS_Salesforce_Ordering_Global_FAST.inflateErrorResponse(om, '11015', 'An Exception');            
        }
    }

    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   test method for TGS_Salesforce_Ordering_Global_FAST.translateOutputMapping method
    Test Class:
    IN:                   
    OUT:            
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    void test_translateOutputMapping(){
        TGS_Salesforce_Ordering_Global.OutputMapping6 ret = new TGS_Salesforce_Ordering_Global.OutputMapping6();
        ret.Error_Code='0';
        ret.Error_Msg= 'Test received - action: TEST';
        ret.Operation_Instance_ID='Operation_Instance_ID'; // CaseNumber
        ret.Operation_Status = TGS_Salesforce_Ordering_Global.Operation_StatusType.Processed;
        ret.SRM_Request_ID='SRM_Request_ID';        // ParentOI.Name
        ret.Incident_ID='Incident_ID';
        TGS_Salesforce_Ordering_Global_FAST.OutputMapping6_FAST ret_FAST = TGS_Salesforce_Ordering_Global_FAST.translateOutputMapping(ret);
        ret.Operation_Status = TGS_Salesforce_Ordering_Global.Operation_StatusType.Cancelled;
        ret_FAST = TGS_Salesforce_Ordering_Global_FAST.translateOutputMapping(ret);
        ret.Operation_Status = TGS_Salesforce_Ordering_Global.Operation_StatusType.Idle;
        ret_FAST = TGS_Salesforce_Ordering_Global_FAST.translateOutputMapping(ret);
    }
}