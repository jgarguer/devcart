@isTest
private class BI_BolsaEspecialMethods_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_BolsaEspecialMethods class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    27/03/2015      		Fernando González    	Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando González
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_BolsaEspecialMethods
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015      		Fernando González    	Initial Version	
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 	
    static testMethod void bolsaEspecialMethodsTest() {
    	
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		list<BI_Bolsas_especiales__c> lstBolsas = new list<BI_Bolsas_especiales__c>();
		list<BI_Bolsas_especiales__c> lstBolsas_del = new list<BI_Bolsas_especiales__c>();
		list<BI_Solicitud_de_plan__c> lstSolPlan = new list<BI_Solicitud_de_plan__c>();
		list<BI_Solicitud_de_plan__c> lstSolPlan_del = new list<BI_Solicitud_de_plan__c>();
		
		lstSolPlan = BI_DataLoadAgendamientos.loadSolicitudPlan(200);
		
		test.startTest();
		
		
		
		//lstBolsas = BI_DataLoadAgendamientos.loadBolsasEspeciales(200 ,lstSolPlan);
		
		for (BI_Solicitud_de_plan__c sol : lstSolPlan ) {
			BI_Bolsas_especiales__c bolsas = new BI_Bolsas_especiales__c(BI_Bolsa_especial__c = 'test bolsa',
												               	BI_Cantidad__c = 10,
												               	BI_Plan_Requerimiento_comercial__c = sol.Id );
		    
		    lstBolsas.Add(bolsas);										               	
		}
		
		try{
    		insert lstBolsas;
    		
    	}catch(exception e){
    		system.assert(e.getMessage().contains('No se puede modificar la bolsa especial en el estado actual del plan'));
    	}

		
		lstSolPlan_del = BI_DataLoadAgendamientos.loadSolicitudPlan_SinEstado(200);
		
		
		for (BI_Solicitud_de_plan__c sol : lstSolPlan_del ) {
			BI_Bolsas_especiales__c bolsas = new BI_Bolsas_especiales__c(BI_Bolsa_especial__c = 'test bolsa',
												               	BI_Cantidad__c = 10,
												               	BI_Plan_Requerimiento_comercial__c = sol.Id );
		    
		    lstBolsas_del.Add(bolsas);										               	
		}
		
		insert lstBolsas_del;										                

    	delete lstBolsas_del;

		test.stopTest();
		
		try
		{
			BI_BolsaEspecialMethods.blockEdition(null);
		}
		catch (Exception e)
		{
			System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
		}
		
		try
		{
			BI_BolsaEspecialMethods.blockDelete(null);
		}
		catch (Exception e)
		{
			System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
		}
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}