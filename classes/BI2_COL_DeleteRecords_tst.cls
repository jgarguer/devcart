/**
**************************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          Telefonica BI_EN Fase 2 Colombia
* @name             BI2_COL_DeleteRecords_tst
* @description      Test class for "INT_DeleteRecords_bch" batchable class.
* @dependencies     None
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------------
* @version   1.0   2017-02-28   Antonio Torres (AT)           Initial version.
**************************************************************************************************************
**/

@isTest
private class BI2_COL_DeleteRecords_tst {
    /**
    * @method       generateTestData
    * @description  Method that generates the test data used in test methods.
    * @author       Antonio Torres - 2017-02-28
    */
    @testSetup
    public static void generateTestData()  {
        BI_Col_Ciudades__c objCity = new BI_Col_Ciudades__c();
        objCity.Name                    = 'Bogota';
        objCity.BI_COL_Pais__c          = 'Colombia';
        objCity.BI_COL_Codigo_DANE__c   = '11001000';
        insert objCity;
        
        BI_Col_Ciudades__c objCity2 = new BI_Col_Ciudades__c();
        objCity2.Name                   = 'Cartagena';
        objCity2.BI_COL_Pais__c         = 'Colombia';
        objCity2.BI_COL_Codigo_DANE__c  = '130001';
        insert objCity2;
    }

    /**
    * @method       deleteRecordsWithoutNotificationTest
    * @description  Method that test deletion of records when email notification is not needed.
    * @author       Antonio Torres - 2017-02-28
    */
    @isTest
    static void deleteRecordsWithoutNotificationTest() {
        Test.startTest();
            Database.executeBatch(new BI2_COL_DeleteRecords_bch('SELECT Id FROM BI_Col_Ciudades__c WHERE Name = \'Bogota\''));
        Test.stopTest();
    }

    /**
    * @method       deleteRecordsWithNotificationTest
    * @description  Method that test deletion of records when an email notification is required when finished.
    * @author       Antonio Torres - 2017-02-28
    */
    @isTest
    static void deleteRecordsWithNotificationTest() {
        Test.startTest();
            Database.executeBatch(new BI2_COL_DeleteRecords_bch('SELECT Id FROM BI_Col_Ciudades__c WHERE Name = \'Cartagena\'', true));
        Test.stopTest();
    }
}