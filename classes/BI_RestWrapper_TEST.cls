@isTest
private class BI_RestWrapper_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_RestWrapper class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    12/10/2014              Pablo Oliva             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for the wrapper classes
    History:
    
    <Date>              <Author>                <Description>
    12/10/2014          Pablo Oliva             Initial version
    10/11/2015			Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void wrapperTest() {
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_RestWrapper.AccountIdType AccountIdType = new BI_RestWrapper.AccountIdType();
        BI_RestWrapper.AccountInfoType AccountInfoType = new BI_RestWrapper.AccountInfoType();
        BI_RestWrapper.AccountRequestType AccountRequestType = new BI_RestWrapper.AccountRequestType();
        BI_RestWrapper.AccountsListType AccountsListType = new BI_RestWrapper.AccountsListType();
        //BI_RestWrapper.AccountType AccountType = new BI_RestWrapper.AccountType(); // FAR 10/11/2015
        BI_RestWrapper.AddressesListType AddressesListType = new BI_RestWrapper.AddressesListType();
        BI_RestWrapper.AddressInfoType AddressInfoType = new BI_RestWrapper.AddressInfoType();
        BI_RestWrapper.AddressType AddressType = new BI_RestWrapper.AddressType();
        BI_RestWrapper.AddrRangeType AddrRangeType = new BI_RestWrapper.AddrRangeType();
        BI_RestWrapper.AddrValueType AddrValueType = new BI_RestWrapper.AddrValueType(); 
        BI_RestWrapper.AllowanceProdPriceAlterationType AllowanceProdPriceAlterationType = new BI_RestWrapper.AllowanceProdPriceAlterationType();
        BI_RestWrapper.ApiTransactionNotificationType ApiTransactionNotificationType = new BI_RestWrapper.ApiTransactionNotificationType();
        BI_RestWrapper.apiTransactionStatusType apiTransactionStatusType = new BI_RestWrapper.apiTransactionStatusType();
        BI_RestWrapper.apiTransactionType apiTransactionType = new BI_RestWrapper.apiTransactionType();
        BI_RestWrapper.BienOppDetailsType BienOppDetailsType = new BI_RestWrapper.BienOppDetailsType();
        BI_RestWrapper.BienProductType BienProductType = new BI_RestWrapper.BienProductType();
        BI_RestWrapper.ChannelType ChannelType = new BI_RestWrapper.ChannelType();
        BI_RestWrapper.CompositeProdPriceType CompositeProdPriceType = new BI_RestWrapper.CompositeProdPriceType();
        BI_RestWrapper.ContactDetailsType ContactDetailsType = new BI_RestWrapper.ContactDetailsType();
        BI_RestWrapper.ContactInfoType ContactInfoType = new BI_RestWrapper.ContactInfoType();
        BI_RestWrapper.ContactingModeType ContactingModeType = new BI_RestWrapper.ContactingModeType(); 
        BI_RestWrapper.ContactsListInfoType ContactsListInfoType = new BI_RestWrapper.ContactsListInfoType(); 
        BI_RestWrapper.ContactsListType ContactsListType = new BI_RestWrapper.ContactsListType();
        BI_RestWrapper.ContactType ContactType = new BI_RestWrapper.ContactType();
        BI_RestWrapper.ContractBasicInfoType ContractBasicInfoType = new BI_RestWrapper.ContractBasicInfoType();
        BI_RestWrapper.ContractInfoType ContractInfoType = new BI_RestWrapper.ContractInfoType();
        BI_RestWrapper.ContractsListInfoType ContractsListInfoType = new BI_RestWrapper.ContractsListInfoType();
        BI_RestWrapper.ContractsListType ContractsListType = new BI_RestWrapper.ContractsListType();
        BI_RestWrapper.CoordinatesType CoordinatesType = new BI_RestWrapper.CoordinatesType();
        BI_RestWrapper.CustomerIdType CustomerIdType = new BI_RestWrapper.CustomerIdType();
        BI_RestWrapper.CustomerInfoType CustomerInfoType = new BI_RestWrapper.CustomerInfoType();
        BI_RestWrapper.CustomerRequestType CustomerRequestType = new BI_RestWrapper.CustomerRequestType();
        BI_RestWrapper.CustomerType CustomerType = new BI_RestWrapper.CustomerType();
        BI_RestWrapper.DescriptionType DescriptionType = new BI_RestWrapper.DescriptionType();
        BI_RestWrapper.DiscountProdPriceAlterationType DiscountProdPriceAlterationType = new BI_RestWrapper.DiscountProdPriceAlterationType();
        BI_RestWrapper.ExceptionType ExceptionType = new BI_RestWrapper.ExceptionType();
        BI_RestWrapper.GenericResponse GenericResponse = new BI_RestWrapper.GenericResponse();
        BI_RestWrapper.IdentificationType IdentificationType = new BI_RestWrapper.IdentificationType();
        BI_RestWrapper.InvoiceBasicInfoType InvoiceBasicInfoType = new BI_RestWrapper.InvoiceBasicInfoType();
        BI_RestWrapper.InvoiceInfoType InvoiceInfoType = new BI_RestWrapper.InvoiceInfoType();
        BI_RestWrapper.InvoiceListType InvoiceListType = new BI_RestWrapper.InvoiceListType();
        BI_RestWrapper.InvoicesListInfoType InvoicesListInfoType = new BI_RestWrapper.InvoicesListInfoType();
        BI_RestWrapper.InvoicesListType InvoicesListType = new BI_RestWrapper.InvoicesListType();
        BI_RestWrapper.KeyvalueType KeyvalueType = new BI_RestWrapper.KeyvalueType();
        BI_RestWrapper.MessageHeaderType MessageHeaderType = new BI_RestWrapper.MessageHeaderType();
        BI_RestWrapper.MessageType MessageType = new BI_RestWrapper.MessageType();
        BI_RestWrapper.MoneyType MoneyType = new BI_RestWrapper.MoneyType();
        BI_RestWrapper.MultiSubscriptionCreateType MultiSubscriptionCreateType = new BI_RestWrapper.MultiSubscriptionCreateType();
        BI_RestWrapper.MultiSubscriptionType MultiSubscriptionType = new BI_RestWrapper.MultiSubscriptionType();
        BI_RestWrapper.OpportunitiesListType OpportunitiesListType = new BI_RestWrapper.OpportunitiesListType();
        BI_RestWrapper.OpportunityBasicInfoType OpportunityBasicInfoType = new BI_RestWrapper.OpportunityBasicInfoType();
        BI_RestWrapper.OpportunityDetailedInfoType OpportunityDetailedInfoType = new BI_RestWrapper.OpportunityDetailedInfoType();
        BI_RestWrapper.OpportunityType OpportunityType = new BI_RestWrapper.OpportunityType();
        BI_RestWrapper.OrderBasicInfoType OrderBasicInfoType = new BI_RestWrapper.OrderBasicInfoType();
        BI_RestWrapper.OrderInfoType OrderInfoType = new BI_RestWrapper.OrderInfoType();
        BI_RestWrapper.OrdersListInfoType OrdersListInfoType = new BI_RestWrapper.OrdersListInfoType();
        BI_RestWrapper.OrdersListType OrdersListType = new BI_RestWrapper.OrdersListType();
        BI_RestWrapper.PaymentMethodType PaymentMethodType = new BI_RestWrapper.PaymentMethodType();
        BI_RestWrapper.ProdPriceChargeType ProdPriceChargeType = new BI_RestWrapper.ProdPriceChargeType();
        //BI_RestWrapper.ProductBundleSubscriptionType ProductBundleSubscriptionType = new BI_RestWrapper.ProductBundleSubscriptionType(); // FAR 10/11/2015
        BI_RestWrapper.ProductBundleType ProductBundleType = new BI_RestWrapper.ProductBundleType(); // FAR 10/11/2015
        BI_RestWrapper.ProductCharacteristicValueType ProductCharacteristicValueType = new BI_RestWrapper.ProductCharacteristicValueType();
        //BI_RestWrapper.ProductSubscriptionType ProductSubscriptionType = new BI_RestWrapper.ProductSubscriptionType(); // FAR 10/11/2015
        BI_RestWrapper.ProductType ProductType = new BI_RestWrapper.ProductType();
        BI_RestWrapper.QuantityType QuantityType = new BI_RestWrapper.QuantityType();
        BI_RestWrapper.RelatedObjectType RelatedObjectType = new BI_RestWrapper.RelatedObjectType();
        BI_RestWrapper.RelatedPartyType RelatedPartyType = new BI_RestWrapper.RelatedPartyType();
        BI_RestWrapper.RoleType RoleType = new BI_RestWrapper.RoleType();
        BI_RestWrapper.smsType smsType = new BI_RestWrapper.smsType();
        BI_RestWrapper.SubscriptionPriceType SubscriptionPriceType = new BI_RestWrapper.SubscriptionPriceType();
        BI_RestWrapper.TicketAttachmentInfoType TicketAttachmentInfoType = new BI_RestWrapper.TicketAttachmentInfoType();
        BI_RestWrapper.TicketAttachmentRequestType TicketAttachmentRequestType = new BI_RestWrapper.TicketAttachmentRequestType();
        BI_RestWrapper.TicketAttachmentsListType TicketAttachmentsListType = new BI_RestWrapper.TicketAttachmentsListType();
        BI_RestWrapper.TicketBasicInfoType TicketBasicInfoType = new BI_RestWrapper.TicketBasicInfoType();
        BI_RestWrapper.TicketInfoType TicketInfoType = new BI_RestWrapper.TicketInfoType();
        BI_RestWrapper.TicketNoteInfoType TicketNoteInfoType = new BI_RestWrapper.TicketNoteInfoType();
        BI_RestWrapper.TicketNoteRequestType TicketNoteRequestType = new BI_RestWrapper.TicketNoteRequestType();
        BI_RestWrapper.TicketNotesListType TicketNotesListType = new BI_RestWrapper.TicketNotesListType();
        BI_RestWrapper.TicketRequestType TicketRequestType = new BI_RestWrapper.TicketRequestType();
        BI_RestWrapper.TicketsListInfoType TicketsListInfoType = new BI_RestWrapper.TicketsListInfoType();
        BI_RestWrapper.TicketsListType TicketsListType = new BI_RestWrapper.TicketsListType();
        BI_RestWrapper.TicketStatusInfoType TicketStatusInfoType = new BI_RestWrapper.TicketStatusInfoType();
        BI_RestWrapper.TimePeriodType TimePeriodType = new BI_RestWrapper.TimePeriodType();
        BI_RestWrapper.TransactionType TransactionType = new BI_RestWrapper.TransactionType();
        BI_RestWrapper.ValidateAddressResponseType ValidateAddressResponseType = new BI_RestWrapper.ValidateAddressResponseType();  
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}