/**
* Avanxo Colombia
* @author 			Manuel Medina href=<mmedina@avanxo.com>
* Project:			Telefonica
* Description:		Clase de prueba encargada de realizar la cobertura de la clase BI_COL_CreateContactWS_cls.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2015-07-30		Manuel Medina (MM)      Definicion de la clase de prueba.
*           1.1		2017-01-02		Gawron, Julián			Add testSetup
					13/03/2017		Marta Gonzalez(Everis)  REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
		
*************************************************************************************************************/
@isTest
private class BI_COL_CreateContactWS_tst {
	
	public static list<BI_Log__c> lstLog = new list <BI_Log__c>();
	Public static list<Profile> 											lstPerfil;
	Public static list <UserRole> 											lstRoles;
	Public static User 													objUsuario;

	@testSetup static void createData(){
		
		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c=true;
		insert objBibypass;

		//lstPerfil
        lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
			insert objUsuario;
		}

		System.runAs(objUsuario){
		Account objAccount											= new Account();
		objAccount.Name												= 'TSTAccount';
		objAccount.BI_Country__c									= 'Colombia';
		insert objAccount;
		
		BI_Col_Ciudades__c objCity									= new BI_Col_Ciudades__c();
		objCity.BI_COL_Codigo_ciudad__c								= 'BOG';
		objCity.BI_COl_ID_Colombia__c								= 'COL';
		objCity.BI_COL_Pais__c										= 'Colombia';
		insert objCity;

		
		BI_Log__c objLog											= new BI_Log__c();
		objLog.BI_COL_Informacion_recibida__c						= 'OK, Respuesta Procesada';
		//objLog.BI_COL_Contacto__c 									= lstContacts[0].Id;
		objLog.BI_COL_Estado__c										= 'FALLIDO';
		objLog.BI_COL_Interfaz__c									= 'NOTIFICACIONES';
		lstLog.add(objLog);	
		insert lstLog;	

		Map<String, List<Object>> mapContactsInfo					= new Map<String, List<Object>>{
																		'TSTContact1'	=> new List<Object>{
																								'3681269852', true, false, false, '1085763264', false,
																								false, true, '', '', false, 'all',
																								objAccount.Id, true, false, false, false, true,
																								true, true, true, true, true, true,
																								true, true, true, true, true, false,
																								false, false, false, '6854491', 'testContact1@test.com', 'General',
																								objCity.Id, 'K 54','DNI',true//REING//
																						},
																		'TSTContact2'	=> new List<Object>{
																								'3681269853', true, false, false, '1085763264', false,
																								false, false, '', '', false, 'all',
																								objAccount.Id, true, false, false, false, false,
																								false, false, false, false, false, false,
																								false, false, false, false, false, false,
																								false, false, false, '6854492', 'testContact2@test.com', 'General',
																								objCity.Id, 'K 54','DNI', false
																						},
																		'TSTContact3'	=> new List<Object>{
																								'3681269854', true, false, false, '1085763264', false,
																								false, false, '', '', false, 'all',
																								objAccount.Id, true, false, false, false, false,
																								false, false, false, false, false, false,
																								false, false, false, false, false, false,
																								false, false, false, '6854493', 'testContact3@test.com', 'General',
																								objCity.Id, 'K 54','DNI', false
																						},
																		'TSTContact4'	=> new List<Object>{
																								'3681269855', true, false, false, '1085763264', false,
																								false, false, '', '', false, 'all',
																								objAccount.Id, true, false, false, false, true,
																								true, true, true, true, true, false,
																								false, false, false, false, false, false,
																								false, false, false, '6854494', 'testContact4@test.com', 'General',
																								objCity.Id, 'K 54','DNI', false
																						},
																		'TSTContact5'	=> new List<Object>{
																								'3681269856', true, false, false, '1085763264', false,
																								false, false, '', '', false, 'all',
																								objAccount.Id, true, false, false, false, true,
																								true, true, true, true, true, true,
																								true, true, true, true, true, false,
																								false, false, false, '6854495', 'testContact5@test.com', 'General',
																								objCity.Id, 'K 54','DNI', false
																						}
																	};
		
		List<Contact> lstContacts									= new List<Contact>();
		for( String strKeyContact : mapContactsInfo.keySet() ){
			Contact objContact										= new Contact();
			objContact.FirstName									= strKeyContact;
			objContact.LastName										= strKeyContact;
			objContact.MobilePhone									= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 0 ) );
			objContact.BI_Activo__c									= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 1 ) );
			objContact.BI_Contacto_mercadeo__c						= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 2 ) );
			objContact.BI_No_recibir_llamadas__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 3 ) );
			objContact.BI_Numero_de_documento__c					= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 4 ) );
			objContact.BI_Proveedor__c								= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 5 ) );
			objContact.BI_Regalos_empresariales__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 6 ) );
			objContact.BI_Representante_legal__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 7 ) );
			objContact.Estado__c									= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 8 ) );
			objContact.Idioma__c									= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 9 ) );
			objContact.Invitar_a_Eventos_de_Relacionamiento__c		= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 10 ) );
			objContact.BI_Filtro_de_busqueda__c						= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 11 ) );
			objContact.TEMPEXT_ID__c								= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 12 ) );
			objContact.Certa_SCP__active__c							= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 13 ) );
			objContact.TGS_Circulo_Allegados__c						= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 14 ) );
			objContact.TGS_No_Enviar_Newsletter__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 15 ) );
			objContact.TGS_Surveys__c								= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 16 ) );
			objContact.TGS_Enviar_Email_Billing_Inquiry__c			= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 17 ) );
			objContact.TGS_Enviar_Email_Change__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 18 ) );
			objContact.TGS_Enviar_Email_Complaint__c				= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 19 ) );
			objContact.TGS_Enviar_Email_Incident__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 20 ) );
			objContact.TGS_Enviar_Email_Order_Management_Case__c	= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 21 ) );
			objContact.TGS_Enviar_Email_Query__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 22 ) );
			objContact.BI_COL_Contacto_principal_holding__c			= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 29 ) );
			objContact.BI_COL_Potestad_para_firmar__c				= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 30 ) );
			objContact.BI_COL_Principal__c							= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 31 ) );
			objContact.BI_COL_Fotocopia_Cedula__c					= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 32 ) );
			objContact.Phone										= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 33 ) );
			objContact.Email										= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 34 ) );
			objContact.BI_Tipo_de_contacto__c						= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 35 ) );
			objContact.BI_COL_Ciudad_Depto_contacto__c				= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 36 ) );
			objContact.BI_COL_Direccion_oficina__c					= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 37 ) );
            //REING_INI
            objContact.BI_Tipo_de_documento__c 						= String.valueOf( mapContactsInfo.get( strKeyContact ).get( 38 ) );
            objContact.FS_CORE_Acceso_a_Portal_Platino__c			= Boolean.valueOf( mapContactsInfo.get( strKeyContact ).get( 39 ) );
            //REING_FIN
			objContact.AccountId = objAccount.Id;
			objContact.BI_Country__c = 'Colombia';
			
			lstContacts.add(objContact);
		}
		
            upsert lstContacts;
        }
    }

	//static testMethod void BI_COL_CreateContactWS_TestCase1() {
		
	//	User objUser												= new User();
	//	objUser														= [SELECT Id
	//																	FROM User
	//																	WHERE Pais__c		!= 'Colombia'
	//																	LIMIT 1
	//																].get( 0 );																			
																
	//	Test.startTest();
	//		Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
	//		Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			
	//		System.runAs( objUser ){
	//			List<Contact> lstContacts							= new List<Contact>( createData() );
	//			insert lstContacts;
	//			//BI_COL_FlagforTGRS_cls.flagTriggerTRSContacto		= true;
				
	//			//delete lstContacts;
				
	//		}
	//	Test.stopTest();
		 
	//}
	
	//static testMethod void BI_COL_CreateContactWS_TestCase2() {
		
	//	User objUser												= new User();
	//	objUser														= [SELECT Id
	//																	FROM User
	//																	WHERE Pais__c		!= 'Colombia'
	//																	LIMIT 1
	//																].get( 0 );
		
	//	List<Contact> lstContacts							= new List<Contact>( createData() );
	//			insert lstContacts;

	//	Test.startTest();
	//		Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
	//		Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			
	//		System.runAs( objUser ){
					
	//			BI_COL_FlagforTGRS_cls.flagTriggerTRSContacto		= true;
				
				
	//		}
	//	//	delete lstContacts[0];
	//	Test.stopTest();
		
	//}

	static testMethod void BI_COL_CreateContactWS_TestCase3() 
	{
		Test.startTest();
        
			Test.setMock(WebServiceMock.class, new BI_COL_CreateContactWS_mws());
        
			List<Contact> lstContacts = [
			  Select Id, FirstName,BI_Country__c, LastName, MobilePhone, BI_Activo__c, BI_Contacto_mercadeo__c, 
			  BI_No_recibir_llamadas__c, BI_Numero_de_documento__c, BI_Proveedor__c, 
			  BI_Regalos_empresariales__c, BI_Representante_legal__c, Estado__c, Idioma__c, 
			  Invitar_a_Eventos_de_Relacionamiento__c, BI_Filtro_de_busqueda__c, TEMPEXT_ID__c, 
			  Certa_SCP__active__c, TGS_Circulo_Allegados__c, TGS_No_Enviar_Newsletter__c, 
			  TGS_Surveys__c, TGS_Enviar_Email_Billing_Inquiry__c, TGS_Enviar_Email_Change__c, 
			  TGS_Enviar_Email_Complaint__c, TGS_Enviar_Email_Incident__c, 
			  TGS_Enviar_Email_Order_Management_Case__c, TGS_Enviar_Email_Query__c, 
			  BI_COL_Contacto_principal_holding__c, BI_COL_Potestad_para_firmar__c, BI_COL_Principal__c, 
			  BI_COL_Fotocopia_Cedula__c, Phone, Email, BI_Tipo_de_contacto__c, 
			  BI_COL_Ciudad_Depto_contacto__c, BI_COL_Direccion_oficina__c,FS_CORE_Referencias_Funcionales__c
			  from Contact
			];
         //REING_INI
        // Contact #0
        
        // Contact #1
        lstContacts[1].BI_Tipo_de_contacto__c = 'General; Cobranza';
        
        // Contacto #2
        lstContacts[2].BI_Tipo_de_contacto__c = null;
        lstContacts[2].FS_CORE_Referencias_Funcionales__c = 'Administrativo';
        
        // Contacto #2
        lstContacts[3].BI_Tipo_de_contacto__c = null;
        lstContacts[3].FS_CORE_Referencias_Funcionales__c = 'Administrativo; Facturación';
        
        //REING_FIN
        
        System.debug('\n\n\n ======================================>>>>>>>>>>>>>>'+lstContacts[0].BI_Tipo_de_contacto__c);
        System.debug('\n\n\n ======================================>>>>>>>>>>>>>>'+lstContacts[0].BI_Tipo_de_contacto__c.indexOf('Administrador Canal Online'));
        
        upsert lstContacts;
        
        BI_COL_CreateContactWS_cls.creaLogTransaccionError('ErrorTest',lstContacts,'Test');
			
			ws_wwwTelefonicaComNotificacionessalesfo.respuesta objTelComNot = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
			objTelComNot.idError = '0';
			
			ws_wwwTelefonicaComNotificacionessalesfo.contacto objTelComNotCont = new ws_wwwTelefonicaComNotificacionessalesfo.contacto();

			ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] arrTelComNot = new List<ws_wwwTelefonicaComNotificacionessalesfo.respuesta>();
			arrTelComNot.add(objTelComNot);

			map<ID,ws_wwwTelefonicaComNotificacionessalesfo.contacto> infContactoEnviad = new map<ID,ws_wwwTelefonicaComNotificacionessalesfo.contacto>();
			for(Contact objCont : lstContacts)
			{
				infContactoEnviad.put(objCont.Id,objTelComNotCont);
			}

			BI_COL_CreateContactWS_cls.creaLogTransaccion(arrTelComNot,lstContacts,infContactoEnviad);

			arrTelComNot[0].idError = '5';
			BI_COL_CreateContactWS_cls.creaLogTransaccion(arrTelComNot,lstContacts,infContactoEnviad);

			

			List<Contact> lstIdAcc2 = new List<Contact>();
			for(Contact objAccT : lstContacts)
				lstIdAcc2.add(objAccT);

			
			BI_COL_CreateContactWS_cls.crearContactoTelefonica(lstIdAcc2,null,'Insertar');

		Test.stopTest();
	}
}