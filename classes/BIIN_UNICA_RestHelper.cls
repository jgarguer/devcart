public with sharing class BIIN_UNICA_RestHelper 
{
/*---------------------------------------------------------------------------------------
    Author:        José Luis González
    Company:       HPE
    Description:   Helper class for UNICA integration, business logic for RoD integration

    <Date>                 <Author>                <Change Description>
    27/04/2016             José Luis González      Initial Version
    06/07/2016             José Luis González      Fix BI_Confidencial__c in creation
    13/07/2016             José Luis González      Fix white spaces in Dates
    19/07/2016             José Luis González      Default value for SLM Status & Allow empty Country
---------------------------------------------------------------------------------------*/
    static final String ORIGEN_REMEDY   = '13000';
    static final String MOTIVO_CASO     = 'Incidencia Técnica';
    static final String TIPO_CASO       = 'Primer Contacto';
    static final DateTime startDateTime = DateTime.newInstance(0);

    public static Case generateTicket(Case ticket, BIIN_UNICA_Pojos.TicketRequestType ticketRequest)
    {
        // Read additional fields
        Map<String, String> additionalData = BIIN_UNICA_Utils.additionalDataToMap(ticketRequest.additionalData);

        Account acc;
        System.debug(' BIIN_UNICA_RestHelper.generateTicket | ticketRequest.customerId (BI_Validador_Fiscal__c): ' + ticketRequest.customerId);
        try 
        {
            acc = [SELECT Id, BI_Country__c FROM Account WHERE BI_Validador_Fiscal__c =: ticketRequest.customerId];
        } 
        catch(QueryException e) 
        {
            System.debug(' BIIN_UNICA_RestHelper.generateTicket | Error, invalid BI_Validador_Fiscal__c: ' + e);
            throw new BIIN_UNICA_BadRequestException('CustomerId not found: ' + ticketRequest.customerId + ', exception: ' + e);
        }

        // filter to check ticket creation
        if(additionalData.get('Case Number') == '' || additionalData.get('Case Number') == null) 
        {
            ticket.AccountId                  = acc.Id;
            ticket.Type                       = TIPO_CASO;
            ticket.Reason                     = MOTIVO_CASO;
            ticket.BI_COL_Fecha_Radicacion__c = startDateTime.addSeconds(Integer.valueOf(ticketRequest.reportedDate.trim()));
            ticket.CreatedDate                = startDateTime.addSeconds(Integer.valueOf(additionalData.get('Created Date').trim()));
            ticket.LastModifiedDate           = startDateTime.addSeconds(Integer.valueOf(additionalData.get('Last Modified Date').trim()));
            ticket.Origin                     = BIIN_Helper.traducirValor('Origen', ORIGEN_REMEDY); // ticketRequest.source
            ticket.BI_Confidencial__c         = true;

            if(ticketRequest.parentTicket != null && ticketRequest.parentTicket != '') 
            {
                ticket.ParentId = [SELECT Id FROM Case WHERE Id =: ticketRequest.parentTicket].Id;
            }
        }

        // filter to avoid null values for BI_Country__c
        String strCountry = ticketRequest.country;
        if(String.isEmpty(strCountry)) {
            strCountry = acc.BI_Country__c;
        }
        ticket.BI_Country__c = strCountry;
        
        Id recordTypeIncTec = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Incidencia Tecnica').getRecordTypeId();
        System.debug(' BIIN_UNICA_RestHelper.generateTicket | Record Type recuperado: ' + recordTypeIncTec);

        ticket.BI_Id_del_caso_legado__c            = ticketRequest.correlatorId;
        ticket.RecordTypeId                        = recordTypeIncTec;
        ticket.TGS_Impact__c                       = Integer.valueOf(ticketRequest.perceivedSeverity)/1000 + '-' + BIIN_UNICA_Utils.translateFromRemedyIDToRemedyValue('Impacto', Integer.valueOf(ticketRequest.perceivedSeverity));
        ticket.Subject                             = ticketRequest.subject;
        ticket.Description                         = ticketRequest.description;
        ticket.BI2_Nombre_Contacto_Final__c        = additionalData.get('First Name');
        ticket.BI2_Apellido_Contacto_Final__c      = additionalData.get('Last Name');
        ticket.BI2_Email_Contacto_Final__c         = additionalData.get('Internet Email');
        ticket.TGS_Urgency__c                      = Integer.valueOf(additionalData.get('Urgency'))/1000 + '-' + BIIN_UNICA_Utils.translateFromRemedyIDToRemedyValue('Urgencia', Integer.valueOf(additionalData.get('Urgency')));
        ticket.BIIN_Site__c                        = additionalData.get('Site');
        ticket.BI_Product_Categorization_Tier_1__c = additionalData.get('Product Categorization Tier 1');
        ticket.BI_Product_Categorization_Tier_2__c = additionalData.get('Product Categorization Tier 2');
        ticket.BI_Product_Categorization_Tier_3__c = additionalData.get('Product Categorization Tier 3');
        ticket.BI_Resolution_Category_Tier_1__c    = additionalData.get('Resolution Category');
        ticket.BI_Resolution_Category_Tier_2__c    = additionalData.get('Resolution Category Tier 2');
        ticket.BI_Resolution_Category_Tier_3__c    = additionalData.get('Resolution Category Tier 3');
        ticket.BIIN_Categorization_Tier_1__c       = additionalData.get('Categorization Tier 1');
        ticket.BIIN_Categorization_Tier_2__c       = additionalData.get('Categorization Tier 2');
        ticket.BIIN_Categorization_Tier_3__c       = additionalData.get('Categorization Tier 3');
        ticket.BIIN_Id_Producto__c                 = additionalData.get('Name');
        ticket.Status                              = BIIN_UNICA_Utils.translateFromRemedyIDToRemedyValue('Estado', Integer.valueOf(additionalData.get('Status')));
        ticket.BI_COL_Codigo_CUN__c                = additionalData.get('BAO_ExternalID');
        ticket.BI_Assigned_Group__c                = additionalData.get('Assigned Group');
        ticket.TGS_Resolution__c                   = additionalData.get('Resolution');
        ticket.Priority                            = BIIN_UNICA_Utils.translateFromRemedyIDToRemedyValue('Prioridad', Integer.valueOf(ticketRequest.perceivedPriority));

        // INICIO: Campos añadidos para fase 2 de Colombia
        if(strCountry == Label.BI_Colombia) {
            ticket.BI2_Nombre_Contacto_Final__c     = additionalData.get('Direct Contact First Name');
            ticket.BI2_Apellido_Contacto_Final__c   = additionalData.get('Direct Contact Last Name');
            ticket.BI2_Email_Contacto_Final__c      = additionalData.get('Direct Contact Internet Email');
            ticket.BI_ECU_Telefono_fijo__c          = additionalData.get('mobile');
            ticket.BI_ECU_Telefono_movil__c         = additionalData.get('phone');
            ticket.BI2_COL_Contacto_de_cierre__c    = additionalData.get('closingContact');
        } else {
            ticket.BI2_Nombre_Contacto_Final__c        = additionalData.get('First Name');
            ticket.BI2_Apellido_Contacto_Final__c      = additionalData.get('Last Name');
            ticket.BI2_Email_Contacto_Final__c         = additionalData.get('Internet Email');
        }

        if(additionalData.get('cunDate') != '' && additionalData.get('cunDate') != null) {
            ticket.BI2_COL_Fecha_generacion_CUN__c = startDateTime.addSeconds(Integer.valueOf(additionalData.get('cunDate').trim()));
        }
        // FIN: Campos añadidos para fase 2 de Colombia<

        if(additionalData.get('BAO_TiempoNetoApertura') != '' && additionalData.get('BAO_TiempoNetoApertura') != null)
        {
            ticket.BIIN_Tiempo_Neto_Apertura__c = decimal.valueOf(additionalData.get('BAO_TiempoNetoApertura'))/3600;
        }

        if(additionalData.get('SLM Status') != null && additionalData.get('SLM Status') != '') 
        {
            ticket.TGS_SLA_Light__c = BIIN_UNICA_Utils.translateFromRemedyByID('SLM-Status', Integer.valueOf(additionalData.get('SLM Status')));
        }
        else
        {
            ticket.TGS_SLA_Light__c = BIIN_UNICA_Utils.translateFromRemedyByID('SLM-Status', 1);
        }

        if(additionalData.get('Last Resolved Date') != null && additionalData.get('Last Resolved Date').trim() != '') 
        {
            ticket.BI2_fecha_hora_de_resolucion__c = startDateTime.addSeconds(Integer.valueOf(additionalData.get('Last Resolved Date').trim()));
        }

        if(additionalData.get('Closed Date') != null && additionalData.get('Closed Date').trim() != '') 
        {
            ticket.BIIN_Fecha_de_Cierre__c = startDateTime.addSeconds(Integer.valueOf(additionalData.get('Closed Date').trim()));
        }

        if(additionalData.get('Status Reason') != '' && additionalData.get('Status Reason') != null) 
        {
            ticket.BI_Status_Reason__c = additionalData.get('Status Reason');
        }

        System.debug(' BIIN_UNICA_RestHelper.generateTicket | Response generated ticket: ' + ticket);

        return ticket;
        
    }
    
    public static BIIN_UNICA_Pojos.TicketDetailType generateTicketDetail(Case ticket)
    {
        System.debug(' BIIN_UNICA_RestHelper.generateTicketDetail | Received ticket: ' + ticket);

        BIIN_UNICA_Pojos.TicketDetailType ticketDetail = new BIIN_UNICA_Pojos.TicketDetailType();
        
        ticketDetail.ticketId          = ticket.BI_Id_del_caso_legado__c; // UNICA mandatory
        ticketDetail.correlatorId      = ticket.BI_Id_del_caso_legado__c;
        ticketDetail.subject           = ticket.Subject; // UNICA mandatory
        ticketDetail.description       = ticket.Description; // UNICA mandatory
        ticketDetail.country           = ticket.BI_Country__c; // UNICA mandatory
        ticketDetail.customerId        = BIIN_UNICA_TicketManager.getTicketValidadorFiscal(ticket.AccountId);
        ticketDetail.reportedDate      = BIIN_UNICA_Utils.dateTimeToSeconds(ticket.BI_COL_Fecha_Radicacion__c);
        ticketDetail.creationDate      = BIIN_UNICA_Utils.dateTimeToSeconds(ticket.CreatedDate); // UNICA mandatory
        ticketDetail.severity          = ticket.TGS_Impact__c;
        ticketDetail.ticketType        = 'Incident'; // UNICA mandatory
        ticketDetail.parentTicket      = ticket.ParentId;
        ticketDetail.statusChangeDate  = BIIN_UNICA_Utils.dateTimeToSeconds(ticket.LastModifiedDate);
        ticketDetail.ticketSubstatus   = ticket.BI_Status_Reason__c;
        ticketDetail.resolutionDate    = BIIN_UNICA_Utils.dateTimeToSeconds(ticket.BI2_fecha_hora_de_resolucion__c);
        ticketDetail.resolution        = ticket.TGS_Resolution__c;
        ticketDetail.responsibleParty  = ticket.BI_Assigned_Group__c;
        ticketDetail.source            = ticket.Origin;
        
        ticketDetail.requestedPriority = Integer.valueOf(BIIN_UNICA_Utils.translateFromRemedyValueToRemedyID('Prioridad', String.valueOf(ticket.Priority)));
        ticketDetail.priority          = ticketDetail.requestedPriority;
        ticketDetail.ticketStatus      = BIIN_UNICA_Utils.stringToEnumValueForTicketStatusType(ticket.Status); // UNICA mandatory ticket.Status

        List<BIIN_UNICA_Pojos.KeyValueType> additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();

        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('First Name', ticket.BI2_Nombre_Contacto_Final__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Last Name', ticket.BI2_Apellido_Contacto_Final__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Internet E-mail', ticket.BI2_Email_Contacto_Final__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Urgency', ticket.TGS_Urgency__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Site', ticket.BIIN_Site__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('HPD_CI', ticket.BIIN_Id_Producto__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Categorization Tier 1', ticket.BIIN_Categorization_Tier_1__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Categorization Tier 2', ticket.BIIN_Categorization_Tier_2__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Categorization Tier 3', ticket.BIIN_Categorization_Tier_3__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Product Categorization Tier 1', ticket.BI_Product_Categorization_Tier_1__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Product Categorization Tier 2', ticket.BI_Product_Categorization_Tier_2__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Product Categorization Tier 3', ticket.BI_Product_Categorization_Tier_3__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution Category Tier 1', ticket.BI_Resolution_Category_Tier_1__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution Category Tier 2', ticket.BI_Resolution_Category_Tier_2__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution Category Tier 3', ticket.BI_Resolution_Category_Tier_3__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('codCUN', ticket.BI_COL_Codigo_CUN__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Case Number', BIIN_UNICA_TicketManager.getTicketCaseNumber(ticket.BI_Id_del_caso_legado__c)));

        ticketDetail.additionalData = additionalData;

        System.debug(' BIIN_UNICA_RestHelper.generateTicketDetail | Response ticketDetail: ' + ticketDetail);
        
        return ticketDetail;
    }
    
    public static void checkTicketRequestMandatoryFields(BIIN_UNICA_Pojos.TicketRequestType ticketRequest)
    {
        List<String> emptyFields = new List<String>();
        Boolean throwException = false;
        if (ticketRequest.subject == '' || ticketRequest.subject == null) 
        {
            emptyFields.add('subject');
            throwException = true;
        }
        if (ticketRequest.description == null) 
        {
            emptyFields.add('description');
            throwException = true;
        }
        if (ticketRequest.country == null) 
        {
            emptyFields.add('country');
            throwException = true;
        }
        if (ticketRequest.ticketType == '' || ticketRequest.ticketType == null) 
        {
            emptyFields.add('ticketType');
            throwException = true;
        }

        if (throwException) 
        {
            System.debug(' BIIN_UNICA_RestHelper.checkTicketRequestMandatoryFields | EXCEPTION Missing mandatory parameter: ' + emptyFields);
            throw new BIIN_UNICA_BadRequestException('Missing mandatory parameter: ' + emptyFields);
        }
    }
}