public without sharing class CWP_CalendarController {
	
    @auraEnabled
    public static list<list<CWP_LHelper.calendarDayWrap>> getDayList(string month1, string year1, string firstWeekDay, String calendarUser){
        integer month = integer.valueOf(month1);
        integer year = integer.valueof(year1);
        
        
		User currentUser = [SELECT Id, Name, AccountId, Contact.BI_Cuenta_activa_en_portal__c FROM User WHERE Id =: userInfo.getUserId()];
        System.debug('user: '+currentUser);
        Account currentAccount = CWP_LHelper.getCurrentAccount(currentUser.AccountId);
        System.debug('Account: '+currentAccount);
        List<BI_Contact_Customer_Portal__c> customerContacts = [SELECT BI_Activo__c,BI_Cliente__c,BI_Contacto__c,BI_User__c,Id FROM BI_Contact_Customer_Portal__c where BI_User__c=:currentUser.id];
		System.debug('customerContacts: ' + customerContacts);
        List<Id> contacts = new List<Id>();
        for(BI_Contact_Customer_Portal__c item: customerContacts){
            contacts.add(item.BI_Contacto__c);
        }
        list<Event> evtList = new list<Event>();
        if(calendarUser != 'current'){
            System.debug('calendarUser: '+calendarUser);
            System.debug('currentUser: '+currentUser.id);
            evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where OwnerId = :calendarUser];
			system.debug('eventos del usuario del Telefonica team	' + evtList);
            
        }else{

            List<EventRelation> eventRelation = [SELECT EventId,Id,RelationId FROM EventRelation WHERE RelationId in :contacts];
            List<Id> eventRelatedIds = new List<Id>();
            for(EventRelation evR: eventRelation){
                eventRelatedIds.add(evR.EventId);
            }
            Id accId = BI_AccountHelper.getCurrentAccountId();
            Account acc = [select Id, OwnerId from Account where Id=:accId limit 1];
            
            if(acc!= null){
                system.debug('acc.OwnerId	' + acc.OwnerId);
                system.debug('currentUser.Id	' + currentUser.Id);
                system.debug('eventRelatedIds	' + eventRelatedIds);
            	evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where  BI_ParentId__c = NULL AND ((OwnerId = :acc.OwnerId and createdById = :currentUser.Id) OR Id IN: eventRelatedIds) ];
				system.debug('eventos del usuario logado, de la cuenta y asociados	' + evtList);
            }else{
                evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where BI_ParentId__c = NULL AND (createdById = :currentUser.Id OR Id in :eventRelatedIds)];
				system.debug('eventos del usuario y asociados	' + evtList);
            }
        }
        system.debug('lista de eventos encontradps	' +evtList);
        map<Date, list<Event>> evtMap = CWP_LHelper.sortEventsByDay(evtList);
        list<list<CWP_LHelper.calendarDayWrap>> retList = CWP_LHelper.getDayList(month, year, integer.valueOf(firstWeekDay), evtMap);
        
        return retList;
    }
    
    @auraEnabled
    public static String getCalendarInfo(){
		User currentUser = [SELECT Id, Name FROM User WHERE Id =: userInfo.getUserId()];
		System.debug(currentUser);
        String result=Label.BI_LabelCalendario1;
        if(!string.isBlank(currentUser.Id)){
            try{
                result = Label.BI_LabelCalendario+' ' +currentUser.Name;
            }catch(exception e){
                system.debug('no user found for id	'+ currentUser.id);
            }
        } 
        return result;
    }
    
    @auraEnabled
    public static User getUser() {
    	User currentUser = [SELECT Id, Name, LanguageLocaleKey FROM User WHERE Id =: userInfo.getUserId()];
    	return currentUser;
    }
    
    @auraEnabled
    public static User getTeamUser(Id teamUserId) {
    	User teamUser = [SELECT Id, Name, LanguageLocaleKey FROM User WHERE Id =: teamUserId];
    	return teamUser;
    }
}