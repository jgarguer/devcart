@isTest
private class BI_Recarterizacion_TEST {
	
	static{BI_TestUtils.throw_exception = false;}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that load all information that will be used in test
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testSetup static void setUp() {
		
		BI_Recarterizacion__c recart = new BI_Recarterizacion__c(
			Name = 'test1'
		);
		insert recart;

		CaseTeamRole ctr = [SELECT Id FROM CaseTeamRole LIMIT 1];



		List <Account> lst_acc = BI_DataLoad.loadAccountsWithCountry(10,new List<String>{Label.BI_Chile});
		System.assertEquals(lst_acc.isEmpty(), false);

		List <Opportunity> lst_opp = new List <Opportunity>();
		List <Case> lst_case = new List <Case>();
		List <AccountTeamMember> lst_atm = new List <AccountTeamMember>();
		RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' LIMIT 1];

		Integer i = 1;
		for(Account acc : lst_acc){

			Opportunity opp = new Opportunity(
            	CloseDate = Date.today(),
            	StageName = Label.BI_F6Preoportunidad,
            	AccountId = acc.Id,
            	BI_Ciclo_ventas__c = Label.BI_Completo,
            	BI_Country__c = acc.BI_country__c,
            	Name = 'TestOppopen' +i
            );
            lst_opp.add(opp);
            Opportunity opp2 = new Opportunity(
            	CloseDate = Date.today(),
            	StageName = Label.BI_F1CanceladaSusp,
            	AccountId = acc.Id,
            	BI_Ciclo_ventas__c = Label.BI_Completo,
            	BI_Country__c = acc.BI_country__c,
            	Name = 'TestOppclosed' +i
            );
            lst_opp.add(opp2);
            Case cas = new Case(
            	Subject = 'Test Subject Acc open' + i,
                AccountId = acc.Id,
                BI_Country__c = acc.BI_Country__c,
                Status= Label.BI_CaseStatus_InProgress,
                Origin = 'Email',
                RecordTypeId = rt.Id
            );
            lst_case.add(cas);
            Case cas2 = new Case(
            	Subject = 'Test Subject Acc open' + i,
                AccountId = acc.Id,
                BI_Country__c = acc.BI_Country__c,
                Status= Label.BI_CaseStatus_Closed,
                Origin = 'Email',
                RecordTypeId = rt.Id
            );
            lst_case.add(cas2);

            AccountTeamMember atm = new AccountTeamMember(
            	AccountId = acc.Id,
            	UserId = UserInfo.getUserId(),
            	TeamMemberRole = 'Account Manager'
            );
            lst_atm.add(atm);
            i++;
		}
		insert lst_opp;
		insert lst_case;
		insert lst_atm;

		BI_DataLoad.loadOpportunityTeamMembers(1, UserInfo.getUserId(), lst_opp);
		

		List <CaseTeamMember> lst_ctm = new List <CaseTeamMember>();
		for(Case cas : lst_case){

			CaseTeamMember caseTM = new CaseTeamMember();
            caseTM.MemberId = UserInfo.getUserId();
            caseTM.TeamRoleId = ctr.Id;
            caseTM.ParentId = cas.Id;

            lst_ctm.add(caseTM);
		}

		insert lst_ctm;

		

		List <User> lst_usu = BI_DataLoad.loadUsers(1, [SELECT Id FROM Profile WHERE Name = 'BI_Standard_CHI' LIMIT 1].Id,Label.BI_COL_Ejecutivo_cliente );
		System.assertEquals(lst_usu.isEmpty(),false);

		List <BI_Linea_de_Recarterizacion__c> lst_lineas = new List <BI_Linea_de_Recarterizacion__c>();
		for(Account acc : lst_acc){

			BI_Linea_de_Recarterizacion__c linea = new BI_Linea_de_Recarterizacion__c(
				BI_AccountId__c = acc.Id,
				BI_Borrar_Equipo_de_Cuenta__c = true,
				BI_Closed_Opp__c = true,
				BI_Open_Agrupacion_Opp__c = true,
				BI_Closed_Agrupacion_Opp__c = true,
				BI_Open_Opp__c = true,
				BI_Borrar_Equipo_de_Oportunidad__c = true,
				BI_Casos_abiertos__c = true,
				BI_Casos_cerrados__c = true,
				BI_Borrar_Equipo_de_Casos__c = true,
				BI_Recarterizacion__c = recart.Id,
				BI_NewOwner__c = lst_usu[0].Id
			);
			lst_lineas.add(linea);
		}
		insert lst_lineas;
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_Recarterizacion_Acc_Batch
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void BI_Recarterizacion_Acc_Batch_TEST() {
		
		List<BI_Recarterizacion__c> lst_recart = [SELECT Id FROM BI_Recarterizacion__c WHERE Name = 'test1'];
		System.assertEquals(lst_recart.isEmpty(), false);
		BI_Recarterizacion_Acc_Batch  recart_Batch = new BI_Recarterizacion_Acc_Batch(lst_recart[0].Id);

		Test.startTest();
        Database.executeBatch(recart_Batch, 2000);
        Test.stopTest();

        List <Account> lst_acc = [SELECT Id FROM Account WHERE OwnerId =: UserInfo.getUserId()];
        System.assertEquals(lst_acc.isEmpty(),true);
        List <Attachment> lst_att = [SELECT Id FROM Attachment WHERE ParentId =: lst_recart[0].id];
        System.assertEquals(lst_att.size(),1);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_GenerateCaseCSV_Batch
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void BI_GenerateCaseCSV_Batch_TEST(){

		List<BI_Recarterizacion__c> lst_recart = [SELECT Id FROM BI_Recarterizacion__c WHERE Name = 'test1'];		
		System.assertEquals(lst_recart.isEmpty(), false);
		
		BI_Linea_de_Recarterizacion__c linea = [SELECT Id, BI_Cases_RecordTypes__c FROM BI_Linea_de_Recarterizacion__c WHERE BI_Recarterizacion__c =: lst_recart[0].id LIMIT 1];
		linea.BI_Cases_RecordTypes__c = 'BI_Caso_Interno';
		update linea;

		BI_GenerateCaseCSV_Batch  recart_Batch = new BI_GenerateCaseCSV_Batch(lst_recart[0].Id);

		Test.startTest();
        Database.executeBatch(recart_Batch, 2000);
        Test.stopTest();

        List <Attachment> lst_att = [SELECT Id FROM Attachment WHERE ParentId =: lst_recart[0].id];
        System.assertEquals(lst_att.size(),2);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_GenerateOppCSV_Batch
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void BI_GenerateOppCSV_Batch_TEST(){

		List<BI_Recarterizacion__c> lst_recart = [SELECT Id FROM BI_Recarterizacion__c WHERE Name = 'test1'];		
		System.assertEquals(lst_recart.isEmpty(), false);

		BI_GenerateOppCSV_Batch recart_Batch = new BI_GenerateOppCSV_Batch(lst_recart[0].Id);

		Test.startTest();
        Database.executeBatch(recart_Batch, 2000);
        Test.stopTest();

        List <Attachment> lst_att = [SELECT Id FROM Attachment WHERE ParentId =: lst_recart[0].id];
        System.assertEquals(lst_att.size(),2);

	}
	
}