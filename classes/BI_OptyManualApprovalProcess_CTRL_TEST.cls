/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Francisco Ayllon
    Company:       Aborda
    Description:   Test method to manage the code coverage for BI_OptyManualApprovalProcess_CTRL
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    09/10/2015              Francisco Ayllon        Initial version
    18/07/2016		    Manuel Medina	    Changed Opty record type label at automaticApproval
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

//SeeAllData cause Approval Process being tested
@isTest
private class BI_OptyManualApprovalProcess_CTRL_TEST {
    
    @isTest static void manualApproval() {
        List<RecordType> lst_rt = new List<RecordType>([SELECT Id, Name FROM RecordType WHERE Name = 'Oportunidad Local' limit 1]);

        List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        Opportunity opp = new Opportunity(CloseDate = Date.today(),
                                        StageName = Label.BI_F5DefSolucion,
                                        Probability = 0,
                                        Name = Label.BI_Test,
                                        AccountId = lst_acc[0].Id,
                                        RecordType = lst_rt[0],
                                        BI_Opportunity_Type__c = 'Alta',
                                        Amount = 100,
                                        BI_Country__c = Label.BI_Argentina,
                                        BI_Duracion_del_contrato_Meses__c = 12,
                                        BI_Plazo_estimado_de_provision_dias__c = 12,
                                        CurrencyIsoCode = 'ARS',
                                        BI_ApprovalStep__c = Label.BI_Factibilidad_Economica,
                                        BI_Enviar_a_aprobacion__c = true,
                                        BI_Manual_Approver__c = true
                                        );
                                        
        insert opp;

        Test.startTest();
        
        ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(opp);
        BI_OptyManualApprovalProcess_CTRL approvalPage = new BI_OptyManualApprovalProcess_CTRL(ctr);
        
        approvalPage.submitForApproval();
        approvalPage.isManual();
        approvalPage.redirect();

        Test.stopTest();

    }
    
    @isTest static void automaticApproval() {
        List<RecordType> lst_rt = new List<RecordType>([SELECT Id, Name FROM RecordType WHERE Name = 'Oportunidad Local' limit 1]);

        List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        Opportunity opp = new Opportunity(CloseDate = Date.today(),
                                        StageName = Label.BI_F5DefSolucion,
                                        Probability = 0,
                                        Name = Label.BI_Test,
                                        AccountId = lst_acc[0].Id,
                                        RecordType = lst_rt[0],
                                        BI_Opportunity_Type__c = 'Alta',
                                        Amount = 100,
                                        BI_Country__c = Label.BI_Argentina,
                                        BI_Duracion_del_contrato_Meses__c = 12,
                                        BI_Plazo_estimado_de_provision_dias__c = 12,
                                        CurrencyIsoCode = 'ARS',
                                        BI_ApprovalStep__c = Label.BI_Factibilidad_Economica,
                                        BI_Enviar_a_aprobacion__c = true,
                                        BI_Manual_Approver__c = false
                                        );
                                        
        insert opp;

        Test.startTest();
        
        ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(opp);
        BI_OptyManualApprovalProcess_CTRL approvalPage = new BI_OptyManualApprovalProcess_CTRL(ctr);
        
        approvalPage.submitForApproval();
        approvalPage.isManual();
        approvalPage.redirect(); 

        Test.stopTest();

    }
    
    @isTest static void errorApproval() {
        List<RecordType> lst_rt = new List<RecordType>([SELECT Id, Name FROM RecordType WHERE Name = 'Oportunidad Local' limit 1]);

        List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        Opportunity opp = new Opportunity(CloseDate = Date.today(),
                                        StageName = Label.BI_F5DefSolucion,
                                        Probability = 0,
                                        Name = Label.BI_Test,
                                        AccountId = lst_acc[0].Id,
                                        RecordType = lst_rt[0],
                                        BI_Opportunity_Type__c = 'Alta',
                                        Amount = 100,
                                        BI_Country__c = Label.BI_Argentina,
                                        BI_Duracion_del_contrato_Meses__c = 12,
                                        BI_Plazo_estimado_de_provision_dias__c = 12,
                                        CurrencyIsoCode = 'ARS',
                                        BI_ApprovalStep__c = '',
                                        BI_Enviar_a_aprobacion__c = false,
                                        BI_Manual_Approver__c = false
                                        );
                                        
        insert opp;

        Test.startTest();
        
        ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(opp);
        BI_OptyManualApprovalProcess_CTRL approvalPage = new BI_OptyManualApprovalProcess_CTRL(ctr);
        
        approvalPage.submitForApproval();
        approvalPage.isManual();
        approvalPage.redirect(); 

        Test.stopTest();

    } 
}