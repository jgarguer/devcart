/***********************************************************************************************
* Avanxo Colombia
* @author           Andrea Nayibe Castañeda href=<acastaneda@avanxo.com>
* Proyecto:         Telefonica
* Descripción:     Clase de rueba que realiza la cobertura del trigger BI_COL_Attachment_tgr

* Control de Cambios (Versión)
* -----------------------------------------------------------------------------------------------
*             No.     Fecha                Autor                   Descripción
*           -----   ----------      --------------------    -------------------------------------
* @version   1.0    2015-11-09      Andrea Castañeda (AC)   Clase de Prueba
*					19/09/2017		Angel F. Santaliestra 	Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
**************************************************************************************************/
@isTest
private class BI_COL_Attachment_tst 
{

	public static final String strTSTCase1	= 'TSTCase1';
	public static final String strTSTCase2	= 'TSTCase2';

	public static List <User>		lstUsuarios;
	public static List <Profile>	lstPerfil;
	public static List <UserRole>	lstRoles;
	public static List <Attachment>	setAttachment;
	public static List <Id>					setAnexos;
	public static Case 				objCOLCase	= 	new Case();
	public static Opportunity 		objOportunidad;
	public static User uariosJefe;

	public static User getUser(String n,ID IDProfile)
	{	
		User sEmp = new User();
		
		sEmp.LastName = 'Test Shipping'+n;
		sEmp.ProfileId = IDProfile;
		sEmp.Email = 'test@test.com'+n;
		sEmp.Username = 'charityg@test.com'+n;
		sEmp.Alias = n; 
		sEmp.TimeZoneSidKey = 'America/Bogota'; 
		sEmp.LocaleSidKey = 'es_CO';
		sEmp.EmailEncodingKey = 'UTF-8';
		sEmp.LanguageLocaleKey = 'es';
		sEmp.Pais__c = 'Colombia';
		
		return sEmp;
	}

	public static void crearData ()
	{
		//perfiles
		lstPerfil                       = new list <Profile>();
		lstPerfil                       = [Select id,Name from Profile where Name='BI_Administrator'];
		system.debug('datos Perfil '+lstPerfil);
		
		//usuarios
		User usuarioTest=getUser('prueba1',lstPerfil[0].Id);
		usuarioTest.BI_Permisos__c='Jefe de Ventas';//'Aplazamiento';
		insert usuarioTest;
		lstUsuarios = [Select Id, Name, BI_Permisos__c FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true And BI_Permisos__c !=: 'Administrador del sistema' Limit 1 ]; //And BI_Permisos__c =: 'Jefe de Ventas'
		uariosJefe=usuarioTest;

		System.debug('Lista de usuario =====================> '+lstUsuarios);
		System.debug(usuarioTest +' >>>>>---- '+UserInfo.getProfileId());
		
		//System.runAs(lstUsuarios[0])
		//{

			System.debug(usuarioTest +' >>>>>---- '+UserInfo.getProfileId());

			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			System.debug('Datos de objBibypass ======>'+ objBibypass);
			
			//Roles
			//lstRoles                = new list <UserRole>();
			//lstRoles                = [Select id,Name from UserRole where Name = 'Administrador del sistema'];
			//system.debug('datos Rol '+lstRoles);

			String strIdRecordType  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
			
			// Cuenta
			Account objAccount								= new Account();			
			objAccount.Name 								= 'Prueba';
			objAccount.RecordTypeId							= strIdRecordType;
			objAccount.BI_Country__c						= 'Colombia';
			objAccount.TGS_Region__c						= 'América';
			objAccount.BI_Tipo_de_identificador_fiscal__c	= '';
			objAccount.BI_No_Identificador_fiscal__c		= '112345676';
			objAccount.BI_Segment__c                        = 'test';
          	objAccount.BI_Subsegment_Regional__c            = 'test';
          	objAccount.BI_Territory__c                      = 'test';
			insert objAccount;
			System.debug('Datos de objAccount ======>' + objAccount );

			//Contacto
			Contact objContact								=	new Contact();
			objContact.LastName								= 'Test';
			objContact.AccountId 							=	objAccount.Id;
			objContact.BI_Country__c 						=	'Colombia';
			objContact.BI_Tipo_de_documento__c 				= 	'Pasaporte';
			objContact.Email 								= 	'a@avanxo.com';
			insert objContact;
			System.debug('Datos de objContact ======>' + objContact );

			//Caso
			objCOLCase 								= 	new Case();
			objCOLCase.AccountId							=	objAccount.Id;
			objCOLCase.ContactId							=	objContact.Id;
			objCOLCase.Description 							=	'Prueba1';
			insert objCOLCase;
			System.debug('Datos de objCOLCase ======>' + objCOLCase );

			String strRTAnexos	= [SELECT Id
	                    	FROM RecordType
							WHERE SobjectType = 'BI_COL_Anexos__c'
							AND DeveloperName = 'BI_FUN'
							LIMIT 1].Id;

			BI_COL_Anexos__c objCOLAnexo	= new BI_COL_Anexos__c();
			objCOLAnexo.Name				= 'FUN-0041414';
			objCOLAnexo.RecordTypeId		= strRTAnexos;
			insert objCOLAnexo;
			System.debug('Datos de objCOLAnexo ======>' + objCOLAnexo );

			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objAccount.Id;
			objOportunidad.BI_Country__c                          	= 'Colombia';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F1 - Cancelled | Suspended';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			objOportunidad.OwnerId                                	= usuarioTest.id;
			insert objOportunidad;
			System.debug('Datos de objOportunidad ======>' + objOportunidad );
			
			list<Opportunity> lstOportunidad 	= new list<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			System.debug('Datos de lstOportunidad ======>' + lstOportunidad );

			//Attachment
			Attachment objAttachment						= new Attachment();
			objAttachment.Body								= Blob.valueOf( 'TSTAttachment' );
			objAttachment.ParentId							= objCOLCase.Id;
			objAttachment.Name								= strTSTCase2;
			insert objAttachment;
			System.debug('Datos de objAttachment ======>' + objAttachment );

			setAttachment = new List<Attachment>();
			setAttachment.add(objAttachment);

			System.debug('lista de Attachment ============>'+setAttachment);



		//}		

	}

	@isTest static void test_method_one() 
	{
		crearData();

		//System.runAs(usuarioTest)
		//{
			//lstUsuarios = [Select Id, Name, BI_Permisos__c FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ]; //BI_Permisos__c !=: 'Administrador del sistema' And BI_Permisos__c =: 'Jefe de Ventas'
			System.debug('informacion lista ===============> '+lstUsuarios);

			Test.startTest();
				//System.runAs(lstUsuarios[0])
				//{
					try
					{
						delete setAttachment;
						//system.assert( false );
					}
					catch( Exception error )
					{
						//system.assert( true );
					}
				//}


				System.debug('lista de Attachment borrada ============>'+setAttachment);

			Test.stopTest();
		//}
	}

	@isTest static void test_method_two() 
	{
		crearData();	

		String strRTAnexos	= [SELECT Id
	                   	FROM RecordType
							WHERE SobjectType = 'BI_COL_Anexos__c'
							AND DeveloperName = 'BI_FUN'
							LIMIT 1].Id;

		BI_COL_Anexos__c objCOLAnexo	= new BI_COL_Anexos__c();
			objCOLAnexo.Name				= 'FUN-0041414';
			objCOLAnexo.RecordTypeId		= strRTAnexos;
			insert objCOLAnexo;
		//	System.debug('Datos de objCOLAnexo ======>' + objCOLAnexo );

		//	setAnexos = new List<Id>();
		//	setAnexos.add(objCOLAnexo.id);	

			Attachment objAttachment2						= new Attachment();
			objAttachment2.Body								= Blob.valueOf( 'TSTAttachment' );
			objAttachment2.ParentId							= objCOLAnexo.Id;
			objAttachment2.Name								= strTSTCase2;
			insert objAttachment2;
		//	System.debug('Datos de objAttachment ======>' + objAttachment );

		//	setAttachment = new List<Attachment>();
		//	setAttachment.add(objAttachment);

		//	System.debug('lista de Attachment 2============>'+setAttachment);

        //lstUsuarios = [Select Id, Name, BI_Permisos__c FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true And BI_Permisos__c !=: 'Administrador del sistema' Limit 1 ]; //And BI_Permisos__c =: 'Jefe de Ventas'

        //System.debug('informacion lista ===============> '+lstUsuarios);
        
        Test.startTest();   
   			System.runAs(uariosJefe)
			{
				try
				{
					delete objAttachment2;
					//system.assert( false );
				}
				catch( Exception error )
				{
					//system.assert( true );
				}
			}
        Test.stopTest();
	}
	
	//public static final String strTSTCase1	= 'TSTCase1';
	//public static final String strTSTCase2	= 'TSTCase2';

	//public static List <User>				lstUsuarios;
	//public static List <Profile>			lstPerfil;
	//public static List <UserRole>			lstRoles;
	//public static List <Attachment>			setAttachment;
	//public static List <Id>					setAnexos;
	//public static Case 						objCOLCase	= 	new Case();
	//public static BI_COL_Attachment_cls    	objAtach;	


	//public static void crearData ()
	//{
	//	//perfiles
	//	lstPerfil                       = new list <Profile>();
	//	lstPerfil                       = [Select id,Name from Profile where Name = 'Administrador del sistema'];
	//	system.debug('datos Perfil '+lstPerfil);
		
	//	//usuarios
	//	lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		
	//	System.runAs(lstUsuarios[0])
	//	{
	//		BI_bypass__c objBibypass = new BI_bypass__c();
	//		objBibypass.BI_migration__c=true;
	//		insert objBibypass;
	//		System.debug('Datos de objBibypass ======>'+ objBibypass);
			
	//		//Roles
	//		lstRoles                = new list <UserRole>();
	//		lstRoles                = [Select id,Name from UserRole where Name = 'Administrador del sistema'];
	//		system.debug('datos Rol '+lstRoles);

	//		String strIdRecordType  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
			
	//		// Cuenta
	//		Account objAccount								= 	new Account();			
	//		objAccount.Name 								= 	'Prueba';
	//		objAccount.RecordTypeId							= 	strIdRecordType;
	//		objAccount.BI_Country__c						= 	'Colombia';
	//		objAccount.TGS_Region__c						= 	'América';
	//		objAccount.BI_Tipo_de_identificador_fiscal__c	= 	'';
	//		objAccount.BI_No_Identificador_fiscal__c		=	'112345676';
	//		insert objAccount;
	//		System.debug('Datos de objAccount ======>' + objAccount );

	//		//Contacto
	//		Contact objContact								=	new Contact();
	//		objContact.LastName								= 'Test';
	//		objContact.AccountId 							=	objAccount.Id;
	//		objContact.BI_Country__c 						=	'Colombia';
	//		objContact.BI_Tipo_de_documento__c 				= 	'Pasaporte';
	//		objContact.Email 								= 	'a@avanxo.com';
	//		insert objContact;
	//		System.debug('Datos de objContact ======>' + objContact );

	//		//Caso
	//		objCOLCase 								= 	new Case();
	//		objCOLCase.AccountId							=	objAccount.Id;
	//		objCOLCase.ContactId							=	objContact.Id;
	//		objCOLCase.Description 							=	'Prueba1';
	//		insert objCOLCase;
	//		System.debug('Datos de objCOLCase ======>' + objCOLCase );

	//		Attachment objAttachment						= new Attachment();
	//		objAttachment.Body								= Blob.valueOf( 'TSTAttachment' );
	//		objAttachment.ParentId							= objCOLCase.Id;
	//		objAttachment.Name								= strTSTCase2;
	//		insert objAttachment;
	//		System.debug('Datos de objAttachment ======>' + objAttachment );

	//		setAttachment = new List<Attachment>();
	//		setAttachment.add(objAttachment);

	//		System.debug('lista de Attachment ============>'+setAttachment);



	//	}		

	//}

	//@isTest static void test_method_one() 
	//{
	//	crearData();		

 //       Test.startTest();

 //       	delete setAttachment;

 //       	System.debug('lista de Attachment borrada ============>'+setAttachment);

 //       Test.stopTest();
	//}

	//@isTest static void test_method_two() 
	//{
	//	crearData();		

	//	String strRTAnexos	= [SELECT Id
	//                    	FROM RecordType
	//						WHERE SobjectType = 'BI_COL_Anexos__c'
	//						AND DeveloperName = 'BI_FUN'
	//						LIMIT 1].Id;

	//	BI_COL_Anexos__c objCOLAnexo	= new BI_COL_Anexos__c();
	//		objCOLAnexo.Name				= 'FUN-0041414';
	//		objCOLAnexo.RecordTypeId		= strRTAnexos;
	//		insert objCOLAnexo;
	//		System.debug('Datos de objCOLAnexo ======>' + objCOLAnexo );

	//		setAnexos = new List<Id>();
	//		setAnexos.add(objCOLAnexo.id);	

	//		Attachment objAttachment						= new Attachment();
	//		objAttachment.Body								= Blob.valueOf( 'TSTAttachment' );
	//		objAttachment.ParentId							= objCOLAnexo.Id;
	//		objAttachment.Name								= strTSTCase2;
	//		insert objAttachment;
	//		System.debug('Datos de objAttachment ======>' + objAttachment );

	//		setAttachment = new List<Attachment>();
	//		setAttachment.add(objAttachment);

	//		System.debug('lista de Attachment 2============>'+setAttachment);

 //       User objUser	= new User();
 //       objUser	 = [SELECT Id
	//				FROM User
	//				WHERE ProfileId	!=: System.UserInfo.getProfileId()
 //       			AND IsActive = true
 //       			LIMIT 1
 //       			].get( 0 );
        
 //       Test.startTest();   

	//        delete setAttachment;
 //       Test.stopTest();
	//}
	
}