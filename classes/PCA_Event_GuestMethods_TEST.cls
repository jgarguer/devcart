@isTest
private class PCA_Event_GuestMethods_TEST {

    static testMethod void triggerOn() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        User user1;
        //List<User> usr = BI_DataLoad.loadUsers(1,BI_DataLoad.searchAdminProfile(),Label.BI_Administrador);
        Id idProf = [select id, name from Profile where name = 'System Administrator' or name = 'Administrador del sistema' limit 1].Id;
        User usr = new User (alias = 'stttt6',email='6u@testorg.com',emailencodingkey='UTF-8', lastname='Testing6',languagelocalekey='en_US',localesidkey='en_US',ProfileId = idProf, BI_Permisos__c=Label.BI_Administrador, timezonesidkey=Label.BI_TimeZoneLA, username= '6ur23@testorg.com');        
        insert usr;
        User usr2 = new User (alias = 'stttt3',email='3u@testorg.com',emailencodingkey='UTF-8', lastname='Testing3',languagelocalekey='en_US',localesidkey='en_US',ProfileId = idProf, BI_Permisos__c=Label.BI_Administrador, timezonesidkey=Label.BI_TimeZoneLA, username= '3ur23@testorg.com');
        insert usr2;
        system.runAs(usr){
                List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
                List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
                List<Account> accList = new List<Account>();
                for(Account item: acc){
                    item.OwnerId = usr.Id;
                    accList.add(item);
            }
            update accList;                
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);                                          
            //user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
        	//insert con;
            DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
        	//system.runAs(usr[0]){ 
        	
       		Event Evento1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
        	insert Evento1;
            EventRelation evrel=new EventRelation(EventId=Evento1.id, RelationId=con[0].Id);
            insert evrel;
        	Event__c Evento2 = new Event__c(Subject__c='Nuevo Evento Custom',Description__c='Test Evento Custom',GuestList__c=usr2.Id, ownerId = acc[0].ownerId,ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,StandardEventId__c=Evento1.Id);
        	insert Evento2;
            
        	Event__c Evento3 = new Event__c(Subject__c='Nuevo Evento Custom',Description__c='Test Evento Custom',GuestList__c=usr2.Id, ownerId = acc[0].ownerId,ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
        	insert Evento3;
            
            List<Event__c>lstEv=new List<Event__c>();
            lstEv.add(Evento2);
            PCA_Event_GuestMethods.deleteEvent(lstEv);
            
            Event Evento4 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
        	insert Evento4;
            Event__c Evento5 = new Event__c(Subject__c='Nuevo Evento Custom',Description__c='Test Evento Custom',GuestList__c=con[0].Id, ownerId = acc[0].ownerId,ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,StandardEventId__c=Evento4.Id);
        	insert Evento5;
            PCA_Event_GuestMethods.PCA_InsertEvent(new Event__c[] {Evento5});
            
        
            //system.runAs(user1){
                //Event Evento1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
                //insert Evento1;
                PCA_Event_GuestMethods constructor = new PCA_Event_GuestMethods();
            //}
        //}
        }
    }
    
    static testMethod void triggerOn2(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        User user1;
        //List<User> usr = BI_DataLoad.loadUsers(1,BI_DataLoad.searchAdminProfile(),Label.BI_Administrador);
        Id idProf = [select id, name from Profile where name = 'System Administrator' or name = 'Administrador del sistema' limit 1].Id;
        User usr = new User (alias = 'stttt6',email='6u@testorg.com',emailencodingkey='UTF-8', lastname='Testing6',languagelocalekey='en_US',localesidkey='en_US',ProfileId = idProf, BI_Permisos__c=Label.BI_Administrador, timezonesidkey=Label.BI_TimeZoneLA, username= '6ur23@testorg.com');        
        insert usr;
        User usr2 = new User (alias = 'stttt3',email='3u@testorg.com',emailencodingkey='UTF-8', lastname='Testing3',languagelocalekey='en_US',localesidkey='en_US',ProfileId = idProf, BI_Permisos__c=Label.BI_Administrador, timezonesidkey=Label.BI_TimeZoneLA, username= '3ur23@testorg.com');
        insert usr2;
        system.runAs(usr){
                List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
                List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
                List<Account> accList = new List<Account>();
                for(Account item: acc){
                    item.OwnerId = usr.Id;
                    accList.add(item);
            }
            update accList;                
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);                                          
            //user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
        
            DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
        //system.runAs(usr[0]){ 
        Event Evento1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
        insert Evento1;
        Event__c Evento2 = new Event__c(Subject__c='Nuevo Evento Custom',Description__c='Test Evento Custom',GuestList__c=usr.Id, ownerId = acc[0].ownerId,ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,StandardEventId__c=Evento1.Id);
        insert Evento2;
            //system.runAs(user1){
                //Event Evento1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
                //insert Evento1;
                PCA_Event_GuestMethods constructor = new PCA_Event_GuestMethods();
            //}
        //}
        }
    }
    
    static testMethod void sendEmailTest(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PCA_Event_GuestMethods.sendEmail(
        [SELECT ApiVersion,Body,BrandTemplateId,CreatedById,CreatedDate,Description,DeveloperName,
        Encoding,FolderId,HtmlValue,Id,IsActive,LastModifiedById,LastModifiedDate,LastUsedDate,Markup,
        Name,NamespacePrefix,OwnerId,Subject,SystemModstamp,TemplateStyle,TemplateType,TimesUsed 
        FROM EmailTemplate limit 1],
        new String[]{'test@test.com'});
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}