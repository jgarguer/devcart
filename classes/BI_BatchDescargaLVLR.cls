global class BI_BatchDescargaLVLR implements Database.Batchable<sObject>,Database.Stateful {
	
	String query;
	Integer OPTION;
	Id USER_ID;

	global Integer TOTAL_REGISTROS_OK;
	global Integer TOTAL_REGISTROS_KO;

	final static Integer BI_MAXLIMIT_AGENDAMIENTOS_BUTTONS;
    final static Integer BI_MAXLIMIT_BATCHDESCARGALVLR;
    static Set<Id> LST_LOGIDS = new Set<Id>();

    static{
       BI_MAXLIMIT_AGENDAMIENTOS_BUTTONS = Integer.valueOf(Label.BI_MaxLimit_Agendamientos_Buttons);
       BI_MAXLIMIT_BATCHDESCARGALVLR     = Integer.valueOf(Label.BI_MaxLimit_BatchDescargaLVLR);
    }


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos  
	Company:       Aborda.es
	Description:   

		OPTION 1 -> BI_Buttons.solicitudBOLineaVentaCHI
		OPTION 2 -> BI_Buttons.solicitudlogisticaLineaVentaCHI
		OPTION 3 -> BI_Buttons.solicitudBOLineaRecambioCHI
		OPTION 4 -> BI_Buttons.solicitudlogisticaLineaRecambioCHI
		OPTION 5 -> BI_Buttons.solicitudBOLineaServicioCHI
		OPTION 6 -> BI_Buttons.solicitudcontingenciaLineaVentaCHI
		OPTION 7 -> BI_Buttons.solicitudcontingenciaLineaRecambioCHI
	
	History: 
	
	<Date>                  <Author>                <Change Description>
	02/09/2015              Micah Burgos          	Initial Version     
	07/12/2015				Guillermo Muñoz			Prevent the email send in Test class
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	global BI_BatchDescargaLVLR(Integer p_option, Id p_user_id) {
		OPTION = p_OPTION;
		USER_ID = p_USER_ID;

		System.debug('Batch: BI_BatchDescargaLVLR --> p_option: ' + p_option);
		System.debug('Batch: BI_BatchDescargaLVLR --> p_user_id: ' + p_user_id);

		if(OPTION == 1){//solicitudBOLineaVentaCHI

            query = 'SELECT Id, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Venta__c WHERE BI_Solicitud_descargada_BackOffice__c = false AND BI_Validada__c = true AND BI_Estado_de_linea_de_venta__c = \'Entregado\'AND ((BI_Estado_de_courier__c IN (\'Entregado\',\'En Courier\') AND BI_Solicitud__r.BI_Tipo_solicitud__c IN (\'Normal\',\'Split billing\',\'Contingencia\')) OR (BI_Estado_de_courier__c = \'Entregado\' AND BI_Solicitud__r.BI_Tipo_solicitud__c = \'Portabilidad\'))';

		}else if(OPTION == 2){//solicitudlogisticaLineaVentaCHI

			query = 'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Venta__c WHERE BI_Estado_de_linea_de_venta__c = \'Pendiente\' AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true AND BI_Contingencia__c = false AND BI_Solicitud__r.BI_Estado_de_ventas__c IN (\'Enviando\', \'Activación\', \'Incompleto\')';

		}else if(OPTION == 3){ //solicitudBOLineaRecambioCHI

			query = 'SELECT Id, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Recambio__c WHERE BI_Estado_de_linea_de_recambio__c = \'Entregado\' AND BI_Estado_de_courier__c = \'Entregado\'AND BI_Solicitud_descargada_BackOffice__c = false AND BI_Validada__c = true AND BI_Solicitud__r.BI_Tipo_solicitud__c IN (\'Normal\',\'Split billing\',\'Contingencia\')';

		}else if(OPTION == 4){//solicitudlogisticaLineaRecambioCHI
			
			query = 'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Recambio__c WHERE BI_Estado_de_linea_de_recambio__c = \'Pendiente\' AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true  AND BI_Contingencia__c = false AND BI_Solicitud__r.BI_Estado_de_recambios__c IN (\'Enviando\', \'Activación\', \'Incompleto\')';

		}else if(OPTION == 5){//solicitudBOLineaServicioCHI

			query = 'SELECT Id, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Servicio__c WHERE BI_Solicitud_descargada_BackOffice__c = false AND BI_Estado_de_linea_de_servicio__c = \'Pendiente\'AND BI_Validada__c = true ';

		}else if(OPTION == 6){//solicitudcontingenciaLineaVentaCHI

			query = 'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Venta__c WHERE BI_Estado_de_linea_de_venta__c = \'Pendiente\'AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true AND BI_Contingencia__c = true ';
			
		}else if(OPTION == 7){//solicitudcontingenciaLineaRecambioCHI

			query = 'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Recambio__c WHERE BI_Estado_de_linea_de_recambio__c = \'Pendiente\' AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true AND BI_Contingencia__c = true AND BI_Solicitud__r.BI_Estado_de_recambios__c IN (\'Enviando\', \'Activación\', \'Incompleto\')';
			
		}

		System.debug('Batch: BI_BatchDescargaLVLR --> query: ' + query);

		System.debug('TOTAL_REGISTROS_KO 0 = ' + TOTAL_REGISTROS_KO);
		System.debug('TOTAL_REGISTROS_OK 0 = ' + TOTAL_REGISTROS_OK);
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		TOTAL_REGISTROS_OK = 0;
		TOTAL_REGISTROS_KO = 0;

		System.debug('TOTAL_REGISTROS_KO 1 = ' + TOTAL_REGISTROS_KO);
		System.debug('TOTAL_REGISTROS_OK 1 = ' + TOTAL_REGISTROS_OK);

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		Integer scope_size = scope.size();

   		try{ 
	   		System.debug('Batch: BI_BatchDescargaLVLR --> scope: ' + scope);

	   		if(OPTION == 1){//solicitudBOLineaVentaCHI
					List<BI_Linea_de_Venta__c> lst_venta = scope;	
	            for(BI_Linea_de_Venta__c ven:lst_venta){                   
	                ven.BI_Usuario_BackOffice__c = USER_ID;
	                ven.BI_Solicitud_descargada_BackOffice__c = true;
	            }
	            update lst_venta;

			}else if(OPTION == 2){//solicitudlogisticaLineaVentaCHI
	            List<BI_Linea_de_Venta__c> lst_venta = scope;	
	            for(BI_Linea_de_Venta__c ven:lst_venta){                   
	                ven.BI_Usuario_Logistica__c = USER_ID;
	                ven.BI_Solicitud_descargada_Logistica__c = true;
	            }
	            update lst_venta;

			}else if(OPTION == 3){ //solicitudBOLineaRecambioCHI
				List<BI_Linea_de_Recambio__c> lst_rec = scope;
	            for(BI_Linea_de_Recambio__c rec:lst_rec){                   
	                rec.BI_Usuario_BackOffice__c = USER_ID;
	                rec.BI_Solicitud_descargada_BackOffice__c = true;
	            }
	            update lst_rec;
				
			}else if(OPTION == 4){//solicitudlogisticaLineaRecambioCHI
		        List<BI_Linea_de_Recambio__c> lst_rec = scope;
		        for(BI_Linea_de_Recambio__c rec:lst_rec){                   
	                rec.BI_Usuario_Logistica__c = USER_ID;
	                rec.BI_Solicitud_descargada_Logistica__c = true;
	            }
	            update lst_rec;

			}else if(OPTION == 5){//solicitudBOLineaServicioCHI
				List<BI_Linea_de_Servicio__c> lst_serv = scope;	
	            for(BI_Linea_de_Servicio__c serv:lst_serv){                   
	                serv.BI_Usuario_BackOffice__c = USER_ID;
	                serv.BI_Solicitud_descargada_BackOffice__c = true;
	            }
	            update lst_serv;


			}else if(OPTION == 6){//solicitudcontingenciaLineaVentaCHI
				List<BI_Linea_de_Venta__c> lst_venta = scope;	
	            for(BI_Linea_de_Venta__c ven:lst_venta){                   
	                ven.BI_Usuario_Logistica__c = USER_ID;
	                ven.BI_Solicitud_descargada_Logistica__c = true;
	            }
	            update lst_venta;
				
			}else if(OPTION == 7){//solicitudcontingenciaLineaRecambioCHI
				List<BI_Linea_de_Recambio__c> lst_rec = scope;
	            for(BI_Linea_de_Recambio__c rec:lst_rec){                   
	                rec.BI_Usuario_Logistica__c = USER_ID;
	                rec.BI_Solicitud_descargada_Logistica__c = true;
	            }
	            update lst_rec;
				
			}

			TOTAL_REGISTROS_OK += scope_size;
		
		}catch(Exception exc){

			TOTAL_REGISTROS_KO += scope_size;

			LST_LOGIDS.add(BI_LogHelper.generate_BILog('BI_BatchDescargaLVLR.execute()', 'BI_EN',exc,  'Apex Batch'));
			throw exc;
		}

		System.debug('TOTAL_REGISTROS_KO 2 = ' + TOTAL_REGISTROS_KO);
		System.debug('TOTAL_REGISTROS_OK 2 = ' + TOTAL_REGISTROS_OK);

		
	
	}
	
	global void finish(Database.BatchableContext BC) {

		System.debug('TOTAL_REGISTROS_KO 3 = ' + TOTAL_REGISTROS_KO);
		System.debug('TOTAL_REGISTROS_OK 3 = ' + TOTAL_REGISTROS_OK);

		String report_name;
		String report_API;
		
   		if(OPTION == 1){//solicitudBOLineaVentaCHI
			report_name = 'Líneas de venta de BackOffice';
			report_API = 'BI_Planilla_Back_Ventas';

		}else if(OPTION == 2){//solicitudlogisticaLineaVentaCHI
            report_name = 'Líneas de venta de Logística';
            report_API = 'BI_Planilla_logistica';

		}else if(OPTION == 3){ //solicitudBOLineaRecambioCHI
			report_name = 'Líneas de recambio de BackOffice';
			report_API = 'BI_Plantilla_back_recambios'; 
			
		}else if(OPTION == 4){//solicitudlogisticaLineaRecambioCHI
	        report_name = 'Líneas de recambio de Logísitca';
	        report_API = 'BI_Planilla_logistica_recambios';

		}else if(OPTION == 5){//solicitudBOLineaServicioCHI
			report_name = 'Líneas de servicio de BackOffice';
			report_API = 'BI_Planilla_Back_servicios';


		}else if(OPTION == 6){//solicitudcontingenciaLineaVentaCHI
			report_name = 'Líneas de venta de Contingencia';
			report_API = 'BI_Planilla_contingencia_ventas';
			
		}else if(OPTION == 7){//solicitudcontingenciaLineaRecambioCHI
			report_name = 'Lineas de recambio de Contingencia';
			report_API = 'BI_Planilla_contingencia_recambios';
			
		}

		List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name,HtmlValue, Subject FROM EmailTemplate WHERE DeveloperName = 'BI_DescargaAsync_Agendamientos_HTML' limit 1];

		List<User> lst_usr = [SELECT Id, Name FROM User WHERE Id  = :USER_ID limit 1];

		AsyncApexJob apex_job = [Select TotalJobItems, Status, NumberOfErrors, JobType, JobItemsProcessed, ExtendedStatus, CreatedById, CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id

        if(!lst_tmpl.isEmpty() && !lst_usr.isEmpty()){

        	String status;
			String subject;

			String final_URL = getReportURL(report_API, lst_usr[0].Name);
			String htmlBody;

    		htmlBody = lst_tmpl[0].HtmlValue;

    		subject = lst_tmpl[0].Subject;
    		subject = subject.replaceAll('REPORT_NAME', report_name);

    		htmlBody = htmlBody.replaceAll('<!\\[CDATA\\[', ''); // replace '<![CDATA['
			htmlBody = htmlBody.replaceAll('\\]\\]>', ''); // replace ']]'

			system.debug(htmlBody);
			system.debug('final_URL: ' + final_URL);
			system.debug('report_name: ' + report_name);
    		
    		htmlBody = htmlBody.replaceAll('REPORT_NAME', report_name);
    		system.debug(htmlBody);


    		if(apex_job.NumberOfErrors == 0 && LST_LOGIDS.isEmpty() && final_URL != null){

    			htmlBody = htmlBody.replaceAll('REPORT_URL', final_URL);

				status = 'El proceso '+ BC.getJobId() +' ha recorrido '+ apex_job.TotalJobItems +' lotes (' + String.valueOf(Integer.valueOf(TOTAL_REGISTROS_KO + TOTAL_REGISTROS_OK)) +' líneas) con ';
				status += apex_job.NumberOfErrors+' lotes erróneos (' + TOTAL_REGISTROS_KO +' líneas)'+'. Se han procesado correctamente '+apex_job.JobItemsProcessed + ' lotes (' + TOTAL_REGISTROS_OK +' líneas).';
        		
        		system.debug(status);
        		htmlBody = htmlBody.replaceAll('JOB_STATUS', status);

        	}else{
        		subject = 'Se ha producido algún error en la generación del informe \"' + report_name + '\"';

				status = 'Se ha producido un error en el proceso '+ BC.getJobId() +' ha recorrido '+ apex_job.TotalJobItems +' lotes (' + String.valueOf(Integer.valueOf(TOTAL_REGISTROS_KO + TOTAL_REGISTROS_OK));
				status += ' líneas) con '+apex_job.NumberOfErrors+' lotes erróneos (' + TOTAL_REGISTROS_KO +' líneas)'+'. Se han procesado correctamente '+apex_job.JobItemsProcessed + ' lotes (' + TOTAL_REGISTROS_OK +' líneas). \n\n Por favor contacte con su administrador y proporcionele el siguiente mensaje: ';

				String error_description = 'Se ha producido un error en la clase BI_BatchDescargaLVLR:  lst_tmpl.isEmpty()= '+ lst_tmpl.isEmpty() +' - lst_usr.isEmpty() '+ lst_usr.isEmpty() +' - NumberOfErrors = ' + apex_job.NumberOfErrors + '.\n';
        			error_description += 'TotalJobItems  = '+ apex_job.TotalJobItems +' - Status= '+ apex_job.Status +' - NumberOfErrors= '+ apex_job.NumberOfErrors +' - JobType= '+ apex_job.JobType+' - JobItemsProcessed= '+apex_job.JobItemsProcessed +' - ExtendedStatus= '+apex_job.ExtendedStatus +' - CreatedById= '+apex_job.CreatedById +' - CompletedDate= ' + apex_job.CompletedDate + '.\n';
        			error_description += 'OPTION= '+ OPTION + 'USER_ID= '+ USER_ID + '\n\n';
        			error_description += 'final_URL= '+ final_URL + '\n\n';

        			error_description += 'BI_MAXLIMIT_AGENDAMIENTOS_BUTTONS= '+ BI_MAXLIMIT_AGENDAMIENTOS_BUTTONS + '\n\n';
        			error_description += 'BI_MAXLIMIT_BATCHDESCARGALVLR= '+ BI_MAXLIMIT_BATCHDESCARGALVLR + '\n\n';

        			error_description += 'Log Ids = ['; 

        			for(Id log_id :LST_LOGIDS){
        				error_description += String.valueOf(log_id) + ',';
        			}

        			error_description = error_description.substring(0, error_description.length() -1) + ']' + '\n\n';

				htmlBody = status + error_description;

        		BI_LogHelper.generateLog('BI_BatchDescargaLVLR.final()', 'BI_EN', error_description, 'Apex Batch');
        	}


	        
			Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

			    mailOwner.setTargetObjectId(lst_usr[0].Id);
				mailOwner.setHtmlBody(htmlBody);
				mailOwner.setSubject(subject);

			    mailOwner.setSenderDisplayName('Telefónica BI_EN');
			    mailOwner.setSaveAsActivity(false);
			if(!Test.isRunningTest()){   
				Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailOwner});
			}
		}else{
			BI_LogHelper.generateLog('BI_BatchDescargaLVLR.final()', 'BI_EN', 'No se encuentra la plantilla BI_DescargaAsync_Agendamientos_HTML', 'Apex Batch');
		}
	}


	private static String getReportURL(String report_API, String parameter_pv0){

		List <Report> lst_report = [SELECT Id, DeveloperName FROM Report where DeveloperName = :report_API limit 1];

		if(!lst_report.isEmpty() && report_API != null){
			
			String ret_URL =  URL.getSalesforceBaseUrl().toExternalForm() + '/' + lst_report[0].Id;

			if(parameter_pv0 != null){
				ret_URL += '?pv0='+ parameter_pv0;
			}

			return ret_URL;

		}else{
			return null;
		}

	}
	
}