/* @Sumary: Esta clase permite la desagrupacion de facturas.
*	Las programaciones se agrupan por periodos para poder asi crear una solo programacion
*	Al agrupar, los Pediudos PE se clonan en la nueva programacion, si esto son iguales
*	por programacion-Periodo los pedidos no se sumarizan.
*	Cuando se desagrupa la programacion la programacion padre se elimina quedando solo las hijo.
*	@Developed by: juliom.ortega@gmail.com		
*
*
*/

public  class ADQ_DesagruparFacturasControllerMod1 {

	public Factura__c factura{get; set;}
	public Registro_de_agrupacion__c regagrupa {get;set;}
	
	public List<Registro_de_agrupacion__c> lsregistro = new List<Registro_de_agrupacion__c>();
	public List<Programacion__c> listaProgramaciones;
	
	public Set<Id> setSelecciona = new Set<Id>();
	public Set<Id> setProgramaciones = new Set<Id>();
	public List<Programacion__c> lstProgramacionesDelete = new List<Programacion__c>();
	public List<Registro_de_agrupacion__c> lsRegistroDelte = new List<Registro_de_agrupacion__c>(); 
	public List<Pedido_PE__c> lsDeletePE = new List<Pedido_PE__c>();
	public List<Pedido__c> lspedidodelete = new List<Pedido__c>();
	
	public Map<id,Registro_de_agrupacion__c> mapaRegistro = new Map<id,Registro_de_agrupacion__c>();
	public List<WrapperProgramacion> listaWrapperProgramaciones =  new List<WrapperProgramacion>();
		
	public Boolean success {get; set;}	
	public String idFactura;
	
	/***Para OnDemand**/			
	public Factura__c encabezado;
	/***Para OnDemand**/
	
	// creamos el constructor
	public ADQ_DesagruparFacturasControllerMod1 (){
		// obtenemos el Id del Encabezado de programacion.
		idFactura = Apexpages.currentPage().getParameters().get('Fid');
		System.debug(' >>>> idFactura  '+ idFactura);
		if (idFactura !=null){		
			
			/***Para OnDemand**/			
			encabezado = [Select Id, RecordTypeId From Factura__c where Id =: idFactura limit 1];
			/***Para OnDemand**/

			//Recupero las programaciones pedndientes de factura seleccionada
			listaProgramaciones = [SELECT Id, Name, CurrencyIsoCode, Fecha_de_inicio_de_cobro__c, 
										Inicio_del_per_odo__c, Fin_del_per_odo__c, Importe__c, IVA__c, Total__c
									FROM Programacion__c
									WHERE Factura__c =: idFactura AND Facturado__c = false 
										AND Exportado__c = false AND Numero_de_factura_SD__c = ''
										AND Bloqueo_por_agrupaci_n__c = false AND Bloqueo_por_facturaci_n__c = false 
										AND Agrupaci_n__c = true
									ORDER BY Inicio_del_per_odo__c ASC];
									
		//buscamos en el objeto de registro de agrupacion todas las facturas correspondientes a encabezado actual.							
			lsregistro = [SELECT activo__c,CreatedDate,
							Encabezado_de_facturaci_n__c,Id,
							Name,Programaci_n_Padre__c,Programaci_n_Precedente__c
							FROM Registro_de_agrupacion__c
							WHERE Encabezado_de_facturaci_n__c =: idFactura 
							];
							
			System.debug('>>> lsregistro  '+ lsregistro); 
									
			
			System.debug(listaProgramaciones);			
					
		}else {
			regagrupa=null;
		}
		
	}
	
	
	public void desagrupar (){
		this.matriz();
			
	}
	
	public void matriz (){
		Integer conta1=0;
		//this.consulta();
		System.debug('>>>> listaWrapperProgramaciones ' +listaWrapperProgramaciones );
		if (listaWrapperProgramaciones.size() > 0 || listaWrapperProgramaciones != null) {
			for (WrapperProgramacion wr : listaWrapperProgramaciones){
				if (wr.selected == true){
					conta1 ++;
				// obtenemos los Id solo de las programaciones seleccionadas
					setSelecciona.add(wr.programacion.id);	
					wr.programacion.Borrar_por_agrupacion__c = true;
					update 	wr.programacion;
					System.debug('>>>wr.programacion ' + wr.programacion);
					lstProgramacionesDelete.add(wr.programacion);
					
				}
			}
			System.debug(' >>conta1  ' + conta1 + '   == lstProgramacionesDelete: ' + lstProgramacionesDelete);
			if (conta1>0){
				this.consultaSelecciona();
				delete lsRegistroDelte;
				delete lsDeletePE;
				delete lspedidodelete;
				delete lstProgramacionesDelete;	
				success = true;
				/***Para OnDemand**/			
/*>>>				if(encabezado.RecordTypeId == FACTURA_ON_DEMAND){
					GestorConsumos gestorConsumos = new GestorConsumos();
					List<Factura__c> lf = new List<Factura__c>();
					lf.add(encabezado);
					gestorConsumos.setListaEncabezados(lf);
					gestorConsumos.asignaConsumosConFacturas();
				}
				/***Para OnDemand**/
			}else{
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.FATAL, 'No se han seleccionado facturas para desagrupar'));
				success=false;
			}
		}else {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.FATAL, 'No se han seleccionado facturas para desagrupar'));
				success=false;
		}
		
	} 
	
	public void consultaSelecciona (){
		if(setSelecciona != null || setSelecciona.size()>0){
			for (Registro_de_agrupacion__c reg1 : [SELECT Encabezado_de_facturaci_n__c,Id,
													Name,Programaci_n_Padre__c,Programaci_n_Precedente__c
													FROM Registro_de_agrupacion__c
													WHERE Programaci_n_Padre__c IN: setSelecciona 
													]){
				setProgramaciones.add(reg1.Programaci_n_Precedente__c);
				lsRegistroDelte.add(reg1);										
				System.debug('>>> reg1  '+ reg1);										
									
			}
			this.cosultaProgramaciones();
			this.eliminaPE();
			this.DelPedido();
		}	
		
	}
	// selecciona las programaciones que se agruparon de acuerdo a la objeto de registro de agrupacion
	public void  cosultaProgramaciones (){
		Integer numerMeses; 
		if(setProgramaciones !=null || setProgramaciones.size() > 0){
			for (Programacion__c prog : [SELECT Id, Name, CurrencyIsoCode, Fecha_de_inicio_de_cobro__c, 
											Inicio_del_per_odo__c, Fin_del_per_odo__c, Importe__c, IVA__c, Total__c
										FROM Programacion__c
										WHERE Id IN:setProgramaciones 
										ORDER BY Inicio_del_per_odo__c ASC
										]){
		/* ================= se valida si el numero de meses entre la fecha de inicio y fin de periodo es mayor a
		*				1 mes significa que hay m?s agrupaciones en esa programacion..
		*/ 											
			 
			 numerMeses = prog.Inicio_del_per_odo__c.monthsBetween(prog.Fin_del_per_odo__c)+1;
			 System.debug('>> numerMeses ' + numerMeses );
			 System.debug('>> numerMeses ' + numerMeses );
			 System.debug('>> prog' + prog);						
			 
			 prog.Bloqueo_por_agrupaci_n__c = false;
			 prog.Bloqueo_por_facturaci_n__c = false;
			 prog.Motivo_de_bloqueo__c = null;
			 if  (numerMeses > 1){
				prog.Agrupaci_n__c = true;
			 }						
			 update (prog);		
			}
		}	
	}
	public void eliminaPE (){
		for (Pedido_PE__c pedPE  : [Select id , Programacion__c 
									from Pedido_PE__c 
									where Programacion__c IN: setSelecciona]){
			lsDeletePE.add(pedPE);	
								
		}
		System.debug('>>lsDeletePE ' + lsDeletePE);
		
		
	}
	
	public void DelPedido (){
		for  (Pedido__c pe :[Select Id, Programacion__c 
							from Pedido__c
							where Programacion__c IN : setSelecciona]){
			lspedidodelete.add(pe);					
		}
		System.debug('>>lspedidodelete ' + lspedidodelete);
	}
	
	
	public void consulta(){
		for (Registro_de_agrupacion__c reg : lsregistro){
			mapaRegistro.put(reg.Programaci_n_Padre__c, reg);
		}
		System.debug('mapaRegistro>>> ' + mapaRegistro);
	}
	
	
	public Pagereference cancelar() {
		return new Apexpages.Pagereference('/'+idFactura);
		
	}
	
	// mi wrapper para indentificar que registros se ha agrupado
	
	public class WrapperProgramacion{
		public Programacion__c programacion{get;set;}
		public Boolean selected{get;set;}
		
		public WrapperProgramacion(Programacion__c programacion, Boolean selected){
			this.programacion = programacion;
			this.selected = selected;
		}
	}
	// valido mi wrapper le meto datos...
	public List<WrapperProgramacion> getListaWrapperProgramaciones(){
		if(listaWrapperProgramaciones == null || listaWrapperProgramaciones.size()==0){
			System.debug('>>>>> listaProgramaciones  '+listaProgramaciones);
			if(listaProgramaciones != null && listaProgramaciones.size() > 0){
				listaWrapperProgramaciones = new List<WrapperProgramacion>();
				for(Programacion__c p : listaProgramaciones){
					listaWrapperProgramaciones.add(new WrapperProgramacion(p, false));
				}
			}
		}
		return listaWrapperProgramaciones;
	}
	public void cubre (){
		integer i=0;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		i++;
		
	}
/*
	public static testmethod void testar(){
		DesagruparFacturasControllerMod1 clsvalida = new DesagruparFacturasControllerMod1 ();
		
			Apexpages.currentPage().getParameters().put('fid', 'a0W70000000vjCb');
			//Apexpages.currentPage().getParameters().put('fid', 'a0W70000000vjCb');
			clsvalida.getListaWrapperProgramaciones();
			clsvalida.desagrupar();
			clsvalida.matriz();
			clsvalida.consulta();
			clsvalida.consultaSelecciona();
			clsvalida.cosultaProgramaciones();	
			clsvalida.eliminaPE();
			clsvalida.DelPedido();
			clsvalida.cancelar();
			
			Factura__c encabezado = new Factura__c();
			encabezado.Activo__c = true;
			encabezado.Fecha_Fin__c = System.today().addMonths(12);
			encabezado.Fecha_Inicio__c = System.today();
			insert encabezado;
			
			Programacion__c progra = new Programacion__c ();
			progra.Fecha__c = System.today();
			progra.Inicio_del_per_odo__c = System.today();
			progra.Fin_del_per_odo__c = System.today().addYears(1);
			progra.Factura__c =encabezado.id;
			insert progra;
			
			Boolean selected = true;
			
			WrapperProgramacion wrapper1 = new WrapperProgramacion(progra,selected);
			clsvalida.cobertura();
	} 
	*/
}