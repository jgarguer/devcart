public class BI_NEPromotionMethods {
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Campaign Triggers 
    Test Class:    BI_NE_PromotionMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    05/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
           
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
    
    History: 	   Method that checks if NE__Promotion__c.Not_available_for_campaigns__c has changed to false and changes the related Campaign isActive to false
    
    <Date>                  <Author>                <Change Description>
    05/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void notAvailable (List<NE__Promotion__c> news, List<NE__Promotion__c> olds){
		try{
			Integer i = 0;
					
			Set <Id> setp = new Set <Id>();
			
			for (NE__Promotion__c promo:news){
				if (promo.Not_available_for_campaigns__c == true && olds[i].Not_available_for_campaigns__c == false){
					setp.add(promo.Id);
				}
				i++;
			}
			if (!setp.isempty()){
				Campaign [] camp = [SELECT Id, NE__Promotion__c FROM Campaign WHERE NE__Promotion__c in :setp];
				for (Campaign cmp:camp){
					cmp.IsActive = false;
				}
				update camp;
			}
		}catch (exception Exc){
			BI_LogHelper.generate_BILog('BI_NEPromotionMethods.notAvailable', 'BI_EN', Exc, 'Trigger');
		}
		
	}
}