/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Address_Management

History:
<Date>							<Author>						<Change Description>
12/06/2017						Eduardo Ventura					Versión inicial
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CHI_Address_Management_Test {
    
    @isTest private static void getNormalizedAddress() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba';
        insert config_integracion;
        
        System.Test.startTest();
        
        try{
            string resultExpected = '{"Status":"Validated","Reason":"0","matchingAddresses":{"validAddresses":[{"region":"string","postalCode":"string","normalized":"string","municipality":"string","locality":"string","isDangerous":false,"floor":{"value":"0","range":null},"country":"string","coordinates":{"longitude":"0","latitude":"0"},"area":"string","apartment":{"value":"10","range":null},"addressType":"string","addressNumber":{"value":"0","range":null},"addressName":"String","addressExtension":"string",'+
                '"additionalData":[{"value":"string","key":"additionalComments"},{"value":"string","key":"betweenStreet1"},{"value":"string","key":"betweenStreet2"},{"value":"0","key":"block"},{"value":"string","key":"housingComplexType"},{"value":"string","key":"housingComplexName"},{"value":"0","key":"boxNr"},{"value":"Y","key":"isRedZone"},{"value":"Y","key":"isBlacklisted"}]}],"totalResults":1},"doubtAddresses":{"validAddresses":[{"region":"string","postalCode":"string","normalized":"string","municipality"'+
                ':"string","locality":"string","isDangerous":false,"floor":{"value":"0","range":null},"country":"string","coordinates":{"longitude":"0","latitude":"0"},"area":"string","apartment":{"value":"10","range":null},"addressType":"string","addressNumber":{"value":"0","range":null},"addressName":"String","addressExtension":"string","additionalData":[{"value":"string","key":"additionalComments"},{"value":"string","key":"betweenStreet1"},{"value":"string","key":"betweenStreet2"},{"value":"0","key":"block"},'+
                '{"value":"string","key":"housingComplexType"},{"value":"string","key":"housingComplexName"},{"value":"0","key":"boxNr"},{"value":"Y","key":"isRedZone"},{"value":"Y","key":"isBlacklisted"}]}],"totalResults":1}}';
  			System.assertEquals(resultExpected.length(), JSON.serialize(FS_CHI_Address_Management.getValidAddresses('addressName', 'addressNumber', '0', 'apartment', 'area', 'locality', 'municipality', 'region', 'country', '0', 'bloque', 'tipoComplejo', 'nombreComplejo', '0')).length());
            //System.assertEquals(JSON.serialize((BI_RestWrapper.ValidateAddressResponseType) JSON.deserialize('{"status":"0","reason":"0","matchingAddresses":{"validAddresses":[{"addressType":"string","normalized":"string","addressName":"String","addressNumber":{"value":0},"floor":{"value":0},"apartment":{"value":10},"area":"string","locality":"string","municipality":"string","region":"string","country":"string","postalCode":"string","addressExtension":"string","coordinates":{"longitude":"0","latitude":"0"},"isDangerous":false,"additionalData":[{"key":"additionalComments","value":"string"},{"key":"betweenStreet1","value":"string"},{"key":"betweenStreet2","value":"string"},{"key":"block","value":"0"},{"key":"housingComplexType","value":"string"},{"key":"housingComplexName","value":"string"},{"key":"boxNr","value":"0"},{"key":"isRedZone","value":"Y"},{"key":"isBlacklisted","value":"Y"}]}],"totalResults":1},"doubtAddresses":{"validAddresses":[{"addressType":"string","normalized":"string","addressName":"String","addressNumber":{"value":0},"floor":{"value":0},"apartment":{"value":10},"area":"string","locality":"string","municipality":"string","region":"string","country":"string","postalCode":"string","addressExtension":"string","coordinates":{"longitude":"0","latitude":"0"},"isDangerous":false,"additionalData":[{"key":"additionalComments","value":"string"},{"key":"betweenStreet1","value":"string"},{"key":"betweenStreet2","value":"string"},{"key":"block","value":"0"},{"key":"housingComplexType","value":"string"},{"key":"housingComplexName","value":"string"},{"key":"boxNr","value":"0"},{"key":"isRedZone","value":"Y"},{"key":"isBlacklisted","value":"Y"}]}],"totalResults":1}}', BI_RestWrapper.ValidateAddressResponseType.class)).length(), JSON.serialize(FS_CHI_Address_Management.getValidAddresses('addressName', 'addressNumber', '0', 'apartment', 'area', 'locality', 'municipality', 'region', 'country', '0', 'bloque', 'tipoComplejo', 'nombreComplejo', '0')).length());
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
}