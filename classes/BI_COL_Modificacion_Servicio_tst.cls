/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-23      Daniel ALexander Lopez (DL)     Create Class
                    24/02/2016      Antonio Masferrer García       Restructure code and add test methods
                    06/08/2016      Humberto Nunes                  Correccion por Too Many Querys... se omitio BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert (17 llamadas de 2 querys c/u)
                    10/02/2017      Pedro Párraga                  Increase coverage
		    13/03/2017      Marta Gonzalez(Everis)	    REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
                    18/08/2017      Guillermo Muñoz                 Changed to use new BI_MigrationHelper functionality
                    20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_Modificacion_Servicio_tst{

  Public static list <UserRole> lstRoles;
  Public static User objUsuario;
  
  public static list<Opportunity> lstOppAsociadas = new list<Opportunity>();
  public static Opportunity objOppty;

  public static List<BI_COL_BtnGenerateMSDown_ctr.WrapperMS> lstWrapper = new List<BI_COL_BtnGenerateMSDown_ctr.WrapperMS>();

  public static Account objCuenta;

  public static BI_COL_Anexos__c objAnexos;

  public static List<BI_COL_Modificacion_de_Servicio__c> lstModSer = new List<BI_COL_Modificacion_de_Servicio__c>();
  public static List<BI_COL_Modificacion_de_Servicio__c> lstModSerOLD = new List<BI_COL_Modificacion_de_Servicio__c>();
  public static BI_COL_Modificacion_de_Servicio__c objModSer;
  public static BI_COL_Modificacion_de_Servicio__c objMS;
  public static BI_COL_Modificacion_de_Servicio__c objMS1;

  public static List<BI_COL_Descripcion_de_servicio__c> lstDesSer;
  public static BI_COL_Descripcion_de_servicio__c objDesSer;

  public static List<RecordType> rtBI_FUN;

  public static BI_Log__c objBiLog;
  public static BI_Log__c objBiLog2;
  public static Contact objContacto;
  public static BI_Col_Ciudades__c objCiudad;
  public static BI_Sede__c objSede;
  public static BI_Punto_de_instalacion__c objPuntosInsta;

  public static Set<Id> set_id_MS = new Set<Id>();
  public static Set<Id> set_id_Opp = new Set<Id>();

  static void loadInfo(){
    //Map<Integer,BI_bypass__c> aux = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),false,true,true,true);
    BI_MigrationHelper.skipAllTriggers();
    //Cuenta
    Account acc = new Account();
    acc.Name = 'prueba';
    acc.BI_Country__c = Label.BI_COL_lblColombia;
    acc.TGS_Region__c = 'América';
    acc.BI_Tipo_de_identificador_fiscal__c = 'NIT';
    acc.CurrencyIsoCode = 'GTQ';
    acc.BI_Segment__c = 'Empresas';
    acc.BI_Subsegment_Regional__c = 'Mayoristas';
    acc.BI_Subsector__c = 'Banca';
    acc.BI_Sector__c = 'Industria';
    acc.BI_No_Identificador_fiscal__c = '123456789';
    acc.BI_Territory__c                       = 'test';
    insert acc;

    //Oportunidad
    Opportunity opp = new Opportunity();
    opp.BI_COL_Importe__c = 0;
    opp.AccountId = acc.Id;
    opp.BI_Country__c = Label.BI_COL_lblColombia;
    opp.Name = 'XXX Test sumTotalModificationService';
    opp.CloseDate = Date.today();
    opp.StageName = Label.BI_F6Preoportunidad;
    opp.BI_Ciclo_ventas__c = Label.BI_Completo;
    opp.BI_Country__c = 'Colombia';

    insert opp;

    //Ciudad
    BI_Col_Ciudades__c ciudad = new BI_Col_Ciudades__c ();
    ciudad.Name = 'Test City';
    ciudad.BI_COL_Pais__c = 'Test Country';
    ciudad.BI_COL_Codigo_DANE__c = 'TestCDa';
    
    insert ciudad;

    //Contactos
    Contact cont = new Contact();
    cont.LastName = 'Test';
    cont.FirstName = 'Test23';
    cont.BI_Country__c = 'Colombia';
    cont.CurrencyIsoCode = 'COP'; 
    cont.AccountId = acc.Id;
    cont.BI_Tipo_de_contacto__c = 'Administrador Canal Online';
    //REING_INI  
    cont.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
    //REING_FIN
    cont.BI_Tipo_de_documento__c = 'DNI';
    cont.Email = 'test@test.com';
    cont.Phone = '91232343';
    cont.BI_Numero_de_documento__c = '12343';
    cont.MobilePhone = '1111111111'; 
    cont.BI_COL_Ciudad_Depto_contacto__c = ciudad.Id;
    cont.BI_COL_Direccion_oficina__c = 'Teststts';

    insert cont;

    //Sede
    BI_Sede__c sede = new BI_Sede__c();
    sede.BI_COL_Ciudad_Departamento__c = ciudad.Id;
    sede.BI_Direccion__c = 'Test Street 123 Number 321';
    sede.BI_Localidad__c = 'Test Local';
    sede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
    sede.BI_COL_Sucursal_en_uso__c = 'Libre';
    sede.BI_Country__c = 'Colombia';
    sede.Name = 'Test Street 123 Number 321, Test Local Colombia';
    sede.BI_Codigo_postal__c = '12356';

    insert sede;
    //BI_MigrationHelper.disableBypass(aux);
    BI_MigrationHelper.cleanSkippedTriggers();
    //Descripciones de servicio
    BI_COL_Descripcion_de_servicio__c des_ser = new BI_COL_Descripcion_de_servicio__c();
    des_ser.BI_COL_Oportunidad__c = opp.Id;
    des_ser.CurrencyIsoCode = 'COP';
    
    insert des_ser;

    //Producto
    NE__Product__c objProd = new NE__Product__c();
    objProd.Offer_SubFamily__c = 'a-test;';
    objProd.Offer_Family__c = 'b-test';
    objProd.BI_Country__c = 'COL';
            
    insert objProd;     

    NE__Catalog__c objCatalog = new NE__Catalog__c();
    objCatalog.Name = 'EMPRESAS P';
    objCatalog.NE__Active__c = True;
    insert objCatalog;

    NE__Catalog_Item__c objCatalogItem = new NE__Catalog_Item__c();
    objCatalogItem.NE__Catalog_Id__c = objCatalog.Id;
    objCatalogItem.NE__ProductId__c = objProd.id;
    objCatalogItem.Country__c='Colombia';
    objCatalogItem.NE__Type__c = 'Product';
    insert objCatalogItem;




     //11/01/2017 START GSPM
    //Order
    NE__Order__c objOrder =  new NE__Order__c(); 
	objOrder.NE__OrderStatus__c =  'Active';
    objOrder.NE__CatalogId__c =  objCatalog.id; 
        insert objOrder;
      
    //Order Item
    NE__OrderItem__c objOI1  = new NE__OrderItem__c ();
    objOI1.NE__OrderId__c = objOrder.id;      
    objOI1.NE__ProdId__c = objProd.id;      
    objOI1.NE__Qty__c = 1;
    objOI1.NE__CatalogItem__c = objCatalogItem.id;
    insert objOI1;

    NE__OrderItem__c objOI2  = new NE__OrderItem__c ();
    objOI2.NE__OrderId__c = objOrder.id;      
    objOI2.NE__ProdId__c = objProd.id; 
    objOI2.NE__Qty__c = 5;
    objOI1.NE__CatalogItem__c = objCatalogItem.id;
    insert objOI2;

    NE__OrderItem__c objOI3  = new NE__OrderItem__c ();
    objOI3.NE__OrderId__c = objOrder.id;      
    objOI3.NE__ProdId__c = objProd.id;      
    objOI3.NE__Qty__c = 3;
    objOI1.NE__CatalogItem__c = objCatalogItem.id;
    insert objOI3;

    //17/01/2017 START ANCR
    //Order Item Attribute
    NE__Order_Item_Attribute__c objOIA = new NE__Order_Item_Attribute__c();
    objOIA.Name = 'Medio';
    objOIA.NE__Order_Item__c = objOI1.Id;
    objOIA.NE__FamPropExtId__c = 'Conectividad:Medio';
    objOIA.NE__Value__c = 'Cobre';
    insert objOIA;

    // Dynamic Property Definition
     NE__DynamicPropertyDefinition__c objDynamic = new NE__DynamicPropertyDefinition__c();
     objDynamic.Name = 'Medio';
     insert objDynamic;

     List<NE__PropertyDomain__c> lst_prope_domain = new List<NE__PropertyDomain__c>();
     // Property Domain
     NE__PropertyDomain__c objPropertydomain = new NE__PropertyDomain__c();
     objPropertydomain.Name = 'Cobre';
     objPropertydomain.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain);

     NE__PropertyDomain__c objPropertydomain1 = new NE__PropertyDomain__c();
     objPropertydomain1.Name = 'Fibra';
     objPropertydomain1.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain1);

     NE__PropertyDomain__c objPropertydomain2 = new NE__PropertyDomain__c();
     objPropertydomain2.Name = 'Radio';
     objPropertydomain2.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain2);

     NE__PropertyDomain__c objPropertydomain3 = new NE__PropertyDomain__c();
     objPropertydomain3.Name = 'SCPC';
     objPropertydomain3.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain3);

     NE__PropertyDomain__c objPropertydomain4 = new NE__PropertyDomain__c();
     objPropertydomain4.Name = 'SIN PREFERENCIA';
     objPropertydomain4.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain4);

     NE__PropertyDomain__c objPropertydomain5 = new NE__PropertyDomain__c();
     objPropertydomain5.Name = 'Tercero';
     objPropertydomain5.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain5);

     NE__PropertyDomain__c objPropertydomain6 = new NE__PropertyDomain__c();
     objPropertydomain6.Name = 'VSAT';
     objPropertydomain6.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain6);

     NE__PropertyDomain__c objPropertydomain7 = new NE__PropertyDomain__c();
     objPropertydomain7.Name = 'Wimax';
     objPropertydomain7.NE__PropId__c = objDynamic.id;
     lst_prope_domain.add(objPropertydomain7);


     insert lst_prope_domain;


    //Modificaciones de servicio
    List<BI_COL_Modificacion_de_Servicio__c> lst_ms = new List<BI_COL_Modificacion_de_Servicio__c>();

    BI_COL_Modificacion_de_Servicio__c ms1 = new BI_COL_Modificacion_de_Servicio__c();
    ms1.BI_COL_Oportunidad__c = opp.Id;
    ms1.BI_COL_Producto__c = objOI1.Id;
    ms1.BI_COL_Cargo_conexion__c = 5;
    ms1.BI_COL_Cargo_fijo_mes__c = 3;
    ms1.BI_COL_Cobro_mensual_adicionales__c = 2;
    ms1.BI_COL_Duracion_meses__c = 2;
    ms1.BI_COL_Cobro_unico_adicionales__c = 4;
    ms1.BI_COL_Clasificacion_Servicio__c = 'ALTA';
    ms1.BI_COL_Codigo_unico_servicio__c = des_ser.Id;
    ms1.BI_COL_Estado__c = 'Activa';
    ms1.BI_COL_Medio_Preferido__c = 'Cobre';
    ms1.BI_COL_Medio_Vendido__c = 'Fibra';
      //17/01/2017 end ANCR
    lst_ms.add(ms1);

    BI_COL_Modificacion_de_Servicio__c ms2 = new BI_COL_Modificacion_de_Servicio__c();
    ms2.BI_COL_Oportunidad__c = opp.Id;
    ms2.BI_COL_Producto__c = objOI2.Id;
    ms2.BI_COL_Cargo_conexion__c = 5;
    ms2.BI_COL_Cargo_fijo_mes__c = 3;
    ms2.BI_COL_Cobro_mensual_adicionales__c = 2;
    ms2.BI_COL_Duracion_meses__c = 2;
    ms2.BI_COL_Cobro_unico_adicionales__c = 4;
    ms2.BI_COL_Clasificacion_Servicio__c = 'ALTA';
    ms2.BI_COL_Codigo_unico_servicio__c = des_ser.Id;
    ms2.BI_COL_Estado__c = 'Activa';
    ms2.BI_COL_Medio_Preferido__c = 'Cobre';
    ms2.BI_COL_Medio_Vendido__c = 'Fibra';

    lst_ms.add(ms2);

    BI_COL_Modificacion_de_Servicio__c ms3 = new BI_COL_Modificacion_de_Servicio__c();
    ms3.BI_COL_Oportunidad__c = opp.Id;
    ms3.BI_COL_Producto__c = objOI3.Id;
    ms3.BI_COL_Cargo_conexion__c = 5;
    ms3.BI_COL_Cargo_fijo_mes__c = 3;
    ms3.BI_COL_Cobro_mensual_adicionales__c = 2;
    ms3.BI_COL_Duracion_meses__c = 2;
    ms3.BI_COL_Cobro_unico_adicionales__c = 4;
    ms3.BI_COL_Clasificacion_Servicio__c = 'ALTA';
    ms3.BI_COL_Codigo_unico_servicio__c = des_ser.Id;
    ms3.BI_COL_Estado__c = Label.BI_COL_EstadoMS_inactiva; 
    ms3.BI_COL_Medio_Preferido__c = 'Cobre';
    ms3.BI_COL_Medio_Vendido__c = 'Fibra';
    lst_ms.add(ms3);

    insert lst_ms;
    //11/01/2017 END GSPM
    
    for(BI_COL_Modificacion_de_Servicio__c ms : lst_ms){
      set_id_MS.add(ms.Id);
      set_id_Opp.add(ms.BI_COL_Oportunidad__c);
    }
  }


  static void crearData(){
    //Map<Integer,BI_bypass__c> aux = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),false,true,true,true);
    BI_MigrationHelper.skipAllTriggers();
    objCuenta = new Account();
    objCuenta.Name = 'prueba';
    objCuenta.BI_Country__c = 'Colombia';
    objCuenta.TGS_Region__c = 'América';
    objCuenta.BI_Tipo_de_identificador_fiscal__c = 'NIT';
    objCuenta.CurrencyIsoCode = 'GTQ';
    objCuenta.BI_Segment__c = 'Empresas';
    objCuenta.BI_Subsegment_Regional__c = 'Mayoristas';
    objCuenta.BI_Subsector__c = 'Banca';
    objCuenta.BI_Sector__c = 'Industria';
    objCuenta.BI_No_Identificador_fiscal__c = '123456789';
    objCuenta.BI_Territory__c                       = 'test';
    
    insert objCuenta;

    System.debug('\n\n\n ======= CUENTA ======\n '+ objCuenta );

    //Ciudad
    objCiudad = new BI_Col_Ciudades__c ();
    objCiudad.Name = 'Test City';
    objCiudad.BI_COL_Pais__c = 'Test Country';
    objCiudad.BI_COL_Codigo_DANE__c = 'TestCDa';
    
    insert objCiudad;
    
    System.debug('Datos Ciudad ===> '+objSede);
   
    //Contactos
    objContacto = new Contact();
    objContacto.LastName = 'Test';
    objContacto.FirstName = 'Test23';
    objContacto.BI_Country__c = 'Colombia';
    objContacto.CurrencyIsoCode = 'COP'; 
    objContacto.AccountId = objCuenta.Id;
    objContacto.BI_Tipo_de_contacto__c = 'Administrador Canal Online';
    objContacto.BI_Tipo_de_documento__c = 'DNI';
    objContacto.Email = 'test@test.com';
    objContacto.Phone = '91232343';
    objContacto.BI_Numero_de_documento__c = '12343';
    objContacto.MobilePhone = '1111111111'; 
    objContacto.BI_COL_Ciudad_Depto_contacto__c = objCiudad.Id;
    objContacto.BI_COL_Direccion_oficina__c = 'Teststts';

    Insert objContacto; 

    //Direccion
    objSede = new BI_Sede__c();
    objSede.BI_COL_Ciudad_Departamento__c = objCiudad.Id;
    objSede.BI_Direccion__c = 'Test Street 123 Number 321';
    objSede.BI_Localidad__c = 'Test Local';
    objSede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
    objSede.BI_COL_Sucursal_en_uso__c = 'Libre';
    objSede.BI_Country__c = 'Colombia';
    objSede.Name = 'Test Street 123 Number 321, Test Local Colombia';
    objSede.BI_Codigo_postal__c = '12356';
    
    insert objSede;
    
    System.debug('Datos Sedes ===> '+objSede);
    
    //Sede
    objPuntosInsta = new BI_Punto_de_instalacion__c ();
    objPuntosInsta.BI_Cliente__c = objCuenta.Id;
    objPuntosInsta.BI_Sede__c = objSede.Id;
    objPuntosInsta.BI_Contacto__c = objContacto.id;
    objPuntosInsta.Name = 'QA Erroro';

    insert objPuntosInsta;

    System.debug('Datos Sucursales ===> '+objPuntosInsta);

    for(Integer i = 0; i < 10; i++){

      objOppty = new Opportunity();
      objOppty.BI_COL_Importe__c = 0;
      objOppty.Name = 'TEST AVANXO OPPTY'+i;
      objOppty.AccountId = objCuenta.Id;
      objOppty.BI_Country__c = 'Colombia';
      objOppty.CloseDate = System.today().addDays(i);
      objOppty.StageName = 'F6 - Prospecting';
      objOppty.BI_Ciclo_ventas__c = Label.BI_Completo;

      lstOppAsociadas.add(objOppty);
    }

    System.debug('\n\n\n ======= lstOppyAsociadas ======\n '+ lstOppAsociadas );

    insert lstOppAsociadas;

    //BI_MigrationHelper.disableBypass(aux);
    BI_MigrationHelper.cleanSkippedTriggers();

    rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];

    objAnexos = new BI_COL_Anexos__c();
    objAnexos.Name = 'FUN-0041414';
    objAnexos.RecordTypeId = rtBI_FUN[0].Id;

    insert objAnexos;

    System.debug('\n\n\n ======== objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);

    objDesSer = new BI_COL_Descripcion_de_servicio__c();
    objDesSer.BI_COL_Oportunidad__c = lstOppAsociadas[0].Id;
    objDesSer.CurrencyIsoCode = 'COP';

    insert objDesSer;

    System.debug('\n\n\n ======= objDesSer '+ lstModSer);

    for(Integer i = 0; i < 1; i++){

      objModSer = new BI_COL_Modificacion_de_Servicio__c();
      objModSer.BI_COL_FUN__c = objAnexos.Id;
      objModSer.BI_COL_Codigo_unico_servicio__c = objDesSer.Id;
      objModSer.BI_COL_Clasificacion_Servicio__c = 'ALTA';
      objModSer.CurrencyIsoCode = 'COP';
      objModSer.BI_COL_Fecha_instalacion_servicio_RFS__c = Date.today().addDays(30);
      objModSer.BI_COL_Oportunidad__c = objOppty.Id;
      objModSer.BI_COL_Bloqueado__c = false;            
      objModSer.BI_COL_Sucursal_de_Facturacion__c = objPuntosInsta.Id;
      objModSer.BI_COL_Sucursal_Origen__c = objPuntosInsta.Id;
      objModSer.BI_COL_Cargo_conexion__c = 10;

      lstModSer.add(objModSer);
    }

    insert lstModSer;

    List<BI_Log__c> lst_BiLog = new List<BI_Log__c>();

    objBiLog = new BI_Log__c();
    objBiLog.BI_COL_Informacion_Enviada__c = 'Informacion Enviada';
    objBiLog.BI_COL_Informacion_recibida__c = 'OK procesado correctamente';         
    objBiLog.BI_COL_Estado__c = 'Sincronizado';
    objBiLog.BI_COL_Interfaz__c = 'NOTIFICACIONES'; 
    objBiLog.BI_COL_Modificacion_Servicio__c = lstModSer[0].Id;

    lst_BiLog.add(objBiLog);

    objBiLog2 = new BI_Log__c();
    objBiLog2.BI_COL_Informacion_Enviada__c = 'Informacion Enviada';
    objBiLog2.BI_COL_Informacion_recibida__c = 'OK, Respuesta Procesada';            
    objBiLog2.BI_COL_Estado__c = 'Pendiente';
    objBiLog2.BI_COL_Interfaz__c = 'NOTIFICACIONES'; 
    objBiLog2.BI_COL_Modificacion_Servicio__c = lstModSer[0].Id;

    lst_BiLog.add(objBiLog2);

    insert lst_BiLog;

    System.debug('\n\n\n ======= lstModSer '+ lstModSer);
  }


  @isTest
  static void methodtest1(){
        
    BI_TestUtils.throw_exception = false;

    //BI_bypass__c objBibypass = new BI_bypass__c();
    //objBibypass.BI_migration__c=false;
    //objBibypass.BI_skip_trigger__c=false;

    TGS_User_Org__c userTGS = new TGS_User_Org__c();
    userTGS.TGS_Is_BI_EN__c = true;
    userTGS.BI_FVI_Is_FVI__c = false;
    userTGS.TGS_Is_TGS__c = false;
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId()];

    System.runAs ( thisUser ) {
      insert userTGS;  
      //insert objBibypass;
      objUsuario=BI_COL_CreateData_tst.getCreateUSer();
    }

    Test.startTest();
    System.runAs(objUsuario){

      crearData();
     
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, true );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, true );

      lstModSer[0].BI_COL_Fecha_instalacion_servicio_RFS__c = Date.today().addDays(20);
      Update lstModSer;

      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, true );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, true );

      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, false, false );
      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, false, true );
      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, true, false );
      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, true, true );

      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, false, false );
      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, false, true );
      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, true, false );
      //ERROR DEPLOY PLL 20160729 BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, true, true );

    }
    Test.stopTest();
  }

  @isTest   
  static void methodtest2(){

    BI_TestUtils.throw_exception = false;
   //
   //BI_bypass__c objBibypass = new BI_bypass__c();
   //objBibypass.BI_migration__c=false;
   //objBibypass.BI_skip_trigger__c=false;

    //BI_MigrationHelper.enableBypass(UserInfo.getUserId(),false,false,false,false);

    TGS_User_Org__c userTGS = new TGS_User_Org__c();
    userTGS.TGS_Is_BI_EN__c = true;
    userTGS.TGS_Is_TGS__c = false;
    userTGS.BI_FVI_Is_FVI__c = false;
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

    System.runAs(thisUser){
      insert userTGS;  
      //insert objBibypass;
      objUsuario=BI_COL_CreateData_tst.getCreateUSer();
    }
    
    System.runAs(objUsuario){
      
      crearData();
      
      Test.startTest();
      objBiLog2.BI_COL_Estado__c = 'Pendientes';
      update objBiLog2;

      objModSer.BI_COL_Estado__c = 'Inactiva';
      objModSer.CurrencyIsoCode = 'MXN';
      objModSer.BI_COL_Cargo_conexion__c = objModSer.BI_COL_Cargo_conexion__c;
      update objModSer;

      //BI_COL_Modificacion_Servicio_cls msclass = new BI_COL_Modificacion_Servicio_cls();

      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, true );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, true );

      lstModSer[0].BI_COL_Fecha_instalacion_servicio_RFS__c = Date.today().addDays(20);
      Update lstModSer;

      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, false, true );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, false );
      // BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( lstModSer , lstModSer, true, true );

      //// BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( NULL , NULL, false, false );

      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, false, false );
      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, false, true );
      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, true, false );
      BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( lstModSer , lstModSer, true, true );

      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, false, false );
      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, false, true );
      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, true, false );
      BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate( lstModSer , lstModSer, true, true );
      Test.stopTest();
    }
  }
    
  @isTest 
  static void sumTotalModificationService_test(){

    BI_TestUtils.throw_exception = false;

    //BI_bypass__c objBibypass = new BI_bypass__c();
    //objBibypass.BI_migration__c = false;
    //objBibypass.BI_skip_trigger__c = false;
    TGS_User_Org__c userTGS = new TGS_User_Org__c();
    userTGS.TGS_Is_BI_EN__c = true;
    userTGS.TGS_Is_TGS__c = false;
    userTGS.BI_FVI_Is_FVI__c = false;
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    System.runAs(thisUser){
      insert userTGS;  
      //insert objBibypass;
      objUsuario = BI_COL_CreateData_tst.getCreateUSer();
    }
   
    System.runAs(objUsuario){
      
      crearData();
      //GMN 26/11/2016 -  Test.startTest() and Test.stopTest() added
      Test.startTest();
      BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(lstModSer, lstModSer);
      BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(null, lstModSer);

      lstModSerOLD.add(objModSer);
      update lstModSerOLD;

      BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(lstModSer, lstModSerOLD);
      BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(lstModSer, null);

      BI_TestUtils.throw_exception = true;
      BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(lstModSer, lstModSerOLD);
       BI_TestUtils.throw_exception = false;
      Test.stopTest();
    }
  }


  @isTest 
  static void assertEquals_test(){

    BI_TestUtils.throw_exception = false;
   // BI_bypass__c objBibypass = new BI_bypass__c();
   // objBibypass.BI_migration__c = false;
   // objBibypass.BI_skip_trigger__c = false;
    TGS_User_Org__c userTGS = new TGS_User_Org__c();
    userTGS.TGS_Is_BI_EN__c = true;
    userTGS.TGS_Is_TGS__c = false;
    userTGS.BI_FVI_Is_FVI__c = false;
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    System.runAs(thisUser){
      insert userTGS;  
    //  insert objBibypass;
      objUsuario = BI_COL_CreateData_tst.getCreateUSer();
    }
   

    System.runAs(objUsuario){   

      loadInfo();

      Test.startTest();

      //La consulta devuelve 3 modificaciones de servicio: activa, activa, inactiva
      List<BI_COL_Modificacion_de_Servicio__c> sel_ms = [SELECT Id, BI_COL_Total_servicio__c,BI_COL_Cargo_fijo_mes__c FROM BI_COL_Modificacion_de_Servicio__c WHERE Id IN: set_id_MS];

      Opportunity sel_opp = [SELECT Id, BI_COL_Importe__c FROM Opportunity WHERE Id IN: set_id_Opp LIMIT 1];
      
      //El valor de total servicio de las modificaciones de servicio activas es 19
      System.assertEquals(19, sel_ms[0].BI_COL_Total_servicio__c);
      System.assertEquals(19 + 38, sel_opp.BI_COL_Importe__c);  // JEG assert from UAT

      //Modifica el valor del total servicio de la primera MS a 16
      sel_ms[0].BI_COL_Cargo_conexion__c = 2;
      update sel_ms;

      sel_ms = [SELECT Id, BI_COL_Total_servicio__c,BI_COL_Cargo_fijo_mes__c FROM BI_COL_Modificacion_de_Servicio__c WHERE Id IN: set_id_MS];
      sel_opp = [SELECT Id, BI_COL_Importe__c FROM Opportunity WHERE Id IN: set_id_Opp LIMIT 1];

      System.assertEquals(16, sel_ms[0].BI_COL_Total_servicio__c);
      System.assertEquals(16 + 38, sel_opp.BI_COL_Importe__c); // JEG assert from UAT

      //Desactiva una MS
      sel_ms[0].BI_COL_Estado__c = Label.BI_COL_EstadoMS_inactiva;
      update sel_ms;

      //sel_ms = [SELECT Id, BI_COL_Total_servicio__c,BI_COL_Cargo_fijo_mes__c FROM BI_COL_Modificacion_de_Servicio__c WHERE Id IN: set_id_MS];
      sel_opp = [SELECT Id, BI_COL_Importe__c FROM Opportunity WHERE Id IN: set_id_Opp LIMIT 1];

      System.assertEquals(19 + 19, sel_opp.BI_COL_Importe__c); // JEG assert from UAT

      //Activa la ultima MS
      sel_ms[2].BI_COL_Estado__c = 'Activa';
      update sel_ms;

      sel_ms = [SELECT Id, BI_COL_Total_servicio__c,BI_COL_Cargo_fijo_mes__c FROM BI_COL_Modificacion_de_Servicio__c WHERE Id IN: set_id_MS];
      sel_opp = [SELECT Id, BI_COL_Importe__c FROM Opportunity WHERE Id IN: set_id_Opp LIMIT 1];

      System.assertEquals(19, sel_ms[2].BI_COL_Total_servicio__c);
      System.assertEquals(19 * 2, sel_opp.BI_COL_Importe__c);

      //Elimina una MS 
      delete sel_ms[2];

      //sel_ms = [SELECT Id, BI_COL_Total_servicio__c,BI_COL_Cargo_fijo_mes__c FROM BI_COL_Modificacion_de_Servicio__c WHERE Id IN: set_id_MS];
      sel_opp = [SELECT Id, BI_COL_Importe__c FROM Opportunity WHERE Id IN: set_id_Opp LIMIT 1];

      System.assertEquals(19, sel_opp.BI_COL_Importe__c);

      Test.stopTest();
    }
  }


  @isTest 
  static void test_Method_One(){

    BI_TestUtils.throw_exception = false;
    TGS_User_Org__c userTGS = new TGS_User_Org__c();
    userTGS.TGS_Is_BI_EN__c = true;
    userTGS.TGS_Is_TGS__c = false;
    userTGS.BI_FVI_Is_FVI__c = false;
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    System.runAs(thisUser){
      insert userTGS;  
      //insert objBibypass;
      objUsuario = BI_COL_CreateData_tst.getCreateUSer();
    }
   
    System.runAs(objUsuario){
    crearData();
  Test.startTest();
    //Producto
    NE__Product__c objProd = new NE__Product__c();
    objProd.Offer_SubFamily__c = 'a-test;';
    objProd.Offer_Family__c = 'b-test';
    objProd.BI_Country__c = 'COL';
            
    insert objProd;     

    NE__Catalog__c objCatalog = new NE__Catalog__c();
    objCatalog.Name = 'EMPRESAS P';
    objCatalog.NE__Active__c = True;
    insert objCatalog;

    NE__Catalog_Item__c objCatalogItem = new NE__Catalog_Item__c();
    objCatalogItem.NE__Catalog_Id__c = objCatalog.Id;
    objCatalogItem.NE__ProductId__c = objProd.id;
    objCatalogItem.Country__c='Colombia';
    objCatalogItem.NE__Type__c = 'Product';
    insert objCatalogItem;

     //11/01/2017 START GSPM
    //Order
    NE__Order__c objOrder =  new NE__Order__c(); 
    objOrder.NE__OrderStatus__c =  'Active';
    objOrder.NE__CatalogId__c =  objCatalog.id; 
    objOrder.NE__OptyId__c = objOppty.Id;
    insert objOrder;
      
    //Order Item
    NE__OrderItem__c objOI1  = new NE__OrderItem__c ();
    objOI1.NE__OrderId__c = objOrder.id;      
    objOI1.NE__ProdId__c = objProd.id;      
    objOI1.NE__Qty__c = 1;
    objOI1.NE__CatalogItem__c = objCatalogItem.id;
    objOI1.BI_COL_ModificacionServicio__c = lstModSer[0].Id;
    insert objOI1;

    NE__OrderItem__c objOI2  = new NE__OrderItem__c ();
    objOI2.NE__OrderId__c = objOrder.id;      
    objOI2.NE__ProdId__c = objProd.id; 
    objOI2.NE__Qty__c = 5;
    objOI1.NE__CatalogItem__c = objCatalogItem.id;
    objOI2.BI_COL_ModificacionServicio__c = lstModSer[0].Id;
    insert objOI2;

    lstModSer[0].CurrencyIsoCode = 'COP';

    update lstModSer;
      
          BI_COL_Modificacion_Servicio_cls.updateConfigurationItem(lstModSer, lstModSerOLD);
      Test.stopTest();
    }
  }

}