/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Manuel Ochoa
 Company:       New Energy Aborda
 Description:   Case progression for SIGMA FullStack Integration - Queueable execution
 Test Class:    
 History:
 
 <Date>            <Author>             <Description>
 24-08-2017			Manuel Ochoa		First version - In development
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class TGS_Salesforce_Case_Progress_FAST implements Queueable{

	public Case caseToProgress {get;set;} // case to progress
    //public Boolean checkIsTermination {get;set;} // check if action is terminate 

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Class constructor
    Test Class:
    IN:    			Case
    OUT:
    
	History
	<Date>      <Author>     <Description>
	------------------------------------------------------------*/
	public TGS_Salesforce_Case_Progress_FAST(Case cas){
		caseToProgress = cas;
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Progress cases to resolve and closed
    Test Class:
    IN:    			
    OUT:
    
	History
	<Date>      <Author>     <Description>
	------------------------------------------------------------*/
	private void ProgressCase(){
        
		if(caseToProgress.Status == 'In Progress'){
            System.debug(LoggingLevel.ERROR,'Resolving CASE');
		    //Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), false, true, false, false);
            caseToProgress.Status = 'Resolved';
            caseToProgress.TGS_Status_Reason__c = '';
            update caseToProgress;
            return;
            //BI_MigrationHelper.disableBypass(mapa);
       	}

       	if(caseToProgress.Status == 'Resolved'){
            System.debug(LoggingLevel.ERROR,'Closing CASE');
            Constants.firstRunBilling = true;
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
            caseToProgress.Status = 'Closed';
            update caseToProgress;
        }
	}

    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Progress cases to resolve
    Test Class:
    IN:             
    OUT:
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    private void resolveCase(){        
        System.debug(LoggingLevel.ERROR,'Resolving CASE');
        //Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), false, true, false, false);
        caseToProgress.Status = 'Resolved';
        caseToProgress.TGS_Status_Reason__c = '';
        update caseToProgress;
        //BI_MigrationHelper.disableBypass(mapa);        
    }

    
    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Progress cases to close
    Test Class:
    IN:             
    OUT:
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    private void closeCase(){        
        System.debug(LoggingLevel.ERROR,'Closing CASE');
        //Constants.firstRunBilling = true;
        //NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
        caseToProgress.Status = 'Closed';
        update caseToProgress;       
    }




    /*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Execute method for queable class
    Test Class:
    IN:
    OUT:   		
    
	History
	<Date>      <Author>     <Description>
	------------------------------------------------------------*/
	public void execute(QueueableContext context) {
        // first run from 'In Progress' to resolve
        // implies asseting the order
        if(caseToProgress.Status == 'In Progress'){
            System.debug(LoggingLevel.ERROR,'CASE status: ' + caseToProgress.Status);
            resolveCase();
            //ProgressCase();
            // hack to wait 5 seconds by scheduling job            
            System.debug(LoggingLevel.ERROR,'Launching sched job');
            Datetime dtNow=Datetime.now();
            System.debug(dtNow);
            dtnow=dtNow.addSeconds(15); // wait seconds
            System.debug(dtNow);
            String strHour = String.valueOf(dtNow.hour());
            String strMin = String.valueOf(dtNow.minute());
            String strSecs = String.valueOf(dtNow.second());                
            //parse to cron expression
            String strNextFireTime = strSecs + ' ' + strMin + ' ' + strHour + ' * * ?';
            System.debug(strNextFireTime);
            TGS_Case_Progress_Schedulable caseProgressorSched = new TGS_Case_Progress_Schedulable();
            caseProgressorSched.caseToProgress=caseToProgress;
            System.schedule('ScheduledJob' + caseToProgress.id, strNextFireTime,caseProgressorSched);            
            return;
        }
        // second run from 'Resolve' to 'Closed'
        if(caseToProgress.Status == 'Resolved'){
            System.debug(LoggingLevel.ERROR,'CASE status: ' + caseToProgress.Status);
            closeCase();
        }
    }   
}