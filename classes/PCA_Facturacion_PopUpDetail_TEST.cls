@isTest
private class PCA_Facturacion_PopUpDetail_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_Facturacion_PopUpDetail class 
    
    History: 
    <Date> 					<Author> 				<Change Description>
    13/08/2014      		Ana Escrich	    		Initial Version
	15/02/2017				Pedro Párraga			Increase coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void getloadInfo() {
 		User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.debug('usr: '+usr);
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            List<RecordType> rT = [select id, name from Recordtype where name='Concepto factura'];
			BI_Facturas__c cobranzaObject = new BI_Facturas__c(Name='Test2',BI_Nombre_del_cliente__c=accList[0].Id, CurrencyIsoCode='ARS');
			insert cobranzaObject;
	    	system.runAs(user1){
			 
				 PageReference pageRef = new PageReference('PCA_DetailFacturas?PCA_Login');
	       		 Test.setCurrentPage(pageRef);
	       		 ApexPages.currentPage().getParameters().put('Id', cobranzaObject.Id);
		   		 
	        	 PCA_Facturacion_PopUpDetail controller = new PCA_Facturacion_PopUpDetail();
	        	 BI_TestUtils.throw_exception = false;
	        	 controller.loadInfo();
                 system.assertEquals(controller.factId,cobranzaObject.Id); 
	        	 controller.checkPermissions();
			}
			system.runAs(user1){
			 
				 PageReference pageRef = new PageReference('PCA_DetailFacturas?PCA_Login');
	       		 Test.setCurrentPage(pageRef);
	       		 ApexPages.currentPage().getParameters().put('Id', cobranzaObject.Id);
		   		 
	        	 PCA_Facturacion_PopUpDetail controller = new PCA_Facturacion_PopUpDetail();
	        	 BI_TestUtils.throw_exception = true;
	        	 controller.loadInfo();
	        	 controller.checkPermissions();
	        	 BI_TestUtils.throw_exception = false;
	        	 controller.createTask();
			}
	    }
	}
}