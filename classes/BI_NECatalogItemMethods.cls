public class BI_NECatalogItemMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the NE__Catalog_Item__c trigger.
    History:
    
    <Date>            <Author>          <Description>
    05/05/2014        Pablo Oliva       Initial version
    14/05/2014        Pablo Oliva       Bug fixed
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   When the NE__Catalog_Item__c.Not_available_for_campaigns__c field changes to true, the associated campaigns must be inactivated
    
    IN:            ApexTrigger.old, ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    05/05/2014        Pablo Oliva       Initial version
    14/05/2014        Pablo Oliva       Bug fixed
    15/05/2017        Cristina Rodríguez       Solve Multiple Trigger On same sObject 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void inactivateCampaign(List<NE__Catalog_Item__c> news, List<NE__Catalog_Item__c> olds){
        try{
	        List<NE__Catalog_Item__c> news2 = new List<NE__Catalog_Item__c>();
	        Set<Id> set_promotions = new Set<Id>();
	        
	        Integer i = 0;
	        for(NE__Catalog_Item__c nci:news){
	            if(nci.Not_available_for_campaigns__c && !olds[i].Not_available_for_campaigns__c)
	                set_promotions.add(nci.NE__PromotionId__c);
	        }
	        
	        if(!set_promotions.isEmpty()){
	            List<Campaign> lst_campaigns = new List<Campaign>();
	        
	            for(Campaign cmp:[select Id from Campaign where NE__Promotion__c IN :set_promotions]){
	                cmp.IsActive = false;
	                lst_campaigns.add(cmp);
	            }
	            
	            update lst_campaigns;
	        }
        }catch (exception Exc){
			BI_LogHelper.generate_BILog('BI_NECatalogItemMethods.inactivateCampaign', 'BI_EN', Exc, 'Trigger');
		}
        
    }
    // START CRM 15/05/2017 - Merge UpdateCurrencyIsoCode.trigger
    public static void updateCurrencyIsoCode(List<NE__Catalog_Item__c> news){
        try{           
            for(NE__Catalog_Item__c nci:news){
                try {           
                    if((nci.CurrencyIsoCode != nci.NE__Currency__c) && (nci.NE__Currency__c != null))
                        nci.CurrencyIsoCode = nci.NE__Currency__c;    
                }
                catch(Exception e) {
                    System.debug(e);
                }
            }        
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_NECatalogItemMethods.updateCurrencyIsoCode', 'BI_EN', Exc, 'Trigger');
        }
        
    }
    // END CRM 15/05/2017 - Merge UpdateCurrencyIsoCode.trigger

}