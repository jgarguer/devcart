/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Contact_Management

History:
<Date>							<Author>						<Change Description>
12/06/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_CHI_Contact_Management_Test {
    
    @testSetup 
    private static void init() {
        
    }
    
    @isTest private static void sync() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueb';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueb';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueb';
        insert config_integracion;
        
        System.Test.startTest();
        
        try{ System.assertEquals(JSON.serialize((BI_RestWrapper.ContactsListType) JSON.deserialize('{"contacts":[{"contactDetails":{"name":{"givenName":"Agustín","familyName":"González"},"addresses":[{"addressName":"Av. Providencia","addressNumber":{"value": "111"},"locality":"Santiago","region":"Metropolitana"}]}}],"totalResults":1}', BI_RestWrapper.ContactsListType.class)).length(), JSON.serialize(FS_CHI_Contact_Management.Sync('RUT', '000000000', '000000000')).length()); 
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
}