/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Alvaro Sevilla
 Company:       Accenture LTDA
 Project:       Subsidios ARG
 Description:   Clase de prueba para BI_SUB_Buttons3

 History:

 <Date>               <Author>             <Change Description>
 07/03/2018         Alvaro Sevilla          Initial Version
 ------------------------------------------------------------------------------------------------*/
@isTest
private class BI_SUB_Buttons3_TEST {


 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   metodo de prueba para metodo cancelarCasoSubsidio

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void cancelarCasoSubsidio1_Test() {
		Test.startTest();
		  Account acc = new Account(
          Name = 'Test1ARG',
          Industry = 'Comercio',
          CurrencyIsoCode = 'ARS',
          BI_Activo__c = 'Sí',
          BI_Cabecera__c = 'Sí',
          BI_Estado__c = true,
          BI_Country__c = Label.BI_Argentina,
          BI_Segment__c = 'Negocios',
          TGS_Es_MNC__c = false,
          BI_No_Identificador_fiscal__c = '30111111118'
        );
        insert acc;

        BI_MigrationHelper.cleanSkippedTriggers();
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            BI_SUB_Subsidio__c = 'Cambio'
        );
        insert opp;

		RecordType rType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];

        Case caso = new Case(
        AccountId = acc.Id,
        BI_Nombre_de_la_Oportunidad__c = opp.Id,
        BI_Country__c = 'Argentina',
        BI_Department__c = 'Pricing Empresas',
        BI_Type__c = 'Subsidio',
        BI_Subtype__c = 'Cambio con Subsidio',
        Status = 'In Progress',
        RecordTypeId = rType.Id

        );
        insert caso;
        String ret = BI_SUB_Buttons3.cancelarCasoSubsidio(caso.Id);
        System.assertEquals('exito', ret);
        String resul = BI_Buttons.validarOptyARGConSubsidio(opp.Id);
        String respsub = resul.substring(0, 7);
        System.assertEquals('ninguno', respsub);
        Test.stopTest();
  }


 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   metodo de prueba para metodo cancelarCasoSubsidio

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  @isTest static void cancelarCasoSubsidio2_Test() {
    Test.startTest();
    String ret = BI_SUB_Buttons3.cancelarCasoSubsidio('5000E0000064iR');
    String retsub = ret.substring(0, 5);
        System.assertEquals('ERROR', retsub);
        Test.stopTest();
	}

}