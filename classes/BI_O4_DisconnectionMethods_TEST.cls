/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_DisconnectionMethods class
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016     Fernando Arteaga	Initial Version
 20/09/2016	Ricardo Pereira		Added loadAccounts(), so it substitute BI_DataLoad.loadAccounts()
 24/11/2016	Alvaro García		Add SKIP_PARAMETER = false
 16/12/2016	Guillermo Muñoz	Fix bugs on deploy
 20/09/2017	Antonio Mendivil	Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
 25/09/2017	Jaime Regidor		Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class BI_O4_DisconnectionMethods_TEST {

	static{
        BI_TestUtils.throw_exception = false;
    }
	
	@testSetup
	static void startup() {
		insert new TGS_User_Org__c(TGS_Is_TGS__c=false, TGS_Is_BI_EN__c=true);
		insert new BI_bypass__c(BI_skip_trigger__c = false);
	}

	@isTest static void test_setCustomFieldAccountOwner() {

		User usu = [SELECT Id FROM User WHERE Profile.Name LIKE 'BI_O4_TNA%' AND IsActive = true LIMIT 1];
		List<Account> lstAccounts;

		System.runAs(usu){
			lstAccounts = loadAccounts(1, new String[]{'España'});
		

			Test.startTest();
			BI_DataLoad.insertDisconnections(1, lstAccounts);
			Test.stopTest();
		}
	}

	@isTest static void test_validateRequiredFields() {

		User usu = [SELECT Id FROM User WHERE Profile.Name LIKE 'BI_O4_TNA%' AND IsActive = true LIMIT 1];
		List<Account> lstAccounts;

		System.runAs(usu){
			lstAccounts = loadAccounts(1, new String[]{'España'});
		

			List<BI_O4_Disconnection__c> lstDisconnections = BI_DataLoad.insertDisconnections(1, lstAccounts);
			
			lstDisconnections[0].BI_O4_Status__c = 'Provider Reviewer Assignment';
			update lstDisconnections;
			
			Test.startTest();
			try {
				lstDisconnections[0].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_REQUEST_REVIEW;
				update lstDisconnections;
				System.assert(false, 'Did not enter "Provider Disconnections" validation');
			} catch (exception e) {
			}
			try {
				lstDisconnections[0].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_PROVIDER_DISCONNECTIONS;
				update lstDisconnections;
				System.assert(false, 'Did not enter validation');
			} catch (exception e) {
			}
			try {
				lstDisconnections[0].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_PROJECT_MANAGER_ASSIGNMENT;
				update lstDisconnections;
				System.assert(false, 'Did not enter validation');
			} catch (exception e) {
			}
			try {
				lstDisconnections[0].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_FINANCIAL_PLANNING_REVIEW;
				update lstDisconnections;
				System.assert(false, 'Did not enter validation');
			} catch (exception e) {
			}
			try {
				lstDisconnections[0].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_DISCONNECTION_EXECUTION;
				update lstDisconnections;
				System.assert(false, 'Did not enter validation');
			} catch (exception e) {
			}
			Test.stopTest();
		}
	}

	@isTest static void test_setRecordsOwnership() {

		User usu = [SELECT Id FROM User WHERE Profile.Name LIKE 'BI_O4_TNA%' AND IsActive = true LIMIT 1];

		List<Account> lstAccounts;

		System.runAs(usu){
			lstAccounts = loadAccounts(1, new String[]{'España'});
		
			BI_DataLoad.SKIP_PARAMETER = false;
			
			List<BI_O4_Disconnection__c> lstDisconnections = BI_DataLoad.insertDisconnections(4, lstAccounts);

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Provider_Reviewer__c = UserInfo.getUserId();
				disc.BI_O4_Project_Manager__c = UserInfo.getUserId();
				disc.BI_O4_Customer_Acceptance_Date__c = System.today();
			}

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Provider Reviewer Assignment';
			}
			update lstDisconnections;

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_REQUEST_REVIEW;
				disc.BI_O4_Provider_Reviewer__c = UserInfo.getUserId();
			}
			update lstDisconnections;

			Test.startTest();
			
			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Provider_Disconnection_Cost__c = 12;
				disc.BI_O4_Total_Credit__c = 12;
				disc.BI_O4_Total_Penalty__c = 1;
				disc.BI_O4_Contract_Status__c = 'Active';
			}
			lstDisconnections[1].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_PENALTY_REVIEW_WITH_CUSTOMER;
			lstDisconnections[2].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_PENALTY_REVIEW_WITH_CUSTOMER;
			update lstDisconnections;
			
			lstDisconnections[2].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_NEGOTIATE_WITH_CUSTOMER;
			update lstDisconnections;

			lstDisconnections[1].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_PENALTIES_DISPUTE_REVIEW;
			lstDisconnections[1].BI_O4_Customer_Objection__c = 'Yes';
			lstDisconnections[1].BI_O4_Objection_Type__c = 'Legal';
			//lstDisconnections[1].BI_O4_Legal_Objection_Solved__c = 'Yes';

			lstDisconnections[2].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_NEGOTIATION_DETAILS_REVIEW;
			lstDisconnections[2].BI_O4_Negotiation_End_Result__c = 'Cancel Disconnection';
			lstDisconnections[2].BI_O4_Customer_Objection__c = 'Yes';
			lstDisconnections[2].BI_O4_Objection_Type__c = 'Legal';
			lstDisconnections[2].BI_O4_Legal_Objection_Solved__c = 'Yes';

			lstDisconnections[0].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_FINANCIAL_PLANNING_REVIEW;
			lstDisconnections[0].BI_O4_Project_Manager_Leader__c = UserInfo.getUserId();
			lstDisconnections[0].BI_O4_Customer_Objection__c = 'Yes';
			lstDisconnections[0].BI_O4_Objection_Type__c = 'Legal';
			lstDisconnections[0].BI_O4_Legal_Objection_Solved__c = 'Yes';
			update lstDisconnections;
			
			lstDisconnections[3].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_PROJECT_MANAGER_ASSIGNMENT;
			lstDisconnections[3].BI_O4_Project_Manager_Leader__c = UserInfo.getUserId();
			lstDisconnections[3].BI_O4_Customer_Objection__c = 'Yes';
			lstDisconnections[3].BI_O4_Objection_Type__c = 'Legal';
			lstDisconnections[3].BI_O4_Reviewed_By_Financial_Planning__c = true;
			lstDisconnections[3].BI_O4_Legal_Objection_Solved__c = 'Yes';
			update lstDisconnections[3];
			
			lstDisconnections[3].BI_O4_Status__c = BI_O4_DisconnectionMethods.BIO4_DISC_STAT_DISCONNECTION_EXECUTION;
			update lstDisconnections[3];


			Test.stopTest();
		}

	}
	
	@isTest static void test_checkRelatedCaseBillingRequest() {

		User usu = [SELECT Id FROM User WHERE Profile.Name LIKE 'BI_O4_TNA%' AND IsActive = true LIMIT 1];
		
		List<Account> lstAccounts;
		List<BI_O4_Disconnection__c> lstDisconnections;

		System.runAs(usu){

			lstAccounts = loadAccounts(1, new String[]{'España'});
		}
		
		lstDisconnections = BI_DataLoad.insertDisconnections(1, lstAccounts);		

		User me = new User(Id = UserInfo.getUserId());

		System.runAs(me){
			update new User(Id = UserInfo.getUserId(), BI_Permisos__c = 'Finance');
		}
		

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Provider Reviewer Assignment';
			}
			update lstDisconnections;

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Request Review';
				disc.BI_O4_Provider_Reviewer__c = usu.Id;
			}
			
			update lstDisconnections;

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Penalty Review With Customer';
				disc.BI_O4_Total_Penalty__c = 12;
				disc.BI_O4_Total_Credit__c = 12;
				disc.BI_O4_Contract_Status__c = 'Active';
				disc.BI_O4_Provider_Disconnection_Cost__c = 12;
			}
			update lstDisconnections;

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Financial Planning Review';
				disc.BI_O4_Customer_Objection__c = 'No';
				disc.BI_O4_Project_Manager_Leader__c = usu.Id;
			}
			Test.startTest();
			update lstDisconnections;

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Financial Planning Review';
				disc.BI_O4_Customer_Objection__c = 'No';
				disc.BI_O4_Project_Manager_Leader__c = usu.Id;
			}
			update lstDisconnections;

		
			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Project Manager Assignment';
				disc.BI_O4_Project_Manager__c = usu.Id;
			}
			update lstDisconnections;


			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Disconnection Execution';
			}
			Map <Integer, BI_bypass__c> mapa = new Map <Integer, BI_bypass__c>();
			mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
			update lstDisconnections;

			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Status__c = 'Disconnection Execution';
			}
			update lstDisconnections;

			List<Case> lstCases = BI_DataLoad.insertDisconnectionCases(1, lstDisconnections);
			System.assertNotEquals(lstCases, null);
			System.assertNotEquals(lstCases.size(), 0);
			System.assertEquals([SELECT BI_Type__c FROM Case WHERE Id = :lstCases.get(0).Id].BI_Type__c, 'Factura');

			
			for(BI_O4_Disconnection__c disc : lstDisconnections) {
				disc.BI_O4_Disconnection_MRC__c = 15.0;
				disc.BI_O4_Disconnection_Effective_Date__c = Date.today();
				disc.BI_O4_Status__c = 'Closed';
			}
			update lstDisconnections;
			BI_MigrationHelper.disableBypass(mapa);
			Test.stopTest();
		
	}
///////
	public static List<Account> loadAccountsWithOtherRT(Integer i, List<String> lst_pais){
        try{
            
            RecordType rtAccCC = [SELECT Id FROM RecordType WHERE DeveloperName =: 'TGS_Customer_Country' AND SObjectType = 'Account' limit 1];
            RecordType rtAccLE = [SELECT Id FROM RecordType WHERE DeveloperName =: 'TGS_Legal_Entity' AND SObjectType = 'Account' limit 1];
            
            List<Account> lst_acc = new List<Account>();
               
                    Account accCC = new Account(Name = 'test CC',
                                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                              BI_Activo__c = Label.BI_Si,
                                              BI_Country__c = lst_pais[0],
                                              BI_Segment__c = 'Empresas',
                                              BI_Subsector__c = 'test', //25/09/2017
											  BI_Sector__c = 'test', //25/09/2017
                                              BI_Subsegment_local__c = 'Top 1',
                                              RecordTypeId = rtAccCC.Id,
                                              BI_Subsegment_Regional__c = 'test',
       										  BI_Territory__c = 'test'
                                              );
                    Account accLE = new Account(Name = 'test LE',
                                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                              BI_Activo__c = Label.BI_Si,
                                              BI_Country__c = lst_pais[0],
                                              BI_Segment__c = 'Empresas',
                                              BI_Subsector__c = 'test', //25/09/2017
											  BI_Sector__c = 'test', //25/09/2017
                                              BI_Subsegment_local__c = 'Top 1',
                                              RecordTypeId = rtAccLE.Id,
                                              BI_Subsegment_Regional__c = 'test',
       										  BI_Territory__c = 'test'
                                              );                          
                    
                    lst_acc.add(accCC);
                    lst_acc.add(accLE);
            	
            insert lst_acc;
            
            return lst_acc;
        }catch (exception Exc){
            System.debug('loadAccountsWithOtherRT-->ERROR'+Exc);
           BI_LogHelper.generate_BILog('BI_O4_DisconnectionMethods_TEST.loadAccounts', 'BI_EN', Exc, 'Trigger');
           return null;
        }
    }
//////
	public static List<Account> loadAccounts(Integer i, List<String> lst_pais){
        try{
            
            RecordType rtAcc = [SELECT Id FROM RecordType WHERE DeveloperName != 'TGS_Business_Unit' AND SObjectType = 'Account' limit 1];
            List<Account> lst_acc = new List<Account>();
                for(Integer j = 0; j < i; j++){
                    
                
                    Account acc = new Account(Name = 'test'+ String.valueOf(j),
                                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                              BI_Activo__c = Label.BI_Si,
                                              BI_Country__c = lst_pais[0],
                                              BI_Segment__c = 'Empresas',
                                              BI_Subsector__c = 'test', //25/09/2017
					      BI_Sector__c = 'test', //25/09/2017
                                              BI_Subsegment_local__c = 'Top 1',
                                              RecordTypeId = rtAcc.Id,
                                              BI_Subsegment_Regional__c = 'test',
       					      BI_Territory__c = 'test'
                                              );
                                              //BI_Sector_Ref__c = lst_pais[j].id); //Sector no admite Ids de objetos Region__c
                    
                    lst_acc.add(acc);
                }
            insert lst_acc;
            
            return lst_acc;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_O4_DisconnectionMethods_TEST.loadAccounts', 'BI_EN', Exc, 'Trigger');
           return null;
        }
    }

}