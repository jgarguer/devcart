/************************************************************************************
* Avanxo Colombia
* @author           Sebastián Ortiz Niño href=<seortiz@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                               Descripción        
*           -----   ----------      ---------------------------         ---------------    
* @version   1.0    2015-30-06      Sebastián Ortiz Niño (SO)           New test Class
* @version   1.1    2015-24-07      Manuel Esthiben Mendez Devia (MEMD) Modificated test class
* @version   1.2    2015-27-08      Jeisson Alexander Rojas Noy (JR)    Modificacion test Class
  					13/03/2017		Marta Gonzalez(Everis)				REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
                    19/09/2017      Angel F. Santaliestra               Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/


@isTest
private class BI_COL_AutoconsumoFUN_tst
{
    Public static list<Profile>                                             lstPerfil;
    Public static list <UserRole>                                           lstRoles;
    Public static User                                                  objUsuario = new User();
    public static BI_COL_Anexos__c                              objAnexos;
    public static BI_COL_Modificacion_de_Servicio__c            objModSer;
    public static BI_COL_Descripcion_de_servicio__c             objDesSer;
    public static Account                                       objCuenta;
    public static Opportunity                                   objOpp;
    public static List<RecordType>                              rtBI_FUN;
    public static List<RecordType>                              rtOpp;
    public static list<BI_COL_AutoconsumoFUN_ctr.WrapperLst>    lstGlobal;
    public static List<BI_COL_Modificacion_de_Servicio__c>      lstMS;
    public static BI_COL_AutoconsumoFUN_ctr                     objAutoconsumoFUN;
    public static Contact                               objContacto;
    public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;

    //(JR)
    Public static List <BI_COL_Modificacion_de_Servicio__c>     lstModSer;
    public static BI_COL_AutoconsumoFUN_ctr.WrapperLst          objWrapperLst;
    
    public static void crearData()
    {
        ////List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        ////list<User> lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
        
        ////lstPerfil
        //lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        ////lstRol
        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        
        //list<User> lstUsuarios = new list<User>();
        //lstUsuarios.add(objUsuario);
        //insert lstUsuarios;

        //System.runAs(lstUsuarios[0])
        //{
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;
            
            objCuenta                                       = new Account();
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
            objCuenta.CurrencyIsoCode                       = 'GTQ';
            objCuenta.BI_Segment__c                        = 'test';
            objCuenta.BI_Subsegment_Regional__c            = 'test';
            objCuenta.BI_Territory__c                      = 'test';

            insert objCuenta;
            System.debug('\n\n\n Sosalida objCuenta '+ objCuenta );

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                          = 'Test';
            objContacto.BI_Country__c                     = 'Colombia';
            objContacto.CurrencyIsoCode                   = 'COP'; 
            objContacto.AccountId                         = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c        = 'Administrador Canal Online';
            objContacto.Phone                     = '571234567';
            objContacto.MobilePhone                 = '3104785925';
            objContacto.Email                   = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c       = 'Dirprueba';
            objContacto.BI_COL_Ciudad_Depto_contacto__c   = objCiudad.Id;
        	//REING-INI
        	//objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
        	objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.BI_Tipo_de_contacto__c          = 'Autorizado;General';
        	//REING_FIN
            Insert objContacto;

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);
            
            objOpp                                          = new Opportunity();
            objOpp.Name                                     = 'prueba opp';
            objOpp.AccountId                                = objCuenta.Id;
            objOpp.BI_Country__c                            = 'Colombia';
            objOpp.CloseDate                                = System.today().addDays(+5);
            objOpp.StageName                                = 'F6 - Prospecting';
            objOpp.CurrencyIsoCode                          = 'GTQ';
            objOpp.Certa_SCP__contract_duration_months__c   = 12;
            objOpp.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
            objOpp.BI_COL_Autoconsumo__c                    = true;
            insert objOpp;
            System.debug('\n\n\n Sosalida objOpp '+ objOpp );
            
            rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            
            objAnexos               = new BI_COL_Anexos__c();
            objAnexos.Name          = 'FUN-0041414';
            objAnexos.RecordTypeId  = rtBI_FUN[0].Id;
            insert objAnexos;
            System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
            
            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOpp.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
            System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);
            List<BI_COL_Descripcion_de_servicio__c> lstqry = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
            System.debug('\n\n\n Sosalida lstqry '+ lstqry);
            
            objModSer = new BI_COL_Modificacion_de_Servicio__c();
            objModSer.BI_COL_FUN__c                         = objAnexos.Id;
            objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModSer.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModSer.BI_COL_Bloqueado__c                   = false;
            objModSer.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            insert objModSer;
            System.debug('\n\n\n Sosalida objModSer '+ objModSer);
        //} 
        
    }

    //Version 1.2 Metodo test_method_one Agrgado (JR)
    static testMethod void test_method_1()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();

            objModSer = new BI_COL_Modificacion_de_Servicio__c();
            //objWrapperLst = new BI_COL_AutoconsumoFUN_ctr.WrapperLst(); 

            lstModSer = new List<BI_COL_Modificacion_de_Servicio__c>();
            lstGlobal = new list<BI_COL_AutoconsumoFUN_ctr.WrapperLst>();

            for (Integer i = 0; i<11 ; i++)
            {
                objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
                objModSer.BI_COL_FUN__c                         = objAnexos.Id;
                objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
                objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
                //objModSer.BI_COL_Oportunidad__c               = objOportunidad.Id;
                objModSer.BI_COL_Bloqueado__c                   = false;
                objModSer.BI_COL_Estado__c                      = label.BI_COL_lblActiva;//label.BI_COL_lblActiva;
                //objModSer.BI_COL_Sucursal_de_Facturacion__c   = objPuntosInsta.Id;
                //objModSer.BI_COL_Sucursal_Origen__c           = objPuntosInsta.Id;

                lstModSer.add(objModSer);           

                objWrapperLst = new BI_COL_AutoconsumoFUN_ctr.WrapperLst();
                objWrapperLst.salto = 'Prueba';
                objWrapperLst.visible = 'prueba 1';
                objWrapperLst.lista = lstModSer;

                lstGlobal.add(objWrapperLst);

            }

            System.debug('objWrapperLst ==============> '+objWrapperLst);
            
    		Test.startTest();
    			    
    			ApexPages.currentPage().getParameters().put('idFUN', objAnexos.Id);            
    			ApexPages.currentPage().getParameters().put('CFM', 'true');
    			ApexPages.currentPage().getParameters().put('CNX', 'true');
    			ApexPages.currentPage().getParameters().put('nombreSucursal', 'true');
    			ApexPages.currentPage().getParameters().put('ciudadDestino', 'true');
    			ApexPages.currentPage().getParameters().put('bwPorExceso', 'true');
    			ApexPages.currentPage().getParameters().put('costoBwPorExceso', 'true');
    			ApexPages.currentPage().getParameters().put('demo', 'true');
    			 
    			objAutoconsumoFUN = new BI_COL_AutoconsumoFUN_ctr();

    			objAutoconsumoFUN.lstMS = lstModSer;
    			lstGlobal.add(objWrapperLst);
    			lstGlobal = objAutoconsumoFUN.lstGlobal;

    		Test.stopTest();
        }
    }

    static testMethod void test_method_2()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();

            objModSer = new BI_COL_Modificacion_de_Servicio__c();
            //objWrapperLst = new BI_COL_AutoconsumoFUN_ctr.WrapperLst(); 

            lstModSer = new List<BI_COL_Modificacion_de_Servicio__c>();
            lstGlobal = new list<BI_COL_AutoconsumoFUN_ctr.WrapperLst>();

            for (Integer i = 0; i<10 ; i++)
            {
                objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
                objModSer.BI_COL_FUN__c                         = objAnexos.Id;
                objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
                objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
                //objModSer.BI_COL_Oportunidad__c               = objOportunidad.Id;
                objModSer.BI_COL_Bloqueado__c                   = false;
                objModSer.BI_COL_Estado__c                      = label.BI_COL_lblActiva;//label.BI_COL_lblActiva;
                //objModSer.BI_COL_Sucursal_de_Facturacion__c   = objPuntosInsta.Id;
                //objModSer.BI_COL_Sucursal_Origen__c           = objPuntosInsta.Id;

                lstModSer.add(objModSer);           
            }

            for (Integer i = 0; i<9 ; i++)
            {
                objWrapperLst = new BI_COL_AutoconsumoFUN_ctr.WrapperLst();
                objWrapperLst.salto = 'Prueba';
                objWrapperLst.visible = 'prueba 1';
                objWrapperLst.lista = lstModSer;

                lstGlobal.add(objWrapperLst);

            }

            System.debug('objWrapperLst ==============> '+objWrapperLst);
            
            Test.startTest();
                
    			ApexPages.currentPage().getParameters().put('idFUN', objAnexos.Id);            
    			ApexPages.currentPage().getParameters().put('CFM', 'true');
    			ApexPages.currentPage().getParameters().put('CNX', 'true');
    			ApexPages.currentPage().getParameters().put('nombreSucursal', 'true');
    			ApexPages.currentPage().getParameters().put('ciudadDestino', 'true');
    			ApexPages.currentPage().getParameters().put('bwPorExceso', 'true');
    			ApexPages.currentPage().getParameters().put('costoBwPorExceso', 'true');
    			ApexPages.currentPage().getParameters().put('demo', 'true');
    			ApexPages.currentPage().getParameters().put('anexoServicios', 'true');

    			objAutoconsumoFUN = new BI_COL_AutoconsumoFUN_ctr();

    			objAutoconsumoFUN.lstMS = lstModSer;
    			lstGlobal.add(objWrapperLst);
    			lstGlobal = objAutoconsumoFUN.lstGlobal;
    			String anexo = objAutoconsumoFUN.anexoServicios;
    			objAutoconsumoFUN.calcularTotales(lstModSer);

            Test.stopTest();
        }
    }

    static testmethod void test_method_3()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();
            Test.startTest();
                
                objAutoconsumoFUN = new BI_COL_AutoconsumoFUN_ctr();

                String  cf = objAutoconsumoFUN.CFM;
                String  cn = objAutoconsumoFUN.CNX;
                String  nombre = objAutoconsumoFUN.nombreSucursal;
                String  cuidad = objAutoconsumoFUN.ciudadDestino;
                String  bwPor = objAutoconsumoFUN.bwPorExceso;
                String  costoBwP = objAutoconsumoFUN.costoBwPorExceso;
                String  dem = objAutoconsumoFUN.demo;
               
            Test.stopTest();
        }       
        
    }

    static testmethod void myTestMethod()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();
            Test.startTest();
                ApexPages.currentPage().getParameters().put('idFUN', objAnexos.Id);            
                ApexPages.currentPage().getParameters().put('CFM', 'true');
                ApexPages.currentPage().getParameters().put('CNX', 'true');
                ApexPages.currentPage().getParameters().put('nombreSucursal', 'true');
                ApexPages.currentPage().getParameters().put('ciudadDestino', 'true');
                ApexPages.currentPage().getParameters().put('bwPorExceso', 'true');
                ApexPages.currentPage().getParameters().put('costoBwPorExceso', 'true');
                ApexPages.currentPage().getParameters().put('demo', 'true');
                objAutoconsumoFUN = new BI_COL_AutoconsumoFUN_ctr();
                objAutoconsumoFUN.fun = objAnexos;
                String str = objAutoconsumoFUN.numContrato;
                String rzn = objAutoconsumoFUN.razonSocial;
                String nt = objAutoconsumoFUN.nit;
                String dir = objAutoconsumoFUN.direccion;
                String tel = objAutoconsumoFUN.telefono;
                String eml = objAutoconsumoFUN.email;
                Decimal tot = objAutoconsumoFUN.totalInstalacion;
                Decimal total = objAutoconsumoFUN.totalServicio;
                Decimal totalOr= objAutoconsumoFUN.totalOrden;
                String mon = objAutoconsumoFUN.tipoMoneda;
                Integer dur = objAutoconsumoFUN.duracionContrato;
                String ase = objAutoconsumoFUN.asesorVtas;
                Integer cant = objAutoconsumoFUN.cantidadColumnas;
                boolean logo = objAutoconsumoFUN.logoMovistar;
                boolean logotel =objAutoconsumoFUN.logoTelefonica;
                String tipoOr= objAutoconsumoFUN.tipoOrden;
                String dire = objAutoconsumoFUN.direcc;
                objAutoconsumoFUN.lstMS = new List<BI_COL_Modificacion_de_Servicio__c>();
                objAutoconsumoFUN.lstMS.add(objModSer);
                String anexo = objAutoconsumoFUN.anexoServicios;
                String  cf = objAutoconsumoFUN.CFM;
                String  cn = objAutoconsumoFUN.CNX;
                String  nombre = objAutoconsumoFUN.nombreSucursal;
                String  cuidad = objAutoconsumoFUN.ciudadDestino;
                String  bwPor = objAutoconsumoFUN.bwPorExceso;
                String  costoBwP = objAutoconsumoFUN.costoBwPorExceso;
                String  dem = objAutoconsumoFUN.demo;
                String numor = objAutoconsumoFUN.numOrdenServicio;
                lstGlobal = objAutoconsumoFUN.lstGlobal;
                List<BI_COL_Modificacion_de_Servicio__c> lst = objAutoconsumoFUN.getMSSolicitudServicio('idFUN');
                objAutoconsumoFUN.calcularTotales(lst);
            Test.stopTest();
        }
    }
}