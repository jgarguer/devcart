@isTest(seeAllData = false)
public class BIIN_ConsultaIncidencia_Ctrl_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales Rodríguez
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_ConsultaIncidencia_Ctrl
            
    <Date>               <Author>                           <Change Description>
    11/2015              Ignacio Morales Rodríguez          Initial Version
    05/07/2016           José Luis González Beltrán          Adapt test to UNICA modifications
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Listado_Ticket_TEST';

    @testSetup static void dataSetup() 
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }

    @isTest static void test_BIIN_ConsultaIncidencia_Ctrl_TEST_1() 
    {
		BI_TestUtils.throw_exception = false;

        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        List<Case> caso = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BIIN_FechaInicial__c, BIIN_FechaFinal__c FROM Case WHERE AccountId =: account.Id LIMIT 1];
        
        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ListadoTickets'));
         
        ApexPages.StandardSetController setcon = new ApexPages.StandardSetController(caso);
        BIIN_ConsultaIncidencia_Ctrl ext = new BIIN_ConsultaIncidencia_Ctrl(setcon);
        PageReference printView = ext.printView();
        ext.getobjHttpRequest();
        ext.getHttpResponse();
        ext.caso = caso.get(0);
        ext.onClickBuscar();

        Test.stopTest();
        
	}
}