public class SEG_HexUtils { 

    
    public static Integer hexToIntLE(String hexStr){
        Integer result = 0;

        for (Integer j = 0; j < hexStr.length(); j+=2){
            result |= (((hexStr.charAt(j  ) & 15)+(hexStr.charAt(j  )>>>6)*9)) << (4*(j+1));
            result |= (((hexStr.charAt(j+1) & 15)+(hexStr.charAt(j+1)>>>6)*9)) << (4*(j  ));
        }

        return result;
    }

    //Int to Little Endian Hex
    public static String intToHexLE(Integer decNumber, Integer sizeInBytes)
    {
        String[] hexValues = new List<String>{'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
        String hexStr = '';
        Integer byteLocation = 0;

        while (decNumber != 0 && hexStr.length()<sizeInBytes*2)
        {
            hexStr += hexValues[(decNumber>>>4) & 15] + hexValues[decNumber & 15];
            decNumber >>>= 8;
            byteLocation++;
        }

        hexStr += '00'.repeat(sizeInBytes-byteLocation);

        return hexStr;
    }
}