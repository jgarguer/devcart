public class PCA_HomeCompController extends PCA_HomeController
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Methods to show home page 
    
    History:
    
    <Date>                    <Author>               <Description>
    10/06/2014              Jorge Longarela         Initial Version 
    29/06/2014              Alejandro Garcia        V2
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<ImageHomeWrapper> lImageHomeWrapper     {get; set;}
    
    /*-- DELOITTE STARTS --*/
    public boolean esTGS {get{
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            System.debug('###Current profile: ' + pName);
            return pName.startsWith('TGS');
        }
        return false;
    }
    set;}
    /*-- DELOITTE ENDS --*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Garcia
    Company:       Salesforce.com
    Description:   Wrapper class to manage url attachments
    
    History:
    
    <Date>                    <Author>               <Description>
    29/06/2014              Alejandro Garcia        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class ImageHomeWrapper
    {
        public Boolean externalURL          {get; set;}
        public BI_ImageHome__c ihome        {get; set;}
        public List<String> attachments     {get; set;}
        
        public ImageHomeWrapper(BI_ImageHome__c ihome, List<String> attachments)
        {
                this.ihome = ihome;
                this.attachments = attachments;
                this.externalURL = false;
                
                if (ihome.BI_Url__c != null)
                {
                    if ((ihome.BI_Url__c.startsWith('www.'))        ||
                        (ihome.BI_Url__c.startsWith('http:'))       ||
                        (ihome.BI_Url__c.startsWith('https:')))
                    {
                        this.externalURL = true;
                    }
                }
            
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Garcia
    Company:       Salesforce.com
    Description:   Wrapper class to manage url attachments
    
    History:
    
    <Date>                    <Author>               <Description>
    29/06/2014              Alejandro Garcia        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_HomeCompController()
    {
        
        enviarALoginComm();
        
            Id uAcc;
            
            //User currentUser = [SELECT AccountId, Country_Region__c, Pais__c FROM User WHERE Id = :UserInfo.getUserId()];
            //String currentUserRegion;
            String currentUserCountry;
            Id currentAcc = BI_AccountHelper.getCurrentAccountId();
            
            if(currentAcc != null){
               Account AccHome = [SELECT Id,BI_Country__c,/*Region__r.Name,*/ BI_Segment__c FROM Account WHERE Id = :currentAcc LIMIT 1];
    
            /*  JLA_Refactorizacion_Necesaria
        Id ImagenInicioPP;
            if(AccHome!=null){
                ImagenInicioPP = AccHome.BI_Imagen_Inicio_PP__c;
            }*/

            //if(AccHome.Region__r.Name != null){
                //currentUserRegion = AccHome.Region__r.Name;
            //}
            if(AccHome.BI_Country__c != null){
                currentUserCountry = AccHome.BI_Country__c;
            }
            
            
            /*else if(currentUser.Country_Region__c != null ){
                currentUserRegion = currentUser.Country_Region__c.toLowerCase();
                uAcc = currentUser.AccountId;
            }*/
            //Account accountUser = [SELECT Id, BI_Segmento_Ref__r.Name FROM Account WHERE Id =:uAcc limit 1];
            //Account accountUser = BI_AccountHelper.getCurrentAccount(uAcc);

            //system.debug('accountUser***: ' +accountUser);
            string accSeg = AccHome.BI_Segment__c;
                
            
            if(accSeg == null){
                accSeg = 'Empresas';
            }
            
            transient List<BI_ImageHome__c> lImageHome = new List<BI_ImageHome__c>(); //BI_Segmento2__c
            Set<Id> IdsClientes = new Set<Id>();
            List<BI_Cliente_PP__c> ClientPP = [Select Id, Name, BI_Activo__c, BI_Cliente__c, BI_Imagen_inicio_PP__c, BI_Country_2__c, BI_Segment_2__c FROM BI_Cliente_PP__c where BI_Cliente__c =: currentAcc AND BI_Activo__c = true AND (BI_Country_2__c <> null OR BI_Segment_2__c <> null)];
            for(BI_Cliente_PP__c Client :ClientPP){
                IdsClientes.add(Client.BI_Imagen_inicio_PP__c);
            }
            lImageHome = [SELECT BI_Fecha_Fin__c, BI_Fecha_Inicio__c, BI_Segment__c, BI_Country__c, BI_Url__c 
                          FROM BI_ImageHome__c WHERE ((((BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c = :accSeg OR BI_Segment__c= null) AND (BI_Country__c <> null OR BI_Segment__c <> null)) OR Id =:IdsClientes) AND RecordType.Name='Slider' AND BI_Activo_c__c = true) ORDER BY BI_Fecha_Inicio__c ASC];
             
/********* JLA_Refactorizacion_Necesaria
            if(ImagenInicioPP!=null){
                lImageHome = [SELECT BI_Fecha_Fin__c, BI_Fecha_Inicio__c, BI_Segmento2__c, BI_Pais_ref__c, BI_Url__c 
                          FROM BI_ImageHome__c WHERE ((BI_Pais_ref__r.Name =: currentUserCountry OR BI_Pais_ref__c = null) AND (BI_Segmento2__r.Name =: accSeg OR BI_Segmento2__c= null)) OR Id =: ImagenInicioPP ORDER BY BI_Fecha_Inicio__c ASC];
             }else{
                lImageHome = [SELECT BI_Fecha_Fin__c, BI_Fecha_Inicio__c, BI_Segmento2__c, BI_Pais_ref__c, BI_Url__c 
                          FROM BI_ImageHome__c WHERE (BI_Pais_ref__r.Name =: currentUserCountry OR BI_Pais_ref__c = null) AND (BI_Segmento2__r.Name =: accSeg OR BI_Segmento2__c= null) ORDER BY BI_Fecha_Inicio__c ASC];
             }
*/
            if (lImageHome.size() > 0)
            {
                transient List<Id> idImageHome = new List<Id>();
                for (BI_ImageHome__c el : lImageHome) {idImageHome.add(el.Id);}
                
                transient List<Attachment> lAttachments = new List<Attachment>();
                lAttachments = [SELECT Id, parentId FROM Attachment WHERE parentId IN :idImageHome];
                
                transient Map<Id, List<String>> MAttachments = new Map<Id, List<String>>();
                
                for (Attachment item : lAttachments)
                {
                    transient List<String> lAux = new List<String>();
                    
                    if (MAttachments.containsKey(item.parentId))
                    {
                        lAux = MAttachments.get(item.parentId);
                    }
                    
                    lAux.add(item.Id);
                    MAttachments.put(item.parentId, lAux);
                }
                
                this.lImageHomeWrapper = new List<ImageHomeWrapper>();
                                
                for (BI_ImageHome__c bImage : lImageHome)
                {
                    if (MAttachments.containsKey(bImage.Id))
                    {
                        if ((bImage.BI_Fecha_Inicio__c <= System.today())   &&
                            ((bImage.BI_Fecha_Fin__c == null) || (bImage.BI_Fecha_Fin__c >= System.today()))) 
                        {
                            ImageHomeWrapper newIH = new ImageHomeWrapper(bImage, MAttachments.get(bImage.Id));
                            this.lImageHomeWrapper.add(newIH);
                        }
                    }
                
            }
            }
        }
    }
    
    
    
}