global class BI_Case_JOB implements Schedulable {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:
    
    IN:				Scheduled class that updates Case fields. 
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Ignacio Llorca      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    global Id idCase;
    private Integer MAX_RECORDS = 250;

    final static Map<String,BusinessHours> MAP_NAME_BUSINESSHOURS = new Map<String,BusinessHours>();
    
    final static BusinessHours BUSINESSHOURS_DEFAULT;

    final static Set <String> SET_SKIPED_STATUS = new Set <String>{Label.BI_CaseStatus_Cancelled, Label.BI_CaseStatus_Closed, Label.BI_CaseStatus_Returned,  Label.BI_CaseStatus_Rejected };

    final static Set <String> SET_ALLOWED_RT = new Set <String>{'BI_Caso_Comercial_Abierto','BI_Caso_Comercial_Cerrado','Caso_eHelp','BI_Caso_Interno','BI_CasoTecnico','BI_Caso_Tecnico_Ecuador'};
    

    static{
       for(BusinessHours schedule :[SELECT CreatedById,CreatedDate,FridayEndTime,FridayStartTime,Id,IsActive,IsDefault,LastModifiedById,LastModifiedDate,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,SystemModstamp,ThursdayEndTime,ThursdayStartTime,TimeZoneSidKey,TuesdayEndTime,TuesdayStartTime,WednesdayEndTime,WednesdayStartTime FROM BusinessHours]){
            MAP_NAME_BUSINESSHOURS.put(schedule.Name, schedule);
            if(schedule.IsDefault){
                BUSINESSHOURS_DEFAULT = schedule;
                system.debug('BUSINESSHOURS_DEFAULT : ' + schedule.Name);
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Constructor method
    
    IN:            Case Id

    <Date>            <Author>          <Description>
    25/04/2014        Ignacio Llorca       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_Case_JOB(Id idjob){
            idCase = idjob;
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that executes the job, and checks the Status of a case to fill the days it's been opened.
    
    IN:            Schedulable context (Parameters to schedule the job)
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Ignacio Llorca       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sch) {
        try{
            system.abortJob(sch.getTriggerId());
    		//Set <String> set_status = new Set <String> {Label.BI_CaseStatus_Cancelled, Label.BI_CaseStatus_Closed, Label.BI_CaseStatus_Returned,  Label.BI_CaseStatus_Rejected };
    		//List <BusinessHours> lst_hours = [SELECT FridayEndTime,FridayStartTime,Id,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,
      //                                SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,
      //                                TuesdayStartTime,WednesdayEndTime,WednesdayStartTime FROM BusinessHours WHERE Name like 'Horario Oficina%' limit 1];
            List<Case> case_lst = new List <Case>();
            
            if(Test.isRunningTest()){MAX_RECORDS = 1;}
            
            if (idCase==null){
                case_lst = [SELECT Id, Status, BI_Tiempo_neto_de_IC_dias__c, CreatedDate, ClosedDate, BI_Codigo_ISO__c, RecordType.DeveloperName FROM Case WHERE Status NOT IN :SET_SKIPED_STATUS AND RecordType.DeveloperName IN :SET_ALLOWED_RT ORDER BY Id ASC limit :MAX_RECORDS];
            }else{
                case_lst = [SELECT Id, Status, BI_Tiempo_neto_de_IC_dias__c, CreatedDate, ClosedDate, BI_Codigo_ISO__c, RecordType.DeveloperName FROM Case WHERE Id > :idCase AND Status NOT IN :SET_SKIPED_STATUS AND RecordType.DeveloperName IN :SET_ALLOWED_RT ORDER BY Id ASC limit :MAX_RECORDS];
            }
            system.debug('SIZE OF case_lst : '+case_lst.size());

            if(!case_lst.isEmpty()){
        		for (Case cases:case_lst){	
                        BusinessHours bh_for_this_case = ( cases.RecordType.DeveloperName != 'Caso_eHelp' && MAP_NAME_BUSINESSHOURS.containsKey('Horario oficina ' + cases.BI_Codigo_ISO__c ))?MAP_NAME_BUSINESSHOURS.get('Horario oficina ' + cases.BI_Codigo_ISO__c ):BUSINESSHOURS_DEFAULT;

                        if(cases.ClosedDate==null){
                            //cases.BI_Tiempo_neto_de_IC_dias__c =  daysWorkedJOB(cases.CreatedDate, Datetime.now(), 'days', lst_hours) / 24;	
                            cases.BI_Tiempo_neto_de_IC_dias__c =  getBusinessHoursDiff(cases.CreatedDate, Datetime.now(), 'hours', bh_for_this_case);
                            idCase=cases.Id;
                        }else{
                            //cases.BI_Tiempo_neto_de_IC_dias__c =  daysWorkedJOB(cases.CreatedDate, cases.ClosedDate, 'days', lst_hours) /24;
                            cases.BI_Tiempo_neto_de_IC_dias__c =  getBusinessHoursDiff(cases.CreatedDate, cases.ClosedDate, 'hours', bh_for_this_case);  
                            idCase=cases.Id;
                        }
                        		
        		}

                //update case_lst;

                try{
                    if (Test.isRunningTest()) case_lst[0].Subject = ''; case_lst[0].BI_Nombre_de_la_Oportunidad__c = null;

                    List<Database.SaveResult> srList = Database.update(case_lst, false);

                    List<BI_Log__c> lst_err = new List<BI_Log__c>();
                    // Iterate through each returned result
                    for (Database.SaveResult sr : srList) {
                        if (sr.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully updated case with ID: ' + sr.getId());
                        }else{
                            // Operation failed, so get all errors                
                            for(Database.Error err : sr.getErrors()) {

                                BI_Log__c log = new BI_Log__c(BI_Asunto__c = 'BI_Case_JOB.execute()',
                                            BI_Bloque_funcional__c = 'BI_EN',
                                            BI_Descripcion__c = 'ID: ' + sr.getId() +'-' + err.getStatusCode() + ': ' + err.getMessage() + ' - Fields that affected this error: ' + err.getFields(),
                                            BI_Tipo_de_componente__c = 'Apex Job');
                                               
                                system.debug('GENERATE_LOG ==> '+log);
                            }
                        }
                    }
                }catch (Exception Exc){
                    BI_LogHelper.generate_BILog('BI_Case_JOB.BI_Case_JOB on Database.SaveResult', 'BI_EN', Exc, 'Apex Job');
                }

        		
                if(case_lst.size() == MAX_RECORDS|| (Test.isRunningTest() && case_lst.size() == 1)){
                    BI_Jobs.updateCases(idCase, false);
                }else if (case_lst.size()<MAX_RECORDS || (Test.isRunningTest() && case_lst.size() == 0)){
                    BI_Jobs.updateCases(idCase, true);
                }
            }else{
                 BI_Jobs.updateCases(null, true);
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_Case_JOB.BI_Case_JOB', 'BI_EN', Exc, 'Apex Job');
        }
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Alternative method to daysWorkedJOB that return decimals and check holidays in businessHours
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    10/03/2015                     Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Decimal getBusinessHoursDiff(Datetime sdate, Datetime edate, String option, BusinessHours HOURS){

        try{
            if(HOURS != null){
                system.debug('HOURS : ' + HOURS);

                Long miliseconds = BusinessHours.diff(HOURS.Id, sdate, edate);
                Decimal thousand = 1000.00;
                Decimal sixty = 60.00;
                Decimal twenty_four = 24.00;



                Decimal ret_result;
                if(option != null){
                    if(option.toLowerCase() == 'seconds'){
                        ret_result = (miliseconds/thousand);
                    }else if(option.toLowerCase() == 'minutes'){
                        ret_result = ((miliseconds/thousand)/sixty);
                    }else if(option.toLowerCase() == 'hours'){
                        ret_result = ( ((miliseconds/thousand)/sixty)/sixty);
                    }else if(option.toLowerCase() == 'days'){
                        ret_result = ( (((miliseconds/thousand)/sixty)/sixty)/twenty_four);
                    }else{ //miliseconds
                        ret_result = (miliseconds);
                    }
                    return ret_result.setScale(2);
                }
                ret_result = decimal.valueOf(miliseconds);
                return ret_result.setScale(2);
            }else{
                 BI_LogHelper.generateLog('BI_CaseMethods.getBusinessHoursDiff', 'BI_EN', 'Parameter HOURS type (List<BusinessHours>) is empty.', 'Trigger');
                 return null;
            }
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_CaseMethods.getBusinessHoursDiff', 'BI_EN', Exc, 'Trigger');
            return null;
        }
    }

}