@isTest
private class BI_Descuento_TEST {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_Bolsa_de_Dinero__c and BI_Transaccion_bolsa_dinero classes
    
    History: 
    <Date> 					<Author> 				<Change Description>
    16/04/2014      		Ignacio Llorca    		Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Pablo Oliva
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_Bolsa_de_Dinero__c and BI_Transaccion_bolsa_dinero
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	16/04/2014      		Ignacio Llorca    		Initial Version	
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/		    

	static testMethod void descuentotest() {    	
	   
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
	    List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);	

		List <BI_Descuento__c> lst_desc = new List <BI_Descuento__c>();     

        for (Integer i=0;i<200;i++){
	        BI_Descuento__c disc = new BI_Descuento__c(BI_Cliente__c = lst_acc[0].Id);
	        lst_desc.add(disc);
    	}
    	insert lst_desc;

    	Test.startTest();

        List <BI_Descuento_para_modelo__c> lst_mod = new List <BI_Descuento_para_modelo__c>();
    	for (Integer i=0;i<200;i++){
    		BI_Descuento_para_modelo__c mod = new BI_Descuento_para_modelo__c(BI_Descuento__c = lst_desc[0].Id);
    		lst_mod.add(mod);
    	}
    	insert lst_mod;    	

    	for (Integer i=0;i<50;i++){
            lst_desc[i].BI_Estado_del_Proceso__c = 'Cancelado';
    	}
    	update lst_desc;

    	for (Integer i=0;i<50;i++){
    		lst_desc[i].BI_Fecha_de_inicio__c = date.today();
    	} 	

    	try{
    		update lst_desc;
    	}catch(exception e){
    		system.assert(e.getMessage().contains('No es posible modificar los descuentos en estado cancelado'));
    	}
    	for(BI_Descuento_para_modelo__c mod:lst_mod){
    		mod.BI_Cantidad__c = 3;
    	}
    	try{
    		update lst_mod;
    	}catch(exception e){
    		system.assert(e.getMessage().contains('No se puede modificar el descuento para modelo en el estado actual del descuento'));
    	}

        try{
            delete lst_mod;
        }catch(exception e){
            system.assert(e.getMessage().contains('No se puede borrar el descuento para modelo en el estado actual del descuento'));
        }

    	BI_DescuentoModeloMethods.blockEdition(null);

    	Test.stopTest();
        
    }	

    static testMethod void updateacc_test() {        

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(2, lst_pais); 

        List <BI_Descuento__c> lst_desc = new List <BI_Descuento__c>();     

        for (Integer i=0;i<50;i++){
            BI_Descuento__c disc = new BI_Descuento__c(BI_Cliente__c = lst_acc[0].Id);
            lst_desc.add(disc);
        }
        insert lst_desc;

        Test.startTest();

        List <BI_Descuento_para_modelo__c> lst_mod = new List <BI_Descuento_para_modelo__c>();
        for (Integer i=0;i<50;i++){
            BI_Descuento_para_modelo__c mod = new BI_Descuento_para_modelo__c(BI_Descuento__c = lst_desc[0].Id);
            lst_mod.add(mod);
        }
        insert lst_mod;     

        for (Integer i=0;i<50;i++){
            lst_desc[i].BI_Estado_del_Proceso__c = 'Aprobado';
            lst_desc[i].BI_Cliente__c = lst_acc[1].Id; //for updateAcc method

        }    
        update lst_desc;

        list<BI_Descuento_para_modelo__c> lst_assert_dm = [SELECT Id, BI_Descuento__c, BI_Cliente_aux__c 
                                                            FROM BI_Descuento_para_modelo__c 
                                                            WHERE BI_Descuento__c = :lst_desc[0].Id
                                                           ];

        system.debug('assert BI_Descuento_para_modelo__c: ' + lst_assert_dm);
        for (Integer i=0;i<50;i++)
            system.assertEquals( lst_assert_dm[i].BI_Cliente_aux__c, lst_acc[1].Name);

        Test.stopTest();
    }

/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}