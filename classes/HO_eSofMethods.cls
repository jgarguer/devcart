/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Iñaki Frial
    Company:       Accenture
    Description:   Methods executed by e Sof Triggers 
    Test Class:    HO_eSofMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    06/03/2018              Iñaki Frial             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class HO_eSofMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Iñaki Frial
	Company:       Accenture
	Description:   Method that updates the status of the service unit if all of its e Sofs are closed
    
    History: 
    
	<Date>                          <Author>                    <Change Description>
	06/03/2018                     Iñaki Frial                  Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateServiceUnitsStage(List<HO_e_SOF__c> news, Map<Id, HO_e_SOF__c> olds){
		List <HO_e_SOF__c> lst_eSofs = new List <HO_e_SOF__c> ();
		Set<Id> lst_SerUnit = new Set<Id>();
		List<HO_Service_Unit__c> lst_auxSerUnit= new List <HO_Service_Unit__c>();
		List<HO_Service_Unit__c> set_toProcess= new List<HO_Service_Unit__c>();
		Boolean cambiarEstado;
		for (HO_e_SOF__c eSof:news){
			if(eSof.HO_Status__c!= olds.get(eSof.ID).HO_Status__c){
				lst_eSofs.add(eSof);
				lst_SerUnit.add(eSof.HO_Service_Unit__c);
				system.debug('Lista e SoFs ' + lst_eSofs);
			}	
		}
		if(!lst_SerUnit.isEmpty()){
			lst_auxSerUnit= [Select id, HO_Status__c, (Select id, HO_Status__c from e_SOFs__r) from HO_Service_Unit__c where id IN: lst_SerUnit];
		}
		if(!lst_auxSerUnit.isEmpty()){
			for(HO_Service_Unit__c serU: lst_auxSerUnit){
				cambiarEstado=true;
				for(HO_e_SOF__c eSof: serU.e_SOFs__r){
					if(eSof.HO_Status__c!='Accepted' && eSof.HO_Status__c!='Cancelled'){
							cambiarEstado=false;
							return;
						}
				}
				if(cambiarEstado){
					serU.HO_Status__c= 'Pending Service Processing';
					set_toProcess.add(serU);
				}
			}
		}
		if(!set_toProcess.isEmpty()){
			update set_toProcess;
		}
	}
}