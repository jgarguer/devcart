/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Julio Asenjo
Company:       Everis
Description:   Class to invoke catalog in lightning 

History:

<Date>            <Author>              <Description>
06/03/2016        Julio Asenjo          Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


public without sharing class CWP_MarketPlace_controller {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Julio Asenjo
Company:       Everis
Description:  Method to Load Inittial information 

History:

<Date>            <Author>              <Description>
06/03/2016        Julio Asenjo          Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static map<string,string> iniciaComponente (){
        Map <String,String> ret= new Map <String,String>();
        String url;
        List<Market_Place_PP__c> marketPlaceFamilies;
        List<BI_Catalogo_PP__c> products;
       
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            Id accId = BI_AccountHelper.getCurrentAccountId();
            if(getAccountCountry(accId)!=null){
                Boolean esTGS=false; 
                Id pId = UserInfo.getProfileId();
                Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
                if (p.size()>0){
                    String pName = p[0].Name;
                    esTGS= pName.startsWith('TGS');
                }else 
                    esTGS= false;
                if(esTGS){
                     marketPlaceFamilies = [SELECT BI_Descripcion__c,BI_Link_icono__c, BI_Link_imagen_slider__c, BI_Link_imagen__c,BI_Country__c,BI_Posicion__c,BI_Subtitulo__c,BI_Texto__c,BI_Titulo__c,Id,Name FROM Market_Place_PP__c where CWP_esTGS__c=true  order By BI_Posicion__c limit 4];
                } else {
                     marketPlaceFamilies = [SELECT BI_Descripcion__c,BI_Link_icono__c, BI_Link_imagen_slider__c, BI_Link_imagen__c,BI_Country__c,BI_Posicion__c,BI_Subtitulo__c,BI_Texto__c,BI_Titulo__c,Id,Name  FROM Market_Place_PP__c where BI_Country__c =:getAccountCountry(accId) ORDER BY BI_Posicion__c limit 4];
                }
                system.debug('getAccountCountry(accId): ' + getAccountCountry(accId));
                system.debug('marketPlaceFamilies: ' + marketPlaceFamilies);
               
                ret.put('marketPlaceFamilies', JSON.serialize(marketPlaceFamilies));
                List<Id> familyIds = new List<Id>();
                for(Market_Place_PP__c item : marketPlaceFamilies){
                    familyIds.add(item.Id);
                }
                products =[SELECT BI_Familia_del_producto__c, Id, Name, BI_Description__c, BI_Imagen_del_producto__c, BI_Texto_de_Introduccion__c, 
                     BI_Titulo_de_seccion_P1__c, BI_Texto_descriptivo_P1__c,
                     BI_Link_icono_1__c, BI_Titulo_icono_1__c, BI_Texto_icono_1__c,
                     BI_Link_icono_2__c, BI_Titulo_icono_2__c, BI_Texto_icono_2__c, 
                     BI_Link_icono_3__c, BI_Titulo_icono_3__c, BI_Texto_icono_3__c, 
                     BI_Link_icono_4__c, BI_Titulo_icono_4__c, BI_Texto_icono_4__c,
                     BI_Titulo_de_seccion_P2__c, BI_Texto_descriptivo_P2__c
                     FROM BI_Catalogo_PP__c where BI_Familia_del_producto__c in :familyIds];
                ret.put('products', JSON.serialize(products));
            }
            
            String currencyCode = getCurrencyISOCode(accId);
            url = 'apex/NE__NewConfiguration?accId='+accId+'&servAccId='+accId+'&billAccId='+accId+'&createOpty=true&cType=New&cSType=Rápido&confCurrency='+currencyCode;
            system.debug('url: '+url);
            ret.put('url', url);
            
            return ret;
       
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Ana Escrich
Company:       Salesforce.com
Description:   This method retrieves the ISOCode of the current account

History:

<Date>            <Author>              <Description>
22/07/2014        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static string getCurrencyISOCode(Id accId){
        
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            if(accId!=null){
                List<Account> accounts = [select Id, CurrencyIsoCode from Account where Id = :accId];
                if(!accounts.isEmpty()){
                    return accounts[0].CurrencyIsoCode;
                }
            }
            return null;
        
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Ana Escrich
Company:       Salesforce.com
Description:   This method retrieves the Country of the current account

History:

<Date>            <Author>              <Description>
22/07/2014        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static string getAccountCountry(Id accId){
      
           
            if(accId!=null){
                List<Account> accounts = [select Id, CurrencyIsoCode, BI_Country__c from Account where Id = :accId];
                system.debug('accountsCatalogo: '+accounts);
                if(!accounts.isEmpty()){
                    return accounts[0].BI_Country__c;
                }
            }
            return null;
        
    }
    
}