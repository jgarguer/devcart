/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Custom controller for the FOCO Delete Error Logs page
	Test Class:     BI_FOCODeleteErrorLogs_TEST
	History
	<Date>          <Author>           <Change Description>
	20-Feb-2015     Virgilio Utrera    Initial Version
	23-Feb-2015		Virgilio Utrera    Modified deleteFOCOErrorLog method
	24-Feb-2015		Virgilio Utrera    Added goBack method
-------------------------------------------------------------------*/

public class BI_FOCODeleteErrorLogs_CTRL {

	public Boolean showPicklist { get; set; }
	public List<SelectOption> errorLogIdOptions { get; set; }
	public String selectedValue { get; set; }

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Class constructor
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	public BI_FOCODeleteErrorLogs_CTRL() {
		getErrorLogIds();
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Calls a batch process that deletes the selected FOCO error log
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
		23-Feb-2015		Virgilio Utrera    Added logic for exception handling
	-------------------------------------------------------------------*/

	public PageReference deleteFOCOErrorLog() {
		Boolean scheduledForDeletion;

		// Check if there is an error log selected for deletion
		if(selectedValue != null && selectedValue.length() > 0) {
			try {
				// Call batch process to delete all error log records from selected FOCO data load
				scheduledForDeletion = BI_FOCOUtil.deleteErrorLog(selectedValue);

				// Show feedback message to the user
				if(scheduledForDeletion)
					ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Se está eliminando el log de errores "' + selectedValue + '". Al finalizar el proceso recibirá un correo electrónico de confirmación.'));
				else
					ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'El log de errores "' + selectedValue + '" ya se encuentra en proceso de eliminación, o bien ya fue eliminado.'));	
			}
			catch(Exception e) {
				ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Ha ocurrido un error al intentar eliminar el log de errores ' + selectedValue));
				ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
				ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString()));
			}
		}
		else {
			ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'No se ha seleccionado un log de errores para eliminar'));
		}

		// Refresh list of error log Ids
		getErrorLogIds();

		return null;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Navigates to the Registro Datos FOCO Error Log tab
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		24-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/
	public PageReference goBack() {
		PageReference ref = new PageReference('/' + BI_Registro_Datos_FOCO_Error_Log__c.sObjectType.getDescribe().getKeyPrefix());
    	return ref;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets the list of FOCO data load Ids available
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial Version
		23-Feb-2015		Virgilio Utrera    Refined how error log Ids picklist is populated
	-------------------------------------------------------------------*/

	private void getErrorLogIds() {
		Boolean addLogId;
		Map<String, BI_FOCO_Available_Error_Logs__c> errorLogIds = new Map<String, BI_FOCO_Available_Error_Logs__c>();
		Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c> errorLogsPendingDeletion = new Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c>();

		errorLogIdOptions = new List<SelectOption>();
		showPicklist = false;

		// Get error log Ids
		errorLogIds = BI_FOCOUtil.getErrorLogIds();

		// Populate and show the error log Ids picklist if there is at least one error log Id
		if(errorLogIds.size() > 0) {
			// Get error log Ids that are pending for deletion
			errorLogsPendingDeletion = BI_FOCOUtil.getErrorLogsPendingDeletion();

			// Populate the picklist with error log Ids, do not include those who are already pending for deletion
			for(String logId : errorLogIds.keySet()) {
				addLogId = true;

				for(String logIdtoDelete : errorLogsPendingDeletion.keySet()) {
					if(logIdtoDelete == logId) {
						addLogId = false;
						break;
					}
				}

				if(addLogId)
					errorLogIdOptions.add(new SelectOption(logId, logId));
			}

			// Show the error log Ids picklist if there is at least one value
			if(errorLogIdOptions.size() > 0)
				showPicklist = true;
		}
	}
}