/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Jose Miguel Fierro
	 Company:       New Energy Aborda
	 Description:   Test class to manage the coverage code for TGS_CallOrder2AssetHandler class 
	 
	 History:
		<Date>                  <Author>                <Change Description>
		01/03/2016              Jose Miguelo Fierro     Initial Version
		07/10/2016				Sara Nuñez Moreno		Added param TGS_Casilla_Desarrollo__c
	 --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class TGS_CallOrder2AssetHandler_TEST {

	@isTest
	public static void test_callOrder2Asset_statusResolved_noAsset() {
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
		
		TGS_Dummy_Test_Data.dummyEndpointsTGS();
		
		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		
		System.runAs(portUsr) {
			insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
			
			NE__OrderItem__c oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			List<Case> cas = [SELECT Id, CaseNumber,Order__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].Type='New';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];*/
			
			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
		}      
	}
	
	/*-------------------------------------------------------------------------------------
	 06/08/2016  Humberto Nunes        Se coloco el NETriggerHelper.setTriggerFired('BI_Case'); para que no se ejecutara ya que da too many querys... 
	 07/10/2016  Sara Nuñez			   Añadido el parametro TGS_Casilla_Desarrollo__c 
	-------------------------------------------------------------------------------------*/
	@isTest
	public static void test_callOrder2Asset_statusResolved_Asset() 
	{

		NETriggerHelper.setTriggerFired('BI_Case');
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);

		TGS_Dummy_Test_Data.dummyEndpointsTGS();

		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		NE__OrderItem__c oi;
		List<Case> cas;
		
		System.runAs(portUsr) {
			insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
			
			oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			cas = [SELECT Id, CaseNumber, Order__c, Order__r.NE__Asset__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].Type='New';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];*/
		}

		System.runAs(portUsr) {
			// Pseudo-asset
			RecordType rt = [SELECT Id FROM RecordType WHERE Name LIKE '%Asset%'];
			NE__Order__c asset = new NE__Order__c(
				RecordTypeId = rt.Id,
				Case__c = cas[0].Id,
				NE__Asset__c = cas[0].Order__r.NE__Asset__c,
				NE__Asset_Configuration__c = cas[0].Order__r.NE__Asset__c
			);
			insert asset;
			insert new NE__OrderItem__c(
				NE__Account__c = oi.NE__Account__c,
				NE__Billing_Account_Asset_Item__c = oi.NE__Billing_Account_Asset_Item__c,
				NE__Service_Account_Asset_Item__c = oi.NE__Service_Account_Asset_Item__c,
				NE__OrderId__c = asset.Id,
				NE__ProdId__c = oi.NE__ProdId__c,
				NE__CatalogItem__c = oi.NE__CatalogItem__c,
				NE__Qty__c=1
			);
			Test.startTest();
			cas[0].Asset__c = asset.Id;
			update cas[0];
			
			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true;
			update cas[0];
			Test.stopTest();
		}
	}
	
	@isTest
	public static void test_callOrder2Asset_statusClosed() {
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
		
		TGS_Dummy_Test_Data.dummyEndpointsTGS();
		
		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		List<Case> cas;
		NE__OrderItem__c oi;
		
		System.runAs(portUsr) {
			TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        	if(userTGS.TGS_Is_BI_EN__c == true || userTGS.TGS_Is_TGS__c == false){
            	userTGS.TGS_Is_BI_EN__c = false;
            	userTGS.TGS_Is_TGS__c = true;
	
            	if(userTGS.Id != null){
                	update userTGS;
            	}
            	else{
                	insert userTGS;
            	}
        	}
        	Test.startTest();
			oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			
			cas = [SELECT Id, CaseNumber, Order__c, Order__r.NE__Asset__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].TGS_Status_Reason__c = 'In Provision';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			cas[0].Type='New';
			update cas[0];*/
			
			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			//update cas[0];
			// Pseudo-asset
			RecordType rt = [SELECT Id FROM RecordType WHERE Name LIKE '%Asset%'];
			NE__Order__c asset = new NE__Order__c(
				RecordTypeId = rt.Id,
				Case__c = cas[0].Id,
				NE__Asset__c = cas[0].Order__r.NE__Asset__c
			);
			insert asset;
			insert new NE__OrderItem__c(NE__Account__c = oi.NE__Account__c,
								 NE__Billing_Account_Asset_Item__c = oi.NE__Billing_Account_Asset_Item__c,
								 NE__Service_Account_Asset_Item__c = oi.NE__Service_Account_Asset_Item__c,
								 NE__OrderId__c = asset.Id,
								 NE__ProdId__c = oi.NE__ProdId__c,
								 NE__CatalogItem__c = oi.NE__CatalogItem__c,
								 NE__Qty__c=1);
			cas[0].Asset__c = asset.Id;
			update cas[0];
			Test.stopTest();
		}
		System.runAs(portUsr) {

			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			NETriggerHelper.setTriggerFired('TGS_Survey_Launcher_Handler'); // We are not interested in updating contacts right now
			//cas[0].Asset__c = cas[0].Order__c;
			cas[0].Status = 'Closed';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
		}
	}

	@isTest
	public static void test_callOrder2Asset_statusClosed_exception() {
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
		
		TGS_Dummy_Test_Data.dummyEndpointsTGS();
		
		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		List<Case> cas;
		NE__OrderItem__c oi;
		
		System.runAs(portUsr) {
			insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
			
			oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			cas = [SELECT Id, CaseNumber, Order__c, Order__r.NE__Asset__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].TGS_Status_Reason__c = 'In Provision';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			cas[0].Type='New';
			update cas[0];*/
			
			Test.startTest();
			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
			Test.stopTest();
		}
		System.runAs(portUsr) {

			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			NETriggerHelper.setTriggerFired('TGS_Survey_Launcher_Handler'); // We are not interested in updating contacts right now
			//cas[0].Asset__c = cas[0].Order__c;
			cas[0].Status = 'Closed';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
		}
	}
	
	@isTest
	public static void test_callOrder2Asset_statusReopened() {
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
		
		TGS_Dummy_Test_Data.dummyEndpointsTGS();
		
		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		
		System.runAs(portUsr) {
			insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
			
			NE__OrderItem__c oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			List<Case> cas = [SELECT Id, CaseNumber, Order__c, Order__r.NE__Asset__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			System.assertEquals(1, cas.size(), 'Multiple cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			cas[0].Type='New';
			update cas[0];*/
			
			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
			
			// Pseudo-asset
			/*RecordType rt = [SELECT Id FROM RecordType WHERE Name LIKE '%Asset%'];
			NE__Order__c asset = new NE__Order__c(
				RecordTypeId = rt.Id,
				Case__c = cas[0].Id,
				NE__Asset__c = cas[0].Order__r.NE__Asset__c
			);
			insert asset;
			cas[0].Asset__c = asset.Id;
			update cas[0];*/
			
			Test.startTest();
			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			//cas[0].Asset__c = cas[0].Order__c;
			cas[0].Status = 'Assigned';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016

			//update cas[0];
			Test.stopTest();
		}
	}
	
	@isTest
	public static void test_callOrder2Asset_statusCancelled() {
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
		
		TGS_Dummy_Test_Data.dummyEndpointsTGS();
		
		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		
		System.runAs(portUsr) {
			insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
			
			NE__OrderItem__c oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			List<Case> cas = [SELECT Id, CaseNumber, Order__c, Order__r.NE__Asset__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			cas[0].Type='New';
			update cas[0];*/
			

			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
			
			// Pseudo-asset
			/*RecordType rt = [SELECT Id FROM RecordType WHERE Name LIKE '%Asset%'];
			NE__Order__c asset = new NE__Order__c(
				RecordTypeId = rt.Id,
				Case__c = cas[0].Id,
				NE__Asset__c = cas[0].Order__r.NE__Asset__c
			);
			insert asset;
			cas[0].Asset__c = asset.Id;
			update cas[0];*/
			
			Test.startTest();
			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			cas[0].Asset__c = cas[0].Order__c;
			cas[0].Status = 'Cancelled';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
			Test.stopTest();
		}
	}
	
	@isTest
	public static void test_callOrder2Asset_statusCancelled_Modification() {
		NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
		
		TGS_Dummy_Test_Data.dummyEndpointsTGS();
		
		User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
		NE__OrderItem__c oi;
		List<Case> cas;
		
		System.runAs(portUsr) {
			insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
			
			oi = TGS_Dummy_Test_Data.dummyConfigurationInProgressInProv(UserInfo.getUserId(), 'New');
			cas = [SELECT Id, CaseNumber, Order__c, Order__r.NE__Asset__c, RecordTypeId, RecordType.Name, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :oi.Id)];
			
			System.assertNotEquals(0, cas.size(), 'No Cases were created for the order!');
			//System.assertNotEquals(null, cas[0].Type);
			
			System.debug('Case RecType: ' + cas[0].RecordType.Name);
			System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(), cas[0].RecordTypeId, cas[0].RecordType.Name);
			
			/*cas[0].Status = 'In Progress';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			cas[0].Type='Change';
			update cas[0];*/
			
			cas[0].Type='Change';
			cas[0].Status = 'Resolved';
			cas[0].TGS_Status_Reason__c = null;
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
			
		}

		// Pseudo-asset
		/*RecordType rt = [SELECT Id FROM RecordType WHERE Name LIKE '%Asset%'];
		NE__Order__c asset = new NE__Order__c(
			RecordTypeId = rt.Id,
			Case__c = cas[0].Id,
			NE__Asset__c = cas[0].Order__r.NE__Asset__c
		);
		insert asset;
		insert new NE__OrderItem__c(NE__Account__c = oi.NE__Account__c,
							 NE__Billing_Account_Asset_Item__c = oi.NE__Billing_Account_Asset_Item__c,
							 NE__Service_Account_Asset_Item__c = oi.NE__Service_Account_Asset_Item__c,
							 NE__OrderId__c = asset.Id,
							 NE__ProdId__c = oi.NE__ProdId__c,
							 NE__CatalogItem__c = oi.NE__CatalogItem__c,
							 NE__Qty__c=1);*/

		System.runAs(portUsr) {
			//cas[0].Asset__c = asset.Id;
			//update cas[0];
			
			Test.startTest();
			NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
			cas[0].Asset__c = cas[0].Order__c;
			cas[0].Status = 'Cancelled';
			cas[0].TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
			update cas[0];
			Test.stopTest();
		}
	}
}