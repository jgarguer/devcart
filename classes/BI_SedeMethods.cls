public without sharing class BI_SedeMethods {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Methods of BI_Sede trigger.

    History: 

    <Date>                  <Author>                <Change Description>
    22/12/2014              Micah Burgos            Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Method that update BI_Punto_de_instalacion's records to TRS if its BI_Sede record is updated.
                   BI_Sede__c = Direccion
                   BI_Punto_de_instalacion__c = Sede
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/12/2014              Micah Burgos            Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public static void updatePuntoInstalacionTRS(List<BI_Sede__c> lst_new){
        try{
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
            
            List<BI_Punto_de_instalacion__c> lst_puntoInstalacion_UPDATE = new List<BI_Punto_de_instalacion__c>();
            List<BI_Sede__c> lst_sedes = [SELECT Id, (SELECT Id FROM Puntos_de_instalacion__r) FROM  BI_Sede__c WHERE  Id IN: lst_new];

            for(BI_Sede__c sede :lst_sedes){
                for(BI_Punto_de_instalacion__c pInsta :sede.Puntos_de_instalacion__r){
                        lst_puntoInstalacion_UPDATE.add(pInsta);
                }
            }
            System.debug('lst_puntoInstalacion_UPDATE: ' +lst_puntoInstalacion_UPDATE);
            if(!lst_puntoInstalacion_UPDATE.isEmpty()){
                //this update calls to BI_COL_CreateBranchWS_cls.crearSucursalTelefonica(lst_puntoInstalacion,'Actualizar') for send TO TRS.
                update lst_puntoInstalacion_UPDATE;
            }

        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_SedeMethods.updatePuntoInstalacionTRS', 'BI_EN', Exc, 'Trigger');
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda
    Description:   Method that send a list of BI_Punto_de_instalacion__c to web service
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    05/02/2016              Antonio Masferrer       Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //public static void crearSedeTelefonica(List<BI_Sede__c> tnew ,List<BI_Sede__c> told, String accion){
    //    try{

    //        if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

    //        Set<Id> set_id = new Set<Id>();
            
    //        for(Integer i = 0 ; i < tnew.size() ; i++){
    //            if(tnew[i] != told[i]){
    //                set_id.add(tnew[i].Id);
    //            }
    //        }

    //        List<BI_Sede__c> lst_sedes = [SELECT Id, (SELECT Id FROM Puntos_de_instalacion__r) FROM  BI_Sede__c WHERE  Id IN: set_id];
    //        List<String> lst_puntoInstalacion = new List<String>();

    //        for(BI_Sede__c sede :lst_sedes){
    //            for(BI_Punto_de_instalacion__c pInsta :sede.Puntos_de_instalacion__r){
    //                    lst_puntoInstalacion.add(pInsta.Id);
    //            }
    //        }

    //        BI_COL_CreateBranchWS_cls.crearSucursalTelefonica(lst_puntoInstalacion,accion);

    //    }catch(Exception exc){
    //        BI_LogHelper.generate_BILog('BI_SedeMethods.crearSedeTelefonica', 'BI_EN', Exc, 'Trigger');
    //    }
    //}

    
   /*
    Author:        Fco. Javier Sanz
    Company:       NEAborda
    * Method name : updateUsershippingAddress
    * Description : Whenever a BI_Sede__c is updated and it belongs to a Primary('Comercial Principal') BI_Punto_de_instalacion__c
                    the client shipping address must be updated with the new BI_Sede__c data.
                     
    * Return Type : void
    * Parameter : trigger.new.map
     <Date>                 <Author>                <Change Description>
    15/05/2016              Fco.Javier Sanz            Initial Version  
    01/08/2016              Humberto Nunes              Se limito a 20 Caracteres el Rellenado del Codigo Postal en la direccion del Cliente. 
    17/09/2016              Alfonso Alvarez(AJAE)       Se corrige error en código de limite a 20 Caracteres el Rellenado del Codigo Postal en la direccion del Cliente. 
    */

    public static void updateUsershippingAddress (map<id,BI_Sede__c> mapNews){
    
        map<id,BI_Sede__c> mapAccountId_nuevaDireccion = new map<id,BI_Sede__c>();
        map<id,string> map_idAccount_direccionvalue = new map<id,string>();
        if (!mapNews.isEmpty()) {
                        
            for (BI_Punto_de_instalacion__c currentSedePrincipal:[select BI_Cliente__c,
                                        toLabel(BI_Sede__r.BI_Tipo_Via__c),
                                    id,BI_Sede__c from BI_Punto_de_instalacion__c 
                                        where BI_Sede__c IN :mapNews.keyset() 
                                        and BI_Tipo_de_sede__c = 'Comercial Principal']) {
                System.debug(currentSedePrincipal.BI_Sede__r.BI_Tipo_Via__c);
                if (mapNews.containskey(currentSedePrincipal.BI_Sede__c)){
                                
                    mapAccountId_nuevaDireccion.put(currentSedePrincipal.BI_Cliente__c,mapNews.get(currentSedePrincipal.BI_Sede__c));
                    map_idAccount_direccionvalue.put(currentSedePrincipal.BI_Cliente__c,currentSedePrincipal.BI_Sede__r.BI_Tipo_Via__c);
                    
                }
                
            }
            if (!mapAccountId_nuevaDireccion.isEmpty()) {
                
                list<Account> lst_AccountToUpdate = [select id,
                                                         ShippingCity,ShippingCountry,
                                                         ShippingLatitude,ShippingLongitude,
                                                         ShippingPostalCode,
                                                         ShippingState,ShippingStreet
                                                    from Account
                                                    where id IN :mapAccountId_nuevaDireccion.keyset()];
                
                String provincia,codigoP;
                String direccion,numero,piso,apartamento,via;

                for (Account accountToUpdate : lst_AccountToUpdate) {
                    
                    BI_Sede__c directionAccount = mapAccountId_nuevaDireccion.get(accountToUpdate.id); 
                    
                    accountToUpdate.ShippingCity = directionAccount.BI_Localidad__c;
                    accountToUpdate.ShippingCountry = directionAccount.BI_Country__c;
                    accountToUpdate.ShippingLatitude = (directionAccount.BI_Longitud__Latitude__s == null) ? 0 : directionAccount.BI_Longitud__Latitude__s;
                    accountToUpdate.ShippingLongitude = (directionAccount.BI_Longitud__Longitude__s == null) ? 0 : directionAccount.BI_Longitud__Longitude__s; 
                    // 17/09/2016  (AJAE)
                    //accountToUpdate.ShippingPostalCode = (directionAccount.BI_Distrito__c == null) ? '' : directionAccount.BI_Distrito__c.substring(0); 
                    accountToUpdate.ShippingPostalCode = (directionAccount.BI_Distrito__c == null) ? '' : ( (directionAccount.BI_Distrito__c.length() <20 ) ? directionAccount.BI_Distrito__c : directionAccount.BI_Distrito__c.substring(0,19));   
                    // FIN 17/09/2016 (AJAE)
                    provincia = (directionAccount.BI_Provincia__c == null) ? '' : directionAccount.BI_Provincia__c;
                    codigoP = (directionAccount.BI_Codigo_postal__c == null) ? '' : directionAccount.BI_Codigo_postal__c;

                    accountToUpdate.ShippingState = provincia +' '+ codigoP;

                    via = (map_idAccount_direccionvalue.get(accountToUpdate.id) == null) ? '' : map_idAccount_direccionvalue.get(accountToUpdate.id);
                    direccion = (directionAccount.BI_Direccion__c == null) ? '' : directionAccount.BI_Direccion__c;
                    numero = (directionAccount.BI_Numero__c == null) ? '' : directionAccount.BI_Numero__c;
                    piso = (directionAccount.BI_Piso__c == null) ? '' : directionAccount.BI_Piso__c;
                    apartamento = (directionAccount.BI_Apartamento__c == null) ? '' : directionAccount.BI_Apartamento__c;
                    
                    
                    accountToUpdate.ShippingStreet = via + ' ' +
                                                    direccion + ' ' +
                                                    numero  + ' ' +
                                                    piso  + ' ' +
                                                    apartamento  + ' ';
                        
                }
                if (!lst_AccountToUpdate.isEmpty()) {
                    update(lst_AccountToUpdate); 
                }
                
            }
            
        }
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Angelo Salamanca
    Company:       Everis Centers Temuco
    Description:   Method that updates the status to "Pendiente de validar" once a field is modified
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    09/02/2016              Angelo Salamanca       Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateStatus(List<BI_Sede__c> tnew ,List<BI_Sede__c> told){
        /*Variable Declaration*/
        Map<Id, BI_Sede__c> oldDataMap = new  Map<Id, BI_Sede__c>(told);
        
        for (BI_Sede__c item : tnew) {
            if(oldDataMap.get(item.Id).Estado__c == 'Validado' && oldDataMap.get(item.Id).BI_Country__c == 'Chile'){
                item.Estado__c = 'Pendiente de validar';
            }
        }
    }
}