/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for visualforce pages in charge of exporting results table as excel or pdf
    Test Class:    CWP_OrderITemListExcel_ctrl_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    23/02/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class CWP_Assignment_Rule{            
    //Fetching the assignment rules on case
    AssignmentRule AR = new AssignmentRule();
    
    /*
    Developer Everis
    Function: method used to retrieve an assignemnt rule for cases
    */           
    public AssignmentRule assignmentRule(){
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];  
        return AR;
                   
    }
}