@isTest
private class ADQ_DesagruparFacturasControllerMod_TEST {
	@isTest static void desagrupa_TEST (){
		
			ADQ_DesagruparFacturasControllerMod1 dfc = new ADQ_DesagruparFacturasControllerMod1 ();		
			

			//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
			Account acc01  = new Account ();
				acc01.Name = 'Cuenta de prueba JMO';
				acc01.BI_Segment__c = 'test';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				//acc01.Raz_n_social__c = 'Cuenta de prueba SA CV';
				acc01.BI_Country__c = Label.Bi_Mexico;
			insert acc01;

		Test.setCurrentPageReference(new PageReference('Page.ADQ_DefinicionFactura'));
		System.currentPageReference().getParameters().put('accid', acc01.Id);
			
			NE__Order__c NEORDER =  new NE__Order__c(); 
				NEORDER.NE__AccountId__c = acc01.Id;
			insert NEORDER;
			
			NE__OrderItem__c NEInsert  = new NE__OrderItem__c ();
				NEInsert.NE__OrderId__c = NEORDER.id; 
				NEInsert.NE__Qty__c = 1;
			insert NEInsert;
			
			NE__OrderItem__c NEInsert2  = new NE__OrderItem__c ();
				NEInsert2.NE__OrderId__c = NEORDER.id; 
				NEInsert2.CurrencyIsoCode = 'USD';
				NEInsert2.NE__OneTimeFeeOv__c = 100;
				NEInsert2.NE__RecurringChargeOv__c = 100;
				NEInsert2.Fecha_de_reasignaci_n_a_factura__c = date.today();
				NEInsert2.NE__Qty__c = 1;
			insert NEInsert2;
			
			NE__OrderItem__c NEInsert3  = new NE__OrderItem__c ();
				NEInsert3.NE__OrderId__c = NEORDER.id; 
				NEInsert3.CurrencyIsoCode = 'USD';
				NEInsert3.NE__OneTimeFeeOv__c = 100;
				NEInsert3.NE__RecurringChargeOv__c = 100;
				NEInsert3.NE__Qty__c = 1;
				NEInsert3.Fecha_de_reasignaci_n_a_factura__c = date.today();
			insert NEInsert3;
			
			Factura__c Factura = new Factura__c ();
				Factura.Activo__c = true;
				Factura.Cliente__c = acc01.Id;
				Factura.Fecha_Inicio__c = date.newinstance(2015, 2, 17);
				Factura.Fecha_Fin__c = Date.today() + 20;
				Factura.Inicio_de_Facturaci_n__c = Date.today();
				Factura.IVA__c = '16%';
				Factura.Sociedad_Facturadora__c = 'GTM';
				Factura.Requiere_Anexo__c = 'SI';
				Factura.CurrencyIsoCode = 'USD';
				Factura.Tipo_de_Factura__c = 'Anticipado'; 
				Factura.RecordTypeId =Schema.SObjectType.Factura__c.getRecordTypeInfosByName().get('Recurrente').getRecordTypeId();
			insert Factura;
			
				Factura.Sociedad_Facturadora__c = 'PCS';
			
			Map<String,Programacion__c> mapProgramacion  = new Map<String,Programacion__c> ();
			Programacion__c programacion = new Programacion__c ();
				programacion.Factura__c = Factura.Id;
				programacion.CurrencyIsoCode = 'MXN';
				programacion.Sociedad_Facturadora_Real__c = 'GTM';
				programacion.Facturado__c = false;
				programacion.Fecha__c = Date.today();
				programacion.Inicio_del_per_odo__c = date.newinstance(2015, 2, 17);
				programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacion.Numero_de_factura_SD__c = null;
				programacion.Folio_fiscal__c = null;
				programacion.Borrar_por_agrupacion__c = true;
				programacion.Exportado__c = false;
				programacion.Bloqueo_por_agrupaci_n__c = false;
				programacion.Bloqueo_por_facturaci_n__c = false;
				programacion.Agrupaci_n__c = true;
							
			insert programacion;
			
			mapProgramacion.put(programacion.Id,programacion);
			
			Pedido_PE__c  p = new Pedido_PE__c ();
                p.Producto_en_Sitio__c = NEInsert2.Id;
                p.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p.Precio_original__c = 3999.98;
                p.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p.Precio_convertido__c = 8990.998;
                p.Inicio_del_per_odo__c = mapProgramacion.get(programacion.Id).Inicio_del_per_odo__c;
                p.Fin_del_per_odo__c = mapProgramacion.get(programacion.Id).Fin_del_per_odo__c;
    			p.Lleva_IEPS__c = false; 
                p.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
	        insert p;
	        
	         Factura_OLI__c f = new Factura_OLI__c();
                f.Factura__c = Factura.Id;
                f.Cantidad__c = 1;
                f.CurrencyIsoCode = 'USD';
                f.Descuento__c = 0.2 * 100;
                f.Plazo__c = 36;  
                f.Precio_Unitario__c = 800.80;
                f.Configuration_Item__c = NEInsert3.Id;
                
          	insert f;


			Apexpages.currentPage().getParameters().put('Fid', Factura.Id);
			ADQ_DesagruparFacturasControllerMod1 clsvalida = new ADQ_DesagruparFacturasControllerMod1 ();

			Factura__c encabezado = new Factura__c();
			encabezado.Activo__c = true;
			encabezado.Fecha_Fin__c = System.today().addMonths(12);
			encabezado.Fecha_Inicio__c = System.today();
			insert encabezado;
			
			Programacion__c progra = new Programacion__c ();
			progra.Fecha__c = System.today();
			progra.Inicio_del_per_odo__c = System.today();
			progra.Fin_del_per_odo__c = System.today().addYears(1);
			progra.Factura__c =encabezado.id;
			insert progra;

			Registro_de_agrupacion__c  rda = new Registro_de_agrupacion__c(Programaci_n_Padre__c = programacion.Id, Programaci_n_Precedente__c = progra.Id);
			insert rda;
			
			Boolean selected = true;
			
			ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion wrapper1 = new ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion(programacion,true);
			ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion wrapper2 = new ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion(programacion,false);

			List<ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion> listWrapper = new List<ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion>();
			listWrapper.add(wrapper1);
			listWrapper.add(wrapper2);

			clsvalida.listaWrapperProgramaciones = listWrapper;

			clsvalida.idFactura = Apexpages.currentPage().getParameters().get('Fid');
			clsvalida.getListaWrapperProgramaciones();
			clsvalida.desagrupar();
			clsvalida.consulta();
			clsvalida.eliminaPE();
			clsvalida.DelPedido();
			clsvalida.cancelar();
			clsvalida.idFactura = Apexpages.currentPage().getParameters().get('Fid');
		
	}
}