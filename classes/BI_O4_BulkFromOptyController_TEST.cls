@isTest
public class BI_O4_BulkFromOptyController_TEST {

    static ID oppPaId;
    static List<String> orderList=new List<String>();

/*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Data load for tests
    IN:
    OUT:        
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    @testSetup 
    static void startUp(){
        System.runAs(new User(Id = UserInfo.getUserId())) {
            String strRecordType = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Quote');

            Id recordtypeCustCtry = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY); 
            Account accCountry =   new Account(name = 'TestAccountCountry' , RecordTypeId=recordtypeCustCtry,BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
            insert accCountry;
             
            Id recordtypeAccId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
            Account acc = new Account(Name='Account', ParentId = accCountry.Id,RecordTypeId=recordtypeAccId,BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
             insert acc;
             
            //Create contact and accounts 
             Contact contact1 = new Contact(
                 FirstName = 'Test',
                 Lastname = 'Test',
                 AccountId = acc.Id,
                 Email = System.now().millisecond() + 'test@test.com'
             );
             insert contact1;
             
             List<Account> lstAccount = BI_O4_DisconnectionMethods_TEST.loadAccountsWithOtherRT(1, new String[]{'Spain'});
             Opportunity oppPa = new Opportunity(Name = 'Test1',
                                        CloseDate = Date.today(),
                                        StageName = Label.BI_F5DefSolucion, //Manuel Medina
                                        AccountId = lstAccount[0].Id,
                                        BI_Opportunity_Type__c = 'Global Distributed',
                                        BI_Ciclo_ventas__c = Label.BI_Completo,
                                        BI_Country__c = 'Spain',
                                        BI_O4_Countries_EMEA__c = 'Spain');
            insert oppPa;

            oppPaId=oppPa.id;

             Opportunity opp = new Opportunity(Name = 'Test',
                                        CloseDate = Date.today(),
                                        StageName = Label.BI_F5DefSolucion, //Manuel Medina
                                        AccountId = lstAccount[0].Id,
                                        BI_Opportunity_Type__c = 'Global Distributed',
                                        BI_Ciclo_ventas__c = Label.BI_Completo,
                                        BI_Country__c = 'Spain',
                                        BI_O4_Countries_EMEA__c = 'Spain',
                                        BI_Oportunidad_Padre__c=oppPA.Id);
            insert opp;

            System.debug('TEST-- Opportunity Parent'+opp.BI_Oportunidad_Padre__c);
            NE__Contract_Header__c contractHead = new NE__Contract_Header__c(NE__Name__c='Contract Header');
            insert contractHead;

            NE__Contract_Account_Association__c caa= new NE__Contract_Account_Association__c(NE__Contract_Header__c=contractHead.id,NE__Account__c=acc.id);
            insert caa;

            Id rtBUId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
            Account acc2    =   new Account(name = 'ThisIsATest2',TGS_Aux_Legal_Entity__c=acc.Id,ParentId = acc.Id, RecordTypeId=rtBUId);
            insert acc2;
            
            Id rtCCId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
             Account acc3   =   new Account(name = 'ThisIsATest3', ParentId = acc2.Id, RecordTypeId= rtCCId);
            insert acc3;

            // Create catalog and services data

            RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
            NE__Product__c prod1 = new NE__Product__c(Name='Test Product 1', RecordTypeId=RTProd.Id);
            insert prod1;
            
            NE__Product__c prod2 = new NE__Product__c(Name='Test Child Product 2', RecordTypeId=RTProd.Id);
            insert prod2;
                
            NE__Family__c familyProd1    =   new NE__Family__c(name = 'Test Family Prod1');
            insert familyProd1;

            NE__Family__c familyProd2    =   new NE__Family__c(name = 'Test Family Prod2');
            insert familyProd2;
                
            NE__DynamicPropertyDefinition__c prop1   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyEnum',NE__Type__c ='Enumerated');
            insert prop1;
            NE__PropertyDomain__c enumProp1 = new NE__PropertyDomain__c(Name='1',NE__PropId__c=prop1.Id);
            NE__PropertyDomain__c enumProp2 = new NE__PropertyDomain__c(Name='2',NE__PropId__c=prop1.Id);
            insert enumProp1;
            insert enumProp2;
            NE__DynamicPropertyDefinition__c prop2   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyString',NE__Type__c ='String');
            NE__DynamicPropertyDefinition__c prop3   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyNumber',NE__Type__c ='Number');
            NE__DynamicPropertyDefinition__c prop4   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyLookup',NE__Type__c ='Dynamic Lookup');        
            insert prop2;
            insert prop3;
            insert prop4;
                
            NE__ProductFamily__c prodFamilyProd1 =   new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = familyProd1.id);
            insert prodFamilyProd1;

            NE__ProductFamily__c prodFamilyProd2 =   new NE__ProductFamily__c(NE__ProdId__c = prod2.id, NE__FamilyId__c = familyProd2.id);
            insert prodFamilyProd2;
                
            NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop1.Id);
            NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop2.Id);
            NE__ProductFamilyProperty__c pfp3 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop3.Id);
            NE__ProductFamilyProperty__c pfp4 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop4.Id,NE__Dynamic_Lookup__c='Lookup Test');
            insert pfp1;
            insert pfp2;
            insert pfp3;
            insert pfp4;
            NE__ProductFamilyProperty__c pfp5 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop1.Id);
            NE__ProductFamilyProperty__c pfp6 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop2.Id);
            NE__ProductFamilyProperty__c pfp7 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop3.Id);
            insert pfp5;
            insert pfp6;
            insert pfp7;

            NE__Dynamic_Lookup__c dynLookUp = new NE__Dynamic_Lookup__c(NE__Name__c='Lookup Test',
                                                                        NE__Columns__c='name',
                                                                        NE__sObject__c='Account');
            insert dynLookUp;

            NE__Lookup_Map__c dynLookUpMap = new NE__Lookup_Map__c( NE__Name__c='Lookup Map Test',
                                                                    NE__Dynamic_Lookup__c=dynLookUp.id,
                                                                    NE__Family__c=familyProd1.id);
            insert dynLookUpMap;

            NE__Lookup_Map_Value__c dynLookUpMapValue = new NE__Lookup_Map_Value__c(NE__Lookup_Map__c=dynLookUpMap.id,
                                                                                    NE__Product_Family_Property__c=pfp4.id,
                                                                                    NE__Type__c='Field',
                                                                                    NE__Value__c='Name');
            insert dynLookUpMapValue;

            NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Catalog Header',
                                                                     NE__Name__C='Catalog Header');
            insert catHead;
            
            NE__Catalog__c  cat = new NE__Catalog__c(Name='TGS Catalog',
                                                    NE__Catalog_Header__c=catHead.Id, 
                                                    NE__StartDate__c=Datetime.now(), 
                                                    NE__Active__c = true);
            insert cat;
            
             NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Service Lines', 
                                                               NE__CatalogId__c=cat.Id);
            insert catCat;
           
            NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Enumerated',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly');
            insert catItem;
            
            
            
            NE__Catalog_Item__c catItemC1 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly');
            insert catItemC1;
            
            NE__Item_Header__c itemHead = new  NE__Item_Header__c(Name='Item Header',
                                                                  NE__Catalog__c=cat.id,
                                                                  NE__Catalog_Item__c=catItemC1.id
                                                                );
            insert itemHead;       
            
            catItemC1.NE__Item_Header__c=itemHead.id;
            update catItemC1;
            
            NE__Catalog_Item__c catItemC2 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Root',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC1.id);
            insert catItemC2;
            
            NE__Catalog_Item__c catItemC3 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Category',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC2.id,
                                                                NE__Root_Catalog_Item__c=catItemC2.id);
            insert catItemC3;
            
            NE__Catalog_Item__c catItemC4 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Child-Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC3.id,
                                                                NE__Root_Catalog_Item__c=catItemC2.id);
            insert catItemC4;

            NE__Lov__c lovItem = new NE__Lov__c(NE__Value1__c='TGS Catalog',
                                                Text_Area_Value__c='string:string');
            insert lovItem;
            
            // order creation
            NE__Order__c ord = new NE__Order__c(NE__OptyId__c=opp.id,
                                                NE__AccountId__c=acc.Id, 
                                                NE__BillAccId__c=acc.Id, 
                                                NE__ServAccId__c=acc.Id, 
                                                NE__CatalogId__c=cat.Id, 
                                                NE__OrderStatus__c='Active', 
                                                NE__ConfigurationStatus__c='Valid',
                                                NE__Contract_Header__c=contractHead.Id, 
                                                NE__Order_date__c=Datetime.now().AddDays(1),
                                                NE__Type__c='Active');
            insert ord;
            
            orderList=new List<String>();
            orderList.add(ord.id);

            NE__OrderItem__c ordit1 = new NE__OrderItem__c(NE__OrderId__c=ord.Id, 
                                                            NE__Parent_Order_Item__c= null,
                                                            NE__Root_Order_Item__c=null,
                                                            NE__ProdId__c=prod1.Id,
                                                            NE__Catalog__c=cat.id,
                                                            NE__CatalogItem__c=catItemC1.Id,
                                                            NE__RecurringChargeFrequency__c =null,
                                                            NE__Qty__c=1,
                                                            NE__Action__c='Add',                                                         
                                                            NE__Status__c ='In progress');
            insert ordit1;
            NE__Order_Item_Attribute__c attr1 = new NE__Order_Item_Attribute__c(Name='Test Attribute 1', NE__Order_Item__c=ordit1.Id, NE__FamPropId__c=pfp1.Id, NE__Value__c='1',NE__Action__c='Add');
            insert attr1;
            NE__Order_Item_Attribute__c attr2 = new NE__Order_Item_Attribute__c(Name='Test Attribute 2', NE__Order_Item__c=ordit1.Id, NE__FamPropId__c=pfp2.Id, NE__Value__c='String',NE__Action__c='Add');
            insert attr2;
            NE__Order_Item_Attribute__c attr3 = new NE__Order_Item_Attribute__c(Name='Test Attribute 3', NE__Order_Item__c=ordit1.Id, NE__FamPropId__c=pfp3.Id, NE__Value__c='11',NE__Action__c='Add');
            insert attr3;
            NE__Order_Item_Attribute__c attr4 = new NE__Order_Item_Attribute__c(Name='Test Attribute 4', NE__Order_Item__c=ordit1.Id, NE__FamPropId__c=pfp4.Id, NE__Value__c='Name',NE__Action__c='Add');
            insert attr4;

            NE__OrderItem__c ordit1SL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,
                                                            NE__Parent_Order_Item__c= ordit1.Id,
                                                            NE__Root_Order_Item__c= ordit1.Id,
                                                            NE__ProdId__c=prod2.Id,
                                                            NE__Catalog__c=cat.id,
                                                            NE__CatalogItem__c=catItemC4.Id,
                                                            NE__RecurringChargeFrequency__c =null,
                                                            NE__Qty__c=1,
                                                            NE__Action__c='Add');
            insert ordit1SL;
            NE__Order_Item_Attribute__c attr5 = new NE__Order_Item_Attribute__c(Name='Test Attribute 5', NE__Order_Item__c=ordit1SL.Id, NE__FamPropId__c=pfp5.Id, NE__Value__c='1',NE__Action__c='Add');
            insert attr5;
            NE__Order_Item_Attribute__c attr6 = new NE__Order_Item_Attribute__c(Name='Test Attribute 6', NE__Order_Item__c=ordit1SL.Id, NE__FamPropId__c=pfp6.Id, NE__Value__c='String',NE__Action__c='Add');
            insert attr6;
            NE__Order_Item_Attribute__c attr7 = new NE__Order_Item_Attribute__c(Name='Test Attribute 7', NE__Order_Item__c=ordit1SL.Id, NE__FamPropId__c=pfp7.Id, NE__Value__c='11',NE__Action__c='Add');
            insert attr7;

            NE__OrderItem__c ordit1TL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,
                                                            NE__Parent_Order_Item__c= ordit1SL.Id,
                                                            NE__Root_Order_Item__c= ordit1.Id,
                                                            NE__ProdId__c=prod2.Id,
                                                            NE__Catalog__c=cat.id,
                                                            NE__CatalogItem__c=catItemC4.Id,
                                                            NE__RecurringChargeFrequency__c =null,
                                                            NE__Qty__c=1,
                                                            NE__Action__c='Add');
            insert ordit1TL;

            NE__OrderItem__c ordit1FL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,
                                                            NE__Parent_Order_Item__c= ordit1TL.Id,
                                                            NE__Root_Order_Item__c= ordit1.Id,
                                                            NE__ProdId__c=prod2.Id,
                                                            NE__Catalog__c=cat.id,
                                                            NE__CatalogItem__c=catItemC4.Id,
                                                            NE__RecurringChargeFrequency__c =null,
                                                            NE__Qty__c=1,
                                                            NE__Action__c='Add');
            insert ordit1FL;
        }       
    }

    static void getTestData(){
        Opportunity oppPa = [SELECT id,Name FROM Opportunity WHERE Name='Test1'];
        oppPaId=oppPa.id;

        NE__Order__C order = [SELECT id,Name FROM NE__Order__c LIMIT 1];
        orderList=new List<String>();
        orderList.add(order.id);
    }


    @isTest static void test_generateDataGrid(){


      // launch test

        getTestData();

        System.debug('OP PADRE-'+oppPaId);
        PageReference pageRef = new PageReference('/apex/BI_O4_BulkFromOpty');

        pageRef.getParameters().put('OptyId', oppPaId);

        Test.setCurrentPage(pageRef);

    	Test.startTest();
    	BI_O4_BulkFromOptyController ctrl=new BI_O4_BulkFromOptyController();
    	ctrl.listStrIdOrders=JSON.serialize(orderList);
        ctrl.strDynLookUpName='Lookup Test';

    	//other methods with less importance
    	/*ctrl.echoVal();
    	BI_O4_BulkFromOptyController.generateRandomNumber();
    	BI_O4_BulkFromOptyController.generateIdentifier('TEST');
    	BI_O4_BulkFromOptyController.generate();
        ctrl.getDynamicLookUpTable();*/
    	//
    	ctrl.generateDataGrid();
    	Test.stopTest();
    }

    

    @isTest static void test_importCSV(){       
      // launch test
        String csvFile = 'head_ConfigurationID;head_Account;head_Catalog;head_Category;head_ConfItemRoot;head_Category;head_ConfItemChild1;head_Category;head_ConfItemChild2;head_Qty;head_Family;head_DynPropDef;head_Value;head_ID;Configuration HoldingId__c;Configuration NE__ServAccId__c;Configuration Cost_Center__c;Configuration Configuration Site__c;Configuration Authorized_User__c;Configuration NE__AccountId__c;Configuration NE__Contract_Account__c';
            
        csvFile += 'ORD-001;accNme;catHeadName;;;;;;;;;;;;;\n';
        csvFile += ';;;catCatName;prod1.Name;;;;;3;familyName;propName;This is a value;;accountId;account2Id;account3Id;;uId;account.Id;contractHeadId\n';
        csvFile += 'ORD-003;accNme;catHeadName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;1;;;;;;;;;;;;\n';
        
        
        PageReference pageRef = new PageReference('/apex/BI_O4_BulkFromOpty');
        Test.setCurrentPage(pageRef);
        Test.startTest();
        BI_O4_BulkFromOptyController ctrl=new BI_O4_BulkFromOptyController();
        ctrl.enteredText1=csvFile;

        //other methods with less importance
        /*ctrl.echoVal();
        BI_O4_BulkFromOptyController.generateRandomNumber();
        BI_O4_BulkFromOptyController.generateIdentifier('TEST');
        BI_O4_BulkFromOptyController.generate();*/
        ctrl.echoVal();
        ctrl.importFileMultiBIR(csvFile);
        
        Test.stopTest();
    }
}