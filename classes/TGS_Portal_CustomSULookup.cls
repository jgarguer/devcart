public class TGS_Portal_CustomSULookup {

  public Integer index {get; set;}
  public Integer maxShown {get; set;} //jmf
  private List<NE__OrderItem__c> allResults; // jmf
  public Integer totMax { get {return allResults.size();}}
  public Integer curMax {get {return (index + maxShown < totMax) ? (index + maxShown) : totMax;}}
    
    
  public NE__OrderItem__c ServiceUnit{get;set;} // new NE__OrderItem__c to create
  public List<NE__OrderItem__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  
  public string tier1{get;set;} // search keyword
  public string tier2{get;set;} // search keyword
  public string tier3{get;set;} // search keyword

  public string userId{get;set;}

  public TGS_Portal_CustomSULookup () {
    ServiceUnit = new NE__OrderItem__c();
    userId = UserInfo.getUserId();
    //userId = '005m0000000kiHYAAY';
    // get the current search string
    //searchString = System.currentPageReference().getParameters().get('lksrch');
    tier1 = System.currentPageReference().getParameters().get('tier1');
    tier2 = System.currentPageReference().getParameters().get('tier2');
    tier3 = System.currentPageReference().getParameters().get('tier3');
	
    maxShown = 10;
    runSearch();  
  }

  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    allResults = performSearch();               
    index = 0;
    results = new List<NE__OrderItem__c>();
    limitResults();
  } 

  // run the search and return the records found. 
  private List<NE__OrderItem__c> performSearch() {

    //String soql = 'select id, name, NE__ProdId__c, NE__ProdId__r.Name from NE__OrderItem__c';
    /*if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';*/
    //soql = soql + ' limit 250';
    //System.debug(soql);
   
    //return database.query(soql);
    
    String condition = '';
    //Funcion que cambie '%' por espacio ' '
    if(tier1 != null && tier1 != ''){
        tier1.replace('%', ' ');          
    }else if(tier2 != null && tier2 != ''){
        tier2.replace('%', ' ');   
    }else if(tier3 != null && tier3 != ''){
        tier3.replace('%', ' ');     
    }
    
        
             
    if(tier3 != null && tier3 != ''){
        System.debug('&&&&&&&&&&&&&&&&&&&' + tier3);
        condition = 'NE__ProductId__r.Name = \'' + tier3 + '\' ';
    }else if(tier2 != null && tier2 != ''){
        System.debug('&&&&&&&&&&&&&&&&&&&' + tier1);
       condition = 'NE__ProductId__r.TGS_CWP_Tier_2__c = \'' + tier2 + '\' ';
    }else if(tier1 != null && tier1 != ''){
        System.debug('&&&&&&&&&&&&&&&&&&&' + tier1);
        condition = 'NE__ProductId__r.TGS_CWP_Tier_1__c = \'' + tier1 + '\' ';
    }   
     
    System.debug('&&&&&&&&&&&&&&&&&&& Antes myCatalogItems');
    List<NE__Catalog_Item__c> myCatalogItems = new List<NE__Catalog_Item__c>();
    myCatalogItems = TGS_Portal_Utils.getFilteredVisibleCatalogItems(condition, userId); 
    System.debug('&&&&&&&&&&&&&&&&&&& myCatalogItems size = ' + myCatalogItems.size());
      
    Map<Id, NE__OrderItem__c> myOrderMap = new Map<Id, NE__OrderItem__c>();    
    myOrderMap = TGS_Portal_Utils.getFilteredInstalledServices(myCatalogItems , userId);
      
    System.debug('&&&&&&&&&&&&&&&&&&& myOrderMap size = ' + myOrderMap.size());
    List<NE__OrderItem__c> myOrderList; //= new List<NE__OrderItem__c>();
    myOrderList = myOrderMap.values();
    System.debug('&&&&&&&&&&&&&&&&&&& myOrderList size = ' + myOrderList.size());

    if(myOrderList != null){
        return myOrderList;
    }else{
        return new List<NE__OrderItem__c>();
    }
  }
/*
  // save the new account record
  public PageReference saveAccount() {
    insert account;
    // reset the account
    account = new Account();
    return null;
  }
*/  

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
  
    
  public Boolean gethasPrev() {
      return index != 0;
  }
  public Boolean gethasNext() {
      return curMax < totMax;
  }
    
  public void limitResults() {
      Integer i = index, j = 0;
      results.clear();
      while(i < allResults.size() && j < maxShown) {
          results.add(allResults.get(i));
          i++; j++;
      }
  }
  public void next() {
      index += maxShown;
      limitResults();
  }
  public void prev() {
      index -= maxShown;
      limitResults();
  }
}