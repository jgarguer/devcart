/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ricardo Pereira
	Company:       New Energy Aborda
	Description:   Test class for BI_O4_ManualMultiselect
	
	History:
	
	<Date>					<Author>				<Change Description>
	20/09/2016				Ricardo Pereira			Initial version
	------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_ManualMultiselect_TEST {

	static{
        BI_TestUtils.throw_exception = false;
  	}
	
	@isTest static void setBackingList() {
		
		Boolean a = false;
		Boolean b = false;
		BI_O4_ManualMultiselect mm = new BI_O4_ManualMultiselect();

		mm.rendered = true;
		System.assertEquals(mm.rendered, true);
		
		List<BI_O4_ManualMultiselect.SelOpt> lopt = new List<BI_O4_ManualMultiselect.SelOpt>();
		BI_O4_ManualMultiselect.SelOpt so = new BI_O4_ManualMultiselect.SelOpt(1,a,b,'UNO','Uno');
		BI_O4_ManualMultiselect.SelOpt so1 = new BI_O4_ManualMultiselect.SelOpt(2,a,b,'DOS','Dos');
		lopt.add(so);
		lopt.add(so1);
		mm.LeftOptions = lopt;
		System.assertEquals(mm.LeftOptions, lopt);
		mm.RightOptions = lopt;
		System.assertEquals(mm.RightOptions, lopt);

		List<SelectOption> lso = new List<SelectOption>();
		SelectOption opt = new SelectOption('UNO', 'Uno', false);
		SelectOption opt1 = new SelectOption('DOS', 'Dos', false);
		lso.add(opt);
		lso.add(opt1);
		mm.SelectedValues = lso;
		System.assertEquals(mm.SelectedValues, lso);

		mm.AllValues = lso;
		System.assertEquals(mm.AllValues, lso);

		mm.setOptions();
		mm.getBackingList();
		
		List<String> l = new List<String>();
		l.add('Uno');
		l.add('Dos');
		mm.backingList = l;
		System.assertEquals(mm.backingList, l);
	}	
}