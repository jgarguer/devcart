/*------------------------------------------------------------
Author:         Ana Cirac 
Company:        Deloitte
Description:    Class to communicate with UDO
History
<Date>          <Author>        <Change Description>
04-feb-2015     Ana Cirac        Initial Version
------------------------------------------------------------*/
global with sharing class TGS_DAO_HPD {
    
    /* Message when the status of the case is Restored in UDO */
    static final String MESSAGE_RESTORED = 'Status Restored in UDO';
    /* Attributes */
    static final String INCIDENT_NUMBER = 'Incident_Number';
    static final String STATUS = 'Status';
    static final String STATUS_REASON = 'Status_Reason';
    
    static final String ACTION = 'Action';
    static final String MODIFY = 'MODIFY';
    static final String SD_VALIDATION = 'In SD Validation';
    static final String UDO = 'UDO';
    /* Valid Status for a case*/
    static final Set<String> validStatus = new Set<String> {'Pending', 'In Progress', 'Resolved'};
    
    global class Result{  
            webservice string Error_Code;
            webservice string Error_Msg;
            webservice string Operation_Status;
            webservice string Operation_Instance_ID;
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Web Service that UDO calls to modify an order
    History
    <Date>          <Author>        <Change Description>
    04-feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    webservice static Result modifyOrder(String System_ID, String Attributes,String Attachment_Name, Blob Attachment_Data) { 
        
        Savepoint savePointCheckout = Database.setSavepoint();   
            
        Result resul = new Result();     
        resul.operation_status = 'OK'; 
        resul.Error_Msg = 'OK'; 
        resul.Error_Code ='0';
        
        try{
            /* Checks mandatory parameters */
            if (!String.isNotBlank(System_ID) || !String.isNotBlank(Attributes)){
                
                throw new IntegrationException('System_ID, and Attributes are mandatory parameters','','202');  
            
            }
            if (!System_ID.contains(UDO)){
                
                throw new IntegrationException('Invalid System_ID','','204');   
            }
            Map<String, String> attrs = generateMapAttributes(Attributes);
            System.debug('ACTION: '+attrs.get(ACTION));
            if (!String.isNotBlank(attrs.get(ACTION))){
                /*Action Mandatory */
                throw new IntegrationException('Action is a mandatory attribute','','202'); 
                
            }else if (!attrs.get(ACTION).equals(MODIFY)){
                /*Invalid Action */
                throw new IntegrationException('Invalid Action','','204');  
            }
            if (!String.isNotBlank(attrs.get(INCIDENT_NUMBER))){
                /*Incident_Number Mandatory */
                throw new IntegrationException('Incident_Number is a mandatory attribute','','202');    
            }
            List<Case> casesUpdated = [SELECT Id, caseNumber, status FROM Case WHERE caseNumber = :attrs.get(INCIDENT_NUMBER)];
            if(casesUpdated.size()!=0){
                Case caseUpdated = casesUpdated.get(0);
                /*
                if (Attachment_Data!=null){
                    /*Attachment_Name Mandatory 
                    if (!String.isNotBlank(Attachment_Name)){
                        throw new IntegrationException('Attachment_Name is mandatory','','202');    
                    }
                    /* Creates an attachment for this case 
                    Attachment attach = new Attachment();
                    /* Checks if Attachment_Name == message error: file size is large than the maximum 
                    attach.Name = Attachment_Name;
                    attach.Body = Attachment_Data;
                    attach.ParentId = caseUpdated.Id;
                    attach.Description = attrs.get('Work_Info_Summary');
                    insert attach;
                }else{
                    */
                    if(String.isNotBlank(attrs.get(STATUS))){
                        
                       if(caseUpdated.status.equals('Pending') && attrs.get(STATUS).equals('In Progress')){
                            
                            throw new IntegrationException('Invalid status transition','', '204'); 
                       }
                       if (validStatus.contains(attrs.get(STATUS))){
                           
                            /* Updates the status of the case 
                            if (attrs.get(STATUS).equals('Resolved')){
                                
                                caseUpdated.Status = 'In Progress';
                                
                            }else{
                            */
                                caseUpdated.Status = attrs.get(STATUS);
                            /*}*/
                            
                            if (!String.isNotBlank(attrs.get(STATUS_REASON)) || attrs.get(STATUS_REASON).equals('From Work in Progress')){
                            //if (attrs.get(STATUS_REASON).equals('From Work in Progress')){
                                /* Updates the TGS_Status_reason__c of the case */
                                caseUpdated.TGS_Status_reason__c = SD_VALIDATION;
                            
                            }
                            if(attrs.get(STATUS).equals('Resolved')){
                                 caseUpdated.TGS_Status_reason__c = '';
                            }
                        }else{
                            
                            throw new IntegrationException('Invalid Status','', '204'); 
                        }
                    }
                    /* Creates a Work Info */                 
                    TGS_Work_Info__c workInfo = new TGS_Work_Info__c();
                    String commentBody = attrs.get('Work_Info_Summary');
                    if (String.isNotBlank(attrs.get('Work_Info_Notes'))){
                        /* commentBody of the caseComment = Work_Info_Summary: Work_Info_Notes */
                        commentBody += ': ' +attrs.get('Work_Info_Notes');
                    }
                    workInfo.TGS_Description__c = commentBody;
                    workInfo.TGS_Case__c=caseUpdated.Id;
                    
                    if(String.isNotBlank(attrs.get('Work_Info_Summary')) || String.isNotBlank(attrs.get('Work_Info_Notes'))){
                        insert workInfo;
                        system.debug(workInfo.id);
                        if (Attachment_Data!=null && Attachment_Data.size()!=0){
                        System.debug('Attachment_Data!=null');
                            /*Attachment_Name Mandatory */ 
                            if (!String.isNotBlank(Attachment_Name)){
                                throw new IntegrationException('Attachment_Name is mandatory','','202');    
                            }
                            /* Creates an attachment for this case */
                            Attachment attach = new Attachment();
                            /* Checks if Attachment_Name == message error: file size is large than the maximum */
                            attach.Name = Attachment_Name;
                            attach.Body = Attachment_Data;
                            system.debug('Asignado Att_body');
                            attach.ParentId = workInfo.Id;
                            //attach.Description = attrs.get('Work_Info_Summary');
                            insert attach;
                            system.debug('AttachId: '+attach);
                            
                        }
                    }
                    
                    
                    update caseUpdated;

                    
                /*}*/   
          }else{
            
            throw new IntegrationException('Invalid INCIDENT_NUMBER','','204'); 
          }  
        } catch(IntegrationException e) {           
            resul.operation_status = 'ERROR';       
            resul.Error_Msg = e.getMessage();        
            resul.Error_Code = e.getErrorIntegration().TGS_ErrorType__c;
            DataBase.rollback(savePointCheckout);
            TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
            error.TGS_Interface__c = 'UDO'; 
            error.TGS_RequestInfo__c = 'Message: ' + e.getMessage();
            error.TGS_Operation__c ='MODIFY' ;
            insert error;
            
            
        } catch(Exception e) {          
            resul.operation_status = 'ERROR';       
            resul.Error_Msg = e.getMessage(); 
            resul.Error_Code = '500';   
            DataBase.rollback(savePointCheckout);
            TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
            error.TGS_Interface__c = 'UDO'; 
            error.TGS_RequestInfo__c = 'Message: ' + e.getMessage()+' '+e.getLineNumber();
            error.TGS_Operation__c ='UDO_SF_MODIFY_ORDER' ;
            insert error;
        }
        
        return resul;
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Creates a Map with key = name of the Attribute 
                    and value= value of the Attribute.
    History
    <Date>          <Author>        <Change Description>
    04-feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    static Map<String, String> generateMapAttributes(String Attributes){
            
        Map<String, String> attrs = new Map<String, String>();
        String[] atts = Attributes.split(';',0);
        for (String att:atts){
            //att = {attributeName,attributeValue} 
            String[] attribute = att.split('=');
            String value = attribute[1];
            value = value.substring(1,value.length()-1);
            attrs.put(attribute[0],value);
        }
        return attrs;
    }
}