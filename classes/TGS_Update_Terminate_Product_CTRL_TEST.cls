@isTest
public class TGS_Update_Terminate_Product_CTRL_TEST {

	public TGS_Update_Terminate_Product_CTRL_TEST() {
		System.debug('TESTEANDO EL TEST');
	}


	@isTest
	public static void loadAndValidate_TEST() {

		NE__Order__c testOrder = TGS_Dummy_Test_Data.dummyOrder();
		NE__OrderItem__c testOi = TGS_Dummy_Test_Data.dummyOrderItem();
		NE__OrderItem__c testOi2 = TGS_Dummy_Test_Data.dummyOrderItem();
		Account testAccount = TGS_Dummy_Test_Data.dummyHierarchy();
		Id rtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
		Account businessUnit = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Business_Unit + String.valueOf(Math.random()),
            RecordTypeId = rtypeId,
            ParentId = testAccount.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = testAccount.Parent.ParentId);
        insert businessUnit;
		BI_Punto_de_instalacion__c site =  TGS_Dummy_Test_Data.dummyPuntoInstalacion(businessUnit.Id);
		//Account testAccount2 = TGS_Dummy_Test_Data.dummyHierarchy();
		//Map<Id,Account> map_Accounts = new Map<Id,Account>([SELECT Id,Name,RecordTypeId,TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c, TGS_Aux_Legal_Entity__c,TGS_Aux_Business_Unit__c FROM Account]);
		testAccount.TGS_Aux_Customer_Country__c = null;
		testAccount.TGS_Aux_Legal_Entity__c = null;
		testAccount.TGS_Aux_Business_Unit__c = null;
		update testAccount;
		NE__Contract_Header__c testCH = new NE__Contract_Header__c(NE__Name__c = 'CHN-0000000001');
		insert testCH;

		TGS_Update_Terminate_Product_Services__c utps = new TGS_Update_Terminate_Product_Services__c();
		utps.TGS_Service__c =  'Blank SIM Emergency Stock';
		utps.Name = '001';
		TGS_Update_Terminate_Product_Services__c utps2 = new TGS_Update_Terminate_Product_Services__c();
		utps2.TGS_Service__c =  'Bundles With Pooling';
		utps2.Name = '002';
		List<TGS_Update_Terminate_Product_Services__c> utpsList = new List<TGS_Update_Terminate_Product_Services__c>();
		utpsList.add(utps);
		utpsList.add(utps2);

		insert utpsList;

		NE__Contract_Account_Association__c testCAA = new NE__Contract_Account_Association__c(NE__Account__c = testAccount.Id, NE__Contract_Header__c = testCH.Id);
		insert testCAA;

		testOrder.NE__AccountId__c = testAccount.Id;
		Case testCase = TGS_Dummy_Test_Data.dummyCaseTGS(Constants.CASE_RTYPE_DEVNAME_ORDER_MNGMNT, '');

		testCase.TGS_Service__c = 'Blank SIM Emergency Stock';
		insert testCase;
		System.debug('CASO 2: ' + testCase);
		testOrder.Case__r = testCase;
		NE__Asset__c testAsset = new NE__Asset__c();
		insert testAsset;
		testOrder.NE__Asset__c = testAsset.Id;
		update testOrder;
		System.debug('ORDID 1: ' + testOrder.Id);
		Contact testContact = TGS_Dummy_Test_Data.dummyContactTGS('Midnight');
		testContact.TGS_Enviar_Email_Order_Management_Case__c = false;
		insert testContact;
		testCase.ContactId = testContact.Id;
		update testCase;
		testCase.Type = 'Registration';
		testCase.Status = 'In Progress';
		update testCase;
		testCase.Status = 'Resolved';
		update testCase;

		

		Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Asset');
		testOrder.RecordTypeId = rtId;
		System.debug('RT: ' + rtId);
		update testOrder;
		testOi.NE__OrderId__c = testOrder.Id;
		testOi.NE__AssetItemEnterpriseId__c = testOi2.Id;
		testOi2.NE__OrderId__c = testOrder.Id;
		testOi2.NE__AssetItemEnterpriseId__c = testOi.Id;
		update testOi;
		update testOi2;
		

		update testOrder;
		TGS_Update_Terminate_Product_CTRL utp = new TGS_Update_Terminate_Product_CTRL();
		Test.setCurrentPageReference(new PageReference('Page.TGS_Update_Terminate_Product')); 
		System.currentPageReference().getParameters().put('ordId',testOrder.Id);
		System.currentPageReference().getParameters().put('ciId',testOi.Id);
		/*utp.ordId = testOrder.Id;
		utp.ciId = testOi.Id;*/
		

		

		

		utp.load();


		utp.save();
		utp.cancel();
		utp.str_SelectedField = 'main';
		utp.id_SelectedID = utp.acc_MainAccount.Id;
		utp.selectAccount();
		Test.setCurrentPageReference(new PageReference('Page.TGS_Update_Terminate_Product')); 
		System.currentPageReference().getParameters().put('ordId',null);
		System.currentPageReference().getParameters().put('ciId',testOi.Id);
		/*utp.ordId = null;*/
		utp.load();
		utp.orderitem.TGS_Billing_end_date__c = Date.today();
		utp.save();
		utp.cancel();

		utp.cancelSelection();



		utp.orderitem.TGS_Service_status__c = 'Received';
		utp.loader_RenderValues_BillingDatesInputs();
		utp.orderitem.TGS_Service_status__c = 'Deleted';
		utp.loader_RenderValues_BillingDatesInputs();
		utp.acc_MainAccount = testAccount;
		utp.acc_ServAccount = testAccount;
		utp.acc_BillAccount = testAccount;
		utp.acc_Site = site;
		utp.loadBillAccounts();
		utp.loadServAccounts();
		utp.loadSites();
		utp.loader_AvailableAccounts();
		TGS_Update_Terminate_Product_CTRL.assigner_ContractAccount(testOrder, utp.acc_MainAccount);

		utp.id_SelectedID = testAccount.Id;
		
		

		utp.str_SelectedField = 'main';
		utp.id_SelectedID = utp.acc_MainAccount.Id;
		utp.selectAccount();
		utp.str_SelectedField = 'serv';
		utp.id_SelectedID = utp.acc_MainAccount.Id;
		utp.selectAccount();
		utp.str_SelectedField = 'bill';
		utp.id_SelectedID = utp.acc_MainAccount.Id;
		utp.selectAccount();
		utp.str_SelectedField = 'site';
		utp.id_SelectedID = utp.acc_Site.Id;
		utp.selectAccount();

		utp.order.NE__AccountId__c = testAccount.Id;
		utp.prevorder.NE__AccountId__c = null;
		utp.save();
	}

	@isTest
	public static void assigner_FundationData_TEST() {


		NE__OrderItem__c testOI = TGS_Dummy_Test_Data.dummyConfigurationOrder(); 
		NE__OrderItem__c testOI2 = TGS_Dummy_Test_Data.dummyConfigurationOrder();


		NE__Order__c testOrderOrder = [SELECT Id, RecordTypeId FROM NE__Order__c WHERE Id = :testOI.NE__OrderId__c];
		NE__Order__c testOrderAsset = TGS_Dummy_Test_Data.dummyOrder();
		testOI2.NE__OrderId__c = testOrderAsset.Id;
		testOi2.NE__AssetItemEnterpriseId__c = testOI.Id;
		update testOI2;
		testOi.NE__AssetItemEnterpriseId__c = testOI2.Id;
		update testOI;

		testOrderOrder = [
                            SELECT  Id, Name, RecordType.DeveloperName, 
                                    NE__AccountId__c, NE__AccountId__r.RecordType.DeveloperName,NE__AccountId__r.Name,
                                    NE__AccountId__r.TGS_Aux_Customer_Country__c,NE__AccountId__r.TGS_Aux_Holding__c,NE__AccountId__r.TGS_Aux_Legal_Entity__c,NE__AccountId__r.TGS_Aux_Business_Unit__c,
                                    NE__ServAccId__c, NE__ServAccId__r.RecordType.DeveloperName,NE__ServAccId__r.Name,
                                    NE__ServAccId__r.TGS_Aux_Customer_Country__c,NE__ServAccId__r.TGS_Aux_Holding__c,NE__ServAccId__r.TGS_Aux_Legal_Entity__c,NE__ServAccId__r.TGS_Aux_Business_Unit__c,
                                    NE__BillAccId__c, NE__BillAccId__r.RecordType.DeveloperName,NE__BillAccId__r.Name,
                                    NE__BillAccId__r.TGS_Aux_Customer_Country__c,NE__BillAccId__r.TGS_Aux_Holding__c,NE__BillAccId__r.TGS_Aux_Legal_Entity__c,NE__BillAccId__r.TGS_Aux_Business_Unit__c,
                                    NE__AssetEnterpriseId__c, Case__r.Order__c, NE__Asset__c,Case__r.TGS_Service__c,NE__Contract_Account__c, 
                                    (
                                        SELECT  Id,
                                            NE__ProdId__c,
                                            NE__AssetItemEnterpriseId__c,
                                            TGS_Service_status__c,NE__Status__c,
                                            NE__Account__c,NE__Asset_Item_Account__c,
                                            NE__Service_Account__c,NE__Service_Account_Asset_Item__c,
                                            NE__Billing_Account__c,NE__Billing_Account_Asset_Item__c,
                                            TGS_billing_start_date__c, TGS_Billing_end_date__c,
                                            TGS_CSUID1__c, TGS_CSUID1_Old__c, TGS_CSUID2__c, TGS_CSUID2_Old__c
                                        FROM    NE__Order_Items__r
                                    )
                            FROM    NE__Order__c 
                            WHERE   Id =:testOrderOrder.Id
                        ];
		testOrderAsset = [
                            SELECT  Id, Name, RecordType.DeveloperName, 
                                    NE__AccountId__c, NE__AccountId__r.RecordType.DeveloperName,NE__AccountId__r.Name,
                                    NE__AccountId__r.TGS_Aux_Customer_Country__c,NE__AccountId__r.TGS_Aux_Holding__c,NE__AccountId__r.TGS_Aux_Legal_Entity__c,NE__AccountId__r.TGS_Aux_Business_Unit__c,
                                    NE__ServAccId__c, NE__ServAccId__r.RecordType.DeveloperName,NE__ServAccId__r.Name,
                                    NE__ServAccId__r.TGS_Aux_Customer_Country__c,NE__ServAccId__r.TGS_Aux_Holding__c,NE__ServAccId__r.TGS_Aux_Legal_Entity__c,NE__ServAccId__r.TGS_Aux_Business_Unit__c,
                                    NE__BillAccId__c, NE__BillAccId__r.RecordType.DeveloperName,NE__BillAccId__r.Name,
                                    NE__BillAccId__r.TGS_Aux_Customer_Country__c,NE__BillAccId__r.TGS_Aux_Holding__c,NE__BillAccId__r.TGS_Aux_Legal_Entity__c,NE__BillAccId__r.TGS_Aux_Business_Unit__c,
                                    NE__AssetEnterpriseId__c, Case__r.Order__c, NE__Asset__c,Case__r.TGS_Service__c,NE__Contract_Account__c, 
                                    (
                                        SELECT  Id,
                                            NE__ProdId__c,
                                            NE__AssetItemEnterpriseId__c,
                                            TGS_Service_status__c,NE__Status__c,
                                            NE__Account__c,NE__Asset_Item_Account__c,
                                            NE__Service_Account__c,NE__Service_Account_Asset_Item__c,
                                            NE__Billing_Account__c,NE__Billing_Account_Asset_Item__c,
                                            TGS_billing_start_date__c, TGS_Billing_end_date__c,
                                            TGS_CSUID1__c, TGS_CSUID1_Old__c, TGS_CSUID2__c, TGS_CSUID2_Old__c
                                        FROM    NE__Order_Items__r
                                    )
                            FROM    NE__Order__c 
                            WHERE   Id =:testOrderAsset.Id
                        ];

		Id rtIdOrder = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
		Id rtIdAsset = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Asset');
		testOrderOrder.RecordTypeId = rtIdOrder;
		testOrderAsset.RecordTypeId = rtIdAsset;
		update testOrderOrder;
		update testOrderAsset;
		Account testAccount = TGS_Dummy_Test_Data.dummyHierarchy();
		//Account testAccount2 = TGS_Dummy_Test_Data.dummyHierarchy();
		//Map<Id,Account> map_Accounts = new Map<Id,Account>([SELECT Id,Name,RecordTypeId,TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c, TGS_Aux_Legal_Entity__c,TGS_Aux_Business_Unit__c FROM Account]);
		testAccount.TGS_Aux_Customer_Country__c = null;
		testAccount.TGS_Aux_Legal_Entity__c = null;
		testAccount.TGS_Aux_Business_Unit__c = null;
		update testAccount;


		List<NE__Order__c>      list_Orders_toUpdate = new List<NE__Order__c>();
        List<NE__OrderItem__c>  list_OrderItems_toUpdate = new List<NE__OrderItem__c>();


        Set<Id> set_IDS_ToRoD = new Set<Id>();
		set_IDS_ToRoD.add(testOI.Id);
		set_IDS_ToRoD.add(testOI2.Id);
		TGS_Update_Terminate_Product_CTRL utp = new TGS_Update_Terminate_Product_CTRL();
		List<TGS_Audit_Item__c> list_AuditItems = new List<TGS_Audit_Item__c>();
		utp.list_AuditItems = list_AuditItems;
		utp.acc_MainAccount = testAccount;
		utp.list_Orders_toUpdate = new List<NE__Order__c>();
		utp.list_OrderItems_toUpdate = new List<NE__OrderItem__c>();
		utp.order = testOrderAsset;
		utp.orderRTOrder = testOrderOrder;

        utp.assigner_FundationData(set_IDS_ToRoD);

        NE__OrderItem__c testOI3 = TGS_Dummy_Test_Data.dummyConfigurationOrder();
        testOI3.NE__Parent_Order_Item__c = testOI.Id;
        update testOI3;

        utp.orderitem = testOI;
        utp.orderitem.NE__AssetItemEnterpriseId__c = testOI2.Id;
        utp.assigner_BillingData(set_IDS_ToRoD);

        //TGS_Update_Terminate_Product_CTRL.assigner_FundationData(testOrderOrder, testOrderAsset, list_Orders_toUpdate, list_OrderItems_toUpdate);

	}

}