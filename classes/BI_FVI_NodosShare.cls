/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   class for sharing object nodos
    Test Class:    BI_FVI_NodosShare_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    10/06/2016           Geraldine P. Montes         Initial Version
    12/05/2017              Cristina Rodriguez       Solve Async Future Method Inside Loops. Changed BI_FVI_NodosShare.createNodosShare
    -------------------------------------------------------------------------------*/ 

public with sharing class BI_FVI_NodosShare {

    public static List<BI_FVI_Nodos__Share> lstNodoShare;

    //START CRM 12/05/2017 Async Future Method Inside Loops
    @Future
    public static void createNodosShare(set<String> setPaises)
    {
        lstNodoShare = new List<BI_FVI_Nodos__Share>();
        String strNodoRecordType = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByName().get('Nodo Adquisición de Gestión').getRecordTypeId();

        List<BI_FVI_Nodos__c> lstNAG = [select id, name, RecordTypeId, BI_FVI_Pais__c from BI_FVI_Nodos__c where RecordTypeId = :strNodoRecordType AND BI_FVI_Pais__c IN :setPaises];
        System.debug('\n\n lstNAG ===========>>>'+lstNAG);


        map<string, string> mapDevPais = new map<string, string>();
        string strPais = '';
        string strGrupo = '';
        for (string setElement : setPaises) {
            strPais = setElement.toUpperCase().substring(0, 3);
            strGrupo = 'BI_FVI_'+strPais+'_DIST_ADM_BO';
            mapDevPais.put(strGrupo,setElement);
        }

        map<string, List<Group>> mapPaisGrupos = new map<string, List<Group>>();
        List<Group> lstGroup = [select id, name from Group where Name IN :mapDevPais.keySet()];
        for(Group objGroup : lstGroup)
        {
            if(mapPaisGrupos.containsKey(mapDevPais.get(objGroup.name)))
            {
                mapPaisGrupos.get(mapDevPais.get(objGroup.name)).add(objGroup);
            }
            else
            {
                /*List <Group> lstWrapemp=new List<Group>();
                lstWrapemp.add(objGroup);                
                mapPaisGrupos.put(mapDevPais.get(objGroup.name),lstWrapemp);*/
                mapPaisGrupos.put(mapDevPais.get(objGroup.name),new List <Group> {objGroup});
            }
        }

        BI_FVI_Nodos__Share objNodoShare;
        for(BI_FVI_Nodos__c objNAG : lstNAG)
        {
            for(Group objGroup : mapPaisGrupos.get(objNAG.BI_FVI_Pais__c))
            {
                System.debug('\n\n objNAG ===========>>>'+objNAG);
                System.debug('\n\n objGroup ===========>>>'+objGroup);
                if(objNAG != null && objGroup != null)
                {
                
                    objNodoShare = new BI_FVI_Nodos__Share();
                    objNodoShare.UserOrGroupId  = objGroup.id;
                    objNodoShare.ParentId = objNAG.id;
                    objNodoShare.AccessLevel = 'Edit';

                    lstNodoShare.add(objNodoShare);
                    
                }
            }
        }

        if(!lstNodoShare.isEmpty())
        {           
            System.debug('\n\n LISTA ===========>>>'+lstNodoShare);
            System.debug('\n\n INSERT ANTES ===> getLimitQueryRows() ====>>'+Limits.getLimitQueryRows() +'  -  getQueryRows() ==>>'+Limits.getQueryRows());
            System.debug('\n\n ====>>>> CPU TIME ====>>> '+Limits.getCpuTime() + ' de LIMIT ====>>>>'+Limits.getLimitCpuTime());

            try
            {
                insert lstNodoShare;
                System.debug('\n\n LISTA INSERT DESPUES ===========>>>'+lstNodoShare);


            }
            catch(Exception e)
            {
                System.debug('\n\nError====>>>'+e.getDMLMessage(0));
            }
            
        }
    }
    /*******OLD
    @Future
    public static void createNodosShare(string StrGroup, string StrCountry)
    {
        lstNodoShare = new List<BI_FVI_Nodos__Share>();
        String strNodoRecordType = Schema.SObjectType.BI_FVI_Nodos__c.getRecordTypeInfosByName().get('Nodo Adquisición de Gestión').getRecordTypeId();
            System.debug('\n\n StrCountry ===========>>>'+StrCountry);

        List<BI_FVI_Nodos__c> lstNAG = [select id, name, RecordTypeId, BI_FVI_Pais__c from BI_FVI_Nodos__c where RecordTypeId = :strNodoRecordType AND BI_FVI_Pais__c = :StrCountry];
        System.debug('\n\n lstNAG ===========>>>'+lstNAG);

        string strPais = StrCountry.toUpperCase().substring(0, 3);
        string strGrupo = 'BI_FVI_'+strPais+'_DIST_ADM_BO';

        List<Group> lstGroup = [select id, name from Group where Name = :strGrupo];

        BI_FVI_Nodos__Share objNodoShare;

        for(BI_FVI_Nodos__c objNAG : lstNAG)
        {
            for(Group objGroup : lstGroup)
            {
                System.debug('\n\n objNAG ===========>>>'+objNAG);
                System.debug('\n\n objGroup ===========>>>'+objGroup);
                if(objNAG != null && objGroup != null)
                {
                
                    objNodoShare = new BI_FVI_Nodos__Share();
                    objNodoShare.UserOrGroupId  = objGroup.id;
                    objNodoShare.ParentId = objNAG.id;
                    objNodoShare.AccessLevel = 'Edit';

                    lstNodoShare.add(objNodoShare);
                    
                }
            }
        }

        if(!lstNodoShare.isEmpty())
        {           
            System.debug('\n\n LISTA ===========>>>'+lstNodoShare);
            System.debug('\n\n INSERT ANTES ===> getLimitQueryRows() ====>>'+Limits.getLimitQueryRows() +'  -  getQueryRows() ==>>'+Limits.getQueryRows());
            System.debug('\n\n ====>>>> CPU TIME ====>>> '+Limits.getCpuTime() + ' de LIMIT ====>>>>'+Limits.getLimitCpuTime());

            try
            {
                insert lstNodoShare;
                System.debug('\n\n LISTA INSERT DESPUES ===========>>>'+lstNodoShare);


            }
            catch(Exception e)
            {
                System.debug('\n\nError====>>>'+e.getDMLMessage(0));
            }
            
        }
    }
    */
    //END CRM 12/05/2017 Async Future Method Inside Loops

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Sevilla Durango
    Company:        NEAborda
    Description:    Method used in the method CrearGruposyAdicionarUsuariosBO to colaborate the NAB Node with a group created
                    the conditions
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History:     
    
    <Date>                  <Author>                <Change Description>
    15/06/2016              Alvaro Sevilla             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @Future 
    public static void createNodosShareNAB(map<String,String> mpGrupoPaisNAB)
    {

        List<BI_FVI_Nodos__Share> lstNodoShareNAB = new List<BI_FVI_Nodos__Share>();
        RecordType NodoRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_NAB'];

        try{

            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            for(BI_FVI_Nodos__c objNAB : [SELECT Id,BI_FVI_Pais__c FROM BI_FVI_Nodos__c WHERE RecordTypeId =: NodoRecordType.Id AND BI_FVI_Pais__c =: mpGrupoPaisNAB.keySet()]){            
                                    
                BI_FVI_Nodos__Share objNodoShare = new BI_FVI_Nodos__Share();
                objNodoShare.ParentId = objNAB.id;
                objNodoShare.AccessLevel = 'Edit';
                objNodoShare.UserOrGroupId  = mpGrupoPaisNAB.get(objNAB.BI_FVI_Pais__c);

                lstNodoShareNAB.add(objNodoShare);
                                                    
            }

            if(lstNodoShareNAB != null && lstNodoShareNAB.size() > 0){          

                insert lstNodoShareNAB;
                
            }

        }catch(Exception exc){

             BI_LogHelper.generate_BILog('BI_UserMethods.createNodosShareNAB', 'BI_FVI', exc, 'Trigger');
        }
    }



}