public class BI_TestUtils {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Wrapper class used to determine if it is necessary to throw an exception during the test execution
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

  	public static Boolean throw_exception = true;

	public static Boolean isRunningTest() {
	    return Test.isRunningTest() && throw_exception;
	}
	
}