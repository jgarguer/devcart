/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Gianluca Campioni
        Company:       New ENergy Group
        Description:   Class to assure BI_FVI_AccountManager class coverage
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        26/08/2016                      Gianluca Campioni           Initial version
        15/09/2016                      Alvaro sevilla              Se hicieron cambios en todos los metodos
        11/10/2016                      Alvaro sevilla              Se hicieron cambios en todos los metodos
        01/02/2017                      Pedro Párraga               Increase in coverage
        22/03/2017                      Humberto Nunes              Cambios en varios metodos y nuevas casuisticas para subir cobertura de 84 a 96%
        20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class BI_FVI_AccountManager_TEST {

  private static String strMsg = '';
  private static String vUserId = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()].Id;
  private static String vRecordTypeAccountId = [SELECT id FROM RecordType WHERE Name = '3.Legal Entity'].id;
  private static String vRecordTypeNodeNcpId = [SELECT id FROM RecordType WHERE Name = 'Nodo Comercial Principal'].id;
  private static String vRecordTypeNodeNavId = [SELECT id FROM RecordType WHERE Name = 'Nodo de Adquisición Volatil'].id;
  private static String vRecordTypeNodeNagId = [SELECT id FROM RecordType WHERE Name = 'Nodo Adquisición de Gestión'].id;
  private static String vRecordTypeNodeNcSgId = [SELECT id FROM RecordType WHERE Name = 'Nodo Comercial Superior'].id;
  private static RestRequest req;
  private static Id idProfile;
  private static List<User> lst_user;

  public static void createData()
  {
        idProfile = BI_DataLoad.searchAdminProfile();
        lst_user = BI_DataLoad.loadUsers(2, idProfile, Label.BI_Administrador);

        req = new RestRequest(); 
        req.requestURI = '/services/apexrest/AccountManager' ; 
        req.addParameter('ruc', '20518569391');
        req.addParameter('uid', vUserId);         
        req.httpMethod = 'Get';  

        RestContext.request = req;

        System.debug('---REQ---->'+req);
  }

  public static void createDataNoParameters(integer CaseWrong)
  {
        idProfile = BI_DataLoad.searchAdminProfile();
        lst_user = BI_DataLoad.loadUsers(2, idProfile, Label.BI_Administrador);

        req = new RestRequest(); 
        req.requestURI = '/services/apexrest/AccountManager' ; 
        req.addParameter('ruc', '20518569391');
        if (CaseWrong ==  0) req.addParameter('uid', null);  // SIN USUARIO 
        if (CaseWrong ==  1) req.addParameter('uid', '123456789123456789');  // USUARIO INEXISTENTE
        if (CaseWrong ==  2) req.addParameter('uid', '0XXX'); // USUARIO MAL FORMADO
        req.httpMethod = 'Get';  
        RestContext.request = req;
        System.debug('---REQ---->'+req);
  }

    @isTest private static void test_retrieveAccountNodeNoParameters()
    {
        createDataNoParameters(0);
        BI_FVI_AccountManager.retrieveAccountNode();
        createDataNoParameters(1);
        BI_FVI_AccountManager.retrieveAccountNode();
        createDataNoParameters(2);
        BI_FVI_AccountManager.retrieveAccountNode();
    }
   
    @isTest private static void test_retrieveAccountNode()
    {
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='No Cliente',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                  
                                                   BI_FVI_Nodo__c = nodeNcpDummy.Id,
                                                     BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test'
                                                   );
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;
        
        BI_FVI_AccountManager.retrieveAccountNode();
        accountDummy.BI_No_Identificador_fiscal__c='20818569322';
        update accountDummy;
        BI_FVI_AccountManager.retrieveAccountNode();

        nodeNcpDummy.OwnerId = lst_user[0].Id;
        update nodeNcpDummy;
        BI_FVI_AccountManager.retrieveAccountNode();
        
    }

    @isTest private static void cero(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='No Cliente',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                  
                                                   BI_FVI_Nodo__c = nodeNcpDummy.Id,
                                                  BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test'
                                                    );
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void uno(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='No Cliente',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                 
                                                   BI_FVI_Nodo__c = nodeNcpDummy.Id,
                                                    BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void dos(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='Cliente',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                   
                                                   BI_FVI_Nodo__c = nodeNcpDummy.Id,
                                                   BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void tres(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='Cliente',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                   
                                                   BI_FVI_Nodo__c = nodeNcpDummy.Id,
                                                   BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void cuatro(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c= null,
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                  
                                                   BI_FVI_Nodo__c = nodeNcpDummy.Id,
                                                   BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void cinco(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcSgId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                 
                                                  BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');

        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void seis(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_Subsegment_Local__c = 'Industria_y_Comercio',
                                                  BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }

    @isTest private static void siete(){
        createData();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=lst_user[0].Id,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        System.debug('Ids: '+lst_user[0].Id+'  '+vUserId);

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                     BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNcpDummy.Id);
        insert new_nag;  
        
        BI_FVI_AccountManager.retrieveAccountNode();
    }
    
    @isTest private static void test_ConsultaRUC(){
        Test.startTest();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;
              
        //Create dummy Node NAG
        BI_FVI_Nodos__c nodeNagDummy = new BI_FVI_Nodos__c(Name='Test Node NAG',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNagId,
                                                           //BI_FVI_NodoPadre__c=vNodeParentId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNagDummy;
        

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='No Cliente',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                  
                                                   BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;

        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNagDummy.Id);
        insert new_nag;  

        createData();
        
        BI_FVI_AccountManager.retrieveAccountNode();
        Test.stopTest();  
        
    }

    @isTest private static void test_ConsultaRUC2(){
        createData();
        Test.startTest();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;       
        
        //Create dummy Node NAG
        BI_FVI_Nodos__c nodeNagDummy = new BI_FVI_Nodos__c(Name='Test Node NAG',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNagId,
                                                           //BI_FVI_NodoPadre__c=vNodeParentId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNagDummy;

        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                                   Name = 'Test Account',
                                                   BI_Activo__c='Sí',
                                                   BI_Tipo_de_identificador_fiscal__c='RUC',
                                                   BI_No_Identificador_fiscal__c='20518569391',
                                                   BI_Country__c='Peru',
                                                   BI_FVI_Tipo_Planta__c='Móvil',
                                                   BI_Subsegment_Local__c = 'Negocios premium provincia FVI',
                                                    BI_Segment__c = 'test',
                                                    BI_Subsegment_Regional__c = 'test',
                                                    BI_Territory__c = 'test');
        Insert accountDummy;
        
        BI_FVI_AccountManager.retrieveAccountNode();
        Test.stopTest();  
      
    }
  
    
    @isTest private static void test_createAccountNode() {
        
        Test.startTest();
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;        
        
        //Create dummy Node NAG
        BI_FVI_Nodos__c nodeNagDummy = new BI_FVI_Nodos__c(Name='Test Node NAG',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNagId,
                                                           //BI_FVI_NodoPadre__c=vNodeParentId,
                                                           BI_FVI_Pais__c='Peru');
            Insert nodeNagDummy;
              
            //Create dummy Node NAV
            BI_FVI_Nodos__c nodeNavDummy = new BI_FVI_Nodos__c(Name='Test Node NAV',
                                                               OwnerId=vUserId,
                                                               RecordTypeId=vRecordTypeNodeNavId,
                                                               BI_FVI_NodoPadre__c=nodeNcpDummy.Id,
                                                               BI_FVI_Pais__c='Peru');
            Insert nodeNavDummy;            
            
            //Create dummy Account
            Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                               Name = 'Test Account',
                                               BI_Activo__c='Sí',
                                               BI_Tipo_de_identificador_fiscal__c='RUC',
                                               BI_No_Identificador_fiscal__c='20518569391',
                                               BI_Country__c='Peru',
                                               BI_FVI_Tipo_Planta__c='Fijo',
                                               BI_Subsegment_Local__c = 'Negocios premium provincia',
                                               BI_Segment__c = 'test',
                                               BI_Subsegment_Regional__c = 'test',
                                               BI_Territory__c = 'test');
        Insert accountDummy;
           

        RestRequest req2 = new RestRequest(); 
        RestResponse res = new RestResponse();

        req2.addHeader('Content-Type', 'application/json');
        req2.addParameter('xxxx', 'xxxy');
        req2.requestURI = '/services/apexrest/AccountManager';
        req2.httpMethod = 'POST';
        req2.requestBody = Blob.valueof('{"RUC":"20518569391","UserId":'+'"'+vUserId+'"'+'}');

        RestContext.request = req2;
        RestContext.response = res;

        String resultado2 = BI_FVI_AccountManager.createAccountNode(); 

        //Insert Old Vinculo de Nodo
        accountDummy.BI_FVI_Tipo_Planta__c='No Cliente';
        update accountDummy; 
        BI_FVI_Nodo_Cliente__c new_nag = new BI_FVI_Nodo_Cliente__c(
                    BI_FVI_IdCliente__c = accountDummy.Id,
                    BI_FVI_IdNodo__c = nodeNagDummy.Id);
        insert new_nag;  
        String resultado = BI_FVI_AccountManager.createAccountNode();         

        Test.stopTest();   
        
    }
    
    @isTest private static void test_existingAccount() {
        Test.startTest();

       RestRequest req2 = new RestRequest(); 
        RestResponse res = new RestResponse();

        req2.addHeader('Content-Type', 'application/json');
        req2.addParameter('xxxx', 'xxxy');
        req2.requestURI = '/services/apexrest/AccountManager';
        req2.httpMethod = 'POST';
        req2.requestBody = Blob.valueof('{"RUC":"205185693xx","UserId":"xxxxx"}');

        RestContext.request = req2;
        RestContext.response = res;

        String resultado = BI_FVI_AccountManager.createAccountNode(); 
        Test.stopTest();
        
    }
    
    @isTest private static void test_existingNodeNAV() {

        Test.startTest();
            Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                           Name = 'Test Account',
                                           BI_Activo__c='Sí',
                                           BI_Tipo_de_identificador_fiscal__c='RUC',
                                           BI_No_Identificador_fiscal__c='20518569391',
                                           BI_Country__c='Peru',
                                           BI_FVI_Tipo_Planta__c='No Cliente',
                                           BI_Subsegment_Local__c = 'Negocios premium provincia',
                                           BI_Segment__c = 'test',
                                           BI_Subsegment_Regional__c = 'test',
                                           BI_Territory__c = 'test');
        Insert accountDummy;
        
        //Create dummy Node NCP
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;
         //Create dummy Node NAV
        BI_FVI_Nodos__c nodeNavDummy = new BI_FVI_Nodos__c(Name='Test Node NAV',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNavId,
                                                           BI_FVI_NodoPadre__c=nodeNcpDummy.Id,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNavDummy;
        RestRequest req2 = new RestRequest(); 
        RestResponse res = new RestResponse();

        req2.addHeader('Content-Type', 'application/json');
        req2.addParameter('xxxx', 'xxxy');
        req2.requestURI = '/services/apexrest/AccountManager';
        req2.httpMethod = 'POST';
        req2.requestBody = Blob.valueof('{"RUC":"20518569391","UserId":'+'"'+vUserId+'"'+'}');

        RestContext.request = req2;
        RestContext.response = res;

        String resultado = BI_FVI_AccountManager.createAccountNode(); 
        Test.stopTest();
    }

   @isTest private static void test_existingNodeNAV2() {

        Test.startTest();
        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                           Name = 'Test Account',
                                           BI_Activo__c='Sí',
                                           BI_Tipo_de_identificador_fiscal__c='RUC',
                                           BI_No_Identificador_fiscal__c='20518569391',
                                           BI_Country__c='Peru',
                                           BI_FVI_Tipo_Planta__c='No Cliente',
                                           BI_Subsegment_Local__c = 'Negocios premium provincia',
                                           BI_Segment__c = 'test',
                                          BI_Subsegment_Regional__c = 'test',
                                            BI_Territory__c = 'test');
        Insert accountDummy;
               
        //Create dummy Node NCP
        BI_FVI_Nodos__c nodeNcpDummy = new BI_FVI_Nodos__c(Name='Test Node NCP',
                                                           OwnerId=vUserId,
                                                           RecordTypeId=vRecordTypeNodeNcpId,
                                                           BI_FVI_Pais__c='Peru');
        Insert nodeNcpDummy;

        RestRequest req2 = new RestRequest(); 
        RestResponse res = new RestResponse();

        req2.addHeader('Content-Type', 'application/json');
        req2.addParameter('xxxx', 'xxxy');
        req2.requestURI = '/services/apexrest/AccountManager';
        req2.httpMethod = 'POST';
        req2.requestBody = Blob.valueof('{"RUC":"20518569391","UserId":'+'"'+vUserId+'"'+'}');

        RestContext.request = req2;
        RestContext.response = res;

        String resultado = BI_FVI_AccountManager.createAccountNode(); 
        Test.stopTest();
  }

  @isTest private static void test_AccountNOCliente() {

        Test.startTest();
        Account accountDummy = new Account(RecordTypeId=vRecordTypeAccountId,
                                           Name = 'Test Account',
                                           BI_Activo__c='Sí',
                                           BI_Tipo_de_identificador_fiscal__c='RUC',
                                           BI_No_Identificador_fiscal__c='20518569391',
                                           BI_Country__c='Peru',
                                           BI_FVI_Tipo_Planta__c='Fijo',
                                           BI_Subsegment_Local__c = 'Negocios premium provincia',
                                           BI_Segment__c = 'test',
                                           BI_Subsegment_Regional__c = 'test',
                                           BI_Territory__c = 'test');
        Insert accountDummy;
               

        RestRequest req2 = new RestRequest(); 
        RestResponse res = new RestResponse();

        req2.addHeader('Content-Type', 'application/json');
        req2.addParameter('xxxx', 'xxxy');
        req2.requestURI = '/services/apexrest/AccountManager';
        req2.httpMethod = 'POST';
        req2.requestBody = Blob.valueof('{"RUC":"20518569391","UserId":"xxxx"}');

        RestContext.request = req2;
        RestContext.response = res;

        String resultado = BI_FVI_AccountManager.createAccountNode(); 
        Test.stopTest();
  }
 
}