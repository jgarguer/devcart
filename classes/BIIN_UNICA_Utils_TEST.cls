@isTest(seeAllData = false)
public class BIIN_UNICA_Utils_TEST 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González Beltrán
    Company:       HPE
    Description:   Test method to manage the code coverage for BIIN_Obtener_Ticket_WS
    
    <Date>          <Author>                                <Change Description>
    05/2016         José Luis González Beltrán              Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testSetup static void dataSetup()
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
    }

    @isTest static void test_BIIN_UNICA_Utils_TEST()
    {
        Test.startTest(); 
            
        String remedyValue = BIIN_UNICA_Utils.translateFromSalesForce('Prioridad', 'Alta');
        String remedyId = BIIN_UNICA_Utils.translateFromSalesForceToID('Prioridad', 'Alta');
        remedyValue = BIIN_UNICA_Utils.translateFromRemedyIDToRemedyValue('Prioridad', 1);
        remedyId = BIIN_UNICA_Utils.translateFromRemedyValueToRemedyID('Prioridad', 'High');
        String salesforceValue = BIIN_UNICA_Utils.translateFromRemedy('Prioridad', 'High');
        salesforceValue = BIIN_UNICA_Utils.translateFromRemedyByID('Prioridad', 1);

        String url = BIIN_UNICA_Utils.addParameterAdditional('', 'name', 'value');
        url = BIIN_UNICA_Utils.addParameter('', 'name', 'value');

        String seconds = BIIN_UNICA_Utils.dateTimeToSeconds(DateTime.now());

        BIIN_UNICA_Pojos.TicketStatusType ticketStatusType = BIIN_UNICA_Utils.stringToEnumValueForTicketStatusType('NEW');
        BIIN_UNICA_Pojos.ContactStatusType contactStatusType = BIIN_UNICA_Utils.stringToEnumValueForContactStatusType('active');
        
        Test.stopTest();
    }
}