/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Rubén Suárez
    Company:        Avanxo
    Description:    Clase controladora de guadar las notas de trabajo
                    de los casos en la página "BI2_COL_PCA_NewWorkNote"

    History:

    <Date>          <Author>            <Description>
    28/02/2017      Rubén Suárez        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class BI2_COL_PCA_NewWorkNote_ctr extends BIIN_BaseController
{
    public case caso {get; set;}
    public String caseId;
    public String  casoNota{get; set;}
    public Transient  Blob adjunto1 {get; set;}
    public String nombre1 {get; set;}
    public Transient  Blob adjunto2 {get; set;}
    public String nombre2 {get; set;}
    public Transient  Blob adjunto3 {get; set;}
    public String nombre3 {get; set;}
    public String  mensaje{get; set;}
    BIIN_Crear_WorkInfo_WS.CrearWISalida respuestaWI = new BIIN_Crear_WorkInfo_WS.CrearWISalida();

    //Constructor - Carga la información del caso
    public BI2_COL_PCA_NewWorkNote_ctr(ApexPages.StandardController controller)
    {
        caseId = System.currentPagereference().getParameters().get('Idcaso');
        System.debug('####Id del caso==== '+caseId);
        if(caseId != null && caseId != ''){
            caso = [SELECT Id, caseNumber, Status, BI_Id_del_caso_Legado__c, AccountId FROM Case Where Id=:caseId];
        }
    }

    // Método para crear la información de la nota de trabajo en remedy
    public void crearWIRoD(){
        String viewAccess = 'Public';
        List<BIIN_Crear_WorkInfo_WS.Adjunto> la = new List<BIIN_Crear_WorkInfo_WS.Adjunto>();
				System.debug(' \n\n caso.Status = '+ caso.Status +' \n\n');
        if((caso.Status != 'Cancelado') && (caso.Status != 'Cerrado')){
            if (adjunto1 != null) {
                system.debug('Adjuntamos 1');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre1,adjunto1);
                la.add(a);
            }
            if (adjunto2 != null) {
                system.debug('Adjuntamos 2');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre2,adjunto2);
                la.add(a);
            }
            if (adjunto3 != null) {
                system.debug('Adjuntamos 3');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre3,adjunto3);
                la.add(a);
            }
            BIIN_Crear_WorkInfo_WS cwi=new BIIN_Crear_WorkInfo_WS();
            String authorizationToken = '';
						System.debug(' \n\n la.size() = '+ la.size() +' \n\n');
						if(la.size()>0)
						{
								respuestaWI = cwi.crearWorkInfo(authorizationToken, caso, la, casoNota, viewAccess);
						}

            if(respuestaWI!=null&&respuestaWI.code=='000'){
                system.debug('EXITO: Se ha creado el WI correctamente');
                mensaje = 'Nota de trabajo añadida correctamente';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,mensaje));
            }else{
                system.debug('ERROR: No se ha creado el WI correctamente');
                mensaje = 'ERROR: No se ha creado el WI correctamente o no se adjunto un archivo';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,mensaje));
            }
        }else{
            system.debug('No se crea en RoD ya que no es un estado válido');
            mensaje = 'No se crea en RoD ya que no es un estado válido';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,mensaje));
        }
    }
}