/*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Carlos Bastidas
    Company:       Everis.com
    Description:   Class that managed of modal CWP_EquipoTelefonicaModalCtrl.
    
    History:
    
    <Date>            <Author>              <Description>
    17/03/2017        Carlos Bastidas       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public without sharing class CWP_EquipoTelefonicaModalCtrl {
    
    /*Method to return the initial info for modal CWP_EquipoTelefonicaModalCtrl*/
    @AuraEnabled
    public static EquipoTelefonicaModalWrapper getloadInfo (String id,String type,String back,String clase){ 
        // datos para la modal 
        EquipoTelefonicaModalWrapper datos = new EquipoTelefonicaModalWrapper();
        
        datos.UserId = id!= null && !String.isEmpty(id)?id:null;
        datos.Type = type!= null && !String.isEmpty(type)?type:null;
        datos.Back = back!= null && !String.isEmpty(back)?back:null;
        datos.teamType = clase!= null && !String.isEmpty(clase)?clase:null;
        
        List<User> UserifOwner = new List<User>();
        List<User> internalUser = new List<User>();
        
        String listAux;
        
        List<Hierarchy__c> UserHierarchy = new List<Hierarchy__c>();
        List<Account> OwnAccount = new List<Account>();
        
        
        User user;
        
        
        UserHierarchy = [Select Id, AboutMe__c, MobilePhone__c, ShowPersonalData__c, Description__c, User__c, NameUser__c, Contact__c, 
                         Email__c, Phone__c, Nivel_de_escalado__c, Role__c From Hierarchy__c where User__c =: datos.UserId];
        
        if(datos.Back != 'false' || datos.Back == null) {
            datos.Back = 'true';
        }
        
        internalUser = [Select Id, Name, ContactId, Email,Contact.AccountId, phone, aboutme, MobilePhone, UserRole.Name From User where id = : datos.UserId limit 1];
        
        UserifOwner = [Select Id, Name, ContactId, Email,Contact.AccountId, phone, aboutme, MobilePhone, UserRole.Name From User where id = :  Userinfo.getUserId() limit 1];       
        
        if(UserifOwner[0].Contact.AccountId != null){
            OwnAccount = [SELECT Owner.Id FROM Account where Id = : BI_AccountHelper.getCurrentAccountId()];            
        }
        
        for(AccountTeamMember at :[Select TeamMemberRole From AccountTeamMember where UserId =:datos.UserId limit 1]){
            datos.Role = at.TeamMemberRole;
        }
        
        datos.Bigphoto = getPhotoConnectApi(datos.UserId);
        
        String escaladoStr = 'escalado';
        if(datos.teamType == escaladoStr && (!UserHierarchy.isEmpty()) && (UserHierarchy[0].ShowPersonalData__c == false)){
            datos.EmailUser = Label.PCA_NoDisponible;
            datos.PhoneUser = Label.PCA_NoDisponible; 
            datos.MobilePhoneUser = Label.PCA_NoDisponible;
        } else{
            datos.EmailUser = internalUser[0].Email;
            datos.PhoneUser = internalUser[0].phone; 
            datos.MobilePhoneUser = internalUser[0].MobilePhone;
        }
        datos.userName = internalUser[0].Name;
        datos.aboutme = internalUser[0].aboutme;
        
        if(datos.teamType=='Equipo') {
            
            if(UserifOwner[0].Contact.AccountId != null && OwnAccount[0].Owner.Id == datos.UserId){               
                
                datos.Role = Label.PCA_Propietario;   
                
            } 
        } else if (datos.teamType == escaladoStr) {
            datos.Role = internalUser[0].UserRole.Name;
        }
        user = [Select Id, Name, ContactId, Email, phone, FullPhotoUrl, Contact.AccountId From User where id = : Userinfo.getUserId()];
        
        datos.emailFrom = user.Email;

        System.debug('DATOS: '+ datos);
        return datos;
    }
    /*Method to return the Photo Link*/
    public static String getPhotoConnectApi (String iduser) {
        String photoLink = '';
        for (User us:[SELECT FullPhotoUrl  FROM User WHERE Id =:iduser or ContactId =:iduser or  ProfileId =:iduser limit 1]){
            photoLink = us.FullPhotoUrl;
        }
        
        return photoLink;
    }
    
    /*Method to return the labels for Phone and MobilePhone*/
    @AuraEnabled
    public static String[] getLabels (){
        List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{'User'}); // this can accept list of strings, we describe only one object here
        System.debug(describeSobjectsResult);
        String objectLabel = describeSobjectsResult[0].getLabel();
        Map<String, Schema.SObjectField> allFields = describeSobjectsResult[0].fields.getMap();
        String phoneLabel = allFields.get('Phone').getDescribe().getLabel();
        String mobilePhoneLabel = allFields.get('MobilePhone').getDescribe().getLabel();
        
        String[] labels = new String[]{phoneLabel,mobilePhoneLabel};   
            return labels;
    }
    
    /*Class Equipo Telefonica Modal Wrapper*/
    public class EquipoTelefonicaModalWrapper {
        
        @AuraEnabled
        public String UserId {get; set;}
        @AuraEnabled
        public String Type {get; set;}
        @AuraEnabled
        public String Back {get; set;}
        @AuraEnabled
        public String teamType {get; set;}
        @AuraEnabled
        public String Bigphoto {get; set;}
        @AuraEnabled
        public String EmailUser {get; set;}
        @AuraEnabled
        public String PhoneUser {get; set;} 
        @AuraEnabled
        public String MobilePhoneUser {get; set;}
        @AuraEnabled
        public String userName {get; set;}
        @AuraEnabled
        public String aboutme {get; set;}
        @AuraEnabled
        public String Role {get; set;}
        @AuraEnabled
        public String emailFrom {get; set;}
    }
    
}