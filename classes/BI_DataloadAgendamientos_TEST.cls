@isTest
private class BI_DataloadAgendamientos_TEST {
	

	static{
    	
    	BI_TestUtils.throw_exception = false;
    }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_DataloadAgendamientos
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/12/2015                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void test_method_one() {
		    list<BI_Linea_de_Recambio__c> lst_rec = BI_DataloadAgendamientos.loadLineaRecambio_Inicial(1);
		    list<BI_Solicitud_de_plan__c> lst_sdp = BI_DataloadAgendamientos.loadSolicitudPlan(1);
		    list<BI_Solicitud_de_plan__c> lst_sdp2 = BI_DataloadAgendamientos.loadSolicitudPlan_SinEstado(1);
		    BI_DataloadAgendamientos.UpdateLineaRecambio(lst_rec);
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_DataloadAgendamientos
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/12/2015                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void test_method_two() {
		list<BI_Linea_de_Servicio__c> lst_lserv = BI_DataloadAgendamientos.loadLineaServicio(1);

		BI_DataloadAgendamientos.UpdateLineaServicio( lst_lserv );

	    for (BI_Linea_de_Servicio__c in_lserv: lst_lserv ){
	    	system.assertEquals( in_lserv.BI_Estado_de_linea_de_servicio__c, ' new test estado 1' );
	    }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_DataloadAgendamientos
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/12/2015                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void test_method_three(){

		List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});
		List <BI_Solicitud_envio_productos_y_servicios__c> lst_sol = BI_DataloadAgendamientos.loadSolicitudEnvio(1,lst_acc[0],true);
		List <BI_Linea_de_Recambio__c> lst_lr = BI_DataLoadAgendamientos.loadLineaRecambio(1, lst_sol[0].Id, lst_acc[0]);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_DataloadAgendamientos
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/12/2015                      Guillermo Muñoz             Initial Version
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void test_method_four(){
		User usu = BI_DataloadAgendamientos.loadValidAdministratorUser();
		List <BI_Linea_de_Venta__c> lst_lv;
		System.runAs(usu){
			lst_lv = BI_DataloadAgendamientos.loadLineaVentaWithSolicitud(1);
		}
	    BI_DataloadAgendamientos.UpdateLineaVenta(lst_lv);
	    BI_DataloadAgendamientos.UpdateLineaVentaStatus(lst_lv);
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_DataloadAgendamientos
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/12/2015                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void test_method_five(){
		List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});
		List <BI_Solicitud_envio_productos_y_servicios__c> lst_sol = BI_DataloadAgendamientos.loadSolicitudEnvio(1,lst_acc[0],true);
		List <BI_Linea_de_Venta__c> lst_lv2 = BI_DataloadAgendamientos.loadLineaVentaWithSolicitud(1,lst_sol[0].Id );
	}

}