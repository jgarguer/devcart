@IsTest
private class PCA_Installed_Services_Controller_TEST {

 /**   static testmethod void loadServicesFromFIdTest() {
        System.debug('### TestMethod getLoadInfo() START: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Community') AND isActive = true LIMIT 1];
        User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS Customer Community Plus') AND isActive = true LIMIT 1];
        //final Id accountId = userTest.Contact.AccountId;
        System.runAs(userTest){
            createDummyCategory();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('fId', testCategory.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            Test.stopTest();
        }
        System.debug('### TestMethod getLoadInfo() END: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());
    }
        
    static testmethod void loadServicesFromSuIdTest() {
        System.debug('### TestMethod getLoadInfo() START: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Community') AND isActive = true LIMIT 1];
        User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS Customer Community Plus') AND isActive = true LIMIT 1];
        final Id accountId = userTest.Contact.AccountId;
        System.runAs(userTest){
            Test.startTest();
            NE__OrderItem__c orderItem = TGS_Dummy_test_Data.dummyOrderItem();
            NE__OrderItem__c orderItem2 = [SELECT Id, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Id,
                                           NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c
                                           FROM NE__OrderItem__c WHERE Id = :orderItem.Id LIMIT 1];
            ApexPages.currentPage().getParameters().put('suId', orderItem2.NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();    
            controller.refreshServices();
            controller.selectedCategory = orderItem2.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Id;
            controller.refreshServices();
            Test.stopTest();
            
        }
        System.debug('### TestMethod getLoadInfo() END: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());
    }
 */   
    /*JV  static testmethod void loadServicesFromSecurityServicesIdTest() {
        //User userTest = [SELECT Id, Name, Contact.AccountId FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Community') AND isActive = true LIMIT 1];
        //User userTest = [SELECT Id, Name, Contact.Id FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS Customer Community Plus') AND isActive = true LIMIT 1];
        Account le = TGS_Dummy_Test_Data.dummyHierarchy();
        User userTest = TGS_Dummy_Test_Data.dummyCustomerCommunityPlusUser(le);
        createDummyConfigurationSecurityServices(le.Id, userTest.ContactId);
        System.runAs(userTest){
            Test.startTest();
            ApexPages.currentPage().getParameters().put('fId', testCategory.Id);
            PCA_Installed_Services_Controller controller = new PCA_Installed_Services_Controller();
            Test.stopTest();
        }
    }*/

    
    static testmethod void loadServicesFromSecurityServicessuIdTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
        	TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Installed_Services_Controller controller = new PCA_Installed_Services_Controller();
            controller.selectedService = testProduct.Id;
            controller.refreshServices();
            controller.searchServiceUnits();
            controller.filtroId = '000';
            controller.listaCampos[0].value = '000';
            controller.searchServiceUnits();
            Test.stopTest();
        }
    }
    
        static testmethod void loadServicesFromSMDMTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Secure Mobile Device, Apps and Content Management' LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller(); 
            controller.refreshServices();
            //System.assert(controller.listaCampos != null && controller.listaCampos.size() > 0);
            controller.filtroId = '000';
            controller.listaCampos[0].value = '000';
            controller.filtroLE = '000';
        	controller.filtroBU = '000';
            controller.filtroCC = '000';
            controller.filtroSite = '000';
            controller.searchServiceUnits();
            Test.stopTest();
        }
    }
    
    static testmethod void loadServicesWithoutTiers() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
           	NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Catalog_Category__c testSubCategory = [SELECT Id, NE__Parent_Category_Name__c FROM NE__Catalog_Category__c WHERE Id IN (SELECT NE__Catalog_Category_Name__c FROM NE__Catalog_Item__c WHERE NE__ProductId__c = :testOrderItem.NE__ProdId__c) LIMIT 1];
    		NE__Catalog_Category__c testCategory = [SELECT Id, Name FROM NE__Catalog_Category__c WHERE Id = :testSubCategory.NE__Parent_Category_Name__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('fId', testCategory.Name);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            controller.selectedCategory = null;
            controller.selectedService = null;
            controller.refreshServices();
            controller.searchServiceUnits();
            Test.stopTest();
        }
    }
    
    static testmethod void loadServicesWithoutService() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
           	NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Catalog_Category__c testSubCategory = [SELECT Id, Name, NE__Parent_Category_Name__c FROM NE__Catalog_Category__c WHERE Id IN (SELECT NE__Catalog_Category_Name__c FROM NE__Catalog_Item__c WHERE NE__ProductId__c = :testOrderItem.NE__ProdId__c) LIMIT 1];
    		NE__Catalog_Category__c testCategory = [SELECT Id, Name FROM NE__Catalog_Category__c WHERE Id = :testSubCategory.NE__Parent_Category_Name__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('fId', testCategory.Name);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            controller.selectedCategory = testSubCategory.Name;
            controller.selectedService = null;
            controller.refreshServices();
            controller.searchServiceUnits();
            Test.stopTest();
        }
    }
    
    static testmethod void loadServicesFromfIdTest() {
		User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
           	NE__OrderItem__c testOrderItem = [SELECT Id, Name, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Catalog_Category__c testSubCategory = [SELECT Id, name, NE__Parent_Category_Name__c FROM NE__Catalog_Category__c WHERE Id IN (SELECT NE__Catalog_Category_Name__c FROM NE__Catalog_Item__c WHERE NE__ProductId__c = :testOrderItem.NE__ProdId__c) LIMIT 1];
    		NE__Catalog_Category__c testCategory = [SELECT Id, Name FROM NE__Catalog_Category__c WHERE Id = :testSubCategory.NE__Parent_Category_Name__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('fId', testCategory.Name);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            controller.selectedCategory = testSubCategory.Name;
            controller.selectedService = testOrderItem.Id;
            // JMF 01/04/2016 - Commented both asserts
            //System.assertNotEquals(null, controller.selectedService);
            //System.assertEquals(null, controller.selectedService);
            controller.refreshServices();
            controller.searchServiceUnits();
			Test.stopTest();
        }
    }
    
    static testmethod void loadServicesFromsuIdTest() {
    	User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Installed_Services_Controller controller = new PCA_Installed_Services_Controller();
            controller.filtroId = '000';
            controller.listaCampos[0].value = '000';
            controller.searchServiceUnits();
            Test.stopTest();
        }
    }
    
    static testmethod void newModificationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
        	NE__OrderItem__c testOrderItem = [SELECT Id FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('selectedServiceUnit', testOrderItem.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            Test.stopTest();
            controller.newModification();
            System.assertNotEquals(controller.returnPage, 'KO');
        }
    }
    
    static testmethod void newModificationKOTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            Test.stopTest();
            controller.newModification();
            System.assertEquals(controller.returnPage, 'KO');
        }
    }
    
    static testmethod void selectCatalogItemTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name, TGS_CWP_Tier_1__c, TGS_CWP_Tier_2__c FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            testProduct.TGS_CWP_Tier_1__c='Enterprise Managed Mobility';
            testProduct.TGS_CWP_Tier_2__c='Universal WIFI';
            update testProduct;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('selectedService', testProduct.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            System.assertNotEquals(null, controller.newRegistration());
            Test.stopTest(); 
        }
    }
    
    static testmethod void newRegistrationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('selectedService', testProduct.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            System.assertNotEquals(null, controller.newRegistration());
            Test.stopTest(); 
        }
    }
    
    static testmethod void newRegistrationKOTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('selectedService', testProduct.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            List<SelectOption> currencies = new List<SelectOption>();
            currencies.add(new SelectOption('EUR', 'EUR'));
            //Primer test: Currency no seleccionada
            controller.listaCurrencies = currencies;
            controller.serviceWithCurrency = true;
            controller.selectedCurrency = null;
            System.assertEquals(null, controller.newRegistration());
            //Segundo test: Account sin Currency
            controller.listaCurrencies = new List<SelectOption>();
            controller.serviceWithCurrency = true;
            System.assertEquals(null, controller.newRegistration());
            //Tercer test: Sin CI
            controller.listaCurrencies = currencies;
            controller.serviceWithCurrency = true;
            controller.selectedCurrency = 'EUR';
            controller.selectedCategory = null; // JMF 01/04/2016
            controller.selectedCI = null;
            System.assertEquals(null, controller.newRegistration());
            Test.stopTest(); 
        }
    }
    
    static testmethod void newTerminationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('selectedServiceUnit', testOrderItem.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();  
            controller.newTermination();
            System.assertNotEquals(controller.selectedCI, '');
            Test.stopTest();
        }
    }
    
    static testmethod void newTerminationKOTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();  
            controller.newTermination();
            Test.stopTest();
            System.assertNotEquals(controller.selectedCI, '');
        }
    }

    static testmethod void clearFiltersTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
        	TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Installed_Services_Controller controller = new PCA_Installed_Services_Controller();
            controller.selectedService = testProduct.Id;
            controller.refreshServices();
            controller.searchServiceUnits();
            controller.filtroId = '000';
            controller.clearFilters();
            System.assertEquals('', controller.filtroId);
            Test.stopTest();
        }
    }
    
    static testmethod void eOrderingItemAndChangesTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            NE__OrderItem__c testOrderItem = [SELECT Id, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name,TGS_CWP_Tier_2__c FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
			testProduct.TGS_CWP_Tier_2__c='Managed SIP Trunking Numbering';
			testProduct.Name='Special Number';
			update testProduct;
			NE__Catalog_Item__c testCatalogItem= [SELECT Id, NE__Technical_Behaviour__c, NE__ProductId__c FROM NE__Catalog_Item__c WHERE NE__ProductId__c = :testProduct.Id LIMIT 1];
			testCatalogItem.NE__Technical_Behaviour__c='Service with pre-approved changes;Service not available for e-Ordering';
			update testCatalogItem;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('fId', 'UNIFIED COMMUNICATIONS AND COLLABORATION');
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller(); 
            Test.stopTest(); 
            controller.eOrderingItemAndChanges(testCatalogItem.Id);
            System.assertNotEquals(controller.selectedCI, '');
			System.assertEquals(controller.esEordering, 'False');
			System.assertEquals(controller.availableChanges, 'True');
        }
    
	}    
    
    static testmethod void loadServicesWithCurrenciesTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(userTest.Contact.AccountId, userTest.ContactId);
            Test.startTest();
           	NE__OrderItem__c testOrderItem = [SELECT Id, Name, NE__ProdId__c FROM NE__OrderItem__c WHERE NE__ProdId__r.Name = 'Universal WIFI' LIMIT 1];
            NE__Catalog_Category__c testSubCategory = [SELECT Id, name, NE__Parent_Category_Name__c FROM NE__Catalog_Category__c WHERE Id IN (SELECT NE__Catalog_Category_Name__c FROM NE__Catalog_Item__c WHERE NE__ProductId__c = :testOrderItem.NE__ProdId__c)];
    		NE__Catalog_Category__c testCategory = [SELECT Id, Name FROM NE__Catalog_Category__c WHERE Id = :testSubCategory.NE__Parent_Category_Name__c];
            
            ApexPages.currentPage().getParameters().put('fId', testCategory.Name);
            PCA_Installed_Services_Controller controller  = new PCA_Installed_Services_Controller();
            controller.selectedCategory = testSubCategory.Name;
            controller.selectedService = testOrderItem.NE__ProdId__c;
            controller.searchServiceUnits();
            controller.refreshServices();
			Test.stopTest();
        }
    }
}