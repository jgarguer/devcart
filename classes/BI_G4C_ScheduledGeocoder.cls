global class BI_G4C_ScheduledGeocoder implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        Integer limite=2200;
        String q='SELECT Id, BI_G4C_Estado_Coordenadas__c, ShippingGeocodeAccuracy FROM Account WHERE BI_G4C_Estado_Coordenadas__c=\'retryCall\' AND BI_Activo__c!=\'No\' ORDER BY LastModifiedDate ASC LIMIT ' +limite;
        
        List<Account> accs = Database.query(q);
        if (accs.size()>0){
            for (Account c:accs){
                c.BI_G4C_Estado_Coordenadas__c='Pendiente Regla';
                c.ShippingGeocodeAccuracy='city';  
            }
       
        	update accs;
		}
    }    
}