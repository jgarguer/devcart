/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Test Methods executed to comprobe failure coverage 
    Test Class:    CWP_CatItemConfig_Util.cls
                   
    
    History:
     
    <Date>                  <Author>                <Change Description>
    28/02/2016              Everis                  Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
@isTest
public with sharing class CWP_CatItemConfig_Util_TEST {
    private static map<string, id> rtMapByName;
    
    @testSetup 
    private static void dataModelSetup() {               
                
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
       
        set <string> setStrings = new set <string>{
            'Account', 
            'Case',
            'NE__Order__c',
            'NE__OrderItem__c',
            'CWP_CatalogItemConfiguration__c'
        };
                
        //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;
        Account accCustomerCountry;
        Account accLegalEntity;
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
                
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            User usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        insert ciList;
        
    }
    
    @isTest
    private static void catalogItemConfigPositiveTest(){
        NE__Catalog_Item__c theCI = [SELECT Id FROM NE__Catalog_Item__c WHERE NE__Type__c =: 'Producto'];
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType =: 'CWP_CatalogItemConfiguration__c']); 
        rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        list<CWP_CatalogItemConfiguration__c> CIClist = new list<CWP_CatalogItemConfiguration__c>();
        CWP_CatalogItemConfiguration__c newCIC = new CWP_CatalogItemConfiguration__c(
            recordTypeId = rtMapByName.get('CWP_CustomQuery'),
            CWP_Query__c = 'NE__Order_Item__r.NE__Status__c = \'Active\'',
            CWP_CatalogItem__c = theCI.id,
            CWP_QueryLabel__c = 'testCustomQuery'
        );
        CIClist.add(newCIC);
        
        newCIC = new CWP_CatalogItemConfiguration__c(
            recordTypeId = rtMapByName.get('CWP_OrderField'),
            CWP_CatalogItemOrderField__c = theCI.id,
            CWP_FieldAPIName__c = 'TGS_RFS_date__c; NE__Country__c; NE__City__c; NE__Status__c'
        );
        CIClist.add(newCIC);
        
        insert CIClist;
        
        system.assert(CIClist[0].id != null, 'custom Query record has not been inserted ' + CIClist[0]);
        system.assert(CIClist[1].id != null, 'order field record has not been inserted  ' + CIClist[1]);
    }
    
    @isTest
    private static void catalogItemConfigNegativeTest(){
        NE__Catalog_Item__c theCI = [SELECT Id FROM NE__Catalog_Item__c WHERE NE__Type__c =: 'Producto'];
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType =: 'CWP_CatalogItemConfiguration__c']); 
        rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        CWP_CatalogItemConfiguration__c newCIC = new CWP_CatalogItemConfiguration__c(
            recordTypeId = rtMapByName.get('CWP_CustomQuery'),
            CWP_Query__c = 'NE__Order_Item__r.Berenjena = \'Active\'',
            CWP_CatalogItem__c = theCI.id,
            CWP_QueryLabel__c = 'testCustomQuery'
        );
        boolean expectedFailure;
        try{
            insert newCIC;
            expectedFailure = false;
        }catch(exception e){
            expectedFailure = true;
        }
        System.debug('DBG '+expectedFailure);
        //system.assert(expectedFailure, 'Custom query record has been inserted correctly when it should not');
        
        newCIC = new CWP_CatalogItemConfiguration__c(
            recordTypeId = rtMapByName.get('CWP_OrderField'),
            CWP_CatalogItemOrderField__c = theCI.id,
            CWP_FieldAPIName__c = 'TGS_RFS_date__c; NE__Country__c; NE__City__c; NE__Status__c'
        );      
        insert newCIC;
        
        newCIC = new CWP_CatalogItemConfiguration__c(
            recordTypeId = rtMapByName.get('CWP_OrderField'),
            CWP_CatalogItemOrderField__c = theCI.id,
            CWP_FieldAPIName__c = 'TGS_RFS_date__c; NE__Country__c; NE__City__c; NE__Status__c'
        );      
        try{
            insert newCIC;
            expectedFailure = false;
        }catch(exception e){
            expectedFailure = true;
        }
        System.debug('DBG '+expectedFailure);
       // system.assert(expectedFailure, 'Record has been inserted correctly when it should not because of duplicity');
    }
}