public  class BIIN_CreacionIncidencia_Ctrl
{    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
	Company:       Deloitte
	Description:   Clase para recoger valores de los campos del caso y crearlo
	    
	History:
	    
	<Date>           <Author>        							  	            <Description>
	10/12/2015       Ignacio Morales Rodríguez, Jose María Martín     		 	Initial version
    17/06/2016       José Luis González Beltrán                                 Adapt to UNICA, onClickGuardarFuturo()
    12/07/2016       José Luis González Beltrán                                 Fix case status when saving
    12/07/2016       José Luis González Beltrán                                 Fix error when saving attachments
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    BIIN_Creacion_Ticket_WS.CamposOutsiders co=new BIIN_Creacion_Ticket_WS.CamposOutsiders();
    public Case caso {get;set;}
    //public Attachment attach {get;set;} 
    public String  contact_Id {get; set;}
    public String account_Id {get; set;}
    public contact contactoRelated {get; set;}
    public Account accountRelated {get; set;}
    //public boolean MasDeTres {get; set;}
    public boolean displayPopup {get; set;} 
    public boolean displayPopup2 {get; set;} 
    public boolean displayPopup3 {get; set;}
    public boolean displayPopup4 {get; set;}
    public boolean displayPopUpAdjuntos  {get; set;}
    //public boolean YaAdjuntado {get; set;}
    //public List <Attachment> ListaAdjuntos; //Moved to method
    //public List <String> ListaNombres {get; set;} 
    //public integer ContAdj {get; set;}   //Meter en el método, creo que no hace falta como variable global
    public String siteName {get; set;}    
    public String emailContacto {get; set;}
    public String nombreContacto {get; set;}
    public String apellidoContacto {get; set;}
    public Id EmailInput {get; set;}   
    public String CIname {get; set;}
    public Id CIinput {get; set;}
    public Id siteInput {get; set;}
    public String StringWSCreacion {get; set;}
    public String valFiscal {get; set;}
    public String rutaBmc {get; set;}
    public boolean MostrarPopUpCategOperac {get; set;}
    public Transient List<BIIN_Creacion_Ticket_WS.Adjunto> la; //Transient in order to down the view state size.
    public boolean UpdateCatDistintas;

    public Transient  Blob adjunto1 {get; set;}
    public String nombre1 {get; set;}
    public Transient  Blob adjunto2 {get; set;}
    public String nombre2 {get; set;}
    public Transient  Blob adjunto3 {get; set;}
    public String nombre3 {get; set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Constructor, donde se definen las variables globales y se toman los valores heredados de páginas padre como el id contacto, cuenta,
						el caso padre.
	    
	    History:
	    
	    <Date>            <Author>          			<Description>
	    12/12/2015     Ignacio Morales Rodríguez    	Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/	

        public BIIN_CreacionIncidencia_Ctrl(ApexPages.StandardSetController controller){      
            caso = new Case(); 
            system.debug('Categorias Constructor' + caso.BIIN_Categorization_Tier_1__c + caso.BIIN_Categorization_Tier_2__c + caso.BIIN_Categorization_Tier_3__c);
            NamedCredential nc=[SELECT endpoint FROM NamedCredential WHERE developerName='BIIN_RoD' LIMIT 1];
            rutaBmc=nc.endpoint;
            if(Test.isRunningTest()){
                siteName = '';
                emailContacto  = '';
                nombreContacto   = '';
                apellidoContacto   = '';
                EmailInput   = caso.id;
                CIname   = '';
                CIinput   = caso.id;
                siteInput     = caso.id; 
            } 
            UpdateCatDistintas = false;
            MostrarPopUpCategOperac = false;
            StringWSCreacion = '';
            displayPopup2 = false;
            displayPopup3 = false;
            displayPopup4 = false;
            displayPopUpAdjuntos = false;
        //ContAdj = 0;
        //YaAdjuntado = false;
        //MasDeTres = false;
        caso.OwnerId = Userinfo.getUserId(); 
        contact_Id = System.currentPagereference().getParameters().get('def_contact_id'); 
        account_Id = System.currentPagereference().getParameters().get('def_account_id');
        string ParentNumberRelated = System.currentPagereference().getParameters().get('cas28');
        String tipoRegistroId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BIIN_Solicitud_Incidencia_Tecnica' limit 1].Id;
        caso.recordtypeid = tipoRegistroId;
        caso.Type='First instance'; 
        caso.Reason = 'Incidencia Técnica';
        contactoRelated = new contact();
        accountRelated = new Account();
        if(contact_Id != null){
            contactoRelated = [SELECT Name, Id FROM contact WHERE Id=: contact_Id];
        }
        if(account_Id != null){
         List<Account> lst_acc = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: account_Id];
            //accountRelated = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: account_Id];
            
            if(!lst_acc.isEmpty()){
                accountRelated = lst_acc[0];
                caso.AccountId = account_Id;
                caso.BI_Country__c = accountRelated.BI_Country__c;
            }
            
        }      
        if(contact_Id != null){
            caso.ContactId = contact_Id;
        }
        if(ParentNumberRelated != null){
            caso.ParentId = [SELECT Id FROM case WHERE caseNumber =: ParentNumberRelated ].Id;
            system.debug( 'Id Caso Padre -->' + caso.ParentId );
        }
        
        //attach = new Attachment();       
        //ListaAdjuntos  = new List <Attachment> (); //Moved to method
        //ListaNombres  = new List <String> ();       
        
        //INICIO MODIFICACION: Recoge los parameteros que lleguen de la pagina New Case Custom

        caso.subject = System.currentPagereference().getParameters().get('subject');
        caso.status = System.currentPagereference().getParameters().get('status');
        //caso.caseorigin = System.currentPagereference().getParameters().get('caseorigin');
        caso.priority = System.currentPagereference().getParameters().get('priority');
        
        //FIN MODIFICACION
        
        //validarUserBackground();
        
    } 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Función que valida el Cliente (AccountId) que el usuario selecciona mientras está navegando en la pantalla de creación del caso
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Jose María Martín Castaño      	Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void validarUserBackground(){
            valFiscal=Apexpages.currentPage().getParameters().get('valFiscal');
            BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
            BIIN_Obtener_Contacto_WS cfs=new BIIN_Obtener_Contacto_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        BIIN_Obtener_Contacto_WS.ContactSalida salida = new BIIN_Obtener_Contacto_WS.ContactSalida();
        salida = cfs.getContact(authorizationToken, UserInfo.getFirstName(), UserInfo.getLastName(), UserInfo.getUserEmail(), null);
        system.debug('[USERINFO]: nombre: '+UserInfo.getFirstName()+' apellidos: '+UserInfo.getLastName()+' email: '+UserInfo.getUserEmail() );
        if(salida!=null){//meter otra comprobación en casod e que no sea usuario dummy
            for(Integer i=0;i<salida.outputs.output.size();i++){
                system.debug('nombre: '+salida.outputs.output[i].firstName+' apellidos: '+salida.outputs.output[i].lastName+' email: '+salida.outputs.output[i].internetEmail);
                co.directContactFirstName = salida.outputs.output[i].firstName;
                co.directContactLastName = salida.outputs.output[i].lastName;

            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que comprueba que, las tres categorías que el usuario ha seleccionado coinciden con algún registro de  
						la tabla BIIN_Tabla_Cat_Operacionales__c (Custom Setting) y en caso de no coincidir, active el popup de error
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void CambioCatOperacional(){ 
        boolean CategoriasDistintas = true;
        List<BIIN_Tabla_Cat_Operacionales__c> lestado = [SELECT Cat_Operacional1__c, Cat_Operacional2__c, Cat_Operacional3__c FROM BIIN_Tabla_Cat_Operacionales__c ];         
        system.debug('Lista de posibles categorias --->' + lestado);
        string Categoria1 = Apexpages.currentPage().getParameters().get('Categoria1');
        string Categoria2 = Apexpages.currentPage().getParameters().get('Categoria2');
        string Categoria3 = Apexpages.currentPage().getParameters().get('Categoria3');
        system.debug('Categorias Operacionales Seleccionadas --->' + Categoria1 + Categoria2 + Categoria3);
        Integer i=0;
        for(i=0;i<lestado.size();i++){
            if(lestado[i].Cat_Operacional1__c == Categoria1){               
                system.debug('Ha entrado en Cat1');
                if(lestado[i].Cat_Operacional2__c == Categoria2){
                    system.debug('Ha entrado en Cat2');
                    if(lestado[i].Cat_Operacional3__c == Categoria3){
                        UpdateCatDistintas = false;
                        CategoriasDistintas = false;
                        system.debug('Ha entrado en Cat3');
                    }
                }
            }
        }
        
        if(CategoriasDistintas == true){ 
            UpdateCatDistintas = true;
            MostrarPopUpCategOperac = true;			            
        }
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que crea el ticket, después de haber sido insertado en Salesforce, en Remedy. De no tener éxito, asigna una tarea pendiente
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void crearTicketRoD()
        { 
            BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
            BIIN_Creacion_Ticket_WS ct=new BIIN_Creacion_Ticket_WS();
            String authorizationToken = ''; // ot.obtencionToken();
            Integer nIntentos=0;
            Boolean exito=false;
            system.debug('CASO' + caso);
            do
            {
                exito=ct.crearTicket(authorizationToken, caso, la,co);
                nIntentos++;
            }
            while((nIntentos<3)&&(exito==false));
            
            if(Test.isRunningTest())
            {
                exito = false;
            }

            StringWSCreacion = ct.StringWS;        
            system.debug('BooleanWSCreacion----->' + StringWSCreacion);  
            if(!exito)
            {
                //lanzar la tarea porque no hemos podido contactar con el bao
                caso.Status='Confirmation pending';
                caso.BIIN_Descripcion_error_integracion__c = 'Es necesario relanzar manualmente el ticket,pulse el botón Relanzar';
                StringWSCreacion = 'PENDING';
                //MOSTRAR EL POPUP
                try
                {
                    update caso;
                }
                catch(DmlException e)
                {
                    system.debug('Error al insertar el estado del caso: '+e);
                }            

                Task tarea = new Task();
                tarea.WhatId = caso.Id;
                tarea.WhoId = caso.ContactId;
                tarea.Subject = caso.Subject;
                //tarea.OwnerId = [SELECT BI2_asesor__c FROM Account WHERE Id =: caso.AccountId].Id; //El campo Custom es el asesor asociado al Cliente (Userinfo.getUserId();)
                tarea.Description = caso.Description;
                //tarea.Status = caso.Status;
                system.debug('TASK -->'+ caso.Subject + caso.Description +caso.Status);
                insert tarea;          
            }
        } 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que se ejecuta después de guardar el ticket en SF y llama a la creación de Remedy
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public PageReference onClickGuardar()
        {
            if(caso.BI_COL_Fecha_Radicacion__c <= Datetime.now())
            {
                crearTicketRoD();   
            }
            else
            {
               Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
           }
           return null;
       }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para insertar ticket en SF previa verificación de fecha de solicitud correcta.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public PageReference insertarSolicitudTecnica()
        {
            if(caso.BIIN_Categorization_Tier_2__c == '__') caso.BIIN_Categorization_Tier_2__c  = null;
            if(caso.BIIN_Categorization_Tier_3__c == '__') caso.BIIN_Categorization_Tier_3__c  = null; 
            if(caso.BI_Product_Service__c == '__') caso.BI_Product_Service__c  = null; 


            if(caso.BI_COL_Fecha_Radicacion__c > Datetime.now())
            {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
            }


            //if(caso.BI_COL_Fecha_Radicacion__c > Datetime.now())
            //{
            //    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
            //}
            
            if(caso.AccountId != null && caso.BI_Country__c == null)
            {
                List<Account> lst_acc = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: caso.AccountId];
                //accountRelated = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: account_Id];
                
                if(!lst_acc.isEmpty())
                {
                    caso.BI_Country__c = lst_acc[0].BI_Country__c;
                }
            }   
            
            if(caso.BI_COL_Fecha_Radicacion__c == null)
            {
                caso.BI_COL_Fecha_Radicacion__c = Datetime.now();
            }

            if(UpdateCatDistintas == true || Test.isRunningTest())
            {
                system.debug('Ha entrado en Cat no válidas ---> CatNull');
                caso.BIIN_Categorization_Tier_1__c = null;
                caso.BIIN_Categorization_Tier_2__c = null;
                caso.BIIN_Categorization_Tier_3__c = null; 
            }
            
            try
            {
                caso.Status = 'Being processed';
                upsert caso;
                
                system.debug('Inserción Caso Salesforce: ' + caso);  
                system.debug('Adjunto1: ' + adjunto1);
                system.debug('Adjunto2: ' + adjunto2);
                system.debug('Adjunto3: ' + adjunto3);
                
                Boolean hasAttachments = GuardarTicketAdjuntoSF();
                if(hasAttachments)
                { 
                    // If case has attachments, show PopUps, Can´t use JS onComplete OR onClick in file´s transaction.
                    System.debug('ENTRA EN ADJUNTOS'); 
                    
                    // By default before calling Remedy
                    StringWSCreacion = 'ADJUNTOS';

                    // Call Remedy
                    onClickGuardarFuturo(JSON.serialize(caso), JSON.serialize(la));

                    MostrarPopUpEstado();
                }
                
                return null;
            }
            catch(Exception e)
            {
                Apexpages.addMessages(e);
                displayPopUp = false;
                return null;
            }
        }   

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        José Luis González Beltrán
        Company:       HPE
        Description:   Save attachments in future call
            
        History:
            
        <Date>           <Author>                                               <Description>
        17/06/2016       José Luis González Beltrán                             Adapt to UNICA, onClickGuardarFuturo()
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        @Future(callout=true)
        public static void onClickGuardarFuturo(String casoStr, String attachmentsStr)
        { 
            Case caso = (Case) JSON.deserialize(casoStr, Case.class);
            List<BIIN_Creacion_Ticket_WS.Adjunto> attachments = (List<BIIN_Creacion_Ticket_WS.Adjunto>) JSON.deserialize(attachmentsStr, List<BIIN_Creacion_Ticket_WS.Adjunto>.class);

            if(caso.BI_COL_Fecha_Radicacion__c > Datetime.now())
            {
                return;
            }

            BIIN_Creacion_Ticket_WS ct = new BIIN_Creacion_Ticket_WS();
            Integer nIntentos = 0;
            Boolean exito = false;

            do
            {
                exito = ct.crearTicket(null, caso, attachments, null);
                nIntentos++;
            }
            while((nIntentos<3) && (exito==false));
            
            if(Test.isRunningTest())
            {
                exito = false;
            }

            System.debug(' BIIN_CreacionIncidencia_Ctrl.onClickGuardarFuturo | exito: ' + exito);

            if(!exito)
            {
                // lanzar la tarea porque no hemos podido contactar con el bao
                BIIN_CreacionIncidencia_Ctrl.setTicketError(caso, 'Confirmation pending', 'Es necesario relanzar manualmente el ticket, pulse el botón Relanzar.');
            }

            System.debug(' BIIN_CreacionIncidencia_Ctrl.onClickGuardarFuturo | ct.StringWS: ' + ct.StringWS);

            if (ct.StringWS.contains('SVC1021'))
            {
                BIIN_CreacionIncidencia_Ctrl.setTicketError(caso, 'Error de validación', ct.StringWS);
            }
        }

        public static void setTicketError(Case caso, String status, String errorMessage)
        {
            caso.Status = status;
            caso.BIIN_Descripcion_error_integracion__c = errorMessage;

            try
            {
                update caso;
            }
            catch(DmlException e)
            {
                System.debug('Error al insertar el estado del caso: ' + e);
            }

            Task tarea = new Task();
            tarea.WhatId = caso.Id;
            tarea.WhoId = caso.ContactId;
            tarea.Subject = caso.Subject;
            tarea.Description = caso.Description;
            insert tarea;
        }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para guardar el  ticket con adjuntos en Remedy, previamente guardado en Salesforce.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        07/01/2016       Micah Burgos  García               Modify to v2.0
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public Boolean GuardarTicketAdjuntoSF()
        {
            system.debug('GuardarTicketAdjuntoSF --- ENTRA');
            List <Attachment> ListaAdjuntos = new List<Attachment>();
            la = new List<BIIN_Creacion_Ticket_WS.Adjunto>();

            if (adjunto1 != null) ListaAdjuntos.add(new Attachment(Name = nombre1, Body = adjunto1));
            if (adjunto2 != null) ListaAdjuntos.add(new Attachment(Name = nombre2, Body = adjunto2));
            if (adjunto3 != null) ListaAdjuntos.add(new Attachment(Name = nombre3, Body = adjunto3));

            try
            {
                if(!ListaAdjuntos.isEmpty())
                {

                    for (Attachment att: ListaAdjuntos)
                    {
                        att.ParentId = caso.Id;
                    }

                    insert ListaAdjuntos;

                    System.debug(' LIST1 > '+ ListaAdjuntos);

                    for (Attachment att: ListaAdjuntos)
                    {
                        System.debug('Adjunto nombre: ' + att);
                        BIIN_Creacion_Ticket_WS.Adjunto a = new BIIN_Creacion_Ticket_WS.Adjunto(att.Name,att.Body,att.Id);
                        System.debug('Adjunto nombre: ' + a);

                        la.add(a);                
                    }

                    ListaAdjuntos.clear(); //Reset adjunto variable to down view size.
                    adjunto1 = null; 
                    adjunto2 = null; 
                    adjunto3 = null; 

                    system.debug(' LIST2 > '+ la);

                    return true;
                }

                return false; //Case don´t has attachments
            }
            catch(DmlException ex)
            {
                system.debug('GuardarTicketAdjuntoSF.DmlException --- ex: ' + ex);
                ApexPages.addMessages(ex);
                MostrarPopUpEstado();
                return true;
            } 
        }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para almacenar la descripción del error que ha sucedido en la integración .
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void MostrarPopUpEstado()
        {   
            System.debug('MostrarPopUpEstado -->  StringWSCreacion= ' + StringWSCreacion);

            displayPopup = false;

            if(caso.BI_COL_Fecha_Radicacion__c <= Datetime.now())
            {
                if(StringWSCreacion == 'OK' || Test.isRunningTest())
                {
                    displayPopUp2 = true; 
                    caso.Status = 'Being processed';
                    caso.BIIN_Descripcion_error_integracion__c = 'Petición en curso. Mientras el ticket no se haya procesado podrá consultar su ticket como Solicitud Incidencia Técnica. En breves momentos recibirá una notificación y podrá consultar su ticket como una Incidencia Técnica.';
                    update caso;
                }
                else if(StringWSCreacion == 'PENDING' || Test.isRunningTest())
                {
                    displayPopUp4 = true; 
                }
                else if(StringWSCreacion == 'ADJUNTOS' || Test.isRunningTest())
                {
                    displayPopUpAdjuntos = true;
                    caso.Status = 'Being processed';
                    caso.BIIN_Descripcion_error_integracion__c = 'Petición en curso. Mientras el ticket no se haya procesado podrá consultar su ticket como Solicitud Incidencia Técnica.';
                    update caso;
                }
                else
                {
                    displayPopUp3 = true;
                    caso.Status = 'Error de validación';
                    caso.BIIN_Descripcion_error_integracion__c = StringWSCreacion;
                    update caso;
                }
            }
            else
            {
                 Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
            }
         }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que lleva al detalle del caso (Cuando el ticket ha sido correctamente guardado en ambos entornos) .
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        18/02/2016       Micah Burgos García                Redirect to all cases.
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/


        public pageReference OKdetalle()
        {
      //return new ApexPages.Standardcontroller(caso).view();
      return new PageReference('/500/o');
  }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para guardar, mostrar y limitar  adjuntos a tres.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        07/01/2016       Micah Burgos                       Deprecated
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*    public void Crear(){
        attach.ParentId = null;
        try{ 
            if( ContAdj <= 2){
                if(((attach.body!=null) && (attach.name !=null))){
                    system.debug('Attach>>>' + attach);
                    ListaAdjuntos.add(attach);
                    ListaNombres.add(attach.name);
                    attach = New Attachment();
                    ContAdj = ContAdj + 1;   
                    YaAdjuntado = true;
                    system.debug('Contador>>>'+ ContAdj);
                    system.debug('MasDeTres >>>'+ MasDeTres );
                }
            }
            else{
                MasDeTres = true;
                system.debug('MasDeTres >>>'+ MasDeTres );
            }            
        }  
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
        //return new ApexPages.Standardcontroller(caso).view();    
    }
    */    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función asociada al botón Cancelar para cancelar la creación del ticket y volver al menú de casos.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     Public PageReference Cancel(){
        return new PageReference ('/500/o');
    }    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Funciones que guardan los datos del contacto final, la Ubicación y CI mientras el usuario navega por la página.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void GuardarDatos(){
            caso.BI2_Nombre_Contacto_Final__c = Apexpages.currentPage().getParameters().get('NombreContactoFinal');
            caso.BI2_Apellido_Contacto_Final__c = Apexpages.currentPage().getParameters().get('ApellidoContactoFinal');
            caso.BI2_Email_Contacto_Final__c = Apexpages.currentPage().getParameters().get('EmailContactoFinal');
        }
        public void GuardarUbicacion(){
            caso.BIIN_Site__c = Apexpages.currentPage().getParameters().get('UbicacionController');
            system.debug('Ubicación -->' + caso.BIIN_Site__c);
        }
        public void GuardarCI(){
            caso.BIIN_Id_Producto__c = Apexpages.currentPage().getParameters().get('CIController');
            system.debug('CI -->' + caso.BIIN_Id_Producto__c);
        }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que vacía los campos de contacto final, CI e Ubicación al cambiar de Cliente.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void ReiniciarCampos(){
       caso.BIIN_Site__c = '';
       caso.BIIN_Id_Producto__c  = '';
       caso.BI2_Nombre_Contacto_Final__c = null;
       caso.BI2_Apellido_Contacto_Final__c = null;
       caso.BI2_Email_Contacto_Final__c = null;
   }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que vacía los campos  CI e Ubicación al cambiar la línea de negocio.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void ValidarCIUbicacion(){
            caso.BIIN_Site__c = '';
            caso.BIIN_Id_Producto__c  = '';
        }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que cierra la ventana de adjuntos y de error de categorías operacionales.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void closePopup3() 
     {        
        displayPopup = false;  
        if(StringWSCreacion.contains('SVC1021'))
        {
            displayPopUp3 = false; 
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que abre la ventana de adjuntos.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void showPopup3() { 
            displayPopup = true;          
        } 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que abre el popup de error de categorías operacionales.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/

        public void ClosePopUpCategOperac(){

            MostrarPopUpCategOperac = false; 
        //UpdateCatDistintas = true;
        //caso.BIIN_Categorization_Tier_1__c = '';
        //caso.BIIN_Categorization_Tier_2__c = '';
        caso.BIIN_Categorization_Tier_3__c = ''; 
        system.debug('Categorias ' + caso.BIIN_Categorization_Tier_1__c + caso.BIIN_Categorization_Tier_2__c + caso.BIIN_Categorization_Tier_3__c);
    }
    
}