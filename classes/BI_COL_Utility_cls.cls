/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-23      Daniel ALexander Lopez (DL)     Cloned Class      
*************************************************************************************/
public with sharing class BI_COL_Utility_cls {
	public BI_COL_Utility_cls() {
		
	}

	 public String encodingMD5(String cadena){
		
		Blob bPrehash = Blob.valueof(cadena);
		
		//String convertedPrehash = EncodingUtil.convertToHex(bPrehash);
		
		Blob bHexPrehash = Blob.valueOf(bPrehash.toString());
		Blob bsig = Crypto.generateDigest('MD5', bPrehash);
		String result = EncodingUtil.convertToHex(bsig);
		return result;
	}

  public List<BI_COL_Configuracion_campos_interfases__c> getConfInfz(String interfaz,String TipoTransaccion,String Direccion)
  {
	String consultaSOQL=' SELECT Id, BI_COL_Calificador_Texto__c,'+
		  ' Name, BI_COL_Segmento__c,'+
		  ' BI_COL_Separador__c, BI_COL_Separador_Decimal__c, BI_COL_Objeto__c'+  
		  ' FROM BI_COL_Configuracion_campos_interfases__c '+ 
		  ' WHERE BI_COL_Interfaz__c = \''+interfaz+'\' '+ 
		  ' AND BI_COL_Tipo_transaccion__c = \''+TipoTransaccion+'\'' +
		  ' AND BI_COL_Direccion__c = \''+Direccion+'\'';

		  System.debug('La consulta de Interfaz es: '+consultaSOQL);

	List<BI_COL_Configuracion_campos_interfases__c> consultaConfiguracionInterfaz=Database.query(consultaSOQL);
	return consultaConfiguracionInterfaz;
  }
  
  public List<BI_COL_Configuracion_campos_interfases__c> configCampos(String identificadorInterfaz) 
  {
	List<BI_COL_Configuracion_campos_interfases__c> consultaConfiguracionCampos = 
		 ([SELECT BI_COL_Convertir_mayuscula__c, BI_COL_Decimales__c, BI_COL_Formato_Campo__c,BI_COL_Longitud__c,
		  BI_COL_Configuracion_Interfaz__r.BI_COL_Segmento__c,BI_COL_Nombre_campo__c, BI_COL_Permite_nulos__c, BI_COL_Tipo_campo__c, BI_COL_Valor_predeterminado__c 
		  FROM BI_COL_Configuracion_campos_interfases__c WHERE BI_COL_Configuracion_Interfaz__c =:identificadorInterfaz AND IsDeleted = false 
		  ORDER BY BI_COL_Configuracion_Interfaz__c ASC, BI_COL_Orden__c ASC]);
	
	return consultaConfiguracionCampos;
  }

  public Static String quitarCaractees(String texto)
  {
	Pattern escaper = Pattern.compile('([^a-zA-z0-9])');
	String[] items = escaper.split(texto);
	String cadena='';
	for (String s : items) 
	{
		if(s!='')
		{
			System.debug(s);
			cadena+=s+' ';
		}
	}
	return cadena;
  }

}