/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-07-22      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_GenerateOT_failed_ctr 
{

	public BI_COL_Anexos__c fun;
	public List<WrapperMS> lstMS;
	public boolean todosMarcados {get;set;}
	public boolean habilitarEnvio {get;set;}
	public boolean showPage{get; set;}
	public String msEnvio {get;set;}
	public String segmento {get;set;}
	
	/** Paramétros del paginador */
	public List<WrapperMS> pageMS;
	public Integer pageNumber;
	public Integer pageSize;
	public Integer totalPageNumber;
	public String Busqueda{get; set;}
	
	private final ApexPages.StandardController ctr;
    public BI_COL_GenerateOT_failed_ctr(ApexPages.StandardController stdController) 
    {
        ctr = stdController;
    }    

	public void Buscar()
	{
		this.showPage = false;
		//Obtenemos el segmento del usuario
		User usu=[Select CompanyName from User where id=: userinfo.getuserid()];
		segmento=usu.CompanyName;
		System.debug('\n\n ---segmento---:'+usu.CompanyName+'\n\n');
		if(segmento != null)
		{
			this.showPage = true;
			
			//Obtenemos la lista de FUN asociados al id recibido como parámetro
			
			//Obtenemos las MS asociadas al FUN
			List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = getMSSolicitudServicio(segmento);
								
			if(lstMSConsulta == null || lstMSConsulta.size() == 0)
			{
				BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.WARNING,
							'No se encontraron Órdenes de trabajo en estado fallido o Solictado '+Busqueda);
			}
			else
			{
			//Llenar listado de MS Wrapper que se muestran en pantalla
			lstMS = new List<WrapperMS>(); 
									
			for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
			{
				WrapperMS wMS = new WrapperMS(ms,false);
				this.lstMS.add(wMS);
			}
			//Inicializar valores del paginador
			pageNumber = 0;
			totalPageNumber = 0;
			pageSize = 10;
			ViewData();
			}
		}
		else
		{
			BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'El usuario no tiene Segmento definido');
		}
	} 
	
	//Clase Wrapper que administra la selección de registros
	public class WrapperMS{
		public boolean seleccionado {get;set;}
		public BI_COL_Modificacion_de_Servicio__c ms {get;set;}
		
		public WrapperMS(BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado){
			this.ms = ms;
			this.seleccionado = seleccionado;
		}
	}
	
	/**
	* Obtiene el listado de MS asociadas a la Solicitud de Servicio
	*
	* @param Segmento Segmento del usuario
	* @return  Lista de MS
	*/
	
	public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String segmento)
	{
		String consultaSOQL='SELECT m.Id, m.Name, m.BI_COL_Oportunidad__c, m.BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c,m.BI_COL_Clasificacion_Servicio__c, '+
				'm.BI_COL_Producto__r.NE__ProdId__r.Name,BI_COL_Fecha_Instalacion_Comprometida__c, '+
				'm.BI_COL_Codigo_unico_servicio__r.Name, '+
				'm.BI_COL_Estado__c, m.BI_COL_Estado_orden_trabajo__c, m.BI_COL_Medio_Vendido__c, '+
				'm.CurrencyIsoCode, m.BI_COL_Total_servicio__c, m.BI_COL_TRM__c, m.BI_COL_Producto__r.NE__ProdId__r.BI_COL_TipoAnexo__c, '+
				'm.BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c, m.BI_COL_Monto_ejecutado__c '+
			'FROM BI_COL_Modificacion_de_Servicio__c m '+
			'WHERE BI_COL_Estado__c = \'Pendiente\' '+
			'AND BI_COL_Estado_orden_trabajo__c IN (\'Fallido\',\'Solicitado\')';
			if(segmento=='EMPRESAS')
			{
				consultaSOQL+= ' AND BI_COL_Oportunidad__r.Account.BI_Segment__c in (\'EMPRESAS\',\'NEGOCIOS\')';
			}
			else
			{
				consultaSOQL+= ' AND BI_COL_Oportunidad__r.Account.BI_Segment__c in (\'MAYORISTAS\')';
			}
			
			if(Busqueda!=null && Busqueda!='' )
			{
				String dsBuscar='';
				if(Busqueda.contains(','))
				{
					List<String> lstDSB=Busqueda.split(',');
					for(String ls:lstDSB)
					{
						dsBuscar+='\''+ls+'\',';
					}
					dsBuscar=dsBuscar.subString(0,dsBuscar.length()-1);
				}
				else
				{
					dsBuscar='\''+Busqueda+'\'';
				}
				consultaSOQL+=' AND Name IN ('+dsBuscar+')'; 
			}
			
			consultaSOQL+=' ORDER BY Name ASC';

			System.debug('\n\n consultaSOQL '+consultaSOQL+'\n\n');
		
		List<BI_COL_Modificacion_de_Servicio__c> lstMS =new List<BI_COL_Modificacion_de_Servicio__c>();
		lstMS=Database.query(consultaSOQL); 
		//system.debug('getMSSolicitudServicio: '+lstMS);
		return lstMS;
	}
	
	public List<WrapperMS> getLstMS()
	{
		if(pageMS!=null&&pageMS.size()>0)
			habilitarEnvio = false;
		else
			habilitarEnvio = true;
		
		return pageMS;
	}
	
	public PageReference ViewData()
	{
		totalPageNumber = 0;
		BindData(1);
		return null;
	}
	
	/**
	* Posiciona el registro segun el numero de pagina
	* @param newPageIndex Indice de la pagina
	* @return 
	*/
	public void BindData(Integer newPageIndex)
	{
		try
		{
			pageMS = new List<WrapperMS>();
			Transient Integer counter = 0;
			Transient Integer min = 0;
			Transient Integer max = 0;
			
			if (newPageIndex > pageNumber)
			{
				min = pageNumber * pageSize;
				max = newPageIndex * pageSize;
			}
			else
			{
				max = newPageIndex * pageSize;
				min = max - pageSize;
				//min = (min <>);
			}
			
			for(WrapperMS wMS : lstMS)
			{
				counter++;
				if (counter > min && counter <= max){
					pageMS.add(wMS);
				}
					
			}
			pageNumber = newPageIndex;
			
			if (pageMS == null || pageMS.size() <= 0)
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.BI_COL_lblDatosNoDisponiblesVista));
		}
		catch(Exception ex)
		{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
		}	
	}
	
	/**
	* Acción para seleccionar todos los registros de MS
	* @return PageReference
	*/
	public PageReference action_seleccionarTodos()
	{
		if(this.lstMS!=null)
		{
			for(WrapperMS wMS : this.lstMS)
			{
				wMS.seleccionado = this.todosMarcados;
			}
		}
		return null;
	}
	
	//IMPLEMENTACION PAGINADOR
	public Integer getPageNumber()
	{
		return pageNumber;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}
	
	public Boolean getPreviousButtonEnabled()
	{
		return !(pageNumber > 1);
	}
	
	public Boolean getNextButtonDisabled()
	{
		if (lstMS == null)
		{
			return true;
		}
		else
		{
			return ((pageNumber * pageSize) >= lstMS.size());
		}
	
	}
	public Integer getTotalPageNumber()
	{
		if (totalPageNumber == 0 && lstMS !=null)
		{
			totalPageNumber = lstMS.size() / pageSize;
			Integer mod = lstMS.size() - (totalPageNumber * pageSize);
			if (mod > 0)
			totalPageNumber++;
		}
		return totalPageNumber;
	}
	
	public PageReference nextBtnClick() {
		if(pageNumber != null)
			BindData(pageNumber + 1);
		return null;
	
	}
	
	public PageReference previousBtnClick() {
		if(pageNumber != null)
			BindData(pageNumber - 1);
		return null;
	
	}
	
	/**
	* Acción para enviar ordenes de trabajo
	* @return PageReference
	*/
	public PageReference action_envioOT()
	{
		
		List<String> lstIdMS = new List<String>();
		List<BI_COL_Modificacion_de_Servicio__c> lstMSUpdate = new List<BI_COL_Modificacion_de_Servicio__c>(); 
		List<BI_COL_Modificacion_de_Servicio__c> lstMSUpdateNoViajanTRS = new List<BI_COL_Modificacion_de_Servicio__c>(); 
		String Mensaje = '';
		Boolean band = false;
		//Iterar registros seleccionado 
		msEnvio='';
		
		String strMensajeContrato = '';
		
		//Calculamos el total de las MS seleccionadas
		Decimal decTotalServiciosMS = 0;
		
		Boolean control = true;
		
		if(this.lstMS != null)
		{ 
			for(WrapperMS wMS : this.lstMS)
			{
				if(wMS.seleccionado)
				{
					System.debug('\n\n wMS.ms.BI_COL_Medio_Vendido__c'+wMS.ms.BI_COL_Medio_Vendido__c+' \n\n');
					
					if(wMS.ms.BI_COL_Medio_Vendido__c != null)
					{
						
						if(wMS.ms.BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c && wMS.ms.BI_COL_Clasificacion_Servicio__c!='RENEGOCIACION')
						{
							lstIdMS.add(wMS.ms.Id);
							wMS.ms.BI_COL_Estado_orden_trabajo__c = 'Solicitado';
							lstMSUpdate.add(wMS.ms);
						}
						else
						{
							//Cambiamos los campos necesarios
							wMS.ms.BI_COL_Estado_orden_trabajo__c = 'En desarrollo'; 
							wMS.ms.BI_COL_Fecha_liberacion_OT__c = System.now();
							wMS.ms.BI_COL_Estado__c = 'Enviado';
							
							lstMSUpdateNoViajanTRS.add(wMS.ms);
						}
						
						msEnvio += wMS.ms.Name + ', ';
					}
					else
					{
						Mensaje += 'La ' + wMS.ms.BI_COL_Codigo_unico_servicio__r.Name + ' - '
							+ wMS.ms.Name + Label.BI_COL_lblMSSinMedioSucursal;
						band = true;
					}
					Double msTrm=wMS.ms.BI_COL_TRM__c!=null?Double.valueOf(wMS.ms.BI_COL_TRM__c):1;
					if(wMS.ms.BI_COL_Clasificacion_Servicio__c!='BAJA')
					{
						decTotalServiciosMs += wMS.ms.BI_COL_Monto_ejecutado__c / wMS.ms.BI_COL_TRM__c;
					}
					
				}
			}
			
			if(band)
			{
				System.debug(Mensaje);
				BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.WARNING, Mensaje);
				return null;
			}
								
			//Cantidad de bloqueos
			/*Integer iBloqueo = 0;
			Map<Integer,List<String>> mapBloques = new Map<Integer,List<String>>();
			Integer intercalaBlq = 0;
			Integer sizeLst = 0;
			List<String> regMS = new List<String>();
			System.debug('\n\n lstIdMS='+lstIdMS+'\n\n');					
			for(String idMS : lstIdMS)
			{
				regMS.add(idMS);
				intercalaBlq++;
				sizeLst++;
				
				if(intercalaBlq == 10 || sizeLst == lstIdMS.size())
				{
					mapBloques.put(iBloqueo,regMS);
					iBloqueo++;
					intercalaBlq=0;
					regMS = new List<String>();
				}
			}*/
			System.debug('\n\n lstIdMS='+lstIdMS+'\n\n');
			envioDatosSISGOT(lstIdMS);
			
								
			try
			{
				if(lstMSUpdate!=null&&lstMSUpdate.size()>0)
				{
					update lstMSUpdate;
				}
				if(lstMSUpdateNoViajanTRS.size() > 0)
				{
					update lstMSUpdateNoViajanTRS;
				}
								
			}
			catch(DMLException e)
			{
				BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, Label.BI_COL_lblErrorActualizacionBD + ' ' + e.getMessage());
				return null;	
			}
								
			List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = getMSSolicitudServicio(this.segmento);
						
			if(lstMSConsulta == null || lstMSConsulta.size()==0)
			{
				msEnvio = Label.BI_COL_lblMSEnviadas + ' ' + msEnvio;
				lstMS = new List<WrapperMS>();
				pageMS = new List<WrapperMS>();
				BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.INFO,msEnvio );
									
				//Actualizamos los valores del presupuesto del contrato
				//this.actualizarPresupuesto(decTotalServiciosMs);
									
				return null;
			}
			else
			{
				//Llenar listado de MS Wrapper que se muestran en pantalla
				lstMS = new List<WrapperMS>(); 
				for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
				{
					WrapperMS wMS = new WrapperMS(ms,false);
					this.lstMS.add(wMS);
				}
				//Inicializar valores del paginador
				pageNumber = 0;
				totalPageNumber = 0;
				pageSize = 10;
				ViewData();
			}		
								
			BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.CONFIRM, Label.BI_COL_lblActualizacionBD);
		}
		return null;
	}
	
	public void actualizarPresupuesto(Decimal decTotalServiciosMs)
	{	
		Decimal montoEjecutado = this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c == null ? 0 : this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c; 
		this.fun.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c = montoEjecutado + decTotalServiciosMs;
		//this.fun.BI_COL_Contrato__r.Saldo_contrato__c -= decTotalServiciosMs;
		
		update this.fun;
	}
	
	@Future (callout=true)
	public static void envioDatosSISGOT(List<String> lstIdMS)
	{
		List<BI_COL_Modificacion_de_Servicio__c> lstMS = [select Id, Name, BI_COL_Codigo_unico_servicio__r.Name,BI_COL_Estado_orden_trabajo__c
												from BI_COL_Modificacion_de_Servicio__c 
												where id in :lstIdMS];
												
		try
		{
			//Ejecutar Servicio Web de OT
			BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS(lstMS);
		}catch(Exception e){
			System.debug(':: Se presento un error generando las ordenes de trabajo: '+e.getMessage());
		}
	}
	
	/*public PageReference action_Regresar()
	{
		PageReference pageRef=new PageReference('/'+idFUN);
		pageRef.setRedirect(true);
		return pageRef;
	}*/

}