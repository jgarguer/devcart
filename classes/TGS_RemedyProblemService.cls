/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         everis
Company:        Everis España
Description:    CR11339 - PROBLEM interface Rod to SF

History:  CR11339 - PROBLEM interface Rod to SF
 
23/01/2017                    everis                        Versión inicial
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class TGS_RemedyProblemService {
    //Declaramos el objeto ProblemInfo con el cual mapearemos el xml recibido del WS 
    global class ProblemInfo {
        webService String ProblemID;
        webService String CoordinatorGroup;
        webService String LocationCompany;
        webService String TicketSite;
        webService String Assignee;
        webService String ProblemCoordinator;
        webService String Summary;
        webService String Notes;
        webService String Impact;
        webService String Urgency;
        webService String Priority;
        webService String Status;
        webService String InvestigationDriver;
        webService String INF6_Pbm_Visible;
        webService String Service;
        webService String CI;
        webService String TargetDate;
        webService String AssignedGroup;
        webService String Vendor;
        webService String VendorTicketNumber;
        webService String StatusReason;
        webService String Workaround;
        webService String Resolution;
        webService String OperationalCategorizationTier1;
        webService String OperationalCategorizationTier2;
        webService String OperationalCategorizationTier3;
        webService String ProductCategorizationTier1;
        webService String ProductCategorizationTier2;
        webService String ProductCategorizationTier3;
        webService String ProblemLocation;
        webService String ProductName;
        webService String ModelVersion;
        webService String Manufactured;
        webService String MasterID;
        webService String CreateToVendor;
        webService String INF2_Migracion;
        webService String DelegatedCompany;
        webService String WorkaroundDeterminatedON;
        webService String WorkDetailNotes;
        webService String KnownErrorCreated;
        webService String LastCompletedDate;
        webService String Submitter;
        webService String SubmitDate;
        webService String LastMdifiedBy;
        webService String LastMdifiedDate;
        
    }
    
    webService static List<String> createProblem(ProblemInfo info){
        //El ProblemID sera el id externo del ticketProblem que viene de Remedy
        String problemaId = info.ProblemID;
        //location guardara el name de la cuenta holding donde se creara el caso
        String location = info.LocationCompany;
        //String site = info.TicketSite;
        String visible = NULL;
        //visible guardará el campo que viene de Remedy que indica si el caso debe ser visible o no al usuario
        if(info.INF6_Pbm_Visible != NULL && !info.INF6_Pbm_Visible.equals('')){
            visible = info.INF6_Pbm_Visible;
        }
        //statusSF guardara el estado del caso
        String statusSF = info.Status;
        //respuesta del WS
        List<String> respuesta = new List<String>();
        String aux = '';
        //codigo de respuesta
        String codRes;
        //descripcion de respuesta
        String descRes;
        //case number de respuesta
        String caseNum = NULL;
        //si el id externo no viene informado en el XML devolvemos un error
        if(info.ProblemID == null || info.ProblemID.equals('')){
            codRes = '400';
            descRes = 'El ID del ticket no viene informado en el XML';
            aux = '<result>'+codRes+'</result><message>'+descRes+'</message><RecordID>'+caseNum+'</RecordID>';
            respuesta.add(aux); 
            return respuesta;
        }
        //creamos un caso para almacenar en SF el problem
        Case caso = new Case();
        //Buscamos la cuenta holding con el name que nos envian desde Remedy, si no esta devolvemos un error y si esta lo asociamos al AccountId
        Id holdingId = [SELECT Id FROM RecordType WHERE Name = '1.Holding'].Id;
        Account[] cuentaHolding = [Select Id,name From Account WHERE Name=:location AND RecordTypeId =:holdingId limit 1];
        if(cuentaHolding !=NULL && cuentaHolding.size() > 0){
            caso.AccountId = cuentaHolding[0].Id;
        }else{
            codRes = '400';
            descRes = 'El Nombre de cuenta (LocationCompany) no se corresponde con ningún nombre de cuenta tipo holding';
            aux = '<result>'+codRes+'</result><message>'+descRes+'</message><RecordID>'+caseNum+'</RecordID>';
            respuesta.add(aux);
            return respuesta;
        }
        //Si el valor que nos envian del campo visible es SI o NO lo asignamos al caso, si no, por defecto, establecemos NO
        if(visible != NULL){
            if(visible.equalsIgnoreCase('No') || visible.equalsIgnoreCase('Yes')){
                caso.TGS_ProblemVisibility__c = visible;
            }else{
                caso.TGS_ProblemVisibility__c = 'No';
            }
        }else{
            caso.TGS_ProblemVisibility__c = 'No';
        }
        //Mapeamos el resto de campos del XML
        if(info.CoordinatorGroup != NULL && !info.CoordinatorGroup.equals('')){
            caso.TGS_CaseOwner__c = info.CoordinatorGroup;
        }
        if(info.TicketSite != NULL && !info.TicketSite.equals('')){
            caso.TGS_Ticket_Text__c = info.TicketSite;
        }  
        if(info.Assignee != NULL && !info.Assignee.equals('')){
            caso.TGS_Technical_Assignee__c = info.Assignee;
        }    
        if(info.ProblemCoordinator != NULL && !info.ProblemCoordinator.equals('')){
            caso.TGS_CaseContact__c = info.ProblemCoordinator;
        }    
        if(info.Summary != NULL && !info.Summary.equals('')){
            caso.Subject = info.Summary;
        }
        if(info.Notes != NULL && !info.Notes.equals('')){
            caso.TGS_Problem_Description__c = info.Notes;
            caso.Description = info.Notes;
        }
        if(info.Impact != NULL && !info.Impact.equals('')){
            caso.TGS_Impact_Text__c = info.Impact;
            caso.TGS_Impact__c = info.Impact;
        }
        if(info.Urgency != NULL && !info.Urgency.equals('')){
            caso.TGS_Urgency_Text__c = info.Urgency;
            caso.TGS_Urgency__c = info.Urgency;
        }
        if(info.Priority != NULL && !info.Priority.equals('')){
            caso.TGS_Priority_Text__c = info.Priority;
        }
        if(info.Status != NULL && !info.Status.equals('')){
            caso.TGS_Status_Text__c = info.Status; 
            caso.Status = info.Status;
        }
        if(info.InvestigationDriver != NULL && !info.InvestigationDriver.equals('')){
            caso.TGS_Investigation_Driver__c = info.InvestigationDriver;
        }
        if(info.Service != NULL && !info.Service.equals('')){
            caso.TGS_Service__c = info.Service;
        }
        if(info.CI != NULL && !info.CI.equals('')){
            caso.TGS_CI_Text__c = info.CI;
            if(info.CI != null && info.CI != ''){
                List<NE__OrderItem__c> oi = [SELECT Id FROM NE__OrderItem__c WHERE Name =: info.CI LIMIT 1];
                if(oi != null && !oi.isEmpty()){
                    caso.TGS_Customer_Services__c = oi.get(0).Id;
                }
            }
        }
        if(info.TargetDate != NULL && !info.TargetDate.equals('')){
            caso.TGS_Target_Date__c = info.TargetDate;
        }
        if(info.AssignedGroup != NULL && !info.AssignedGroup.equals('')){
            caso.TGS_Assigned_Group_Text__c = info.AssignedGroup;
        }
        if(info.Vendor != NULL && !info.Vendor.equals('')){
            caso.TGS_Vendor__c = info.Vendor;
        }
        if(info.VendorTicketNumber != NULL && !info.VendorTicketNumber.equals('')){
            caso.TGS_Vendor_Ticket__c = info.VendorTicketNumber;
        }
        if(info.StatusReason != NULL && !info.StatusReason.equals('')){
            caso.TGS_Status_Reason_Text__c = info.StatusReason;
        }
        if(info.Workaround != NULL && !info.Workaround.equals('')){
            caso.TGS_Workaround__c = info.Workaround;
        }
        if(info.Resolution != NULL && !info.Resolution.equals('')){
            caso.TGS_Resolution__c = info.Resolution;
        }
        if(info.OperationalCategorizationTier1 != NULL && !info.OperationalCategorizationTier1.equals('')){
            caso.TGS_Operat_Categorization_Tier_1__c = info.OperationalCategorizationTier1;
        }
        if(info.OperationalCategorizationTier2 != NULL && !info.OperationalCategorizationTier2.equals('')){
            caso.TGS_Operat_Categorization_Tier_2__c = info.OperationalCategorizationTier2;
        }
        if(info.OperationalCategorizationTier3 != NULL && !info.OperationalCategorizationTier3.equals('')){
            caso.TGS_Operat_Categorization_Tier_3__c = info.OperationalCategorizationTier3;
        }
        if(info.ProductCategorizationTier1 != NULL && !info.ProductCategorizationTier1.equals('')){
            caso.TGS_Product_Categorization_Tier_1_Text__c = info.ProductCategorizationTier1;
            caso.TGS_Product_Tier_1__c = info.ProductCategorizationTier1;
        }
        if(info.ProductCategorizationTier2 != NULL && !info.ProductCategorizationTier2.equals('')){
            caso.TGS_Product_Categorization_Tier_2_Text__c = info.ProductCategorizationTier2;
            caso.TGS_Product_Tier_2__c = info.ProductCategorizationTier2;
        }
        if(info.ProductCategorizationTier3 != NULL && !info.ProductCategorizationTier3.equals('')){
            caso.TGS_Product_Categorization_Tier_3_Text__c = info.ProductCategorizationTier3;
            caso.TGS_Product_Tier_3__c = info.ProductCategorizationTier3;
        }
        if(info.ProblemLocation != NULL && !info.ProblemLocation.equals('')){
            caso.TGS_Problem_Location__c = info.ProblemLocation;
        }
        if(info.ProductName != NULL && !info.ProductName.equals('')){
            caso.TGS_Product_Name_Text__c = info.ProductName;
        }
        if(info.ModelVersion != NULL && !info.ModelVersion.equals('')){
            caso.TGS_Model_Version__c = info.ModelVersion;
        }
        if(info.Manufactured != NULL && !info.Manufactured.equals('')){
            caso.TGS_Manufactured__c = info.Manufactured;
        }
        if(info.MasterID != NULL && !info.MasterID.equals('')){
            caso.TGS_Master_ID__c = info.MasterID;
        }
        if(info.CreateToVendor != NULL && !info.CreateToVendor.equals('')){
            caso.TGS_Create_To_Vendor__c = info.CreateToVendor;
        }
        if(info.INF2_Migracion != NULL && !info.INF2_Migracion.equals('')){
            caso.TGS_INF2_Migracion__c = info.INF2_Migracion;
        }
        if(info.DelegatedCompany != NULL && !info.DelegatedCompany.equals('')){
            caso.TGS_Delegated_Company__c = info.DelegatedCompany;
        }
        if(info.WorkaroundDeterminatedON != NULL && !info.WorkaroundDeterminatedON.equals('')){
            caso.TGS_Workaround_Determinated_ON__c = info.WorkaroundDeterminatedON;
        }
        if(info.KnownErrorCreated != NULL && !info.KnownErrorCreated.equals('')){
            caso.TGS_Known_Error_Created__c = info.KnownErrorCreated;
        }
        if(info.LastCompletedDate != NULL && !info.LastCompletedDate.equals('')){
            caso.TGS_Last_Completed_Date_Text__c = info.LastCompletedDate;
        }
        if(info.Submitter != NULL && !info.Submitter.equals('')){
            caso.TGS_Submitter__c = info.Submitter;
        }
        if(info.SubmitDate != NULL && !info.SubmitDate.equals('')){
            caso.TGS_Submit_Date__c = info.SubmitDate;
        }
        if(info.LastMdifiedBy != NULL && !info.LastMdifiedBy.equals('')){
            caso.TGS_Last_Modified_By__c = info.LastMdifiedBy;
        }
        if(info.LastMdifiedDate != NULL && !info.LastMdifiedDate.equals('')){
            caso.TGS_Last_Modified_Date_Text__c = info.LastMdifiedDate;
        }
        List<TGS_Work_Info__c> listWIToInsert = new List<TGS_Work_Info__c>();
        if(info.WorkDetailNotes != NULL && !info.WorkDetailNotes.equals('')){
            caso.TGS_Work_Detail_Notes__c = info.WorkDetailNotes;
            List<String> listDescriptionWI = info.WorkDetailNotes.split('//');
            if(listDescriptionWI != null && !listDescriptionWI.isEmpty()){
                for(String des : listDescriptionWI){
                    TGS_Work_Info__c wi = new TGS_Work_Info__c();
                    wi.TGS_Description__c = des;
                    wi.TGS_Public__c = true;
                    listWIToInsert.add(wi);
                }
            }
            
        }

        //Buscamos en SF si el caso ya existe a traves del id externo ProblemID
        Case[] casoSF = [SELECT Id,TGS_Ticket_Id__c,CaseNumber FROM CASE WHERE TGS_Ticket_Id__c=:problemaId];
        //si existe el caso, le asignamos su ID al nuevo caso 
        if(casoSF!=NULL && casoSF.size() > 0){         
            caso.Id = casoSF[0].id;
            //Si el nuevo estado que recibimos del XML coincide con los valores de la picklist STATUS de SF lo asignamos
            if(getStatus(statusSF)){
                caso.Status = statusSF;
            }
            try{
                update caso;
                codRes = '201';
                descRes = 'Registro actualizado correctamente';
                caseNum = casoSF[0].CaseNumber;
                aux = '<result>'+codRes+'</result><message>'+descRes+'</message><RecordID>'+caseNum+'</RecordID>';
                respuesta.add(aux);
                /*---------------------------------------------------------------------
                
                09/05/2017: Javier Martinez Sanz: Se comenta esta parte del código porque se ha decidido enviar los WI por el interfaz estándar
                
                // Primero borro los Work Info que hay en SF asociados a este caso, después inserto los que vienen en el WS para evitar duplicados.
                List<TGS_Work_Info__c> wiToDelete = [SELECT Id FROM TGS_Work_Info__c WHERE TGS_Case__c =: caso.Id];
                if(wiToDelete != null && !wiToDelete.isEmpty()){
                    delete wiToDelete;
                }
                if(listWIToInsert != null && !listWIToInsert.isEmpty()){
                    for(TGS_Work_Info__c wi : listWIToInsert){
                        wi.TGS_Case__c = caso.Id;
                    }
                    insert listWIToInsert;
                }
                ----------------------------------------------------------------------*/
            }catch(DmlException d){
                codRes = '400';
                descRes = d.getMessage();
                aux = '<result>'+codRes+'</result><message>'+descRes+'</message><RecordID>'+caseNum+'</RecordID>';
                respuesta.add(aux); 
            }
            
            
            return respuesta;
        //Si no existe el caso en SF lo insertamos con el RecordType "Problem" 
        }else{        
            try{
                caso.RecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE Name ='Problem' and SobjectType ='Case'  limit 1].Id;
                //Al crear un caso nuevo, el unico estado posible del campo STATUS es "Assigned"
                caso.Status = 'Assigned';
                //Le asignamos el ID externo ProblemID
                caso.TGS_Ticket_Id__c = problemaId;
                insert caso;
                
                // Inserto los Work Info que vienen en el WS 
                if(listWIToInsert != null && !listWIToInsert.isEmpty()){
                    for(TGS_Work_Info__c wi : listWIToInsert){
                        wi.TGS_Case__c = caso.Id;
                    }
                    insert listWIToInsert;
                }
                codRes = '201';
                descRes = 'Registro insertado correctamente';
                Case auxCase = [Select CaseNumber From Case Where Id=:caso.Id];
                caseNum = auxCase.CaseNumber;
                aux = '<result>'+codRes+'</result><message>'+descRes+'</message><RecordID>'+caseNum+'</RecordID>';
                respuesta.add(aux);
            }catch(DmlException d){
                codRes = '400';
                descRes = d.getMessage();
                aux = '<result>'+codRes+'</result><message>'+descRes+'</message><RecordID>'+caseNum+'</RecordID>';
                respuesta.add(aux); 
            }
            return respuesta;
        }
    }
    //Metodo que busca si el valor STATUS que recibimos del XML existe en la picklist Status en SF
    static boolean getStatus(String valorStatus){
        String ret = NULL;
        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple){
            if(f!=NULL && valorStatus.equals(f.getValue())){
                ret = f.getValue();
            }
        }       
        if(ret == NULL){
            return false;
        }else{
            return true;
        }
    }
}