@RestResource(urlMapping='/customeraccounts/v1/accounts/*/contacts')
global class BI_ContactsForAccountRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Account's related contacts Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Retrieves the list of contacts associated to the specific account
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.ContactsListInfoType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.ContactsListInfoType getContacts() {
		
		BI_RestWrapper.ContactsListInfoType conListInfoType;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', 'MISSING REQUIRED HEADERS: countryISO');
			
			}else{
			
				//MULTIPLE CONTACTS
				conListInfoType = BI_RestHelper.getContactsForAccount(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (conListInfoType == null)?404:200;//404 NOT FOUND; 200 OK
					
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContactsForAccountRest.getContacts', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return conListInfoType;
		
	}
	
}