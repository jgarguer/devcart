public with sharing class PublishAllItemsController {

    public String catId {get;set;}
    public NE__Catalog__c catalog;
    public List<Attachment> att;
    public List<NE__Catalog_Item__c> catalogItems;
    public NE__Map__c mapp;
    List<Attachment> lstAttach;
    List<Attachment> newlstAttach;
    List<Attachment> delAttach;
    public String done {get;set;}
    public Boolean stopPoller {get;set;}
    public Integer percentComplete {get; set;}
    public Integer index {get;set;}
    Integer lastRow = 0;
    Integer firstRow = 0;
    public Integer totalItems {get;set;}
    public Integer current {get;set;}

    public PublishAllItemsController(){
        
        catId = Apexpages.Currentpage().getParameters().get('catId');
        /*att = [SELECT id, Name, ParentId FROM Attachment WHERE ParentId IN
                         (SELECT Id FROM NE__Catalog_item__c WHERE NE__catalog_id__c = :catId AND NE__type__c = 'root') 
                          AND Name = 'XML_Complex_Product.xml'];*/
        
        catalogItems =[select id, Name, NE__ProductId__r.Name, (select id, Name, ParentId from Attachments where Name = 'XML_Complex_Product.xml') from NE__Catalog_item__c where NE__type__c = 'root' and NE__catalog_Id__c= :catId]; 
        system.debug('*catalogItems ' + catalogItems);
        system.debug('*catalogItems size ' + catalogItems.size());
        
        mapp=[select Id,  NE__Map_Name__c from NE__Map__c where NE__Map_Name__c = 'ComplexProduct'];
        
        stopPoller = false;
        percentComplete = 0;
        done = 'Starting... ' + '0/0';
    }

    public PageReference assignAttachments(){  
        
        try{    
            if(mapp != null){
                               
                String mapName = mapp.NE__Map_Name__c;                  
                String dateFormat = 'yyyy-MM-dd';
                
                Integer currentItem = 0;
                totalItems = 0;
                if(catalogItems.size() > 0){
                     totalItems = catalogItems.size();
                }   
                lstAttach = new List<Attachment>(); 
                newLstAttach = new List<Attachment>();  
                delAttach = new List<Attachment>();
                String xmlString;
                
                index = 1;
                current = firstRow + 1;
                system.debug('*Catalog Item size ' + catalogItems.size());
                if((firstRow + index) < catalogItems.size()){
                    lastRow = firstRow + index;
                    stopPoller = false; 
                    done = ' Processing... ' + String.valueOf(current) + '/' + String.valueOf(totalItems);             
                }
                else {
                    
                    lastRow = catalogItems.size();
                    stopPoller = true;
                    done = 'Process completed!';
                    
                }
                system.debug('*Process in progress');
                Integer i;
                for( i = firstRow; i < lastRow; i++){
                    system.debug('*catalogItems[i].attachments.size() before ' + catalogItems[i].attachments.size());
                    if(catalogItems[i].attachments.size() > 0){ //if I find at least one attachment associated to this root item
                        for(Attachment a: catalogItems[i].attachments){                 
                            delAttach.add(a);   
                            system.debug('*att for deleting ' + delAttach);
                            //delete a;
                            //system.debug('*Deleted! ' + a);                                                                                                       
                        }
                                     
                    }
                     /*** NEW INSERT ***/
                    xmlString = callXmlMapper(mapName, catalogItems[i].Id, dateFormat );    
                    system.debug('*xmlStringInsert ' + xmlString);          
                    Attachment attach = new Attachment();
                    attach.Name = 'XML_Complex_Product.xml';
                    attach.Body = Blob.valueof(xmlString);
                    attach.ParentId = catalogItems[i].Id;
                    attach.Description = 'Xml of the most recent version of this complex product'; 
                    newlstAttach.add(attach);    
                    //insert attach;                
                    //system.debug('*Inserted! ' + attach);
                }
                
                system.debug(lastRow+'      '+totalItems+'     '+catalogItems.size());
                // Update progress bar
                percentComplete = ((Double.valueOf(lastRow) / Double.valueOf(totalItems)) * 100).intValue();
                
                if(stopPoller == false) {
                    // Set firstRow for the next call
                    firstRow = i;
                    system.debug('*firstrow in end every poller ' + firstRow);
                }
                                                                        
                insAttach();
                              
            }
        }catch(Exception e){
            stopPoller = true;
            done = 'ERROR: Process Not completed!';
            system.debug('Error: ' + e.getMessage() + ' ' + e.getLineNumber());
            
        }
        return null;
    }
    
    public void insAttach(){
        
        try{
            if(delAttach.size() > 0){
                delete delAttach;               
                system.debug('*Delete DONE! ' + delAttach);
            }
            if(newLstAttach.size() > 0){
                insert newLstAttach;
                system.debug('*Insert DONE! ' + newLstAttach);
            }
            
        }catch(Exception e){
            
            done = 'ERROR: Process Not completed!';
        }
        
        
    }
  
  
    public String callXmlMapper(String mapName, String parentId, String dateFormat){
        NE.XmlMap  x          =  new NE.XmlMap();     
        NE.XmlMap.XmlMapRequest req   =   new NE.XmlMap.XmlMapRequest();
        
        req.mapName   = mapName;
        req.ParentId  = parentId;
        req.dateFormat  = dateFormat;
    
        NE.XmlMap.XmlMapResponse result  =  x.generateXml(req);
        NE.XMLDom.Element xml        =  result.xml;
    
        if(result.ErrorCode == '0'){
         
          return xml.toXmlString();
        }
        else{
          
          return result.ErrorMessage;
        }
    }

}