@isTest
private class BI_ProductosNumero_JOB_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Salesforce.com
	Description:   Method that cover BI_ProductosNumero_JOB class
	History: 

	 <Date>                     <Author>                <Change Description>
	15/01/2015                  Micah Burgos            Initial Version
    25/05/2015                  Raul Agüera             Add Query FROM NE__OrderItem__c
    08/06/2015					Fernando Arteaga		Added method BI_ProductosNumero_JOB_Exceute_Exception_TEST
    27/09/2015                  Francisco Ayllon        Deprecated
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    /*static testMethod void BI_ProductosNumero_JOB_Exceute_TEST() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        ////////////////////////////////////
                    Integer K = 2;
        ///////////////////////////////////
        
        List <String> lst_region = BI_DataLoad.loadPaisFromPickList(1);
        
        //BI_Pickup_Option__c pickupOption = BI_DataLoadRest.loadPickUpOptions(lst_region);          							   			 
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null);
        
        List <NE__Order__c> lst_orders = BI_DataLoadRest.loadOrders(1, lst_acc[0], lst_opp[0]);
        
        List <NE__OrderItem__c> lst_oi = BI_DataLoadRest.loadOrderItem(lst_acc[0], lst_orders);

        set<Id> setOpportId = new set<Id>();
        for(Opportunity opp: lst_opp){
        	opp.BI_Producto_actualizado__c = false;
            opp.BI_Productos_numero__c = null;
            setOpportId.add(opp.Id);
        }
    	
    	update lst_opp;

    	Test.startTest();*/

       /* Add 25/05/2015 Raul Aguera */
        /*List <NE__OrderItem__c> lst_oi_rltd = 
        [SELECT Id, NE__OrderId__r.NE__OptyId__r.BI_Producto_actualizado__c, NE__OrderId__r.NE__OptyId__r.BI_Productos_numero__c 
        FROM NE__OrderItem__c  WHERE NE__OrderId__r.NE__OptyId__c IN :setOpportId];
        
        system.assert(!lst_oi_rltd.isEmpty());*/
		/* End 25/05/2015 Raul Aguera */

        /*BI_Jobs.updateProductosNumero(false);
		
        Test.stopTest();

        lst_opp = [SELECT Id, Name, BI_Producto_actualizado__c FROM Opportunity WHERE BI_Producto_actualizado__c = null ];

		system.assert(lst_opp.isEmpty());
    }
    
	static testMethod void BI_ProductosNumero_JOB_Exceute_Exception_TEST()
	{
    	try
    	{
    		BI_Jobs.updateProductosNumero(null);
    	}
    	catch (Exception e)
    	{
    		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
    	}
	}*/
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}