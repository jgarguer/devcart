global with sharing class BI2_Buttons {

	public static final String B2P_BI2_CASE_DOC = '/apex/NE__DocumentComponentExport?mapName=BI_EN+F2+MapCase&nameDCH={NombreDePlantilla}&dateFormat=dd-MM-yyyy&parentId={IdDeCaso}';
	static webService String getBit2PubDoc4Case(String caseId) { // Use String bc some some Ids are not recognized as such (why though?)
		try {
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

			String url; // Non-url return values are used to give error messages.

			if(caseId == '' || caseId == null){
				url = null; // Give me a null and I'll give you a null. Null also indicates a problem, like an exception
			} else {
				List<Case> lst_cas = [SELECT Id, BI_Country__c, BI2_per_tipologia__c, Reason FROM Case WHERE Id = :caseId];
				System.debug('lst_cas: ' + lst_cas);
				if(lst_cas.size() > 0) {
					Case cas = lst_cas[0];
					List<BI2_Asignacion_Plantilla__c> lst_plantilla = [SELECT Id, BI2_Nombre_de_Plantilla__c FROM BI2_Asignacion_Plantilla__c WHERE BI2_Pais__c = :cas.BI_Country__c AND BI2_Tipologia__c = :cas.BI2_per_tipologia__c AND BI2_Motivo_del_Contacto__c = :cas.Reason];
					System.debug('lst_plantilla: ' + lst_plantilla);
					if(lst_plantilla.size() > 0) {
						if(lst_plantilla.size() > 1) System.debug(LoggingLevel.WARN, 'Varias (' + lst_plantilla.size() + ') posibles asignaciones: ' + lst_plantilla);

						BI2_Asignacion_Plantilla__c plant = lst_plantilla[0];
						url = '' + B2P_BI2_CASE_DOC;
						if(url != null || url != '') {
							url = url.replace('{NombreDePlantilla}', EncodingUtil.urlEncode(plant.BI2_Nombre_de_Plantilla__c, 'UTF-8'));
							url = url.replace('{IdDeCaso}', cas.Id);
						}
					} else {
						url = ''; // Could not find a match
					}
				} // else Id was not of a case (or was of a different Org)
			}

			System.debug('Return URL: ' + url);
			return url;
		} catch(Exception exc) {
			BI_LogHelper.generate_BILog('BI2_Buttons.getBit2PubDoc4Case', 'BI_EN', exc, 'WebService Button');
			return null;
		}
	}
}