@isTest
public class BI_ApprovalHelper_TEST {

    private static final String SEGMENT = 'Segmento';
    private static final String SUBSEGMENT = 'Multinacionales';
    private static string STAGE_F6= Label.BI_F6Preoportunidad;
    private static string STAGE_F4= Label.BI_DesarrolloOferta; //F4 - Offer Development
    private static Date closeDate = Date.today().addDays(30);

    @istest
    private static void testGetUseStandardTechnicalApproval() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        Opportunity opptyBadStage = new Opportunity(stagename=STAGE_F6, BI_Country__c = 'Argentina', BI_Nivel_de_aprobacion__c = 'Técnico', closedate = closeDate, amount=1000.0);
        BI_ApprovalHelper helper = new BI_ApprovalHelper(opptyBadStage);
        system.assert(helper.getUseStandardTechnicalApproval()); 

        Opportunity opptyBadCountry = new Opportunity(stagename=STAGE_F4, BI_Country__c = 'Chile', BI_Nivel_de_aprobacion__c = 'Técnico', closedate = closeDate, amount=1000.0);
        helper = new BI_ApprovalHelper(opptyBadCountry);
        system.assert(helper.getUseStandardTechnicalApproval()); 

        Opportunity goodOppty = new Opportunity(stagename=STAGE_F4,BI_Country__c = 'Argentina', BI_Nivel_de_aprobacion__c = 'Técnico', closedate = closeDate, amount=1000.0);
        helper = new BI_ApprovalHelper(goodOppty);
        system.assert(!helper.getUseStandardTechnicalApproval()); 
        
    }

    @istest
    private static void testSubmitTechnicalApprovalRequestInvalid() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;        
        Account a = new Account(name='Test Account', BI_Country__c = 'Argentina', BI_Segment__c = SEGMENT, BI_Subsegment_Local__c = SUBSEGMENT,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
        insert a;
        Opportunity oppty = new Opportunity(Accountid = a.id, name = 'Test Oppty', StageName = STAGE_F6, BI_Country__c = 'Argentina', BI_Nivel_de_aprobacion__c = 'Técnico', closedate = closeDate, amount=1000.0);
        insert oppty;
        
        BI_ApprovalHelper helper = new BI_ApprovalHelper(oppty);
        system.assert(!helper.submitTechnicalApprovalRequest());
        system.assertEquals(BI_ApprovalHelper.ERROR_NOT_READY_FOR_TECHNICAL_APPROVAL, helper.lastError);
        
    }

    @istest
    private static void testSubmitTechnicalApprovalNoSalesEngineer() { 
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;       
        Account a = new Account(name='Test Account',BI_Country__c =  'Argentina', BI_Segment__c = SEGMENT, BI_Subsegment_Local__c = SUBSEGMENT,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
        insert a;
        Opportunity oppty = new Opportunity(Accountid = a.id, name = 'Test Oppty', StageName = STAGE_F4, BI_Country__c = 'Argentina', BI_Nivel_de_aprobacion__c = 'Técnico', closedate = closeDate, amount=1000.0, BI_Productos_numero__c = 2);
        insert oppty;

        Profile standardProfile = [select id from Profile where name = :Label.BI_StandardUser];
        string email = 'user@example'+String.valueOf(Math.random())+'.com';
        User engineer = new User(
            Username = email,
            LastName = 'TestUser',
            CommunityNickname = 'nick' + String.valueOf(Math.random()),
            alias = 'testuser', //max length 8
            Email = email,
            profileid = standardProfile.id,
            TimeZoneSidKey = 'America/New_York',
            EmailEncodingKey='UTF-8',
            BI_Permisos__c = 'Ingeniería/Preventa',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US');
        insert engineer;
        
        OpportunityTeamMember member = new OpportunityTeamMember(OpportunityId = Oppty.id, UserId = engineer.id, teammemberrole = 'Pre-Sales Consultant');
        insert member;
        
        BI_ApprovalHelper helper = new BI_ApprovalHelper(oppty);
        system.assert(!helper.submitTechnicalApprovalRequest());
        system.assertEquals(BI_ApprovalHelper.ERROR_NO_SALES_ENGINEER, helper.lastError);
        
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:     
    Company:       Accenture
    Description:   
    <Date>            <Author>          <Description>

    13/11/2017        Gawron, Julian    Adding Test.StartTest() Test.StopTest()
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @istest
    private static void testSubmitTechnicalApprovalSuccess() {
        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;


        BI_MigrationHelper.skipAllTriggers();
        Account a = new Account(name='Test Account',BI_Country__c =  'Argentina', BI_Segment__c = SEGMENT, BI_Subsegment_Local__c = SUBSEGMENT,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
        insert a;
        Opportunity oppty = new Opportunity(Accountid = a.id, name = 'Test Oppty', StageName = STAGE_F4, BI_Country__c = 'Argentina', BI_Nivel_de_aprobacion__c = 'Técnico', closedate = closeDate, amount=1000.0, BI_Productos_numero__c = 2);
        insert oppty;
        BI_MigrationHelper.cleanSkippedTriggers();       

        Profile standardProfile = [select id from Profile where name = :Label.BI_StandardUser];
        string email = 'user@example'+String.valueOf(Math.random())+'.com';
        User engineer = new User(
            Username = email,
            LastName = 'TestUser',
            CommunityNickname = 'nick' + String.valueOf(Math.random()),
            alias = 'testuser', //max length 8
            Email = email,
            profileid = standardProfile.id,
            TimeZoneSidKey = 'America/New_York',
            EmailEncodingKey='UTF-8',
            BI_Permisos__c = 'Ingeniería/Preventa',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US');
        insert engineer;
        Test.startTest();
        OpportunityTeamMember member = new OpportunityTeamMember(OpportunityId = Oppty.id, UserId = engineer.id, teammemberrole = 'Sales Engineer');
        insert member;

        BI_ApprovalHelper helper = new BI_ApprovalHelper(oppty);
        system.assert(string.isblank(helper.lastError));
        system.assert(helper.submitTechnicalApprovalRequest());
        Test.stopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}