public class BI_COL_CreateProduct_cls 
{
    public BI_COL_CreateProduct_cls() 
    {
        
    }

    @InvocableMethod
    public static void invokeapexcallout( list<BI_COL_Homologacion_integraciones__c> lstHomologacion ) 
    {
        List<String> idHomologacion = new List<String>();
        List<BI_Log__c> logs = new List<BI_Log__c>();
        List<String> lstIDproductos = new List<string>();

        /*User usu = [
            Select  CompanyName,Profile.Name,Pais__c 
            from    User 
            where   Id =: userinfo.getuserid()];*/
        
        if( !System.isFuture() && !System.isBatch()) //&& usu.Pais__c == 'Colombia')
        {
            System.debug('\n\n########------->Ejecución Trigger crearUsuarioTRS \n\n');
            for( BI_COL_Homologacion_integraciones__c hm : lstHomologacion)
            {
                idHomologacion.add( hm.Id );
                BI_Log__c registroBitacora = new BI_Log__c();
                registroBitacora.BI_COL_Identificador__c = 'CREACION PRODUCTO';
                registroBitacora.BI_COL_Estado__c = 'Pendiente';
                registroBitacora.BI_COL_Tipo_Transaccion__c = 'CREACION PRODUCTO';
                //registroBitacora.BI_COL_Producto_Telefonica__c= hm.ID;
                logs.add( registroBitacora );
                lstIDproductos.add( hm.ID );
            }
            
            BI_COL_CreateProduct_cls.wsMethodCreacion( lstIDproductos );

            insert logs;
        }
    }
    
    //Antonio Pardo - 17/05/2016        Uncommented

   @Future (callout=true)
   public static void wsMethodCreacion( List<String> lstIDproductos )
   {
        ws_TrsMasivo.ProductoPDataType[] productosPDataT = new ws_TrsMasivo.ProductoPDataType[0];
        Map<String, ws_TrsMasivo.ProductoPDataType> infoProdEnviados = new Map<String, ws_TrsMasivo.ProductoPDataType>();
        
        List<BI_COL_Homologacion_integraciones__c> lstProductos = [
            SELECT  ID, NAME, BI_COL_Ancho_banda__c, BI_COL_Linea__c, BI_COL_Producto__c, BI_COL_Segmento__c,
                    BI_COL_Descripcion_referencia__c, BI_COL_Cod_Desc_Referencia__c
            FROM    BI_COL_Homologacion_integraciones__c
            where   Id IN: lstIDproductos  ];

            
        System.debug('=========>>> lstProductos '+lstProductos);

        List<String> idProdTel = new List<string>();
        String estadoProceso = 'Invocacion Correcta';
        String error = 'La respuesta fue nula';

        for( BI_COL_Homologacion_integraciones__c producto : lstProductos )
        {
            idProdTel.add( producto.ID );
            ws_TrsMasivo.ProductoPDataType productoPDataType = new ws_TrsMasivo.ProductoPDataType();
            productoPDataType.cts_name = producto.Name;//Name
            productoPDataType.cts_anchos_de_banda = producto.BI_COL_Ancho_banda__c;//productoTelefonica.Anchos_de_banda__c.Name
            productoPDataType.cts_linea = producto.BI_COL_Linea__c;//Linea__c
            productoPDataType.cts_producto = producto.BI_COL_Producto__c;//Producto__c
            productoPDataType.cts_segmento = producto.BI_COL_Segmento__c;//Segmento__c
            productoPDataType.cts_descripcion_referencia = producto.BI_COL_Descripcion_referencia__c;//Descripcion_Referencia__c
            productoPDataType.cts_cod_desc_referencia = producto.BI_COL_Cod_Desc_Referencia__c;//Cod_Desc_Referencia__c
            productosPDataT.add( productoPDataType );
            infoProdEnviados.put( productoPDataType.cts_name, productoPDataType );
        }

        System.debug('**************** Productos en objeto WS \n'+productosPDataT);ws_TrsMasivo.RespuestaProductoPDataType[] respuestas=null;

        try
        {
        
         /* 04/08/2015 Daniel Lopez: Comenta el test is running test, para usar el MOCK*/
        //    if( Test.isRunningTest() )
        //    {
        //          ws_TrsMasivo.RespuestaProductoPDataType respuestaTest = new ws_TrsMasivo.RespuestaProductoPDataType();
        //          respuestaTest.cts_name = productosPDataT[0].cts_name;
        //          respuestaTest.codigoError = 0;
        //          ws_TrsMasivo.RespuestaProductoPDataType respuestaTestf = new ws_TrsMasivo.RespuestaProductoPDataType();
        //          respuestaTestf.cts_name = productosPDataT[1].cts_name;
        //          respuestaTestf.codigoError = 1;
        //          respuestas = new ws_TrsMasivo.RespuestaProductoPDataType[0];
        //          respuestas.add( respuestaTest );
        //          respuestas.add( respuestaTestf );
        //    }
        //    else
        //    {
                ws_TrsMasivo.serviciosSISGOTSOAP ws_TRSViaT = new ws_TrsMasivo.serviciosSISGOTSOAP();
                respuestas = ws_TRSViaT.CargarProductosP(productosPDataT);
            //}

        }
        catch( Exception e )
        {
            System.debug('Falló la comunicación. Excepción:\n'+e);
            estadoProceso = 'Falló la comunicación. Excepción: ';
            error = e+'';
        }

        List<BI_Log__c> bitacoras = [
            SELECT  Name, BI_COL_Estado__c, BI_COL_Identificador__c, BI_COL_Tipo_Transaccion__c,
                    Descripcion__c, BI_COL_Informacion_recibida__c, BI_COL_Producto_Telefonica__c
            FROM    BI_Log__c
            WHERE   BI_COL_Producto_Telefonica__c IN :idProdTel
            AND     BI_COL_Tipo_Transaccion__c = 'CREACION PRODUCTO'
            AND     BI_COL_Estado__c = 'Pendiente'
            ];

        System.debug('=========>>> bitacoras '+bitacoras);
        if( estadoProceso == 'Invocacion Correcta' || ( respuestas != null && respuestas.size() > 0 ) )
        {
            Map<String,BI_Log__c> mapaBitacoras = new Map<String,BI_Log__c>();
            
            for( BI_Log__c log : bitacoras )

                    mapaBitacoras.put( log.BI_COL_Identificador__c, log );
                    System.debug('mapaBitacoras ' + mapaBitacoras);

            for( ws_TrsMasivo.RespuestaProductoPDataType rta : respuestas )
            {
                
                //Antonio Pardo - 24/05/2016       Code optimization, prevent null pointer BI_Log

                if(mapaBitacoras.containsKey(rta.cts_name)){               
                    BI_Log__c log = mapaBitacoras.get( rta.cts_name );

                    System.debug('=========>>> rta '+rta);
                    System.debug('=========>>> rta. name '+rta.cts_name);
                    System.debug('\n\n=========>>>infoProdEnviados '+infoProdEnviados);
                    System.debug('\n\n=========>>>infoProdEnviados.get( rta.cts_name )  '+infoProdEnviados.get( rta.cts_name ));

                    if(infoProdEnviados.containsKey( rta.cts_name ) ){ //Fix Bug
                        log.Descripcion__c = infoProdEnviados.get( rta.cts_name )!=null?( infoProdEnviados.get( rta.cts_name ) ) + '':'';
                    }
                    if( rta.codigoError == 0 )
                    {
                        log.BI_COL_Estado__c = 'Exitoso';
                        log.BI_COL_Informacion_recibida__c = rta.respuestaAccion;
                    }
                    else
                    {
                        log.BI_COL_Informacion_recibida__c =rta.respuestaAccion!=null?rta.codigoError +' '+ rta.respuestaAccion:'rta';
                        log.BI_COL_Estado__c = 'Fallido';
                    }
                }
            } 
        }
        else
        {
            for( BI_Log__c log : bitacoras )
            {
                log.BI_COL_Informacion_recibida__c = 'Falló la comunicación. ' + error;
                log.BI_COL_Estado__c = 'Fallido';
            }
        }
        update bitacoras;
    }
}