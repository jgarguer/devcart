public with sharing class BI_O4_Project_deleteProject_Controller {

	public Id idProject {get; set;}
	public String urlBackId { get { return '/' + idProject;} }
	public String urlProjectsTab { get { return '/' + SObjectType.Milestone1_Project__c.getKeyPrefix();} }

	public BI_O4_Project_deleteProject_Controller(ApexPages.StandardController stc) {
		idProject = stc.getId();
		if(String.isBlank(String.valueof(idProject))) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a project to delete'));
		}
	}

	public PageReference deleteProject() {
		
		if(idProject != null) {

			Savepoint sp = Database.setSavepoint();
			try {
				if(BI_TestUtils.isRunningTest())
                	throw new BI_Exception('test');

				Milestone1_Project__c project = new Milestone1_Project__c(Id=idProject, BI_O4_Bypass__c = 4);
				update project;
				delete project;
				return new PageReference(urlProjectsTab);
				
			} catch (Exception exc) {

				ApexPages.addMessages(exc);
				Database.rollback(sp);
				//BI_LogHelper.generate_BILog('BI_O4_Project_deleteProject_Controller.deleteProject', 'BI_O4', exc, 'ApexClass');
				return null;

			}
		}
		return null;
	}
}