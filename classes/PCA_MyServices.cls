public with sharing class PCA_MyServices extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Controller of relationship management page
    
    History:
    
    <Date>            <Author>              <Description>
    16/12/2014        Antonio Moruno        Initial version
    02/12/2016        Pedro Párraga         Map creation and call to PCA_ProfileHelper.getPermissionSet ();
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    public BI_ImageHome__c ImgBack {get;set;}
    public Attachment AttImg {get;set;}
    public BI_ImageHome__c RelationImage {get;set;}
    public BI_ImageHome__c RelationImageMovi {get;set;}
    public BI_ImageHome__c RelationImageTel {get;set;}
    public String AccSegmento {get;set;}
    public map<string,string> mapVisibility {get; private set;}
    public String oneDriveLink {get;set;}
    public User portalUser{get;set;}
    public list<Account> ActAccs {get;set;}
    public string ValueOption {get;set;}
    public list <User> UserActual {get;set;}
    public String AccName{get;set;}
    
    public PCA_MyServices() {
        mapVisibility = PCA_ProfileHelper.getPermissionSet();
        
        List<User> listPortalUser = [Select id, Pais__c, ContactId, ProfileId, Profile.Name from User where id=:UserInfo.getUserId() limit 1];
            
        if(listPortalUser!= null && listPortalUser.size()>0) {
            this.portalUser = listPortalUser.get(0); 
        }
        
        //login = false;
                ActAccs = BI_AccountHelper.getAccounts();
                
                Id ActAccId = BI_AccountHelper.getCurrentAccountId();
                
                
                if (ActAccId!=null){
                    UserActual = [Select Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User where Id =:UserInfo.getUserId()];
                    //system.debug('UserActual' +UserActual[0]);
                    if(ActAccs.size() == 1){
                        system.debug('ActAccs' +ActAccs);
                        system.debug('ActAccs.size' +ActAccs.size());
                        String TotalName = ActAccs[0].Name;
                        if(TotalName.length() > 32){
                            AccName = TotalName.substring(0,32);
                            AccName = AccName + '...';
                        }else{
                            AccName = TotalName;
                        }
                        
                    }
                    ValueOption = ActAccId;
                    Account SegAccount = [Select Id, Name,BI_Country__c, BI_Segment__c, CWP_OneDriveAddress__c FROM Account Where Id=:ActAccId];
                    if(SegAccount!=null){
                        //List<String> accSegmentoList = SegAccount[0].BI_SegmentoRef__r.Name;
                        AccSegmento =  SegAccount.BI_Segment__c;
                        system.debug('AccSegmento**'+AccSegmento);
                    }
                    //buildFooter(SegAccount); 
                    
                    /*---------------------
                    Start Redirect Documentation Change
                    -----------------------*/
                    system.debug('perfil: ' + portalUser.Profile.Name);
                    system.debug('visibilidad de documentos: ' + mapVisibility.get('pca_documentmanagement'));
                    system.debug('link a one drive: ' + SegAccount.CWP_OneDriveAddress__c);
                    if(portalUser.Profile.Name.containsIgnoreCase('TGS') && mapVisibility.get('pca_documentmanagement') != null && mapVisibility.get('pca_documentmanagement') == 'true'){
                        if(SegAccount.CWP_OneDriveAddress__c != null){
                            oneDriveLink = SegAccount.CWP_OneDriveAddress__c;
                        }else{
                            mapVisibility.put('pca_documentmanagement', 'false');
                        }
                    }
                    
                    /*---------------------
                    End Redirect Documentation Change
                    -----------------------*/

                }else{
                    ValueOption = ' '; 
                }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   method that check permissions
    History:
    
    <Date>            <Author>              <Description>
    16/12/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public PageReference checkPermissions (){
        try{
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_MyServices.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Return the image and description of the global page
    
    History:
    
    <Date>                    <Author>               <Description>
    17/12/2014                Antonio Moruno         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public void loadInfo(){
        try{
        Id ActAccId = BI_AccountHelper.getCurrentAccountId();
            system.debug('**Acc: ' + ActAccId);
        Account SegAccount = [Select Id, Name,BI_Country__c,BI_Segment__c FROM Account Where Id=:ActAccId];
        List<BI_ImageHome__c> RelImg = new List<BI_ImageHome__c>();
            if(SegAccount.BI_Segment__c!=null){
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'My Service' AND  RecordType.Name= 'General' AND BI_Segment__c=: SegAccount.BI_Segment__c LIMIT 1];
                
                if(!RelImg.isEmpty()) {
                    if(RelImg[0].BI_Segment__c!=null){
                        
                        if(RelImg[0].BI_Segment__c=='Empresas'){
                            RelationImageTel = RelImg[0];
                        }else if(RelImg[0].BI_Segment__c=='Negocios'){
                            RelationImageMovi = RelImg[0];
                        }
                    }
                }
            }else{
                    RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'My Service' AND  RecordType.Name= 'General'  AND BI_Segment__c='Empresas' LIMIT 1];
                    if(!RelImg.isEmpty()){
                        RelationImageTel = RelImg[0];
                    }else{
                        RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'My Service' AND  RecordType.Name= 'General'  AND BI_Segment__c='Negocios' LIMIT 1];
                        if(!RelImg.isEmpty()){
                            RelationImageMovi = RelImg[0];
                        }
                    
                    }
                }
        if(RelationImageTel!=null){
            
            ImgBack = RelationImageTel;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageTel.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        }else if(RelationImageMovi!=null){
            
            ImgBack = RelationImageMovi;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageMovi.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        }/*else if(RelationImage!=null){
            
            ImgBack = RelationImage;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImage.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        
        }*/        
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_MyServices.loadInfo', 'Portal Platino', Exc, 'Class');
        
        }
    }
    
    public boolean esTGS {get{
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            System.debug('###Current profile: ' + pName);
            return pName.startsWith('TGS');
        }
        return false;
    }
    set;}

}