@isTest(isParallel=true)
public without sharing class TGS_Portal_Utils_2_TEST {
    
    static String profileRunAs = 'TGS System Administrator';
    static Contact c;
    static Case caseT;
    static Account holding;
    static Account customerCountry;
    static Account legalEntity;
    static Account businessUnit;
    static Account costCenter;
    static NE__Product__c product;
    static NE__OrderItem__c orderItem;
    
    //Dummy Portal User    
    enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert){
        return getPortalUser(portalType,userWithRole,doInsert,3);        
    }
    
    //Se agrega el level de account que se va a asignar al Contact
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert,integer level) {
        BI_TestUtils.throw_exception=false;
        /* Make sure the running user has a role otherwise an exception 
           will be thrown. */
        if(userWithRole == null) {   
            
            if(UserInfo.getUserRoleId() == null) {

                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com',
                                    BI_Permisos__c = 'Empresas Platino', isActive = True);
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            System.assert(userWithRole.userRoleId != null, 
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }

        Account a;
        Contact cAux;
        System.runAs(userWithRole) {

            a = TGS_Dummy_Test_data.dummyHierarchy();
            //Database.insert(a);
            String accId;
            if(level==1){//Holding
                accId=a.Parent.parentId;
            }else if(level==2){// Customer Country
                accId=a.ParentId;
            }else if(level==3){// Legal entity
                accId=a.id;
            }else if(level==4){// BusinessUnit
                Account auxAcc=[SELECT Id FROM Account WHERE parentId=:a.id LIMIT 1];
                accId=auxAcc.Id;
            }else if(level==5){// Cost Center
                Account auxAcc=[SELECT Id FROM Account WHERE parent.parentId=:a.id LIMIT 1];
                accId=auxAcc.Id;
            }            
            cAux = new Contact(AccountId = accId,
                            lastname = 'lastname',
                            Email = 'contactTest@telefonica.com',
                            BI_Activo__c = true
                            );
            Database.insert(cAux);

        }
        
        /* Get any profile for the given type.*/
        Profile p = [select id 
                      from profile 
                     where usertype = :portalType.name() and Name='TGS Customer Community Plus'
                     limit 1];   
        
        String nRandom = String.valueOf((Integer)(Math.random()*100));
        String nRandom2 = String.valueOf((Integer)(Math.random()*100));
        String testemail = 'telefonicaTest'+nRandom+'@tefonica.com';
        User pu = new User(profileId = p.id, username = testemail+nRandom2, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = cAux.id, BI_Permisos__c = 'Empresas Platino',
                          isActive = True);
        
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }

    private static void setConfiguration() {
        Test.startTest();
        
        legalEntity = TGS_Dummy_Test_Data.dummyHierarchy();        
        List<Account> listAcc = TGS_Portal_Utils.getLevel1(legalEntity.Id, 1);
        if(listAcc.size()>0){
            holding = listAcc[0];
        }
        listAcc = TGS_Portal_Utils.getLevel2(legalEntity.Id, 1);
        if(listAcc.size()>0){
            customerCountry = listAcc[0];
        }
        listAcc = TGS_Portal_Utils.getLevel4(legalEntity.Id, 1);
        if(listAcc.size()>0){
            businessUnit = listAcc[0];
            TGS_Dummy_Test_Data.dummyPuntoInstalacion(businessUnit.Id);
        }
        listAcc = TGS_Portal_Utils.getLevel5(legalEntity.Id, 1);
        if(listAcc.size()>0){
            costCenter = listAcc[0];
        }
        caseT = TGS_Dummy_Test_Data.dummyTicketCase();
        caseT.AccountId = legalEntity.Id;
        update caseT;
        c = TGS_Dummy_Test_Data.dummyContact(legalEntity.Id);
        product = TGS_Dummy_Test_Data.dummyCommercialProduct();
        orderItem = TGS_Dummy_Test_Data.dummyConfiguration();
        
        Test.stopTest();
    }
    
	static testMethod void getCatalogItemsWithStandardVisibilityTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            setConfiguration();
            NE__Catalog_Item__c catalogItem = [SELECT Id, Name FROM NE__Catalog_Item__c WHERE Id = :orderItem.NE__CatalogItem__c LIMIT 1];
            List<NE__Catalog_Item__c> listCatalogItem = TGS_Portal_Utils.getVisibleCatalogItems(userTest.Id);
        }
    }
    
    static testMethod void getStatusMachineListByCaseStatusTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            setConfiguration();
            
            NE__OrderItem__c testOrderItem=TGS_Dummy_Test_Data.dummyConfiguration();
            Id orderId=testOrderItem.NE__OrderId__c;
            Case testCase=createDummyCase(orderId, userTest.Id);
            
            system.assertNotEquals(null,testCase);
            List<TGS_Status_machine__c> listStatusMachineTest =TGS_Portal_Utils.getStatusMachineListByCaseStatus(testCase);
            system.assertNotEquals(null,listStatusMachineTest);
            
        }
    }
    
    static testMethod void getProfileOfCreatorOfCaseTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            setConfiguration();
            NE__OrderItem__c testOrderItem=TGS_Dummy_Test_Data.dummyConfiguration();
            Id orderId=testOrderItem.NE__OrderId__c;
            
            Case testCase=createDummyCase(orderId,userTest.Id);
            
            system.assertNotEquals(null,testCase);
            String profileCreatedByTest =TGS_Portal_Utils.getProfileOfCreatorOfCase(testCase);
            system.assertNotEquals(null,profileCreatedByTest);
            
        }
    }
    
    private static Case createDummyCase(Id confId, Id testUserId) {
        List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Order Management Case'];
        Case testCase;
        if(listRecordTypes.size()>0){
            testCase = new Case(CreatedById=testUserId,RecordTypeId = listRecordTypes[0].Id, Subject = 'Test Subject', Status='Assigned',TGS_Status_reason__c='', Order__c = confId, Asset__c= confId, TGS_Service__c='Universal WIFI');
            upsert testCase;
        }
        return testCase;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Test methods that are too small (generaly 4 lines or fewer) to justify giving them
					 their own test method
    
      History:
      <Date>        <Author>              <Description>
      02/02/2016    Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @isTest static void test_smallMethods() {
    
        List<Case> lstCas = new List<Case>{ new Case(Subject='TEST SUBSUB', Status='Assigned', TGS_Service__c='TEST SERVICE') };
        insert lstCas;
        lstCas = (List<Case>)TGS_Portal_Utils.executeQuery('SELECT Id, Subject, RecordTypeId, TGS_Service__c FROM Case');
        
        List<Attachment> lstAtt = new List<Attachment>();
        for(Case cas : lstCas) {
            lstAtt.add(new Attachment(Name='Attch of: ' + cas.Id, ParentId=cas.Id, Body=Blob.valueOf('Attch of: ' + cas.Id)));
        }
        insert lstAtt;
        
        Test.startTest();
        TGS_Portal_Utils.getConfigurationItems('');
        TGS_Portal_Utils.getActualCase(lstCas[0].Id);
        TGS_Portal_Utils.getRecordTypeById(lstCas[0].RecordTypeId);
        TGS_Portal_Utils.getAttributesbyOrderItem(null);
        TGS_Portal_Utils.getConfItemSubser(null);
        TGS_Portal_Utils.getTemplateCaseId(lstCas[0].Id);
        TGS_Portal_Utils.getCaseNumber(null);
        TGS_Portal_Utils.getSites(null);
        TGS_Portal_Utils.getlookupServiceUnits(new List<Account>());
        TGS_Portal_Utils.getCategoryName('');
        TGS_Portal_Utils.getProductName(null);
        TGS_Portal_Utils.getOrderItem(null);
        TGS_Portal_Utils.getPropertiesDomains(new List<NE__ProductFamilyProperty__c> { new NE__ProductFamilyProperty__c() });
        TGS_Portal_Utils.getConfigurationItemAttributes(null, null, 'Enumerated');
        TGS_Portal_Utils.getConfigurationItemAttributes(null, null, '');
        TGS_Portal_Utils.getServiceIcon(lstCas[0].Id);
        TGS_Portal_Utils.getCaseRecordType();
        TGS_Portal_Utils.getCaseHistory();
        TGS_Portal_Utils.getStatusMachineListByCaseForDesiredStatusAndStatusReason(lstCas[0], 'Assigned', null);
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Test the method TGS_Portal_Utils.getInstalledServices
    
      History:
      <Date>        <Author>              <Description>
      02/02/2016    Jose Miguel Fierro    Initial version
      29/11/2016    Gawron, Julian        Mixed SQL Exception fixed
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @isTest static void test_getInstalledServices() {
        User portalUser = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        //  User portalUser = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        String selectedService = ''; //dummyConfiguration, dummyCommercialProduct, createDummyConfigurationSecurityServices
        Id userId = portalUser.Id;
        String filterCondition = '';
        String selectedCurrency = 'EUR'; // NE__CatalogItem__r.CurrencyIsoCode
        boolean serviceWithCurrency = true;
        integer maxrows = 15;
        
        Test.startTest();
         TGS_Portal_Utils.getInstalledServices(selectedService, userId, filterCondition, selectedCurrency, serviceWithCurrency, maxrows);
        Test.stopTest();
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Test the method TGS_Portal_Utils.getInstalledServicesByCategoryAndCurrency
    
      History:
      <Date>            <Author>              <Description>
      03/02/2016		  Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @isTest static void test_getInstalledServicesByCategoryAndCurrency() {
        User portalUser = getPortalUser(PortalType.PowerCustomerSuccess, null, true);   
        
        String selectedService = '';
        Id userId = portalUser.Id;
        String filterCondition = '';
        String selectedCurrency = 'EUR'; // NE__CatalogItem__r.CurrencyIsoCode
        integer maxrows = 15;
        
        Test.startTest();
        TGS_Portal_Utils.getInstalledServicesByCategoryAndCurrency(selectedService, userId, filterCondition, selectedCurrency, maxrows);
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Test the method TGS_Portal_Utils.getInstalledServices
    
      History:
      <Date>        <Author>              <Description>
      03/02/2016    Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @isTest static void test_getKeyFamilyProperties() {
        TGS_Dummy_Test_Data.dummyCatalogItem();
        List<NE__Product__c> lstPrds  = [SELECT Id FROM NE__Product__c Limit 1]; //Adding limit 1 
        String selectedService = lstPrds[0].Id;
        
        Test.startTest();
		TGS_Portal_Utils.getKeyFamilyProperties(selectedService);        
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Test the method TGS_Portal_Utils.validTicketOrderServiceVisibility
    
      History:
      <Date>        <Author>              <Description>
      03/02/2016    Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @isTest static void test_validTicketOrderServiceVisibility() {
        User portalUser;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            portalUser = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        }
        NE__Catalog_Item__c catIt;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            catIt = TGS_Dummy_Test_Data.dummyCatalogItem();
        }
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            TGS_Portal_User_Configuration__c puc = new TGS_Portal_User_Configuration__c(Contact__c=portalUser.ContactId);
            insert puc;
            TGS_Portal_User_Configuration_Product__c pucp = new TGS_Portal_User_Configuration_Product__c(Portal_User_Configuration__c = puc.Id, Commercial_Product__c=catIt.NE__ProductId__c);
            insert pucp;
        }
        System.runAs(portalUser) {
            Test.startTest();
            TGS_Portal_Utils.validTicketOrderServiceVisibility(1, '');
            try {
                TGS_Portal_Utils.validTicketOrderServiceVisibility(2, ''); // NE__Order_Item__c no tiene AccountId
            } catch (QueryException exc) {
                System.debug('[TGS_Portal_Utils_2_TEST.test_validTicketOrderServiceVisibility]: Exception: ' + exc);
            }
            
            Test.stopTest();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Test the method TGS_Portal_Utils.getInstalledServicesByFamily
    
      History:
      <Date>        <Author>              <Description>
      04/02/2016    Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @isTest static void test_getInstalledServicesByFamily() {
        User portalUser;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            portalUser = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        }
        
        System.runAs(portalUser) {
            Test.startTest();
            TGS_Portal_Utils.getInstalledServicesByFamily('A Service Family', portalUser.id, '', 15);
            Test.stopTest();
        }
    }
    
}