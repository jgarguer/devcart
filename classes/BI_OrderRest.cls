@RestResource(urlMapping='/accountresources/v1/orders/*')
global class BI_OrderRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Order (Individual) Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    24/07/2014        Pablo Oliva       Initial version
    10/11/2015        Fernando Arteaga  BI_EN - CSB Integration
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains detailed information stored in the server for a specific order.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.OrderInfoType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    24/07/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.OrderInfoType getOrder() {
		
		BI_RestWrapper.OrderInfoType res;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				//ONE ORDER
				res = BI_RestHelper.getOneOrder(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND, 200 OK
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_OrderRest.getOrder', 'BI_EN', exc, 'Web Service');
			
		}
		
		return res;
		
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Updates the order with the subscriptionId from the external system.
    
    IN:            RestContext.request
    OUT:           void
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          	<Description>
    10/11/2015        Fernando Arteaga      Initial version
    06/04/2016	      Fernando Arteaga      Update Descripcion__c with error text
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void updateOrder()
	{
		try
		{
			Blob blob_json = RestContext.request.requestBody;
			String string_json = blob_json.toString().replace('"Transaction":', '"Transaction_x":');
			BI_RestWrapper.MultiSubscriptionNotificationType notifType = (BI_RestWrapper.MultiSubscriptionNotificationType) JSON.deserialize(string_json, BI_RestWrapper.MultiSubscriptionNotificationType.class);
			System.debug('notifType:' + notifType);
			String transactionId = notifType.transaction_x.transactionId;
			Boolean resourceDataIsNull = false;

			List<NE__Order__c> lst_order = [SELECT Id, NE__SerialNum__c, NE__OrderStatus__c, BI_CSB_Descripcion_Errores__c
											FROM NE__Order__c
											WHERE BI_Id_Legado__c = :transactionId limit 1];
		
			if(!lst_order.isEmpty())
			{
				if (notifType.transaction_x.transactionStatus.transactionStatus == 'Success')
				{
					if (notifType.resourceData != null)
						lst_order[0].NE__SerialNum__c = notifType.resourceData.subscriptionId;
					else
						resourceDataIsNull = true;
						
					lst_order[0].NE__OrderStatus__c = 'Completed';
				}
				else if (notifType.transaction_x.transactionStatus.transactionStatus == 'Fail')
				{
					lst_order[0].NE__OrderStatus__c = 'Error CSB';
					lst_order[0].BI_CSB_Descripcion_Errores__c = notifType.transaction_x.transactionStatus.error.exceptionText;
				}
				
				if (notifType.transaction_x.transactionStatus.transactionStatus == 'Success' ||
					notifType.transaction_x.transactionStatus.transactionStatus == 'Fail')
				{
					update lst_order[0];
					
					List<NE__OrderItem__c> lst_orderItems = [SELECT Id,  NE__Status__c
															 FROM NE__OrderItem__c
															 WHERE NE__OrderId__c = :lst_order[0].Id
															 AND NE__CatalogItem__r.BI_CSB_Code__c != null];
					if (!lst_orderItems.isEmpty())
					{
						for (NE__OrderItem__c item: lst_orderItems)
							item.NE__Status__c = lst_order[0].NE__OrderStatus__c;
	
						update lst_orderItems;
					}
					
					List<NE__Order_Header__c> lst_ordHeader = [SELECT Id, NE__OrderStatus__c
															   FROM NE__Order_Header__c
															   WHERE NE__OrderId__c = :lst_order[0].Id];
					
					if (!lst_ordHeader.isEmpty())
					{
						lst_ordHeader[0].NE__OrderStatus__c = lst_order[0].NE__OrderStatus__c;
						update lst_ordHeader[0];
					}
					
					List<BI_Log__c> trans = [SELECT Id, BI_COL_Estado__c, Descripcion__c
							 				 FROM BI_Log__c
											 WHERE BI_CSB_Record_Id__c = :String.valueOf(lst_order[0].Id).substring(0, 15)
											 ORDER BY CreatedDate DESC LIMIT 1];
											 
					if (!trans.isEmpty())
					{
						trans[0].BI_COL_Estado__c = notifType.transaction_x.transactionStatus.transactionStatus == 'Success' ? Label.BI_CSB_Procesado : Label.BI_CSB_Fallido;
						if (resourceDataIsNull)
							trans[0].Descripcion__c = Label.BI_CSB_Verificar_Pedido; // FAR 19/04/2016
						else
							trans[0].Descripcion__c = notifType.transaction_x.transactionStatus.transactionStatus == 'Fail' ? notifType.transaction_x.transactionStatus.error.exceptionText : Label.BI_CSB_Pedido_Activado;
						//trans[0].Descripcion__c = notifType.transaction_x.transactionStatus.transactionStatus == 'Fail' ? notifType.transaction_x.transactionStatus.error.exceptionText : Label.BI_CSB_Pedido_Activado; // FAR 06/04/2016
						update trans[0];
					}
					
					System.debug('after update order and transaction');
				}
				
				RestContext.response.statuscode = 200;//OK
			}else{
				RestContext.response.statuscode = 404;//NOT_FOUND
			}		
			
		}catch(Exception exc){
			System.debug('exc:' +  exc.getStackTraceString());
			System.debug('exc:' +  exc.getLineNumber());
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage', exc.getMessage());
			BI_LogHelper.generate_BILog('BI_OrderRest.updateOrder', 'BI_EN', exc, 'Web Service');
			
		}
	}
}