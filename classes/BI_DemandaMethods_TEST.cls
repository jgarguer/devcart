/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Masferrer
Company:       NEAborda
Description:   Test class for BI_DemandaMethods

History:

<Date>            <Author>          <Description>
01/03/2014    Antonio Masferrer	    Initial version
31/10/2017	  Angel F. Santaliestra Added the test_calculateTimeBetweenStages.
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class BI_DemandaMethods_TEST {

	public static Group grp_dem;
	public static Group grp_pro;
	public static Group grp_pet;

	public static List<BI_Demanda__c> lst_dem = new List<BI_Demanda__c>();

	public static User thisUser;

	public static Set<Id> set_id = new Set<Id>();

	static{

		BI_TestUtils.throw_exception = false;	
	}

	public static void dataLoad(){
		
		grp_dem =[SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cola demandas' LIMIT 1];
		grp_pro =[SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cola proyectos' LIMIT 1];
		grp_pet =[SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cola petición' LIMIT 1];

		thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		
		for(Integer i = 0; i < 5; i++){
			BI_Demanda__c dem = new BI_Demanda__c();
			dem.BI_Asunto_de_la_demanda__c = 'XXX test BI_Demanda' + i;
			dem.BI_Tipologia__c = 'Demanda';
			dem.BI_Fecha_de_solicitud__c = Date.today();
			dem.BI_Objeto_sobre_el_que_aplica__c = 'Casos';
			dem.BI_OB_solicitante__c = 'Ecuador';
			dem.BI_Prioridad_del_Cliente__c = 'Alta';
			dem.BI_Horas_Estimadas__c = 5;
			dem.BI_Necesidad_del_negocio__c = 'XXX test BI_Demanda' + i;
			dem.BI_Solicitante__c = thisUser.Id;
			dem.BI_Estado_de_la_demanda__c = '1- Por revisar'; //JEG 23/11/2017
			lst_dem.add(dem);
		}

		insert lst_dem;

		for(BI_Demanda__c d : lst_dem){
			set_id.add(d.id);
		}
	}
	@isTest static void test_calculateTimeBetweenStages() {
	
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        
        insert userTGS;

        dataLoad();

        List<BI_Demanda__c> lst_d = [SELECT Id, BI_Tipologia__c, BI_Estado_de_la_demanda__c, OwnerId, BI_Tiempo_1_Por_revisar__C FROM BI_Demanda__c WHERE Id IN: set_id];
        Datetime dTime = System.now();
    	lst_d[1].BI_Fecha_ultima_modificacion_etapa__c = dTime.addDays(-3); // 3 dias = 4320 minutos.
        lst_d[1].BI_Tiempo_1_Por_revisar__C = 0;
        update lst_d[1];

        Id idDemanda = lst_d[1].Id;

        lst_d[1].BI_Estado_de_la_demanda__c = '2- Congelado';
        update lst_d[1];

        BI_Demanda__c oDemanda = [SELECT Id, BI_Tipologia__c, BI_Estado_de_la_demanda__c, OwnerId, BI_Tiempo_1_Por_revisar__C FROM BI_Demanda__c WHERE Id = :idDemanda];
        
        system.assertEquals(oDemanda.BI_Tiempo_1_Por_revisar__C, 4320);
    }
	@isTest static void test_asignaPropietario() {
	
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        
        insert userTGS;

        dataLoad();

        List<BI_Demanda__c> lst_d = [SELECT Id, BI_Tipologia__c, BI_Estado_de_la_demanda__c, OwnerId FROM BI_Demanda__c WHERE Id IN: set_id];

        system.assertEquals(lst_d.size(), 5);

        lst_d[0].BI_Tipologia__c = 'Instalación Paquete';
        lst_d[1].BI_Tipologia__c = 'Demanda';
        lst_d[2].BI_Tipologia__c = 'Proyecto';
        lst_d[3].BI_Tipologia__c = 'Petición Ambientes';

        update lst_d;

        lst_d[0].BI_Tipologia__c = 'Demanda';
        lst_d[1].BI_Tipologia__c = 'Proyecto';
        lst_d[2].BI_Tipologia__c = 'Petición Ambientes';
        lst_d[3].BI_Tipologia__c = 'Instalación Paquete';

        update lst_d;

		List<BI_Asignacion_demanda__c> lst_cambio_estado = [SELECT Id FROM BI_Asignacion_demanda__c WHERE BI_Demanda__c IN: set_id]; 

		lst_d = [SELECT Id, BI_Tipologia__c, BI_Estado_de_la_demanda__c, OwnerId FROM BI_Demanda__c WHERE Id IN: set_id];  

		system.assertEquals(lst_cambio_estado.size(), 7);
		system.assertEquals(grp_dem.Id, lst_d[0].OwnerId);
		system.assertEquals(grp_pro.Id, lst_d[1].OwnerId);     
		system.assertEquals(grp_pet.Id, lst_d[2].OwnerId);     
		system.assertEquals(grp_pet.Id, lst_d[3].OwnerId);

		lst_d[0].BI_Estado_de_la_demanda__c = 'Aprobado';
		lst_d[0].BI_Tipologia__c = 'Petición Ambientes';
		lst_d[0].BI_Horas_Estimadas__c = 3;

		update lst_d;

		lst_cambio_estado = [SELECT Id, BI_Estado_actual__c, BI_Estado_anterior__c, BI_Fecha_modificacion__c FROM BI_Asignacion_demanda__c WHERE BI_Demanda__c IN: set_id]; 
		lst_d = [SELECT Id, BI_Tipologia__c,BI_Tipo_de_demanda__c , BI_Estado_de_la_demanda__c, OwnerId FROM BI_Demanda__c WHERE Id IN: set_id];  


		system.assertEquals('Inmediata', lst_d[0].BI_Tipo_de_demanda__c);
		system.assertEquals('Aprobado', lst_cambio_estado[lst_cambio_estado.size()-1].BI_Estado_actual__c);
		system.assertEquals('1- Por revisar', lst_cambio_estado[lst_cambio_estado.size()-1].BI_Estado_anterior__c); //JEG
		system.assertEquals(lst_cambio_estado[lst_cambio_estado.size()-1].BI_Fecha_modificacion__c, Date.today());

		lst_d[1].OwnerId = thisUser.Id;
		lst_d[1].BI_Horas_Estimadas__c = 24;
		
		update lst_d;

		
		lst_cambio_estado = [SELECT Id, BI_Estado_actual__c, BI_Estado_anterior__c, BI_Fecha_modificacion__c FROM BI_Asignacion_demanda__c WHERE BI_Demanda__c IN: set_id]; 

		system.assertEquals(lst_cambio_estado.size(), 9);
		system.assertEquals(thisUser.Id, lst_d[1].OwnerId);  
		system.assertEquals('Minor', lst_d[1].BI_Tipo_de_demanda__c);

		lst_d[1].BI_Tipologia__c = 'Demanda';
		lst_d[1].BI_Horas_Estimadas__c = 25;

		update lst_d;

		lst_d = [SELECT Id, BI_Tipologia__c, BI_Estado_de_la_demanda__c,BI_Tipo_de_demanda__c, OwnerId FROM BI_Demanda__c WHERE Id IN: set_id];  
		
		system.assertEquals(grp_dem.Id, lst_d[1].OwnerId);
		system.assertEquals('Major', lst_d[1].BI_Tipo_de_demanda__c);

		delete lst_d;
	}



	@isTest static void test_exceptions(){
		dataLoad();
		BI_TestUtils.throw_exception = true;
		List<BI_Demanda__c> lst_n = new List<BI_Demanda__c>();
		List<BI_Demanda__c> lst_o = new List<BI_Demanda__c>();

		BI_DemandaMethods.calculateTimeBetweenStages(lst_n, lst_o);


		
		List<BI_Demanda__c> lst_d = [SELECT Id, BI_Tipologia__c, BI_Estado_de_la_demanda__c, OwnerId FROM BI_Demanda__c WHERE Id IN: set_id];
		
		update lst_d;
		BI_TestUtils.throw_exception = false;
		lst_d[0].BI_Fecha_ultima_modificacion_etapa__c = null;
		update lst_d[0];


	}
}