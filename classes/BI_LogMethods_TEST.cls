@isTest
private class BI_LogMethods_TEST {
    
    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Setup method for the test class
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        09/03/2016                      Guillermo Muñoz             Initial version
        02/01/2017                      Marta GLez                  REING-01-Adaptacion Reingenieria de contactos
        20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c   
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testSetup static void setup(){

        Account acc = new Account(
            Name = 'TestAcc1',
            BI_Country__c = Label.BI_Colombia,
            TGS_Region__c = 'América',
            BI_Tipo_de_identificador_fiscal__c  = 'NIT',
            CurrencyIsoCode = 'GTQ',
            BI_Segment__c = 'Mayoristas',
            BI_Subsegment_Regional__c = 'Mayoristas',
            BI_Subsector__c = 'Banca',
            BI_Sector__c = 'Industria',
            BI_Territory__c = 'test'
        );
        insert acc;
        
        Contact cnt = new Contact(
            LastName = 'TestCnt1',
            BI_Country__c = Label.BI_Colombia,
            CurrencyIsoCode  = 'COP',
            AccountId = acc.Id,
            BI_Tipo_de_contacto__c = 'Administrador Canal Online',
            //REING-01_INI
            FS_CORE_Acceso_a_Portal_Platino__c = true,
            BI_Tipo_de_documento__c = 'Otros',
            BI_Numero_de_documento__c = '00000000X'
            //REING-01_FIN
        );
        insert cnt; 
                
        BI_Col_Ciudades__c ciudad = new BI_Col_Ciudades__c (
            Name = 'Test City',
            BI_COL_Pais__c = 'Test Country',
            BI_COL_Codigo_DANE__c = 'TestCDa'
        );
        insert ciudad;

        BI_Sede__c sede = new BI_Sede__c(
            BI_COL_Ciudad_Departamento__c = ciudad.Id,
            BI_Direccion__c = 'Test Street 123 Number 321',
            BI_Localidad__c = 'Test Local',
            BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero,
            BI_COL_Sucursal_en_uso__c = 'Libre',
            BI_Country__c = Label.BI_Colombia,
            Name = 'Test Street 123 Number 321, Test Local Colombia',
            BI_Codigo_postal__c = '12356'
        );
        insert sede;

        BI_Punto_de_instalacion__c pInst = new BI_Punto_de_instalacion__c (
            BI_Cliente__c = acc.Id,
            BI_Sede__c = sede.Id,
            BI_Contacto__c = cnt.Id,
            Name = 'QA Erroro'
        );
        insert pInst;

        Opportunity opp = new Opportunity(
            Name = 'TestOpp1',
            AccountId = acc.Id,
            BI_Country__c = 'Colombia',
            CloseDate = System.today().addDays(3),
            StageName = 'F6 - Prospecting',
            BI_Ciclo_ventas__c = Label.BI_Completo
        );
        insert opp;

        RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'BI_COL_Anexos__c' AND DeveloperName = 'BI_FUN' LIMIT 1];

        BI_COL_Anexos__c anexo = new BI_COL_Anexos__c(
            Name = 'FUN-0041414',
            RecordTypeId = rt.Id
        );
        insert anexo;

        BI_COL_Descripcion_de_servicio__c descSer  = new BI_COL_Descripcion_de_servicio__c(
            BI_COL_Oportunidad__c = opp.Id,
            CurrencyIsoCode = 'COP'
        );
        insert descSer;

        BI_COL_Modificacion_de_Servicio__c ms = new BI_COL_Modificacion_de_Servicio__c(
            BI_COL_FUN__c = anexo.Id,
            BI_COL_Codigo_unico_servicio__c = descSer.Id,
            BI_COL_Clasificacion_Servicio__c = 'ALTA',
            CurrencyIsoCode = 'COP',
            BI_COL_Fecha_instalacion_servicio_RFS__c = Date.today().addDays(30),
            BI_COL_Oportunidad__c = opp.Id,
            BI_COL_Bloqueado__c = false,    
            BI_COL_Sucursal_de_Facturacion__c = pInst.Id,
            BI_COL_Sucursal_Origen__c = pInst.Id
        );
        insert ms;

        BI_COL_Viabilidad_Tecnica__c vt = new BI_COL_Viabilidad_Tecnica__c(
            BI_COL_Modificacion_de_Servicio__c = ms.Id,
            BI_COL_Descripcion_de_servicio_DSR__c = descSer.Id
        );
        insert vt;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the code coverage of BI_LogMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        09/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void checkLogsToDeleteMayoristas(){

        List <BI_Log__c> lst_logs = new List <BI_Log__c>();

        /***************Se borran los posibles logs generados en el setUp para realizar una prueba fiable***************/
        delete [SELECT Id FROM BI_Log__c WHERE BI_Delete__c = true];
        /***************************************************************************************************************/

        //Log de MS
        BI_Log__c logMs = new BI_Log__c(
            BI_COL_Modificacion_Servicio__c = [SELECT Id FROM BI_COL_Modificacion_de_Servicio__c LIMIT 1].Id,
            BI_COL_Estado__c = Label.BI_COL_lblFallido
        );
        lst_logs.add(logMs);

        //Log de Oportunidades
        BI_Log__c logOpp = new BI_Log__c(
            BI_COL_Oportunidad__c = [SELECT Id FROM Opportunity LIMIT 1].Id,
            BI_COL_Estado__c = Label.BI_COL_lblFallido
        );
        lst_logs.add(logOpp);

        //Log de Contactos
        BI_Log__c logCnt = new BI_Log__c(
            BI_COL_Contacto__c = [SELECT Id FROM Contact LIMIT 1].Id,
            BI_COL_Estado__c = Label.BI_COL_lblFallido
        );
        lst_logs.add(logCnt);

        //Log de Punto de Instalacion
        BI_Log__c logSede = new BI_Log__c(
            BI_COL_Sede__c = [SELECT Id FROM BI_Punto_de_instalacion__c LIMIT 1].Id,
            BI_COL_Estado__c = Label.BI_COL_lblFallido
        );
        lst_logs.add(logSede);

        //Log de viabilidad tecnica
        BI_Log__c logVia = new BI_Log__c(
            BI_COL_Solicitud_viabilidad__c = [SELECT Id FROM BI_COL_Viabilidad_Tecnica__c LIMIT 1].Id,
            BI_COL_Estado__c = Label.BI_COL_lblFallido
        );
        lst_logs.add(logVia);

        insert lst_logs;

        List <BI_Log__c> lst_assertLogs = [SELECT BI_Delete__c FROM BI_Log__c WHERE BI_Delete__c = true];
        System.assertEquals(lst_assertLogs.size(),5);
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the code coverage of BI_LogMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        09/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void checkLogsToDeleteNotMayoristas(){

        List <BI_Log__c> lst_logs1 = new List <BI_Log__c>();
        List <BI_Log__c> lst_logs2 = new List <BI_Log__c>();
        List <BI_Log__c> lst_logs3 = new List <BI_Log__c>();

        Account cuenta = [SELECT Id,BI_Segment__c FROM Account LIMIT 1];
        cuenta.BI_Segment__c = 'Negocios';
        update cuenta; 
        /***************Se borran los posibles logs generados en el setUp para realizar una prueba fiable***************/
        delete [SELECT Id FROM BI_Log__c WHERE BI_Delete__c = true];
        /***************************************************************************************************************/
        Id idCont = [SELECT Id FROM Contact LIMIT 1].Id;
        Id idPuntIns = [SELECT Id FROM BI_Punto_de_instalacion__c LIMIT 1].Id;

        ///////////////////////////////////////////////Primera Inserción///////////////////////////////////////////////
        //Log de Contactos
        BI_Log__c logCnt1 = new BI_Log__c(
            BI_COL_Contacto__c = idCont,
            BI_COL_Estado__c = Label.BI_Exitoso,
            BI_descripcion__c = 'con1'
        );
        lst_logs1.add(logCnt1);

        //Log de Punto de Instalacion
        BI_Log__c logSede1 = new BI_Log__c(
            BI_COL_Sede__c = idPuntIns,
            BI_COL_Estado__c = Label.BI_Exitoso,
            BI_descripcion__c = 'sed1'
        );
        lst_logs1.add(logSede1);

        insert lst_logs1;

        ///////////////////////////////////////////////Segunda Inserción///////////////////////////////////////////////
        //Log de Contactos
        BI_Log__c logCnt2 = new BI_Log__c(
            BI_COL_Contacto__c = idCont,
            BI_COL_Estado__c = Label.BI_Exitoso,
            BI_descripcion__c = 'con2'
        );
        lst_logs2.add(logCnt2);

        //Log de Punto de Instalacion
        BI_Log__c logSede2 = new BI_Log__c(
            BI_COL_Sede__c = idPuntIns,
            BI_COL_Estado__c = Label.BI_Exitoso,
            BI_descripcion__c = 'sed2'
        );
        lst_logs2.add(logSede2);
        //añadido while vacío para que las fechas de creación no sean iguales
        Decimal now = System.currentTimeMillis() + 1000;
        while (now > System.currentTimeMillis()){

        }
        insert lst_logs2;

        ///////////////////////////////////////////////Tercera Inserción///////////////////////////////////////////////
        //Log de Contactos
        BI_Log__c logCnt3 = new BI_Log__c(
            BI_COL_Contacto__c = idCont,
            BI_COL_Estado__c = Label.BI_COL_lblFallido,
            BI_descripcion__c = 'con3'
        );
        lst_logs3.add(logCnt3);

        //Log de Punto de Instalacion
        BI_Log__c logSede3 = new BI_Log__c(
            BI_COL_Sede__c = idPuntIns,
            BI_COL_Estado__c = Label.BI_COL_lblFallido,
            BI_descripcion__c = 'sed3'
        );
        lst_logs3.add(logSede3);
        //añadido while vacío para que las fechas de creación no sean iguales
        now = System.currentTimeMillis() + 1000;
        while (now > System.currentTimeMillis()){
            
        }
        insert lst_logs3;

        System.debug('*******' + [SELECT BI_descripcion__c,BI_Delete__c FROM BI_Log__c]);
        List <Id> lst_idLogs;
        //Tendrían que marcarse los logs de la primera inserción
        lst_idLogs = new List <Id>{logCnt1.Id, logSede1.Id};
        List <BI_Log__c> lst_assertLogsPrimera = [SELECT BI_Delete__c FROM BI_Log__c WHERE BI_Delete__c = true AND Id IN : lst_idLogs];
        System.assertEquals(lst_assertLogsPrimera.size(),2);

        //No tendrían que marcarse los logs de la segunda inserción
        lst_idLogs = new List <Id>{logCnt2.Id, logSede2.Id};
        List <BI_Log__c> lst_assertLogsSegunda = [SELECT BI_Delete__c FROM BI_Log__c WHERE BI_Delete__c = false AND Id IN : lst_idLogs];
        System.assertEquals(lst_assertLogsSegunda.size(),2);

        //Tendrían que marcarse los logs de la tercera inserción
        lst_idLogs = new List <Id>{logCnt3.Id, logSede3.Id};
        List <BI_Log__c> lst_assertLogsTercera = [SELECT BI_Delete__c FROM BI_Log__c WHERE BI_Delete__c = true AND Id IN : lst_idLogs];
        System.assertEquals(lst_assertLogsTercera.size(),2);

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the exceptions thrown in BI_LogMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        09/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void exceptions(){

        BI_TestUtils.throw_exception = true;

        BI_Log__c log = new BI_Log__c(
            BI_Asunto__c = 'Test'
        );
        insert log;

        BI_TestUtils.throw_exception = false;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the code coverage of BI_DeleteLogs_JOB
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        09/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void BI_DeleteLogs_JOB_Batch_TEST(){

        List <BI_Log__c> lst_logs = new List <BI_Log__c>();

        /***************Se borran los posibles logs generados en el setUp para realizar una prueba fiable***************/
        delete [SELECT Id FROM BI_Log__c];
        /***************************************************************************************************************/

        BI_Log__c log1 = new BI_Log__c(
            BI_Asunto__c = 'Test'
        );
        lst_logs.add(log1);

        BI_Log__c log2 = new BI_Log__c(
            BI_Delete__c = true
        );
        lst_logs.add(log2);

        BI_Log__c log3 = new BI_Log__c(
            BI_Delete__c = false
        );
        lst_logs.add(log3);

        insert lst_logs;

        

        Test.startTest();
        
        BI_DeleteLogs_JOB obj = new BI_DeleteLogs_JOB();
        Database.executeBatch(obj);
        Test.stopTest();

        System.assertEquals([SELECT Id FROM BI_Log__c].size(),1);

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the code coverage of BI_DeleteLogs_JOB
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        09/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void BI_DeleteLogs_JOB_Schedule_TEST(){

        String jobId;

        Test.startTest();

        BI_DeleteLogs_JOB obj = new BI_DeleteLogs_JOB();

        Datetime ahora = Datetime.now().addSeconds(3);
        Integer hour = ahora.hour();
        Integer minute = ahora.minute();
        Integer second = ahora.second();
        Integer day = ahora.day();
        Integer month = ahora.month();
        Integer year = ahora.year();
        String cronexpression = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year;

        jobId = System.schedule('Test borrado logs', cronexpression, obj);

        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id =: jobId LIMIT 1];

        System.assertNotEquals(ct, null);

        Test.stopTest();
    }
    
}