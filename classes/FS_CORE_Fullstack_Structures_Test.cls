/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CORE_Fullstack_Structures

History:
<Date>							<Author>						<Change Description>
3/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CORE_Fullstack_Structures_Test {
    
    @testSetup
    private static void init() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        
        /* Integrations */
        FS_CORE_Integracion__c ORGConfiguration = new FS_CORE_Integracion__c();
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1 Top 2 Top 3 Top 4 Platino';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';        
        insert ORGConfiguration;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, FS_CORE_ID_Cliente_legado__c = '000000000X',
                                      FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        insert account;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        
        /* Contact */
        Contact contact = new Contact(FirstName = 'Integración', LastName = 'Perú', BI_Activo__c = true,
                                      BI_Tipo_de_contacto__c = 'Administrativo', BI_Representante_legal__c = true, Phone = '000000000',
                                      AssistantPhone = '000000000', HomePhone = '000000000', MobilePhone = '000000000',
                                      OtherPhone = '000000000', Fax = '000000000', email = 'integraciones@integraciones.test',
                                      BI_Country__c = 'Peru', BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '0000000X',
                                      Birthdate = Date.today(), BI_Genero__c = 'Masculino', accountId = account.Id,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Active_Contact' LIMIT 1][0].Id, BI_Id_del_contacto__c = '00000000X', FS_CORE_Estado_de_Sincronizacion__c = 'Sincronizado');
        
        insert contact;
    }
    
     @isTest private static void accountToApex() {
         /* Test */
         System.Test.startTest();
         
         FS_CORE_Fullstack_Structures.accountToApex([SELECT Id, Name, Description, BI_Activo__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Riesgo__c,
                                                     BI_Denominacion_comercial__c, BI_Sector__c, TGS_Es_MNC__c, BI_Ambito__c, BI_Subsector__c, Website, TGS_Fecha_Activacion__c,
                                                     TGS_Deactivation_Date__c, FS_CHI_Tipo_Credito__c, FS_CHI_CREDIT_CLASS__c, FS_CHI_Credit_Score__c, FS_CHI_Limite_de_consumo__c, FS_CHI_Limite_de_credito__c, FS_CHI_Limite_credito_portabilidad__c,
                                                     FS_CHI_Fecha_de_ultima_ev__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Phone, Fax, FS_CORE_Account_Massive__c,
                                                     FS_CORE_ATM_Massive__c, FS_CORE_ISO_2_Country__c, RecordTypeId, FS_CORE_ID_Cliente_legado__c, FS_CORE_Estado_Sincronizacion__c, ParentId FROM Account LIMIT 1][0]);
         
         System.Test.stopTest();
    }
    
    @isTest private static void ContactDetailsTypeToApex() {
        /* Test */
        System.Test.startTest();
        
        FS_CORE_Fullstack_Structures.ContactDetailsTypeToApex([SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                           AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                           BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                           BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0]);
        
        System.Test.stopTest();
    }
    
    @isTest private static void AgentRelationTypeToApex() {
        /* Test */
        System.Test.startTest();
        
        FS_CORE_Fullstack_Structures.AgentRelationTypeToApex(UserInfo.getUserId(), 'Account Manager');
        
        System.Test.stopTest();
    }
    
    @isTest private static void isValidAccountRecord() {
        /* Test */
        System.Test.startTest();
        
        Account account = [SELECT Id, Name, Description, BI_Activo__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Riesgo__c,
                           BI_Denominacion_comercial__c, BI_Sector__c, TGS_Es_MNC__c, BI_Ambito__c, BI_Subsector__c, Website, TGS_Fecha_Activacion__c,
                           TGS_Deactivation_Date__c, FS_CHI_Tipo_Credito__c, FS_CHI_CREDIT_CLASS__c, FS_CHI_Credit_Score__c, FS_CHI_Limite_de_consumo__c, FS_CHI_Limite_de_credito__c, FS_CHI_Limite_credito_portabilidad__c,
                           FS_CHI_Fecha_de_ultima_ev__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Phone, Fax, FS_CORE_Account_Massive__c,
                           FS_CORE_ATM_Massive__c, FS_CORE_ISO_2_Country__c, RecordTypeId, FS_CORE_ID_Cliente_legado__c, FS_CORE_Estado_Sincronizacion__c, ParentId FROM Account LIMIT 1][0];
        
        /* Estado: Sincronizado */
        FS_CORE_Fullstack_Structures.isValidAccountRecord(account, true);
        
        /* Estado: No aplica */
        account.BI_Subsegment_Local__c = 'Masivo';
        update account;
        
        FS_CORE_Fullstack_Structures.isValidAccountRecord(account, true);
        
        /* Estado: No Sincronizado */
        delete [SELECT Id FROM Opportunity];
        
        FS_CORE_Fullstack_Structures.isValidAccountRecord(account, true);
        
        System.Test.stopTest();
    }
    
    @isTest private static void isValidAccount() {
         /* Test */
         System.Test.startTest();
         
         FS_CORE_Fullstack_Structures.isValidAccount([SELECT Id, Name, Description, BI_Activo__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Riesgo__c,
                                                     BI_Denominacion_comercial__c, BI_Sector__c, TGS_Es_MNC__c, BI_Ambito__c, BI_Subsector__c, Website, TGS_Fecha_Activacion__c,
                                                     TGS_Deactivation_Date__c, FS_CHI_Tipo_Credito__c, FS_CHI_CREDIT_CLASS__c, FS_CHI_Credit_Score__c, FS_CHI_Limite_de_consumo__c, FS_CHI_Limite_de_credito__c, FS_CHI_Limite_credito_portabilidad__c,
                                                     FS_CHI_Fecha_de_ultima_ev__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Phone, Fax, FS_CORE_Account_Massive__c,
                                                     FS_CORE_ATM_Massive__c, FS_CORE_ISO_2_Country__c, RecordTypeId, FS_CORE_ID_Cliente_legado__c, FS_CORE_Estado_Sincronizacion__c, ParentId FROM Account LIMIT 1][0]);
         
         System.Test.stopTest();
    }
    
    @isTest private static void isValidContactRecord() {
        /* Test */
        System.Test.startTest();
        
        Account account = [SELECT Id, Name, Description, BI_Activo__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Riesgo__c,
                           BI_Denominacion_comercial__c, BI_Sector__c, TGS_Es_MNC__c, BI_Ambito__c, BI_Subsector__c, Website, TGS_Fecha_Activacion__c,
                           TGS_Deactivation_Date__c, FS_CHI_Tipo_Credito__c, FS_CHI_CREDIT_CLASS__c, FS_CHI_Credit_Score__c, FS_CHI_Limite_de_consumo__c, FS_CHI_Limite_de_credito__c, FS_CHI_Limite_credito_portabilidad__c,
                           FS_CHI_Fecha_de_ultima_ev__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Phone, Fax, FS_CORE_Account_Massive__c,
                           FS_CORE_ATM_Massive__c, FS_CORE_ISO_2_Country__c, RecordTypeId, FS_CORE_ID_Cliente_legado__c, FS_CORE_Estado_Sincronizacion__c, ParentId FROM Account LIMIT 1][0];
        
        Contact contact = [SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                           AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                           BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                           BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0];
        
        /* Estado: Sincronizado */
        FS_CORE_Fullstack_Structures.isValidContactRecord(contact, true);
        
        /* Estado: No aplica */
        contact.BI_Tipo_de_contacto__c = '';
        contact.BI_Representante_legal__c = false;
        update contact;
        
        FS_CORE_Fullstack_Structures.isValidContactRecord(contact, true);
        
        /* Estado: No Sincronizado */
        delete [SELECT Id FROM Opportunity];
        account.FS_CORE_Estado_Sincronizacion__c = 'No sincronizado';
        update account;
        
        FS_CORE_Fullstack_Structures.isValidContactRecord(contact, true);
        
        System.Test.stopTest();
    }
    
    @isTest private static void isValidContact() {
         /* Test */
         System.Test.startTest();
         
         FS_CORE_Fullstack_Structures.isValidContact([SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                           AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                           BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                           BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0]);
         
         System.Test.stopTest();
    }
    
    @isTest private static void isAccountChanged() {
         /* Test */
         System.Test.startTest();
         
        Account account = [SELECT Id, Name, Description, BI_Activo__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Riesgo__c,
                           BI_Denominacion_comercial__c, BI_Sector__c, TGS_Es_MNC__c, BI_Ambito__c, BI_Subsector__c, Website, TGS_Fecha_Activacion__c,
                           TGS_Deactivation_Date__c, FS_CHI_Tipo_Credito__c, FS_CHI_CREDIT_CLASS__c, FS_CHI_Credit_Score__c, FS_CHI_Limite_de_consumo__c, FS_CHI_Limite_de_credito__c, FS_CHI_Limite_credito_portabilidad__c,
                           FS_CHI_Fecha_de_ultima_ev__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Phone, Fax, FS_CORE_Account_Massive__c,
                           FS_CORE_ATM_Massive__c, FS_CORE_ISO_2_Country__c, RecordTypeId, FS_CORE_ID_Cliente_legado__c, FS_CORE_Estado_Sincronizacion__c, ParentId FROM Account LIMIT 1][0];
        
         FS_CORE_Fullstack_Structures.isAccountChanged(account, account);
         
         System.Test.stopTest();
    }
    
    @isTest private static void isContactChanged() {
         /* Test */
         System.Test.startTest();
         
        Contact contact = [SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                           AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                           BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                           BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0];
        
         FS_CORE_Fullstack_Structures.isContactChanged(contact, contact);
         
         System.Test.stopTest();
    }
}