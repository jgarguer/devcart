@isTest
private class PCA_CatalogoController_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_CatalogoController class 
    
    History: 
    <Date> 					<Author> 				<Change Description>
    13/08/2014      		Ana Escrich	    		Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ana Escrich
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for PCA_CatalogoController.loadInfo
		    
 	History: 
 	
 	 <Date> 					<Author> 				<Change Description>
    12/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void getloadInfo() {
 		User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());    	
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
	    system.runAs(usr){
 		List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
        
        List<Account> accounts = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
        //List<Account> accounts2 = BI_DataLoad.loadAccounts(1, lst_pais);
       	List<Account> accList = new List<Account>();
       	//List<Account> accList2 = new List<Account>();
        for(Account item: accounts){
            	item.OwnerId = usr.Id;
            	accList.add(item);
        }
        /*for(Account item: accounts2){
            	item.OwnerId = usr.Id;
            	accList2.add(item);
        }*/
        update accList;
       // update accList2;
        List<Contact> con = BI_DataLoad.loadPortalContacts(1, accounts);
       // List<Contact> con2 = BI_DataLoad.loadPortalContacts(1, accounts2);
		 User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());	
         //User user2 = BI_DataLoad.loadSeveralPortalUsers(con2[0].Id, BI_DataLoad.searchPortalProfile());  	
		PCA_CatalogoController constructor = new PCA_CatalogoController();
		system.runAs(user1){
		     BI_TestUtils.throw_exception = false;
			constructor.checkPermissions();			
			constructor.loadInfo();			
		}
		PCA_CatalogoController constructor2 = new PCA_CatalogoController();
		system.runAs(user1){
			constructor2.checkPermissions();
			BI_TestUtils.throw_exception = true;
			constructor2.loadInfo();			
		}
		PCA_CatalogoController constructor3 = new PCA_CatalogoController();
		system.runAs(user1){
			constructor3.checkPermissions();
			constructor3.loadInfo();		
			BI_TestUtils.throw_exception = true;
			constructor3.getCurrencyISOCode(accList[0].Id);	
		}
		PCA_CatalogoController constructor4 = new PCA_CatalogoController();
		system.runAs(user1){ 
			constructor4.checkPermissions();
			constructor4.loadInfo();		
			BI_TestUtils.throw_exception = true;
			constructor4.getAccountCountry(accList[0].Id);	
		}
			BI_TestUtils.throw_exception = false;
			constructor.checkPermissions();
			constructor.loadInfo();	
	    }
 	}
 	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}