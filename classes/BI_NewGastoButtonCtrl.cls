public with sharing class BI_NewGastoButtonCtrl {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Controller for BI_NewGastoButton
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Micah Burgos		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public String   regionId			{get; set;}
	public String   regionName			{get; set;}
	
	public String   currencyType		{get; set;}
	
	public String   camp_Name           {get;set;}
	public String   camp_Id           {get;set;}
		
	public String   keyPrefix			{get; set;}
	public map<string,string> dynamicIds {get; set;}
	
	private set<String>  setFieldToId;	
	private string sObjectTypeRecord;
	
	
		
	public BI_NewGastoButtonCtrl(ApexPages.StandardController std){
	
		sObjectTypeRecord = std.getRecord().getSObjectType().getDescribe().getName();
		
		keyPrefix = Schema.getGlobalDescribe().get(sObjectTypeRecord).getDescribe().getKeyPrefix();
		
		
		System.debug('--> sObjectTypeRecord: ' + sObjectTypeRecord);
		System.debug('--> keyPrefix: ' + keyPrefix);
		
		
		setFieldToId = new set<String>();
		
		if(sObjectTypeRecord == 'BI_Gastos__c'){
			setFieldToId.add('BI_Country__c');
			setFieldToId.add('BI_Campana__c');
		}
		
	}

	public void actionFunction(){
		try{
			dynamicIds = BI_DynamicFieldId.getDynamicField(sObjectTypeRecord, setFieldToId);
			
			
			regionId = '';
			regionName = '';
			currencyType = '';
			
			
			
			camp_Name = '';
			if(ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Campana__c')) != null){
				camp_Name = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Campana__c'));
			}
			
			camp_Id = '';
			if(ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Campana__c') + '_lkid') != null){
				camp_id = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Campana__c') + '_lkid');
			
				
				list<Campaign> camp = [SELECT Id, CurrencyIsoCode, BI_Country__c FROM Campaign WHERE Id = :camp_id ];
				
				if(!camp.isEmpty()){
					//regionId = camp[0].BI_Country__c;
					regionName = camp[0].BI_Country__c;
					currencyType = camp[0].CurrencyIsoCode;
				}
				 
			}
		}catch (exception exc){
		   BI_LogHelper.generate_BILog('BI_NewGastoButtonCtrl.actionFunction', 'BI_EN', exc , 'Controller VF');
		}	
	}	
}