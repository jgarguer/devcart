/*---------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Eduardo Ventura
Company:		Everis España
Description:	RFG-INT-CHI-01: Validación del identificador fiscal.

History:

<Date>                      <Author>                        <Change Description>
24/04/2016                  Eduardo Ventura					Versión Inicial
---------------------------------------------------------------------------------------------------------------------------------------------------------------*/

global class FS_CHI_Contact_Management_Mock implements HttpCalloutMock{
    /* DECLARACIÓN DE VARIABLES */
    /* Vacío */
    
    global static HttpResponse respond (HttpRequest request){
        /* Declaración de variables */
        HttpResponse response = new HttpResponse();
        
        /* Mock */
        response.setStatusCode(200);
        //response.setBody('{"contacts":[{"contactDetails":{"name":{"givenName":"Agustín","familyName":"González"},"addresses":[{"addressName":"Av. Providencia","addressNumber":{"value": "111"},"locality":"Santiago","region":"Metropolitana"}]}}],"totalResults":1}');
        String req = request.getEndpoint().substring(65,76);
        System.debug('@@@@ '+req);
        if(req.contains('120047787')){
        response.setBody('{"contactList":{"contacts":[{"contactDetails":{"name":{"givenName":"Agustín","familyName":"González"},"addresses":[{"addressName":"Av. Providencia","addressNumber":{"value":"111"},"locality":"Santiago","region":"Metropolitana"}]}}]},"totalResults":9}');
        }else if(req.contains('91600307')){
            response.setBody('{"contactList":{"contacts":[{"contactDetails":{"name":{"givenName":"Agustín","familyName":"González"},"addresses":[{"addressName":"Av. Providencia","addressNumber":{"value":"111"},"locality":"Santiago","region":"Metropolitana"}]}}]},"totalResults":8}');
        }else{
            response.setBody('{"contactList":{"contacts":[{"contactDetails":{"name":{"givenName":"Agustín","familyName":"González"},"addresses":[{"addressName":"Av. Providencia","addressNumber":{"value":"111"},"locality":"Santiago","region":"Metropolitana"}]}}]},"totalResults":1}');
        }
            return response;
    } 
}