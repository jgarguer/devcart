global with sharing class  PCA_LoginController  {    //extends ForgotPasswordController
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Controller of login page 
    
    History:
    
    <Date>                    <Author>               <Description>
    24/04/2014              Jorge Longarela         Initial Version
    08/07/2014               Micah Burgos           v2
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global String username {get; set;}
    global String password {get; set;}
    global Boolean confirmPanel {get; set;}
    global String msg {get;set;}
    global String ErrorMsg {get;set;}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Controller of login page 
    
    History:
    
    <Date>                    <Author>               <Description>
    24/04/2014              Jorge Longarela         Initial Version
    08/07/2014               Micah Burgos           v2 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    global PageReference login() {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            if(checkUser() && checkPass()){
                String startUrl = (System.currentPageReference().getParameters().get('startURL')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('startURL')):null;
                startUrl = (System.currentPageReference().getParameters().get('retURL')!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get('retURL')):null;
                system.debug('startUrl: ' + startUrl);
                system.debug('usuario: ' +username);
                system.debug('password: ' +password);
                //system.debug('System.currentPageReference().getParameters()' + System.currentPageReference().getParameters());
                system.debug('getBaseUrl' +Site.getBaseUrl());
                PageReference result = Site.login(username, password, startUrl);
                system.debug('result' +result);
                if(result == null) {
                    //result = new PageReference(Site.getCurrentSiteUrl()+'PCA_Login'); 
                    ErrorMsg=Label.BI_UsrPassIncorrecto;
                }
                return result;
            }              
            return null;
         
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_LoginController.login', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   forgot Password 
    
    History:
    
    <Date>                    <Author>               <Description>
    08/07/2014               Micah Burgos           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public void forgotPassword() {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            System.debug('PCA_LoginController->forgotPassword()->checkUser(): '+ checkUser() );
            if(checkUser()){
                list<User>uList = new list<user>();
                
                uList = [Select Id, ProfileId From User WHERE username = :username AND (Profile.Name = 'Customer Community User Clone' OR Profile.Name = 'Customer Community Plus Custom' OR Profile.Name = 'BI_Customer Communities' OR Profile.Name = 'BI_Customer_Community_Plus')];
                boolean success = false;
                
                if(!uList.isEmpty()){
                     Site.forgotPassword(username); 
                     success = true;
                     ErrorMsg = '';
                     msg = Label.PCA_ResetPasswdCustom;
                     
                }
                System.debug('PCA_LoginController->forgotPassword()->success: '+ success );
                if (!success) {             
                    ErrorMsg = Label.PCA_ResetPassFail;
                }
                
            }else{
                ErrorMsg=Label.BI_IntroduceEmail;
                
            }
            System.debug('PCA_LoginController->forgotPassword()->ErrorMsg: '+ ErrorMsg );
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_LoginController.forgotPassword', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*To test class:
    @IsTest(SeeAllData=true) public static void testForgotPasswordController() {
        // Instantiate a new controller with all parameters in the page
        ForgotPasswordController controller = new ForgotPasswordController();
        controller.username = 'test@salesforce.com';        
    
        System.assertEquals(controller.forgotPassword(),null); 
    }
    */ 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Controller 
    
    History:
    
    <Date>                    <Author>               <Description>
    08/07/2014               Jorge Longarela         Initial Version    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
    global PCA_LoginController() {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            confirmPanel = false;
            ErrorMsg = '';
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_LoginController.PCA_LoginController', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   checkUser 
    
    History:
    
    <Date>                    <Author>               <Description>
    08/07/2014               Jorge Longarela         Initial Version    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    global boolean checkUser(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            if(username != null && username != ''){
                return true;
            }else{
                ErrorMsg=Label.BI_LabelErrorUsuario;
                return false;
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_LoginController.checkUser', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   checkPass 
    
    History:
    
    <Date>                    <Author>               <Description>
    08/07/2014               Jorge Longarela         Initial Version    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/       
    global boolean checkPass(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            if(password != null && password != ''){
                return true;
            }else{
                ErrorMsg=Label.BI_LabelErrorUsuario;
                return false;
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_LoginController.checkPass', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
}