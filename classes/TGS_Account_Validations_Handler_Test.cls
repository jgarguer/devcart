/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        ---
 Company:       Deloitte
 Description:   Class that covers coverage of TGS_Account_Validations_Handler and TGS_Account_Hierarchy
    
 History

 <Date>            <Author>                      <Description>
 ?                 ?                             Initial version
 22/03/2016        Jose Miguel Fierro            Added methods to cover new code in TGS_Account_Hierarchy
 08/11/2017        Álvaro López                  Added preventAccountToBeDeleted_Test method
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class TGS_Account_Validations_Handler_Test {

    static testMethod void checkActiveAccount() {
        String profile = 'TGS Integration User';
        
        Profile miProfile = [SELECT Id, Name FROM Profile WHERE Name = :profile LIMIT 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            insert new TGS_User_Org__c(TGS_Is_TGS__c = true, SetupOwnerId = miProfile.Id);
            //[Micah Burgos]Comentado por error en subida de PL. Al tener el (SeeAllData=true) y el valor en el sistema salta duplicado
        }
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        u.BI_Permisos__c = 'TGS';
        insert u;
       
        System.runAs(u) {
            Account accountLE = TGS_Dummy_Test_Data.dummyHierarchy();
            
            /*accountLE.TGS_Aux_Customer_Country__c;
            accountLE.TGS_Aux_Holding__c;*/

            Account acBU = [SELECT Id, BI_Activo__c FROM Account WHERE ParentId = :accountLE.Id LIMIT 1];
            Account acCC = [SELECT Id, BI_Activo__c FROM Account WHERE ParentId = :acBU.Id LIMIT 1];
            
            acCC.BI_Activo__c = Label.BI_Si;
            acBU.BI_Activo__c = Label.BI_Si;
            accountLE.BI_Activo__c = Label.BI_Si;

            try {
                update acCC;
                update acBU;
                update accountLE;
            } catch(Exception e) {
                System.debug('Exception: '+e);
                System.assert(false);
            }
            
            Account myAccountLE = TGS_Dummy_Test_Data.dummyHierarchy();          
            
            List<Account> aCcList = [SELECT Id, ParentId FROM Account WHERE Id = :accountLE.ParentId];
                                    
            if(aCcList.size()>0){

                try {
                    myAccountLE.ParentId = aCcList.get(0).ParentId;
                    update myAccountLE;
                } catch(Exception e){ }
                
                try {
                    myAccountLE.BI_Activo__c = Label.BI_No;
                    update myAccountLE;
                } catch(Exception e){ }
            }
            
            TGS_Account_Validations_Handler.comprobarCI(acCC);
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Adds coverage for TGS_Account_Hierarchy.fillSitesInformation and tests its correct functionality

     History: 
    
     <Date>                     <Author>                    <Change Description>
     22/03/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void testFillSitesInformation() {
        insert new  TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
        
        Account le = TGS_Dummy_Test_Data.dummyHierarchy();
        Account ho = [Select Id, Name FROM Account WHERE Id = :le.Parent.ParentId];
        Account bu = [Select Id, Name, TGS_Aux_Holding__c, Parent.ParentId FROM Account WHERE ParentId = :le.Id];
        le.TGS_Aux_Holding__c = ho.Id;
        update le;
        
        BI_sede__c dir = new BI_Sede__c(BI_Direccion__c='Calle', BI_Localidad__c='Ciudad', BI_Provincia__c='Prov', BI_Codigo_postal__c='00000', BI_Country__c='Pais', Estado__c='Pais', Name='<null>');
        insert dir;
        
        //BI_bypass__c bp = new BI_bypass__c(BI_migration__c=true, BI_skip_assert__c=true, BI_skip_trigger__c=true, BI_stop_job__c=true);
        //insert bp;
        List<BI_Punto_de_instalacion__c> sites = new List<BI_Punto_de_instalacion__c>();
        for(Integer iter = 0; iter < 51; iter++) {
            //sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = le.Parent.ParentId, Name='TEST-SITE-HO-'+iter, BI_Sede__c = dir.Id));
            //sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = le.Id, Name='TEST-SITE-LE-'+iter, BI_Sede__c = dir.Id));
            //sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = bu.Parent.ParentId, Name='TEST-SITE-CuCo-'+iter, BI_Sede__c = dir.Id));
            sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = bu.Id, Name='TEST-SITE-BU-'+iter, BI_Sede__c = dir.Id));
        }
        insert sites;
        //delete bp;        
        
        sites = [SELECT Id, TGS_Main_Account_Name__c  FROM BI_Punto_de_Instalacion__c WHERE Id IN :sites];
        String sites0AccName = sites[0].TGS_Main_Account_Name__c;
        System.assertNotEquals(null, sites0AccName); // Comment out if bypass was active during site creation
        System.assert(sites0AccName.startsWith('Account Test Name'));
        System.debug('[TGS_Account_Validations_Handler_Test.testFillSitesInformation] sites[0].TGS_Main_Account_Name__c: ' + sites[0].TGS_Main_Account_Name__c);
        
        Test.startTest();
        ho.Name = 'TGTGS';
        update ho;
        Test.stopTest();
        
        sites = [SELECT Id, TGS_Main_Account_Name__c  FROM BI_Punto_de_Instalacion__c WHERE Id IN :sites];
        System.assertNotEquals(sites0AccName, sites[0].TGS_Main_Account_Name__c);
        System.debug('[TGS_Account_Validations_Handler_Test.testFillSitesInformation] sites[0].TGS_Main_Account_Name__c: ' + sites[0].TGS_Main_Account_Name__c);
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Adds coverage for TGS_Account_Hierarchy.updateAllHeirarchySites and tests its correct functionality

     History: 
    
     <Date>                     <Author>                    <Change Description>
     22/03/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void testUpdateAllHeirarchySites() {
        insert new  TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);
        
        Account le = TGS_Dummy_Test_Data.dummyHierarchy();
        Account bu = [Select Id, Name, TGS_Aux_Holding__c, Parent.ParentId FROM Account WHERE ParentId = :le.Id];

        BI_sede__c dir = new BI_Sede__c(BI_Direccion__c='Calle', BI_Localidad__c='Ciudad', BI_Provincia__c='Prov', BI_Codigo_postal__c='00000', BI_Country__c='Pais', Estado__c='Pais', Name='<null>');
        insert dir;
        
        BI_bypass__c bp = new BI_bypass__c(BI_migration__c=true, BI_skip_assert__c=true, BI_skip_trigger__c=true, BI_stop_job__c=true);
        insert bp;
        List<BI_Punto_de_instalacion__c> sites = new List<BI_Punto_de_instalacion__c>();
        for(Integer iter = 0; iter < 51; iter++) {
            sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = bu.Id, Name='TEST-SITE-BU-'+iter, BI_Sede__c = dir.Id));
            //sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = bu.Parent.ParentId, Name='TEST-SITE-CuCo-'+iter, BI_Sede__c = dir.Id));
            //sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = le.Id, Name='TEST-SITE-LE-'+iter, BI_Sede__c = dir.Id));
            //sites.add(new BI_Punto_de_instalacion__c(BI_Cliente__c = le.Parent.ParentId, Name='TEST-SITE-HO-'+iter, BI_Sede__c = dir.Id));
        }
        insert sites;
        delete bp;      
        
        Account ho = [Select Id, Name FROM Account WHERE Id = :le.Parent.ParentId];
        le.TGS_Aux_Holding__c = ho.Id;
        update le;
        
        sites = [SELECT Id, TGS_Main_Account_Name__c  FROM BI_Punto_de_Instalacion__c WHERE Id IN :sites];
        String sites0AccName = sites[0].TGS_Main_Account_Name__c;
        //System.assertNotEquals(null, sites0AccName);  // Comment out if bypass was active during site creation
        //System.assert(sites0AccName.startsWith('Account Test Name'));
        System.debug('[TGS_Account_Validations_Handler_Test.testFillSitesInformation] sites[0].TGS_Main_Account_Name__c: ' + sites[0].TGS_Main_Account_Name__c);

        Test.startTest();
        Id jobId = TGS_Account_Hierarchy.updateAllHeirarchySites();
        Test.stopTest();

        sites = [SELECT Id, TGS_Main_Account_Name__c  FROM BI_Punto_de_Instalacion__c WHERE Id IN :sites];
        System.assertNotEquals(sites0AccName, sites[0].TGS_Main_Account_Name__c);
        System.debug('[TGS_Account_Validations_Handler_Test.testFillSitesInformation] sites[0].TGS_Main_Account_Name__c: ' + sites[0].TGS_Main_Account_Name__c);
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Add coverage for lines that are never called during normal test execution. This does not test correct functionality

     History: 
    
     <Date>                     <Author>                    <Change Description>
     22/03/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void test_testSuppressed() {
        Map<Id, String> m = new Map<Id, String>{'00Dg0000006SKDm' => 'UAT OrgId'};
        new TGS_Account_Hierarchy.UpdateSites(m).execute(null);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Álvaro López
     Company:       Accenture
     Description:   Add coverage for method TGS_Account_Validations_Handler.preventAccountToBeDeleted

     History: 
    
     <Date>                     <Author>                    Change Description>
     08/11/2017                 Álvaro López                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void preventAccountToBeDeleted_Test(){

        Id recordtypeHold = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);

        Account accHo = new Account(Name='Test Acc Ho', RecordTypeId = recordtypeHold,
                                      TGS_Es_MNC__c = true , BI_Identificador_Externo__c = '1000');

        insert accHo;

        String profile = 'TGS_Perfil 1';

        Profile miProfile = [SELECT Id, Name
        FROM Profile
        Where Name = :profile
        Limit 1];

        User u = [SELECT Id FROM User WHERE Profile.Name =: profile AND BI_Permisos__c = 'TGS' AND isActive = true LIMIT 1];

        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;

            PermissionSet myPermissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'TGS_Instanciaciones'];

            PermissionSetAssignment psa = new PermissionSetAssignment(
                PermissionSetId = myPermissionSet.Id, 
                AssigneeId = u.Id
            );
          
            insert psa; 
           
        }

        System.runAs(u){
            try{
                delete accHo;
            }
            catch(Exception e){
                System.assert(e.getMessage().contains('You have not permissions to delete this record'));
            }
        }

    }


    
}