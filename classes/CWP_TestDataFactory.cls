/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   helper class for new modification creation process
    Test Class:    CWP_InstalledServicesController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    23/02/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest
public class CWP_TestDataFactory {
    
    /*ejemplo de testSetup*/    
    /*
    @testSetup
    //static void createEnvironment(){
    private static void createEnvironment(){
        
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType = 'Account']); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapBydevName.put(i.DeveloperName, i.id);
        }
        
        Account accHolding;
        Account accCustCountry;
        Account accLegalEntity;
        Account accBussinesUnit;
        Account accCostCenter;
        
        accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
        insert accHolding;
        
        accCustomerCountry = CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
        insert accCustomerCountry;
        
        // prueba realizada con metodo alternativo
        accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
        accLegalEntity.ParentId = accCustomerCountry.id,
        accLegalEntity.TGS_Aux_Holding__c = accHolding.id,
        insert accLegalEntity;
        
        accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'bussinesUnit1', accHolding.id, accLegalEntity);
        insert accBussinesUnit;
                
        accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'), 'costCenter1', accHolding.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id);
        insert accCostCenter;
        
        Contact contactDummy = CWP_TestDataFactory.createContact(accLegalEntity.id, 'name1');
        insert contactDummy;
            
    }
    */
    
    /*
    Developer Everis
    Function: Method used to create an account of type holding
    */
    public static Account createHolding(id accRT, string accName) {
        Account retAcc = new Account(
            recordTypeId = accRT,
            Name = accName,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True
        );
        return retAcc;
    }

    /*
    Developer Everis
    Function: Method used to create an account of type customer country
    */    
    public static Account createCustomerCountry(id accRT, string accName, id holdingId){
        Account retAcc = new Account(
            recordTypeId = accRT,
            Name = accName,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holdingId,
            ParentId = holdingId
        );
        return retAcc;
    }
    
    /*
    Developer Everis
    Function: Method used to create an account of type legal entity
    */
    public static Account createLegalEntity(id accRT, string accName, id holdingId, id accParentId){
        Account retAcc = new Account(
            recordTypeId = accRT,
            Name = accName,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            ParentId = accParentId,
            TGS_Aux_Holding__c = holdingId
        );
        return retAcc;
    }
    
    /*
    Developer Everis
    Function: Method used to create an account of type business unit
    */
    public static Account createBussinesUnit(id accRT, string accName, id holdingId, id accParentId){
        Account retAcc = new Account(
            recordTypeId = accRT,
            Name = accName,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            ParentId = accParentId,
            TGS_Aux_Holding__c = holdingId
        );
        return retAcc;
    }
    
    /*
    Developer Everis
    Function: Method used to create an account of type cost center
    */    
    public static Account createCostCenter(id accRT, string accName, id holdingId, id accParentId, id businessUnit, id customerCountry, id legalEntity){
        Account retAcc = new Account(
            recordTypeId = accRT,
            Name = accName,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            ParentId = accParentId,
            TGS_Aux_Holding__c = holdingId,
            TGS_Aux_Business_Unit__c = businessUnit,
            TGS_Aux_Customer_Country__c = customerCountry,
            TGS_Aux_Legal_Entity__c = legalEntity
        );
        return retAcc;
    }
    
        /*
    Developer Everis
    Function: Method used to create a contact
    */
    public static Contact createContact(Id accountId, string cName){        
        String mailName = 'telefonicaTest'+cName+'@telefonica.com';
        Contact contactTest = new Contact(LastName = cName,
                                          AccountId = accountId,
                                          Email = mailName                                          
        );        
        return contactTest;
    }
    
    /*
    Developer Everis
    Function: Method used to create a profile
    */
    public static Profile getProfile(String profileName){
        Profile p = [Select Id From Profile Where Name=:profileName];
        return p;
    }
    
    /*
    Developer Everis
    Function: Method used to create an user role
    */
    public static UserRole createRole(String name){
        UserRole r = new UserRole(Name = name);
        return r;
    } 
    
    /*
    Developer Everis
    Function: Method used to create an User
    */
    public static User createUser(string name, id roleId, id profileId){        
        User u =  new User( 
            Alias = name,
            Email= name +'@roletest1.com',
            UserRoleId = roleId, 
            LocalesIdKey='en_US', 
            EmailEncodingKey='UTF-8',
            Lastname='Testing', 
            LanguageLocaleKey='en_US', 
            ProfileId = profileId, 
            TimezonesIdKey='America/Los_Angeles',
            FirstName = name,
            UserName= name +'@roletest1.com', 
            BI_Permisos__c = 'TGS', 
            isActive = true
        );      
        return u;           
    }
    
    /***********************************************/
    /***** SERVICE TRACKING: Cases_Controller ******/
        
    /*
    Developer Everis
    Function: Method used to create a catalog
    */
    public static NE__Catalog__c createCatalog(string catName){
        NE__Catalog__c theCat = new NE__Catalog__c(
            Name = catName
        );
        return theCat;
    }
    
    /*
    Developer Everis
    Function: Method used to create a catalog category
    */
    public static NE__Catalog_Category__c createCatalogCategory(string ccName, id catId, id ccParentId){
        NE__Catalog_Category__c theCC = new NE__Catalog_Category__c(
            name = ccName,
            NE__CatalogId__c = catId,
            NE__Parent_Category_Name__c = ccParentId
        );
        return theCC;
    }
    
    /*
    Developer Everis
    Function: Method used to create a commercial product
    */
    public static NE__Product__c createCommercialProduct(string ccp){
        NE__Product__c testProduct = new NE__Product__c(
            Name=ccp
        );
        return testProduct;
    }
    
    /*
    Developer Everis
    Function: Method used to create a catalog item
    */    
    public static NE__Catalog_Item__c createCatalogoItem(string product, id idSubcategory, id idCatalog, id idProduct){
        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(
            NE__Type__c = product,                          //'Product'
            NE__Catalog_Category_Name__c = idSubcategory,   //testCatalogSubCategory.Id, 
            NE__Catalog_Id__c = idCatalog,                  //testCatalog.Id, 
            NE__ProductId__c = idProduct                    //testProduct.Id 
        );
        return testCatalogItem;
    }
    
    /*
    Developer Everis
    Function: Method used to create an asset
    */     
    public static NE__Asset__c createAsset(id accountId){
        NE__Asset__c commAsset = new NE__Asset__c(
            NE__ServAccId__c = accountId,//user.Contact.AccountId,
            NE__BillAccId__c = accountId,//user.Contact.AccountId,
            TGS_RFB_date__c=date.today(),
            TGS_RFS_date__c=date.today(),
            TGS_Billing_end_date__c=date.today()
        );
        return commAsset;
    }
    
    /*
    Developer Everis
    Function: Method used to create an order
    */
    public static NE__Order__c createOrder(id idRecordType, id idTestCatalog, id idAsset, string orderType, id idAccount){
        NE__Order__c testOrder = new NE__Order__c(
            RecordTypeId = idRecordType,                //rtId,
            NE__CatalogId__c = idTestCatalog,           //testCatalog.Id,
            NE__Asset__c = idAsset,                     //commAsset.Id,
            NE__Configuration_Type__c = orderType,
            NE__AccountId__c = idAccount                //user.Contact.AccountId
        );
        return testOrder;
    }
    
    /*
    Developer Everis
    Function: Method used to create an order item
    */
    public static NE__OrderItem__c createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c OrderItem = new NE__OrderItem__c(
            NE__Account__c = accountId,                     //user.Contact.AccountId,
            NE__OrderId__c = orderId,                       //ord.Id,
            NE__ProdId__c = productId,                      //testProduct.Id,
            NE__CatalogItem__c = catalogItem,               //testCatalogItem.Id,
            NE__Qty__c= quantity,                            //1
            NE__Country__c='Spain',
            NE__City__c='Madrid'
        );
        return OrderItem;
    }
    
    /*
    Developer Everis
    Function: Method used to create a family
    */
    public static NE__Family__c createFamily(string famName){
        NE__Family__c retFam = new NE__Family__c(
            name = famName
        );
        return retFam;
    }
    
    /*
    Developer Everis
    Function: Method used to create a dynamic property definition
    */
    public static NE__DynamicPropertyDefinition__c createDynamiyProperty(string name){
        NE__DynamicPropertyDefinition__c retDyn = new NE__DynamicPropertyDefinition__c(
            Name = name
        );
        return retDyn;
    }
    
    /*
    Developer Everis
    Function: Method used to create a product family property
    */
    public static NE__ProductFamilyProperty__c createFamilyProperty(id famId, id propDefId){
        NE__ProductFamilyProperty__c retFamProp = new NE__ProductFamilyProperty__c(
            NE__FamilyId__c = famId,
            TGS_Hidden_in_CWP__c = false,
            NE__PropId__c = propDefId
        );
        return retFamProp;
    }
    
    /*
    Developer Everis
    Function: Method used to create an order item attribute
    */
    public static NE__Order_Item_Attribute__c createOrderItemAttribute(id oiId, id famPropId){
        NE__Order_Item_Attribute__c retOIA = new NE__Order_Item_Attribute__c(
            NE__Order_Item__c = oiId,
            NE__FamPropId__c = famPropId,
            Name = 'attributeName',
            NE__Value__c = 'attributeValue'
        );
        return retOIA;
    }
    
    /*
    Developer Everis
    Function: Method used to create a case
    */
    public static Case createCase(id accountId, id orderItemId, id rtId, string subject, string status, string tgsService){
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');        
        //Case testCaseOrder = new Case(RecordTypeId = rtId , Subject = 'Test order case', Status = 'Assigned', TGS_Service__c = 'Smart M2M - SIM Request');
        Case testCaseOrder = new Case(
            AccountId = accountId,
            TGS_customer_Services__c = orderItemId,
            RecordTypeId = rtId, 
            Subject = subject,          //'Test order case', 
            Status = status,            //'Assigned', 
            TGS_Service__c = tgsService // 'Smart M2M - SIM Request'
        );        
        return testCaseOrder;       
    }       
    
            
    /*
    Developer Everis
    Function: Method used to create and insert a case of type order
    */
    public static Case dummyOrderCase() {
        //Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Order Management Case').getRecordTypeId();
        Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
        Case testCaseOrder = new Case(RecordTypeId = rtId , Subject = 'Test order case', Status = 'Assigned', TGS_Service__c = 'Smart M2M - SIM Request');
        insert testCaseOrder;
        return testCaseOrder;
    }
    
    /*
    Developer Everis
    Function: Method used to create a testing catalog
    */
    public static NE__Catalog_Item__c dummyCatalogItem() {
        NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
        insert testCatalog;
        NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
        insert testCatalogCategory;
        NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(Name='Test SubCategory', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCatalogCategory.Id);
        insert testCatalogSubCategory;
        NE__Product__c testProduct = new NE__Product__c(Name='Test Product');
        insert testProduct;
        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCatalogSubCategory.Id, NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
        insert testCatalogItem;
        return testCatalogItem;
    }
    
    /*
    Developer Everis
    Function: Method used to create a testing configuration with all the related objects needed
    */
    public static NE__OrderItem__c dummyConfiguration(Id userId, String orderType) {
        User user = TGS_Portal_Utils.getUser(userId);  
        //Id rtId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        
        NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
        insert testCatalog;

        NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(
            Name = 'Test Category',
            NE__CatalogId__c = testCatalog.Id
        );
        insert testCatalogCategory;

        NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(
            Name = 'Test SubCategory',
            NE__CatalogId__c = testCatalog.Id,
            NE__Parent_Category_Name__c = testCatalogCategory.Id
        );
        insert testCatalogSubCategory;

        NE__Product__c testProduct = new NE__Product__c(Name='Test Product');
        insert testProduct;

        // Family property
        NE__Family__c family = new NE__Family__c(Name = 'Product Family');
        insert family;
        NE__DynamicPropertyDefinition__c property = new NE__DynamicPropertyDefinition__c(Name = 'Attribute 1', NE__Type__c = 'String');
        insert property;
        NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(
            NE__FamilyId__c = family.Id,
            NE__PropId__c = property.Id,
            NE__Required__c = 'No',
            TGS_Is_key_attribute__c = true
        );
        insert familyProp;
        NE__ProductFamily__c productFamily = new NE__ProductFamily__c(
            NE__ProdId__c = testProduct.Id,
            NE__FamilyId__c = family.Id
        );
        insert productFamily;
        
        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(
            NE__Type__c = 'Product',
            NE__Catalog_Category_Name__c = testCatalogSubCategory.Id,
            NE__Catalog_Id__c = testCatalog.Id,
            NE__ProductId__c = testProduct.Id
        );
        insert testCatalogItem;
        
        NE__Asset__c commAsset = new NE__Asset__c(
            NE__ServAccId__c = user.Contact.AccountId,
            NE__BillAccId__c = user.Contact.AccountId,
            TGS_RFB_date__c=date.today(),
            TGS_RFS_date__c=date.today(),
            TGS_Billing_end_date__c=date.today()
        );
        insert commAsset;

        // JMF 27/10/2016 - Insert order with Commertial Asset
        NE__Order__c ord = new NE__Order__c(
            RecordTypeId = rtId,
            NE__CatalogId__c = testCatalog.Id,
            NE__Asset__c = commAsset.Id,
            NE__Configuration_Type__c = orderType,
            NE__AccountId__c = user.Contact.AccountId
        );
        insert ord;
        NE__OrderItem__c ci = new NE__OrderItem__c(
            NE__Account__c = user.Contact.AccountId,
            NE__Billing_Account_Asset_Item__c = TGS_Portal_Utils.getLevel5(userId, 0)[0].Id,
            NE__Service_Account_Asset_Item__c = TGS_Portal_Utils.getLevel3(userId, 0)[0].Id,
            NE__OrderId__c = ord.Id,
            NE__ProdId__c = testProduct.Id,
            NE__CatalogItem__c = testCatalogItem.Id,
            NE__Qty__c=1
        );
        insert ci;
        /* Attribute of the OrderItem*/
        NE__Order_Item_Attribute__c cia = new NE__Order_Item_Attribute__c(
            Name = 'Attribute 1',
            NE__Value__c = 'test',
            NE__Order_Item__c = ci.Id
        );
        insert cia;


        /* Case */
        if(orderType == null) {
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
            
            // If there is an order type, code will create the case for you
            Case testCase = new Case(
                AccountId = user.Contact.AccountId,
                RecordTypeId = rtId,
                Subject = 'Test Case',
                Status = 'Assigned',
                Order__c = ord.Id,
                TGS_Customer_Services__c = ci.Id,
                TGS_Invoice_Date__c = Date.today(),
                TGS_Service__c = testProduct.Name, //New value needed JEG 29/11/2016
                Type='New'
            );
            insert testCase;

            ord.Case__c = testCase.Id;
            update ord;
        }
        return ci;
    }    
    /***** SERVICE TRACKING: Cases_Controller ******/
    /***********************************************/
    
    /*
    Developer Everis
    Function: Method used to create an Event
    */
    public static void createEvent (String selectedUser, Event__c event,String fecha,String initHour, String finishHour, String webdate) {   	
    	
		
            if(event.id != null && String.isEmpty(event.id)) {
                List<Event> eventToEdit = [Select StartDateTime, Subject, Location, EndDateTime, Description, Type, OwnerId, CreatedById, Id, BI_ParentId__c From Event e where id= :event.id limit 1];                
       
                if(!eventToEdit.isEmpty()){
                    if(eventToEdit[0].BI_ParentId__c!=null) {
                        eventToEdit = [Select StartDateTime, Subject, Location, EndDateTime, Description, Type, OwnerId, CreatedById, Id, BI_ParentId__c From Event e where id= :eventToEdit[0].BI_ParentId__c limit 1];
                    }
                    event.Location__c = eventToEdit[0].Location;                                                     
                    event.OwnerId = eventToEdit[0].OwnerId;                             
                    event.StandardEventId__c = eventToEdit[0].Id;
     
                }
            } 
            
            string [] dates = fecha.split('-');
            string month = dates[1];
            month = String.valueOf(Integer.valueOf(month));
            string day = dates[2].substring(0,2);
            string year = dates[0];
            year = String.ValueOf(Integer.valueOf(year));
            
            
            String stringDate =  year + '-' + month + '-' + day + ' ' + initHour+':00';
            String stringDatefin = year + '-' + month + '-' + day + ' ' + finishHour+':00';
            
            event.EndDateTime__c = datetime.valueOf(stringDatefin);
            event.StartDateTime__c = datetime.valueOf(stringDate);
            
            event.GuestList__c = '';
            if(selectedUser != null && !String.isEmpty(selectedUser)) {
                System.debug('SELECTED: '+selectedUser);
                event.GuestList__c = selectedUser;
            } 
            Id accId = BI_AccountHelper.getCurrentAccountId();
            for(Account acc:[select Id, OwnerId from Account where Id=:accId limit 1]){
                event.OwnerId = acc.OwnerId; 
            }
            event.Type__c = Label.PCA_Meeting;
            
            insert event;
    }
}