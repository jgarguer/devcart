/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for BI_COL_Opportunity_ctr
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-05-29      Daniel ALexander Lopez (DL)     New test Class
* @version   1.1    2015-05-31      Raul Mora (RM)                  Finish class
*            1.2    2016-12-30      Gawron, Julián                  Add testSetup
             1.3    2017-01-26      Pedro Párraga                   Increase in coverage
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
public with sharing class BI_COL_Opportunity_tst{
    
    @isTest static void test_Method_One(){

        BI_bypass__c objBibypass = new BI_bypass__c();
        objBibypass.BI_migration__c=true;
        insert objBibypass;
            
         Account objCuenta = new Account(Name = 'prueba',
                                        BI_Country__c = 'Colombia',
                                        TGS_Region__c = 'América',
                                        BI_Tipo_de_identificador_fiscal__c = 'NIT',
                                        CurrencyIsoCode  = 'GTQ',BI_Segment__c = 'test',
                                        BI_Subsegment_Regional__c = 'test',
                                        BI_Territory__c = 'test');
        insert objCuenta;
            
        Opportunity objOppty = new Opportunity(Name = 'prueba opp',
                                               AccountId = objCuenta.Id,
                                               BI_Country__c = 'Colombia',
                                               CloseDate = System.today().addDays(+5),
                                               StageName = 'F1 - Closed Won',
                                               CurrencyIsoCode = 'COP',
                                               Certa_SCP__contract_duration_months__c = 12,
                                               BI_Plazo_estimado_de_provision_dias__c = 0);
                        
        insert objOppty;
            
        BI_Col_Ciudades__c objCiudad = new BI_Col_Ciudades__c (Name = 'Test City',
                                                               BI_COL_Codigo_DANE__c = 'TestCDa',
                                                               BI_COL_Pais__c = 'Test Country');
        insert objCiudad;
            
        BI_Sede__c objSede = new BI_Sede__c(BI_COL_Ciudad_Departamento__c = objCiudad.Id,
                                            BI_Direccion__c = 'Test Street 123 Number 321',
                                            BI_Localidad__c = 'Test Local',
                                            BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero,
                                            BI_COL_Sucursal_en_uso__c = 'Libre',
                                            BI_Country__c = 'Colombia',
                                            Name = 'Test Street 123 Number 321, Test Local Colombia',
                                            BI_Codigo_postal__c = '12356');
        insert objSede;
            
        BI_Punto_de_instalacion__c objPuntosInsta = new BI_Punto_de_instalacion__c (BI_Cliente__c = objCuenta.Id,
                                                                                    BI_Sede__c = objSede.Id,
                                                                                    Name = 'Pella sucursal');
        insert objPuntosInsta;
            
        BI_COL_Descripcion_de_servicio__c objDesSer = new BI_COL_Descripcion_de_servicio__c(BI_COL_Oportunidad__c = objOppty.Id,
                                                                                                CurrencyIsoCode = 'COP');
        insert objDesSer;

        List<BI_COL_Modificacion_de_Servicio__c> lst_mds = new List<BI_COL_Modificacion_de_Servicio__c>();

        BI_COL_Modificacion_de_Servicio__c objModServ  = new BI_COL_Modificacion_de_Servicio__c(BI_COL_Codigo_unico_servicio__c = objDesSer.Id,
                                                                                                BI_COL_Clasificacion_Servicio__c = 'ALTA',
                                                                                                BI_COL_Oportunidad__c = objOppty.Id,
                                                                                                BI_COL_Bloqueado__c = false,
                                                                                                BI_COL_Estado__c = 'Activa',
                                                                                                BI_COL_Sucursal_de_Facturacion__c = objPuntosInsta.Id,
                                                                                                BI_COL_Sucursal_Origen__c = objPuntosInsta.Id,
                                                                                                BI_COL_Renovable__c = true,
                                                                                                BI_COL_Estado_aprobacion__c = 'Rechazado');
        lst_mds.add(objModServ);

        BI_COL_Modificacion_de_Servicio__c objModServ2  = new BI_COL_Modificacion_de_Servicio__c(BI_COL_Codigo_unico_servicio__c = objDesSer.Id,
                                                                                                BI_COL_Oportunidad__c = objOppty.Id,
                                                                                                BI_COL_Bloqueado__c = false,
                                                                                                BI_COL_Estado__c = 'Activa',
                                                                                                BI_COL_Sucursal_de_Facturacion__c = objPuntosInsta.Id,
                                                                                                BI_COL_Sucursal_Origen__c = objPuntosInsta.Id,
                                                                                                BI_COL_Renovable__c = true,
                                                                                                BI_COL_Clasificacion_Servicio__c = 'SERVICIO INGENIERIA',
                                                                                                BI_COL_Cargo_conexion__c = 0,
                                                                                                BI_COL_Estado_aprobacion__c ='Rechazado');
        lst_mds.add(objModServ2);
        insert lst_mds;

        BI_COL_CamposValidarEtapaOpp__c objCampValEtOpp = new BI_COL_CamposValidarEtapaOpp__c(Name = '1',
                                                                                              Api2__c = '',
                                                                                              Api__c = 'BI_COL_Tipo_Facturacion__c',
                                                                                              Etapa_1__c = 'F2 - Negotiation',
                                                                                              Etapa_2__c = 'F2 - Negotiation',
                                                                                              Mensaje_Error__c = 'Error MS Tipo Facturación Null.',
                                                                                              ValidacionEspecial__c = false);
        insert objCampValEtOpp;

        BI_COL_MIDAS__c objMid = new BI_COL_MIDAS__c(BI_COL_Proyecto__c = objOppty.Id,
                                                     BI_COL_Estado_de_la_aprobacion__c = 'Aprobado',
                                                     BI_COL_Capex_Total__c = 3.5);
        insert objMid;

        User objUsuario;
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        }

        System.runAs(objUsuario) {
            Test.startTest();

            ApexPages.StandardController controlador = new ApexPages.StandardController(objModServ);         
            BI_COL_Opportunity_ctr objOpportunity = new BI_COL_Opportunity_ctr(controlador);
    
            objOpportunity = new BI_COL_Opportunity_ctr(controlador);

            objCampValEtOpp.ValidacionEspecial__c = true;
            objCampValEtOpp.Api__c = 'BI_COL_Bloqueado__c';
            update objCampValEtOpp;
         
            objOpportunity = new BI_COL_Opportunity_ctr(controlador);

            objCampValEtOpp.Api__c = 'BI_Contacto__c';
            objPuntosInsta.BI_Contacto__c = null;
            update objCampValEtOpp;
          
            objOpportunity = new BI_COL_Opportunity_ctr(controlador);

            objCampValEtOpp.Api__c = 'BI_COL_Autoconsumo__c';
            objCampValEtOpp.Api2__c = 'BI_COL_Renovable__c';
            objOppty.BI_COL_Autoconsumo__c = true;

            update objCampValEtOpp;
            update objOppty;
        
            objOpportunity = new BI_COL_Opportunity_ctr(controlador);

            objCampValEtOpp.Api__c = 'BI_COL_Cargo_conexion__c';
            update objCampValEtOpp;


            ApexPages.StandardController controlador2 = new ApexPages.StandardController(objModServ2); 

            objOpportunity = new BI_COL_Opportunity_ctr(controlador2);

            objCampValEtOpp.Api__c = 'BI_COL_Autoconsumo__c';
            objCampValEtOpp.Api2__c = 'BI_COL_Renovable__c';
            objOppty.BI_COL_Autoconsumo__c = true;
            objModServ.BI_COL_Renovable__c = true;

            update objCampValEtOpp;
            update objOppty;
            update objModServ;
        
            objOpportunity = new BI_COL_Opportunity_ctr(controlador);

            BI_COL_Opportunity_ctr.quitaDecimal(objMid.BI_COL_Capex_Total__c);

            List<Opportunity> lst_opp = new List<Opportunity>();
            lst_opp.add(objOppty);

            BI_COL_Opportunity_ctr.creaLogTransaccionError('Error', lst_opp, 'infEnviada');

            objOppty.StageName = 'F3 - Offer Presented';
            update objOppty;

            objOpportunity = new BI_COL_Opportunity_ctr(controlador);

            Test.stopTest();
        }        
    }
}