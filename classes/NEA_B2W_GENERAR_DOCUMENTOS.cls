global with sharing class NEA_B2W_GENERAR_DOCUMENTOS
{
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Pachas y Alejandro Pantoja  
    Company:       New Energy Aborda
    Description:   This class is controller of the type of Document Header of Bit2Publish that is exported. This depends on what products are in the order.
    History: 

    <Date>                  <Author>                <Change Description>
    17/05/2016              Pedro Pachas             Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static List<NE__OrderItem__c> ProductsList;
	static List<Id> ComercialProdsId;
	static List<NE__Product__c> ProductosCom;
	static List<SelectOption> OptionsCateg;

	static String URLs;
	static List<String> valores;

	static boolean existeSM;
	static boolean existeVDC;

	WebService static String CapturarCategorizacionProductos(String orderId)
	{
		String Identi = orderId;
		existeSM = false;
		existeVDC = false;

		//Controlling Custom PickList Field is not empty 
		try
		{
			ProductsList = [Select Id, NE__ProdId__c from NE__OrderItem__c where NE__OrderId__c=:orderId];
			ComercialProdsId = new List<Id>();

			for(NE__OrderItem__c CI : ProductsList)
			{
				ComercialProdsId.add(CI.NE__ProdId__c);
			}

			ProductosCom = [Select Id, Name, Family_Local__c from NE__Product__c where Id IN :ComercialProdsId];
			//Controlling Custom Methods are working
			try
			{
				OptionsCateg = getValuesCategorizacion();

				for(NE__Product__c pro : ProductosCom)
				{
					String valor = pro.Family_Local__c;
					//The value 'MOVIL' is in the first position
					if(OptionsCateg.get(0).getValue().equals(valor))
					{
						existeSM = true;
					}//The value 'VDC' is in the second position
					else if(OptionsCateg.get(1).getValue().equals(valor))
					{
						existeVDC = true;
					}
				}
			}

			catch(Exception e)
			{
				System.debug('Exception: ' + e.getMessage());
			}

		}
		catch(Exception e)
		{
			System.debug('Exception: ' + e.getMessage());
		}

		/*	EEContractDataMapTelefonica
		EEContractDataMapclone*/
		if(existeSM && !existeVDC)
		{
			URLs = '/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapTelefonica&nameDCH=DocOfertaSM&dateFormat=dd-MM-yyyy&parentId='+Identi;
		}
		else if(existeVDC && !existeSM)
		{
			URLs = '/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapTelefonica&nameDCH=DocOfertaVDC&dateFormat=dd-MM-yyyy&parentId='+Identi;
		}
		else if(existeSM  && existeVDC)
		{
			URLs = '/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapTelefonica&nameDCH=DocOfertaCot&dateFormat=dd-MM-yyyy&parentId='+Identi;
		}
		else
			URLs = 'No existe ningún producto de Soluciones Moviles o VDC en esta orden.';
		return URLs;
	}

	global static List<SelectOption> getValuesCategorizacion()
	{
	  	List<SelectOption> options = new List<SelectOption>();
	        
	    Schema.DescribeFieldResult fieldResult =
		NE__Product__c.Family_Local__c.getDescribe();
	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        
	    for( Schema.PicklistEntry f : ple)
	    {
	    	/*We have to take two values: 'VDC' and 'MOVIL' and we don't have any way to take them without using literal words
	    	If the value from one of these changed, also that changed will be updated in this method*/
	    	if(f.getValue().equals('VDC') || f.getValue().equals('MOVIL'))
	    	{
	    		options.add(new SelectOption(f.getLabel(), f.getValue()));
	    	}	       
	    }       
	    return options;
	}
}