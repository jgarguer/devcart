@isTest
private class BI_ProjectAttachmentCtrl_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Class to manage cover of BI_ProjectAttachmentCtrl

    History: 
    
     <Date>                     <Author>                <Change Description>
    14/11/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	@isTest static void BI_ProjectAttachmentCtrl_TEST() {
		List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		Opportunity opp = new Opportunity(Name = 'Test_opp',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Country__c = lst_pais[0]);
		insert opp;

		Milestone1_Project__c proj = new Milestone1_Project__c ();   		
			       		proj.Name = 'TEST PROJ ' + Datetime.now().getTime();
			        	proj.BI_Oportunidad_asociada__c = opp.Id;
			        	proj.BI_Etapa_del_proyecto__c = Label.BI_Kickoff_inicio;
			        	proj.BI_Country__c = lst_pais[0];
			        	proj.BI_Complejidad_del_proyecto__c=Label.BI_Complejo;
		insert proj;


        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        Attachment attach=new Attachment(Name='Unit Test Attachment',
        								body=bodyBlob,
        								parentId=proj.Id, 
        								ContentType='image/png');  
        
        insert attach;  

        ApexPages.currentPage().getParameters().put('id',attach.parentId);
        ApexPages.currentPage().getParameters().put('description','test');
        
        Test.startTest();


        BI_ProjectAttachmentCtrl clase = new BI_ProjectAttachmentCtrl();
        //Try
        system.assertEquals(clase.haveError, false);

        clase.myDoc.name = 'test_attch_2';
        bodyBlob = Blob.valueOf('Unit Test Attachment Body 2');
        clase.myDoc.body = bodyBlob;

        clase.attachFile();
		
		system.assertEquals(clase.haveError, false);
		system.assertEquals(clase.noError, true);
      	
      	//Catch
		clase.attachFile();
		ApexPages.currentPage().getParameters().put('id',attach.Id); //NOT VALID ID 
		clase = new BI_ProjectAttachmentCtrl();


      	clase.returnpage();
      	Test.stopTest();

	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}