public with sharing class TGS_Attachment_Handler {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Angel Galindo, Carlos Campillo
    Company:       Deloitte
    Description:   This class contains a method related with Attachment object.
					Class invoked by the trigger TGS_AttachmentTrigger.
    
    History

    <Date>            <Author>                  	<Description>
    27/04/2015        Miguel Angel Galindo			Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
    
    /* 
     *  This method checks the number of attachs in an object, if is bigger than 3 an error is shown 
	 */
    public static void checkAttachmentNumber(List<Attachment> newAttachmentList){
		
		List<Id> parentId = new List<Id>();
		for(Attachment att :newAttachmentList){
			String objName = att.ParentId.getSObjectType().getDescribe().getName();
			if(objName == 'TGS_Work_Info__c'){
				parentId.add(att.ParentId);
			}
		}
		
		AggregateResult[] attAggregatedResult = [SELECT ParentId, COUNT(Id)
									FROM Attachment
									WHERE ParentId IN :parentId
									GROUP BY ParentId];
		
		for (AggregateResult ar : attAggregatedResult)  {
    		System.debug('ParentId' + ar.get('ParentId'));
    		if((Integer)ar.get('expr0')>=3){
    			System.debug('COUNT(Id)' + ar.get('expr0'));
    			for(Attachment att : newAttachmentList){
    				if(ar.get('ParentId')==att.ParentId){
    					att.addError('You can NOT attach more than 3 attachment');
    				}
    			}
    		}
		}
		
		
	}
}