/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Custom controller for the FOCO Revenue Goal Chart
	Test Class:     BI_FOCOPercentageOfGoalChart_TEST
	History
	<Date>          <Author>           <Change Description>
	25-Sep-2014     Virgilio Utrera    Initial Version
	02-Oct-2014     Virgilio Utrera    Modified class constructor
	15-Oct-2014     Virgilio Utrera    Modified methods to take into account visibility rules
	20-Oct-2014		Virgilio Utrera    Moved queries to BI_FOCOUtil class and added currency conversion
	11-Mar-2015		Virgilio Utrera    Refactored methods to handle large data volumes
-------------------------------------------------------------------*/

public class BI_FOCOPercentageOfGoalChart_CTRL {

	public transient Boolean renderErrorMessage { get; set; }
	public transient Decimal percentageOfAnnualRevenueGoal { get; set; }
	public transient String errorMessage { get; set; }

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Class constructor
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		25-Sep-2014     Virgilio Utrera    Initial Version
		02-Oct-2014     Virgilio Utrera    Added error handling
		20-Oct-2014		Virgilio Utrera    Added currency conversion
		11-Mar-2015		Virgilio Utrera    Transferred logic to getChartData method
	-------------------------------------------------------------------*/

	public BI_FOCOPercentageOfGoalChart_CTRL() {
		errorMessage = '';
		getChartData();

		if(errorMessage.length() > 0)
			renderErrorMessage = true;
		else
			renderErrorMessage = false;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets the chart data for the running user
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		11-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	private void getChartData() {
		Decimal amount;
		Decimal goal;
		Id userId;
		List<AggregateResult> focoResults = new List<AggregateResult>();
		List<BI_Objetivo_Comercial__c> goalResults = new List<BI_Objetivo_Comercial__c>();
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		Map<String, Double> conversionRatesMap = new Map<String, Double>();
		Set<Id> subordinateIds;
		String userDefaultCurrency;
		String corporateCurrency;

		percentageOfAnnualRevenueGoal = 0;
		amount = 0;
		goal = 0;
		userId = UserInfo.getUserId();
		userDefaultCurrency = UserInfo.getDefaultCurrency();
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);
		currenciesList = BI_FOCOUtil.getCurrenciesList();

		// Populate map with conversion rates, set the currency corporate code
		for(CurrencyType currencyType : currenciesList) {
			conversionRatesMap.put(currencyType.IsoCode, currencyType.ConversionRate);

			if(currencyType.IsCorporate)
				corporateCurrency = currencyType.IsoCode;
		}

		// Get Ids from all subordinate users
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);

		if(!subordinateIds.isEmpty())
			// There are subordinates, query FOCO records from user and subordinates for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByPeriod(userId, subordinateIds);
		else
			// There are no subordinates, query FOCO records from user for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByPeriod(userId, null);

		// Stop the execution if there are no FOCO records to show
		if(focoResults.isEmpty()) {
			errorMessage = 'No se han encontrado Registros de Datos FOCO para el año en curso';
			System.debug(errorMessage);
			return;
		}

		if(!subordinateIds.isEmpty())
			// There are subordinates, query commercial goals from user and subordinates for current year
			goalResults =  BI_FOCOUtil.getRevenueGoalsFromCurrentYear(userId, subordinateIds);
		else
			// There are no subordinates, query commercial goals from user for current year
			goalResults =  BI_FOCOUtil.getRevenueGoalsFromCurrentYear(userId, null);

		// Stop the execution if there are no commercial goal records to show
		if(goalResults.isEmpty() || goalResults[0].BI_Objetivo__c == null) {    
			errorMessage = 'No se han encontrado Objetivos Comerciales para el año en curso';
			System.debug(errorMessage);
			return;
		}

		// Calculate total amount for current year
		for(AggregateResult ar : focoResults) {
			// FOCO results' BI_Monto__c is automatically returned in the org's currency because the query contains a SUM() aggregate function with a GROUP BY clause
			if(userDefaultCurrency == corporateCurrency)
				// No currency conversion needed
				amount = amount + ((Decimal)ar.get('BI_Monto__c')).setScale(2, System.RoundingMode.CEILING);
			else
				// Convert amount into user's currency
				amount = amount + (((Decimal)ar.get('BI_Monto__c')) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
		}

		// Calculate total goal for current year
		for(BI_Objetivo_Comercial__c oc : goalResults) {
			if(oc.CurrencyIsoCode == userDefaultCurrency)
				// Currency is the same as user's currency
				goal = goal + oc.BI_Objetivo__c.setScale(2, System.RoundingMode.CEILING);
			else {
				if(oc.CurrencyIsoCode == corporateCurrency)
					// Currency is corporate currency, amount is converted into user's default currency
					goal = goal + (oc.BI_Objetivo__c * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
				else
					// Currency is other than user's or corporate
					goal = goal + ((oc.BI_Objetivo__c / conversionRatesMap.get(oc.CurrencyIsoCode)) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
			}
		}

		// Calculate amount as a percentage of annual goal
		if(goal > 0)
			percentageOfAnnualRevenueGoal = ((amount / goal) * 100).setScale(2, System.RoundingMode.CEILING);
	}
}