/*-------------------------------------------------------------------
	Author:        Virgilio Utrera
	Company:       Salesforce.com
	Description:   Test class for BI_AmountVsGoalChart_CTRL
	
	History
	<Date>          <Author>           <Change Description>
	29-Sep-2014     Virgilio Utrera    Initial Version
	01-Oct-2014     Virgilio Utrera    Added exception handling coverage
	14-Oct-2014     Virgilio Utrera    Adjusted expected test results
	15-Oct-2014     Virgilio Utrera    Refined test data creation methods
	22-Oct-2014		Virgilio Utrera    Added currency handling
	27-Oct-2014		Virgilio Utrera    Added role hierarchy handling
	06-Feb-2015		Virgilio Utrera    Changed running user to allow mixed DML operations
-------------------------------------------------------------------*/

@isTest
private class BI_FOCOAmountVsGoalChart_TEST {

	@testSetup static void userOrgCS() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
		userTGS.TGS_Is_BI_EN__c = true;
		insert userTGS;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for pulling amount vs goal data with FOCO currency equals to corporate currency
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		22-Oct-2014     Virgilio Utrera    Initial Version
		27-Oct-2014		Virgilio Utrera    Added role hierarchy handling
		06-Feb-2015		Virgilio Utrera    Test data is now created by the running user to allow mixed DML operations
	-------------------------------------------------------------------*/

	static testMethod void testGetAmountVsGoalDataCorporateCurrency() {
		BI_FOCOAmountVsGoalChart_CTRL controller;
		CurrencyType userCurrency;
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		String organizationCurrencyIsoCode;
		User supervisorUser;
		User thisUser;
		User user;
		UserRole supervisorUserRole;
		UserRole userRole;

		thisUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];

		System.runAs(thisUser) {
			currenciesList = [ SELECT IsoCode, IsCorporate, ConversionRate FROM CurrencyType WHERE IsActive = TRUE LIMIT 1000 ];	

			for(CurrencyType currencyType : currenciesList) {
				if(currencyType.IsCorporate)
					organizationCurrencyIsoCode = currencyType.IsoCode;
				else
					userCurrency = currencyType;
			}

			// Create hierarchical user roles
			supervisorUserRole = createUserRole('Supervisor_Role_Test', null);
			userRole = createUserRole('User_Role_Test', supervisorUserRole.Id);

			// Create test users
			supervisorUser = createUser('testfocosupervisoruser@testorg.com', 'focosupervisoruser', 'tfsu@testorg.com', 'tfsu', userCurrency.IsoCode, supervisorUserRole.Id);
			user = createUser('testfocouser@testorg.com', 'focouser', 'tfu@testorg.com', 'tfu', userCurrency.IsoCode, userRole.Id);

			// Create test data
			createGoalRecords(organizationCurrencyIsoCode, user.Id);
			createFOCORecords(organizationCurrencyIsoCode, user.Id);
		}

		// Execute test
		System.runAs(supervisorUser) {
			Test.startTest();
			controller = new BI_FOCOAmountVsGoalChart_CTRL();
			Test.stopTest();

			System.assertEquals(0, controller.errorMessage.length());
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for pulling amount vs goal data with FOCO currency different than corporate and user's currency
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		22-Oct-2014     Virgilio Utrera    Initial Version
		27-Oct-2014		Virgilio Utrera    Added role hierarchy handling
		06-Feb-2015		Virgilio Utrera    Test data is now created by the running user to allow mixed DML operations
	-------------------------------------------------------------------*/

	static testMethod void testGetAmountVsGoalDataDifferentCurrencies() {
		BI_FOCOAmountVsGoalChart_CTRL controller;
		CurrencyType focoCurrency;
		CurrencyType userCurrency;
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		User supervisorUser;
		User thisUser;
		User user;
		UserRole supervisorUserRole;
		UserRole userRole;

		thisUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];

		System.runAs(thisUser) {
			currenciesList = [ SELECT IsoCode, IsCorporate, ConversionRate FROM CurrencyType WHERE IsActive = TRUE LIMIT 1000 ];	

			for(CurrencyType currencyType : currenciesList) {
				if(!currencyType.IsCorporate) {
					if(userCurrency == null)
						userCurrency = currencyType;
					else {
						if(focoCurrency == null)
							focoCurrency = currencyType;
					}
				}
			}

			// Create hierarchical user roles
			supervisorUserRole = createUserRole('Supervisor_Role_Test', null);
			userRole = createUserRole('User_Role_Test', supervisorUserRole.Id);

			// Create test users
			supervisorUser = createUser('testfocosupervisoruser@testorg.com', 'focosupervisoruser', 'tfsu@testorg.com', 'tfsu', userCurrency.IsoCode, supervisorUserRole.Id);
			user = createUser('testfocouser@testorg.com', 'focouser', 'tfu@testorg.com', 'tfu', userCurrency.IsoCode, userRole.Id);

			// Create test data
			createGoalRecords(focoCurrency.IsoCode, user.Id);
			createFOCORecords(focoCurrency.IsoCode, user.Id);
		}

		// Execute test
		System.runAs(supervisorUser) {
			Test.startTest();
			controller = new BI_FOCOAmountVsGoalChart_CTRL();
			Test.stopTest();

			System.assertEquals(0, controller.errorMessage.length());
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for pulling amount vs goal data with FOCO currency equals to user currency
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		29-Sep-2014     Virgilio Utrera    Initial Version
		01-Oct-2014     Virgilio Utrera    Refactored method for simplicity
		14-Oct-2014     Virgilio Utrera    Adjusted expected test results
		15-Oct-2014     Virgilio Utrera    Adjusted expected test results
		22-Oct-2014		Virgilio Utrera    Added currency handling
		27-Oct-2014		Virgilio Utrera    Added role hierarchy handling
		06-Feb-2015		Virgilio Utrera    Test data is now created by the running user to allow mixed DML operations
	-------------------------------------------------------------------*/

	static testMethod void testGetAmountVsGoalDataSameCurrencies() {
		BI_FOCOAmountVsGoalChart_CTRL controller;
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		String organizationCurrencyIsoCode;
		User supervisorUser;
		User thisUser;
		User user;
		UserRole supervisorUserRole;
		UserRole userRole;

		thisUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];

		System.runAs(thisUser) {
			currenciesList = [ SELECT IsoCode, IsCorporate FROM CurrencyType WHERE IsActive = TRUE LIMIT 1000 ];	

			for(CurrencyType currencyType : currenciesList) {
				if(currencyType.IsCorporate) {
					organizationCurrencyIsoCode = currencyType.IsoCode;
					break;
				}
			}

			// Create hierarchical user roles
			supervisorUserRole = createUserRole('Supervisor_Role_Test', null);
			userRole = createUserRole('User_Role_Test', supervisorUserRole.Id);

			// Create test users
			supervisorUser = createUser('testfocosupervisoruser@testorg.com', 'focosupervisoruser', 'tfsu@testorg.com', 'tfsu', organizationCurrencyIsoCode, supervisorUserRole.Id);
			user = createUser('testfocouser@testorg.com', 'focouser', 'tfu@testorg.com', 'tfu', organizationCurrencyIsoCode, userRole.Id);

			// Create test data
			createGoalRecords(organizationCurrencyIsoCode, user.Id);
			createFOCORecords(organizationCurrencyIsoCode, user.Id);
		}

		// Execute test
		System.runAs(supervisorUser) {
			Test.startTest();
			controller = new BI_FOCOAmountVsGoalChart_CTRL();
			Test.stopTest();

			System.assertEquals(0, controller.errorMessage.length());
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for handling exception when no FOCO records are found
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		01-Oct-2014     Virgilio Utrera    Initial Version
		27-Oct-2014		Virgilio Utrera    Added role hierarchy handling
	-------------------------------------------------------------------*/

	static testMethod void testNoFOCOResults() {
		BI_FOCOAmountVsGoalChart_CTRL controller;
		CurrencyType corporateCurrency;
		String organizationCurrencyISOCode;
		User user;
		UserRole userRole;

		corporateCurrency = [ SELECT IsoCode FROM CurrencyType WHERE IsCorporate = TRUE AND IsActive = TRUE LIMIT 1 ];
		organizationCurrencyISOCode = corporateCurrency.IsoCode;

		// Create test user
		userRole = createUserRole('User_Role_Test', null);
		user = createUser('testfocouser@testorg.com', 'focouser', 'tfu@testorg.com', 'tfu', organizationCurrencyIsoCode, userRole.Id);

		// Execute test
		System.runAs(user) {
			Test.startTest();
			controller = new BI_FOCOAmountVsGoalChart_CTRL();
			Test.stopTest();

			System.assertEquals('', controller.chartData);
			System.assertNotEquals(0, controller.errorMessage.length());
		}	
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for handling exception when no goal records are found
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		01-Oct-2014     Virgilio Utrera    Initial Version
		22-Oct-2014		Virgilio Utrera    Added currency handling
		27-Oct-2014		Virgilio Utrera    Added role hierarchy handling
	-------------------------------------------------------------------*/

	static testMethod void testNoGoalResults() {
		BI_FOCOAmountVsGoalChart_CTRL controller;
		CurrencyType corporateCurrency;
		String organizationCurrencyISOCode;
		User user;
		UserRole userRole;

		corporateCurrency = [ SELECT IsoCode FROM CurrencyType WHERE IsCorporate = TRUE AND IsActive = TRUE LIMIT 1 ];
		organizationCurrencyISOCode = corporateCurrency.IsoCode;

		// Create test user
		userRole = createUserRole('User_Role_Test', null);
		user = createUser('testfocouser@testorg.com', 'focouser', 'tfu@testorg.com', 'tfu', organizationCurrencyIsoCode, userRole.Id);

		// Create test data
		createFOCORecords(organizationCurrencyISOCode, user.Id);

		// Execute test
		System.runAs(user) {
			Test.startTest();
			controller = new BI_FOCOAmountVsGoalChart_CTRL();
			Test.stopTest();

			System.assertEquals('', controller.chartData);
			System.assertNotEquals(0, controller.errorMessage.length());
		}    
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Helper method for creating FOCO records
		IN:				String: currency for FOCO records
		OUT:
		History
		<Date>          <Author>           <Change Description>
		01-Oct-2014     Virgilio Utrera    Initial Version
		15-Oct-2014     Virgilio Utrera    Modified test data creation
		22-Oct-2014     Virgilio Utrera    Added currency as parameter
	-------------------------------------------------------------------*/

	static private void createFOCORecords(String currencyIsoCode, Id userId) {
		Account account;
		Account newAccount;
		BI_Periodo__c period;
		BI_Periodo__c newPeriod;
		BI_Registro_Datos_FOCO__c foco;
		BI_Registro_Datos_FOCO__c newFoco;
		CurrencyType defaultCurrency;
		String currentYear;
		String organizationCurrencyISOCode;

		currentYear = String.valueOf(System.Today().year());

		// Add test Account
		account = new Account(Name = 'Cliente_AmountVsGoal_Test', OwnerId = userId,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');

		try {
			insert account;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
			return;
		}

		newAccount = [ SELECT Id, Name, OwnerId FROM Account WHERE Name = :account.Name LIMIT 1 ];
		System.assertNotEquals(null, newAccount);
		System.assertEquals(account.Name, newAccount.Name);

		newAccount.OwnerId = userId;

		try {
			update newAccount;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
			return;
		}

		// Add test BI_Periodo__c
		period = new BI_Periodo__c(Name = 'Enero_' + currentYear + '_Test', BI_Mes_Ano__c = '01-' + currentYear);
		
		try {
			insert period;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
			return;
		}
		
		newPeriod = [ SELECT Id, Name FROM BI_Periodo__c WHERE Name = :period.Name LIMIT 1 ];
		System.assertNotEquals(null, newPeriod);
		System.assertEquals(period.Name, newPeriod.Name);

		// Add test BI_Registro_Datos_FOCO__c
		foco = new BI_Registro_Datos_FOCO__c(BI_Cliente__c = newAccount.Id, BI_Monto__c = 1000, BI_Periodo__c = newPeriod.Id, BI_Tipo__c = 'Ingreso Real', CurrencyIsoCode = currencyIsoCode);

		try {
			insert foco;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
			return;
		}

		newFoco = [ SELECT Id, BI_Cliente__r.Name, BI_Cliente__r.Owner.Name FROM BI_Registro_Datos_FOCO__c WHERE BI_Cliente__c = :newAccount.Id LIMIT 1 ];
		System.assertNotEquals(null, newFoco);
		System.assertEquals(newAccount.Name, newFoco.BI_Cliente__r.Name);
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Helper method for creating commercial goal records
		IN:				String: currency for goal records
		OUT:
		History
		<Date>          <Author>           <Change Description>
		01-Oct-2014     Virgilio Utrera    Initial Version
		15-Oct-2014     Virgilio Utrera    Refined test data creation
		22-Oct-2014     Virgilio Utrera    Added currency as parameter
	-------------------------------------------------------------------*/

	static private void createGoalRecords(String currencyIsoCode, Id userId) {
		BI_Objetivo_Comercial__c goal;
		BI_Objetivo_Comercial__c newGoal;
		BI_Periodo__c period;
		BI_Periodo__c newPeriod;
		String currentYear;

		currentYear = String.valueOf(System.Today().year());

		// Add test BI_Periodo__c
		period = new BI_Periodo__c(Name = 'Febrero_' + currentYear + '_Test', BI_Mes_Ano__c = '02-' + currentYear);

		try {
			insert period;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
			return;
		}

		newPeriod = [ SELECT Id, Name FROM BI_Periodo__c WHERE Name = :period.Name LIMIT 1 ];
		System.assertNotEquals(null, newPeriod);
		System.assertEquals(period.Name, newPeriod.Name);

		// Add test BI_Objetivo_Comercial__c
		goal = new BI_Objetivo_Comercial__c(BI_Objetivo__c = 2000, BI_Periodo__c = newPeriod.Id, BI_Tipo__c = 'Cartera', CurrencyIsoCode = currencyIsoCode, OwnerId = userId);

		try {
			insert goal;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
			return;
		}

		newGoal =  [ SELECT Id, BI_Periodo__r.Name FROM BI_Objetivo_Comercial__c WHERE BI_Periodo__c = :newPeriod.Id AND BI_Tipo__c = 'Cartera' LIMIT 1 ];
		System.assertNotEquals(null, newGoal);
		System.assertEquals(period.Name, newGoal.BI_Periodo__r.Name);
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Helper method for creating a user role
		IN:				String: role name
						Id: parent role Id
		OUT:			User: created role
		History
		<Date>          <Author>           <Change Description>
		27-Oct-2014     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static private UserRole createUserRole(String name, Id parentRoleId) {
		UserRole newUserRole;
		UserRole userRole;

		if(parentRoleId != null)
			userRole = new UserRole(Name = name, ParentRoleId = parentRoleId);	
		else
			userRole = new UserRole(Name = name);

		try {
			insert userRole;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
		}

		newUserRole = [ SELECT Id FROM UserRole WHERE Id = :userRole.Id LIMIT 1];
		System.assertNotEquals(null, newUserRole);
		System.assertEquals(userRole.Id, newUserRole.Id);

		return newUserRole;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Helper method for creating a user
		IN:				String: username
						String: user's last name
						String: user's email address
						String: user's alias
						String: default currency for the user
						Id: user role Id
		OUT:			User: created user
		History
		<Date>          <Author>           <Change Description>
		22-Oct-2014     Virgilio Utrera    Initial Version
		27-Oct-2014     Virgilio Utrera    Added user information as parameters
	-------------------------------------------------------------------*/

	static private User createUser(String userName, String lastName, String email, String alias, String currencyIsoCode, Id userRoleId) {
		Profile userProfile;
		User testUser;
		User newTestUser;

		userProfile = [ SELECT Id FROM Profile WHERE Name = :System.Label.BI_Administrador_Sistema LIMIT 1 ];

		testUser = new User(
								Alias = alias,
								Email = email,
								EmailEncodingKey = 'UTF-8',
								LastName = lastName,
								LanguageLocaleKey = 'en_US',
								LocaleSidKey = 'en_US',
								ProfileId = userProfile.Id,
								BI_Permisos__c = Label.BI_Administrador,
								TimeZoneSidKey = Label.BI_TimeZoneLA,
								Username = userName,
								DefaultCurrencyIsoCode = currencyIsoCode,
								UserRoleId = userRoleId
							);

		try {
			insert testUser;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCOAmountVsGoalChart_TEST, line ' + e.getLineNumber() + ': ' + e.getMessage());
		}

		newTestUser = [ SELECT Id, DefaultCurrencyIsoCode FROM User WHERE Id = :testUser.Id LIMIT 1];
		System.assertNotEquals(null, newTestUser);
		System.assertEquals(testUser.Id, newTestUser.Id);

		return newTestUser;
	}
}