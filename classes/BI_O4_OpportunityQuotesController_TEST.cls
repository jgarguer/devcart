/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ricardo Pereira
    Company:       New Energy Aborda
    Description:   Test class for BI_O4_OpportunityQuotesController
    Test Class:   
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Ricardo Pereira          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_OpportunityQuotesController_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }

    @isTest static void test_method_one() {
    
        BI_TestUtils.throw_exception=true;

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        BI_DataLoad.set_ByPass(true, false);

		Map<String,Id> MAP_NAME_RT = new Map<String,Id>();

        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('Opportunity', 'Account', 'NE__Order__c')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
       
        Account account = new Account(Name = 'Cliente Puerto Rico 2',
        							  RecordTypeId = MAP_NAME_RT.get(Constants.RECORD_TYPE_TGS_HOLDING),
                                      type = 'Prospect',
                                      BI_Country__c = 'Puerto Rico',
                                      BI_No_Identificador_fiscal__c = '2222444',
                                      CurrencyIsoCode = 'USD');
        insert account;

        Opportunity opptyLocal = new Opportunity(Name = 'Oportunidad Test Local',
        									RecordTypeId = MAP_NAME_RT.get('BI_Ciclo_completo'),
                                              AccountId = account.Id,
                                              BI_Country__c = 'Puerto Rico',
                                              BI_Opportunity_Type__c = 'Digital',
                                              CloseDate = date.today().addDays(5),
                                              StageName = Label.BI_F5DefSolucion,
                                              BI_Probabilidad_de_exito__c = '25',
                                              CurrencyIsoCode = 'USD',
                                              BI_Ingreso_por_unica_vez__c = 50,
											  BI_Recurrente_bruto_mensual__c = 10,
                                              BI_Duracion_del_contrato_Meses__c = 6,
                                              BI_Plazo_estimado_de_provision_dias__c = 5,
                                              BI_Fecha_de_vigencia__c = date.today().addDays(8));
        
        insert opptyLocal;
        
        List<NE__Order__c> lstQuotes = new List<NE__Order__c>();
        
		NE__Order__c order1 = new NE__Order__c(RecordTypeId = MAP_NAME_RT.get('Quote'),
        										 NE__OptyId__c = opptyLocal.Id,
        										 NE__OrderStatus__c = 'Revised',
        										 NE__AccountId__c = account.Id,
        										 NE__Recurring_Charge_Total__c = 100,
        										 NE__One_Time_Fee_Total__c = 200);
												   
		lstQuotes.add(order1);
		
		Test.startTest();
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(opptyLocal);
		BI_O4_OpportunityQuotesController oqc = new BI_O4_OpportunityQuotesController(stdController);
		
				
		try
		{
			List<NE__Order__c> quotes = oqc.getQuotes();
		}
		catch(Exception e)
		{
			System.assert(e.getMessage().contains('test'));
		}		

		BI_TestUtils.throw_exception = false;
		
		insert lstQuotes;
		
		stdController = new ApexPages.StandardController(opptyLocal);
		oqc = new BI_O4_OpportunityQuotesController(stdController);
		List<NE__Order__c> quotes = oqc.getQuotes();
		
		Test.stopTest();
		
    }
}