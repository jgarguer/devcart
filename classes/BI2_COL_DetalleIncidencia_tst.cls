/**
********************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          BI_EN Fase 2 Colombia
* @name             BI2_COL_DetalleIncidencia_tst
* @description      Test class that covers the following classes/triggers:  
*                                                                           
*                   Based on "BIIN_Detalle_Incidencia_TEST" class.
* @dependencies     
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------
* @version   1.0   2017-02-24   Antonio Torres (AT)           Initial version.
********************************************************************************************************
**/

@isTest(seeAllData = false)
public class BI2_COL_DetalleIncidencia_tst {
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Detalle_Incidencia_TEST';
    
    @testSetup static void dataSetup() {
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;
        List<Account> acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];

        List<RecordType> rT = [SELECT Id, Name FROM Recordtype WHERE Name = 'Solicitud Incidencia Técnica'];
        Case caseTest = new Case(RecordTypeId = rT.get(0).Id, AccountId = acc.get(0).Id, BI_Otro_Tipo__c = 'test', Status = 'Assigned', 
                                    Priority = 'Media', Type = 'Reclamo', Subject = 'Test', CurrencyIsoCode = 'ARS', Origin = 'Portal Cliente', 
                                    BI_Confidencial__c = false, Reason = 'Reclamos administrativos', Description = 'testdesc',
                                    BI_Id_del_caso_Legado__c = 'INC000000022352', BIIN_Tiempo_Neto_Apertura__c = 324234);
        insert caseTest;
        //Fin creacion de caso
    }

    @isTest static void BI2_COL_DetalleIncidencia_tst() {
        BI_TestUtils.throw_exception=false;
        
        Case caseTest = [SELECT Id, RecordTypeId, BI_Otro_Tipo__c, Status, Priority, Type, Subject, CurrencyIsoCode, Origin, BI_Confidencial__c, 
                                Reason, Description, BI_Id_del_caso_Legado__c, BIIN_Tiempo_Neto_Apertura__c
                            FROM Case WHERE BI_Id_del_caso_Legado__c =: 'INC000000022352'];

        Test.startTest();
        PageReference p = Page.BI2_COL_DetalleIncidencia_pag;
        p.getParameters().put('Idcaso', caseTest.Id);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caseTest);
        BI2_COL_DetalleIncidencia_ctr ceController = new BI2_COL_DetalleIncidencia_ctr(controller);
        ceController.attachName    = '0-#-0';
        ceController.worklogId     = 'LogId';
        ceController.posicionWI    = 'Pos01';
        ceController.MasDeTres     = false;            
        ceController.nombreContact = '';    
        ceController.nombreOwner   = '';  
        ceController.casoNota      = 'Hola';
        ceController.rutaBase      = '';
        ceController.IdAdjunto     = '';
        ceController.nombre1       = '';
        ceController.nombre2       = '';
        ceController.nombre3       = '';
        
        ceController.closePopup();
        ceController.showPopup();
        
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerAnexo'));
        ceController.getAttBody();
        ceController.crearWIRoD();
        
        PageReference Gotomenu = ceController.Gotomenu();
        ceController.ClosePopupError();
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerTicket'));
        ceController.loadData();
        ceController.doCancel();
        ceController.GuardarCampoConfidencial();
        ceController.ModificarCampoConfidencial();
        Test.stopTest();
    }
}