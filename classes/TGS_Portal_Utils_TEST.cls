@isTest(isParallel=true)
public without sharing class TGS_Portal_Utils_TEST {

    static String profileRunAs = 'TGS System Administrator';
    static Contact c;
    static Case caseT;
    static Account holding;
    static Account customerCountry;
    static Account legalEntity;
    static Account businessUnit;
    static Account costCenter;
    static NE__Product__c product;
    static NE__OrderItem__c orderItem;


////////Dummy Portal User    
    enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    
    static testmethod void usertest() {
        User pu = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.assert([select isPortalEnabled 
                         from user 
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');       
        
        System.RunAs(pu) {
            System.assert([select isPortalEnabled 
                             from user 
                            where id = :UserInfo.getUserId()].isPortalEnabled, 
                          'User wasnt portal enabled within the runas block. ');
        }
    }
    
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert){
        return getPortalUser(portalType,userWithRole,doInsert,3);        
    }
    
    //Se agrega el level de account que se va a asignar al Contact
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert,integer level) {
    	BI_TestUtils.throw_exception=false;
        /* Make sure the running user has a role otherwise an exception 
           will be thrown. */
        if(userWithRole == null) {   
            
            if(UserInfo.getUserRoleId() == null) {

                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                String nRandom = String.valueOf((Integer)(Math.random()*100));
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole'+nRandom+'@testorg.com',
                                    BI_Permisos__c = 'Empresas Platino', isActive = True);
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            System.assert(userWithRole.userRoleId != null, 
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }

        Account a;
        Contact cAux;
        System.runAs(userWithRole) {

            a = TGS_Dummy_Test_data.dummyHierarchy();
            //Database.insert(a);
            String accId;
            if(level==1){//Holding
                accId=a.Parent.parentId;
            }else if(level==2){// Customer Country
                accId=a.ParentId;
            }else if(level==3){// Legal entity
                accId=a.id;
            }else if(level==4){// BusinessUnit
                Account auxAcc=[SELECT Id FROM Account WHERE parentId=:a.id LIMIT 1];
                accId=auxAcc.Id;
            }else if(level==5){// Cost Center
                Account auxAcc=[SELECT Id FROM Account WHERE parent.parentId=:a.id LIMIT 1];
                accId=auxAcc.Id;
            }            
            cAux = new Contact(AccountId = accId,
                            lastname = 'lastname',
                            Email = 'contactTest@telefonica.com',
                            BI_Activo__c = true
                            );
            Database.insert(cAux);

        }
        
        /* Get any profile for the given type.*/
        Profile p = [select id 
                      from profile 
                     where usertype = :portalType.name() and Name='TGS Customer Community Plus'
                     limit 1];   
        
        String nRandom = String.valueOf((Integer)(Math.random()*100));
        String nRandom2 = String.valueOf((Integer)(Math.random()*100));
        String testemail = 'telefonicaTest'+nRandom+'@tefonica.com';
        User pu = new User(profileId = p.id, username = testemail+nRandom2, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = cAux.id, BI_Permisos__c = 'Empresas Platino',
                          isActive = True);
        
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }
//////////////////    
    
    
    
    private static void setConfiguration() {
        Test.startTest();
        
        legalEntity = TGS_Dummy_Test_Data.dummyHierarchy();        
        List<Account> listAcc = TGS_Portal_Utils.getLevel1(legalEntity.Id, 1);
        if(listAcc.size()>0){
            holding = listAcc[0];
        }
        listAcc = TGS_Portal_Utils.getLevel2(legalEntity.Id, 1);
        if(listAcc.size()>0){
            customerCountry = listAcc[0];
        }
        listAcc = TGS_Portal_Utils.getLevel4(legalEntity.Id, 1);
        if(listAcc.size()>0){
            businessUnit = listAcc[0];
            TGS_Dummy_Test_Data.dummyPuntoInstalacion(businessUnit.Id);
        }
        listAcc = TGS_Portal_Utils.getLevel5(legalEntity.Id, 1);
        if(listAcc.size()>0){
            costCenter = listAcc[0];
        }
        caseT = TGS_Dummy_Test_Data.dummyTicketCase();
        caseT.AccountId = legalEntity.Id;
        update caseT;
        c = TGS_Dummy_Test_Data.dummyContact(legalEntity.Id);
        product = TGS_Dummy_Test_Data.dummyCommercialProduct();
        orderItem = TGS_Dummy_Test_Data.dummyConfiguration();
        
        Test.stopTest();
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getAccount
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    04/11/2016              Guillermo Muñoz         Fix bugs on Deploy
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        static testMethod void getAccountTest() {
            /*User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
            userTest.BI_Permisos__c = 'TGS';
            update userTest;*/
            User userTest = [SELECT Id FROM User WHERE Profile.Name = 'TGS System Administrator' AND IsActive = true LIMIT 1];
            System.runAs(userTest){
                if (legalEntity==null){setConfiguration();}
                Account acc = TGS_Portal_Utils.getAccount(legalEntity.Id);
                System.assertEquals(legalEntity.Name, acc.Name);
            }
        }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getUserAccountTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        String auxName = [SELECT Contact.Account.Name FROM User WHERE ID=:userTest.Id LIMIT 1].Contact.Account.Name;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            
            Account acc = TGS_Portal_Utils.getUserAccount(userTest.Id);
            System.assertEquals(auxName, acc.Name);
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getAccountLevel
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    04/11/2016              Guillermo Muñoz         Fix bugs on deploy
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getAccountLevelTest() {
        
        	dotestAccountLevel(5);
        
    }
    
    static void dotestAccountLevel(integer level){
		User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true,level);
        /*Id aux = [SELECT ContactId FROM User WHERE id=:userTest.Id LIMIT 1].ContactId;
        Contact contactTest = [SELECT Id, AccountId FROM Contact WHERE Id = :aux LIMIT 1];*/
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            integer levelObtained = TGS_Portal_Utils.getAccountLevel(userTest.Id);
            System.assertEquals(levelObtained, level);
        }            
    }    
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getProduct
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getProductTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            List<NE__Product__c> listProd = TGS_Portal_Utils.getProduct(product.Id);
            System.assertEquals(product.Name, listProd[0].Name);
        }
    }

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the methods TGS_Portal_Utils.getLevelN, getLevel1, getLevel2, getLevel3, getLevel4, getLevel5
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getLevelTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            // Holding
            TGS_Portal_Utils.getLevelN(holding.Id, 1, 1);
            TGS_Portal_Utils.getLevelN(holding.Id, 1, 2);
            TGS_Portal_Utils.getLevelN(holding.Id, 1, 3);
            TGS_Portal_Utils.getLevelN(holding.Id, 1, 4);
            TGS_Portal_Utils.getLevelN(holding.Id, 1, 5);
            // Customer country
            TGS_Portal_Utils.getLevelN(customerCountry.Id, 1, 1);
            TGS_Portal_Utils.getLevelN(customerCountry.Id, 1, 2);
            TGS_Portal_Utils.getLevelN(customerCountry.Id, 1, 3);
            TGS_Portal_Utils.getLevelN(customerCountry.Id, 1, 4);
            TGS_Portal_Utils.getLevelN(customerCountry.Id, 1, 5);
            // Legal entity
            TGS_Portal_Utils.getLevelN(legalEntity.Id, 1, 1);
            TGS_Portal_Utils.getLevelN(legalEntity.Id, 1, 2);
            TGS_Portal_Utils.getLevelN(legalEntity.Id, 1, 3);
            TGS_Portal_Utils.getLevelN(legalEntity.Id, 1, 4);
            TGS_Portal_Utils.getLevelN(legalEntity.Id, 1, 5);
            // Business unit
            TGS_Portal_Utils.getLevelN(businessUnit.Id, 1, 1);
            TGS_Portal_Utils.getLevelN(businessUnit.Id, 1, 2);
            TGS_Portal_Utils.getLevelN(businessUnit.Id, 1, 3);
            TGS_Portal_Utils.getLevelN(businessUnit.Id, 1, 4);
            TGS_Portal_Utils.getLevelN(businessUnit.Id, 1, 5);
            // Cost center
            TGS_Portal_Utils.getLevelN(costCenter.Id, 1, 1);
            TGS_Portal_Utils.getLevelN(costCenter.Id, 1, 2);
            TGS_Portal_Utils.getLevelN(costCenter.Id, 1, 3);
            TGS_Portal_Utils.getLevelN(costCenter.Id, 1, 4);
            TGS_Portal_Utils.getLevelN(costCenter.Id, 1, 5);
            // Users
            /*TGS_Portal_Utils.getLevelN(portalUser.Id, 0, 1);
            TGS_Portal_Utils.getLevelN(portalUser.Id, 0, 2);
            TGS_Portal_Utils.getLevelN(portalUser.Id, 0, 3);
            TGS_Portal_Utils.getLevelN(portalUser.Id, 0, 4);
            TGS_Portal_Utils.getLevelN(portalUser.Id, 0, 5);*/
            //Exceptions
            TGS_Portal_Utils.getLevelN(legalEntity.Id, 1, 6);
        }
    }
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getCatalogItemsBySId
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getCatalogItemsBySIdTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            List<NE__OrderItem__c> listOrderItem = [SELECT Id, Name, NE__ProdId__c, NE__ProdId__r.Name FROM NE__OrderItem__c WHERE Id = :orderItem.Id];
            System.assertNotEquals( 0, listOrderItem.size());
            List<NE__Catalog_Item__c> listCatalogItem = TGS_Portal_Utils.getCatalogItemsBySId(listOrderItem[0].NE__ProdId__c);
            System.assertNotEquals( 0, listCatalogItem.size());
        }
    }
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getConfigurationItemsBySuId
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getConfigurationItemsBySuIdTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            NE__OrderItem__c ordItem = [SELECT Id, Name FROM NE__OrderItem__c WHERE Id = :orderItem.Id LIMIT 1];
            List<NE__OrderItem__c> listOrderItem = TGS_Portal_Utils.getConfigurationItemsBySuId(orderItem.Id);
            System.assertEquals( ordItem.Name, listOrderItem[0].Name);
        }
    }
    
    
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getRecordType
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getRecordTypeTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            List<RecordType> listRecord = TGS_Portal_Utils.getRecordType('Incident', 'Case');
            System.assertNotEquals( 0, listRecord.size());
        }
    }
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.initCase
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void initCaseTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            Case caseTest = new Case();
            caseTest = TGS_Portal_Utils.initCase(caseTest, 'Incident', 'CALL', userTest.Id);
        }
    }
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getBUList
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getBUListTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            Case caseTest = new Case(AccountId = legalEntity.Id);
            List<SelectOption> buList = TGS_Portal_Utils.getBUList(caseTest);
            System.assertNotEquals( 0, buList.size());
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getSiteListTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            List<SelectOption> siteList = TGS_Portal_Utils.getSiteList(businessUnit.Id);
            System.assertNotEquals( 0, siteList.size());
        }
    }
    
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.createCase
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createCaseTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            Case caseTest = new Case();
            PageReference pr = TGS_Portal_Utils.createCase(caseTest, 'CALL', businessUnit.Id);
        }
    }
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getService
            
     <Date>                 <Author>                <Change Description>
    29/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getServiceTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            List<NE__OrderItem__c> listOrderItem = TGS_Portal_Utils.getService(orderItem.Id);
            System.assertNotEquals( 0, listOrderItem.size());
        }
    }
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getKeyFamilyProperties
            
     <Date>                 <Author>                <Change Description>
    29/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getKeyFamilyPropertiesTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            Set<Id> setCI = new Set<Id>();
            setCI.add(orderItem.Id);
            List<NE__ProductFamilyProperty__c> listProdFamilyProp = TGS_Portal_Utils.getKeyFamilyProperties(setCI);
            System.assertNotEquals( 0, listProdFamilyProp.size());
        }
    }
    
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getAttributesByCI
            
     <Date>                 <Author>                <Change Description>
    29/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getAttributesByCITest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            Set<Id> setCI = new Set<Id>();
            setCI.add(orderItem.Id);
            Map<Id, List<NE__Order_Item_Attribute__c>> listOrderItemAttr = TGS_Portal_Utils.getAttributesByCI(setCI);
            System.assertNotEquals( 0, listOrderItemAttr.get(orderItem.Id).size());
        }
    }
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getFilteredInstalledServices
            
     <Date>                 <Author>                <Change Description>
    29/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getFilteredInstalledServicesTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            //if (legalEntity==null){setConfiguration();}
            setConfiguration();
            List<NE__Catalog_Item__c> filteredCatalogItems = new List<NE__Catalog_Item__c>();
            NE__Catalog_Item__c catalogItem = [SELECT Id, NE__ProductId__c FROM NE__Catalog_Item__c WHERE Id = :orderItem.NE__CatalogItem__c LIMIT 1];
            filteredCatalogItems.add(catalogItem);
            Map<Id, NE__OrderItem__c> listOrderItem = TGS_Portal_Utils.getFilteredInstalledServices(filteredCatalogItems, userTest.Id);
        }
    }
    
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getFilteredVisibleCatalogItems
            
     <Date>                 <Author>                <Change Description>
    29/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getFilteredVisibleCatalogItemsTest() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            //if (legalEntity==null){setConfiguration();}
            setConfiguration();
            Set<Id> setCI = new Set<Id>();
            setCI.add(orderItem.Id);
            List<NE__Catalog_Item__c> listVisibleCatalogItems = TGS_Portal_Utils.getFilteredVisibleCatalogItems('', userTest.Id);
            List<TGS_Portal_User_Configuration_Product__c> myConfProd = new List<TGS_Portal_User_Configuration_Product__c>();
        }
    }
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the method TGS_Portal_Utils.getServiceDescription
            
     <Date>                 <Author>                <Change Description>
    29/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getServiceDescriptionTest(){
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            String description = TGS_Portal_Utils.getServiceDescription( product.Id, thisUser.Id);
            System.assertEquals( '', description);
            Note n = new Note(parentId = product.Id, Title = 'Descripcion_ES', Body = 'Descripcion ES');
            insert n;
            n = new Note(parentId = product.Id, Title = 'Descripcion_EN', Body = 'Descripcion EN');
            insert n;
            n = new Note(parentId = product.Id, Title = 'Descripcion_DE', Body = 'Descripcion DE');
            insert n;
            n = new Note(parentId = product.Id, Title = 'Descripcion_PT', Body = 'Descripcion PT');
            insert n;
            n = new Note(parentId = product.Id, Title = 'Descripcion_FR', Body = 'Descripcion FR');
            insert n;
            thisUser.LanguageLocaleKey = 'es';
            update thisUser;
            description = TGS_Portal_Utils.getServiceDescription( product.Id, thisUser.Id);
            System.assertNotEquals( '', description);
            thisUser.LanguageLocaleKey = 'fr';
            update thisUser;
            description = TGS_Portal_Utils.getServiceDescription( product.Id, thisUser.Id);
            System.assertNotEquals( '', description);
            thisUser.LanguageLocaleKey = 'de';
            update thisUser;
            description = TGS_Portal_Utils.getServiceDescription( product.Id, thisUser.Id);
            System.assertNotEquals( '', description);
            thisUser.LanguageLocaleKey = 'pt_PT';
            update thisUser;
            description = TGS_Portal_Utils.getServiceDescription( product.Id, thisUser.Id);
            System.assertNotEquals( '', description);
            thisUser.LanguageLocaleKey = 'en_US';
            update thisUser;
            description = TGS_Portal_Utils.getServiceDescription( product.Id, thisUser.Id);
            System.assertNotEquals( '', description);
        }
    }
    
    static testMethod void getConfigurationItemsBySuId2Test() {
        User userTest = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        userTest.BI_Permisos__c = 'TGS';
        update userTest;
        System.runAs(userTest){
            if (legalEntity==null){setConfiguration();}
            NE__OrderItem__c ordItem = [SELECT Id, Name FROM NE__OrderItem__c WHERE Id = :orderItem.Id LIMIT 1];
            List<NE__OrderItem__c> listOrderItem = TGS_Portal_Utils.getConfigurationItemsBySuId2(orderItem.Id);
            System.assertEquals( ordItem.Name, listOrderItem[0].Name);
        }
    }
}