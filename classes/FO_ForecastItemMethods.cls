public without sharing class FO_ForecastItemMethods {
    /***************************************************************
     * Author:Alfonso Alvarez
     * Description: Update Forecast Commit and/or Forecast Upside in parent Forecast related.
     *              En caso de borrado de ForecastItem, solo se recibe una lista de objetos borrados en el parámetro old. El parámetro news debe ir a null
     * IN: list of new ForecastItem, 
     * IN: list of old ForecastItem
     * 
     * *************************************************************/
   public static void updateForecastCommitUpside(List<FO_Forecast_Item__c> news, List<FO_Forecast_Item__c> olds){ 
     
     System.debug('FO_ForecastItemMethods.updateForecastCommitUpside() BEGIN');  
     List<Id> lst_fo = new List<Id>();
     Map<Id,FO_Forecast_Item__c> mp_olds;
     Map<Id,FO_Forecast_Item__c> mp_foi_actualizar;  
     Map<Id,double> mp_fo_commit=new Map<Id,double>(); // Mapa que almacena para cada Forecast el valor a sumar o restar acumulado de commit
     Map<Id,double> mp_fo_upside=new Map<Id,double>(); // Mapa que almacena para cada Forecast el valor a sumar o restar acumulado de upside
	 List<CurrencyType> lst_cr = [Select IsoCode,ConversionRate from CurrencyType where isActive=true];
     Map<String,double> mp_curr = new Map<String,double>();
        List<String> lst_idFo = new List<String>();
     for(CurrencyType curr:lst_cr)
         mp_curr.put(curr.IsoCode,curr.ConversionRate);
     for(FO_Forecast_Item__c foi_old : olds){
         lst_idFo.add(foi_old.FO_Forecast_Padre__c);
     }
     Map<Id,FO_Forecast__c> mp_foCurr = new Map<Id,FO_Forecast__c>([Select Id,CurrencyIsoCode from FO_Forecast__c where Id in :lst_idFo]);
     if(news==null){ // Se están borrando Forecast Item.
        
         
         for(Integer i=0;i<olds.size();i++){
             FO_Forecast_Item__c foi_old=(FO_Forecast_Item__c)olds.get(i);
             String currFo=mp_foCurr.get(foi_old.FO_Forecast_Padre__c).CurrencyIsoCode;
             double value = foi_old.FO_NAV__c;
             if(foi_old.CurrencyIsoCode.equals(currFo)==false){
                 value=value*mp_curr.get(currFo)/mp_curr.get(foi_old.CurrencyIsoCode);
             }
             lst_fo.add(foi_old.FO_Forecast_Padre__c) ;
             if( foi_old.FO_Tipo_Forecast__c == 'Commit'){ //RESTAR COMMIT
                System.debug('AJAE: Restar en Commit para Forecast: '+foi_old.FO_Forecast_Padre__c);
                if(mp_fo_commit.get(foi_old.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit
                  mp_fo_commit.put(foi_old.FO_Forecast_Padre__c, - value);
                }else{
                  double valor_commit= mp_fo_commit.get(foi_old.FO_Forecast_Padre__c);
                  valor_commit=valor_commit - value;
                  mp_fo_commit.put(foi_old.FO_Forecast_Padre__c, valor_commit); 
                } 
             }//if( foi_old.FO_Tipo_Forecast__c == 'Commit'){
             //RESTAR UPSIDE
             if (foi_old.FO_Tipo_Forecast__c=='Upside') {
               System.debug('AJAE: Restar en Upside para Forecast: '+foi_old.FO_Forecast_Padre__c);
               if(mp_fo_upside.get(foi_old.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit
                 mp_fo_upside.put(foi_old.FO_Forecast_Padre__c, - value);
               }else{
                 double valor_upside= mp_fo_upside.get(foi_old.FO_Forecast_Padre__c);
                 valor_upside=valor_upside - value;
                 mp_fo_upside.put(foi_old.FO_Forecast_Padre__c,valor_upside); 
               }
             }
         }//for(Integer i=0;i<olds.size();i++){    
        
             
     }else { // Se están actualizando Forecast Item.
       for(Integer i=0;i<olds.size();i++){
         FO_Forecast_Item__c foi_new=(FO_Forecast_Item__c)news.get(i);
         FO_Forecast_Item__c foi_old=(FO_Forecast_Item__c)olds.get(i);
         if( foi_new.FO_Tipo_Forecast__c != foi_old.FO_Tipo_Forecast__c ){ // Comprobamos que se ha modificado el campo Tipo de Forecast en ForecastItem

           System.debug('AJAE: Forecast Item: '+ foi_new.Id+ ' valor antiguo: ' + foi_old.FO_Tipo_Forecast__c + ' valor nuevo: '+ foi_new.FO_Tipo_Forecast__c);
           lst_fo.add(foi_new.FO_Forecast_Padre__c) ; // Añadimos el Forecast Id para luego obtener los objetos a cambiar:
			String currFO = mp_foCurr.get(foi_old.FO_Forecast_Padre__c).CurrencyIsoCode;
           // Bloque que realiza la suma y/o la resta en commit y/o upside según los valores de forecast new y forecast old               
           double value =foi_new.FO_NAV__c;
             if(foi_new.CurrencyIsoCode.equals(currFO)==false){
                 value=value*mp_curr.get(currFO)/mp_curr.get(foi_new.CurrencyIsoCode);
             }
           //RESTAR COMMIT
           if (foi_old.FO_Tipo_Forecast__c=='Commit') {
             System.debug('AJAE: Restar en Commit para Forecast: '+foi_new.FO_Forecast_Padre__c);
             if(mp_fo_commit.get(foi_new.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit
               mp_fo_commit.put(foi_new.FO_Forecast_Padre__c, - value);
             }else{
               double valor_commit= mp_fo_commit.get(foi_new.FO_Forecast_Padre__c);
               valor_commit=valor_commit - value;
               mp_fo_commit.put(foi_new.FO_Forecast_Padre__c, valor_commit); 
             }
           }    
           //RESTAR UPSIDE
           if (foi_old.FO_Tipo_Forecast__c=='Upside') {
             System.debug('AJAE: Restar en Upside para Forecast: '+foi_new.FO_Forecast_Padre__c);
             if(mp_fo_upside.get(foi_new.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit
               mp_fo_upside.put(foi_new.FO_Forecast_Padre__c, - value);
             }else{
               double valor_upside= mp_fo_upside.get(foi_new.FO_Forecast_Padre__c);
               valor_upside=valor_upside -value;
               mp_fo_upside.put(foi_new.FO_Forecast_Padre__c, valor_upside); 
             }
           }
         //SUMAR COMMIT 
         if (foi_new.FO_Tipo_Forecast__c=='Commit') {
           System.debug('AJAE: Sumar en Commit para Forecast: '+foi_new.FO_Forecast_Padre__c);
           if(mp_fo_commit.get(foi_new.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit
             mp_fo_commit.put(foi_new.FO_Forecast_Padre__c,  value);
           }else{
             double valor_commit= mp_fo_commit.get(foi_new.FO_Forecast_Padre__c);
             valor_commit=valor_commit + value;
             mp_fo_commit.put(foi_new.FO_Forecast_Padre__c, valor_commit); 
           }
         }
         //SUMAR UPSIDE                
         if (foi_new.FO_Tipo_Forecast__c=='Upside') {
           System.debug('AJAE: Sumar en Upside para Forecast: '+foi_new.FO_Forecast_Padre__c);
           if(mp_fo_upside.get(foi_new.FO_Forecast_Padre__c)== null){ // Incluimos el Forecast con el valor a restar en el mapa de cambios de commit
             mp_fo_upside.put(foi_new.FO_Forecast_Padre__c, value);
           }else{
             double valor_upside= mp_fo_upside.get(foi_new.FO_Forecast_Padre__c);
             valor_upside=valor_upside + value;
             mp_fo_upside.put(foi_new.FO_Forecast_Padre__c, valor_upside); 
           }
         }

       } //if if( foi_new.FO_Tipo_Forecast__c != foi_old.FO_Tipo_Forecast__c ){ // Comprobamos que se ha modificado el campo Tipo de Forecast en ForecastItem
     }// for(Integer i=0;i<news.size();i++){
     }//if(news==null){    
     
     System.debug('AJAE: Lista de forecast :'+lst_fo);
     System.debug('AJAE: Cambios en commit:'+mp_fo_commit);
     System.debug('AJAE: Cambios en upside:'+mp_fo_upside);
               
     // Obtenermos el mapa de Forecast que hay que actualizar
     Map <Id,FO_Forecast__c> mp_fo=new map <Id,FO_Forecast__c> ([Select Id,FO_Commit_Pipeline__c,FO_Upside_Pipeline__c from FO_Forecast__c where Id in:lst_fo]);
     System.debug('AJAE: Forecast Mapa:'+mp_fo);
       
     //Recorremos la lista de forecast y vamos actualizando 
     for (Id idFo : lst_fo){
       FO_Forecast__c fo=mp_fo.get(idFo);
       if(fo.FO_Commit_Pipeline__c==null) //Inicializamos valor del forecast si está vacío
         fo.FO_Commit_Pipeline__c=0;
       if(fo.FO_Upside_Pipeline__c==null) //Inicializamos valor del forecast si está vacío
         fo.FO_Upside_Pipeline__c=0;
       if(mp_fo_commit.get(idFo)!=null)
         fo.FO_Commit_Pipeline__c+=mp_fo_commit.get(idFo);
       if(mp_fo_upside.get(idFo)!=null)
         fo.FO_Upside_Pipeline__c+=mp_fo_upside.get(idFo);
     }//for (Id idFo : lst_fo){
     System.debug('AJAE: Forecast Mapa actualizado'+ mp_fo);
     upsert(mp_fo.values());
     System.debug('FO_ForecastItemMethods.updateForecastCommitUpside() BEGIN');  

    }//   updateForecastCommitUpside
       
     /***************************************************************
     * Author:Alfonso Alvarez
     * Description: Update CloseDate in Opty. If new Date is out of the actual Period, Forecast Item will be deleted. 
     * Ejecutado en Before Update
     * IN: list of new ForecastItem, 
     * IN: list of old ForecastItem
     * 
     * *************************************************************/
   public static void updateForecastCloseDate(List<FO_Forecast_Item__c> news, List<FO_Forecast_Item__c> olds){
       System.debug('FO_ForecastItemMethods.updateForecastCloseDate() BEGIN');   
       List<Id> lst_opty = new List<Id>();
       List<Id> lst_fo = new List<Id>();
       List<Id> lst_foi_id_delete= new List <Id>();
       //Obtenemos listado de ID de oportunidades a actualizar y de Forecast implicados
       for(FO_Forecast_Item__c foi :news){
         lst_opty.add(foi.FO_Opportunity__c);
         lst_fo.add(foi.FO_Forecast_Padre__c);  
       }//for(FO_Forecast_Item__c foi :news){
       //Obtenemos las oportunidades
       Map <Id,Opportunity> mp_opty=new map <Id,Opportunity> ([Select Id,CloseDate from Opportunity where Id in:lst_opty ]);
       //Obtenemos los Forecast
       Map <Id,FO_Forecast__c> mp_fo=new map <Id,FO_Forecast__c> ([Select Id,FO_Periodo_Fecha_Inicio__c,FO_Periodo_Fecha_Fin__c from FO_Forecast__c where Id in:lst_fo]);
       // Mover las fechas de las opty
       for(FO_Forecast_Item__c foi :news){
         Opportunity opty=(Opportunity)mp_opty.get(foi.FO_Opportunity__c);
         opty.CloseDate=foi.FO_CloseDate__c;
         // averiguar si la fecha nueva de forecast está fuera del periodo
         Date fecha_inicio=(mp_fo.get(foi.FO_Forecast_Padre__c).FO_Periodo_Fecha_Inicio__c);
         Date fecha_fin=(mp_fo.get(foi.FO_Forecast_Padre__c).FO_Periodo_Fecha_Fin__c);
         Date fecha_foi=foi.FO_CloseDate__c;
         // Si lo está. Hay que borrar el Forecast Item
         if((fecha_foi<fecha_inicio)||(fecha_foi>fecha_fin)){
           lst_foi_id_delete.add(foi.Id);                          
         }
         // Si no lo está. No hacer nada.
       }//for(FO_Forecast_Item__c foi :news)
       // Actualizamos las oportunidades
       upsert(mp_opty.values());
       // Borramos los Foi
       if(lst_foi_id_delete.size()>0){
           List <FO_Forecast_Item__c> lst_foi_delete =[Select Id from FO_Forecast_Item__c where Id in :lst_foi_id_delete ];
           System.debug('AJAE: Se deben borrar los siguientes foi: '+ lst_foi_delete);
           delete lst_foi_delete;
       }
         
       System.debug('FO_ForecastItemMethods.updateForecastCloseDate() END');     
   }//public static void updateForecastCloseDate
}