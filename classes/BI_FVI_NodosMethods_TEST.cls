@isTest
private class BI_FVI_NodosMethods_TEST {

    public static List<BI_FVI_Nodos__c> lstNAGold = new List<BI_FVI_Nodos__c>();
    public static List<BI_FVI_Nodos__c> lstNAGnew = new List<BI_FVI_Nodos__c>();
    public static Id idProfile =BI_DataLoad.searchAdminProfile();
    public static List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García
    Company:       NEAborda
    Description:   Método para cargar datos de prueba

    History: 

    <Date>                  <Author>                <Change Description>
    06/05/2016              Antonio Masferrer       Initial Version  
    01/02/2007              Pedro Párraga           Increase in coverage 
    02/02/2017              Adrián Caro             test errors solved 
    20/09/2017               Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private static void dataLoad() {
    
        List<RecordType> lst_rt = [SELECT Id,DeveloperName 
                                   FROM RecordType 
                                   WHERE DeveloperName = 'BI_FVI_NCP'
                                   OR DeveloperName = 'BI_FVI_NAV'
                                   OR DeveloperName = 'BI_FVI_NAB'
                                   OR DeveloperName = 'BI_FVI_NAT'
                                   OR DeveloperName = 'BI_FVI_NAG'];

        
        Id rt_NCP,rt_NAT,rt_NAV,rt_NAB,rt_NAG;

        system.runAs(lst_user[0]){
        
        for(RecordType rt : lst_rt)
        {
            if(rt.DeveloperName == 'BI_FVI_NCP'){rt_NCP = rt.Id;}
            else if(rt.DeveloperName == 'BI_FVI_NAV'){rt_NAV = rt.Id;}
            else if(rt.DeveloperName == 'BI_FVI_NAT'){rt_NAT = rt.Id;}
            else if(rt.DeveloperName == 'BI_FVI_NAG'){rt_NAG = rt.Id;}
            else{rt_NAB = rt.Id;}
        }

        //NODOS NCP, NAV y NAT
        List<BI_FVI_Nodos__c> lst_nodos = new List<BI_FVI_Nodos__c>();

        BI_FVI_Nodos__c nodo_ncp = new BI_FVI_Nodos__c(
            Name = 'NCP_TEST', 
            RecordTypeId = rt_NCP,
            BI_FVI_Pais__c = Label.BI_Peru
        );

        insert nodo_ncp;

        BI_FVI_Nodos__c ncp = [SELECT Id FROM BI_FVI_Nodos__c WHERE Name = 'NCP_TEST'];

        lst_nodos.add(new BI_FVI_Nodos__c(Name = 'NAV_TEST', RecordTypeId = rt_NAV, BI_FVI_NodoPadre__c = ncp.Id, BI_FVI_Pais__c = Label.BI_Peru));
        lst_nodos.add(new BI_FVI_Nodos__c(Name = 'NAT_TEST', RecordTypeId = rt_NAT, BI_FVI_NodoPadre__c = ncp.Id, BI_FVI_Pais__c = Label.BI_Peru));

        insert lst_nodos;
        system.debug('lst_nodos --->> ' + lst_nodos[1]);

        //NODOS DE BARBECHO
        List<BI_FVI_Nodos__c> lst_NAB = new List<BI_FVI_Nodos__c>();
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_ENE', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_FEB', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_MAR', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_ABR', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_MAY', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_JUN', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_JUL', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_AGO', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_SEP', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_OCT', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_NOV', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        lst_NAB.add(new BI_FVI_Nodos__c(Name = 'NAB_DEC', RecordTypeId = rt_NAB, BI_FVI_Pais__c = Label.BI_Peru));
        insert lst_NAB;


        BI_FVI_NodosMethods.ColaborarNodosNAB(lst_NAB, lst_NAB);
         
        //LOVS
        List<NE__Lov__c> lst_lov = new List<NE__Lov__c>();
        NE__Lov__c lov1 = new NE__Lov__c(
            Name = 'N_DIAS_BARBECHO_SALTO_TEMPORAL',
            NE__Active__c = true,
            NE__Type__c = 'NODOS',
            NE__Value1__c = '60',
            NE__Value2__c = '2',
            NE__Value3__c = '60',
            Country__c = Label.BI_Venezuela
        );

        NE__Lov__c lov2 = new NE__Lov__c(
            Name = 'N_DIAS_BARBECHO_SALTO_TEMPORAL',
            NE__Active__c = true,
            NE__Type__c = 'NODOS',
            NE__Value1__c = '90',
            NE__Value2__c = '8',
            NE__Value3__c = '30',
            Country__c = Label.BI_Peru
        );

        NE__Lov__c lov3 = new NE__Lov__c(
            Name = 'N_CLIENTES_VOLATIL_TEMPORAL_ADQ',
            NE__Active__c = true,
            Country__c = Label.BI_Peru,
            NE__Type__c = 'NODOS',
            NE__Value1__c = '10',
            NE__Value2__c = '10',
            NE__Value3__c = '10'
        );

        NE__Lov__c lov4 = new NE__Lov__c(
            Name = 'N_CLIENTES_VOLATIL_TEMPORAL_ADQ',
            NE__Active__c = true,
            Country__c = Label.BI_Venezuela,
            NE__Type__c = 'NODOS',
            NE__Value1__c = '20',
            NE__Value2__c = '20',
            NE__Value3__c = '20'
        );

        lst_lov.add(lov1);
        lst_lov.add(lov2);
        lst_lov.add(lov3);
        lst_lov.add(lov4);

        insert lst_lov;

        //CUENTAS
         List<Account> lst_acc = new List<Account>();
        
         //MUEVE AL NODO BARBECHO ACTUAL [PERU]
        Account acc_PER_actual1 = new Account(
        Name = 'TestNodos1',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 95,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
         BI_Segment__c = 'test',
          BI_Subsegment_Regional__c = 'test',
         BI_Territory__c = 'test'
        );

        

        //MUEVE AL NODO BARBECHO ACTUAL [VENEZUELA]
        Account acc_VEN_actual1 = new Account(
        Name = 'TestNodos2',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 65,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
                BI_Territory__c = 'test'

        );

        //MUEVE AL NODO BARBECHO ACTUAL + SALTO [PERU]
        Account acc_PER_salto1 = new Account(
        Name = 'TestNodos3',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today(),
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
                BI_Territory__c = 'test'

        );

        //MUEVE AL NODO BARBECHO ACTUAL + SALTO [VENEZUELA]
        Account acc_VEN_salto1 = new Account(
        Name = 'TestNodos4',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 25,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
                BI_Territory__c = 'test'

        );

        lst_acc.add(acc_PER_actual1);
        lst_acc.add(acc_VEN_actual1);
        lst_acc.add(acc_PER_salto1);
        lst_acc.add(acc_VEN_salto1);

        
        insert lst_acc;

        system.debug('lst_acc insert--->> ' + lst_acc);

        lst_nodos = [SELECT Id, Name, RecordType.Name FROM BI_FVI_Nodos__c WHERE Name LIKE '%_TEST' AND (RecordTypeId =: rt_NAT OR RecordTypeId =: rt_NAV)];
        system.debug('lst_nodos--->> ' + lst_nodos);
        lst_acc = [SELECT Id ,Name, BI_FVI_Nodo__r.Name, BI_FVI_Nodo__r.RecordType.Name, BI_Country__c FROM Account WHERE Name LIKE '%TestNodos%'];
        system.debug('lst_acc--->> ' + lst_acc);
            
        //NODOS CLIENTES        
        insert new BI_FVI_Nodo_Cliente__c(BI_FVI_IdCliente__c = lst_acc[0].Id , BI_FVI_IdNodo__c = lst_nodos[0].Id);
        insert new BI_FVI_Nodo_Cliente__c(BI_FVI_IdCliente__c = lst_acc[1].Id , BI_FVI_IdNodo__c = lst_nodos[0].Id);
        //insert new BI_FVI_Nodo_Cliente__c(BI_FVI_IdCliente__c = lst_acc[2].Id , BI_FVI_IdNodo__c = lst_nodos[0].Id);
        //insert new BI_FVI_Nodo_Cliente__c(BI_FVI_IdCliente__c = lst_acc[3].Id , BI_FVI_IdNodo__c = lst_nodos[0].Id);


        //BI_FVI_Nodos__c objNAG = new BI_FVI_Nodos__c();
        //    objNAG.Name = 'NAG_Prueba';
        //    //objNAG.BI_FVI_Asignar_a__c = objUser.Id;
        //    objNAG.BI_FVI_Pais__c = Label.BI_Peru;
        //    objNAG.RecordTypeId = rt_NAG;

        //    insert objNAG;
        //    system.debug('objNAG@@@--->' + objNAG);
    }
  }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García
    Company:       NEAborda
    Description:   Método para testear la tarea programada en la clase BI_FVI_Nodos_JOB

    History: 

    <Date>                  <Author>                <Change Description>
    06/05/2016              Antonio Masferrer       Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  @isTest static void execute_TEST() {
    
       String cron_exp = '0 0 0 15 3 ? 2022';
    
    Test.startTest();

      String jobId = System.schedule('ScheduleApexClassTest', cron_exp, new BI_FVI_Nodos_JOB());

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id =: jobId];

      System.assertEquals(cron_exp,ct.CronExpression);

      System.assertEquals(0, ct.TimesTriggered);

      System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
      
    Test.stopTest();
  }
  

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García
    Company:       NEAborda
    Description:   Método para testear la tara programada en la clase BI_FVI_Nodos_JOB

    History: 

    <Date>                  <Author>                <Change Description>
    06/05/2016              Antonio Masferrer       Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  @isTest static void asignaNodosBarbechoSchedule_TEST() {

    BI_TestUtils.throw_exception = false;
    
    User user = [select Id from User where Id = :UserInfo.getUserId()];
    
    system.runAs(user){

        Test.startTest();

            dataLoad();

        Test.stopTest();

        Map<Integer,String> monthMap = new Map<Integer,String>();
            
        monthMap.put(1 , 'NAB_ENE');
        monthMap.put(2 , 'NAB_FEB');
        monthMap.put(3 , 'NAB_MAR');
        monthMap.put(4 , 'NAB_ABR');
        monthMap.put(5 , 'NAB_MAY');
        monthMap.put(6 , 'NAB_JUN');
        monthMap.put(7 , 'NAB_JUL');
        monthMap.put(8 , 'NAB_AGO');
        monthMap.put(9 , 'NAB_SEP');
        monthMap.put(10 , 'NAB_OCT');
        monthMap.put(11 , 'NAB_NOV');
        monthMap.put(12 , 'NAB_DEC');

        Date fecha_actual = Date.today();
          Integer mes_actual = fecha_actual.month();

        String nombreNodoActual = monthMap.get(mes_actual);
        String nombreNodoSaltoPeru = monthMap.get(mes_actual + 3);
        String nombreNodoSaltoVenezuela = monthMap.get(mes_actual + 2);
            
        List<Account> acc = [SELECT Id,Name, BI_FVI_Nodo__r.Name, BI_Country__c FROM Account WHERE Name LIKE 'TestNodos%' ];
        Set<Id> id_acc = new Set<Id>();
        
        for(Account a : acc){
            system.debug(a);
            id_acc.add(a.Id);
        }

        List<BI_FVI_Nodo_Cliente__c> lst_nc =[SELECT Id, BI_FVI_IdCliente__r.Name , BI_FVI_IdNodo__r.Name FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdCliente__c IN:  id_acc];

        for(BI_FVI_Nodo_Cliente__c nc : lst_nc){
            system.debug('·_· BI_FVI_Nodo_Cliente__c (Antes): '+nc);
        }

        BI_FVI_NodosMethods.asignaNodosBarbechoSchedule_Countries(null);

            
        lst_nc =[SELECT Id, BI_FVI_IdCliente__r.Name , BI_FVI_IdNodo__r.Name FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdCliente__c IN:  id_acc];
        
        for(BI_FVI_Nodo_Cliente__c nc : lst_nc){
            system.debug('·_· BI_FVI_Nodo_Cliente__c (Despues): '+nc);
        }
            
          /*System.assertEquals(acc[0].BI_FVI_Nodo__r.Name, nombreNodoActual);
          System.assertEquals(acc[1].BI_FVI_Nodo__r.Name, nombreNodoActual);
           System.assertEquals(acc[2].BI_FVI_Nodo__r.Name, nombreNodoSaltoPeru);
           System.assertEquals(acc[3].BI_FVI_Nodo__r.Name, nombreNodoSaltoVenezuela);*/
        BI_TestUtils.throw_exception = true;
    }
  }

  /*------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       NEAborda
    Description:   Método para enviar NAG a NAV

    History: 

    <Date>                  <Author>                <Change Description>
    26/05/2016              Geraldine Pérez Montes       Initial Version     
    ----------------------------------------------------------------------*/ 
  @isTest static void NAGtoNAV_TEST() {
    BI_TestUtils.throw_exception = false;
    
    
    Id rt_NAG, rt_NAV, rt_NCP, rt_NAT;

    List<RecordType> lst_rt = [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName = 'BI_FVI_NAG' OR DeveloperName = 'BI_FVI_NAV' OR DeveloperName = 'BI_FVI_NCP' OR DeveloperName = 'BI_FVI_NAT'];

           
    for(RecordType objRecordType : lst_rt)
    {
       if(objRecordType.DeveloperName == 'BI_FVI_NAG'){rt_NAG = objRecordType.Id;}
       else if(objRecordType.DeveloperName == 'BI_FVI_NCP'){rt_NCP = objRecordType.Id;}
       else if(objRecordType.DeveloperName == 'BI_FVI_NAT'){rt_NAT = objRecordType.Id;}
       else {rt_NAV = objRecordType.Id;}
        
    }

    User objUser = [select Id, name, BI_Permisos__c from User Where Id = :UserInfo.getUserId()];

    system.runAs(lst_user[0])
    {
        Test.startTest();
        dataLoad();

        BI_FVI_Nodos__c objNCP = new BI_FVI_Nodos__c();
        objNCP.Name = 'NCP_Prueba'; 
        objNCP.RecordTypeId = rt_NCP;
        objNCP.BI_FVI_Pais__c = Label.BI_Peru;       

        insert objNCP;
        
        // BI_FVI_Nodos__c ncp = [SELECT Id FROM BI_FVI_Nodos__c WHERE Name = 'NCP_Prueba'];

        List<BI_FVI_Nodos__c> lstNAT = new List<BI_FVI_Nodos__c>();

            lstNAT.add(new BI_FVI_Nodos__c(Name = 'NAT_Prueba', RecordTypeId = rt_NAT, BI_FVI_NodoPadre__c = objNCP.id, BI_FVI_Pais__c = Label.BI_Peru));
            lstNAT.add(new BI_FVI_Nodos__c(Name = 'NAT_Prueba1', RecordTypeId = rt_NAT, BI_FVI_NodoPadre__c = objNCP.id, BI_FVI_Pais__c = Label.BI_Peru));
        
         insert lstNAT;
        
         List<Account> lstAcc = new List<Account>();
        
        Account objAcc = new Account(
        Name = 'TestNodos1',
        BI_FVI_FechaHoraUltimaVisita__c = Date.today() - 95,
        BI_Country__c = Label.BI_Peru,
        BI_FVI_Nodo__c = lstNAT[0].Id,
        BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
                BI_Territory__c = 'test');
        
        lstAcc.add(objAcc);

        insert lstAcc;

        List<BI_FVI_Nodos__c> lstNAVold = new List<BI_FVI_Nodos__c>();

            lstNAVold.add(new BI_FVI_Nodos__c(Name = 'NAV_Prueba', RecordTypeId = rt_NAV, BI_FVI_NodoPadre__c = objNCP.id, BI_FVI_Pais__c = Label.BI_Peru));
            lstNAVold.add(new BI_FVI_Nodos__c(Name = 'NAV_Prueba1', RecordTypeId = rt_NAV, BI_FVI_NodoPadre__c = objNCP.id, BI_FVI_Pais__c = Label.BI_Peru));
        
         insert lstNAVold;

        
            lstNAGold.add(new BI_FVI_Nodos__c(Name = 'NAG_Prueba', RecordTypeId = rt_NAG, BI_FVI_Asignar_a__c = lstNAVold[0].id, BI_FVI_Pais__c = Label.BI_Peru));
            lstNAGold.add(new BI_FVI_Nodos__c(Name = 'NAG_Prueba1', RecordTypeId = rt_NAG, BI_FVI_Asignar_a__c = lstNAVold[1].id, BI_FVI_Pais__c = Label.BI_Peru));
        insert lstNAGold;

        List<BI_FVI_Nodos__c> lstNAVnew = new List<BI_FVI_Nodos__c>();

            lstNAVnew.add(new BI_FVI_Nodos__c(Name = 'NAV_Prueba2', RecordTypeId = rt_NAV, BI_FVI_NodoPadre__c = objNCP.id, BI_FVI_Pais__c = Label.BI_Peru));
            lstNAVnew.add(new BI_FVI_Nodos__c(Name = 'NAV_Prueba3', RecordTypeId = rt_NAV, BI_FVI_NodoPadre__c = objNCP.id, BI_FVI_Pais__c = Label.BI_Peru));
        
         insert lstNAVnew;

        
            lstNAGnew.add(new BI_FVI_Nodos__c(Name = 'NAG_Prueba2', RecordTypeId = rt_NAG, BI_FVI_Asignar_a__c = lstNAVnew[0].id, BI_FVI_Pais__c = Label.BI_Peru));
            lstNAGnew.add(new BI_FVI_Nodos__c(Name = 'NAG_Prueba3', RecordTypeId = rt_NAG, BI_FVI_Asignar_a__c = lstNAVnew[1].id, BI_FVI_Pais__c = Label.BI_Peru));
        insert lstNAGnew;
       
          BI_FVI_NodosMethods.NAGtoNAV(lstNAGnew, lstNAGold );  
          BI_FVI_NodosMethods.NodoShare(lstNAGnew);
          BI_FVI_NodosMethods.changeOwnerAccounts(lstNAGnew, lstNAGnew);
            
        Test.stopTest();        

    }
  }
  /*************************************************
    Author: Javier Lopez Andradas
    Company: gCTIO
    Description: Base test to cober BI_FVI_NodosMethods.changeOwnerActivateNode

    <date>      <version>       <description>
    20/04/2018  1.0             Initial


  **************************************************/
  @isTest
  public static void changeOwnerTest(){

    Account acc = new Account(Name='TEST_JLA_Node_1',BI_Country__c=Label.BI_Peru, BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
    			BI_Territory__c = 'test');
    DataBase.SaveResult acc_svr = DataBase.insert (acc);
    if(acc_svr.isSuccess()==false){
    	for(Database.Error err : acc_svr.getErrors()) {
            System.debug('The following error has occurred.');                   
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Account fields that affected this error: ' + err.getFields());
        }
        System.assert(1==2);
    }
    RecordType rt = [Select Id from RecordType where DeveloperName='BI_FVI_NCP' limit 1];
    BI_FVI_Nodos__c node = new BI_FVI_Nodos__c(Name='JLA_Node_NCP_test_1',RecordTypeId=rt.Id,BI_FVI_Pais__c=Label.BI_Peru,OwnerId=lst_user[0].Id,BI_FVI_Activo__C=false);
    Database.SaveResult svr = Database.insert(node);
    if(svr.isSuccess()==false){
    	for(Database.Error err : svr.getErrors()) {
            System.debug('The following error has occurred.');                   
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Account fields that affected this error: ' + err.getFields());
        }
        System.assert(1==2);
    }
    BI_FVI_Nodo_Cliente__c ncd = new BI_FVI_Nodo_Cliente__c(BI_FVI_IdCliente__c=acc_svr.getId(),BI_FVI_IdNodo__c=svr.getId());
    try{
    	insert ncd;
    }catch(Exception e){
    	System.assert(1==2,e.getMessage());
    }
    //BI_FVI_Nodos__c nodeAux = new BI_FVI_Nodos__c(Name='JLA_Node_NCP_test_1',RecordTypeId=rt.Id,BI_FVI_Pais__c=Label.BI_Peru,OwnerId=lst_user[0].Id,BI_FVI_Activo__C=true,Id=svr.getId());
    node.Id = svr.getId();
    node.BI_FVI_Activo__c=true;
    //Test.startTest();
    TGS_User_Org__c userTGS = new TGS_User_Org__c(BI_FVI_Is_FVI__c=true);
    
    insert userTGS;
    System.debug('JLA_Start TEST');
    update node;
    System.debug('JLA_END TEST');
    System.assertEquals([Select OwnerId from Account where Id =:acc_svr.getId()].OwnerId,lst_user[0].Id);
    //Test.stopTest();
    
  }
  /*************************************************
    Author: Javier Lopez Andradas
    Company: gCTIO
    Description: Base test to cober BI_FVI_NodosMethods.changeOwnerActivateNode in AccountTeamMember

    <date>      <version>       <description>
    20/04/2018  1.0             Initial


  **************************************************/
  @isTest
  public static void changeAccountTeamMember(){
  	String idPer = [Select ID from Profile where Name = 'BI_Standard_PER'].Id;
  	BI_DataLoad.loadUsers(2,idPer,Label.BI_Administrador);
  	List<User> lst_usrper = [Select Id from User where ProfileId=:idPer AND isActive=true ORDEr by CreatedDate DESC limit 2];
  	Account acc = new Account(Name='TEST_JLA_Node_1_actm',BI_Country__c=Label.BI_Peru, BI_FVI_Tipo_Planta__c = 'No Cliente',
                BI_Segment__c = 'test',
                BI_Subsegment_Regional__c = 'test',
    			BI_Territory__c = 'test');
  	String idRT = [Select Id from RecordType where DeveloperName='BI_FVI_NCS'].Id;
  	List<BI_FVI_Nodos__c> lst_nodes = new List<BI_FVI_Nodos__c>();
  	BI_FVI_Nodos__c node_1 = new BI_FVI_Nodos__c(Name='TEST_NCS_JLA_1',
  		RecordTypeId=idRT,
  		BI_FVI_Pais__c='Peru',
  		OwnerId=lst_usrper[0].Id,
  		BI_FVI_Activo__c=true,
  		BI_FVI_Acceso_oportunidades__c='Read',
  		BI_FVI_Acceso_clientes__c='Read',
  		BI_FVI_Acceso_casos__c='None',
  		BI_FVI_Asignacion_atomatica__c=true
  		);
  	BI_FVI_Nodos__c node_2 = new BI_FVI_Nodos__c(Name='TEST_NCS_JLA_2',
  		RecordTypeId=idRT,
  		BI_FVI_Pais__c='Peru',
  		OwnerId=lst_usrper[1].Id,
  		BI_FVI_Activo__c=false,
  		BI_FVI_Acceso_oportunidades__c='Read',
  		BI_FVI_Acceso_clientes__c='Read',
  		BI_FVI_Acceso_casos__c='None',
  		BI_FVI_Asignacion_atomatica__c=true,
  		BI_FVI_Desasignacion_atomatica__c=true
  		);
  	lst_nodes.add(node_1);
  	lst_nodes.add(node_2);
  	Database.SaveResult[] ar_svr = Database.insert(lst_nodes);
  	for(Integer i = 0 ;i<ar_svr.size();i++){
  		Database.SaveResult svr = ar_svr[i];
  		if(svr.isSuccess()==true){
  			
  		}else{
  			System.assert(1==2,'Hubo un error insertando los nodos'+svr.getErrors()[0].getMessage());
  		}
  	}
  	DAtaBase.SaveResult acc_svr = Database.insert(acc);
  	if(acc_svr.isSuccess()==false){
  			System.assert(1==2,'Hubo un error insertando la cuenta'+acc_svr.getErrors()[0].getMessage());
  	}
  	for(BI_FVI_Nodos__c node :[Select Id,BI_FVI_Activo__c,RecordType.DeveloperName from BI_FVI_Nodos__c]){
  		System.debug('JLA_: '+node.Id+'||'+node.RecordType.DeveloperName+'@@@'+node.BI_FVI_Activo__c);	
  		if(node.BI_FVI_Activo__c==true){
  			node_1=node;
  		}else{
  			node_2=node;
  		}
  	}
  	TGS_User_Org__c userTGS = new TGS_User_Org__c(BI_FVI_Is_FVI__c=true);
    insert userTGS;
  	BI_FVI_Nodo_Cliente__c ndc_1 = new BI_FVI_Nodo_Cliente__c(BI_FVI_IdNodo__c=node_1.Id,BI_FVI_IdCliente__c=acc_svr.getId());
  	BI_FVI_Nodo_Cliente__c ndc_2 = new BI_FVI_Nodo_Cliente__c(BI_FVI_IdNodo__c=node_2.Id,BI_FVI_IdCliente__c=acc_svr.getId());
  	List<BI_FVI_Nodo_Cliente__c> lst_ndc = new List<BI_FVI_Nodo_Cliente__c>();
  	lst_ndc.add(ndc_1);
  	lst_ndc.add(ndc_2);
  	test.startTest();
  	System.debug('JLA_START_NCSSS');
  	Database.SaveResult[] arrNDC_svr = DataBase.insert(lst_ndc);
  	for(Integer i = 0;i<arrNDC_svr.size();i++){
  		DataBase.SaveResult svr = arrNDC_svr[i];
  		if(svr.isSuccess()==true){
  			if(i==0)
  				ndc_1.Id=svr.getId();
  			else
  				ndc_2.Id=svr.getId();
  		}else{
  			System.assert(1==2,'Hubo un error insertando los nodos clientes i: '+i+' error:'+acc_svr.getErrors()[0].getMessage());		
  		}
  		
  	}
  	
  	System.assert([Select ID from AccountTeamMember].size()==1,'PArece ser que no son iguales:'+[Select ID from AccountTeamMember].size());
  	System.debug([Select RecordType.DeveloperName from BI_FVI_Nodos__c where Id=:node_2.Id]);
  	node_2.BI_FVI_Activo__c=true;
  	System.debug('JLA_: HAcemos el update del nodod');
  	update node_2;
  	System.debug('JLA_: Finalizamos el update del nodo');
  	System.assert([Select ID from AccountTeamMember].size()==2,'PArece ser que no son iguales:'+[Select ID from AccountTeamMember].size());
  	delete ndc_2;
  	System.assert([Select ID from AccountTeamMember].size()==1,'No igual a uno no se ha borrado la asignacion');
  	ndc_1.BI_FVI_IdNodo__c=node_2.Id;
  	update ndc_1; 
  	System.assert([Select ID from AccountTeamMember].size()==2,'O no lo ha igualado o a fallado la creacion');
  	
  	test.stopTest();

  }
}