global without sharing class BI_CurrencyHelper {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Static Helper Method that is used on BI_Buttons class. Convert diferent currencys values.
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    23/07/2014              Micah Burgos 	        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static final Map<String,Double> CONVERSION_RATES = new Map<String,Double> ();
    global static final List<CurrencyType> LST_CURRENCYTYPE;

    static {
    	LST_CURRENCYTYPE = [SELECT IsoCode,ConversionRate FROM CurrencyType];
    	
    	for(CurrencyType curr: LST_CURRENCYTYPE){          
            CONVERSION_RATES.put(curr.IsoCode,curr.ConversionRate);        
        }
    }
    
	  
	public static Double convertCurrency(String oCurrency, String nCurrency, Double amount){
        try{
    		//system.debug('BI_CurrencyHelper.convertCurrency()->: oCurrency'+oCurrency);
    		//system.debug('BI_CurrencyHelper.convertCurrency()->: nCurrency'+nCurrency);
    		//system.debug('BI_CurrencyHelper.convertCurrency()->: OLD amount'+amount);
    		
            //Convert incoming Amount into ORG Currency (ARS)
            Double conversionRate = CONVERSION_RATES.get(oCurrency);
            //system.debug('BI_CurrencyHelper.convertCurrency()->: conversionRate to '+ oCurrency+': '+conversionRate);
            amount = amount / conversionRate;
            
            //convert amount as per new currency. 
            conversionRate = CONVERSION_RATES.get(nCurrency);
            //system.debug('BI_CurrencyHelper.convertCurrency()->: conversionRate to '+ nCurrency+': '+conversionRate);
            amount = amount * conversionRate;

            //system.debug('BI_CurrencyHelper.convertCurrency()->: NEW amount'+amount);
            return amount;
        
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('BI_CurrencyHelper.convertCurrency', 'BI_EN', Exc, 'Web Service');
            return null;
        }
    }
}