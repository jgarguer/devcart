/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for CWP_SelfManagementOffer component
    Test Class:    CWP_SelfManagementOfferCtrlTest
    
    History:
     
    <Date>                  <Author>                <Change Description>
    30/03/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public without sharing class CWP_SelfManagementOfferCtrl {
   @auraEnabled
    public static map<string,string> getRecords(String tableControllerS){
        List<ClaimsRequestRegs> records = new List<ClaimsRequestRegs>();
        List<ClaimsRequestHeaders> recordsdHeaders = new List<ClaimsRequestHeaders>();
        String accountId= BI_AccountHelper.getCurrentAccountId();
        String nameFS = 'PCA_MainTableOportunidad';
        String nameObject = 'Opportunity';
        String accountFieldName = 'AccountId';
        String accountValue = accountFieldName + ';' + AccountId;
        System.debug('Account Value: ' + accountValue + ' AccountId: ' + accountId);
        FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, '', accountValue, null);
        System.debug('FieldSetContainer: ' + fieldSetRecords);
        Map <String,String> ret= new Map <String,String>();
        TableController tableControl;
        String query='';
        List<Case> lCases;
        // Comprobamos si es la llamada inicial o es un cambio de parámetros 
        if (tableControllerS!='init'){
            tableControl = (CWP_SelfmanagementOfferCtrl.TableController)JSON.deserialize(tableControllerS, CWP_SelfmanagementOfferCtrl.TableController.class);
            tableControl.finIndex=tableControl.iniIndex+tableControl.viewSize-1;
        }else {
            tableControl= new TableController('tickets',  1,  10,  0,  10 );
        }
        for(FieldSetHelper.FieldSetRecord fieldSet : fieldSetRecords.regs){
            ClaimsRequestRegs record = new ClaimsRequestRegs();
            for(FieldSetHelper.FieldSetResult result : fieldSet.values){
                if(result.field.equals('BI_Id_Interno_de_la_Oportunidad__c')){
                    record.idOpportunity = result.value;
                }
                if(result.field.equals('BI_Opportunity_Type__c')){
                    record.bI_Fecha_de_entrega_de_la_oferta = result.value;
                }
                if(result.field.equals('BI_Fecha_de_entrega_de_la_oferta__c')){
                    record.bI_Fecha_de_entrega_de_la_oferta = result.value;
                }
                if(result.field.equals('BI_Recurrente_bruto_mensual__c')){
                    record.bI_Recu_bruto_mensual = result.value;
                }
                if(result.field.equals('BI_Duracion_del_contrato_Meses__c')){
                    record.bI_Duracion_del_contrato_Meses = result.value;   
                }  
                if(result.field.equals('CloseDate')){
                    record.closeDate = result.value; 
                }
            }   
            records.add(record);
        }

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        ClaimsRequestHeaders recordHeader = new ClaimsRequestHeaders();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Opportunity');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('PCA_MainTableOportunidad');
        for (Schema.FieldSetMember fld2 : fieldSetObj.getFields()){
            if(fld2.getFieldPath().equals('BI_Id_Interno_de_la_Oportunidad__c')){
                recordHeader.idOpportunity = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Opportunity_Type__c')){
                recordHeader.bI_Opportunity_Type = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Fecha_de_entrega_de_la_oferta__c')){
                recordHeader.bI_Fecha_de_entrega_de_la_oferta = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Recurrente_bruto_mensual__c')){
                recordHeader.bI_Recu_bruto_mensual = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Duracion_del_contrato_Meses__c')){
                recordHeader.bI_Duracion_del_contrato_Meses = fld2.getLabel();  
            }
            if(fld2.getFieldPath().equals('CloseDate')){
                recordHeader.closeDate = fld2.getLabel();
            }
        }
        recordsdHeaders.add(recordHeader);
        tableControl.numRecords=records.size();

        //Controlamos si tenemos menos registros de los que se pueden mostrar. 
        if (tableControl.numRecords < tableControl.viewSize){
            tableControl.finIndex= tableControl.numRecords;
        }
        if (tableControl.finIndex > tableControl.numRecords){
            tableControl.finIndex= tableControl.numRecords;
        }
        system.debug(records);
        
        List<ClaimsRequestRegs> recordsFilter = new List<ClaimsRequestRegs>();
        Integer inicio = tableControl.iniIndex -1;
        Integer fin = tableControl.finIndex;
        
        if (fin > tableControl.numRecords) {
            fin = tableControl.numRecords;
        }
        
        for(Integer i=inicio;i < fin;i++){
            recordsFilter.add(records[i]);
        }
        
        ret.put('lisTickets', JSON.serialize(recordsFilter));
        
        ret.put('offsetController', JSON.serialize(tableControl));
        
        ret.put('recordsdHeaders', JSON.serialize(recordsdHeaders));
        
        return ret; 

    }
    
    @auraEnabled
    public static  map<String,String> getFieldsValues(String tableControllerS){
        Map <String,String> ret= new Map <String,String>();
        String CASERT = Label.CWP_ticketsProfiles;
        String whereQuery;
        whereQuery=' WHERE RecordType.DeveloperName IN'+ CASERT;
        //SELECT Id, Subject, caseNumber, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, toLabel(type), status,TGS_Customer_Services__r.NE__Country__c, TGS_Customer_Services__r.NE__City__c, CWP_textDate__c, CWP_KeyValue__c FROM Case'
        ret.put('type',JSON.serialize(getOptions('type', whereQuery)));
        ret.put('TGS_Product_Tier_1__c',JSON.serialize(getOptions('TGS_Product_Tier_1__c', whereQuery)));
        List<CWP_SelectOption> options = new List<CWP_SelectOption>();
        options.add(new CWP_SelectOption(Label.CWP_FistSelect+ ' '+Label.TGS_CWP_T1, Label.CWP_FistSelect+ ' '+Label.TGS_CWP_T1));
        ret.put('TGS_Product_Tier_2__c',JSON.serialize(options));
        ret.put('TGS_Product_Tier_3__c',JSON.serialize(options));
        ret.put('status',JSON.serialize(getOptions('status', whereQuery)));
        ret.put('Country',JSON.serialize(getOptions('TGS_Customer_Services__r.NE__Country__c', whereQuery)));
        ret.put('City',JSON.serialize(getOptions('TGS_Customer_Services__r.NE__City__c', whereQuery)));
        
        
        options = new List<CWP_SelectOption>();
        options.add(new CWP_SelectOption(Label.TGS_CWP_SELOPT,Label.TGS_CWP_SELOPT));
        options.add(new CWP_SelectOption('My Requests', String.valueOf(userInfo.getUserId())));
        options.add(new CWP_SelectOption('Requires your attention','ATTREQ'));
        options.add(new CWP_SelectOption('All Requests', 'NONESELECTED'));
        
        
        ret.put('toShow',JSON.serialize(options));
        
        
        
        return ret;
        
    }
    
    public static  List<CWP_SelectOption> getOptions(String field, String whereQuery){
        List<CWP_SelectOption> options = new List<CWP_SelectOption>();
        String query;
        query='SELECT '+field+ ' FROM Case '+ whereQuery + 'AND '+field+' !=null GROUP BY '+field+' ORDER BY '+field ;
        System.debug('la query es -->' +query); 
        options.add(new CWP_SelectOption(Label.TGS_CWP_SELOPT,Label.TGS_CWP_SELOPT));
        
        field=(field.substringAfterLast('.')=='')?field:field.substringAfterLast('.');
        for (AggregateResult a: Database.query(query)){
            
            System.debug ('@@ AggregateResult '+a);
            
            options.add(new CWP_SelectOption(String.valueOf(a.get(field)),String.valueOf(a.get(field))));
            
        }
        if (options.size()<2){
            options.clear();
            options.add(new CWP_SelectOption(Label.CWP_NoOptions,Label.CWP_NoOptions));            
        }
        return options;
    }
    
    
     @auraEnabled
    public static map<string,string> getFilteredRecords(String field, String value){
        List<ClaimsRequestRegs> records = new List<ClaimsRequestRegs>();
        List<ClaimsRequestHeaders> recordsdHeaders = new List<ClaimsRequestHeaders>();
        String accountId= BI_AccountHelper.getCurrentAccountId();
        String nameFS = 'PCA_MainTableOportunidad';
        String nameObject = 'Opportunity';
        String accountFieldName = 'AccountId';
        String accountValue = accountFieldName + ';' + AccountId;
        String searchFinal = field + ';' + value;
        FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, searchFinal , accountValue, null);
        Map <String,String> ret= new Map <String,String>();
        TableController tableControl;
        String query='';
        List<Case> lCases;
        tableControl= new TableController('tickets',  1,  10,  0,  10 );
        for(FieldSetHelper.FieldSetRecord fieldSet : fieldSetRecords.regs){
            ClaimsRequestRegs record = new ClaimsRequestRegs();
            for(FieldSetHelper.FieldSetResult result : fieldSet.values){
                if(result.field.equals('BI_Id_Interno_de_la_Oportunidad__c')){
                    record.idOpportunity = result.value;
                }
                if(result.field.equals('BI_Opportunity_Type__c')){
                    record.bI_Fecha_de_entrega_de_la_oferta = result.value;
                }
                if(result.field.equals('BI_Fecha_de_entrega_de_la_oferta__c')){
                    record.bI_Fecha_de_entrega_de_la_oferta = result.value;
                }
                if(result.field.equals('BI_Recurrente_bruto_mensual__c')){
                    record.bI_Recu_bruto_mensual = result.value;
                }
                if(result.field.equals('BI_Duracion_del_contrato_Meses__c')){
                    record.bI_Duracion_del_contrato_Meses = result.value;   
                }  
                if(result.field.equals('CloseDate')){
                    record.closeDate = result.value; 
                }
            }   
            records.add(record);
        }

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        ClaimsRequestHeaders recordHeader = new ClaimsRequestHeaders();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Opportunity');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('PCA_MainTableOportunidad');
        for (Schema.FieldSetMember fld2 : fieldSetObj.getFields()){
            if(fld2.getFieldPath().equals('BI_Id_Interno_de_la_Oportunidad__c')){
                recordHeader.idOpportunity = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Opportunity_Type__c')){
                recordHeader.bI_Opportunity_Type = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Fecha_de_entrega_de_la_oferta__c')){
                recordHeader.bI_Fecha_de_entrega_de_la_oferta = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Recurrente_bruto_mensual__c')){
                recordHeader.bI_Recu_bruto_mensual = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Duracion_del_contrato_Meses__c')){
                recordHeader.bI_Duracion_del_contrato_Meses = fld2.getLabel();  
            }
            if(fld2.getFieldPath().equals('CloseDate')){
                recordHeader.closeDate = fld2.getLabel();
            }
        }
        recordsdHeaders.add(recordHeader);
        tableControl.numRecords=records.size();

        //Controlamos si tenemos menos registros de los que se pueden mostrar. 
        if (tableControl.numRecords < tableControl.viewSize){
            tableControl.finIndex= tableControl.numRecords;
        }
        if (tableControl.finIndex > tableControl.numRecords){
            tableControl.finIndex= tableControl.numRecords;
        }
        system.debug(records);
        
        List<ClaimsRequestRegs> recordsFilter = new List<ClaimsRequestRegs>();
        Integer inicio = tableControl.iniIndex -1;
        Integer fin = tableControl.finIndex;
        
        if (fin > tableControl.numRecords) {
            fin = tableControl.numRecords;
        }
        
        for(Integer i=inicio;i < fin;i++){
            recordsFilter.add(records[i]);
        }
        
        ret.put('lisTickets', JSON.serialize(recordsFilter));
        
        ret.put('offsetController', JSON.serialize(tableControl));
        
        ret.put('recordsdHeaders', JSON.serialize(recordsdHeaders));
        
        return ret; 

    }
    
      @auraEnabled
    public static map<string,string> getSortRecords(String field, String direction){
        List<ClaimsRequestRegs> records = new List<ClaimsRequestRegs>();
        List<ClaimsRequestHeaders> recordsdHeaders = new List<ClaimsRequestHeaders>();
        String accountId= BI_AccountHelper.getCurrentAccountId();
        String nameFS = 'PCA_MainTableOportunidad';
        String nameObject = 'Opportunity';
        String accountFieldName = 'AccountId';
        String accountValue = accountFieldName + ';' + AccountId;
        String searchFinal = field;
        FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, searchFinal , accountValue, direction);
        Map <String,String> ret= new Map <String,String>();
        TableController tableControl;
        String query='';
        List<Case> lCases;
        tableControl= new TableController('tickets',  1,  10,  0,  10 );
        for(FieldSetHelper.FieldSetRecord fieldSet : fieldSetRecords.regs){
            ClaimsRequestRegs record = new ClaimsRequestRegs();
            for(FieldSetHelper.FieldSetResult result : fieldSet.values){
                if(result.field.equals('BI_Id_Interno_de_la_Oportunidad__c')){
                    record.idOpportunity = result.value;
                }
                if(result.field.equals('BI_Opportunity_Type__c')){
                    record.bI_Fecha_de_entrega_de_la_oferta = result.value;
                }
                if(result.field.equals('BI_Fecha_de_entrega_de_la_oferta__c')){
                    record.bI_Fecha_de_entrega_de_la_oferta = result.value;
                }
                if(result.field.equals('BI_Recurrente_bruto_mensual__c')){
                    record.bI_Recu_bruto_mensual = result.value;
                }
                if(result.field.equals('BI_Duracion_del_contrato_Meses__c')){
                    record.bI_Duracion_del_contrato_Meses = result.value;   
                }  
                if(result.field.equals('CloseDate')){
                    record.closeDate = result.value; 
                }
            }   
            records.add(record);
        }

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        ClaimsRequestHeaders recordHeader = new ClaimsRequestHeaders();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Opportunity');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('PCA_MainTableOportunidad');
        for (Schema.FieldSetMember fld2 : fieldSetObj.getFields()){
            if(fld2.getFieldPath().equals('BI_Id_Interno_de_la_Oportunidad__c')){
                recordHeader.idOpportunity = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Opportunity_Type__c')){
                recordHeader.bI_Opportunity_Type = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Fecha_de_entrega_de_la_oferta__c')){
                recordHeader.bI_Fecha_de_entrega_de_la_oferta = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Recurrente_bruto_mensual__c')){
                recordHeader.bI_Recu_bruto_mensual = fld2.getLabel();
            }
            if(fld2.getFieldPath().equals('BI_Duracion_del_contrato_Meses__c')){
                recordHeader.bI_Duracion_del_contrato_Meses = fld2.getLabel();  
            }
            if(fld2.getFieldPath().equals('CloseDate')){
                recordHeader.closeDate = fld2.getLabel();
            }
        }
        recordsdHeaders.add(recordHeader);
        tableControl.numRecords=records.size();

        //Controlamos si tenemos menos registros de los que se pueden mostrar. 
        if (tableControl.numRecords < tableControl.viewSize){
            tableControl.finIndex= tableControl.numRecords;
        }
        if (tableControl.finIndex > tableControl.numRecords){
            tableControl.finIndex= tableControl.numRecords;
        }
        system.debug(records);
        
        List<ClaimsRequestRegs> recordsFilter = new List<ClaimsRequestRegs>();
        Integer inicio = tableControl.iniIndex -1;
        Integer fin = tableControl.finIndex;
        
        if (fin > tableControl.numRecords) {
            fin = tableControl.numRecords;
        }
        
        for(Integer i=inicio;i < fin;i++){
            recordsFilter.add(records[i]);
        }
        
        ret.put('lisTickets', JSON.serialize(recordsFilter));
        
        ret.put('offsetController', JSON.serialize(tableControl));
        
        ret.put('recordsdHeaders', JSON.serialize(recordsdHeaders));
        
        return ret; 

    }
    
    public class TableController{
        public string table;
        public Integer iniIndex;
        public Integer finIndex;
        public Integer numRecords;
        public Integer viewSize;
        
        public  TableController(String tab, Integer iniIndex, Integer finIndex, Integer numRecords, Integer viewSize ){
            this.table=tab;
            this.iniIndex=iniIndex;
            this.finIndex=finIndex;
            this.numRecords=numRecords;
            this.viewSize=viewSize;
        }       
    }
    
    public class CWP_SelectOption {
        public string label;
        public string value;
        
        
        public  CWP_SelectOption(string label, string value){
            this.label=label;
            this.value=value;
            
        }       
    }

    public class ClaimsRequestRegs{
        @auraEnabled
        public String idOpportunity {get;set;}
        @auraEnabled
        public String bI_Opportunity_Type {get;set;}
        @auraEnabled
        public String bI_Fecha_de_entrega_de_la_oferta {get;set;}
        @auraEnabled
        public String bI_Recu_bruto_mensual {get;set;}
        @auraEnabled
        public String bI_Duracion_del_contrato_Meses {get;set;}
        @auraEnabled
        public String closeDate {get;set;} 
    }
    
    public class ClaimsRequestHeaders{
        @auraEnabled
        public String idOpportunity {get;set;}
        @auraEnabled
        public String bI_Opportunity_Type {get;set;}
        @auraEnabled
        public String bI_Fecha_de_entrega_de_la_oferta {get;set;}
        @auraEnabled
        public String bI_Recu_bruto_mensual {get;set;}
        @auraEnabled
        public String bI_Duracion_del_contrato_Meses {get;set;}
        @auraEnabled
        public String closeDate {get;set;}       
    }
}