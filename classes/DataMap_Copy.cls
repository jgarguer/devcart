global with sharing class DataMap_Copy
{
	private Map<Id,RecordType> moMap; 
	global map<String,String> mapOfCustomQueries; //Gc 3.2 introducing the cust queries for a dmap name
	global list<String> listOfPlaceHolders; //Gc 3.2 introducing the cust queries for a dmap name
	global boolean updateActive;//Enable the "updateKey" management
	
	global DataMap_Copy()
	{
		moMap               =   new Map<Id,RecordType>([SELECT Id,SObjectType,Name FROM RecordType WHERE IsActive=true ORDER BY SObjectType]);
		mapOfCustomQueries  =   new map<String,String>();
		listOfPlaceHolders  =   new list<String>();
		updateActive        =   false;
	}
	
	global Map<String,String> GenerateMapObjects(String mapName, String sourceId)
	{
		Map<String,String> result       = new Map<String,String>();
		Map<String,List<String>> idMapTarget    = new Map<String,List<String>>();   
		Map<String,List<String>> idMapSource    = new Map<String,List<String>>();   
		
		Map<String,String> value                = new Map<String,String>(); 
		Map<String,String> parentValue          = new Map<String,String>(); 
		Map<String,String> keyfieldmap          = new Map<String,String>(); //gc 2.0
		
		List<NE__Map__c> dMap;
		List<NE__MapObject__c> mapObject;
		List<NE__MapObjectItem__c> mapObjectItems;
		Map<String,SObject> sObjectMap          = new Map<String,SObject>();
		//GC 2.0
		Map<String,SObject> sObjectSourceMap    = new Map<String,SObject>();
		
		
		List<String> sourceField    = new List<String>();
		List<String> targetField    = new List<String>();
		
		String ErrorCode    = '0';
		String ErrorMessage = '';
		
		String objId        =   ''; 
		String objName      =   '';
		String objSource    =   ''; 
		String objTarget    =   ''; 
		String objParent    =   ''; 
		String objFilter    =   '';
		String objOrder     =   ''; //GC 2.0 
		boolean filterActive    =  false; 
		String filterSource     =  ''; 
		String filterOperation  =  '';
		
		boolean booleanTarget    =  false;
		String  stringTarget     =  '';
		Integer integerTarget    =  0;
		
		//GC 2.0, added support for date, decimal, currency and datetime filter
		Decimal decimalTarget   =   0; 
		Date    dateTarget;
		DateTime dateTimeTarget;

		String parentSource     =   ''; 
		String parentTarget     =   ''; 
		String parentParent     =   ''; 
		String parentName       =   ''; 
		String parentQuery      =   ''; 
		String parentSourceId   =   ''; 
		String parentTargetId   =   ''; 
		String parentId         =   '';
		
		String itemType     =   '';
		String rtId         =   '';
		String sourceString =   '';
		String targetString =   '';
		String queryString  =   '';
		String queryUpdate  =   '';
		String sourceKey    =   '';
		String updateSourceKey  =   '';
		String updateTargetKey  =   '';
		
		Boolean isUpd           =   false;
		Boolean toUpd           =   false;
		
		SObject sObj;
		//SObject queryObject;
		List<SObject> queryObjectChildren;
		List<SObject> queryObjectList;
		List<SObject> listofUpdObj;
		
		List<SObject> objectsToUpdate   =    new List<SObject>();
		List<SObject> objectsToInsert   =    new List<SObject>();
	
		//Check the map existence
		dMap                                        =   [SELECT Id,Name,NE__Map_Name__c FROM NE__Map__c WHERE NE__Map_Name__c =: mapName AND NE__Type__c = 'Object2Object'];
		List<NE__MapObject__c>  ListOfMapObjects        =   new list<NE__MapObject__c>([SELECT Id,NE__Activate_Sort__c,NE__Sort_Source__c,NE__Sort_Operation__c,Name,NE__Source__c,NE__Target__c,NE__Type__c,NE__Map__c,NE__Parent__c,NE__Filter__c,NE__Activate_Filter__c,NE__Filter_Operation__c,NE__Filter_Source__c,NE__Filter_Target__c,(SELECT Id,Name,NE__SourceField__c,NE__TargetField__c,NE__Type__c, NE__Value__c,NE__RecordTypeId__c FROM NE__Map_Object_Items__r) FROM NE__MapObject__c WHERE NE__Map__r.NE__Map_Name__c =: mapName ORDER BY NE__Sequence__c ASC]); 
		Map<String,NE__MapObject__c> MapOfMapObjects    =   new Map<String,NE__MapObject__c>(ListOfMapObjects);
		System.debug(LoggingLevel.ERROR, 'Resultado de la ListOfMapObjects:' + ListOfMapObjects );
		
		Savepoint savePointDatamap                  =   Database.setSavepoint();
		
		try
		{
			if (!dMap.isEmpty()) 
			{ 
				System.debug('Calling map: '+ mapName);
				System.debug(LoggingLevel.ERROR, 'Resultado de la tamaño:' + ListOfMapObjects.size());         
				//for every mapObject of the list create the object
				for (NE__MapObject__c mo : ListOfMapObjects) 
				{
					System.debug(LoggingLevel.ERROR, 'Cada mo' + mo);
					isUpd           =   false;
					toUpd           =   false;
					
					objId       =   mo.ID;
					objSource   =   mo.NE__Source__c;
					objTarget   =   mo.NE__Target__c;
					objParent   =   mo.NE__Parent__c;
					objName     =   mo.Name;
					objFilter   =   mo.NE__Filter__c;
					objOrder    =   '';
					filterActive    =  mo.NE__Activate_Filter__c;
					filterOperation =  mo.NE__Filter_Operation__c;
					filterSource    =  mo.NE__Filter_Source__c;
					stringTarget    =  mo.NE__Filter_Target__c;
					
					
					objFilter='';                                           
					System.debug('PARENT: ' + objParent + ' - '+ mo.NE__Parent__c);
					
					 //GC 2.0, added support for date, decimal, currency and datetime filter
					if(filterActive == true)
					{
						filterSource    =   String.escapeSingleQuotes(filterSource);
						filterOperation =   String.escapeSingleQuotes(filterOperation);
						
						//get the filter type for generating the dynamic query
						Map<String, Schema.SObjectField> M = Schema.getGlobalDescribe().get(objSource).getDescribe().fields.getMap();
						Schema.SObjectField field = M.get(filterSource);
						Schema.DisplayType FldType = field.getDescribe().getType();  
						
						System.Debug('Field Type for filter:' +FldType);   
					 
						//GC 1.7.2 manage null query    
						if(stringTarget == null)
							stringTarget =  '';                          

						if(FldType == Schema.DisplayType.Boolean)
						{
							booleanTarget = stringTarget.equalsIgnoreCase('true');
							objFilter=' AND ( '+filterSource+' '+filterOperation+' booleanTarget)';
						}
						else if(FldType == Schema.DisplayType.Integer)
						{
							if(stringTarget != null && stringTarget != '')
								integerTarget = Integer.valueof(stringTarget);
							else
								integerTarget   =   null;
								
							objFilter=' AND ( '+filterSource+' '+filterOperation+' integerTarget)';
						}
						else if(FldType == Schema.DisplayType.Double || FldType == Schema.DisplayType.Currency || FldType == Schema.DisplayType.Percent)//GC 1.9 support for decimal (double) values
						{
							if(stringTarget != null && stringTarget != '')
								decimalTarget= Decimal.valueof(stringTarget);
							else
								decimalTarget   =   null;
								
							objFilter=' AND ( '+filterSource+' '+filterOperation+' decimalTarget)';
						}  
						else if(FldType == Schema.DisplayType.Date)
						{  
							System.debug(LoggingLevel.ERROR, 'StringTarget Date: ' + stringTarget);
							if(stringTarget != null && stringTarget != '')
							{
								String[] dateIstance =  stringTarget.split('/');                            
								dateTarget  =   date.newInstance(Integer.valueof(dateIstance[0]),Integer.valueof(dateIstance[1]), Integer.valueof(dateIstance[2]));
							}                               
							else
								dateTarget  =   null;
								
							objFilter=' AND ( '+filterSource+' '+filterOperation+' dateTarget)';
							System.debug(LoggingLevel.ERROR, 'DateTarget: ' + dateTarget); 
						}
						else if(FldType == Schema.DisplayType.DateTime)
						{
							System.debug(LoggingLevel.ERROR, 'FldType DateTime:' + stringTarget);  
							if(stringTarget != null && stringTarget != '')
							{
								String[] dtIstance   =  stringTarget.split(' ');     
								String[] dateIstance =  dtIstance[0].split('/'); 
								String[] timeIstance;
								if(dtIstance[1] != null)  
									timeIstance =  dtIstance[1].split(':'); 
								else
									timeIstance =    new String[]{'00', '00','00'};
													
								dateTimeTarget  =   datetime.newInstance(Integer.valueof(dateIstance[0]),Integer.valueof(dateIstance[1]), Integer.valueof(dateIstance[2]),
																		Integer.valueof(timeIstance[0]),Integer.valueof(timeIstance[1]),Integer.valueof(timeIstance[2]));
							}                               
							else
								dateTimeTarget  =   null;
								
							objFilter=' AND ( '+filterSource+' '+filterOperation+' dateTimeTarget)';
							System.debug(LoggingLevel.ERROR, 'FldType DateTime objFilter:' + objFilter); 
						}                                                   
						else
						{
							if(stringTarget != null && stringTarget != '')
								stringTarget    =   String.escapeSingleQuotes(stringTarget);
							objFilter=' AND ( '+filterSource+' '+filterOperation+' stringTarget)';
						}
						
						objFilter = String.escapeSingleQuotes(objFilter);                  
					}       
					
					//GC 3.2 manage custom queries
					System.Debug('MAPACustomQUERY:' +mapOfCustomQueries.get(objName));
					System.Debug(LoggingLevel.ERROR, 'MAPACustomQUERY:' +mapOfCustomQueries);
					String customQuery  =   mapOfCustomQueries.get(objName);
					String ph0;
					String ph1;
					String ph2;
					String ph3;
					String ph4;
					System.Debug('CustomQUERY:' +customQuery);
					if(customQuery != null && customQuery != '')
					{
						try
						{
							ph0 =   listOfPlaceHolders.get(0);
							customQuery =   customQuery.replace('{!0}','ph0');
							ph1 =   listOfPlaceHolders.get(1);
							customQuery =   customQuery.replace('{!1}','ph1');
							ph2 =   listOfPlaceHolders.get(2);
							customQuery =   customQuery.replace('{!2}','ph2');
							ph3 =   listOfPlaceHolders.get(3);
							customQuery =   customQuery.replace('{!3}','ph3');
							ph4 =   listOfPlaceHolders.get(4);                      
							customQuery =   customQuery.replace('{!4}','ph4');
						}
						catch(Exception e)
						{}
						if(!Test.isRunningTest()){
							objFilter+=' AND ('+customQuery+')';
						}
					}           

					//GC 2.0 manage order by in the query
					if(mo.NE__Activate_Sort__c)
					{
						objOrder+=' ORDER BY '+mo.NE__Sort_Source__c+' '+mo.NE__Sort_Operation__c;                      
					}
				   
					//DEBUG
					System.Debug('Id: '+objId);
					System.Debug('Source: '+objSource);
					System.Debug('Target: '+objTarget);
					System.Debug('Parent: '+objParent);
					System.Debug('objName: '+objName);
					System.Debug('Filter query: '+objFilter);
					//END DEBUG
					
					if(objParent == null)
					{
						//Parent mapping block
						System.Debug('Begin to populate the item');     
						queryString += 'SELECT Id';
						//get the object from the target
						System.Debug('FOR ObjectItem: '+mo.NE__Map_Object_Items__r);

						for(NE__MapObjectItem__c moi : mo.NE__Map_Object_Items__r)
						{
							itemType = moi.NE__Type__c;
							
							//generate the field to get
							if(itemType.equalsIgnoreCase('Field'))
							{
								sourceString    =   moi.NE__SourceField__c;
								targetString    =   moi.NE__TargetField__c;
																
								sourceField.add(moi.NE__SourceField__c);
								targetField.add(moi.NE__TargetField__c);
								
								if(!sourceString.equalsIgnoreCase('Id'))
								{                               
									queryString =   queryString.replaceAll(' ,'+sourceString+' ', '');
									queryString+=' ,'+sourceString+' ';       
								}          
							}
							else if(itemType.equalsIgnoreCase('Value'))
							{
								value.put(moi.NE__TargetField__c,moi.NE__Value__c);
							}
							else if(itemType.equalsIgnoreCase('UpdateKey') && updateActive == true)
							{
								updateSourceKey = moi.NE__SourceField__c;
								updateTargetKey = moi.NE__TargetField__c;                       
								
								queryString =   queryString.replaceAll(' ,'+updateSourceKey+' ', '');
								queryString+=' ,'+updateSourceKey+' ';
								isUpd   =   true;
							}
							else if(itemType.equalsIgnoreCase('RecordType'))
							{
								rtId = moi.NE__RecordTypeId__c;
								System.Debug('RType for '+objTarget+' :'+rtId);
								RecordType rtemp = moMap.get(rtId);
								System.Debug('RTObj: '+rtemp);
								value.put('RecordTypeId',moMap.get(rtId).Id);
							}   
							
						}      

						//gc 3.4.4 manage batch mode with sourceId == null                       
						if(sourceId != null)
						{
							sourceId = String.escapeSingleQuotes(sourceId);
							queryString+=' FROM '+String.escapeSingleQuotes(mo.NE__Source__c)+' WHERE Id = :sourceId '+String.escapeSingleQuotes(objFilter);  
						}
						else
							queryString+=' FROM '+String.escapeSingleQuotes(mo.NE__Source__c)+' WHERE Id != null '+String.escapeSingleQuotes(objFilter);  
						System.Debug('Query String: '+queryString);
						System.Debug(LoggingLevel.ERROR, 'Resultado Query: ' +Database.query(queryString)); 
						queryObjectList = Database.query(queryString);

						//Store the information regarding the id of the target and the source in a new list
						List<String> targetIdList   = new List<String>();
						List<String> sourceIdList   = new List<String>();

						System.Debug('Object List: '+queryObjectList); 
						System.Debug(LoggingLevel.ERROR, 'IsUpd:' +isUpd);
						for(sObject queryObject:queryObjectList)
						{
							if(isUpd == true)
							{
								System.Debug(LoggingLevel.ERROR, 'UpdateSourceKey ' +updateSourceKey); 
								if(queryObject.get(updateSourceKey) != null)
								{            
									System.Debug('source diverso da null');
									String upd = (String)queryObject.get(updateSourceKey);  
									if(!upd.equalsIgnoreCase(''))
									{
										//Create the parent object
										Object obj = queryObject.get(updateSourceKey);                                    
										queryUpdate = 'SELECT Id FROM '+String.escapeSingleQuotes(objTarget)+' WHERE '+String.escapeSingleQuotes(updateTargetKey)+' = : obj ';
										System.Debug(LoggingLevel.ERROR, 'QueryUpdate: ' +queryUpdate);

										listofUpdObj = Database.query(queryUpdate);                                 
										if(listofUpdObj.size()>0)
										{
											System.Debug('Trovato update'); 
											sObj = listofUpdObj.get(0);                                     
										}
									}
								}
							}
							System.Debug(LoggingLevel.ERROR, 'sObj:' +sObj);    
							if(sObj == null)
							{
								System.Debug('Nessun oggetto trovato, ne creo uno nuovo'); 
								sObj = Schema.getGlobalDescribe().get(objTarget).newSObject(); 
							}
							else
								toUpd   =true;
							System.Debug(LoggingLevel.ERROR, 'SourceField: ' +sourceField);
							for(Integer i=0;i<sourceField.size();i++)
							{
								if(!targetField.get(i).equalsIgnoreCase('UpdateKey'))
								{
									sObj.put(targetField.get(i) , queryObject.get(sourceField.get(i)));       
								}
							}
												
							for (String fieldName : value.keySet())
							{
								value.get(fieldName);
								sObj.put(fieldName , value.get(fieldName));
							}                        
							
							//GC 3.4.4
							if(toUpd==false)
							{
								System.Debug('Eseguo Insert'); 
								objectsToInsert.add(sObj);
								//insert sObj;
							}   
							else
							{
								System.Debug('Eseguo Update'); 
								objectsToUpdate.add(sObj);
								//update sObj;   
							}                         
								
							System.debug(LoggingLevel.ERROR, 'Cada mo2' + mo);     
							parentId = (String)sObj.get('Id');                 
							
							targetIdList.add((String)sObj.get('Id'));
							sourceIdList.add((String)queryObject.get('Id'));
											
							sObjectMap.put((String)sObj.get('Id'),sObj);  
							
							//GC 2.0 manage parents
							sObjectSourceMap.put((String)queryObject.get('Id'),sObj);
							system.debug('insert root quer obj id: '+(String)queryObject.get('Id'));
							
							isUpd   =   false;
							toUpd   =   false;
							sObj    =   null;

						}//FIN del FOR



						//GC 3.4.4
						idMapTarget.put(objTarget+objName,targetIdList);
						idMapSource.put(objSource+objName,sourceIdList); 
						System.debug('Map Source: '+ objSource+objName+' SourceList: '+sourceIdList);                          
						Database.insert(objectsToInsert);
						Database.update(objectsToUpdate);
						
						parentId    =   '';
						 System.Debug(LoggingLevel.ERROR, 'SourceId:' +sourceId);
						if(sourceId == null)
						{
							for(sObject objParentIds:objectsToInsert)
								parentId    +=  (String)objParentIds.get('Id')+';';     
							
							for(sObject objParentIds:objectsToUpdate)
								parentId    +=  (String)objParentIds.get('Id')+';';                
						}
						else
						{
							if(objectsToInsert.size() == 1)
								parentId = (String)objectsToInsert.get(0).get('Id');
							else if(objectsToUpdate.size() > 0)
								parentId = (String)objectsToUpdate.get(0).get('Id');
						}
						
						objectsToInsert.clear();
						objectsToUpdate.clear();
						sourceField.clear();
						targetField.clear();    
						if(listofUpdObj!=null)
							listofUpdObj.clear();                
							
						value.clear();
						queryString = '';                                           
					}
					else
					{
						//Generate Child object
						System.Debug('Begin to populate the child item');
						
						//get the parent information
						//MapObject__c parInfo  =   [SELECT Id,Name,Source__c,Target__c,Type__c,Map__c,Parent__c,Filter__c FROM MapObject__c WHERE Map__r.Map_Name__c =: mapName AND Id =: objParent];                       
						NE__MapObject__c parInfo    =   MapOfMapObjects.get(objParent);
						
						parentSource    = parInfo.NE__Source__c;
						parentTarget    = parInfo.NE__Target__c;
						parentParent    = parInfo.NE__Parent__c;    
						parentName      = parInfo.Name;
						
						system.debug('parInfo: '+parInfo);
						system.debug('parentSource: '+parentSource); 
						system.debug('parentTarget: '+parentTarget); 
						system.debug('parentParent: '+parentParent); 
						system.debug('parentName: '+parentName);                        
						
						System.Debug('objName for parent= '+objName);                       
						
						//generate the query
						queryString += 'SELECT Id';
						
						//GC 2.0 load the field description outside the cycle
						//Map<String, Schema.SObjectType> objectList      =   Schema.getGlobalDescribe();
						//Map<String, Schema.SObjectField> objectFields   =   objectList.get(mo.Source__c).getDescribe().fields.getMap();
						
						for(NE__MapObjectItem__c moi : mo.NE__Map_Object_Items__r)
						{
							itemType = moi.NE__Type__c;
							
							//generate the field to get
							if(itemType.equalsIgnoreCase('Field'))
							{
								sourceString    =   moi.NE__SourceField__c;
								targetString    =   moi.NE__TargetField__c;
								
								sourceField.add(moi.NE__SourceField__c);
								targetField.add(moi.NE__TargetField__c);
								
								if(!sourceString.equalsIgnoreCase('Id'))
								{                               
									queryString =   queryString.replaceAll(' ,'+sourceString+' ', '');
									queryString+=' ,'+sourceString+' ';       
								}      
										 
							}
							else if(itemType.equalsIgnoreCase('Value'))
							{
								value.put(moi.NE__TargetField__c,moi.NE__Value__c);
							}
							else if(itemType.equalsIgnoreCase('KeyField') || itemType.equalsIgnoreCase('KeyField-Secondary'))
							{
								system.debug('putting key: '+moi.NE__SourceField__c+' moi.TargetField__c: '+moi.NE__TargetField__c);
								
								//GC 2.0 multiple keyfield management                                                   
								keyfieldmap.put(moi.NE__SourceField__c,moi.NE__TargetField__c);
								
								//Schema.SObjectField objectF   =   objectFields.get(moi.SourceField__c);
								//String relatedTo;
								//if(objectF.getDescribe().getReferenceTo().size() > 0)
								//  relatedTo           =   objectF.getDescribe().getReferenceTo().get(0).getDescribe().getName();
								
								//System.Debug('relatedTo: '+relatedTo+' parInfo.Target__c: '+parInfo.Target__c);
								if(itemType.equalsIgnoreCase('KeyField'))
									parentQuery                 =   moi.NE__SourceField__c;//objectF.getDescribe().getRelationshipName();
									
								queryString+=' ,'+moi.NE__SourceField__c+' ';
				  
							}
							else if(itemType.equalsIgnoreCase('UpdateKey') && updateActive == true)
							{
								updateSourceKey = moi.NE__SourceField__c;
								updateTargetKey = moi.NE__TargetField__c;       
								
								queryString =   queryString.replaceAll(' ,'+updateSourceKey+' ', '');
								queryString+=' ,'+updateSourceKey+' ';
								isUpd   =   true;
												
							}   
							else if(itemType.equalsIgnoreCase('RecordType'))
							{
								rtId = moi.NE__RecordTypeId__c;
								System.Debug('RType for '+parentTarget+' : '+rtId);
								RecordType rtemp = moMap.get(rtId);
								System.Debug('RTObj: '+rtemp);
								value.put('RecordTypeId',moMap.get(rtId).Id);
							} 
							else if(itemType.equalsIgnoreCase('Parent'))
							{
								parentValue.put(moi.NE__SourceField__c,moi.NE__TargetField__c);
							}
						}   
											 
												
						String tmpQuery =   queryString;                                
						
						//Get the Ids for getting the parent-child relationship
						System.debug('SourceList: '+idMapSource.get(parentSource+parentName));
						List<String> sourceList =   idMapSource.get(parentSource+parentName);
						List<String> targetList =   idMapTarget.get(parentTarget+parentName);
											   
						System.Debug('Found '+sourceList.size()+' sources');
						System.Debug('sourceList: '+sourceList);
						
						List<String> targetIdList   = new List<String>();
						List<String> sourceIdList   = new List<String>();
 
						System.Debug('parentSourceId: '+parentSourceId+' parentTargetId: '+parentTargetId);          
						System.debug('QueryString: '+queryString);

						queryString = tmpQuery;
						parentSourceId = String.escapeSingleQuotes(parentSourceId);
						queryString+=' FROM '+String.escapeSingleQuotes(mo.NE__Source__c)+' WHERE '+String.escapeSingleQuotes(parentQuery)+' IN: sourceList '+String.escapeSingleQuotes(objFilter)+objOrder;
						
						System.debug('QueryString FROM: '+String.escapeSingleQuotes(mo.NE__Source__c));
						System.debug('QueryString WHERE: '+String.escapeSingleQuotes(parentQuery));
						System.debug('QueryString IN: '+String.escapeSingleQuotes(objFilter)+objOrder);              
						//end:generate the query
						System.debug('query to be ex: '+queryString);
						
						queryObjectList                     =   Database.query(queryString); //GC 2.0 reduce the number of query 
						   
						queryObjectChildren                 =   new list<sObject>();
						list <SObject> queryObjectChildrenBK=   new list<sObject>();//used to resolve parent

						//for every source founded create the respective childs
						for(Integer j=0;j<sourceList.size();j++ )
						{
							parentSourceId          =   sourceList.get(j);
							parentTargetId          =   targetList.get(j);
							queryObjectChildrenBK   =   new list<SObject>();

							for(sObject qObj:queryObjectList)
							{
								if(qObj.get(parentQuery) == parentSourceId)
									queryObjectChildren.add(qObj);
							}
							
							if(!queryObjectChildren.isEmpty())
							{
								system.debug('queryObjectChildren.size:' +queryObjectChildren.size());               
								//cycle the object query
								for(SObject queryObjectChild:queryObjectChildren)
								{                       
									
									if(isUpd == true)
									{
										if(queryObjectChild.get(updateSourceKey) != null)
										{            
											System.Debug('source diverso da null');
											String upd = (String)queryObjectChild.get(updateSourceKey);  
											if(!upd.equalsIgnoreCase(''))
											{
												//Create the parent object
												Object obj = queryObjectChild.get(updateSourceKey);
												queryUpdate = 'SELECT Id FROM '+String.escapeSingleQuotes(objTarget)+' WHERE '+String.escapeSingleQuotes(updateTargetKey)+' = : obj ';

												listofUpdObj = Database.query(queryUpdate);
												if(listofUpdObj.size()>0)
												{
													System.Debug('Trovato update'); 
													sObj = listofUpdObj.get(0);                                     
												}
											}
										}
									}
									
									if(sObj == null)
									{
										System.Debug('Nessun oggetto trovato, ne creo uno nuovo'); 
										sObj = Schema.getGlobalDescribe().get(objTarget).newSObject(); 
										toUpd=false;    
									}
									else
										toUpd=true; 
													 
									
									for(Integer i=0;i<sourceField.size();i++)
									{
										if(!targetField.get(i).equalsIgnoreCase('UpdateKey'))
										{
											sObj.put(targetField.get(i) , queryObjectChild.get(sourceField.get(i)));
										}
									}
									
									
									for (String fieldName : value.keySet())
									{
										sObj.put(fieldName , value.get(fieldName));                                 
									}
									
									//GC 2.0 manage multikeyfield
									//GC 3.0 cycle on the keyset
									for(String keyName:keyfieldmap.keySet())
									{
										try
										{
											system.debug('Object to map id: '+queryObjectChild.get('id'));
											
											//GC 2.0
											//system.debug('keyName: '+keyName+' keyfieldmap.get(keyName): '+keyfieldmap.get(keyName));
											//system.debug('(String)queryObjectChild.get(keyName): '+(String)queryObjectChild.get(keyName));
											
											sObject parentObj;
											if((String)queryObjectChild.get(keyName) != null && (String)queryObjectChild.get(keyName) != '' )
												parentObj   =   sObjectSourceMap.get((String)queryObjectChild.get(keyName));  
												
											system.debug('parent inside cycle: '+parentObj);   
											if(parentObj != null)       
											{           
												//system.debug('ParentObjId: '+parentObj.get('Id'));                                                            
												sObj.put(keyfieldmap.get(keyName) , parentObj.get('Id'));
											}
										}
										catch(System.Exception e)
										{
											System.debug(e+' at line: '+e.getLineNumber());
											System.debug('Master detail or update error');
										}
									}
									
									
									//Parent value
									for (String parFieldName : parentValue.keySet())
									{
										try
										{
											System.Debug('parentTargetId '+parentTargetId);
											System.Debug(sObjectMap);
											
											Sobject o   =   sObjectMap.get(parentTargetId);
											System.Debug('Parent value object: '+o);
											
											String val = (String)o.get(parFieldName);
																					
											System.Debug('Parent value data: '+val+ ' For parent field: '+parentValue.get(parFieldName));
											sObj.put(parentValue.get(parFieldName),val);    
										}    
										catch(System.Exception e)
										{
											System.debug('Master detail or update error');
										}                                   
									}
									
			
									if(toUpd==false)
									{
										System.Debug('Eseguo Insert'); 
										//insert sObj;      
										objectsToInsert.add(sObj); 
									}   
									else
									{
										objectsToUpdate.add(sObj); 
										//update sObj;                            
									}                                     
									
									sourceIdList.add((String)queryObjectChild.get('Id'));    
									system.debug('insert quer obj id: '+(String)queryObjectChild.get('Id'));   
									sObjectSourceMap.put((String)queryObjectChild.get('Id'),sObj);                                          
									
									sObj    =   null;
									if(listofUpdObj!=null)
										listofUpdObj.clear();       
								}
								
								queryObjectChildrenBK.addAll(queryObjectChildren);
								queryObjectChildren.clear(); //gc 3.2
							}
							
							//queryObjectChildren.clear();//gc 2.0
						}
						
						//GC 2.0 manage multiple keyfields  
						Database.insert(objectsToInsert);
						Database.update(objectsToUpdate);
						
						for(sobject o:objectsToInsert)
						{
							targetIdList.add((String)o.get('Id'));
							sObjectMap.put((String)o.get('Id'),o); 
						}
							
						for(sobject o:objectsToUpdate)
						{
							targetIdList.add((String)o.get('Id'));   
							sObjectMap.put((String)o.get('Id'),o); 
						}

						//Store the information regarding the id of the target and the source in a new list
						idMapTarget.put(objTarget+objName,targetIdList);
						idMapSource.put(objSource+objName,sourceIdList);


						//GC 2.0 manage multikeyfield
						//GC 3.0 cycle on the keyset       
						//GC 3.2 resolve the parents AFTER the insert     
						objectsToUpdate.clear();            
						for(SObject queryObjectChild:queryObjectChildrenBK)
						{   
							SObject objToUpd    =   sObjectSourceMap.get(String.valueof(queryObjectChild.get('id')));
							system.debug('Object to map id: '+queryObjectChild.get('id'));
							Boolean toUpdKey    =   false;
							if(objToUpd != null)
							{
								for(String keyName:keyfieldmap.keySet())
								{
									try
									{           
										//GC 2.0
										system.debug('keyName: '+keyName+' keyfieldmap.get(keyName): '+keyfieldmap.get(keyName));
										system.debug('(String)queryObjectChild.get(keyName): '+(String)queryObjectChild.get(keyName));
										
										sObject parentObj;
										if((String)queryObjectChild.get(keyName) != null && (String)queryObjectChild.get(keyName) != '' )
											parentObj   =   sObjectSourceMap.get((String)queryObjectChild.get(keyName));  
											
										system.debug('object parentObj: '+parentObj);   
										if(parentObj != null)       
										{           
											system.debug('In obj: '+objToUpd.get('Id')+' putting in field: '+keyfieldmap.get(keyName)+' value: '+parentObj.get('Id'));                                              
											objToUpd.put(keyfieldmap.get(keyName) , parentObj.get('Id'));                                           
											toUpdKey    =   true;
										}
									}
									catch(System.Exception e)
									{
										System.debug(e+' at line: '+e.getLineNumber());
										System.debug('Master detail or update error');
									}
								} 
								
								if(toUpdKey)
									objectsToUpdate.add(objToUpd);
							}                           
						}    
						
						if(objectsToUpdate.size() > 0)
						{
						//Remove duplicates
							set<SObject>    setObjToUpd =   new set<SObject>();
							setObjToUpd.addAll(objectsToUpdate);
							objectsToUpdate.clear();
							objectsToUpdate.addAll(setObjToUpd);
							
							Database.update(objectsToUpdate);    
						}

					   
						keyfieldmap.clear();//gc 2.0
						sourceField.clear();
						targetField.clear();
						objectsToInsert.clear();//gc 2.0
						objectsToUpdate.clear();//gc 2.0          
						value.clear();
						parentValue.clear();
						queryString = '';
						isUpd   =   false;
						toUpd   =   false;  
						sObj    =   null;       
					}
				}   
			}
			else
			{
				System.debug('No map found');
				ErrorCode    =  '-1';
				parentId     =  '';
				ErrorMessage =  'No map found';
			}   
		}
		catch(System.Exception e)
		{
			System.debug('Generic Error: '+e.getMessage()+' at line: '+e.getLineNumber());
			ErrorCode    =  '-1';
			ErrorMessage =  e.getMessage();
			parentId     =  '';
			
			DataBase.rollback(savePointDatamap);
		}

		System.debug('End');
		
		result.put('ErrorMessage',ErrorMessage);
		result.put('ErrorCode',ErrorCode);
		result.put('ParentId',parentId);
		
		system.debug('result: '+result);
		
		return result;
	}
	
	//GC 3.2 new datamap call
	global DataMapResponse callDataMap(DataMapRequest req)
	{
		DataMapResponse resp    =   new DataMapResponse();
		Datamap_Copy dmap           =   new Datamap_Copy();
		Map<String,String> mRes =   new Map<String,String>();
		
		dmap.mapOfCustomQueries =   req.mapOfCustomQueries;
		dmap.listOfPlaceHolders =   req.listOfPlaceHolders;
		dmap.updateActive       =   req.updateActive;
		mRes                    =   dmap.GenerateMapObjects(req.mapName, req.sourceId);
		
		resp.ErrorMessage       =   mRes.get('ErrorMessage');
		resp.ErrorCode          =   mRes.get('ErrorCode');
		resp.ParentId           =   mRes.get('ParentId');
		
		return resp;
	}   
	
	global class DataMapResponse
	{
		global String ErrorMessage;
		global String ErrorCode;
		global String ParentId;
		
		global DataMapResponse()
		{
			ErrorMessage =  '';
			ErrorCode    =  '0';
			ParentId     =  '';         
		}
	}
	
	
	global class DataMapRequest
	{
		global String  mapName;
		global String  sourceId;
		global map<String,String> mapOfCustomQueries;
		global list<String> listOfPlaceHolders;
		global boolean updateActive;
		
		global DataMapRequest()
		{
			mapOfCustomQueries  =   new map<String,String>();
			listOfPlaceHolders  =   new list<String>();
			updateActive        =   false;
		}
	}    
	
	
}