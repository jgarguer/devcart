public class BI_COL_EmailUseful_cls {
	
	private Messaging.SingleEmailMessage singleEmailMessage;
	private final List<String> toAddresses;
	//optional parameters set to default       
	private String subject = '';
	private String htmlBody = '';
	private Boolean useSignature = false;
	private List<Messaging.EmailFileAttachment> fileAttachments = null;
	//defaults to current user's first name + last name
	private String senderDisplayName = UserInfo.getFirstName()+' '+UserInfo.getLastName();
	//get the current user in context
	User currentUser = [Select email from User where username = :UserInfo.getUserName() limit 1];       
	//replyTo defaults to current user's email
	private String replyTo = currentUser.email;
	private String plainTextBody = '';
	
	public BI_COL_EmailUseful_cls(List<String> addresses) 
	{
		this.toAddresses = addresses;
	}
	
	public BI_COL_EmailUseful_cls senderDisplayName(String val) 
	{
		senderDisplayName = val;
		return this;
	}

	public BI_COL_EmailUseful_cls subject(String val) 
	{
		subject = val;
		return this;
	}

	public BI_COL_EmailUseful_cls htmlBody(String val) 
	{
		htmlBody = val;
		return this;
	}

	public BI_COL_EmailUseful_cls useSignature(Boolean bool) 
	{
		useSignature = bool;
		return this;
	}

	public BI_COL_EmailUseful_cls replyTo(String val) 
	{
		replyTo = val;
		return this;
	}

	public BI_COL_EmailUseful_cls plainTextBody(String val) 
	{
		plainTextBody = val;
		return this;
	}
	
	public BI_COL_EmailUseful_cls fileAttachments(List<Messaging.Emailfileattachment> val) 
	{
		fileAttachments = val;
		return this;
	}

	//where it all comes together
	//this method is private and is called from sendEmail()
	private BI_COL_EmailUseful_cls build() 
	{
		singleEmailMessage = new Messaging.SingleEmailMessage();
		singleEmailMessage.setToAddresses(this.toAddresses);
		singleEmailMessage.setSenderDisplayName(this.senderDisplayName);
		singleEmailMessage.setSubject(this.subject);
		singleEmailMessage.setHtmlBody(this.htmlBody);
		singleEmailMessage.setUseSignature(this.useSignature);
		singleEmailMessage.setReplyTo(this.replyTo);
	//	singleEmailMessage.setPlainTextBody(this.plainTextBody);
		singleEmailMessage.setFileAttachments(this.fileAttachments);
		return this;
	}

	//send the email message
	public boolean sendEmail() 
	{
		boolean retorno=true;
		try 
		{
			//call build first to create the email message object
			build();
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { singleEmailMessage }); 
			
		} 
		catch (Exception ex) 
		{
			//throw new GenericException('There was a problem while calling Messaging.sendEmail()');
			System.debug('There was a problem while calling Messaging.sendEmail() '+ex.getMessage());
			retorno=true;
		}
		return retorno;
	}
	
	/* ===================================
		Tests:  
		
	======================================= 
	private static testMethod void test1()
	{
		List<String> toAddresses = new List<String> {'john.doe@acme.org'};
		String replyToAddress = 'john.duh@acme.org';
		BI_COL_EmailUseful_cls emailUtil= new BI_COL_EmailUseful_cls(toAddresses);
		emailUtil.htmlBody('<b>I want my Ring back Frodo Baggins.</b>')
				.senderDisplayName('Saurons Eye')
				.subject('One Ring to Rule them all')
				.useSignature(true)
				.replyTo(replyToAddress)
				.sendEmail();
	}*/

}