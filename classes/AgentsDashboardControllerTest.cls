@isTest
public class AgentsDashboardControllerTest{
    
    static testMethod void doLoadTest(){
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        
        insert u;
        
        System.runAs(u){
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            Group queue=new Group(Type = 'Queue',Name='SMC L1 Ordering');
            insert queue;
            PageReference p = Page.AgentsDashboard;
            Test.setCurrentPage(p);
            AgentsDashboardController contr = new AgentsDashboardController();
            List<Group> groupList=contr.getQueues();
            List<GroupMember>gmList=new List<GroupMember>();
            GroupMember gm;
            for(Group g:groupList){
                gm=new GroupMember();
                gm.GroupId=g.Id;
                gm.UserOrGroupId=u.Id;
                gmList.add(gm);
            }
            if(!gmList.isEmpty()) {
                System.debug('Group Member List is ' + GmList);
                insert gmList;
            }
            Case testCase=TGS_Dummy_Test_Data.dummyCaseTGS('Order_Management_Case',  'On Behalf');
            testCase.TGS_Assignee__c=u.id;
            testCase.OwnerId= groupList[0].Id;
            insert testCase;
            //SELECT Name, (SELECT RecordType.Name FROM Cases__r) FROM User WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :queuesList[0].Id)
            
            contr.getAgents();
            //do test on resultList
        
        }
    }
}