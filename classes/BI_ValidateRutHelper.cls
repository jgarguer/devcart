public with sharing class BI_ValidateRutHelper {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Helper class that check if RUT code is valid.
    Test Class:    BI_ValidateRutHelper_TEST

    History: 
    
    <Date>                     <Author>                <Change Description>
    20/01/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public static Boolean check_RUT_chile(String rut){
		//system.debug('RUT INFO: length-> ' + rut.length());
		//system.debug('RUT INFO: NUM-> ' + rut.substring(0,rut.length()-1));
		//system.debug('RUT INFO: DASH-> ' + rut.substring(rut.length()-1,rut.length()));
		//system.debug('RUT INFO: CONTROL-> ' + rut.substring(rut.length()-1,rut.length()));
		
		//if(rut != null && rut.length() == 9 &&  rut.substring(0,8).isNumeric() && rut.substring(8,9).isAlphanumeric()  ){ // XXXXXXXX-Y
        try{
			if(rut != null && rut.length()>=2 && get_ctrlChar(rut.substring(0,rut.length()-1)) == rut.substring(rut.length()-1,rut.length()).toUppercase()){
				return true;
			}else{
				system.debug('RUT NOT VALID:' + rut);
				return false;	
			}
        }catch(Exception exc){
            system.debug('Validate RUT Exception: ' + exc);
            return false;
        } 
		//}else{
		//	system.debug('RUT NO CORRECT FORMAT: XXXXXXXXY:' + rut);
		//	return false;	
		//}
		
	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that get the control char

    History: 
    
     <Date>                     <Author>                <Change Description>
    20/01/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static String get_ctrlChar(String rut){
		if(rut != null){
		   Integer ret = get_sum(invert_string(rut));
            if(ret == 10){
                return 'K';
            }else if(ret == 11){
            	return '0';
            }else{
            	return String.valueOf(ret);
            }
		}else{
			return null;
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that invert a string

    History: 
    
     <Date>                     <Author>                <Change Description>
    20/01/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static String invert_string(String cadena) {
		String cadenaInvertida = '';
		List<String>lst_char = get_charList(cadena);

		for (Integer i = lst_char.size() - 1; i >= 0; i-=1) {
			cadenaInvertida = cadenaInvertida + lst_char[i];
		}
		return cadenaInvertida;
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that get a list of chars

    History: 
    
     <Date>                     <Author>                <Change Description>
    20/01/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static List<String> get_charList(String cad){
		List<String> toRet = new List<String>();
		Integer i= 0;
		do{
			toRet.add(cad.substring(i,i+1));
			i+=1;
		}while(i < cad.length());

		return toRet;
	}
 
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Micah Burgos
     Company:       Salesforce.com
     Description:   Method that get the sum of module 11 algorithm 
 
     History: 
     
      <Date>                     <Author>                <Change Description>
     20/01/2015                  Micah Burgos             Initial Version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static Integer get_sum(String cadena) {
		Integer pivote = 2;
		Integer longitudCadena = cadena.length();
		Integer cantidadTotal = 0;
		Integer b = 1;
		for (Integer i = 0; i < longitudCadena; i++) {
			if (pivote == 8) {
				pivote = 2;
			}
			Integer temporal = Integer.valueOf('' + cadena.substring(i, b));
			b++;
			temporal *= pivote;
			pivote++;
			cantidadTotal += temporal;
		}
		cantidadTotal = 11 - Math.mod(cantidadTotal,11);
		return cantidadTotal;
	}
}