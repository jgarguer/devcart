@isTest
public class TGS_BillingProfile_Handler_TEST {
    
    static testMethod void fillLastModifiedFD_test(){
        
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
            
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        
       
        
        System.runAs(userTest){
            
            Account testAccount = TGS_Dummy_Test_Data.dummyHierarchy();
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:testAccount.Id];
        	Account acCC = [Select Id, ParentId FROM Account WHERE Id=:testAccount.ParentId];
            NE__Billing_Profile__c BillingProfile = new NE__Billing_Profile__c (NE__Account__c = testAccount.id,
                                                                               
                                                                               TGS_Billing_Type__c = 'Distributed',
                                                                               TGS_Invoice_Postal_Code__c = ''
                                                                  			   );
            insert BillingProfile;
            Test.startTest(); 
            NE__Billing_Profile__c bP = [SELECT id, TGS_Invoice_Postal_Code__c from  NE__Billing_Profile__c where id = :BillingProfile.Id];
            System.debug(bp.Id);
            bP.TGS_Invoice_Postal_Code__c = 'test';
            update bP;
          
                                                                            
            
            
          Test.stopTest();   
        }
    }
    
}