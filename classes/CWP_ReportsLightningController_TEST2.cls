/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for testing CWP_ReportsLightningController
    Test Class:    CWP_ReportsLightningController
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/04/2017              Everis                    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest(SeeAllData='true')
private class CWP_ReportsLightningController_TEST2 {    
    @isTest
    private static void getReportListTest() {     
        system.debug('test 2');
         CWP_ReportsLightningController constructor = new CWP_ReportsLightningController();
        /*Attachment att =[SELECT id FROM Attachment WHERE Name =: 'att'];
        CWP_ReportsLightningController.AttImg = att;
        BI_ImageHome__c home= [SELECT id FROM BI_ImageHome__c WHERE Name =: 'home'];
        CWP_ReportsLightningController.ImgBack=home;
        CWP_ReportsLightningController.RelationImage=home;
        CWP_ReportsLightningController.RelationImageMovi=home;
        CWP_ReportsLightningController.RelationImageTel=home;
        CWP_ReportsLightningController.tokenApex = 'token';    
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        */
        //List<String> reports = new List<String>{'00Ow0000006PI27EAG', '00Ow0000006PI2BEAW', '00Ow0000006PI2DEAW', '00Ow0000006PI2EEAW', '00Ow0000006PI2FEAW', '00Ow0000006PI2HEAW', '00Ow0000006PI2IEAW', '00Ow0000006PI2JEAW', '00Ow0000006nV0rEAE'};
        //System.runAs(usuario){  
            List<Report> repList = [SELECT Id FROM Report LIMIT 5];
            if(!repList.isEmpty()){
                List<String> reports = new List<String>();
                for(Report r:repList){
                    reports.add(r.Id);
                }
                Test.startTest();                      
                CWP_ReportsLightningController.getReportList( 0,5, reports, 'LastModifiedDate', true);  
                Test.stopTest();
            }
        //}       
        
    } 
}