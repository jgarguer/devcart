/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo Corral
Company:       Accenture
Description:   Controller for Salesforce1 custom tabs, configured with customMetadata

History: 
<Date>                  <Author>                <Change Description>
30/11/2017              Antonio Pardo Corral    Initial Version    
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_SF1_Tab_Ctrl {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo Corral
Company:       Accenture
Description:   retrieves the configuration for the tab, stored in the BI_SF1_Custom_tabs_configuration__mdt custom metadata, given an Object name, the 
               metadata configuration is stored in the key 'metadataConfig'

IN:            String MasterLabel - Object Api name
OUT:           Map<String, Object> - map with the values needed in the component

History: 
<Date>                  <Author>                <Change Description>
30/11/2017              Antonio Pardo corral    Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static Map<String, Object> getInfo(String MasterLabel){
        
        Map<String, Object> map_return = new Map<String, Object>();

        BI_SF1_Custom_tabs_configuration__mdt metadataConfig = [SELECT DeveloperName, MasterLabel, Label,BI_SF1_Campo_descripcion__c,
                                                                BI_SF1_Icono_de_la_cabecera__c, BI_SF1_Filtro_de_busqueda__c, BI_SF1_Titulo_de_la_cabecera__c,
                                                                (SELECT DeveloperName, MasterLabel, Label, BI_SF1_Custom_tab_configuration__c, BI_SF1_filtro_query__c, BI_SF1_Campos_a_mostrar__c, BI_SF1_Vista_por_defecto__c FROM BI_SF1_Custom_tab_listView_configuration__r ORDER BY BI_SF1_Vista_por_defecto__c desc)
                                                                FROM BI_SF1_Custom_tabs_configuration__mdt WHERE MasterLabel = :MasterLabel LIMIT 1];
        map_return.put('metadataConfig', metadataConfig);
        return map_return;
    }

}