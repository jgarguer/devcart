@isTest
Global class BI_COL_CancelacionMS_tst
{    
    public static User                                  objUsuario;
    public static Account                               objCuenta;
    public static Contact                               objContacto;
    public static Opportunity                           objOppty;
    public static BI_COL_Anexos__c                      objAnexos;
    public static BI_COL_Descripcion_de_servicio__c     objDesSer;
    public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;
    public static BI_COL_Modificacion_de_Servicio__c    objModSer;
    public static BI_Log__c                             objBiLog;

    public static List<User>                                lstUsuarios;
    public static List<Profile>                             lstPerfil;
    public static List<UserRole>                            lstRoles;
    public static List<Opportunity>                         lstOppAsociadas;
    public static List<RecordType>                          rtBI_FUN;
    public static List<BI_COL_Modificacion_de_Servicio__c>  lstModSer;

    global class wsmock implements WebServiceMock
    {
    
        global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType )
        {   
            
            ws_TrsMasivo.ArrayOfrespuestacancelarInstalaciones_element objResp = new ws_TrsMasivo.ArrayOfrespuestacancelarInstalaciones_element();
            objResp.instalacionRespuesta = new List<ws_TrsMasivo.cancelarInstalacionesResponse>();
            
            response.put( 'response_x', objResp.instalacionRespuesta);
        }
    }
    
    static void crearData ()
    {

        //lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];

        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        //insert objUsuario;

        //lstUsuarios = new List<User>();
        //lstUsuarios.add(objUsuario);

        //System.runAs(lstUsuarios[0])
        //{
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;

            objCuenta                                       = new Account();
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
            objCuenta.CurrencyIsoCode                       = 'GTQ';
            // 19/09/2017        Angel F. Santaliestra Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
            // END 19/09/2017    Angel F. Santaliestra
            insert objCuenta;

            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto; 

            System.debug('\n\n\n ======= CUENTA ======\n '+ objCuenta );
            objOppty                                            = new Opportunity();
            objOppty.Name                                       = 'prueba opp';
            objOppty.AccountId                                  = objCuenta.Id;
            objOppty.BI_Country__c                              = 'Colombia';
            objOppty.CloseDate                                  = System.today().addDays(1);
            objOppty.StageName                                  = 'F6 - Prospecting';
            objOppty.CurrencyIsoCode                            = 'COP';
            objOppty.Certa_SCP__contract_duration_months__c     = 12;
            objOppty.BI_Plazo_estimado_de_provision_dias__c     = 0 ;
            objOppty.BI_Fecha_de_vigencia__c                    = System.today().addDays(12);
            //objOppty.OwnerId                                    = lstUsuarios[0].id;
            insert objOppty;
            //lstOppAsociadas.add(objOppty);

            System.debug('\n\n\n ======= objOppty ======\n '+ objOppty );
            //insert lstOppAsociadas;

            rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            objAnexos                   = new BI_COL_Anexos__c();
            objAnexos.Name              = 'FUN-0041414';
            objAnexos.RecordTypeId      = rtBI_FUN[0].Id;
            insert objAnexos;
            System.debug('\n\n\n ======== objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);

            objDesSer                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c     = objOppty.Id;
            //objDesSer.BI_COL_Oportunidad__c     = lstOppAsociadas[0].Id;
            objDesSer.CurrencyIsoCode           = 'COP';
            insert objDesSer;
            System.debug('\n\n\n ======= objDesSer '+ lstModSer);

            //objModSer                                               = new BI_COL_Modificacion_de_Servicio__c();
            //objModSer.BI_COL_FUN__c                                 = objAnexos.Id;
            //objModSer.BI_COL_Codigo_unico_servicio__c               = objDesSer.Id;
            //objModSer.BI_COL_Clasificacion_Servicio__c              = 'ALTA';
            //objModSer.CurrencyIsoCode                               = 'COP';
            //objModSer.BI_COL_Fecha_instalacion_servicio_RFS__c      = Date.today().addDays(30);
            ////objModSer.BI_COL_Direccion_IP__c            = i+'';
            //insert objModSer;
            //lstModSer.add(objModSer);
            //insert lstModSer;
            //MS
            objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
            objModSer.BI_COL_FUN__c                         = objAnexos.Id;
            objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModSer.BI_COL_Oportunidad__c                 = objOppty.Id;
            objModSer.BI_COL_Bloqueado__c                   = false;
            objModSer.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            insert objModSer;

            objBiLog                                    = new BI_Log__c();
            objBiLog.BI_COL_Informacion_Enviada__c      = 'Informacion Enviada';
            objBiLog.BI_COL_Informacion_recibida__c     = 'OK procesado correctamente';         
            objBiLog.BI_COL_Estado__c                   = 'Sincronizado';
            objBiLog.BI_COL_Interfaz__c                 = 'NOTIFICACIONES'; 
            objBiLog.BI_COL_Modificacion_Servicio__c    = objModSer.Id;
            insert objBiLog;
        //}

    }

    @isTest
    public static void methodtest1()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();
            objModSer.BI_COL_Estado__c = 'Cerrada perdida';
            update objModSer;

            lstModSer = new List<BI_COL_Modificacion_de_Servicio__c>();
            lstModSer = [select BI_COL_FUN__c, BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
                                BI_COL_Oportunidad__c, BI_COL_Bloqueado__c, BI_COL_Estado__c, BI_COL_Sucursal_de_Facturacion__c,
                                BI_COL_Sucursal_Origen__c From BI_COL_Modificacion_de_Servicio__c];
            System.debug('/n lstModSer ==============> '+lstModSer);
            List<String> lstIdMS = new List<String>();
            lstIdMS.add(lstModSer.get(0).Id);

            Test.startTest();

                Test.setMock( WebServiceMock.class, new wsmock() );

                BI_COL_CancelacionMS_cls.invokeapexcallout(lstModSer);
                BI_COL_CancelacionMS_cls.envioDatosTR(lstIdMS);
            Test.stopTest();
        }
    }


}