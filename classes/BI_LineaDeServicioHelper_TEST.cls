/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LineaDeServicioHelper method
            
    History:  
    
    <Date>                  <Author>                <Change Description>
    10/04/2015              Raul Aguera             Initial Version
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class BI_LineaDeServicioHelper_TEST {

    static{
        
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LineaDeServicioHelper method
            
    History:  
    
    <Date>                  <Author>                <Change Description>
    10/04/2015              Raul Aguera             Initial Version
    03/02/2016              Guillermo Muñoz         Fix bugs caused by news validations rules
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void deleteLineaServicio() {
        
        Test.StartTest();
        
        User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

        List<BI_Linea_de_Servicio__c> lst_serv;
        system.runAs(usu){
            lst_serv = BI_DataLoadAgendamientos.loadLineaServicio(1);
        }
        
        system.assertEquals( lst_serv.isEmpty(), false );
        
        BI_LineaDeServicioHelper.eliminarLineaDeServicio(lst_serv); 
        
        List<BI_Linea_de_Servicio__c> lstdeleted = [ SELECT Id FROM BI_Linea_de_Servicio__c LIMIT 1];
        
        system.assertEquals( lstdeleted.isEmpty(), true);
        
        Test.stopTest();        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LineaDeServicioHelper method
            
    History:  
    
    <Date>                  <Author>                <Change Description>
    10/04/2015              Raul Aguera             Initial Version
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void insertLineaServicio() {
        
        set<Id> setModeloId = new set<Id>();
        
        list <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];
        
        list<BI_Modelo__c> lstBIModelo = new list<BI_Modelo__c>();
        BI_Modelo__c modelo;
        for (Integer i=0;i<10;i++){
            modelo = new BI_Modelo__c(
                Name = 'test servicio ' + i,
                RecordTypeId = lst_rt[0].Id
            );
            lstBIModelo.add( modelo );
        }
                
        insert lstBIModelo;
        
        for( BI_Modelo__c inBIModelo : lstBIModelo ){           
            setModeloId.add( inBIModelo.Id );
        }
        
        list<BI_Linea_de_Recambio__c> lstLineaRecambio = BI_DataLoadAgendamientos.loadLineaRecambio_Inicial( 3 );
        
        list<BI_Linea_de_Servicio__c> lstCheck = [ SELECT Id FROM BI_Linea_de_Servicio__c WHERE BI_Modelo__c IN :setModeloId];
        
        system.assertEquals( lstCheck.isEmpty(), false );       
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LineaDeServicioHelper method
            
    History:  
    
    <Date>                  <Author>                <Change Description>
    10/04/2015              Raul Aguera             Initial Version
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void insertLineaServicioModeloNull() {
        Test.StartTest();
        
		BI_Linea_de_Servicio__c lineaServicio = new BI_Linea_de_Servicio__c();
		lineaServicio.BI_Accion__c = 'test Action';
		lineaServicio.BI_Estado_de_linea_de_servicio__c = 'test Servicio';
		lineaServicio.BI_Numero_de_telefono__c = '645885522';
		
		try
		{	
			insert lineaServicio;
		}
		catch (Exception e)
		{
			System.assert(e.getMessage().contains('Por favor, rellene los campos obligatorios'));
		}

        Test.stopTest();       
        
    }
    
}