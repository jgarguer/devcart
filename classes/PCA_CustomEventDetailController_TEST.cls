@isTest
private class PCA_CustomEventDetailController_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_CustomEventDetailController class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    13/08/2014              Ana Escrich             Initial Version
    06/04/2015              Juan Santisi            Removed LookUp dependencies
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_CustomEventDetailController.loadInfo
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    13/08/2014                  Ana Escrich             Initial Version
    27/09/2017                  Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getloadInfo() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //insert usr;DurationInMinutes__c
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event__c Evento2; 
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_CustomEventDetail');
            Test.setCurrentPage(pageRef);
            /**/DateTime day = Datetime.NOW();
            DateTime day10 = day.AddMinutes(10); 
            AccountTeamMember AccMem = new AccountTeamMember(UserId=usr.Id,TeamMemberRole='Apoderado',AccountId=acc[0].Id);
            insert AccMem; 
            system.runAs(user1){
             Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=con.get(0).LastName);
            insert Evento2;
            Event__c Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=String.valueOf(usr.Id));
            insert Evento1; 
            }           
            /*EventRelation EvRel1 = new EventRelation(RelationId=con[0].Id,EventId=Evento2.Id);
            insert EvRel1;     */
            system.runAs(user1){              
                ApexPages.currentPage().getParameters().put('idUser',  user1.Id);
                ApexPages.currentPage().getParameters().put('nameUser', user1.LastName);  
                ApexPages.currentPage().getParameters().put('edit', 'true');
                ApexPages.currentPage().getParameters().put('id', Evento2.Id); 
                ApexPages.currentPage().getParameters().put('pastday', 'true');
                ApexPages.currentPage().getParameters().put('day', string.valueof(day)); 
                ApexPages.currentPage().getParameters().put('pastday', 'true');
                ApexPages.currentPage().getParameters().put('format',  string.valueof(day.format()));
                ApexPages.currentPage().getParameters().put('FromChater',  'true');       
                PCA_CustomEventDetailController constructor = new PCA_CustomEventDetailController();
                BI_TestUtils.throw_exception = false;
                constructor.checkPermissions();
                constructor.loadInfo();                                
                constructor.createEvent();  
                constructor.edit();                              
                constructor.save();
                constructor.cancel();
                constructor.deleteEvent();
                system.assertEquals(user1.Id, ApexPages.currentPage().getParameters().get('idUser'));
                //system.assertEquals(EvRel1.EventId, Evento2.Id);
            }
        }
    }
       static testMethod void getloadInfo2() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event__c Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_CustomEventDetail');
            Test.setCurrentPage(pageRef);
            DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
            system.runAs(user1){
            Event__c Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=day,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=String.valueOf(usr.Id));
            insert Evento2;
            Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=day,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=String.valueOf(usr.Id));
            insert Evento1; 
            }           
            /*EventRelation EvRel1 = new EventRelation(RelationId=con[0].Id,EventId=Evento1.Id);
            insert EvRel1; */
            system.runAs(user1){                   
                ApexPages.currentPage().getParameters().put('idUser',  user1.Id);
                ApexPages.currentPage().getParameters().put('nameUser', user1.LastName);  
                ApexPages.currentPage().getParameters().put('edit', 'false');
                ApexPages.currentPage().getParameters().put('id', Evento1.Id);
                ApexPages.currentPage().getParameters().put('day', 'Mon SEP 30 00:00:00 GMT 2014'); 
                ApexPages.currentPage().getParameters().put('pastday', 'false');
                ApexPages.currentPage().getParameters().put('format',  string.valueof(day.format()));    
                ApexPages.currentPage().getParameters().put('FromChater',  'false');                                
                PCA_CustomEventDetailController constructor = new PCA_CustomEventDetailController();
                BI_TestUtils.throw_exception = false;
                constructor.checkPermissions();
                constructor.loadInfo();                                
                constructor.createEvent();         
                constructor.edit();                       
                constructor.save();
                constructor.cancel();
                constructor.deleteEvent();
                
            }               
        }
        
    
}
       static testMethod void getloadInfo3() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event__c Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_CustomEventDetail');
            Test.setCurrentPage(pageRef);
            /*DateTime day = Datetime.NOW();
            DateTime day10 = day.AddMinutes(10); */
            DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
            
            Event Event1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
            insert Event1;
            EventRelation EvRel1 = new EventRelation(RelationId=con[0].Id,EventId=Event1.Id);
            insert EvRel1; 
            
            
            
            system.runAs(user1){
            Event__c Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
            insert Evento2;
            Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
            insert Evento1; 
                
            
            
            /*}           
            
            system.runAs(user1){    */ 
                ApexPages.currentPage().getParameters().put('idUser',  user1.Id);
                ApexPages.currentPage().getParameters().put('nameUser', user1.LastName);  
                ApexPages.currentPage().getParameters().put('edit', 'false');
                ApexPages.currentPage().getParameters().put('id', Event1.Id);
                ApexPages.currentPage().getParameters().put('day', 'Mon SEP 30 00:00:00 GMT 2014');//string.valueof(day)); 
                ApexPages.currentPage().getParameters().put('pastday', 'false');
                ApexPages.currentPage().getParameters().put('format',  string.valueof(day.format()));    
                ApexPages.currentPage().getParameters().put('FromChater',  'false');                                
                PCA_CustomEventDetailController constructor = new PCA_CustomEventDetailController();
                BI_TestUtils.throw_exception = false;
                constructor.checkPermissions();
                constructor.loadInfo();                                
                constructor.createEvent();           
                constructor.edit();                     
                constructor.save();
                constructor.cancel();
                constructor.deleteEvent();
                
            }               
        }   
    }
       static testMethod void getloadInfo4() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        List<User> usr2 = BI_DataLoad.loadUsers(1,BI_DataLoad.searchAdminProfile(),Label.BI_Administrador);
        system.runAs(usr2[0]){
            //Region__c pais = new Region__c(Name = 'test'+ String.valueOf(3), BI_Codigo_ISO__c = String.valueOf(3));
            //insert pais;
            Account acc2 = new Account(Name = 'test'+ String.valueOf(3),
                                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Segment__c = 'test', //28/09/2017
                                      BI_Subsegment_Regional__c = 'test', //28/09/2017
                                      BI_Territory__c = 'test' //28/09/2017
                                      );
            insert acc2;                                              
            AccountTeamMember AccMem2 = new AccountTeamMember(UserId=usr2[0].Id,TeamMemberRole='Apoderado',AccountId=acc2.Id);
            insert AccMem2; 
        }
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event__c Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            AccountTeamMember AccMem = new AccountTeamMember(UserId=usr.Id,TeamMemberRole='Apoderado',AccountId=acc[0].Id);
            insert AccMem; 
            Hierarchy__c hierar1 = new Hierarchy__c(User__c=usr.Id, Contact__c=con[0].Id);
            insert hierar1;
            PageReference pageRef = new PageReference('PCA_CustomEventDetail');
            Test.setCurrentPage(pageRef);
            /*DateTime day = Datetime.NOW();
            DateTime day10 = day.AddMinutes(10); */
            DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
            system.runAs(user1){
            Event__c Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=String.valueOf(usr.Id));
            insert Evento2;
            Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=String.valueOf(usr.Id));
            insert Evento1; 
            }           
            /*EventRelation EvRel1 = new EventRelation(RelationId=con[0].Id,EventId=Evento1.Id);
            insert EvRel1; */
            system.runAs(user1){     
                ApexPages.currentPage().getParameters().put('edit', 'false');
                ApexPages.currentPage().getParameters().put('id', null);
                ApexPages.currentPage().getParameters().put('day', 'Mon SEP 30 00:00:00 GMT 2014');//string.valueof(day)); 
                ApexPages.currentPage().getParameters().put('pastday', 'false');
                ApexPages.currentPage().getParameters().put('format',  string.valueof(day.format()));    
                ApexPages.currentPage().getParameters().put('FromChater',  'false');                                
                PCA_CustomEventDetailController constructor = new PCA_CustomEventDetailController();
                BI_TestUtils.throw_exception = false;
                constructor.checkPermissions();
                constructor.loadInfo();                                
                constructor.createEvent();       
                constructor.edit();                         
                constructor.save();
                constructor.cancel();
                constructor.deleteEvent();
                
            }
            system.runAs(user1){
                ApexPages.currentPage().getParameters().put('edit', 'false');
                ApexPages.currentPage().getParameters().put('id', null);
                ApexPages.currentPage().getParameters().put('day', 'Mon SEP 30 00:00:00 GMT 2014');//string.valueof(day)); 
                ApexPages.currentPage().getParameters().put('pastday', 'false');
                ApexPages.currentPage().getParameters().put('format',  string.valueof(day.format()));    
                ApexPages.currentPage().getParameters().put('FromChater',  'false');                                
                PCA_CustomEventDetailController constructor = new PCA_CustomEventDetailController();
                constructor.checkPermissions();
                BI_TestUtils.throw_exception = true;
                constructor.loadInfo();       
                constructor.edit();                           
                constructor.save();
                constructor.cancel();
                constructor.deleteEvent();
            }               
        }   
    }   
    
    static testMethod void getloadInfo5() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event__c Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_CustomEventDetail');
            Test.setCurrentPage(pageRef);
            /*DateTime day = Datetime.NOW();
            DateTime day10 = day.AddMinutes(10); */
            DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
            
            Event Event1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
            insert Event1;
            EventRelation EvRel1 = new EventRelation(RelationId=con[0].Id,EventId=Event1.Id);
            insert EvRel1; 
            
            
            
            system.runAs(user1){
            Event__c Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
            insert Evento2;
            Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day,GuestList__c=String.valueOf(usr.Id));
            insert Evento1; 
                
            
            
            /*}           
            
            system.runAs(user1){    */ 
                ApexPages.currentPage().getParameters().put('idUser',  user1.Id);
                ApexPages.currentPage().getParameters().put('nameUser', user1.LastName);  
                ApexPages.currentPage().getParameters().put('edit', 'false');
                ApexPages.currentPage().getParameters().put('id', Event1.Id);
                ApexPages.currentPage().getParameters().put('day', string.valueof(day)); 
                ApexPages.currentPage().getParameters().put('pastday', 'false');
                ApexPages.currentPage().getParameters().put('format',  string.valueof(day.format()));    
                ApexPages.currentPage().getParameters().put('FromChater',  'true');                                
                PCA_CustomEventDetailController constructor = new PCA_CustomEventDetailController();
                BI_TestUtils.throw_exception = false;
                constructor.checkPermissions();
                constructor.loadInfo();                                
                constructor.createEvent();           
                constructor.edit();                     
                constructor.save();
                constructor.cancel();
                constructor.deleteEvent();
                
            }               
        }   
    }
    static testMethod void exceptions() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        BI_TestUtils.throw_exception = true;
        Event__c eve = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ActivityDateTime__c=System.now(),ActivityDate__c=Date.today(),EndDateTime__c=System.now().addDays(1),StartDateTime__c=System.now());
        insert eve;
        PageReference pagRef = new PageReference('PCA_CustomEventDetail');
        ApexPages.currentPage().getParameters().put('id', eve.Id);
        ApexPages.currentPage().getParameters().put('day', 'fecha invalida');
        PCA_CustomEventDetailController custDetCont = new PCA_CustomEventDetailController();
        
        custDetCont.checkPermissions();
        custDetCont.createEvent();
        custDetCont.editEvent();
        custDetCont.save();        
		
        if(custDetCont.subject != null) {}
        if(custDetCont.description != null) {}
        
        BI_TestUtils.throw_exception = false;
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}