public  class ADQ_DivisaUtility {
	public CurrencyTypeFacturacion__c corporatecurrency;
	public Map<String, CurrencyTypeFacturacion__c> conversionrates = new Map<String, CurrencyTypeFacturacion__c>();
	public List<CurrencyTypeFacturacion__c> systemcurrencies;

	public ADQ_DivisaUtility(){
		//CurrencyTypeFacturacion__c corporatecurrency = [Select IsoCode__c, IsCorporate__c, Id, ConversionRate__c From CurrencyTypeFacturacion__c Where IsCorporate__c = true and IsActive__c = true limit 1];
		CurrencyTypeFacturacion__c  corporatecurrency = new CurrencyTypeFacturacion__c ();
		for (CurrencyTypeFacturacion__c cc01 : [Select IsoCode__c, IsCorporate__c, Id, ConversionRate__c 
													From CurrencyTypeFacturacion__c 
													Where IsCorporate__c = true and IsActive__c = true limit 1]){
			corporatecurrency = cc01;											
		}
		this.corporatecurrency =  corporatecurrency;
		
		//Traemos todas las monedas disponibles en el sistema y su conversionrate
		systemcurrencies = new List<CurrencyTypeFacturacion__c>([Select IsoCode__c, IsCorporate__c, Id, ConversionRate__c from CurrencyTypeFacturacion__c where IsActive__c = true]);
		
		//Corremos por la lista de monedas para ordenarlas en el mapa
		for(CurrencyTypeFacturacion__c c : systemcurrencies){
			conversionrates.put(c.IsoCode__c,c);
		}
	}

	public Double transformCurrency(String original,String targetdiv,Double value){
		Double result;
		
		if(original == targetdiv){
			result = value;
		}else if(original == this.corporatecurrency.IsoCOde__c){
			//CurrencyType targetcurrency = [select ConversionRate from CurrencyType where IsoCode = :targetdiv and IsActive = true limit 1];
			if (conversionrates.get(targetdiv) != null){
				CurrencyTypeFacturacion__c targetcurrency = conversionrates.get(targetdiv);
				result = value * targetcurrency.ConversionRate__c;
			}
		}else if(targetdiv == this.corporatecurrency.IsoCOde__c){
			//CurrencyType originalcurrency = [select ConversionRate from CurrencyType where IsoCode = :original and IsActive = true limit 1];
			if (conversionrates.get(original) != null){
				CurrencyTypeFacturacion__c originalcurrency = conversionrates.get(original);
				result = value / originalcurrency.ConversionRate__c;
			}	
		}
		else {
			
			Double d = transformCurrency(original, this.corporatecurrency.IsoCode__c,value);
			result = transformCurrency(this.corporatecurrency.IsoCode__c, targetdiv, d);
		}
		
		return result;
	}
	
	
	public Double round(Double qnum, Double qdecimal){
		Double tempReturn = 0.0;
		//qnum = this.truncate(qnum, 3);
		if (qnum != null && qdecimal != null ){
			Double d = Math.pow(10, Math.roundToLong(qdecimal));
			tempReturn = Math.roundToLong(qnum * d) / d;
		}
		
		return tempReturn;
	}
	
	public String formatCurrency(Double amount){
		String r;
		if(amount == null) { r = '0.00'; }
		else{
			String s = '' + amount;
			if(s.indexOf('.') < 0) { s += '.00'; }
			if(s.indexOf('.') == (s.length() - 2)) { s += '0'; }
			//String[] a = s.split('5', 2);
			
			String d = s.substring(s.indexOf('.'),s.indexOf('.') + 3);
			String n = s.substring(0,s.indexOf('.'));
			
			String cadenaNumero = '';
		
			while(n.length() > 3) {
				String block = n.substring(n.length()-3);
				cadenaNumero = ',' + block + cadenaNumero;
				n = n.substring(0,n.length()-3);
			}
			cadenaNumero = n + cadenaNumero + d;
			r = cadenaNumero;
		}
		return r;
	}
	
	public Double truncate(Double num, Integer numDecimals){
		String strNum = string.valueOf(num);
		
		if(strNum.indexOf('.', 0) > -1){
			String intVal = strNum.substring(0, strNum.indexOf('.', 0));
			String decVal = strNum.substring(strNum.indexOf('.', 0) + 1);
			
			if(decVal.length() > numDecimals){
				strNum = intVal + decVal.substring(0, numDecimals);
			}
		}
		return Double.valueOf(strNum);
	}
/*
	public static testMethod void testDivisaUtility(){

			try {
			
			DivisaUtility divisautility = new DivisaUtility();
			
			String original = 'MXN';
			String target1 = 'MXN';
			Double value = 5;
			Double result = divisautility.transformCurrency(original, target1, value);
			
			String original2 = 'MXN';
			String target2 = 'USD';
			Double value2 = 5;
			Double result2 = divisautility.transformCurrency(original2, target2, value2);
			
			String original3 = 'USD';
			String target3 = 'MXN';
			Double value3 = 5;
			Double result3 = divisautility.transformCurrency(original3, target3, value3);
			
			String original4 = 'USD';
			String target4 = 'EUR';
			Double value4 = 5;
			Double result4 = divisautility.transformCurrency(original4, target4, value4);
			
			Double rounded = divisautility.round(5.98765, 2);
			String currencyFormated = divisautility.formatCurrency(rounded);
			Double doubleTruncate = divisautility.truncate(Double.valueOf('123.432938'), 3);
			
		}catch (Exception ex){
  			
		}	
		
	}
	*/
}