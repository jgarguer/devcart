public without sharing class PCA_Pedido_PopUpDetail extends PCA_HomeController {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show pedidos details 
    
    History:
    
    <Date>            <Author>          	<Description>
    26/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public NE__Order__c viewPedido {get; set;}
	public List<FieldSetHelper.FieldSetResult> fieldSetRecordIt {get; set;}
	public List <FieldSetHelper.FieldSetResult> Items{get;set;}
	public List<FieldSetHelper.FieldSetResult> fieldSetRecordA {get; set;}
	public List<FieldSetHelper.FieldSetResult> fieldSetRecordB {get; set;}
	public list<NE__OrderItem__c> ActiveItems{get;set;}
		

//da
	public FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
	public List<FieldSetHelper.FieldSetRecord> viewRecords 	{get; set;}
	public String firstHeader					{get; set;}
	public Integer index 						{get; set;}
	public Integer pageSize						{get; set;}
	public Integer totalRegs 					{get; set;}
//da

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user 
    
    History:
    
    <Date>            <Author>          	<Description>
    26/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
		public PageReference checkPermissions (){
		try{
			PageReference page = enviarALoginComm();
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_Pedido_PopUpDetail.checkPermissions', 'Portal Platino', Exc, 'Class');
		   return null;
		}
	}

		
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current userConstructor 
    
    History:
    
    <Date>            <Author>          	<Description>
    26/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	 
	public PCA_Pedido_PopUpDetail() {}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   load info of pedidos 
    
    History:
    
    <Date>            <Author>          	<Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	 	
		public void loadInfo (){
			try{
				String objectName = 'NE__Order__c';
				String objectName2 = 'NE__OrderItem__c';
				Id OrderId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
				String fieldSetName = ''; 
				String fieldSetName2='PCA_RelationTableLineasProductos';
					
				
				//List<NE__Order__c> pedidoList = [SELECT CreatedDate, recordtype.DeveloperName, Owner.FirstName,NE__Delivery_Date__c,Id,NE__Description__c,NE__FulfilmentStatus__c,
				//			NE__OpportunityId__c,NE__OrderStatus__c,NE__Order_date__c,NE__Type__c,Order_Code_Pedido__c,OwnerId FROM NE__Order__c WHERE Id = :OrderId];
							
				//ActiveItems = [SELECT Id, NE__ProdId__r.Name, NE__ProdId__c,NE__OrderId__c,NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c,NE__Qty__c FROM NE__OrderItem__c Where NE__OrderId__c = : OrderId];
				
				
				
									
				//if(!pedidoList.isEmpty()){
				//	if(pedidoList[0].RecordType.DeveloperName.contains('Order') ){
				//		fieldSetName = 'PCA_DetailPedido';
				//	} 
					
				//}
				
				//system.debug('ActiveItems: ' +ActiveItems);
				
				//if(!ActiveItems.isEmpty()){
					
				//	fieldSetName2='PCA_RelationTableLineasProductos';
					
				//}
				//system.debug('fieldsetname2: ' +fieldSetName2);
				
				system.debug(OrderId);
				if (OrderId != null){
					/*if(fieldSetName2=='PCA_RelationTableLineasProductos'){
						system.debug('ENTRA');
						Items = new List <FieldSetHelper.FieldSetResult>(); 
						for(NE__OrderItem__c val :ActiveItems){
							List<FieldSetHelper.FieldSetResult>  fieldSetRecordIt = FieldSetHelper.FieldSetHelperS(fieldSetName2, objectName2, val.Id);
							
							system.debug('fieldSetRecordIt: ' +fieldSetRecordIt);
					
							Items.add(fieldSetRecordIt);
						system.debug('Items: ' +Items);
							//Items.add(fieldSetRecordIt);
						}
					
						//this.fieldSetRecord = new List<FieldSetHelper.FieldSetResult>();
						
					}*/
					
					//Esto hace que depende del RT del Id que se pase por parametro muestre distintos fieldSet.
					//Actualmente no es usado porque parque comercial no tiene detailPage.
					/*
					List<NE__Order__c> ListPedidoRt = [SELECT recordtype.DeveloperName FROM NE__Order__c WHERE Id = :OrderId];
					if(!ListPedidoRt.isEmpty()){
						String pedidoRt = ListPedidoRt[0].recordtype.DeveloperName;
						
						System.debug('***pedidoRt: ' + pedidoRt);
						
						if(pedidoRt == 'Asset'){
							fieldSetName='PCA_DetailParque'; 
							 
						}else{*/
							fieldSetName='PCA_DetailPedido';
						//}
		
		
		//da			
					//Obtener detalle del pedido
				
					if(fieldSetName2=='PCA_RelationTableLineasProductos'){ //Siempre se cumple la condición.
						String pedidoValue = 'NE__OrderId__c;' + OrderId;
						
						this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(fieldSetName2, objectName2, '', pedidoValue,null);
						
						/*if (this.fieldSetRecords.regs.size() > 0)
						{
							this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
						}
						
						this.index = 1;	
						this.totalRegs = this.fieldSetRecords.regs.size();
						*/
						
						this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
						for (FieldSetHelper.FieldSetRecord item : this.fieldSetRecords.regs)
						{
							FieldSetHelper.FieldSetRecord iRecord = item; 
							this.viewRecords.add(iRecord);
						}
						
						system.debug(this.viewRecords); 
					}
					
		//da			
					
					//Obtenemos deatil pedido
					//if(fieldSetName=='PCA_DetailPedido')
					//{
						//Set<String> textValues = FieldSetHelper.textAreaFields(fieldSetName, objectName);
						List<FieldSetHelper.FieldSetResult>  fieldSetRecord = FieldSetHelper.FieldSetHelperSimple(fieldSetName, objectName, OrderId);
						
						//this.fieldSetRecord = new List<FieldSetHelper.FieldSetResult>();          
						Integer tam = fieldSetRecord.size();
						
						this.fieldSetRecordA = new List<FieldSetHelper.FieldSetResult>();
						this.fieldSetRecordB = new List<FieldSetHelper.FieldSetResult>();			 
						
						for (Integer i=0; i<(tam/2); i++)
						{
							FieldSetHelper.FieldSetResult value = fieldSetRecord[i]; 
							this.fieldSetRecordA.add(value);
						}
						
						for (Integer i=(tam/2); i<tam; i++)
						{
							FieldSetHelper.FieldSetResult value = fieldSetRecord[i]; 
							this.fieldSetRecordB.add(value);
						}                                                      
					//}
				//}
				}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_Pedido_PopUpDetail.loadInfo', 'Portal Platino', Exc, 'Class');
		}
	}
}