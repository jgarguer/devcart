public with sharing class PCA_InventoryController extends PCA_HomeController {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Methods executed by Event Triggers 
    
    History:
    
    <Date>                    <Author>               <Description>
    13/06/2014              Jorge Longarela         Initial Version
    17/06/2014               Antonio Moruno         v2
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public string LastMod {get;set;}
    
    public String nameFS;
    public String nameObject                    {get; set;}
    public String accountFieldName;
    
    public String nameFS1;
    public String nameObject1                   {get; set;}
    public String accountFieldName1;
    
    public String nameFS2;
    public String nameObject2                   {get; set;}
    public String accountFieldName2;
    
    public String searchFinal;
    
    public Id AccountId                         {get; set;}
    
    public String searchText                    {get; set;}
    public String searchField                   {get; set;}
    public String firstHeader                   {get; set;}
    
    public Integer index                        {get; set;}
    public Integer pageSize                     {get; set;}
    public Integer totalRegs                    {get; set;}
    
    //public Boolean OptionA                        {get; set;}
    
    public List<SelectOption> searchFields      {get; set;}
    
    public List<FieldSetHelper.FieldSetRecord> viewRecords  {get; set;}
    public FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    
    public String fieldImage {get;set;}
    public String Mode {get;set;}
    
    public String FieldOrder {get;set;}
    public String Field;
    public String PageValue {get;set;}
    //public boolean onlyOrders {get; set;}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.checkPermissions', 'Portal Platino', e.getMessage(), 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   controller
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public PCA_InventoryController() {}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void loadInfo (){    
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.index = 1;
            this.pageSize = 10;
            this.searchFinal = null;        
            this.AccountId = BI_AccountHelper.getCurrentAccountId();
            system.debug('****AccountId: '+this.AccountId);
            
            string countryAccount = BI_AccountHelper.getCountryAccount(AccountId);
            
            List<BI_Actualizaciones_integracion__c> regions = [select Id, BI_Entidad__c, BI_Country__c, BI_UltimaModificacion__c from BI_Actualizaciones_integracion__c where BI_Country__c= : countryAccount AND BI_Entidad__c=:Label.BI_LabelParque];
            
            
            
            if(!regions.isEmpty()){
                LastMod = regions[0].BI_UltimaModificacion__c;
                system.debug('regions' +regions);
            }
            
            /*
            this.nameObject1 = 'NE__Order__c';
            this.nameFS1 = 'PCA_MainTablePedidos';
            this.accountFieldName1 = 'AccountId';
            
            this.nameObject2 = 'NE__Order__c';
            this.nameFS2 = 'PCA_MainTablePedidos';
            this.accountFieldName2 = 'NE__AccountId__c';
            */
            //this.nameFS = 'PCA_MainTableParque';
            //this.nameObject = 'NE__Order__c';
            this.nameFS = 'PCA_MainTableAsset';
            this.nameObject = 'NE__Asset__c';
            this.accountFieldName = 'NE__AccountId__c';
            
            defineSearch();
            defineRecords();
            defineViews();
            
            /*
            onlyOrders = false;
            string typeUrl = Apexpages.currentPage().getParameters().get('type'); 
            if (typeUrl != null && typeUrl == 'o'){
                onlyOrders = true;
                cambioPatron();
            }
            */
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.PCA_InventoryController', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Define Records of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public void defineRecords()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            //String accountValue = this.accountFieldName + ';' + this.AccountId;
            
            
            //String accountValue = 'NE__OrderId__r.NE__Asset_Account__c;'+ this.AccountId;
            //if(this.nameObject == this.nameObject2){
            //  this.searchFinal = this.searchFinal + '::NE__OrderId__r.RecordType.DeveloperName;Asset'; //Edited
                //this.searchFinal = this.searchFinal + '::RecordType.Name;Asset'; //Edited
            //}
            //if(this.nameObject == this.nameObject2){
            //  this.searchFinal = 'RecordType.Name;order';
            //}
             
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            system.debug('accountValue: '+accountValue);
            this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.searchFinal, accountValue,null);
            
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field; 
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.defineRecords', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Define Views of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public void defineViews()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
            Integer topValue = ((this.index + this.pageSize) > this.totalRegs) ? this.totalRegs : (this.index + this.pageSize - 1);
            
            for (Integer i = (this.index - 1); i < topValue; i++)
            {
                FieldSetHelper.FieldSetRecord iRecord = this.fieldSetRecords.regs[i]; 
                this.viewRecords.add(iRecord);
            }
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.defineViews', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Define search of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public void defineSearch()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.searchFields = FieldSetHelper.defineSearchFields(this.nameFS, this.nameObject);    
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.defineSearch', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Search records of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public void searchRecords()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.searchFinal = this.searchField + ';' + this.searchText;
            defineRecords();
            defineViews();
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.searchRecords', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   if has previous pages of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    public Boolean getHasPrevious()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return (this.index != 1);
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.getHasPrevious', 'Portal Platino', e.getMessage(), 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   if has next pages of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public Boolean getHasNext()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return !((this.index + this.pageSize) > this.totalRegs);
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.getHasNext', 'Portal Platino', e.getMessage(), 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   next page of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    public void Next()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.index += this.pageSize;
            defineViews();
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.Next', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   previous page of inventory
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    public void Previous()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.pageSize = 10;
            this.index -= this.pageSize;
            defineViews();  
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_InventoryController.Previous', 'Portal Platino', e.getMessage(), 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines records in page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public List<SelectOption> getItemPage() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        options.add(new SelectOption('200','200'));
        system.debug('itemPage: '+ options);
        return options;
    }   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show more values in a page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void valuesInPage(){
        
        this.pageSize = Integer.valueOf(PageValue);
        system.debug('pageSize*: '+this.pageSize);
        defineViews();
        
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Order table with field
    
    History:
    
    <Date>            <Author>              <Description>
    06/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void Order(){
        
        try{/*
            this.searchText = null;
            this.searchFinal = null;
            this.OptionA = this.OptionA ? false : true;
            this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
            this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
            this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
    */          
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            Mode = '';
            defineSearch();
            fieldImage = FieldOrder;
            //String accountValue = 'NE__OrderId__r.NE__Asset_Account__c;'+ this.AccountId;
            //if(this.nameObject == this.nameObject2){
            //  this.searchFinal = '::NE__OrderId__r.RecordType.DeveloperName;Asset;'+FieldOrder;
            //}
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            if(Field == FieldOrder){
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'DESC');
                FieldOrder = '';
                Mode = 'DESC';
            }else{
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'ASC');
                Mode = 'ASC';
            }
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
            
            Field = this.FieldOrder;
            defineViews();
             
        }catch (exception e){
            BI_LogHelper.generateLog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', e.getMessage(), 'Class');
        }
        
    }
}