/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Controller class for ManagementOpps_EditMultiSubOpp VF
    Test Class:    BI_O4_ManagementOpps_EditMultiSubOpp_TEST   
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Fernando Arteaga          Initial version
    11/08/2017              Oscar Bartolo
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_ManagementOpps_EditMultiSubOpp
{
    public List<Opportunity> listOpp {get; set;}
    public List<Opportunity> selectedOportunities {get; set;}
    
    public List<NE__OrderItem__c> listProd {get; set;}
    public String listOppActive {get; set;}
    public Map<Id, List<prodWrapper>> wrapperProdMap {get; set;}
    public Map<Id, List<NE__OrderItem__c>> selectedProducts {get; set;}
    
    public Id oppId {get; set;}
    public List<oppWrapper> wrapperList {get; set;}
    public Boolean isEdit {get; set;}
    
    public List<String> error                               {get; set;}
    public List<String> warning                             {get; set;}
  
    public static final String MAIN_VIEW = 'main_view';
    public static final String NEW_SUBOPPORTUNITY = 'new_subopp';
    public static final String EDIT_MULTI_OPPORTUNITY = 'edit_multiopp';
    public static final String VIEW_EDIT_OPPORTUNITY = 'view_opportunity';
    public static final String CREATE_MULTI_OPPORTUNITY = 'create_multiopp';
    
    public BI_O4_ManagementOpps_EditMultiSubOpp(ApexPages.standardController controller)
    {
        isEdit = false;
        Set<Id> setOppId = new Set<Id>();
        
        oppId = (Id) controller.getId();
        
        if (oppId != null)
        {
            String query = BI_O4_Utils.getSOQLQueryAllFields(Opportunity.getSobjectType()) + ' WHERE BI_Oportunidad_padre__c = :oppId';
            this.listOpp = Database.query(query);

            wrapperList = new List<oppWrapper>();
                
            for (Opportunity opp : this.listOpp)
            {
                oppWrapper wOpportunity = new oppWrapper(opp);
                wrapperList.add(wOpportunity);
            }
            
            String queryProd = 'SELECT Id, NE__OrderId__c, Ne__OrderId__r.Ne__OptyId__c, ';
            
            Schema.FieldSet fsEconomico = Schema.SObjectType.NE__OrderItem__c.fieldSets.BI_O4_MultipleEdit_Economico;
            for(Schema.FieldSetMember fieldSetMemberObj : fsEconomico.getFields()) {
				queryProd += fieldSetMemberObj.getFieldPath() + ', ';
            }
            queryProd = queryProd.removeEnd(', ') + ' FROM NE__OrderItem__c WHERE Ne__OrderId__c in (SELECT id FROM Ne__Order__c WHERE Ne__OptyId__c in : listOpp AND NE__OrderStatus__c in (\'Active\', \'F1 - Closed Won\', \'Active - Locked product\'))';
            
            this.listProd = Database.query(queryProd);
            
            wrapperProdMap = new Map<Id, List<prodWrapper>>();
            
            this.listOppActive = '';
            for (Opportunity opp : this.listOpp) {
                List<prodWrapper> listProdOpp = new List<prodWrapper>();
                for  (NE__OrderItem__c prod : this.listProd) {
                    if (prod.Ne__OrderId__r.Ne__OptyId__c == opp.Id) {
                        prodWrapper wProduct = new prodWrapper(prod);
                        listProdOpp.add(wProduct);
                        this.listOppActive = this.listOppActive + opp.id;
                    }
                }
                if (!listProdOpp.isEmpty()) {
                    wrapperProdMap.put(opp.Id, listProdOpp);
                }
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.BI_O4_No_Opportunities_selected));
        }
        
        //this.currentView = EDIT_MULTI_OPPORTUNITY;
    }

    public PageReference editSelectedOpportunities()
    {
        isEdit = true;
        String listSelectedIds = '';
        selectedOportunities = new List<Opportunity>();
        selectedProducts = new Map<Id, List<NE__OrderItem__c>>();
        
        for (oppWrapper wrapper: this.wrapperList)
        {
            if (wrapper.check)
            {
                selectedOportunities.add(wrapper.opportunity);
                listSelectedIds += wrapper.opportunity.Id + ';';
                List<NE__OrderItem__c> listProdOpp = new List<NE__OrderItem__c>();
                if (this.listOppActive.contains(wrapper.opportunity.Id)) {
                    for (prodWrapper prod: this.wrapperProdMap.get(wrapper.opportunity.Id)){
                        listProdOpp.add(prod.product);
                    }
                    if (!listProdOpp.isEmpty()) {
						selectedProducts.put(wrapper.opportunity.id, listProdOpp);
                	}
                }
            }
        }

        if (listSelectedIds == '')
        { 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'No opportunities selected'));
            return null;
        }
        
        return null;        
    }
        
    public PageReference saveOpportunities()
    {
        try
        {
            Integer errors = 0;
        
            //upsert selectedOportunities;
            List<NE__OrderItem__c> listProdOpp = new List<NE__OrderItem__c>();
            List<Database.UpsertResult> listUR = Database.upsert(selectedOportunities, false);
            
            for (Opportunity opp : selectedOportunities) {
                if (selectedProducts.containsKey(opp.Id)) {
                    List<NE__OrderItem__c> listProdOppAux = selectedProducts.get(opp.Id);
                    for (NE__OrderItem__c ordItem : listProdOppAux) {
                        if (ordItem.NE__RecurringChargeOv__c == null) {
                            ordItem.NE__RecurringChargeOv__c = 0;
                        }
                        if (ordItem.NE__OneTimeFeeOv__c == null) {
                            ordItem.NE__OneTimeFeeOv__c = 0;
                        }
                        if (ordItem.BI_Ingreso_Recurrente_Anterior_Producto__c == null) {
                            ordItem.BI_Ingreso_Recurrente_Anterior_Producto__c = 0;
                        }
                        listProdOpp.add(ordItem);
                    }
                }    
            }
            listUR.addAll(Database.upsert(listProdOpp, false));
            System.debug('Numero: '+listUR.size());
        
            for (Integer i = 0; i < listUR.size(); i++)
            {
                if (!listUR[i].isSuccess())
                {
                    errors++;
                    selectedOportunities[i].StageName.addError(listUR[i].getErrors()[0].getMessage());
                    System.debug('The following error has occurred: ');                    
                    System.debug(listUR[i].getErrors()[0].getStatusCode() + ': ' + listUR[i].getErrors()[0].getMessage());
                    System.debug('Account fields that affected this error: ' + listUR[i].getErrors()[0].getFields());
                }
            }
        
            if (errors == 0)
            {
                setOrderValues(selectedOportunities);
                Map<String,String> mapParams = Apexpages.currentPage().getParameters();
                Pagereference page = new Pagereference('/' + oppId );
                page.setRedirect(true);
                return page;
            }
            else
                return null;
        }
        catch (Exception exc)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, exc.getMessage()));
            BI_LogHelper.generate_BILog('BI_O4_ManagementOpps_EditMultiSubOpp.saveOpportunities', 'BI_EN', exc, 'Clase');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Óscar Bartolo
        Company:       Aborda
        Description:   Method that calculate the economic values of the opportunity and the order
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        21/08/2017                      Óscar Bartolo               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static void setOrderValues(List<Opportunity> opp){

        List<NE__Order__c> lst_ord = [SELECT Id, NE__One_Time_Fee_Total__c, NE__Recurring_Charge_Total__c, CurrencyIsoCode, (SELECT NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c, NE__Qty__c, NE__Parent_Order_Item__c, NE__Parent_Order_Item__r.NE__Qty__c, CurrencyIsoCode FROM NE__Order_Items__r) FROM NE__Order__c WHERE NE__OptyId__c in: opp AND (NE__OrderStatus__c LIKE 'Activ%' OR NE__OrderStatus__c = 'F1 - Closed Won' OR NE__OrderStatus__c = 'Pending' OR NE__OrderStatus__c = 'F1 - Cancelled | Suspended' OR NE__OrderStatus__c = 'F1 - Closed Lost') ORDER BY CreatedDate DESC];
        if(!lst_ord.isEmpty()){
            List<NE__Order__c> ordToUpdate = new List<NE__Order__c>();
            
            for(NE__Order__c ord : lst_ord){
                Decimal otf = 0;
                Decimal rc = 0;
                for(NE__OrderItem__c oi : ord.NE__Order_Items__r){
                     
                    if(oi.CurrencyIsoCode != ord.CurrencyIsoCode){
                        if(oi.NE__Parent_Order_Item__c != null){
                            otf += BI_CurrencyHelper.convertCurrency(oi.CurrencyIsoCode, ord.CurrencyIsoCode, Double.valueOf((oi.NE__Qty__c * oi.NE__Parent_Order_Item__r.NE__Qty__c) * oi.NE__OneTimeFeeOv__c));
                            rc +=  BI_CurrencyHelper.convertCurrency(oi.CurrencyIsoCode, ord.CurrencyIsoCode, Double.valueOf((oi.NE__Qty__c * oi.NE__Parent_Order_Item__r.NE__Qty__c) * oi.NE__RecurringChargeOv__c));
                        }
                        else if (oi.NE__OneTimeFeeOv__c != null && oi.NE__RecurringChargeOv__c != null && oi.NE__Qty__c != null){
                            otf += BI_CurrencyHelper.convertCurrency(oi.CurrencyIsoCode, ord.CurrencyIsoCode, Double.valueOf(oi.NE__Qty__c * oi.NE__OneTimeFeeOv__c));
                            rc +=  BI_CurrencyHelper.convertCurrency(oi.CurrencyIsoCode, ord.CurrencyIsoCode, Double.valueOf(oi.NE__Qty__c * oi.NE__RecurringChargeOv__c));
                        }
                    }
                    else{
                        
                        if(oi.NE__Parent_Order_Item__c != null){
                            otf += (oi.NE__Qty__c * oi.NE__Parent_Order_Item__r.NE__Qty__c) * oi.NE__OneTimeFeeOv__c;
                            rc +=  (oi.NE__Qty__c * oi.NE__Parent_Order_Item__r.NE__Qty__c) * oi.NE__RecurringChargeOv__c;
                        }
                        else {
                            otf += oi.NE__Qty__c * oi.NE__OneTimeFeeOv__c;
                            rc +=  oi.NE__Qty__c * oi.NE__RecurringChargeOv__c;
                        }
                    }
                }
                ord.NE__One_Time_Fee_Total__c = otf;
                ord.NE__Recurring_Charge_Total__c = rc;
                
                ordToUpdate.add(ord);
            }

            NETriggerHelper.setTriggerFiredTest('TGS_Order_Handler.NeCheckOrder',false);
            update ordToUpdate;
        }
    }
    
    public PageReference doActionCancel()
    {
        Pagereference page = new Pagereference('/' + Apexpages.currentPage().getParameters().get('id'));
        page.setRedirect(true);
        return page;
    }
    
    class oppWrapper 
    {
        public Boolean check                {get; set;}
        public Opportunity opportunity      {get; set;}
    
        public oppWrapper (Opportunity opportunity)
        {
            this.opportunity = opportunity;
            this.check = false;
        }
    }
    
    class prodWrapper 
    {
        public Boolean checkProd                {get; set;}
        public NE__OrderItem__c product     {get; set;}
    
        public prodWrapper (NE__OrderItem__c product)
        {
            this.product = product;
            this.checkProd = false;
        }
    }
}