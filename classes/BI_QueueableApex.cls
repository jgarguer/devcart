/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Class to handle CSB callouts when invoked from Account trigger.
    
    History:
    
    <Date>            <Author>          <Description>
    10/11/2015        Fernando Arteaga  BI_EN - CSB Integration
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
public with sharing class BI_QueueableApex implements Queueable, Database.AllowsCallouts
{
    Account account;
    BI_Log__c trans;
    String operation;
    Integer maxIntentos;
    Integer numIntentos;
    Boolean finished = false;
    String error = '';
    
    public BI_QueueableApex (Account acc, BI_Log__c t, String op)
    {
        this.account = acc;
        this.trans = t;
        this.operation = op;
        
        Map<String, BI_CSB_WS_Retry__c> mapCs = BI_CSB_WS_Retry__c.getAll();
        if (!mapCs.isEmpty())
        {
            List<BI_CSB_WS_Retry__c> listAux = mapCs.values();
            this.maxIntentos = listAux != null && listAux[0].BI_CSB_Reintentos__c != null ? listAux[0].BI_CSB_Reintentos__c.intValue() : 5;
        }
        
        if (this.maxIntentos == null)
            this.maxIntentos = 5;
            
        this.numIntentos = 0;
    }
    
    public void execute(QueueableContext context)
    {
        BI_RestWrapper.GenericResponse gresp = new BI_RestWrapper.GenericResponse();
        
        try
        {
            do
            {
                HttpRequest req = new HttpRequest();
        
        		String namedCredential = getCountryNamedCredential(account.BI_Country__c);
        		if (namedCredential.startsWith('BI_CSB_API_UNICA'))
                	req.setEndpoint('callout:' + namedCredential + '/customeraccounts/v1/accounts/' + account.BI_Id_CSB__c + '/status');
                else
                {
                	updateTransaction(namedCredential);
                	return;
                }
                    
                req.setMethod('PUT');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('UNICA-ServiceCallback', System.URL.getSalesforceBaseUrl().toExternalForm().replace('visual.force.com', 'my.salesforce.com').replace('--c.', '.').replace('--e.', '.') + '/services/apexrest/customeraccounts/v1/accounts/status/'); 
                req.setBody('{"accountStatus": ' + (operation == 'b' ? '"cancelled"}' : operation == 's' ? '"suspended"}' : '"active"}'));
                req.setTimeout(60000);
                
                System.debug('body:' + req.getBody());
                
                Http http = new Http();
                HTTPResponse res = new HTTPResponse();
                    
                if(!Test.isRunningTest()){
                    res = http.send(req);
                }else{
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"test":"bar"}');
                    res.setStatusCode(202);
                }   
                
                System.debug('#### res: '+res);
                System.debug('#### res body: '+res.getBody());
                    
                if(res.getStatusCode() == 202)
                {
                	//OK
                    finished = true;
                    gresp.transactionType = (BI_RestWrapper.apiTransactionType) JSON.deserialize(res.getBody(), BI_RestWrapper.apiTransactionType.class);
                    System.debug('gresp.transactionType:' + gresp.transactionType);
                    
                    if (operation != 'b')
                    {
	                    this.account.BI_CSB_TransactionId__c = gresp.transactionType.transactionId;
	                    update this.account;
                    }
                    
                }else{
                    this.numIntentos = this.numIntentos + 1;
                    error = res.getStatus();
                }
            }
            while (this.numIntentos < this.maxIntentos && !finished);

            // Si llegamos aquí significa que se ha reintentado más del num máximo de veces
            if (!finished)
                updateTransaction(Label.BI_CSB_WS_Max_Intentos + ': ' + error);

        } catch(Exception exc) {
            
            updateTransaction(exc.getMessage());
            //BI_LogHelper.generate_BILog('BI_QueueableApex.execute', 'BI_EN', exc, 'Web Service');
            //gresp.error = exc.getMessage();
            
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Updates a transaction record when a callout fails (BI_Log__c)
    
    IN:            Error message
    OUT:           void
    
    History:
    
    <Date>            <Author>              <Description>
    10/11/2015        Fernando Arteaga      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void updateTransaction(String errorMessage)
    {
        trans.BI_COL_Estado__c = Label.BI_COL_lblFallido;
        trans.Descripcion__c = errorMessage;
        trans.BI_Tipo_Error__c = String.valueOf(this.numIntentos);
        update trans;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Gets the named credential corresponding to the country
    
    IN:            accCountry: country (BI_Country__c) of the account

    OUT:           Named credential api name
    
    History:
    
    <Date>            <Author>          	<Description>
    15/04/2016        Fernando Arteaga      Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static String getCountryNamedCredential(String accCountry)
	{
		String retMessage;
		
		if (accCountry != null)
		{
			BI_CSB_Credenciales_Pais__c credencial = BI_CSB_Credenciales_Pais__c.getInstance(accCountry);
			if (credencial != null && credencial.BI_CSB_Credencial__c != null)
				retMessage = credencial.BI_CSB_Credencial__c;
			else if (credencial != null && credencial.BI_CSB_Credencial__c == null)
				retMessage = Label.BI_CSB_Error_Credencial;
			else
				retMessage = Label.BI_CSB_Endpoint_No_Encontrado + ' ' + accCountry;
		}
		else
			retMessage = Label.BI_CSB_Pais_Obligatorio;
		
		System.debug('namedCredential:' + retMessage);
		return retMessage;
	}
}