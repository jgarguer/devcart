@isTest(seeAllData = false)
public class BIIN_UNICA_UserMethods_TEST 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González Beltrán
  Company:       HPE
  Description:   Test method to manage the code coverage for BIIN_UNICA_AccountHandler
  
  <Date>             <Author>                        	      <Change Description>
  22/06/2016         José Luis González Beltrán             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
  private final static String LE_NAME = 'TestAccount_ContactHandler_TEST';
  private static final String CONTACT_EMAIL = 'Test_UNICA_ContactHandler_TEST@email.com';

  @testSetup static void dataSetup() 
  {
    Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
    insert account;

    Contact con = BIIN_UNICA_TestDataGenerator.generateContact(CONTACT_EMAIL, account.Id);
    insert con;

    Profile profComm = [select Id, Name from Profile where Name = 'BI_Partner Communities' limit 1];

    User usertest = new User (
                              alias             = 'test01',
                              email             = 'BIIN_UNICA_UserMethods_TEST@testorg.com',
                              emailencodingkey  = 'UTF-8',
                              lastname          = 'Testing',
                              languagelocalekey = 'en_US',
                              localesidkey      = 'en_US',
                              isActive          = true,
                              CommunityNickname = 'testUser123',
                              ProfileId         = profComm.Id,
                              BI_Permisos__c    = 'Canal Indirecto',
                              ContactId         = con.Id,
                              timezonesidkey    = Label.BI_TimeZoneLA,
                              username          = 'ur23@testorg.com',
                              Pais__c           = 'Argentina'
                              );
                     
    insert usertest;
  }

  @isTest static void test_BIIN_UNICA_UserMethods_TEST()
  {
    List<User> us = [SELECT Id, ContactId FROM User WHERE email =: 'BIIN_UNICA_UserMethods_TEST@testorg.com' LIMIT 1];

  	Test.startTest(); 

  	BIIN_UNICA_UserMethods.updateRoDContact(us, us);
		        
    Test.stopTest();
  }
}