public class NETriggerHelper{
    
    private static boolean alreadyModified = false;
    private static map<String,Boolean> mapOfOrderIts 		= new map<String,Boolean>();
    private static map<String,Boolean> mapOfTriggerFired 	= new map<String,Boolean>();
    private static boolean generateCase 					= true;

    
    public static boolean isgenerateCase() {
    	return generateCase;
    }

    public static void setgenerateCase(boolean createC) {
    	generateCase = createC;
    }    
    
    public static boolean isAlreadyModified() {
    	return alreadyModified;
    }

    public static void setAlreadyModified() {
    	alreadyModified = true;
    }
    
    public static void setOitMap(String oiId) {
    	mapOfOrderIts.put(oiId,true);
    }
    
    public static Boolean getOitMap(String oiId) {
    	return mapOfOrderIts.get(oiId);
    }
    
    public static void setTriggerFired(String triggerName) {
    	mapOfOrderIts.put(triggerName,true);
    }
    
    public static void setTriggerFiredTest(String triggerName, Boolean bool) {
        if(mapOfOrderIts.get(triggerName) != null)
            mapOfOrderIts.remove(triggerName);
        mapOfOrderIts.put(triggerName, bool);
    }
    
    public static Boolean getTriggerFired(String triggerName) {
    	if(mapOfOrderIts.get(triggerName) != null)
    		return mapOfOrderIts.get(triggerName);
    	else
    		return false;
    }    
    
    public static Boolean getTGSTriggerFired(String ownerProfile) {
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
        Boolean TGS = userTGS.TGS_Is_TGS__c;

        if(ownerProfile.startsWith('TGS') || TGS)
            return true;
        else
            return false;
    }
}