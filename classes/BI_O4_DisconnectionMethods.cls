/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Jose Miguel Fierro
	Company:       New Energy Aborda
	Description:   Methods executed by the BI_O4_Disconnection__c trigger.
	Test Class:    BI_O4_DisconnectionMethods_TEST

	History

	<Date>          <Author>                      <Description>
	28/06/2016      Jose Miguel Fierro            Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class BI_O4_DisconnectionMethods {

	private static Boolean processRunning = false;

	// Disconnection possible statuses
	public static final String BIO4_DISC_STAT_REQUEST_REVIEW = 'Request Review';
	public static final String BIO4_DISC_STAT_PROVIDER_DISCONNECTIONS = 'Provider Disconnections';
	public static final String BIO4_DISC_STAT_PROJECT_MANAGER_ASSIGNMENT = 'Project Manager Assignment';
	public static final String BIO4_DISC_STAT_FINANCIAL_PLANNING_REVIEW = 'Financial Planning Review';
	public static final String BIO4_DISC_STAT_DISCONNECTION_EXECUTION = 'Disconnection Execution';
	public static final String BIO4_DISC_STAT_NEGOTIATION_DETAILS_REVIEW = 'Negotiation Details Review';
	public static final String BIO4_DISC_STAT_PENALTY_REVIEW_WITH_CUSTOMER = 'Penalty Review With Customer';
	public static final String BIO4_DISC_STAT_PENALTIES_DISPUTE_REVIEW = 'Penalties Dispute Review';
	public static final String BIO4_DISC_STAT_NEGOTIATE_WITH_CUSTOMER = 'Negotiate With Customer';
	public static final String BIO4_DISC_STAT_CLOSED = 'Closed';

	public static final String BI_CAS_RT_CASO_INTERNO = 'BI_Caso_Interno';

	// Queues
	public static final String BIO4_Q_LEGAL = 'BI_O4_Legal';
	public static final String BIO4_Q_FINANCIAL_PLANNING = 'BI_O4_Financial_Planning';

	/**
     * Get the value of the variable
     * @return True if the approval process has already been started before in the same context
     * false if not
     */
	public static Boolean isProcessRunning() {
		return processRunning;
	}
	public static void setProcessRunning() {
		processRunning = true;
	}

	private static Map<Id, Account> getDisconnectionAccounts(List<BI_O4_Disconnection__c> news) {
		Set<Id> setIdRelatedAccounts = new Set<Id>();
		for(BI_O4_Disconnection__c disconnection : news) {
			setIdRelatedAccounts.add(disconnection.BI_O4_Account__c);
		}
		return new Map<Id, Account>([SELECT OwnerId FROM Account WHERE Id IN :setIdRelatedAccounts]);
	}

	private static Map<Id, List<Case>> getRelatedCases(Map<Id, BI_O4_Disconnection__c> newMap) {
		Map<Id, List<Case>> relatedCases = new Map<Id, List<Case>>();
		for(Case c : [SELECT BI_O4_Disconnection__c, RecordType.DeveloperName, BI_Type__c FROM Case WHERE BI_O4_Disconnection__c IN :newMap.keyset()]) {
			if(relatedCases.containsKey(c.BI_O4_Disconnection__c)) {
				relatedCases.get(c.BI_O4_Disconnection__c).add(c);
			} else {
				relatedCases.put(c.BI_O4_Disconnection__c, new Case[]{c});
			}
		}
		return relatedCases;
	}

	private static Map<String, QueueSObject> getDisconnectionQueues() {
		Map<String, QueueSObject> queues = new Map<String, QueueSObject>();
		for(QueueSObject qso : [SELECT QueueId, Queue.Name, Queue.DeveloperName FROM QueueSObject WHERE SObjectType = 'BI_O4_Disconnection__c']) {
			queues.put(qso.Queue.DeveloperName, qso);
		}
		return queues;
	}

	public static void setCustomFieldAccountOwner(List<BI_O4_Disconnection__c> news) {
		Map<ID, Account> mapDisconnectionAccounts = getDisconnectionAccounts(news);
		for(BI_O4_Disconnection__c disc : news) {
			disc.BI_O4_Account_Owner__c = mapDisconnectionAccounts.get(disc.BI_O4_Account__c).OwnerId;
		}
	}

	public static void validateRequiredFields(List<BI_O4_Disconnection__c> news) {
		for(BI_O4_Disconnection__c disc : news) {

			if(disc.BI_O4_Status__c == BIO4_DISC_STAT_REQUEST_REVIEW && disc.BI_O4_Provider_Reviewer__c == null) {
				disc.BI_O4_Provider_Reviewer__c.addError('Provider Reviewer is required');
			}
			else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_PROVIDER_DISCONNECTIONS && disc.BI_O4_Provider_Reviewer__c == null) {
				disc.BI_O4_Provider_Reviewer__c.addError('Provider Reviewer is required');
			}
			else if((disc.BI_O4_Status__c == BIO4_DISC_STAT_PROJECT_MANAGER_ASSIGNMENT
				|| disc.BI_O4_Status__c == BIO4_DISC_STAT_FINANCIAL_PLANNING_REVIEW) && disc.BI_O4_Project_Manager_Leader__c == null) {
				disc.BI_O4_Project_Manager_Leader__c.addError('Project Manager Leader is required');
			}
			else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_DISCONNECTION_EXECUTION && disc.BI_O4_Project_Manager__c == null) {
				disc.BI_O4_Project_Manager__c.addError('Project Manager is required');
			}
		}
	}

	public static void setRecordsOwnership(List<BI_O4_Disconnection__c> news, Map<Id, BI_O4_Disconnection__c> oldMap) {
		Map<Id, Account> mapDisconnectionAccounts = getDisconnectionAccounts(news);
		Map<String, QueueSObject> mapDisconnectionQueues = getDisconnectionQueues();
		System.debug('[BI_O4_DisconnectionMethods.setRecordsOwnership] Disconnection Queues: ' + mapDisconnectionQueues);

		for(BI_O4_Disconnection__c disc : news) {
			if(disc.BI_O4_Status__c != oldMap.get(disc.Id).BI_O4_Status__c) {

				
				if( disc.BI_O4_Status__c == BIO4_DISC_STAT_NEGOTIATION_DETAILS_REVIEW || (disc.BI_O4_Status__c == BIO4_DISC_STAT_PENALTY_REVIEW_WITH_CUSTOMER
						&& oldMap.get(disc.Id).BI_O4_Status__c == BIO4_DISC_STAT_PENALTIES_DISPUTE_REVIEW) ) {
					disc.OwnerId = disc.CreatedById;
				}
				else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_PENALTIES_DISPUTE_REVIEW) {
					if(mapDisconnectionQueues.containsKey(BIO4_Q_LEGAL)) {
						disc.OwnerId = mapDisconnectionQueues.get(BIO4_Q_LEGAL).QueueId;
					}
				}
				else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_NEGOTIATE_WITH_CUSTOMER) {
					disc.OwnerId = mapDisconnectionAccounts.get(disc.BI_O4_Account__c).OwnerId;
				}
				else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_FINANCIAL_PLANNING_REVIEW) {
					if(mapDisconnectionQueues.containsKey(BIO4_Q_FINANCIAL_PLANNING)) {
						disc.OwnerId = mapDisconnectionQueues.get(BIO4_Q_FINANCIAL_PLANNING).QueueId;
					}
				}
				else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_PROVIDER_DISCONNECTIONS && disc.BI_O4_Provider_Reviewer__c != null) {
					disc.OwnerId = disc.BI_O4_Provider_Reviewer__c;
				}
				else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_PROJECT_MANAGER_ASSIGNMENT && disc.BI_O4_Project_Manager_Leader__c != null) {
					disc.OwnerId = disc.BI_O4_Project_Manager_Leader__c;
				}
				else if(disc.BI_O4_Status__c == BIO4_DISC_STAT_DISCONNECTION_EXECUTION && disc.BI_O4_Project_Manager__c != null) {
					disc.OwnerId = disc.BI_O4_Project_Manager__c;
				}
			}
		}
	}

	public static void submitForApproval(Map<Id, BI_O4_Disconnection__c> newMap, Map<Id, BI_O4_Disconnection__c> oldMap) {
		List<Approval.ProcessSubmitRequest> lstApprovalRequest =  new List<Approval.ProcessSubmitRequest>();
		for(BI_O4_Disconnection__c disc : newMap.values()) {
			if(disc.BI_O4_Status__c != oldMap.get(disc.Id).BI_O4_Status__c) {
				if(disc.BI_O4_Status__c == BIO4_DISC_STAT_REQUEST_REVIEW) {
					Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
					req.setComments(Label.BI_O4_Sent_for_Approval);
					req.setObjectId(disc.Id);
					lstApprovalRequest.add(req);
				}
			}
		}

		if(!lstApprovalRequest.isEmpty()) {
			List<Approval.ProcessResult> lstResults = Approval.process(lstApprovalRequest);
			for(Approval.ProcessResult result : lstResults) {
				if(!result.isSuccess()) {
					newMap.get(result.getEntityId()).addError(result.getErrors().get(0).getMessage());
				}
			}
		}
	}

	public static void checkRelatedCaseBillingRequest(Map<Id, BI_O4_Disconnection__c> newMap, Map<Id, BI_O4_Disconnection__c> oldMap) {
		Map<Id, List<Case>> mapDisconnectionCases = getRelatedCases(newMap);

		for(BI_O4_Disconnection__c disc : newMap.values()) {
			System.debug(LoggingLevel.ERROR, '@@ NewStat: ' + disc.BI_O4_Status__c + ' OldStat: ' + oldMap.get(disc.Id).BI_O4_Status__c);
			if(disc.BI_O4_Status__c == BIO4_DISC_STAT_CLOSED && disc.BI_O4_Status__c != oldMap.get(disc.Id).BI_O4_Status__c) {
				Boolean hasInternalCase = false;

				if(mapDisconnectionCases.containsKey(disc.Id)) {
					for(Case cas : mapDisconnectionCases.get(disc.Id)) {
						if(cas.RecordType.DeveloperName == 'BI_O4_Billing_Request') {
							hasInternalCase = true;
							break;
						}
					}
				}
				if(!hasInternalCase) {
					disc.addError('At least one related "Billing Request" case has to be created before changing the Disconnection Status to "Closed".');
				}
			}
		}
	}

}