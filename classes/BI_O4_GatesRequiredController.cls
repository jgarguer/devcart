/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Controller class for visualforce gates required
 Test Class:    BI_O4_GatesRequiredController_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/07/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_GatesRequiredController
{
	public Quote quote {get; set;}
	
	public BI_O4_GatesRequiredController (ApexPages.standardController controller)
	{
		this.quote = (Quote) controller.getRecord();
	}
	
    public PageReference setGatesRequired()
    {
    	try
	    {
		    if (!this.quote.BI_O4_Gates_required__c)
		    {
		    	this.quote.BI_O4_Gates_required__c = true;
		        update this.quote;
		    }
	    }
	    catch (exception e)
	    {
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
	    	return null;
	    }
	    
	    return new PageReference('/' + this.quote.Id);
  	}
  	
	public PageReference cancel()
	{
		return new PageReference('/' + this.quote.Id);
	}
}