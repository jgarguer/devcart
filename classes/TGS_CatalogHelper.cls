/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   Class to get product catalog structure
                   
     Test Class:    TGS_CatalogHelper_Test
     History:
     
     <Date>            <Author>             <Description>
    01/03/2018         Antonio Pardo        First version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class TGS_CatalogHelper {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   Method to get multiple product structures divided in maps
                   
     IN:		Set<Id> - Item Header Id
				String - Name of the catalog
	 OUT:		Map<String, Object> - Map containing in each itemHeaderId the structure for that product
     History:
     
     <Date>            <Author>             <Description>
    01/03/2018         Antonio Pardo        First version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Map<String, Object> getProductTree(Set<Id> headerId, String catalog){
        
        list<NE__Catalog_Item__c> listCatalogItem= new list<NE__Catalog_Item__c>([SELECT 
                                                                                  id,name,BI_Nombre_del_Producto__c,NE__Active__c,NE__Catalog_Category_Name__c,
                                                                                  NE__Catalog_Category_Name__r.name,NE__Catalog_Id__c,NE__Catalog_Id__r.NE__Catalog_Header__C,
                                                                                  NE__Min_Qty__c, NE__Max_Qty__c,NE__Default_Qty__c,
                                                                                  NE__ProductId__c,NE__ProductId__r.name, NE__ProductId__r.NE__Source_Product_Id__c,NE__Type__c,NE__Parent_Catalog_Item__c,NE__Root_Catalog_Item__c,
                                                                                  NE__CheckRoot__c,NE__Complex_Item__c,NE__Item_Header__c, NE__Item_Header__r.NE__Source_Item_Header_Id__c,NE__Item_Header__r.name
                                                                                  FROM NE__Catalog_Item__c 
                                                                                  WHERE NE__Item_Header__r.NE__Source_Item_Header_Id__c IN :headerId  AND NE__Catalog_Id__r.Name=:catalog
                                                                                  AND NE__Active__c=true AND NE__Type__c!='Category' ORDER BY NE__Item_Header__c]);
        
        if(!listCatalogItem.isEmpty()){
            Map<String, List<NE__Catalog_Item__c>> map_cis= new Map<String, List<NE__Catalog_Item__c>>();
            for(NE__Catalog_Item__c ci : listCatalogItem){
                System.debug('CIProdName ' + ci.NE__ProductId__r.name);
                //separando los catalog items por su Item Header
                if(map_cis.containsKey(ci.NE__Item_Header__r.NE__Source_Item_Header_Id__c)){
                    map_cis.get(ci.NE__Item_Header__r.NE__Source_Item_Header_Id__c).add(ci);
                }else{
                    map_cis.put(ci.NE__Item_Header__r.NE__Source_Item_Header_Id__c, new List<NE__Catalog_Item__c>{ci});
                }
                
            }
            Map<String, Object> map_returnTree = new Map<String, Object>();
            System.debug('MapCi --> '  + map_cis);
            for(String sourceId : map_cis.keySet()){
                System.debug('cisCreateProd ' + map_cis.get(sourceId));
                map_returnTree.put(sourceId, createProductTree((List<NE__Catalog_Item__c>)map_cis.get(sourceId)));
            }
            return map_returnTree;
        }else{
            return null;
        }
        
    }
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   Method to get the catalogItems structre of the catalog
                   
     IN:		List<NE__Catalog_Item__c> - List of catalog items of the same item header
	 OUT:		Map<String, Object> - Map containing the structure of each catalogItem
     History:
     
     <Date>            <Author>             <Description>
    01/03/2018         Antonio Pardo        First version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Map<String, Object> createProductTree(List<NE__Catalog_Item__c> listCatalogItem){
        Map<Id,List<ProductAttributeWrapper>> mapIdProductListAttrWrapper = new Map<Id,List<ProductAttributeWrapper>>();
        //System.debug('Item Header ID '+strItemHeaderId);
        //System.debug('stringCatalogName '+ stringCatalogName);
        Set<Id> setIdProduct= new Set<Id>();
        
        /**list<NE__Catalog_Item__c> listCatalogItem= new list<NE__Catalog_Item__c>([SELECT 
id,name,BI_Nombre_del_Producto__c,NE__Active__c,NE__Catalog_Category_Name__c,
NE__Catalog_Category_Name__r.name,NE__Catalog_Id__c,NE__Catalog_Id__r.NE__Catalog_Header__C,
NE__Min_Qty__c, NE__Max_Qty__c,NE__Default_Qty__c,
NE__ProductId__c,NE__ProductId__r.name, NE__ProductId__r.NE__Source_Product_Id__c,NE__Type__c,NE__Parent_Catalog_Item__c,NE__Root_Catalog_Item__c,
NE__CheckRoot__c,NE__Complex_Item__c,NE__Item_Header__c,NE__Item_Header__r.name
FROM NE__Catalog_Item__c 
WHERE NE__Item_Header__c=:strItemHeaderId AND NE__Catalog_Id__r.Name=:stringCatalogName
AND NE__Active__c=true AND NE__Type__c!='Category']);**/
        
        for(NE__Catalog_Item__c item : listCatalogItem){
            
            setIdProduct.add(item.NE__ProductId__c);
        }
        
        system.debug('@@@@@@@@ MOF'); 
        system.debug('@@@@@@@@ MOF' + setIdProduct);
        system.debug('@@@@@@@@ MOF' + mapIdProductListAttrWrapper);
        
        mapIdProductListAttrWrapper = generateAttributesList(setIdProduct);
        
        Map<Id,Map<String,String>> mapIdProductAttrSeq = new Map<Id,Map<String,String>>();
        for(Id idKey: mapIdProductListAttrWrapper.KeySet()){
            List<ProductAttributeWrapper> listAttrs = mapIdProductListAttrWrapper.get(idKey);
            map<String,String> mapAttrSeq = new map<String,String>();
            system.debug('@@@@@@@@ MOF' + listAttrs);
            for(ProductAttributeWrapper attr : listAttrs){
                mapAttrSeq.put(attr.attribute,attr.strSequence);
            }
            mapIdProductAttrSeq.put(idKey,mapAttrSeq);
        }
        
        if(!listCatalogItem.IsEmpty()){
            //First take root catalog item ID - type=="Product"
            Id idProductTypeId;
            id idRootTypeId;
            List<ProductDataWrapper> listProductItem = new List<ProductDataWrapper>();
            Map<String,ProductDataWrapper> mapIdProductItem = new Map<String,ProductDataWrapper>();
            ProductDataWrapper rootProductItem = new ProductDataWrapper();
            
            Map<String,ProductDataWrapper> productsFirstLevelMap = new Map<String,ProductDataWrapper>();
            Map<String,ProductDataWrapper> productsSecondLevelMap = new Map<String,ProductDataWrapper>();
            Map<String,ProductDataWrapper> productsThirdLevelMap = new Map<String,ProductDataWrapper>();
            Map<String,ProductDataWrapper> productsFourthLevelMap = new Map<String,ProductDataWrapper>();
            
            for(NE__Catalog_Item__c catalogItem: listCatalogItem){
                if(catalogItem.NE__Type__c=='Product'){
                    System.debug('Product CatalogItem-->'+catalogItem);
                    idProductTypeId=catalogItem.id;
                }
                if(catalogItem.NE__Type__c=='Root'){
                    System.debug('Root CatalogItem-->'+catalogItem);
                    idRootTypeId=catalogItem.id;
                }
            }
            
            for(NE__Catalog_Item__c catalogItem: listCatalogItem){
                // 'Root' type CatalogItem must be ommited and 2nd level OI's must be linked with 'product' type CatalogItem
                if(catalogItem.NE__Type__c=='Root'){
                    System.debug('CIProdName root' + catalogItem.NE__ProductId__r.name);
                    continue;
                }
                
                // creation of order item data for a product item
                ProductDataWrapper newProductItem = new ProductDataWrapper();
                newProductItem.idCatalog = catalogItem.NE__Catalog_Id__c; // NE__Catalog__c
                newProductItem.catalogItemId = catalogItem.id;// NE__CatalogItem__c
                newProductItem.productId = catalogItem.NE__ProductId__c; // NE__ProdId__c
                newProductItem.productName = catalogItem.NE__ProductId__r.name;
                newProductItem.productSourceId = catalogItem.NE__ProductId__r.NE__Source_Product_Id__c;
                // mark 'product' type as root
                newProductItem.decMinQty=catalogItem.NE__Min_Qty__c;
                newProductItem.decMaxQty=catalogItem.NE__Max_Qty__c;
                newProductItem.decDefQty=catalogItem.NE__Default_Qty__c;
                if(catalogItem.NE__Min_Qty__c>0){
                    newProductItem.bRequired=true;
                }else{
                    newProductItem.bRequired=false;
                }
                if(catalogItem.id==idProductTypeId){
                    newProductItem.bRoot = true;
                }else{
                    newProductItem.bRoot = false;
                }
                // link 2nd level oi's with 'product' type catalogItem
                if(catalogItem.NE__Root_Catalog_Item__c==idRootTypeId){
                    newProductItem.strRootId = idProductTypeId;
                }else{
                    newProductItem.strRootId = catalogItem.NE__Root_Catalog_Item__c;
                }				
                newProductItem.strParentId = catalogItem.NE__Parent_Catalog_Item__c;
                newProductItem.listAttributes = mapIdProductListAttrWrapper.get(catalogItem.NE__ProductId__c);
                newProductItem.listChildProducts = new List<ProductDataWrapper>();
                newProductItem.mapAvailableProducts = new Map<String,Map<String, String>>();
                
                listProductItem.add(newProductItem);
                mapIdProductItem.put(newProductItem.catalogItemId,newProductItem);
                
                if(newProductItem.bRoot){
                    rootProductItem=newProductItem;
                }
            }
            
            // first pass -- root order items
            for(ProductDataWrapper productItem : listProductItem){				
                if(productItem.strRootId==null){
                    productItem.itemLevel=0;
                    productsFirstLevelMap.put(productItem.catalogItemId,productItem);					
                }
            }
            
            // second pass
            for(ProductDataWrapper productItem : listProductItem){				
                if(productItem.strRootId!=null && productsFirstLevelMap.containsKey(productItem.strRootId)){
                    productItem.itemLevel=1;
                    productsSecondLevelMap.put(productItem.catalogItemId,productItem);				
                }
            }
            
            // third pass
            for(ProductDataWrapper productItem : listProductItem){				
                if(productItem.strRootId!=null && productsSecondLevelMap.containsKey(productItem.strRootId)){
                    productItem.itemLevel=2;
                    productsThirdLevelMap.put(productItem.catalogItemId,productItem);				
                }
            }
            
            // fourth pass
            for(ProductDataWrapper productItem : listProductItem){				
                if(productItem.strRootId!=null && productsThirdLevelMap.containsKey(productItem.strRootId)){
                    productItem.itemLevel=3;
                    productsFourthLevelMap.put(productItem.catalogItemId,productItem);				
                }
            }
            
            // fill OiWrapper Maps
            for(string strKeyPrdWrapper :mapIdProductItem.keyset()){
                system.debug('@@@_MOF strKeyPrdWrapper: ' + strKeyPrdWrapper);
                ProductDataWrapper prdWrapper=mapIdProductItem.get(strKeyPrdWrapper);
                system.debug('@@@_MOF prdWrapper: ' + prdWrapper);
                
                if(mapIdProductItem.containsKey(prdWrapper.strRootId)){
                    system.debug('@@@_MOF root');
                    mapIdProductItem.get(prdWrapper.strRootId).listChildProducts.add(prdWrapper);
                    mapIdProductItem.get(prdWrapper.strRootId).mapAvailableProducts.put(prdWrapper.productSourceId, new Map<String, String>{prdWrapper.catalogItemId => prdWrapper.productName});
                	mapIdProductItem.get(prdWrapper.strRootId).mapAvailableProducts.get(prdWrapper.productSourceId).put('ciId', prdWrapper.catalogItemId);
                }
            }
            
            system.debug('@@@_MOF rootProductItem: ' + rootProductItem);
            String strPrdItemTreeJSON = JSON.serialize(rootProductItem);			
            system.debug('@@@_MOF strPrdItemTreeJSON: ' + strPrdItemTreeJSON);
            String strPrdItemJSON = JSON.serialize(mapIdProductItem);			
            system.debug('@@@_MOF strPrdItemJSON: ' + strPrdItemJSON);
            
            Map<String, Object> map_return = new Map<String, Object>();
            map_return.put('rootProductItem', rootProductItem);
            map_return.put('mapIdProductItem' , mapIdProductItem);
            return map_return;
            
        }else{
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   Method to get the attribute of the products
                   
     IN:		Set<Id> - set Id of products
	 OUT:		Map<Id,List<ProductAttributeWrapper>> - Map containing a list of products linked to their products
     History:
     
     <Date>            <Author>             <Description>
    01/03/2018         Antonio Pardo        First version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Map<Id,List<ProductAttributeWrapper>> generateAttributesList(Set<Id> setIdProduct){
        
        List<Id> setIdFamilies= new List<Id>();
        Map<Id,NE__ProductFamily__c> mapAuxProdFam=new Map<Id,NE__ProductFamily__c>();
        Map<String,String> mapStrAttrSequence = new Map<String,String>();
        List<Id> listIdEnumProps= new List<Id>();
        Map<Id,List<ProductAttributeWrapper>> mapIdProductListAttrWrapper = new Map<Id,List<ProductAttributeWrapper>>();
        Map<Id,List<NE__ProductFamilyProperty__c>> mapListPropertyByFamily = new Map<Id,List<NE__ProductFamilyProperty__c>>();
        
        Map<Id,ProductAttributeWrapper> mapProductAttr = new Map<Id,ProductAttributeWrapper>();
        
        Map<Id,NE__ProductFamily__c> mapProductFamilies = new Map<Id,NE__ProductFamily__c>([SELECT Id,Name,NE__FamilyId__c,NE__ProdId__c,NE__ProdId__r.Name,NE__Sequence__c
                                                                                            FROM NE__ProductFamily__c 
                                                                                            WHERE NE__ProdId__c IN: setIdProduct]);
        System.debug('mapProductFamilies'+ mapProductFamilies);
        for(NE__ProductFamily__c item : mapProductFamilies.values()){
            mapAuxProdFam.put(item.NE__FamilyId__c,item);
            setIdFamilies.add(item.NE__FamilyId__c);
        }
        
        // con los ids de las familias cogemos los atributos
        List<NE__ProductFamilyProperty__c> listProductFamilyProperty = new List<NE__ProductFamilyProperty__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name,NE__PropId__r.NE__Type__c, 
                                                                                                               NE__Required__c,NE__FamilyId__c,NE__FamilyId__r.Name,
                                                                                                               NE__Dynamic_Lookup__c,NE__Sequence__c,NE__Source_Product_Family_Property_Id__c
                                                                                                               from NE__ProductFamilyProperty__c 
                                                                                                               where NE__FamilyId__c IN :setIdFamilies
                                                                                                               ORDER BY NE__FamilyId__r.Name]);
        
        
        for(NE__ProductFamilyProperty__c item: listProductFamilyProperty){
            System.debug(item.NE__PropId__r.Name + ' - ' + item.NE__PropId__r.NE__Type__c);
            /*if(item.NE__PropId__r.NE__Type__c == 'Enumerated'){
                listIdEnumProps.add(item.NE__PropId__c);
            }*/
            listIdEnumProps.add(item.NE__PropId__c);
        }
        
        Map<Id,NE__PropertyDomain__c> mapEnumProps = new Map<Id,NE__PropertyDomain__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name  
                                                                                        from NE__PropertyDomain__c
                                                                                        where NE__PropId__c IN :listIdEnumProps]);		
        
        for(NE__ProductFamilyProperty__c property : listProductFamilyProperty){
            ProductAttributeWrapper attrWrapper = new ProductAttributeWrapper();
            attrWrapper.attrSourceId = property.NE__Source_Product_Family_Property_Id__c;
            attrWrapper.pfpId = property.Id;
            if(property.NE__Required__c == 'Yes'){
                attrWrapper.required = true;
                attrWrapper.selected = true;
            }else{
                attrWrapper.required = false;
                attrWrapper.selected = false;
            }
            attrWrapper.attribute = property.NE__PropId__r.Name;
            attrWrapper.xType = property.NE__PropId__r.NE__Type__c;
            if(attrWrapper.xType=='Dynamic Lookup'){
                attrWrapper.strIdDynLookUp = property.Id;
                attrWrapper.strNameDynLookUp = property.NE__Dynamic_Lookup__c;
            }else{
                attrWrapper.strIdDynLookUp = 'false';
                attrWrapper.strNameDynLookUp = 'false';
            }
            attrWrapper.idFamily = property.NE__FamilyId__c;
            attrWrapper.family = property.NE__FamilyId__r.Name;
            String strSequence;
            if(property.NE__Sequence__c!=null){
                strSequence=String.valueOf(property.NE__Sequence__c);
                if(strSequence.length()==1){
                    strSequence='0' + strSequence;
                }
            }else{
                strSequence=property.NE__PropId__r.Name;
            }
            
            if(mapAuxProdFam.get(property.NE__FamilyId__c).NE__Sequence__c==null){
                attrWrapper.strSequence=property.NE__FamilyId__r.Name + '_' + strSequence;
            }else{
                attrWrapper.strSequence=mapAuxProdFam.get(property.NE__FamilyId__c).NE__Sequence__c + '_' + strSequence;
            }
            
            
            system.debug('@@@__MOF: ' + mapProductFamilies);
            system.debug('@@@__MOF: ' + attrWrapper.idFamily);
            
            if(attrWrapper.xType == 'Enumerated'){
                List<String> auxValues = new List<String>();
                for(NE__PropertyDomain__c enumProp : mapEnumProps.values()){
                    if(enumProp.NE__PropId__c == property.NE__PropId__c){
                        auxValues.add(enumProp.Name);
                    }
                }
                attrWrapper.enumeratedValues = auxValues;
            }			
            mapProductAttr.put(property.id,attrWrapper);
            
            // loop oiaMap to link DynLookUp fields by family
            for(String attrWrapperId : mapProductAttr.keySet()){
                ProductAttributeWrapper wrapper=mapProductAttr.get(attrWrapperId);
                // if type is DynLookUp loop again to search all the family
                if(wrapper.xType=='Dynamic Lookup'){
                    wrapper.mapDynLookUpValues = new Map<String,String>();
                    wrapper.mapDynLookUpValues.put(wrapper.attribute,wrapper.attribute);
                    for(String attrWrapperIdAux : mapProductAttr.keySet()){
                        ProductAttributeWrapper auxWrapper=mapProductAttr.get(attrWrapperIdAux);
                        if(wrapper.family==auxWrapper.family && attrWrapperId!=attrWrapperIdAux){
                            auxWrapper.strIdDynLookUp=attrWrapperId;
                            //auxWrapper.xType='DynLookUpField';
                            wrapper.mapDynLookUpValues.put(auxWrapper.attribute,auxWrapper.attribute);
                        }
                    }
                }
            }							
        }
        
        for(Id idProduct :setIdProduct){
            List<ProductAttributeWrapper> listAttr = new List<ProductAttributeWrapper>();
            for( NE__ProductFamily__c productFamily :mapProductFamilies.values()){
                if(productFamily.NE__ProdId__c==idProduct){
                    for(ProductAttributeWrapper attrWrapper :mapProductAttr.values()){
                        if(attrWrapper.idFamily==productFamily.NE__FamilyId__c){
                            listAttr.add(attrWrapper);
                        }
                    }
                }
            }
            mapIdProductListAttrWrapper.put(idProduct,listAttr);
        }
        return 	mapIdProductListAttrWrapper;
    }
         


   
    
    public class ProductDataWrapper{
        public Id idCatalog; // NE__Catalog__c
        public Id catalogItemId; // NE__CatalogItem__c
        public Id productId; // NE__ProdId__c
        public Id productSourceId;
        public String productName; // product name
        public decimal decMinQty;
        public decimal decMaxQty;
        public decimal decDefQty;
        public Boolean bRequired;
        public Boolean bRoot;
        public integer itemLevel;
        public String strRootId;
        public String strParentId;
        public List<ProductAttributeWrapper> listAttributes;
        public List<ProductDataWrapper> listChildProducts;
        public Map<String,Map<String, String>> mapAvailableProducts;	 
    }
    
    public class ProductAttributeWrapper {
        public Boolean selected;
        public String attribute;
        public String idFamily;
        public Id attrSourceId;
        public Id pfpId;
        public String family;	
        public Boolean required;
        public String strSequence;
        public String xType;
        public String strIdDynLookUp;
        public String strNameDynLookUp;
        public Map<String,String> mapDynLookUpValues;
        public List<String> enumeratedValues;
    }
}