/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Alejandro Labrin
Company:		Everis Centers Temuco
Description:	RFG-NOR-DIR-004 / RFG-NOR-DIR-005 / RFG-NOR-DIR-006: 
Página Lightning Experience para normalización de direcciones.

History:

<Date>                      <Author>                        <Change Description>
22/11/2016                  Alejandro Labrin				Initial Version
24/10/2017					Daniel Leal		   				Ever01:Inclusion de logs
12/01/2018					Jonatan Rey                     Ever02: Cambio del mapeo de los campos betweenstreet1 y 2
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class FS_CHI_Address_Record_Controller {
    
    @AuraEnabled
    public static BI_Sede__c[] SearchAddress(String addressId) {
        System.debug('Nombre de clase: FS_CHI_Address_Record_Controller   Nombre de método: SearchAddress   Comienzo del método');
        /*Variable Declaration*/
        BI_Sede__c[] result; 
        try{            
            BI_Sede__c[] temp = Database.query('SELECT Id, BI_interno_id_direccion__c, Estado__c, Name, BI_Direccion__c, BI_Numero__c, BI_Provincia__c, BI_Country__c, BI_Codigo_postal__c, BI_Distrito__c, BI_Localidad__c, BI_Piso__c, BI_Apartamento__c FROM BI_Sede__c WHERE id = \''+addressId+'\'');
            if(!temp.isEmpty()) result = temp;
            else result = null;
        }catch(Exception except){
            BI_LogHelper.generate_BILog('FS_CHI_Address_Record_Controller.SearchAddress', 'BI_EN', except, 'Trigger');
        }
        System.debug('Nombre de clase: FS_CHI_Address_Record_Controller   Nombre de método: SearchAddress   Finalización del método    Resultado: ' + result);
        return result;
    }
    
    @AuraEnabled
    public static String getNormalizedAddress(String addressId) {
        System.debug('Nombre de clase: FS_CHI_Address_Record_Controller   Nombre de método: getNormalizedAddress   Comienzo del método');
        /*Variable Declaration*/
        List<BI_Sede__c[]> respuesta = new List<BI_Sede__c[]>();
        BI_Sede__c temp;
        try {
            temp = Database.query('SELECT BI_Direccion__c, BI_Numero__c, BI_Piso__c, BI_Apartamento__c, BI_Localidad__c, BI_Distrito__c, BI_Provincia__c, BI_Country__c, BI_Codigo_postal__c, FS_CHI_Bloque__c, FS_CHI_Tipo_Complejo_Habitacional__c, FS_CHI_Nombre_Complejo_Habitacional__c, FS_CHI_Numero_de_Casilla__c FROM BI_Sede__c WHERE id = \''+addressId+'\'');
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('FS_CHI_Address_Record_Controller.getNormalizedAddress', 'BI_EN', exc, 'Trigger');
        }        
        if(temp != null){
            String calle = temp.BI_Direccion__c;
            String numero = temp.BI_Numero__c;
            String piso = temp.BI_Piso__c;
            String apartamento = temp.BI_Apartamento__c;
            String area = '';
            String localidad = temp.BI_Localidad__c;
            String distrito = temp.BI_Distrito__c;
            String provincia = temp.BI_Provincia__c;
            String pais = temp.BI_Country__c;
            String codigoPostal = temp.BI_Codigo_postal__c;
            
            //Nuevos campos
            String bloque = temp.FS_CHI_Bloque__c;
            String tipoComplejo = temp.FS_CHI_Tipo_Complejo_Habitacional__c;
            String nombreComplejo = temp.FS_CHI_Nombre_Complejo_Habitacional__c;
            String numCasilla = temp.FS_CHI_Numero_de_Casilla__c;
            
            BI_RestWrapper.ValidateAddressResponseType validateAddressResponseType = FS_CHI_Address_Management.getValidAddresses(calle, numero, piso, apartamento, area, localidad, distrito, provincia, pais, codigoPostal, bloque, tipoComplejo, nombreComplejo, numCasilla);
            
            if (validateAddressResponseType != null){
                if(validateAddressResponseType.Status == 'Validated'){
                    BI_Sede__c[] result = new List<BI_Sede__c>();                    
                    if (validateAddressResponseType.matchingAddresses != null) {                   
                        BI_Sede__c responseAddressOK;
                        //MATCHINFADDRESS LIST
                        for(BI_RestWrapper.AddressInfoType adressInfo : validateAddressResponseType.matchingAddresses.validAddresses){                       
                            responseAddressOK = new BI_Sede__c();
                            for(BI_RestWrapper.KeyvalueType keyValue : adressInfo.additionalData){
                                if(keyValue.Key == 'additionalComments'){
                                    responseAddressOK.BI_Direccion_2__c = keyValue.value;
                                } else if(keyValue.Key == 'betweenStreet1'){
                                  //ever02  responseAddressOK.BI_COL_Complemento_1__c = keyValue.value;
							      //ever02 - INI
                                    responseAddressOK.BI_FVI_Entrecalle_1__c = keyValue.value;
                                  //ever02 - FIN
                                } else if(keyValue.Key == 'betweenStreet2'){
                               //ever02 - INI     
                               //ever02 responseAddressOK.BI_COL_Complemento_2__c = keyValue.value;
                                  responseAddressOK.BI_FVI_Entrecalle_2__c = keyValue.value;
                               //ever02 - FIN
                                } else if(keyValue.Key == 'block'){
                                    responseAddressOK.FS_CHI_Bloque__c = keyValue.value;
                                } else if(keyValue.Key == 'housingComplexType'){
                                    responseAddressOK.FS_CHI_Tipo_Complejo_Habitacional__c = keyValue.value;
                                } else if(keyValue.Key == 'housingComplexName'){
                                    responseAddressOK.FS_CHI_Nombre_Complejo_Habitacional__c = keyValue.value;
                                } else if(keyValue.Key == 'boxNr'){
                                    responseAddressOK.FS_CHI_Numero_de_Casilla__c = keyValue.value;
                                } else if(keyValue.Key == 'isRedZone'){
                                    responseAddressOK.FS_CHI_Es_Zona_Roja__c = (keyValue.value == 'Y');
                                } else if(keyValue.Key == 'isBlacklisted'){
                                    responseAddressOK.FS_CHI_Es_Lista_Negra__c = (keyValue.value == 'Y');
                                }
                            }
                            responseAddressOK.BI_Country__c = adressInfo.country;
                            responseAddressOK.BI_Provincia__c = adressInfo.region;
                            if(adressInfo.municipality != null || adressInfo.municipality != ''){
                                responseAddressOK.BI_Distrito__c = adressInfo.municipality;
                            }
                            responseAddressOK.BI_Localidad__c = adressInfo.locality;
                            responseAddressOK.BI_Direccion__c = adressInfo.addressName;
                            if(adressInfo.addressNumber != null){
                                responseAddressOK.BI_Numero__c = adressInfo.addressNumber.value;                            
                            }
                            responseAddressOK.BI_Codigo_postal__c = adressInfo.postalCode;
                            if(adressInfo.floor != null){
                                responseAddressOK.BI_Piso__c = adressInfo.floor.value;
                            }
                            if(adressInfo.apartment != null){
                                responseAddressOK.BI_Apartamento__c = adressInfo.apartment.value;
                            } 
                            if(adressInfo.coordinates.longitude != null && adressInfo.coordinates.latitude != null){                            
                                responseAddressOK.BI_Longitud__Latitude__s = Double.valueOf(adressInfo.coordinates.latitude);
                                responseAddressOK.BI_Longitud__Longitude__s = Double.valueOf(adressInfo.coordinates.longitude);
                            }                            
                            result.add(responseAddressOK);
                        }
                        respuesta.add(result);
                    }
                    //DOUBTADDRESS
                    if (validateAddressResponseType.doubtAddresses != null) {  
                        BI_Sede__c[] resultNOK = new List<BI_Sede__c>();
                        BI_Sede__c responseAddressNOK;
                        for(BI_RestWrapper.AddressInfoType adressInfo : validateAddressResponseType.doubtAddresses.validAddresses){                            
                            responseAddressNOK = new BI_Sede__c();
                            for(BI_RestWrapper.KeyvalueType keyValue : adressInfo.additionalData){
                                if(keyValue.Key == 'additionalComments'){
                                    responseAddressNOK.BI_Direccion_2__c = keyValue.value;
                                } else if(keyValue.Key == 'betweenStreet1'){
                          //ever02  responseAddressNOK.BI_COL_Complemento_1__c = keyValue.value;
                          //ever02 - INI
                                  responseAddressNOK.BI_FVI_Entrecalle_1__c = keyValue.value;
                          //ever02 - FIN
                                } else if(keyValue.Key == 'betweenStreet2'){
                          //ever02  responseAddressNOK.BI_COL_Complemento_2__c = keyValue.value;
                          //ever02 - INI
                                  responseAddressNOK.BI_FVI_Entrecalle_2__c = keyValue.value;
                          //ever02 - FIN
                                } else if(keyValue.Key == 'block'){
                                    responseAddressNOK.FS_CHI_Bloque__c = keyValue.value;
                                } else if(keyValue.Key == 'housingComplexType'){
                                    responseAddressNOK.FS_CHI_Tipo_Complejo_Habitacional__c = keyValue.value;
                                } else if(keyValue.Key == 'housingComplexName'){
                                    responseAddressNOK.FS_CHI_Nombre_Complejo_Habitacional__c = keyValue.value;
                                } else if(keyValue.Key == 'boxNr'){
                                    responseAddressNOK.FS_CHI_Numero_de_Casilla__c = keyValue.value;
                                } else if(keyValue.Key == 'isRedZone'){
                                    responseAddressNOK.FS_CHI_Es_Zona_Roja__c = (keyValue.value == 'Y');
                                } else if(keyValue.Key == 'isBlacklisted'){
                                    responseAddressNOK.FS_CHI_Es_Lista_Negra__c = (keyValue.value == 'Y');
                                }
                            }
                            responseAddressNOK.BI_Country__c = adressInfo.country;
                            responseAddressNOK.BI_Provincia__c = adressInfo.region;
                            if(adressInfo.municipality != null || adressInfo.municipality != ''){
                                responseAddressNOK.BI_Distrito__c = adressInfo.municipality;
                            }
                            responseAddressNOK.BI_Localidad__c = adressInfo.locality;
                            responseAddressNOK.BI_Direccion__c = adressInfo.addressName;
                            if(adressInfo.addressNumber != null){
                                responseAddressNOK.BI_Numero__c = adressInfo.addressNumber.value;                            
                            }
                            responseAddressNOK.BI_Codigo_postal__c = adressInfo.postalCode;
                            if(adressInfo.floor != null){
                                responseAddressNOK.BI_Piso__c = adressInfo.floor.value;
                            }
                            if(adressInfo.apartment != null){
                                responseAddressNOK.BI_Apartamento__c = adressInfo.apartment.value;
                            } 
                            if(adressInfo.coordinates.longitude != null && adressInfo.coordinates.latitude != null){
                                responseAddressNOK.BI_Longitud__Latitude__s = Double.valueOf(adressInfo.coordinates.latitude);
                                responseAddressNOK.BI_Longitud__Longitude__s = Double.valueOf(adressInfo.coordinates.longitude);
                            }
                            resultNOK.add(responseAddressNOK);
                        }
                        respuesta.add(resultNOK);
                    }
                    return JSON.serialize(respuesta);
                }
            }
        }
        System.debug('Nombre de clase: FS_CHI_Address_Record_Controller   Nombre de método: getNormalizedAddress   Finalización del método    Resultado: ' + JSON.serialize(respuesta));
        return JSON.serialize(respuesta);
    }
    
    @AuraEnabled
    public static String updateAddress(String addressId, String direccion_2, String pais, String provincia, String distrito, String localidad, String calle, String numero, String codigoPostal, String comple1, String comple2, String piso, String apartamento, String longitud, String latitud, String bloque, String tipoComplejo, String nomComplejo, String numCasilla, String zonaRed, String listaNegra) {
        System.debug('Nombre de clase: FS_CHI_Address_Record_Controller   Nombre de método: updateAddress   Comienzo del método');
        /*Variable Definition*/
        String response = 'SUCCESS';
        //Ever01-INI
        try {
            //Ever01-FIN
            BI_Sede__c updateAddress = new BI_Sede__c (id = addressId);
            updateAddress.BI_Direccion_2__c = direccion_2;
            updateAddress.BI_Country__c = pais;
            updateAddress.BI_Provincia__c = provincia;
            updateAddress.BI_Distrito__c = distrito;
            updateAddress.BI_Localidad__c = localidad;
            updateAddress.BI_Direccion__c = calle;
            updateAddress.BI_Numero__c = numero;
            updateAddress.BI_Codigo_postal__c = codigoPostal;
            //ever02 - INI
            updateAddress.BI_COL_Complemento_1__c = comple1;
            // updateAddress.BI_FVI_Entrecalle_1__c = comple1;
            //ever02 updateAddress.BI_COL_Complemento_2__c = comple2;
            updateAddress.BI_FVI_Entrecalle_2__c = comple2;
            //ever02 - FIN
            updateAddress.BI_Piso__c = piso;
            updateAddress.BI_Apartamento__c = apartamento;
            updateAddress.Estado__c = 'Validado';
            updateAddress.BI_Longitud__Latitude__s = Double.valueOf(latitud);
            updateAddress.BI_Longitud__Longitude__s = Double.valueOf(longitud);
            
            //Nuevos campos
            updateAddress.FS_CHI_Bloque__c = bloque;
            updateAddress.FS_CHI_Tipo_Complejo_Habitacional__c = tipoComplejo;
            updateAddress.FS_CHI_Nombre_Complejo_Habitacional__c = nomComplejo;
            updateAddress.FS_CHI_Numero_de_Casilla__c = numCasilla;
            updateAddress.FS_CHI_Es_Zona_Roja__c = zonaRed == 'true';
            updateAddress.FS_CHI_Es_Lista_Negra__c = listaNegra == 'true';            
            update updateAddress;
            //Ever01-INI
        } catch(Exception exc){
            BI_LogHelper.generate_BILog('FS_CHI_Address_Record_Controller.updateAddress', 'BI_EN', exc, 'Trigger');
            response = 'NOK';
        }
        //Ever01-FIN
        System.debug('Nombre de clase: FS_CHI_Address_Record_Controller   Nombre de método: updateAddress   Finalización del método    Resultado: ' + response);
        return response;
    }
    
}