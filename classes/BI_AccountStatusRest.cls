@RestResource(urlMapping='/customeraccounts/v1/accounts/status/*')
global class BI_AccountStatusRest
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Class for Account Rest WebService that updates account status
    
    History:
    
    <Date>            <Author>          	<Description>
    10/11/2015		  Fernando Arteaga		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Updates the account status when called from CSB after suspension / reactivation from BI_EN.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          	<Description>
    10/11/2015		  Fernando Arteaga		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@HttpPost
	global static void updateAccountStatus()
	{
		try{
			RestRequest req = RestContext.request;
			Blob blob_json = req.requestBody; 
			String string_json = blob_json.toString().replace('"Transaction":', '"Transaction_x":'); 
			BI_RestWrapper.ApiTransactionNotificationType apiNotifType = (BI_RestWrapper.ApiTransactionNotificationType) JSON.deserialize(string_json, BI_RestWrapper.ApiTransactionNotificationType.class);
			List<Account> listAcc = new List<Account>();
			Boolean doUpdate = false;
			
			if (apiNotifType.transaction_x != null)
			{
				String transId = apiNotifType.transaction_x.transactionId;
										     
				listAcc = [SELECT Id, BI_COL_Estado__c, BI_Activo__c
						   FROM Account
						   WHERE BI_CSB_TransactionId__c = :transId];
				
				if (listAcc.isEmpty())
				{
					// If the account is not found, it will be a CSB deactivation callback (transactionId couldn't be saved because the account was deactivated)
					// We get the account from the response body
					String accId = apiNotifType.transaction_x.resourceUri != null ? apiNotifType.transaction_x.resourceUri.substring(apiNotifType.transaction_x.resourceUri.lastIndexOf('/') + 1) : null;
					System.debug('accId: ' + accId);
					
					if (accId != null)
					{
						listAcc = [SELECT Id, BI_COL_Estado__c, BI_Activo__c
								   FROM Account
								   WHERE BI_Id_CSB__c = :accId];
					}
					else
					{
						RestContext.response.statusCode = 404;
						return;
					}
				}
				
				List<BI_Log__c> trans = [SELECT Id, BI_COL_Estado__c
						 				 FROM BI_Log__c
										 WHERE BI_CSB_Record_Id__c = :String.valueOf(listAcc[0].Id).substring(0, 15)
										 ORDER BY CreatedDate DESC LIMIT 1];
				
				if (apiNotifType.transaction_x.transactionStatus.transactionStatus == 'Success')
				{
					if (listAcc[0].BI_COL_Estado__c == Label.BI_CSB_Pendiente_Suspension)
					{
						listAcc[0].BI_COL_Estado__c = Label.BI_CSB_Suspendido;
						doUpdate = true;
					}
					else if (listAcc[0].BI_COL_Estado__c == Label.BI_CSB_Pendiente_Reactivacion)
					{
						listAcc[0].BI_COL_Estado__c = Label.BI_CSB_Activo;
						doUpdate = true;
					}
					
					if (doUpdate)
						update listAcc[0];
					
					// Si se trata de un callback de cancelación eliminamos el parque comercial
					if (listAcc[0].BI_Activo__c == 'No')
					{
						List<NE__Asset__c> listAssets = [SELECT Id
														 FROM NE__Asset__c
														 WHERE NE__AccountId__c = :listAcc[0].Id];
														 
						if (!listAssets.isEmpty())
							delete listAssets;
					}
				}
				
				if (apiNotifType.transaction_x.transactionStatus.transactionStatus == 'Success' ||
					apiNotifType.transaction_x.transactionStatus.transactionStatus == 'Fail')
				{
					if (!trans.isEmpty())
					{
						trans[0].BI_COL_Estado__c = apiNotifType.transaction_x.transactionStatus.transactionStatus == 'Success' ? Label.BI_CSB_Procesado : Label.BI_CSB_Fallido;
						if (apiNotifType.transaction_x.transactionStatus.transactionStatus == 'Fail')
							trans[0].Descripcion__c = apiNotifType.transaction_x.transactionStatus.error.exceptionText;
						update trans[0];
					}
				}
				
				RestContext.response.statuscode = 200;//OK
			}
			else
			{
				RestContext.response.statuscode = 400;
				RestContext.response.headers.put('errorMessage', 'Parameter \'Transaction\' is mandatory');
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_AccountStatusRest.updateAccountStatus', 'BI_EN', exc, 'Web Service');
			
		}
	}
}