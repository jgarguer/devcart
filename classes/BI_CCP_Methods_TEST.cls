@isTest
public class BI_CCP_Methods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jose Miguel Fierro
    Company:       Salesforce.com
    Description:   Methods to test coverage of BI_CCP_Methods 
    Test Class:    BI_CCP_Methods
    
    History:
     
    <Date>                  <Author>                <Change Description>
    26/05/2015              Jose Miguel Fierro      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static testMethod void insertPermission_CCP_TEST() {
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);         
        System.debug('+Acc: ' + acc);
        
        List<Contact> contact = BI_DataLoad.loadContacts(1, acc);
        
        BI_Permisos_PP__c permiso = new BI_Permisos_PP__c();
        permiso.BI_Country__c = acc[0].BI_Country__c;
        permiso.BI_Segment__c = acc[0].BI_Segment__c;
        insert permiso;
        
        User usr = BI_DataLoad.loadPortalUser(contact[0].Id,BI_DataLoad.searchPortalProfile());
        
        System.debug('__ usr __: ' + usr);
        //permiso.OwnerId = usr.Id;
        //update permiso;
        
        System.runAs(usr) {
            
            List<BI_Contact_Customer_Portal__c> lstCCPs = new List<BI_Contact_Customer_Portal__c>();
            for(Integer iter = 0; iter< 5; iter++) {
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=usr.Id, BI_User__c=usr.Id);
                lstCCPs.add(ccp);
            }
            insert lstCCPs;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Methods to test coverage of BI_CCP_Methods.insert_AccountTeamMember_CCP method.
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/10/2015              Micah Burgos            Initial Version     
    09/12/2016              Gawron, Julián          Change Assert to new Dataload
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void insertAccountShare_CCP_TEST() {

        Integer K = 5;
        K = BI_Dataload.MAX_LOOP; //JEG 09/12/2016

        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        List<Account>lst_acc = BI_DataLoad.loadAccounts(K, lst_pais);  
        System.assertEquals(K, lst_acc.size());
        System.debug('+Acc: ' +lst_acc);
        
        List<Contact> lst_con = BI_DataLoad.loadContacts(1,lst_acc);
        System.assertEquals(K, lst_con.size());
        
        BI_Permisos_PP__c permiso = new BI_Permisos_PP__c();
        permiso.BI_Country__c =lst_acc[0].BI_Country__c;
        permiso.BI_Segment__c =lst_acc[0].BI_Segment__c;
        insert permiso;
        
        User usr = BI_DataLoad.loadPortalUser(lst_con[0].Id,BI_DataLoad.searchPortalProfile());
        
        //System.runAs(usr) {
            
        //    List<BI_Contact_Customer_Portal__c> lstCCPs = new List<BI_Contact_Customer_Portal__c>();
        //    for(Integer iter = 0; iter< 5; iter++) {
        //        BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=usr.Id, BI_User__c=usr.Id);
        //        lstCCPs.add(ccp);
        //    }
        //    insert lstCCPs;
        //}

        List<BI_Contact_Customer_Portal__c> lstCCPs = new List<BI_Contact_Customer_Portal__c>();
        for(Account acc : lst_acc) {
            BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(BI_Cliente__c = acc.Id, BI_User__c=usr.Id, BI_Activo__c = true);
            lstCCPs.add(ccp);
        }
        Test.startTest();
        insert lstCCPs;
        Test.stopTest();

        List<AccountShare> lst_ACTM = [SELECT Id FROM AccountShare];
        System.debug('********lst_ACTM: ' + lst_ACTM);
        System.assert(!lst_ACTM.isEmpty());

    }

    /*
    09/12/2016              Gawron, Julián          Change Assert to new Dataload
    */
    public static testMethod void deleteAccountShare_CCP_TEST() {

        Integer K = 5;
        K = BI_Dataload.MAX_LOOP; //JEG 09/12/2016

        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        List<Account>lst_acc = BI_DataLoad.loadAccounts(K, lst_pais);   
        System.assertEquals(K, lst_acc.size());     
        System.debug('+Acc: ' +lst_acc);
        
        List<Contact> lst_con = BI_DataLoad.loadContacts(1,lst_acc);
        System.assertEquals(K, lst_con.size());
        
        BI_Permisos_PP__c permiso = new BI_Permisos_PP__c();
        permiso.BI_Country__c =lst_acc[0].BI_Country__c;
        permiso.BI_Segment__c =lst_acc[0].BI_Segment__c;
        insert permiso;
        
        User usr = BI_DataLoad.loadPortalUser(lst_con[0].Id,BI_DataLoad.searchPortalProfile());
        
        //System.runAs(usr) {
            
        //    List<BI_Contact_Customer_Portal__c> lstCCPs = new List<BI_Contact_Customer_Portal__c>();
        //    for(Integer iter = 0; iter< 5; iter++) {
        //        BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=usr.Id, BI_User__c=usr.Id);
        //        lstCCPs.add(ccp);
        //    }
        //    insert lstCCPs;
        //}

        List<BI_Contact_Customer_Portal__c> lstCCPs = new List<BI_Contact_Customer_Portal__c>();
        for(Account acc : lst_acc) {
            BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(BI_Cliente__c = acc.Id, BI_User__c=usr.Id, BI_Activo__c = true);
            lstCCPs.add(ccp);
        }
        
        insert lstCCPs;
        

        List<AccountShare> lst_ACTM = [SELECT Id FROM AccountShare];
        System.debug('********lst_ACTM: ' + lst_ACTM);
        System.assert(!lst_ACTM.isEmpty());
        
        Test.startTest();
        delete lstCCPs;
        Test.stopTest();

    }

    public static testMethod void insert_BI_Logs_TEST() {
            String e = 'uno';
            List<String> error = new List<String>();
            error.add(e);
            BI_CCP_Methods.insert_BI_Logs(error);

    }
}