/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for batch BI_COL_NoveltyBilling_cls
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-24      Raul Mora (RM)                  Create Class  
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*************************************************************************************/
@isTest
private class BI_COL_NoveltyBilling_tst
{
    
    public static List<BI_COL_Modificacion_de_Servicio__c> lstModServCreate;
    public static List<BI_COL_Cobro_Parcial__c> lstCobroParcial;
    public static list<User> lstUser;
    public static User objUsuario;
    public static list <UserRole> lstRoles;
    public static Contact                               objContacto;
    public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;
    public static BI_COL_Modificacion_de_Servicio__c    objModSer;
    
    public static void createData()
    {
       /* List<Profile> lstProfile = [ SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
       //List<User> lstUser = [ Select Id FROM User where country != 'Colombia' AND ProfileId =: lstProfile[0].Id And isActive = true Limit 1 ];
         //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstProfile.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUser = new list<User>();
        lstUser.add(objUsuario);



        System.runAs( lstUser[0] )
        {*/
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c = true;
            insert objBibypass;
            
            Account objAccount = new Account();      
            objAccount.Name = 'prueba';
            objAccount.BI_Country__c = 'Colombia';
            objAccount.TGS_Region__c = 'América';
            objAccount.BI_Tipo_de_identificador_fiscal__c = '';
            objAccount.CurrencyIsoCode = 'COP'; 
            objAccount.BI_Fraude__c = false;
            insert objAccount;

                         //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objAccount.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto; 

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objAccount.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);


            Opportunity objOpp = new Opportunity();
            objOpp.Name = 'prueba opp';
            objOpp.AccountId = objAccount.Id;
            objOpp.BI_Country__c = 'Colombia';
            objOpp.CloseDate = System.today().addDays(+5);
            //objOpp.StageName = 'F5 - Solution Definition';
            objOpp.StageName = 'F6 - Prospecting';
            objOpp.CurrencyIsoCode = 'COP';
            objOpp.Certa_SCP__contract_duration_months__c = 12;
            objOpp.BI_Plazo_estimado_de_provision_dias__c = 0 ;
            //objOpp.OwnerId = lstUser[0].id;
            insert objOpp;      

            NE__Order__c objOrder =  new NE__Order__c();
            objOrder.NE__OptyId__c = objOpp.Id; 
            objOrder.NE__OrderStatus__c='Active';
            insert objOrder;    

            RecordType objRecTyp = [ Select Id From RecordType Where sObjectType = 'NE__Product__c' Limit 1 ];

            NE__Product__c objProd = new NE__Product__c();
            objProd.Name = Constants.PRODUCT_SMDM_INVENTORY;
            objProd.BI_COL_TipoRegistro__c = objRecTyp.Id;
            insert objProd;

            NE__OrderItem__c objOrdItm  = new NE__OrderItem__c();
            objOrdItm.NE__OrderId__c = objOrder.Id; 
            objOrdItm.CurrencyIsoCode = 'USD';
            objOrdItm.NE__OneTimeFeeOv__c = 100;
            objOrdItm.NE__RecurringChargeOv__c = 100;
            objOrdItm.Fecha_de_reasignaci_n_a_factura__c = date.today();
            objOrdItm.NE__Qty__c = 1;
            objOrdItm.NE__ProdId__c = objProd.Id;
            insert objOrdItm;       

            BI_COL_Descripcion_de_servicio__c objDescServ = new BI_COL_Descripcion_de_servicio__c();
            objDescServ.BI_COL_Codigo_paquete__c = '123456789';
            objDescServ.BI_COL_Producto_Telefonica__c = objOrdItm.Id;
            objDescServ.BI_COL_Estado_de_servicio__c = 'Inactiva';
            objDescServ.BI_COL_Oportunidad__c = objOpp.Id;
            insert objDescServ; 
            
            lstModServCreate = new List<BI_COL_Modificacion_de_Servicio__c>(); 
            
            BI_COL_Modificacion_de_Servicio__c objModServ = new BI_COL_Modificacion_de_Servicio__c();
            objModServ.BI_COL_Estado__c = 'Enviado';
            objModServ.BI_COL_Clasificacion_Servicio__c = 'ALTA';
            objModServ.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ.BI_COL_Delta_modificacion_upgrade__c = 50;
            objModServ.BI_COL_Producto__c = objOrdItm.Id;
            objModServ.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            objModServ.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ.BI_COL_Bloqueado__c                   = false;
            objModServ.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            lstModServCreate.add( objModServ );

            BI_COL_Modificacion_de_Servicio__c objModServ2 = new BI_COL_Modificacion_de_Servicio__c();
            objModServ2.BI_COL_Estado__c = 'Enviado';
            objModServ2.BI_COL_Clasificacion_Servicio__c = 'SERVICIO INGENIERIA';
            objModServ2.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ2.BI_COL_Delta_modificacion_upgrade__c = 50;
            objModServ2.BI_COL_Producto__c = objOrdItm.Id;
            objModServ2.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            objModServ2.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ2.BI_COL_Bloqueado__c                   = false;
            objModServ2.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ2.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            lstModServCreate.add( objModServ2 );    
            
            insert lstModServCreate;
            
            lstCobroParcial = new List<BI_COL_Cobro_Parcial__c>();
            
            BI_COL_Cobro_Parcial__c objCobroParcial = new  BI_COL_Cobro_Parcial__c ();
            objCobroParcial.BI_COL_MS__c = objModServ.Id;
            objCobroParcial.BI_COL_No_Renovable__c = false;
            objCobroParcial.BI_COL_Procesado__c = false;
            objCobroParcial.BI_COL_Renovable__c = false;
            objCobroParcial.BI_COL_Fecha_Inicial__c = System.today();
            objCobroParcial.BI_COL_Fecha_final__c = System.today();
            objCobroParcial.BI_COL_Fecha_envio__c = System.today();
            objCobroParcial.BI_COL_Valor_Cobro__c = 1500000;
            lstCobroParcial.add( objCobroParcial );
            
            BI_COL_Cobro_Parcial__c objCobroParcial2 = new  BI_COL_Cobro_Parcial__c ();
            objCobroParcial2.BI_COL_MS__c = objModServ2.Id;
            objCobroParcial2.BI_COL_No_Renovable__c = false;
            objCobroParcial2.BI_COL_Procesado__c = false;
            objCobroParcial2.BI_COL_Renovable__c = false;
            objCobroParcial2.BI_COL_Fecha_Inicial__c = System.today();
            objCobroParcial2.BI_COL_Fecha_final__c = System.today();
            objCobroParcial2.BI_COL_Fecha_envio__c = System.today();
            objCobroParcial2.BI_COL_Valor_Cobro__c = 1300000;
            lstCobroParcial.add( objCobroParcial2 ); 
            
            insert lstCobroParcial;
                    
       // }
    }
    
    static testMethod void myUnitTest() 
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            createData();
            Test.startTest();
                BI_COL_NoveltyBilling_cls.ProcesarServCorporativos_ws( lstModServCreate, true );
                BI_COL_NoveltyBilling_cls.RegistrarCobrosParciales_ws( lstCobroParcial, lstCobroParcial, true );
            Test.stopTest();
        }
    }
}