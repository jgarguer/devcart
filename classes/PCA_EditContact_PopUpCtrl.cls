public without sharing class PCA_EditContact_PopUpCtrl extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for manage data of pop up that allow to edit a contact in Portal Platino 
    
    History:
    
    <Date>            <Author>          	<Description>
    11/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public Id ContactId{get;set;}
	public list<Contact> ContactActual {get;set;}
	public String Mode{get;set;}
	public list<BI_Contact_Customer_Portal__c> attorney {get;set;}
	public boolean isAttorney {get;set;}
	public boolean haveError	{get;set;}
	
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			PageReference page = enviarALoginComm();
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception e){
		   BI_LogHelper.generateLog('PCA_EditContact_PopUpCtrl.checkPermissions', 'Portal Platino', e.getMessage(), 'Class');
		   return null;
		}
	}

	public PCA_EditContact_PopUpCtrl(){}

	public void loadInfo (){
		
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
		
		ContactId = (System.currentPageReference().getParameters().get('id')!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get('id')):null;
		Mode = (System.currentPageReference().getParameters().get('Mode')!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get('Mode')):null;
		ContactActual = [Select Name, Email, Phone, MobilePhone, BI_Activo__c, Title FROM Contact Where Id =: ContactId];
		attorney = [SELECT BI_Activo__c,BI_Perfil__c,BI_User__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: BI_AccountHelper.getCurrentAccountId()];
		system.debug('*****Apoderado' +attorney);
		isAttorney=false;
		
		if(!attorney.IsEmpty()){
			
				if(attorney[0].BI_Perfil__c==Label.BI_Apoderado){
					isAttorney = true;		
				}			
			
		}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_EditContact_PopUpCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
		}
	}

	public void save(){
			//if(BI_TestUtils.isRunningTest()){
				//throw new BI_Exception('test');
			//}
			haveError=false;
			try{
				if(contactActual[0].Email!='' && contactActual[0].Email!=null){
					update ContactActual[0];
				}
				else{
					apexpages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.BI_EmailObligatorio));
					haveError = true;	
				}
			}catch(Exception Exc){
				 
				 Apexpages.Message msg = new Apexpages.Message(Apexpages.Severity.Fatal, Exc.getMessage());
			 	 Apexpages.addMessage(msg);      
				 BI_LogHelper.generate_BILog('PCA_EditContact_PopUpCtrl.save', 'Portal Platino', Exc, 'Class');
			}
			
			
	}


}