global class BI_Recarterizacion_Acc_Batch implements Database.Batchable<sObject> {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Micah Burgos
	    Company:       Aborda
	    Description:   Batch class that generate a csv doc with the cases of the 'Recarterización'
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Micah Burgos             	Initial version
	    25/02/2016						Guillermo Muñoz				End of the develop
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	//String query = 'SELECT BI_AccountId__c,BI_Borrar_Equipo_de_Casos__c,BI_Borrar_Equipo_de_Cuenta__c,BI_Borrar_Equipo_de_Oportunidad__c,BI_Casos_abiertos__c,BI_Casos_cerrados__c,BI_ChildrenAccount__c,BI_Closed_Agrupacion_Opp__c,BI_Closed_Opp__c,BI_NewOwner__c,BI_OldOwner__c,BI_Open_Agrupacion_Opp__c,BI_Open_Opp__c,BI_Status__c,Id,Name FROM BI_Recarterizacion__c WHERE BI_Processed__c = false';
	String query;
	Set<Id> set_all_acc;

	final Id RECARTERIZACION_ID;
	final Id DOC_ID;

	
	global BI_Recarterizacion_Acc_Batch(Id rec_param) {

		RECARTERIZACION_ID = rec_param;

		//SEARCH ACCOUNT FROM BI_Linea_de_Recarterizacion__c
		List<BI_Linea_de_Recarterizacion__c> lst_rec =  [SELECT Id,BI_AccountId__c, BI_Recarterizacion__c, BI_Cases_RecordTypes__c, 
														BI_Casos_abiertos__c, BI_Casos_cerrados__c, BI_Open_Opp__c 
														FROM BI_Linea_de_Recarterizacion__c WHERE BI_Recarterizacion__c = :RECARTERIZACION_ID];

		set_all_acc = new Set<Id>();

		for(BI_Linea_de_Recarterizacion__c rec :lst_rec){
			set_all_acc.add(rec.BI_AccountId__c);
		}

		Attachment doc = new Attachment();
		doc.Name = 'Borrar equipo de cuentas ['+String.valueOf(System.now())+'].csv';
		doc.ParentId = rec_param; 


		//Header CSV
		String csvDocHeader = 'AccountTeamMemberId,UserId,AccountId,TeamMemberRole,AccountAccessLevel,Propietario,RecarterizacionId'+'\n';
		doc.Body  = Blob.valueOf(csvDocHeader);  
		insert doc;

		DOC_ID = doc.Id;

		query = 'SELECT ID, OwnerId, (SELECT Id, UserId, AccountId, TeamMemberRole, AccountAccessLevel FROM AccountTeamMembers) FROM Account WHERE ID IN :set_all_acc';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Account> lst_acc = scope;

		List<Attachment> lst_doc = [SELECT ID, Body FROM Attachment WHERE Id = :DOC_ID];

		//Crear mapa de recarterizaciones 
		List<BI_Linea_de_Recarterizacion__c> lst_rec2 = [SELECT Id, BI_AccountId__c,BI_Borrar_Equipo_de_Cuenta__c,BI_NewOwner__c,BI_OldOwner__c,BI_Status__c 
   													FROM BI_Linea_de_Recarterizacion__c WHERE BI_Recarterizacion__c = :RECARTERIZACION_ID AND BI_AccountId__c IN :lst_acc];
   		Map<String, BI_Linea_de_Recarterizacion__c> map_rec = new Map<String, BI_Linea_de_Recarterizacion__c>();

   		for(BI_Linea_de_Recarterizacion__c rec2 : lst_rec2){
			map_rec.put(rec2.BI_AccountId__c, rec2);
   		}


   		//Estas listas permiten que se asocie lo que se va a actualizar con el registro de 
   		//recarterización para asociar el log de recarterización en caso de que fallara algo.
   		Map<String,String>  map_AccID_DEL_ACTMB = new Map<String,String>();
   		Map<String,Account> map_acc_to_UPD = new Map<String,Account>();
   		List<BI_Log_de_Recarterizacion__c> lst_log_insert = new List<BI_Log_de_Recarterizacion__c>();

   		//Listas aux
   		Set<Id> set_acc_to_delete_ACTMB = new Set<Id>();
   		Map<Id,Id> map_acc_recart = new Map<Id,Id>();

   		String csvDoc = lst_doc[0].Body.toString();

   		//Recorrer clientes recarterizandolos
   		for(Account acc :lst_acc){
   			
			//Actualizar registro Recarterización 
			map_rec.get(acc.Id).BI_OldOwner__c = acc.OwnerId;
			map_rec.get(acc.Id).BI_accounts_updated__c = true;
			map_acc_recart.put(acc.Id, map_rec.get(acc.Id).Id);


   			//Actualizar registro Acc
			acc.OwnerId = map_rec.get(acc.Id).BI_NewOwner__c;
			//AccountTeamMemberId,UserId,AccountId,TeamMemberRole,AccountAccesLevel,Propietario,RecarterizacionId
			System.debug('#####' + acc.AccountTeamMembers.size());
			if(map_rec.get(acc.Id).BI_Borrar_Equipo_de_Cuenta__c){
				if(!acc.AccountTeamMembers.isEmpty()){
					for(AccountTeamMember atm : acc.AccountTeamMembers){
						if(atm.UserId == acc.OwnerId){
							csvDoc += atm.Id + ',' + atm.UserId + ',' + atm.AccountId + ',' + atm.TeamMemberRole + ',' + atm.AccountAccessLevel + ',true,' + RECARTERIZACION_ID + '\n';
						}
						else{
							csvDoc += atm.Id + ',' + atm.UserId + ',' + atm.AccountId + ',' + atm.TeamMemberRole + ',' + atm.AccountAccessLevel + ',,' + RECARTERIZACION_ID + '\n';
						}
					}
				}
			}
   		}
   		lst_doc[0].Body = Blob.valueOf(csvDoc); 

		update lst_doc;

   //		//Eliminar miembros de los equipos.
   //		List<AccountTeamMember> lst_actm = [SELECT Id, AccountId FROM AccountTeamMember WHERE AccountId IN :set_acc_to_delete_ACTMB ];
   //		Map<Id,Id> map_accTmb_recart = new Map<Id,Id>(); //Mapa que asocia el Id del miembro de equipo con su recarterización.
   //		for(AccountTeamMember accTMB :lst_actm){
			//map_accTmb_recart.put(accTMB.Id, map_rec.get(accTMB.AccountId).Id);
   //		}

   //		//custom_DML(map_accTmb_recart,lst_actm,'DELETE');
   //		//try{
   //			//delete lst_actm;
			//List<Database.DeleteResult> drList = Database.delete(lst_actm, false);
   //			// Iterate through each returned result
   //         for (Database.DeleteResult dr : drList) {
   //             if (dr.isSuccess()) {
   //                 // Operation was successful, so get the ID of the record that was processed
   //                 System.debug('Successfully updated case with ID: ' + dr.getId());
                     
   //             }else{
   //             	//return_OK = false;
   //                 // Operation failed, so get all errors            
   //                     for(Database.Error err : dr.getErrors()) {

   //                         BI_Log_de_Recarterizacion__c log = new BI_Log_de_Recarterizacion__c(
   //                                     BI_Id_Objeto_Error__c = dr.getId(),
   //                                     BI_Error__c = '['+err.getStatusCode() + ']: ' + err.getMessage() + ' - Fields that affected this error: ' + err.getFields(),
   //                                     BI_Linea_de_Recarterizacion__c = map_accTmb_recart.get(dr.getId())
   //                                     );

   //                         lst_log_insert.add(log);
                                           
   //                     }
   //             }
   //         }

   		//Update Accs
   		//custom_DML(map_acc_recart,lst_acc,'UPDATE');
   		Map <Integer,Bi_bypass__c> bypass = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
   		
   		List<Database.SaveResult> srList = Database.update(lst_acc, false); 

   		BI_MigrationHelper.disableBypass(bypass);
   		
   		

		Set<Id> set_errors = new Set<Id> ();

   		 for (Database.SaveResult sr : srList) {
	        if (sr.isSuccess()) {
	            // Operation was successful, so get the ID of the record that was processed
	            System.debug('Successfully updated case with ID: ' + sr.getId());
	             
	        }else{
	        	//return_OK = false;
	            // Operation failed, so get all errors   
	            set_errors.add(sr.getId());   

                for(Database.Error err : sr.getErrors()) {

                    BI_Log_de_Recarterizacion__c log = new BI_Log_de_Recarterizacion__c(
                                BI_Id_Objeto_Error__c = sr.getId(),
                                BI_Error__c = '['+err.getStatusCode() + ']: ' + err.getMessage() + ' - Fields that affected this error: ' + err.getFields(),
                                BI_Recarterizacion__c = RECARTERIZACION_ID
                                );

                    lst_log_insert.add(log);
                                   
                }
	        }
	    }
   		//update lst_acc;

   		//Actualizar recarterizaciones
   		for(Id rec : map_rec.keySet()){
   			if(set_errors.contains(map_rec.get(rec).BI_AccountId__c) ){

   			}
   		}
   		update map_rec.values();
		//update map_rec.values();


		//Insertar Logs
   		insert lst_log_insert;
   		


/*		List<Account> lst_acc = scope;
   		Map<Id, Account> map_acc = new Map<Id, Account>(lst_acc); //DataBase.query(query);
   		List<BI_Recarterizacion__c> lst_rec2 = [SELECT Id, BI_AccountId__c,BI_Borrar_Equipo_de_Cuenta__c,BI_NewOwner__c,BI_OldOwner__c,BI_Status__c 
   													FROM BI_Recarterizacion__c WHERE BI_Processed__c = false AND BI_AccountId__c IN :map_acc.keySet()];

   		//Change Owner.
   		
   		for(BI_Recarterizacion__c rec2 :lst_rec2){

   			rec2.BI_OldOwner__c = map_acc.get(rec2.BI_AccountId__c).OwnerId;
			map_acc.get(rec2.BI_AccountId__c).OwnerId = rec2.BI_NewOwner__c;

			if(rec2.BI_Borrar_Equipo_de_Cuenta__c){
				set_acc_to_delete_ACTMB.add(rec2.BI_AccountId__c);
			}
   		}
   		//Update Accs
   		update map_acc.values();

   		//Actualizar recarterizaciones
   		update lst_rec2;*/
	}
	
	global void finish(Database.BatchableContext BC) {

		BI_Recarterizacion__c recarterizacion = new BI_Recarterizacion__c(
			Id = RECARTERIZACION_ID,
			BI_End_batch_acc__c = true
		);

		update recarterizacion;

/*
		        BI_Recarterizacion_Opp_Batch  recart_Batch = new BI_Recarterizacion_Opp_Batch();
        Id insBatchId = Database.executeBatch(recart_Batch, 200);
        system.debug(insBatchId);*/
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos  
	Company:       Aborda.es
	Description:   Method that check if DML operation has been sucesfully.

	IN: lst_obj - SObject to insert/update/delete
		option  - 'INSERT', 'UPDATE', 'DELETE'

	History: 

	<Date>                  <Author>                <Change Description>
	15/07/2014              Micah Burgos          	Initial Version     
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*	private static Boolean custom_DML(Map<Id,Id> map_Id_Recart,List<sObject> lst_obj, String option) {
				Boolean return_OK = true;

				//DO DML OPERATION
                List<Database.SaveResult> srList = new List<Database.SaveResult>();
                
                if(option.toUpperCase() == 'INSERT'){
					srList = Database.insert(lst_obj, false);
                }else if(option.toUpperCase() == 'UPDATE'){
                	srList = Database.update(lst_obj, false);
                }else if(option.toUpperCase() == 'DELETE'){
               		drList = Database.delete(lst_obj, false);
          		}

                List<BI_Log_de_Recarterizacion__c> lst_err = new List<BI_Log_de_Recarterizacion__c>();
                
                if(option.toUpperCase() != 'DELETE'){
	                // Iterate through each returned result
	                for (Database.SaveResult sr : srList) {
	                    if (sr.isSuccess()) {
	                        // Operation was successful, so get the ID of the record that was processed
	                        System.debug('Successfully updated case with ID: ' + sr.getId());
	                         
	                    }else{
	                    	return_OK = false;
	                        // Operation failed, so get all errors            
		                        for(Database.Error err : sr.getErrors()) {

		                            BI_Log_de_Recarterizacion__c log = new BI_Log_de_Recarterizacion__c(
		                                        BI_Id_Objeto_Error__c = sr.getId(),
		                                        BI_Error__c = '['+err.getStatusCode() + ']: ' + err.getMessage() + ' - Fields that affected this error: ' + err.getFields(),
		                                        BI_Recarterizacion__c = (map_Id_Recart != null)?map_Id_Recart.get(sr.getId()):sr.getId());
		                                           
		                        }
	                    }
	                }
            	}else if(option.toUpperCase() == 'DELETE'){
	                // Iterate through each returned result
	                for (Database.DeleteResult dr : drList) {
	                    if (dr.isSuccess()) {
	                        // Operation was successful, so get the ID of the record that was processed
	                        System.debug('Successfully updated case with ID: ' + dr.getId());
	                         
	                    }else{
	                    	return_OK = false;
	                        // Operation failed, so get all errors            
		                        for(Database.Error err : dr.getErrors()) {

		                            BI_Log_de_Recarterizacion__c log = new BI_Log_de_Recarterizacion__c(
		                                        BI_Id_Objeto_Error__c = dr.getId(),
		                                        BI_Error__c = '['+err.getStatusCode() + ']: ' + err.getMessage() + ' - Fields that affected this error: ' + err.getFields(),
		                                        BI_Recarterizacion__c = (map_Id_Recart != null)?map_Id_Recart.get(dr.getId()):dr.getId());
		                                           
		                        }
	                    }
	                }
	            }

                return return_OK;
		
		}*/
	
}