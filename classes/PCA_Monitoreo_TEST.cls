@isTest
public class PCA_Monitoreo_TEST {
    public static Boolean TGSUSer = false;
    @testSetup static void methodSetup() {
        BI_Service_Tracking_URL_PP__c serv1 = new BI_Service_Tracking_URL_PP__c();
        serv1.BI_URL__c='www.prueba.es';
        serv1.CWP_App_Name__c='SalesForce';
        serv1.CWP_Description__c='TestDescription';
        serv1.CWP_TGS_Users__c=true;
        serv1.BI_Section__c='Empresas';
        serv1.CWP_Logo__c='http://logo';
        insert serv1;
        
        BI_Service_Tracking_URL_PP__c serv2 = new BI_Service_Tracking_URL_PP__c();
        serv2.BI_URL__c='www.prueba.es';
        serv2.CWP_App_Name__c='SalesForce';
        serv2.CWP_Description__c='TestDescription';
        serv2.CWP_TGS_Users__c=true;
        serv2.BI_Section__c='Empresas';
        serv2.CWP_Logo__c='http://logo';
        serv2.CWP_isPublic__c = true;
        insert serv2;
        
        BI_Service_Tracking_URL_PP__c serv3 = new BI_Service_Tracking_URL_PP__c();
        serv3.BI_URL__c='www.prueba.es';
        serv3.CWP_App_Name__c='Consolidated View';
        serv3.CWP_Description__c='TestDescription';
        serv3.CWP_TGS_Users__c=true;
        serv3.BI_Section__c='Empresas';
        serv3.CWP_Logo__c='http://logo';
        serv3.CWP_isPublic__c = true;
        insert serv3;
        
        BI_Service_Tracking_URL_PP__c serv4 = new BI_Service_Tracking_URL_PP__c();
        serv4.BI_URL__c='www.prueba.es';
        serv4.CWP_App_Name__c='Top Performance';
        serv4.CWP_Description__c='TestDescription';
        serv4.CWP_TGS_Users__c=true;
        serv4.BI_Section__c='Empresas';
        serv4.CWP_Logo__c='http://logo';
        serv4.CWP_isPublic__c = true;
        insert serv4;
        
        BI_Service_Tracking_URL_PP__c serv5 = new BI_Service_Tracking_URL_PP__c();
        serv5.BI_URL__c='www.prueba.es';
        serv5.CWP_App_Name__c='Network Quality';
        serv5.CWP_Description__c='TestDescription';
        serv5.CWP_TGS_Users__c=true;
        serv5.BI_Section__c='Empresas';
        serv5.CWP_Logo__c='http://logo';
        serv5.CWP_isPublic__c = true;
        insert serv5;
        
        BI_Service_Tracking_URL_PP__c serv6 = new BI_Service_Tracking_URL_PP__c();
        serv6.BI_URL__c='www.prueba.es';
        serv6.CWP_App_Name__c='Traffic Management';
        serv6.CWP_Description__c='TestDescription';
        serv6.CWP_TGS_Users__c=true;
        serv6.BI_Section__c='Empresas';
        serv6.CWP_Logo__c='http://logo';
        serv6.CWP_isPublic__c = true;
        insert serv6;
	}

    static testMethod void PCA_Monitoreo1() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        TGSUser = true;
        
       User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        usr.FederationIdentifier='mnc.test@cspad-premad.wh.test';
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Negocios';
        insert seg1;
        system.debug('*$*$*$: '+seg1.Id);*/
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Negocios';
            ima1.BI_Pestana__c = 'Monitoreo';
        insert ima1;
        }
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'Negocios';
                item.BI_Subsegment_Local__c = null;
                accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());   
            user1.FederationIdentifier='mnc.test@cspad-premad.wh.test';
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId());
                insert ccp;
                Test.stopTest();
                
                //BI_ImageHome__c ihome = new BI_ImageHome__c(BI_Description__c='desc',BI_Fecha_Fin__c=System.today()-1, BI_Fecha_Inicio__c=System.today()+1,BI_Url__c='https://www.google.es/images/srpr/logo11w.png',BI_Country__c=);
                
                PageReference pageRef = new PageReference('PCA_MarketPlace');
                 Test.setCurrentPage(pageRef);

                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_Monitoreo();
                tmpContr.checkPermissions();
                
                BI_ImageHome__c biImage = new BI_ImageHome__c();
                biImage.BI_Pestana__c = 'Monitoreo';
                Id generalId = [SELECT Id FROM RecordType WHERE SobjectType = 'BI_ImageHome__c' AND name='General'].Id;
                biImage.RecordTypeId = generalId;  
                biImage.BI_Segment__c='Empresas';
                insert biImage;
                //public Map <BI_Service_Tracking_URL_PP__c,String> Description {get;set;}
    			//public Map <BI_Service_Tracking_URL_PP__c,String> AppName {get;set;}
    			tmpContr.ImgBack = biImage;
                tmpContr.RelationImageMovi = biImage;
                tmpContr.RelationImageTel = biImage;
    			//public static BI_ImageHome__c RelationImageMovi {get;set;}
    			//public static BI_ImageHome__c RelationImageTel {get;set;}

    			tmpContr.AttImg = attach;
                //public static Attachment AttImg {get;set;}
    			//public static Map <String,BI_Service_Tracking_URL_PP__c> appli {get;set;}
    
            }
        }
    }
    static testMethod void PCA_Monitoreo2() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Empresas';
            ima1.BI_Pestana__c = 'Monitoreo';
        insert ima1;
        }
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'Empresas';
                item.BI_Subsegment_Local__c = null;
                accList.add(item);
            }
           update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());  
            //user1.FederationIdentifier='mnc.cdcop01@cspad-premad.wh.telefonica';
            update user1;
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId());
                insert ccp;
                Test.stopTest();
                
                PageReference pageRef = new PageReference('PCA_Monitoreo');
                 Test.setCurrentPage(pageRef);

                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_Monitoreo();
                tmpContr.checkPermissions();
    
            }
        }
    }
    static testMethod void PCA_MarketPlace3() {
       /** TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
       /* BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Empresas';
            ima1.BI_Pestana__c = 'Monitoreo';
        insert ima1;
        }
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){ 
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                         
           Attachment attach=new Attachment(Name='Unit Test Attachment', 
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId());
                insert ccp;
                Test.stopTest();
                
                PageReference pageRef = new PageReference('PCA_Monitoreo');
                Test.setCurrentPage(pageRef); 

                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_Monitoreo();
                tmpContr.checkPermissions();
    
            }
        }
     
    }
    
    static testMethod void PCA_Monitoreo4() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
       User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id;
        }
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Negocios';
            ima1.BI_Pestana__c = 'Monitoreo';
            insert ima1;
        
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = null;
                item.BI_Subsegment_Local__c = null;
                accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob, parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId());
                insert ccp;
                Test.stopTest();
                
                PageReference pageRef = new PageReference('PCA_MarketPlace');
                 Test.setCurrentPage(pageRef);

                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                
                if(tmpContr.RelationImage == null) {}
                
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_Monitoreo();
                tmpContr.checkPermissions();
    			
            }
        }
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
    @isTest static void PCA_Monitoreo5(){
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Empresas';
            ima1.BI_Pestana__c = 'Monitoreo';
        insert ima1;
        }
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'Empresas';
                item.BI_Subsegment_Local__c = null;
                item.BI_Country__c = 'Peru';
                accList.add(item);
            }
           update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;

            List <BI_Service_Tracking_URL_PP__c> lst_stu = new List <BI_Service_Tracking_URL_PP__c>();

            BI_Service_Tracking_URL_PP__c stu1 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM1',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Consolidated View',
                BI_URL__c = 'TestUrl'
                
            );
            lst_stu.add(stu1);

            BI_Service_Tracking_URL_PP__c stu2 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM2',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Top Performance',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu2);

            BI_Service_Tracking_URL_PP__c stu3 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM3',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Network Quality',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu3);

            BI_Service_Tracking_URL_PP__c stu4 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM4',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Traffic Management',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu4);
            insert lst_stu;

            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId(),BI_SW_User__c = 'TestUser', BI_SW_Pwd__c = 'TestPass');
                insert ccp;
                Test.stopTest();
                
                PageReference pageRef = new PageReference('PCA_Monitoreo');
                 Test.setCurrentPage(pageRef);
                    
                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                String url = tmpContr.getUrlConsolidatedView();
                url = tmpContr.getUrltopPerformance();
                url = tmpContr.getUrlnetworkQuality();
                url = tmpContr.getUrltrafficManagement();

                BI_TestUtils.throw_exception = true;
                tmpContr.getUrlConsolidatedView();
                tmpContr.getUrltopPerformance();
                tmpContr.getUrlnetworkQuality();
                tmpContr.getUrltrafficManagement();
                BI_TestUtils.throw_exception = false;
                TGSUSer = true;
                List<BI_Service_Tracking_URL_PP__c> testGetAplis = PCA_Monitoreo.getAplis();
                

            }
        }
    }
    @isTest static void PCA_Monitoreo6(){
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Empresas';
            ima1.BI_Pestana__c = 'Monitoreo';
        insert ima1;
        }
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'Empresas';
                item.BI_Subsegment_Local__c = null;
                item.BI_Country__c = 'Peru';
                accList.add(item);
            }
           update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;

            List <BI_Service_Tracking_URL_PP__c> lst_stu = new List <BI_Service_Tracking_URL_PP__c>();

            BI_Service_Tracking_URL_PP__c stu1 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM1',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Consolidated View',
                BI_URL__c = 'TestUrl'
                
            );
            lst_stu.add(stu1);

            BI_Service_Tracking_URL_PP__c stu2 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM2',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Top Performance',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu2);

            BI_Service_Tracking_URL_PP__c stu3 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM3',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Network Quality',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu3);

            BI_Service_Tracking_URL_PP__c stu4 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'BSM4',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Traffic Management',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu4);
            insert lst_stu;

            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            //Usuario TGS
            Id perfil= [select id, name from Profile where Name LIKE '%TGS%' limit 1].Id;
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, perfil);
            user1.Country='Peru';
            user1.FederationIdentifier='mnc.test@cspad-premad.wh.test';
            system.debug('Federation: id:'+user1.Id);
          //  update user1;
            
            //Custom Setting
            TGS_User_Org__c setting = new TGS_User_Org__c();
			setting.TGS_Is_TGS__c = True;
			setting.SetupOwnerId = perfil;
            setting.TGS_Is_BI_EN__c=False;
            setting.Name= 'pruebaCustomSetting';
			insert setting;
            
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId(),BI_SW_User__c = 'TestUser', BI_SW_Pwd__c = 'TestPass');
                insert ccp;
                Test.stopTest();
                
                PageReference pageRef = new PageReference('PCA_Monitoreo');
                 Test.setCurrentPage(pageRef);

                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                tmpContr.tokenApex='prueba token';
                Map<String, BI_Service_Tracking_URL_PP__c> mapaAux= new Map<String, BI_Service_Tracking_URL_PP__c>();
                tmpContr.setmapa(mapaAux);
                mapaAux=tmpContr.getmapa();

                String url = tmpContr.getUrlConsolidatedView();
                url = tmpContr.getUrltopPerformance();
                url = tmpContr.getUrlnetworkQuality();
                url = tmpContr.getUrltrafficManagement();

                BI_TestUtils.throw_exception = true;
                tmpContr.getUrlConsolidatedView();
                tmpContr.getUrltopPerformance();
                tmpContr.getUrlnetworkQuality();
                tmpContr.getUrltrafficManagement();
                BI_TestUtils.throw_exception = false;
                
                List<BI_Service_Tracking_URL_PP__c> testGetAplis = PCA_Monitoreo.getAplis();
            }
        }
    }

    @isTest static void PCA_Monitoreo7(){
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //usr.BI_Country__c = 'Peru';
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Empresas';
            ima1.BI_Pestana__c = 'Monitoreo';
        insert ima1;
        }
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'Empresas';
                item.BI_Subsegment_Local__c = null;
                item.BI_Country__c = 'Peru';
                accList.add(item);
            }
           update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;

            List <BI_Service_Tracking_URL_PP__c> lst_stu = new List <BI_Service_Tracking_URL_PP__c>();

            BI_Service_Tracking_URL_PP__c stu1 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'Consolidated View',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Consolidated View',
                BI_URL__c = 'TestUrl'
                
            );
            lst_stu.add(stu1);

            BI_Service_Tracking_URL_PP__c stu2 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'Top Performance',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Top Performance',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu2);

            BI_Service_Tracking_URL_PP__c stu3 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'Network Quality',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Network Quality',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu3);

            BI_Service_Tracking_URL_PP__c stu4 = new BI_Service_Tracking_URL_PP__c(
                CWP_App_Name__c = 'Traffic Management',
                BI_Country__c = 'Peru',
                BI_Section__c = 'Traffic Management',
                BI_URL__c = 'TestUrl'
            );
            lst_stu.add(stu4);
            insert lst_stu;

            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                Test.startTest();
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=BI_AccountHelper.getCurrentAccountId(),BI_SW_User__c = 'TestUser', BI_SW_Pwd__c = 'TestPass');
                insert ccp;
                
                system.debug('vista user1:'+user1);
                system.debug('vista customer:'+ccp);
                
                PageReference pageRef = new PageReference('PCA_Monitoreo');
                 Test.setCurrentPage(pageRef);
                    
                PCA_Monitoreo tmpContr = new PCA_Monitoreo();
                String url = tmpContr.getUrlConsolidatedView();
                url = tmpContr.getUrltopPerformance();
                url = tmpContr.getUrlnetworkQuality();
                url = tmpContr.getUrltrafficManagement();

                BI_TestUtils.throw_exception = true; 
                tmpContr.getUrlConsolidatedView();
                tmpContr.getUrltopPerformance();
                tmpContr.getUrlnetworkQuality();
                tmpContr.getUrltrafficManagement();
                BI_TestUtils.throw_exception = false;
                
                List<BI_Service_Tracking_URL_PP__c> testGetAplis = PCA_Monitoreo.getAplis();
                Test.stopTest();
                
				 String[] dt = new String[]{'prueba'};
                Cache.Session.put('local.CWPexcelExport.AppNames', dt, 3600, Cache.Visibility.ALL, true);
                tmpContr.checkPermissions();
            }
        }
    }
}