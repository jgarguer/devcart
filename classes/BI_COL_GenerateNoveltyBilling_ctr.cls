/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:		Carlos Carvajal
	Company:	   Salesforce.com
	Description:   
	
	History:
	
	<Date>			<Author>		  <Description>
	27/03/2015		Carlos Carvajal	Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
public class BI_COL_GenerateNoveltyBilling_ctr 
{
	/**Atributos*/
	public boolean mostrarRegresar {get;set;}
	public List<WrapperMS> lstMS {get;set;}
	public boolean todosMarcados {get;set;}
	public boolean mostrarValidaciones{get; set;}
	public String Busqueda{get; set;}
	public Map<String,Integer> tempMap{get; set;}
	public String strIdCliente {get; set;}
	private Account cliente{get;set;}
	private List<Id> lstIdClientes{get;set;}
	public Map<Id,String> mapaRepLegal{get; set;}
	public User usuario{get;set;}
	public static String tramaDav{get;set;}
	public static List<BI_COL_Modificacion_de_Servicio__c> lstMSProcesar = new List<BI_COL_Modificacion_de_Servicio__c>();
	public static List<String>listaNF = new List<String>();
	public List<String> listaMS=new List<String>();
	public List<BI_Log__c> scope{get;set;}
	public List<String> idMsSeleccionada{get;set;}
	public String armadoMS{get;set;}
	
	public String msgEnviado{get;set;}
	public String msgNOEnviado{get;set;}
	
	/**Parametros del paginador*/
	private Integer pageNumber;
	private Integer totalPageNumber;
	private Integer pageSize;
	private List<WrapperMS> pageMS;

	public BI_COL_GenerateNoveltyBilling_ctr() 
	{
		strIdCliente='';
		pageNumber=1;
		lstIdClientes=new List<Id>();
		List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = new List<BI_COL_Modificacion_de_Servicio__c>();		
		usuario=[Select Id, CompanyName from User where id=: userinfo.getuserid()];
		
		if(usuario!=null && usuario.CompanyName!=null)
		{
			lstMSConsulta = getMSSolicitudServicio(usuario.Id); 
			//Buscar(lstMSConsulta);	
		}
		else
		{
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No es posible realizar la consulta debido a que el usuario no cuenta con un segmento'));
		}
	}
	public List<SelectOption> listaClientes
	{
	 	get
	 	{ 
	 		 List<SelectOption> lst= new List<SelectOption>();
             List<Account > lstClientes=[SELECT  Id, Name FROM Account where BI_Country__c='Colombia' limit 900];
             system.debug('Cantidad de clientes encontrados====>'+lstClientes);
             if(lstClientes.size()>0){
                lst.add( new SelectOption('', '------------' ) );                
                for(Account ac: lstClientes)
                    lst.add( new SelectOption( ac.Id, ac.Name ) );
			}
          return lst;
	 	}
	 	set;
	 }

	public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String strIdCliente)
	{	
		String consultaSOQL = 'SELECT ' +
				'm.Id, ' +
				'm.Name, ' +
				'm.BI_COL_Autorizacion_facturacion__c,' +
				'm.BI_COL_Oportunidad__c, ' +
				'm.BI_COL_Oportunidad__r.Account.id,' +
				//'m.Producto_Telefonica__r.Linea__c, ' +
				//'m.Producto_Telefonica__r.Name, ' + 
				'm.BI_COL_Producto__r.NE__ProdId__r.Name, ' +
				'm.BI_COL_Producto__r.NE__ProdId__r.Familia_MKTG_Nivel_1__c, '+
				'm.BI_COL_Duracion_meses__c, ' +
				'm.BI_COL_Codigo_unico_servicio__r.BI_COL_Codigo_paquete__c, '+
				'm.BI_COL_Codigo_unico_servicio__r.Name, '+
				'm.BI_COL_Estado__c, ' +
				'm.BI_COL_Clasificacion_Servicio__c,'+
				'm.BI_COL_Fecha_inicio_de_cobro_RFB__c, '+
				'm.BI_COL_Fecha_fin__c, '+
				'm.BI_COL_Estado_orden_trabajo__c, '+
				'm.BI_COL_RepresentanteLegal__c, '+
				'm.BI_COL_Fecha_comprometida_baja_tecnica__c '+
			' FROM '+ 
				'BI_COL_Modificacion_de_Servicio__c m '+ 
			'WHERE ';
		consultaSOQL+=' BI_COL_Autorizacion_facturacion__c=false';
		consultaSOQL+=' and (BI_COL_Clasificacion_Servicio__c=\'BAJA\' OR (BI_COL_Estado__c=\'Activa\' and BI_COL_Estado_orden_trabajo__c = \'Ejecutado\') )';
		consultaSOQL+=' and 	BI_COL_Fecha_liberacion_OT__c<>null';
		//consultaSOQL+=' and 	BI_COL_FUN__c<>null';
		
		/*if(strIdCliente!=null && !strIdCliente.equals(''))
		{
			system.debug('-- Id de cliente que se desea filtrar--->'+strIdCliente);
			consultaSOQL+=' and BI_COL_Oportunidad__r.Account.id =\''+strIdCliente+'\'';
		}*/
		
		System.debug('consultaSOQL----->>>>'+consultaSOQL+ '---->Busqueda: '+Busqueda);
		
		if(Busqueda!=null && Busqueda!='' )	
		{
			String dsBuscar='';
			if(Busqueda.contains(','))
			{
				List<String> lstDSB=Busqueda.split(',');
				System.debug('lstDSB--->'+lstDSB+'-----<');
				for(String ls:lstDSB)
				{
					//if( dsBuscar!= null )
					//{
						System.debug('lstDSB---> linea 126'+lstDSB+'-----<');
						dsBuscar=dsBuscar.trim();
						dsBuscar+='\''+ls+'\',';
					//}
				}
				System.debug('dsBuscar--->'+dsBuscar+'-----<');
				dsBuscar=dsBuscar.subString(0,dsBuscar.length()-1);
			}
			else
			{
				dsBuscar=dsBuscar.trim();
				dsBuscar='\''+Busqueda+'\'';
			}
			consultaSOQL+=' AND Name IN ('+dsBuscar+')'; 
		}
		
		consultaSOQL+=' limit 1000';
		
		System.debug('----->>>>'+consultaSOQL);
		List<BI_COL_Modificacion_de_Servicio__c> lstMS=new List<BI_COL_Modificacion_de_Servicio__c>();
		System.debug('');
		lstMS=Database.query(consultaSOQL);
		
		System.debug('\n\n ******* Tamaño de La lista de MS de la consulta es: ' + lstMS.size());
		
		System.debug('\n\n ******* La lista de MS de la consulta es: ' + lstMS);
		
		return lstMS;
	}

	/**Clase Wrapper que administra la selección de registros*/
	public class WrapperMS
	{
		public boolean seleccionado {get;set;}
		public Integer diasServicio {get;set;}
		public Double valorMulta{get; set;}
		public BI_COL_Modificacion_de_Servicio__c ms {get;set;}
		
		public WrapperMS(BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado)
		{
			this.ms = ms;
			this.seleccionado = seleccionado;
		}
	}

	public void cambioCliente()
	{
		if(lstMS!=null) 
		{
			lstMS.clear();
		}
		List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = new List<BI_COL_Modificacion_de_Servicio__c>();
		lstMSConsulta= getMSSolicitudServicio(strIdCliente);
		Buscar(lstMSConsulta);
	}

	public void Buscar(List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta)
	{
							
		Integer cont=0;
		tempMap=new Map<String,Integer>();
		System.debug('Ingresa al método con estos rgistros--->'+lstMSConsulta.size());
		if(lstMSConsulta != null && lstMSConsulta.size() > 0)
		{	
			System.debug('Bandera 1');
			//Llenar listado de MS Wrapper que se muestran en pantalla
			lstMS = new List<WrapperMS>();
			this.mostrarRegresar = true; 
			this.mostrarValidaciones = false; 
			for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
			{
				lstIdClientes.add(ms.BI_COL_Oportunidad__r.Account.id);
				WrapperMS wMS = new WrapperMS(ms, false);
				this.lstMS.add(wMS);
				System.debug('Agregando elemento===='+wMs);
			}
			buscarRepresentanteLegal();
			System.debug('\n\n this.mostrarValidaciones='+this.mostrarValidaciones+'\n\n');
			System.debug('La lista de Wrapper MS es: ' + lstMS);
				
				//Inicializar valores del paginador
			pageNumber = 1;
			totalPageNumber = 0;
			pageSize = 200;
			ViewData();
		}
		else
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No existe modificaciones de servicios activas asociadas'));
		}
	}

	public void buscarRepresentanteLegal()
	{
		List<Contact> LSconRepr=new List<Contact>();
		mapaRepLegal=new map<Id,String>();
		if(lstIdClientes.size()>0)
		{
			LSconRepr=[select Name,Account.ID from Contact WHERE BI_Representante_legal__c = true AND Account.ID IN:lstIdClientes];
			for(Contact contacto: LSconRepr)
			{				
				mapaRepLegal.put(contacto.Account.ID, contacto.Name);
			}
		}
	}

	public PageReference ViewData()
	{
		totalPageNumber = 0;
		BindData(1);
		return null;
	}

	/**IMPLEMENTACION PAGINADOR*/
	public Integer getPageNumber()
	{
		return pageNumber;
	}
	
	/**
	* Obtiene el numer de paginas de la tabla 
	* @return numero de paginas
	*/
	public Integer getTotalPageNumber()	
	{
		if (totalPageNumber == 0 && lstMS !=null)
		{
			totalPageNumber = lstMS.size() / pageSize;
			Integer mod = lstMS.size() - (totalPageNumber * pageSize);
			if (mod > 0)
			totalPageNumber++;
		}
		return totalPageNumber;
	}
	
	public PageReference nextBtnClick() 
	{
		BindData(pageNumber + 1);
		return null;
	}
	
	
	public PageReference previousBtnClick()
	{
		BindData(pageNumber - 1);
		return null;
	}
	
	public List<WrapperMS> getLstMS()
	{
		return pageMS;
	}
	
	/**
	* Posiciona el registro segun el numero de pagina
	* @param newPageIndex Indice de la pagina
	* @return 
	*/
	private void BindData(Integer newPageIndex)
	{
		try{
			pageMS = new List<WrapperMS>();
			Transient Integer counter = 0;
			Transient Integer min = 0;
			Transient Integer max = 0;
			
			if (newPageIndex > pageNumber){
				min = pageNumber * pageSize;
				max = newPageIndex * pageSize;
			}else{
				max = newPageIndex * pageSize;
				min = max - pageSize;
			}
			
			for(WrapperMS wMS : lstMS){
				counter++;
				if (counter > min && counter <= max){
					pageMS.add(wMS);
				}
					
			}
			pageNumber = newPageIndex;
			
			if (pageMS == null || pageMS.size() <= 0)
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
		}
		catch(Exception ex)
		{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
		}	
	}
	
	public Boolean getPreviousButtonEnabled()
	{
		return !(pageNumber > 1);
	}
	
	public Boolean getNextButtonDisabled()
	{
		if (lstMS == null){	return true;}
		else{return ((pageNumber * pageSize) >= lstMS.size());}
	
	}
	
	public Integer getPageSize()
	{
		return pageSize;
	}

	public void action_generarModServNovedad()
	{
		
		System.debug('------------->segundo método de envio-------');
		System.Debug('\n\n ListaNF: ' + listaMS+' ####### \n\n');
		scope= [SELECT ID, BI_COL_Informacion_Enviada__c,BI_COL_Informacion_recibida__c,BI_COL_Estado__c
									FROM BI_Log__c 
									where BI_COL_Modificacion_Servicio__c in:listaMS and BI_COL_Estado__c='Pendiente'];
		for(BI_Log__c nf:scope)
		{
			listaNF.add(nf.id);
			nf.BI_COL_Estado__c='Procesando';
		}
		update scope;
		tramaDav=armarXml('SERV. CORPORATIVO - NOVEDADES',scope);	
		envioTramaDavox(tramaDav,idMsSeleccionada,listaNF);
		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, armadoMS));
	}

	/**
	* Accion para generar MS de Baja
	* @PageReference 
	*/
	public PageReference action_generarNovedadFacturacion()
	{
		String msgEnviado='';
		String msgNOEnviado='';
		String repLegal='';
		idMsSeleccionada=new List<String>();
		if(lstMS.size()>0)
		{
			for(WrapperMS w:lstMS)
			{
				System.debug('La var w'+w);
				System.debug('La var w.seleccionado'+w.seleccionado);
				System.debug('La var w.ms'+w.ms);
				if (w != null && w.seleccionado!=null && w.seleccionado) 
				{
					lstMSProcesar.add(w.ms);
					idMsSeleccionada.add(w.ms.Id);
				}
			}	
		}
		
		listaNF=new List<String>();
		listaMS=new List<String>();
		if (lstMSProcesar.size()>0)
		{
			for(BI_COL_Modificacion_de_Servicio__c ms:lstMSProcesar)
			{
				ms.BI_COL_Autorizacion_facturacion__c=true;
				if(ms.BI_COL_Clasificacion_Servicio__c=='BAJA')
				{
					System.debug('\n\n ms.Fecha_solicitud_baja__c: '+ms.BI_COL_Fecha_comprometida_baja_tecnica__c+' ms.Name: '+ms.Name+'\n\n');
					ms.BI_COL_Fecha_fin__c=ms.BI_COL_Fecha_comprometida_baja_tecnica__c;
					listaMS.add(ms.id);
					msgEnviado+=ms.Name+', ';
				}
				else if(ms.BI_COL_Fecha_inicio_de_cobro_RFB__c!=null)
				{
					Integer intDuracion = ( ms.BI_COL_Duracion_meses__c != null ? Integer.valueOf( String.valueOf( ms.BI_COL_Duracion_meses__c ) ) : 0 );
					System.debug('\n\n ms.Fecha_inicio_cobro_RFB__c ' +ms.BI_COL_Fecha_inicio_de_cobro_RFB__c +'\n\n');
					Date  dtFechaFinal=Date.valueOf(ms.BI_COL_Fecha_inicio_de_cobro_RFB__c).addMonths(intDuracion).addDays(-1);
					System.debug('\n\n dtFechaFinal: '+dtFechaFinal+'\n\n');
					ms.BI_COL_Fecha_fin__c=dtFechaFinal;
					listaMS.add(ms.id);
					msgEnviado+=ms.Name+', ';
				}
				else
				{
					msgNOEnviado+=ms.Name+', ';
				}
				ms.BI_COL_Fecha_de_facturacion__c=System.today();
				//---------------
				if(mapaRepLegal.containsKey(ms.BI_COL_Oportunidad__r.Account.id))
				{
					repLegal=mapaRepLegal.get(ms.BI_COL_Oportunidad__r.Account.id);
					ms.BI_COL_RepresentanteLegal__c=repLegal;
				}
			}
			System.debug('\n\n listaMS.size(): '+listaMS.size()+'\n\n');
			if(listaMS.size()>0)
			{
				update lstMSProcesar;
				List<BI_Log__c> nFact=[Select n.BI_COL_Modificacion_Servicio__c, n.BI_COL_Estado__c, n.Id, n.BI_COL_Tipo_Transaccion__c 
												from BI_Log__c n 
												where n.BI_COL_Modificacion_Servicio__c in:listaMS and n.BI_COL_Estado__c <> 'Cerrado'];
				
				System.debug('List<Novedad_Fcaturacion_c> nFact = ' + nFact);
				for(BI_Log__c nf:nFact)
				{
					System.debug('Recorriendo la lista nFact');
					nf.BI_COL_Estado__c='Cerrado';
					System.debug('nf.Estado__c cambiado a cerrado');
				}
				System.debug('Llega a actualizar nFact');
				update nFact;
				System.debug('\n\n Despues de actualizar\n\n');		
						
				BI_COL_NoveltyBilling_cls.ProcesarServCorporativos_ws(lstMSProcesar,true);
			}
			
		}
		armadoMS='';
		if(msgEnviado.length()>1)
		{
			armadoMS='Se enviaron las Siguientes Modificaciones de servicio a Davox '+msgEnviado;
		}
		if(msgNOEnviado.length()>1)
		{
			armadoMS+='\nLa siguientes MS no tienen Fecha de Incio RFB y NO fueron enviadas '+ msgNOEnviado;
		}

		this.lstMS=null;
		return null;
	}

	/**
	* Acción para seleccionar todos los registros de MS
	* @return PageReference
	*/
	public PageReference action_seleccionarTodos()
	{
		system.debug('Cambiando valor Seleccionado');
		if(this.lstMS != null)
		{
			system.debug('Si hay valores en MS');
			for(WrapperMS wMS : this.lstMS)	
			{
				system.debug('Convirtiendo un valor');
				wMS.seleccionado = this.todosMarcados;
			}
		}
		return null;
	}

	public static String armarXml(String typeX,List<BI_Log__c> novedades)
	{
		System.debug('Entra a armarXml');
		XmlStreamWriter xmlWriter=new XmlStreamWriter();
		xmlWriter.writeStartDocument('utf-8','1.0');
		xmlWriter.writeStartElement(null, 'WS', null);

		xmlWriter.writeStartElement(null, 'USER', null);
		xmlWriter.writeCharacters('USER');
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'PASSWORD', null);
		xmlWriter.writeCharacters('PASSWORD');
		xmlWriter.writeEndElement();
		xmlWriter.writeStartElement(null, 'DATEPROCESS', null);
		xmlWriter.writeCharacters(String.valueOf(System.Today()));
		xmlWriter.writeEndElement();
		xmlWriter.writeStartElement(null, 'PACKCODE', null);
		xmlWriter.writeCharacters(Math.floor((Math.random()*10000)+1)+'-ASD654AS'+System.now());
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'SEPARATOR', null);
		xmlWriter.writeCharacters('Õ');
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'TYPE', null);
		xmlWriter.writeCharacters(typeX);
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'VALUES', null);

		for (BI_Log__c n : novedades)
		{
			xmlWriter.writeStartElement(null, 'VALUE', null);
			xmlWriter.writeCharacters(n.BI_COL_Informacion_Enviada__c);
			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'COUNT', null);
		xmlWriter.writeCharacters(String.valueOf(novedades.size()));
		xmlWriter.writeEndElement();

		xmlWriter.writeEndElement();

		string tramaDavox = xmlWriter.getXmlString();
		tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
		xmlWriter = new XmlStreamWriter();

		return tramaDavox;
	}

	@Future (callout=true)
	public static void envioTramaDavox(String tramaDavox,List<String> idMSAfectadas,List<String> listaNF2)
	{
		String strRespuestaServicio='';
		try
		{
			BI_COL_Davox_wsdl.BasicServiceWSSOAP ws = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
			ws.timeout_x=12000;
			System.Debug('\n\n EL XML ENVIADO AL SERVICIO WEB ES: ' + tramaDavox+' \n\n');
			
			strRespuestaServicio = ws.add(tramaDavox);
			System.Debug('\n\n ####### Respuesta del XML: ' + strRespuestaServicio+' ####### \n\n');
			System.Debug('\n\n ListaNF: ' + listaNF+' ####### \n\n');
			
			ActualizaNovedades(listaNF, strRespuestaServicio);
		}
		catch(Exception ex)
		{
			
			List<BI_Log__c> novedades=[Select BI_COL_Causal_Error__c,BI_COL_Estado__c,BI_COL_Informacion_recibida__c from BI_Log__c where id IN:listaNF2];
			for(BI_Log__c n : novedades)
			{
				n.BI_COL_Causal_Error__c ='Error enviando la Novedad a DAVOX: '+ex.getMessage();
				n.BI_COL_Estado__c='Error Conexion';
				n.BI_COL_Informacion_recibida__c=ex.getMessage();
			}
			update novedades;
			actualizarMsFallo(idMSAfectadas);
			System.Debug('Error del XML: ' + ex.getMessage());
		} 
	}

	public static void ActualizaNovedades(List<String> idnov, string strXmlRespuesta)
	{
		system.debug('TRAMA DE RETORNO==='+strXmlRespuesta);
		
		List<String> idMS=new List<String>(); 
		Map<String,String> mapNovError=new Map<String,String>(); 
		System.debug('Entra a ActuaizarNovedades');
		if (strXmlRespuesta.length() != 0)
		{
			System.debug('La respuesta tiene una longitud superior a 0');
			XmlStreamReader reader = new XmlStreamReader(strXmlRespuesta);
			string strSeparador;
			string RespuestaDavox;
			Map<String, String> CatalogoCodigos = LeeCatalogoCodigos();
			while (reader.hasNext())
			{
				System.debug('Se recorre el XmlStreamReader');
				if (reader.getEventType() == XmlTag.START_ELEMENT)
				{
					if (reader.getLocalName() == 'SEPARATOR')
					{
						strSeparador = Parse(reader);
					}
					else if (reader.getLocalName() == 'Value')
					{
						RespuestaDavox = Parse(reader);
						System.debug('\n\n RespuestaDavox: '+RespuestaDavox+'\n\n');
						string[] respuestaNovedad = RespuestaDavox.split(strSeparador);
						mapNovError.put(respuestaNovedad[0],respuestaNovedad[1]);
						idMS.add(respuestaNovedad[0]);
					}
				}
				reader.next();
			}
			if(mapNovError.size()>0)
			{
				System.debug('\n\n Ingreso al if MapNoveda \n\n');
				List<BI_Log__c> novedades=[Select id,BI_COL_Causal_Error__c,BI_COL_Estado__c,BI_COL_Informacion_recibida__c,BI_COL_Modificacion_Servicio__r.ID 
							from BI_Log__c 
							where BI_COL_Modificacion_Servicio__r.ID in:idMS and BI_COL_Estado__c='Procesando'];
				
				for(BI_Log__c n : novedades)
				{
					String rtaDavox=mapNovError.get(n.BI_COL_Modificacion_Servicio__r.ID);
					n.BI_COL_Informacion_recibida__c=CatalogoCodigos.get(rtaDavox);
					if(rtaDavox.equals('100'))
					{
						n.BI_COL_Estado__c = 'Pendiente Sincronización';
					}
					else
					{
						n.BI_COL_Estado__c = 'Devuelto';
					}
				}
				update novedades;
			}
		}
		System.debug('Termina de actualizar novedades');
	}

	public static void actualizarMsFallo(List<String> idMSAfectadas)
	{
    	System.debug('inicia cambio de ms---'+idMSAfectadas);
    	List<BI_COL_Modificacion_de_Servicio__c> lstMSRespaldo = [select id,BI_COL_Fecha_de_facturacion__c,BI_COL_Autorizacion_facturacion__c from BI_COL_Modificacion_de_Servicio__c where id IN:idMSAfectadas];
    	for(BI_COL_Modificacion_de_Servicio__c ms: lstMSRespaldo)
    	{
    		ms.BI_COL_Fecha_de_facturacion__c=null;
    		ms.BI_COL_Autorizacion_facturacion__c=false;
    	}
    	System.debug('ms modificadas------>'+lstMSRespaldo);
    	update lstMSRespaldo;
    }

    private static String Parse(XmlStreamReader reader)
	{
		System.debug('Entra al Parse de xml');
		String strTextoDevuelve = '';
		
		while(reader.hasNext())
		{
			System.debug('Se recorre el reader en el Parse');
			if (reader.getEventType() == XmlTag.END_ELEMENT)
			{
				break;
			} 
			else if (reader.getEventType() == XmlTag.CHARACTERS)
			{
				if (reader.getText() != null)
					strTextoDevuelve = reader.getText();
			}
			reader.next();
		}

		return strTextoDevuelve;
	}

	public static Map<String,String> LeeCatalogoCodigos()
	{
		Map<String, String> CatalogoCodigos = new Map<String, String>();
		
		for(BI_COL_RespuestasDavox__c c : [select id, Name,BI_COL_Descripcion_Codigo__c from BI_COL_RespuestasDavox__c])
		{
			CatalogoCodigos.put(c.Name, c.BI_COL_Descripcion_Codigo__c);
		}
		
		return CatalogoCodigos;		
	}
}