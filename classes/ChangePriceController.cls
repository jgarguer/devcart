public with sharing class ChangePriceController {
	
	public Apexpages.StandardController controller {get;set;}
	public String ordId {get;set;}
	public list<NE__OrderItem__c> parqueItems {get;set;}
	
	
	public ChangePriceController(ApexPages.StandardController stdController) {
		this.controller = stdController;
		ordId = ApexPages.currentPage().getParameters().get('id');
		
		parqueItems = [SELECT Id, NE__OrderId__c, Order_Item_Code_Linea_Pedido__c, NE__ProdId__c, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name 
					   FROM NE__OrderItem__c 
					   WHERE NE__OrderId__c =: ordId AND (NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name =: 'Parque Modification' 
														  OR NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name =: 'Parque Baja')];
	}
	
	
	public PageReference save() {
		update parqueItems;
		PageReference page = new PageReference('/apex/AssetItemSubtype?id='+ordId+'&parque=true');
		return page;
	}

}