/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales
    Company:       Deloitte
    Description:   Test class to manage the coverage code for BIIN_Detalle_Redirect class
    
    History: 
    <Date>                  <Author>                <Change Description>
    02/11/2015              Ignacio Morales         Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest(seeAllData = true)

public class BIIN_Incidencia_Detalle_Redirect_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Detalle_Redirect
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    01/11/2015              Ignacio Morales         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BIIN_Incidencia_Detalle_Redirect() {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];

        System.runAs(ActualUser){      
            Case caso = new Case(Status='Assigned', Description='Descripcion test');
            insert caso;
            Test.startTest();
            PageReference p = Page.BIIN_IncidenciaDetalle_Redirect;
            p.getParameters().put('retURL', caso.Id);
            Test.setCurrentPageReference(p);
            
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BIIN_IncidenciaDetalle_Redirect_Ctrl ceController = new BIIN_IncidenciaDetalle_Redirect_Ctrl(controller);
            PageReference redirect = ceController.redirect();
            System.debug('###Pagina1=== '+redirect);

            Test.stopTest();
        }
    }
    
    @istest
    static void caso_SolicitudIncidenciaTecnicaColombia() {
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: Label.BI2_COL_Solicitud_incidencia_tecnica].Id;
        caso.Subject = 'Caso test';
        caso.BI_Otro_Tipo__c = 'test';
        caso.BI_Country__c = 'Colombia';
        caso.BusinessHoursId = [SELECT Id, Name FROM BusinessHours WHERE Name =: Label.BI2_COL_Horario_de_oficina AND IsActive = true LIMIT 1].Id;
        caso.Status = 'Assigned';
        insert caso;

        Test.startTest();

        PageReference p = Page.BIIN_IncidenciaDetalle_Redirect;
        p.getParameters().put('retURL', caso.Id);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BIIN_IncidenciaDetalle_Redirect_Ctrl ceController = new BIIN_IncidenciaDetalle_Redirect_Ctrl(controller);
        PageReference redirect = ceController.redirect();
        System.debug('###Pagina2=== '+redirect);

        Test.stopTest();
    }
    
    @istest
    static void caso_IncidenciaTecnicaColombia() {        
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: Label.BI2_COL_Incidencia_tecnica].Id;
        caso.Subject = 'Caso test';
        caso.BI_Otro_Tipo__c = 'test';
        caso.BI_Country__c = 'Colombia';
        caso.BusinessHoursId = [SELECT Id, Name FROM BusinessHours WHERE Name =: Label.BI2_COL_Horario_de_oficina AND IsActive = true LIMIT 1].Id;
        caso.Status = 'Assigned';
        insert caso;
        
        Test.startTest();
        
        PageReference p = Page.BIIN_IncidenciaDetalle_Redirect;
        p.getParameters().put('retURL', caso.Id);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BIIN_IncidenciaDetalle_Redirect_Ctrl ceController = new BIIN_IncidenciaDetalle_Redirect_Ctrl(controller);
        PageReference redirect = ceController.redirect();
        System.debug('###Pagina3=== '+redirect);

        Test.stopTest();
    }

    @istest
    static void caso_SolicitudIncidenciaTecnica() {
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: Label.BI2_COL_Solicitud_incidencia_tecnica].Id;
        caso.Subject = 'Caso test';
        caso.BI_Otro_Tipo__c = 'test';
        caso.BI_Country__c = 'XXXXXXXXXX';
        caso.BusinessHoursId = [SELECT Id, Name FROM BusinessHours WHERE Name =: Label.BI2_COL_Horario_de_oficina AND IsActive = true LIMIT 1].Id;
        caso.Status = 'Assigned';
        insert caso;

        Test.startTest();

        PageReference p = Page.BIIN_IncidenciaDetalle_Redirect;
        p.getParameters().put('retURL', caso.Id);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BIIN_IncidenciaDetalle_Redirect_Ctrl ceController = new BIIN_IncidenciaDetalle_Redirect_Ctrl(controller);
        PageReference redirect = ceController.redirect();
        System.debug('###Pagina4=== '+redirect);

        Test.stopTest();
    }
    
    @istest
    static void caso_IncidenciaTecnica() {        
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: Label.BI2_COL_Incidencia_tecnica].Id;
        caso.Subject = 'Caso test';
        caso.BI_Otro_Tipo__c = 'test';
        caso.BI_Country__c = 'XXXXXXXXXX';
        caso.BusinessHoursId = [SELECT Id, Name FROM BusinessHours WHERE Name =: Label.BI2_COL_Horario_de_oficina AND IsActive = true LIMIT 1].Id;
        caso.Status = 'Assigned';
        insert caso;
        
        Test.startTest();
        
        PageReference p = Page.BIIN_IncidenciaDetalle_Redirect;
        p.getParameters().put('retURL', caso.Id);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BIIN_IncidenciaDetalle_Redirect_Ctrl ceController = new BIIN_IncidenciaDetalle_Redirect_Ctrl(controller);
        PageReference redirect = ceController.redirect();
        System.debug('###Pagina5=== '+redirect);

        Test.stopTest();
    }
  }