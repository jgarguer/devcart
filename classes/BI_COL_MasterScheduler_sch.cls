global class BI_COL_MasterScheduler_sch implements Schedulable {
	
	public Integer fase{get; set;}
	
	global BI_COL_MasterScheduler_sch(){
		this.fase = 0;
	}
	
	global BI_COL_MasterScheduler_sch(Integer Fase){
		this.fase = Fase;	
	}
	
	global void execute(SchedulableContext sc){
		//this.EliminarSch();
		if(fase == 0)
		{
			database.executebatch(new BI_COL_BatchDeleteDuplicateDirection_bch(),200); 
		}
		/*else if(fase == 1)
		{
			database.executebatch(new Aqui el batch,200); 
		}
		*/
	}
	
	public void EliminarSch()
    {
        List<Crontrigger> a=[Select id,state from CronTrigger where state='DELETED'];
        for(CronTrigger ct : a)
        {
            System.abortjob(ct.Id);
        }
    }

}