@RestResource(urlMapping = '/quotations')
global class API_AC_Quotations {
	public static final String COUNTRY					= 'Country';
	public static final String CITY						= 'City';
	public static final String ADDRESS					= 'Address';
	public static final String CONTRACT					= 'Contract-Term';
	public static final String BANDWIDTH				= 'Bandwidth';
	public static final String ROUTER					= 'Router';
	public static final String SPECIFICATIONS			= 'specifications';
	public static final String TECHNOLOGY				= 'technology';

	private static final String COST_CPE_MRC			= 'cost_cpe_mrc';
	private static final String COST_CPE_NRC			= 'cost_cpe_nrc';
	private static final String COST_MRC				= 'cost_mrc';
	private static final String COST_NRC				= 'cost_nrc';
	private static final String CURRENT_PRICE			= 'current_price';
	private static final String INTERNATIONAL_PRICE_MRC	= 'international_price_mrc';
	private static final String INTERNATIONAL_PRICE_NRC	= 'international_price_nrc';
	

	private static final String INTERNET				= 'INTERNET ACCESS';
	private static final String ETHERNET				= 'ETHERNET';
	private static final String MPLS					= 'MPLS VPN';

	private static final String PARENTSTR				= 'PARENT';

	private static final String STATE_COMPLETED			= 'completed';
	private static final String STATE_QUEUED			= 'queued';
	private static final String STATE_ERROR				= 'error';

	private static final String QUOTE_SOLITITUDE		= 'solicitud de cotizacion';

	private static final String USERORCLIENTERROR		= 'The User or Client is not correct.';
	private static final String MALFORMEDJSONERROR		= 'The JSON is malformated.';
	private static final String MALFORMEDJSONRELATEDPARTIESERROR		= 'The JSON is malformated at relatedParties.';
	private static final String CLIENTNOTVALID			= 'User or client are not valid.';
	private static final String DATANOTVALID			= 'The data is not valid.';
	private static final String BULKFAIL				= 'Bulk fail.';

	private static User oUser;
	private static Account oCustomer;
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Method that retrieve the Post call
	
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void doPost() { 
		System.debug('API_AC_Quotations##doPost');
		postQuotations();
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Method that manages the request, deserialize the JSON , convert it to CSV and then create the response
	
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static void postQuotations() {
		System.debug('API_AC_Quotations##postQuotations');
		WrpRequest wrpReq;
		// ******************** Comprobar que el JSON esta bien estructurado. ********************
		try {
			RestRequest request = RestContext.request; 
			String requestBody =  request.requestBody.toString();
			System.debug('RequestBody ==> ' + requestBody);
			wrpReq = (WrpRequest)JSON.deserialize(requestBody, WrpRequest.class);
			System.debug('RequestBodyDeserialized ==> ' + wrpReq);
		} catch (System.JSONException jsone) {
			genericError(MALFORMEDJSONERROR + jsone);
			return;
		}
		// ******************** Comprobar Usuario y Cliente. ToDo. ********************

		if (wrpReq.relatedParties.size() != 2) {
			genericError(MALFORMEDJSONRELATEDPARTIESERROR);
			return;
		}
		String nameUser;
		String nameCustomer;
		if (wrpReq.relatedParties[0].role == 'user' && wrpReq.relatedParties[1].role == 'customer') {
			nameUser		= wrpReq.relatedParties[0].id;
			nameCustomer	= wrpReq.relatedParties[1].id;
		} else if (wrpReq.relatedParties[0].role == 'customer' && wrpReq.relatedParties[1].role == 'user') {
			nameCustomer	= wrpReq.relatedParties[0].id;
			nameUser		= wrpReq.relatedParties[1].id;
		} else {
			genericError(MALFORMEDJSONRELATEDPARTIESERROR);
			return;
		}

		try {
			oUser = [	SELECT	id,
								Username
						FROM user
						WHERE Username = :nameUser
						LIMIT 1];
			oCustomer = [	SELECT	id,
									Name
							FROM account
							WHERE name = :nameCustomer
							LIMIT 1]; 
		} catch(Exception idError) {
			genericError(CLIENTNOTVALID);
			return;
		}

		// ******************** Comprobar que los datos son correctos. ********************
		Map<String,WrpParent> parents = convertDataToParentChild(wrpReq);
		if (parents == null) {
			genericError(DATANOTVALID);
			return;
		}
		List<WrpResponseQuoteItem> listRespQI = isDataCorrect(wrpReq.quoteItems,parents);
		if (listRespQI == null) {
			genericError(DATANOTVALID);
			return;
		}
		// ******************** Comenzar el Bulk. ********************
		

		String csv = convertDataToCsv(oCustomer.Name,wrpReq.relatedParties[0].id,parents);

		System.debug('KQC! Debugging csv String.');
		System.debug(csv);
		CWP_AC_BulkAutoQuotesVFCtrl bulkAutoQuotesVFCtrl = new CWP_AC_BulkAutoQuotesVFCtrl();
		String birId = bulkAutoQuotesVFCtrl.importFile(csv);	// bir value ==>  Bit2WinHUB__Bulk_Import_Request__c.Id 
		System.debug('KQC! Debugging return statement of CWP_AC_BulkAutoQuotesVFCtrl.importFile');
		System.debug(birId);
		WrpResponse wrpRes = new WrpResponse();
		if (birId == null) {
			// Bulk failed. CSV empty or Exception. ToDo controlarlo. // añadir aqui un else para controlar que respuesta saldra y el tipo de respuesta
			genericError(BULKFAIL);
			return;
		} 
		
		System.debug('birId' + birId);
		Bit2WinHUB__Bulk_Import_Request__c bir = [	SELECT	Id,
															Bit2WinHUB__Session_Parameters__c
													FROM Bit2WinHUB__Bulk_Import_Request__c
													WHERE Id = :birId
													LIMIT 1];
		bir.Bit2WinHUB__Session_Parameters__c = wrpReq.callbackUrl;
		UPDATE bir;


		// 	// coger el Request_Id__c => req-1478994381
		// System.debug('CVA birId' + birId);
		// String birReqId = [	SELECT	Id,
		// 													Bit2WinHUB__Request_Id__c
		// 											FROM Bit2WinHUB__Bulk_Import_Request__c
		// 											WHERE Id = :birId
		// 											LIMIT 1].Bit2WinHUB__Request_Id__c;
		// System.debug('CVA birReqId' + birReqId);

		// //update
		// Bit2WinHUB__Bulk_Configuration_Request__c bcr = [	SELECT	Id,
		// 													Bit2WinHUB__Request_Id__c
		// 											FROM Bit2WinHUB__Bulk_Configuration_Request__c
		// 											WHERE Bit2WinHUB__Request_Id__c = :birReqId
		// 											LIMIT 1];
		// System.debug('CVA bcr' + bcr);
		// bcr.Bit2WinHUB__Session_Parameters__c = wrpReq.callbackUrl;
		// UPDATE bcr;


		// Bulk is working. We get the Id in the bir attribute.
		wrpRes.id								= birId;
		wrpRes.href								= 'endpoint/v1/quotations/' + birId;
		wrpRes.quoteDate						= Datetime.now();
		wrpRes.state							= STATE_QUEUED;
		wrpRes.description						= QUOTE_SOLITITUDE;
		wrpRes.validFor							= new WrpValidFor();
		wrpRes.validFor.startDateTime			= Datetime.now();
		wrpRes.validFor.endDateTime				= Datetime.now().addMonths(1);
		wrpRes.callbackUrl						= wrpReq.callbackUrl;
		wrpRes.relatedParties 					= wrpReq.relatedParties;
		wrpRes.quoteItems						= listRespQI;


		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(wrpRes));
		System.debug(wrpRes);
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	Method that convert the WrpRequest data into csv string.
   
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Angel F Santaliestra		Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static String convertDataToCsv(String client, String accountId, Map<String,WrpParent> parents) {
		System.debug('API_AC_Quotations##convertDataToCsv >=> Params ==> client : ' + client + ' accountId : ' + accountId + ' parents : ' + parents);

		//Roberto Niubó -> 20/04/2018 -> Retrieving Record Type from TGS_RecordTypes_Util class!
		String strRecordType = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Quote');
		System.debug('RNAdebug -> API_AC_Quotations.cls -> TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, Quote) -> ' + strRecordType);


		//List<WrpParent> parents = convertDataToParentChild(wrpReq);	//012w0000000hBVCAA2							// Siempre viene vacio
		String csvLine1 = 'Configuration ID;'	+ 'NE__AccountId__c;'	+ 'RecordTypeId;'	+ 'CWP_AC_Cliente_Final__c;'	+ 'NE__Quote__c;'	+ 'Catalog';
		String csvLine2 = '0;'					+ oCustomer.Id + ';'		+ strRecordType+';'			+ client + ';'					+ ';'			+ 'Wholesale';
		for (WrpParent parent : parents.values()) {
			csvLine1 += ';Configuration Item Level 1;Quantity;Family;Dynamic Property Definition;Value;Dynamic Property Definition;Value;Dynamic Property Definition;Value;Dynamic Property Definition;Value';
			csvLine2 += ';Connectivity Project;1;Project Configuration;Country;' + parent.wrpCountry + ';City;' + parent.wrpCity + ';Address;' + parent.wrpAddress + ';Contract Term;' + parent.wrpContract;
			for (WrpChild child : parent.mapChilds.values()) {
				csvLine1 += ';Configuration Item Level 2;Quantity;Family;Dynamic Property Definition;Value;Dynamic Property Definition;Value';
				csvLine2 += ';' + child.name + ';1;' + child.name + ' Configuration;Bandwidth;' + child.wrpBandwidth + ';Router;' + child.wrpRouter;
			}
		}
		System.debug('RNAdebug -> csvLine FLAG -> 1 -> ' + csvLine1);
		System.debug('RNAdebug -> csvLine FLAG -> 2 -> ' + csvLine2);
		return csvLine1 + '\n' + csvLine2;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	Method that convert the WrpRequest data into WrpParent and WrpChild's.
	
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Angel F Santaliestra		Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static Map<String,WrpParent> convertDataToParentChild(WrpRequest wrpReq) {
		System.debug('API_AC_Quotations##convertDataToParentChild >=> Params ==> wrpReq : ' + wrpReq);
		Map<String,WrpParent> mapParents = new Map<String,WrpParent>();
		for (WrpQuoteItem wrpQuoItem : wrpReq.quoteItems) {
			if (mapParents.get(wrpQuoItem.id) == null)
				mapParents.put(wrpQuoItem.id,new WrpParent());
			WrpParent parent = mapParents.get(wrpQuoItem.id);
			if (wrpQuoItem.productOffering.name == INTERNET || wrpQuoItem.productOffering.name == MPLS || wrpQuoItem.productOffering.name == ETHERNET) {
				// Child.
				WrpChild child = new WrpChild(wrpQuoItem.productOffering.name);
				for (WrpCharacteristic wrpChar : wrpQuoItem.product.characteristics) {
					if (wrpChar.name == BANDWIDTH)
						child.wrpBandwidth = wrpChar.value;
					else if (wrpChar.name == ROUTER)
						child.wrpRouter = wrpChar.value;
				}
				parent.mapChilds.put(child.name,child);
			} else if (wrpQuoItem.productOffering.name == PARENTSTR) {
				// Parent.
				for (WrpCharacteristic wrpChar : wrpQuoItem.product.characteristics) {
					if (wrpChar.name == COUNTRY)
						parent.wrpCountry = wrpChar.value;
					else if (wrpChar.name == CITY)
						parent.wrpCity = wrpChar.value;
					else if (wrpChar.name == CONTRACT)
						parent.wrpContract = wrpChar.value;
					else if (wrpChar.name == ADDRESS)
						parent.wrpAddress = wrpChar.value;
				}
			} else {
				return null;
			}
		}
		System.debug('Mapa de Parents ==> ' + mapParents);
		return mapParents;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	Method that matches the CWP_AC_ServicesCost__c objects with the Request objects to make the response body.
	
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Angel F Santaliestra		Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static List<WrpResponseQuoteItem> isDataCorrect(List<WrpQuoteItem> items,Map<String,WrpParent> parents) {
		System.debug('API_AC_Quotations##postQuotations >=> Params ==> items : ' + items + ' parents : ' + parents);
		String query = buildQuery(parents);
		String queryMarginDiscount = buildMarginQuery(parents);
		System.debug('query' + query);
		System.debug('queryMarginDiscount' + queryMarginDiscount);

		Map<String,API_AC_Quotations.WrpParent> parentsv2 = new Map<String,API_AC_Quotations.WRpParent>(); 
		for(String pv2:parents.keySet()){
			parentsv2.put(pv2, parents.get(pv2));
		}
		System.debug('parentsv2:' + parentsv2);
		System.debug('parentsv1:' + parents);
		Map<String,CWP_AC_ServicesCost__c> mapServiceCosts = new Map<String,CWP_AC_ServicesCost__c>();
		for (CWP_AC_ServicesCost__c serviceCost : Database.query(query))
			mapServiceCosts.put(serviceCost.CWP_AC_Country__c + '-' + serviceCost.CWP_AC_City__c + '-' + serviceCost.CWP_AC_Contract_Term__c + '-' + serviceCost.CWP_AC_Bandwidth__c + '-' + serviceCost.CWP_AC_Router__c + '-' + serviceCost.CWP_AC_ServiceName__c,serviceCost);
		
		Map<String,CWP_AC_MARGINDISCOUNTS__c> mapMarginDiscounts = new Map<String,CWP_AC_MARGINDISCOUNTS__c>();
		for (CWP_AC_MARGINDISCOUNTS__c oCWPACMarginDiscount : Database.query(queryMarginDiscount))
			mapMarginDiscounts.put(oCWPACMarginDiscount.CWP_AC_Country__c + '-' + oCWPACMarginDiscount.CWP_AC_ProductName__c + '-' + oCWPACMarginDiscount.CWP_AC_ClientName__c,oCWPACMarginDiscount);
		
		List<WrpResponseQuoteItem> listRespQuoteItems = new List<WrpResponseQuoteItem>();
		WrpResponseQuoteItem respQItem;
		Integer contador = 0;
		for (WrpQuoteItem qi : items) {
			WrpParent parent = parentsv2.remove(qi.id);
			System.debug('qi' + qi);
			if (parent != null){
				
				for (WrpChild child : parent.mapChilds.values()) {
					CWP_AC_ServicesCost__c serviceCost	= mapServiceCosts.get(parent.wrpCountry + '-' + parent.wrpCity + '-' + parent.wrpContract + '-' + child.wrpBandwidth + '-' + child.wrpRouter + '-' + child.name);
					CWP_AC_MARGINDISCOUNTS__c oCWPACMarginDiscount = mapMarginDiscounts.get(parent.wrpCountry + '-' + child.name + '-' + oCustomer.Name);

					if (serviceCost == null){
						return null;
					}
					if (oCWPACMarginDiscount == null){
						return null;
					}

					Double mrc = (serviceCost.CWP_AC_Coste_MRC__c + serviceCost.CWP_AC_Coste_CPE_MRC__c ) / (1 - oCWPACMarginDiscount.CWP_AC_Margin__C) + (serviceCost.CWP_AC_Precio_Internacional_MRC__c * ( 1 - oCWPACMarginDiscount.CWP_AC_Discount__C));
					Double nrc = (serviceCost.CWP_AC_Coste_NRC__c + serviceCost.CWP_AC_Precio_Internacional_NRC__c) / (1 - oCWPACMarginDiscount.CWP_AC_Margin__C) + (serviceCost.CWP_AC_Precio_Internacional_NRC__c * (1 - oCWPACMarginDiscount.CWP_AC_Discount__C));

					respQItem = new WrpResponseQuoteItem(qi.id,new WrpProductOffering(serviceCost.CWP_AC_ServiceName__c),new WrpProduct().fillCharacteristics(serviceCost,oCWPACMarginDiscount),mrc,nrc);
					listRespQuoteItems.add(respQItem);
				}
			}
		}
		return listRespQuoteItems;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:
	
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Angel F Santaliestra		Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static String buildQuery(Map<String,WrpParent> parents) {
		String query = '	SELECT	Id,'
								+ '	CWP_AC_Country__c,'
								+ '	CWP_AC_City__c,'
								+ '	CWP_AC_Contract_Term__c,'
								+ '	CWP_AC_Bandwidth__c,'
								+ '	CWP_AC_Router__c,'
								+ '	CWP_AC_ServiceName__c,'
								+ '	CWP_AC_Specifications__c,'
								+ '	CWP_AC_Technology__c,'
								+ '	CWP_AC_Coste_CPE_MRC__c,'
								+ '	CWP_AC_Coste_CPE_NRC__c,'
								+ '	CWP_AC_Coste_MRC__c,'
								+ '	CWP_AC_Coste_NRC__c,'
								+ '	CWP_AC_CurrencyPrice__c,'
								+ '	CWP_AC_Precio_Internacional_MRC__c,'
								+ '	CWP_AC_Precio_Internacional_NRC__c'
						+ '	FROM CWP_AC_ServicesCost__c'
						+ '	WHERE';
		Boolean firstParent = true;
		for (WrpParent parent : parents.values()) {
			for (WrpChild child : parent.mapChilds.values()) {
				if (!firstParent)
					query += ' OR ';
				firstParent = false;	
				query += ' (CWP_AC_Country__c = \'' + parent.wrpCountry + '\' AND CWP_AC_City__c = \'' + parent.wrpCity + '\' AND CWP_AC_Contract_Term__c = ' + parent.wrpContract + '';
				query += ' AND CWP_AC_Bandwidth__c = ' + child.wrpBandwidth + ' AND CWP_AC_Router__c = \'' + child.wrpRouter + '\' AND CWP_AC_ServiceName__c = \'' + child.name + '\')';
			}
		}
		return query;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	
	
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static String buildMarginQuery(Map<String,WrpParent> parents) {
		String queryMarginDiscount = '	SELECT CWP_AC_Margin__c, CWP_AC_Discount__c, CWP_AC_ProductName__c, CWP_AC_ClientName__c, CWP_AC_Country__c FROM CWP_AC_MARGINDISCOUNTS__c WHERE ';
		Boolean firstParent = true;
		for (WrpParent parent : parents.values()) {
			for (WrpChild child : parent.mapChilds.values()) {
				if (!firstParent)
					queryMarginDiscount += ' OR ';
				firstParent = false;	
				queryMarginDiscount += ' (CWP_AC_ClientName__c = \'' + oCustomer.Name + '\' AND CWP_AC_Country__c = \'' + parent.wrpCountry + '\'';
				queryMarginDiscount += ' AND CWP_AC_ProductName__c = \'' + child.name + '\')';
			}
		} 
		return queryMarginDiscount;
	}
	private static void genericError(String errorType) {
		RestContext.response.responseBody = Blob.valueOf(errorType);
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the request.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpRequest {
		String description;						// La descripcion de la llamada , normalmente será "solicitud de cotizacion"
		List<WrpRelatedParty> relatedParties;	// Lista con los datos del cliente
		List<WrpQuoteItem> quoteItems;			// Lista con los datos de las ofertas
		String callbackUrl;						// Url utilizada para la gestión asíncrona
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the request and response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpRelatedParty {  // SE USA TANTO EN EL REQUEST COMO EN LA RESPONSE, Modificar con responsabilidad
		public String id;		// Id del cliente
		public String name;		// Nombre del cliente --> "cliente final"
		public String role;		// Rol del cliente --> "customer"

		public WrpRelatedParty(String id,String name,String role){
			this.id	= id;
			this.name	= name;
			this.role	= role;
		}

	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the request.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpQuoteItem { 
		String id;							// ID de cada QuoteItem 
		String action;						// Descripcion de la accion que se va a hacer, siempre es "add"
		WrpProductOffering productOffering;	// Objeto para el nombre del producto 
		WrpProduct product;					// Objeto con las caracteristicas del producto
		String quantity;					// Numero de productos dentro de este QuoteItem. Siempre es "01"
		public WrpQuoteItem(String name,String mrc,String nrc) {
			this.productOffering	= new WrpProductOffering(name);
			this.product			= new WrpProduct();
			this.product.characteristics.add(new WrpCharacteristic(COST_MRC,mrc));
			this.product.characteristics.add(new WrpCharacteristic(COST_NRC,nrc));
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the request and the response
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpProductOffering { // SE USA TANTO EN EL REQUEST COMO EN LA RESPONSE, Modificar con responsabilidad
		public String id;			// ID para identificar la oferta del producto. Formado por "ID_" + el name
		public String name;			// Nombre de la oferta del producto (Internet MPLS o Ethernet)
		public String entityType;	// Tipo , siempre es "product"
		public WrpProductOffering(String name) {
			this.name		= name;
			this.id			= 'ID_' + this.name;
			this.entityType	= 'product';
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the request and the response
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpProduct {	// SE USA TANTO EN EL REQUEST COMO EN LA RESPONSE, Modificar con responsabilidad
		public List<WrpCharacteristic> characteristics;	// Lista de las caracteristicas del producto
		public WrpProduct() {			// ToDo. No se si se utilizan todos los campos. 27-03
			this.characteristics = new List<WrpCharacteristic>();
		}
		public WrpProduct fillCharacteristics(CWP_AC_ServicesCost__c serviceCost,CWP_AC_MARGINDISCOUNTS__c marginDiscount) { // CWP_AC_Margin__c, CWP_AC_Discount__c, CWP_AC_ProductName__c, CWP_AC_ClientName__c, CWP_AC_Country__c
			this.characteristics.add(new WrpCharacteristic(COUNTRY,serviceCost.CWP_AC_Country__c));
			this.characteristics.add(new WrpCharacteristic(CITY,serviceCost.CWP_AC_City__c));
			this.characteristics.add(new WrpCharacteristic(CONTRACT,String.valueOf(serviceCost.CWP_AC_Contract_Term__c)));
			this.characteristics.add(new WrpCharacteristic(BANDWIDTH,String.valueOf(serviceCost.CWP_AC_Bandwidth__c)));
			this.characteristics.add(new WrpCharacteristic(ROUTER,serviceCost.CWP_AC_Router__c));
			this.characteristics.add(new WrpCharacteristic(SPECIFICATIONS,serviceCost.CWP_AC_Specifications__c));
			this.characteristics.add(new WrpCharacteristic(TECHNOLOGY,serviceCost.CWP_AC_Technology__c));
			return this;
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the request.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpCharacteristic {
		String valueType;	// El valor del tipo en el que llega la informacion , siempre es "String" 
		String type;		// El tipo,  siempre es "StringType"
		String name;		// El nombre del producto (Pueden ser varios como Bandwidth o router o city o country o contract o los de los precios )
		String value;		// El valor. Este varia dependiendo del producto que se trate
		public WrpCharacteristic(String field,String value) {
			this.valueType	= 'String';
			this.type		= 'StringType';
			this.name		= field;
			this.value		= value;
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	Wrapper for the request.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Angel Felipe Santaliestra Pasias			Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpParent {
		String wrpCountry;				// El nombre del pais
		String wrpCity;					// El nombre de la ciuddad
		String wrpContract;				// El valor del Contract-Term
		String wrpAddress;				// La direccion de la calle
		Map<String,WrpChild> mapChilds;	// Hijos con los productos de los que se solicitan el precio
		public WrpParent() {
			this.mapChilds = new Map<String,WrpChild>();
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	Wrapper for the request.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Angel Felipe Santaliestra Pasias				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpChild {
		String name;			// Nombre del producto
		String wrpBandwidth;	// Nombre para la bandwith (ancho de banda)
		String wrpRouter;		// nombre para los productos router
		public WrpChild(String name) {
			this.name = name;
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpResponse {
		public String id;								// identificador único de la consulta asignado por el servidor que procesa la solicitud de cotización
		public String href;								// "endpoint/v1/quotations/{quotationId}"
		public Datetime quoteDate;						// fecha en que se  recibió el request
		public String state;							// el estado de la consulta -> "completed"
		public String description; 						// siempre -> "solicitud de cotización",
		public List<WrpRelatedParty> relatedParties;	// Lista con los datos del cliente
		public List<WrpResponseQuoteItem> quoteItems;	// Lista con los datos de las ofertas
		public WrpValidFor validFor;					// Objeto con fechas para la cotizacion
		public String callbackUrl;						// Url utilizada para la gestión asíncrona
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpResponseQuoteItem {
		public String id;								// ID de cada QuoteItem 
		public String action;							// Descripcion de la accion que se va a hacer, siempre es "add"
		public WrpProductOffering productOffering;		// Objeto para el nombre del producto 
		public WrpProduct product;						// Objeto con las caracteristicas del producto
		public String quantity;							// Numero de productos dentro de este QuoteItem. Siempre es "01"
		public String state;							// Estado de la operacion --> "completed"
		public List<WrpQuoteItemPrice> quoteItemPrice;	// Objeto las caracteristicas del precio del producto, esta combinación solo incluye un pago recurrente
		
		public WrpResponseQuoteItem(String id,WrpProductOffering productOffering,WrpProduct product,Decimal mrc,Decimal nrc) {
			this.id					= id;
			this.action				= 'add';
			this.productOffering	= productOffering;
			this.product			= product;
			this.quantity			= '1';
			this.state				= STATE_QUEUED;
			this.quoteItemPrice		= new List<WrpQuoteItemPrice>();
			this.quoteItemPrice.add(new WrpQuoteItemPrice('recurring','monthly',String.valueOf(mrc)));
			this.quoteItemPrice.add(new WrpQuoteItemPrice('usage','one-shot',String.valueOf(nrc)));
		}
		public WrpResponseQuoteItem(String prodName,Decimal mrc,Decimal nrc, String id) {
			this.state				= STATE_COMPLETED;
			this.quoteItemPrice		= new List<WrpQuoteItemPrice>();
			this.productOffering	= new WrpProductOffering(prodName);
			this.product			= new WrpProduct();
			this.quantity			= '1';
			this.action				= 'add';
			this.id					= id;
			this.quoteItemPrice.add(new WrpQuoteItemPrice('recurring','monthly',String.valueOf(mrc)));
			this.quoteItemPrice.add(new WrpQuoteItemPrice('usage','one-shot',String.valueOf(nrc)));
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpQuoteItemPrice {
		public String priceType;				// El tipo de precio , recurring o usage
		public String recurringChargePeriod;	// Si el precio es recurrente el periodo en el que se recurre
		public WrpPrice price;					// Objeto con el precio
		public WrpUnitOfMeasure unitOfMeasure;	// para indicar el elemnto de consumo que se usa para aplicar el precio (2 min voz, 200 MB de datos, 1 GB de storage, …)

		public WrpQuoteItemPrice(String priceType,String period,String price) {
			this.priceType				= priceType;
			this.recurringChargePeriod	= period;
			this.price					= new WrpPrice(price);
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpPrice {
		public String amount;	// La cantidad  del precio para el producto
		public String units;	// La unidad de dinero (dolares, euros...)

		public WrpPrice(String price) {
			this.amount	= price;
			this.units	= 'eur';
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpUnitOfMeasure {
		public String amount;	// La cantidad de la unidad
		public String units;	// La unidad que la que estamos hablando (MB,min,GB...)

		public WrpUnitOfMeasure(String price) {
			this.amount	= price;
			this.units	= 'obj';
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture
	Description:	Wrapper for the response.
		
	History:
	<Date>			<Author>					<Description>
	20/04/2018		Carlos Vicente				Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public class WrpValidFor{
		public Datetime startDateTime {	// Fecha en la que ha empezado la cotizacion
			get;
			set {
				startDateTime = Datetime.valueOf(value);
			}
		}
		public Datetime endDateTime {	// Fecha valida para la cotización, es válida por un mes
			get;
			set {
				endDateTime = Datetime.valueOf(value);
			}
		}
	}
}