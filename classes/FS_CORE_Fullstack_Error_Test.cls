/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CORE_FullStack_Error

History:
<Date>							<Author>						<Change Description>
3/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CORE_Fullstack_Error_Test {
    
    @testSetup
    private static void init() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        insert bypass;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, FS_CORE_ID_Cliente_legado__c = '000000000X',
                                      FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        insert account;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        
        /* Contact */
        Contact contact = new Contact(FirstName = 'Integración', LastName = 'Perú', BI_Activo__c = true,
                                      BI_Tipo_de_contacto__c = 'Administrativo', BI_Representante_legal__c = true, Phone = '000000000',
                                      AssistantPhone = '000000000', HomePhone = '000000000', MobilePhone = '000000000',
                                      OtherPhone = '000000000', Fax = '000000000', email = 'integraciones@integraciones.test',
                                      BI_Country__c = 'Peru', BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '0000000X',
                                      Birthdate = Date.today(), BI_Genero__c = 'Masculino', accountId = account.Id,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Active_Contact' LIMIT 1][0].Id, BI_Id_del_contacto__c = '00000000X', FS_CORE_Estado_de_Sincronizacion__c = 'Sincronizado');
        
        insert contact;
    }
    
    @isTest private static void createTask() {
        /* Test */
        System.Test.startTest();
        
        For(integer i = 0; i < 9; i++){
            FS_CORE_FullStack_Error.createTask(500, '{"Error":{"code":{"service" :"0","operation":"0","layer":null,"tamSystem":null,"legacySystem":null,"api":null,"error":null},"description":"Generic Mock Error","details":{"timeStamp":null,"cause":null}}}', 'Account', [SELECT Id FROM Account LIMIT 1][0].Id, false, i , null);
        }
        
        System.Test.stopTest();
    }
}