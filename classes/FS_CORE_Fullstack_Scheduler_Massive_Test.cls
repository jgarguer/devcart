/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							
Company:						Everis España
Description:					

History:
<Date>							<Author>						<Change Description>

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CORE_Fullstack_Scheduler_Massive_Test {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    
    static testmethod void test() {
        
        Test.startTest();
        
        FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();            
        ORGConfiguration.FS_CORE_Recarterizacion_Limit__c= 2456;
        ORGConfiguration.FS_CORE_Resegmentacion_Limit__c= 4562;
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';
            
        insert ORGConfiguration;
        //ini eve
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name = :Label.BI_Administrador_del_sistema limit 1];
		String en_US = 'en_US';
        
        String nombre = BI_Dataload.generateRandomString(4);

       
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789',FS_CORE_Account_Massive__c = True, FS_CORE_ATM_Massive__c = True,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id);
        
        insert account;
		        
        //List <AccountTeamMember> lst_AccTM = new List <AccountTeamMember>();
            AccountTeamMember accTM = new AccountTeamMember();
             accTM.AccountId = account.Id;
             accTM.UserId = UserInfo.getUserId();
             //lst_AccTM.add(accTM);
        insert accTM;    
            /*AccountTeamMember accTM2 = new AccountTeamMember();
             accTM2.AccountId = [SELECT Id From Account LIMIT 1][0].Id;
             lst_AccTM.add(accTM2);
             accTM.UserId = UserInfo.getUserId();
        
        insert lst_AccTM;*/
           
        //fin eve
        String jobId = System.schedule('Testing FS_CORE_Fullstack_Scheduler_Massive', CRON_EXP, new FS_CORE_Fullstack_Scheduler_Massive());
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        System.assertEquals(0, ct.TimesTriggered);
        
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
              
    }
 
}