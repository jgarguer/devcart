/*------------------------------------------------------------------------------------------------------------------------------------------
Author:        José María Martín
Company:       Deloitte
Description:   Test method to manage the code coverage for BIIN_Crear_WorkInfo_WS

<Date>              <Author>                    <Change Description>
11/2015             José María Martín           Initial Version
24/05/2016          Julio Laplaza               Adaptation to UNICA
------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest(seeAllData = false)
public class BIIN_Crear_WorkInfo_TEST 
{
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Crear_WorkInfo_TEST';

    @testSetup static void dataSetup() 
    {
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));  

        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;
        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];

        Case ticket = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        Case ticketWithoutAccount = new Case();
        ticketWithoutAccount.BI_Id_del_caso_legado__c = 'TestTicketWithoutAcc';
        insert ticket;
        insert ticketWithoutAccount;
    }

    static testMethod void test_BIIN_Crear_WorkInfo_TEST() 
    {
        BI_TestUtils.throw_exception=false;

        // Create attachments
        List<BIIN_Crear_WorkInfo_WS.Adjunto> ladjuntos= new List<BIIN_Crear_WorkInfo_WS.Adjunto>();
        Blob cuerpo = Blob.valueof('Cuerpo del Adjunto');
        BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto('Adjunto1', cuerpo);
        BIIN_Crear_WorkInfo_WS.Adjunto a1 = new BIIN_Crear_WorkInfo_WS.Adjunto('Adjunto2', cuerpo);
        ladjuntos.add(a); 
        ladjuntos.add(a1); 
        
		Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticket = [SELECT Id, BI_Id_del_caso_legado__c, AccountId FROM Case WHERE AccountId =: account.Id];
        Case ticketWithoutAccount = [SELECT Id, BI_Id_del_caso_legado__c, AccountId FROM Case WHERE BI_Id_del_caso_legado__c =: 'TestTicketWithoutAcc']; 
       
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));  
        Test.startTest(); 

        BIIN_Crear_WorkInfo_WS ceController = new BIIN_Crear_WorkInfo_WS();
        ceController.crearWorkInfo('', ticket, ladjuntos, 'Nota', '2');
        ceController.crearWorkInfo('', ticketWithoutAccount, ladjuntos, 'Nota', '2');

        Test.stopTest();
    }
}