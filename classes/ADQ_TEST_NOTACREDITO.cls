@isTest
private class ADQ_TEST_NOTACREDITO {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Adrián Caro
    Company:       Accenture
    Description:   test class about ADQ_NotaCreditoController
    
    History:
    
    <Date>            <Author>              <Description>
    03/01/2017       Adrián Caro          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	//variables
	private static NotadeCredito__c ncp;
	private static Programacion__c programacion;
	private static ApexPages.StandardController sc;
	private static ADQ_NotaCreditoController ncc;
	private static ADQ_NotaCreditoController.ConceptoFactura  wrapper01;
	private static Pedido_PE__c pedido;
	private static NotaCredito_OLI__c ncpoli;
	private static Factura__c factura;
	private static Account cuenta;
	private static List<ADQ_NotaCreditoController.ConceptoFactura> listaConceptos;

	@testsetup
	private static void setUpTest(){
		//ejecutar con un perfil administrador para que no salten las validation rules
		Id idProfile =BI_DataLoad.searchAdminProfile();
		List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
		system.runAs(lst_user[0]){
		
		//lista cuentas
		List <Account> lstacc = BI_DataLoad.loadAccounts(1, new List <String> {'Spain'});
		//lista facturas
		List <Factura__c> lstfac = BI_DataLoad.generateFactura(5, lstacc[0].Id);

		//lista de programacion
		List <Programacion__c> lstprog = BI_DataLoad.generateprogramacion(5, lstfac[0].Id);

		//lista NotadeCredito
		List <NotadeCredito__c> lstncp = BI_DataLoad.generateNotadeCredito(5, lstprog[0].Id);

		//lista de pedidos
		List <Pedido_PE__c> lstped = BI_DataLoad.generatePedido(5, lstprog[0].Id);

		List <NotaCredito_OLI__c> lstncpoli = BI_DataLoad.generateNotadeCreditoOLI(5, lstncp[0].Id);
		}
	}

	@isTest
	public static void ADQ_NotaCreditoController_test(){
		//coverage getMuestraBloqueSeleccionNC(), getImporteReadOnly(), getMuestraBotonContinuar(), getMuestraBoton(),getProgramacion(), getCliente()
		ncp = [select Id, Factura__c, Importe__c, Cliente__c from NotadeCredito__c LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		wrapper01 = new ADQ_NotaCreditoController.ConceptoFactura ('000001',  'VPN MPLS RENTA', 200, 200, 200, true,false);
		ncc.getMuestraBloqueSeleccionNC();
		ncc.getImporteReadOnly();
		ncc.getMuestraBotonContinuar();
		ncc.getMuestraBoton();
		ncc.getProgramacion();
		ncc.getCliente();
	}

	@isTest
	public static void seleccionaTipoNota_test(){
		//coverage seleccionaTipoNota(), generaNotaTotal(), generaNotaParcial(), validaNotaCredito()
		ncp = [select Id, Factura__c,TipoNota__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.seleccionaTipoNota();

		ncp.TipoNota__c = 'Total';
		update ncp;
		ncc.seleccionaTipoNota();
		ncc.generaNotaTotal();
		ncc.generaNotaParcial();
		ncc.validaNotaCredito();
	}

	@isTest
	public static void creaConceptos_test(){
		//coverage creaConceptos(),validaNotaTotal()
		pedido = [select Id, Programacion__c, Precio_convertido__c, Concepto__c from Pedido_PE__c LIMIT 1];
		ncp = [select Id, Factura__c,TipoNota__c from NotadeCredito__c LIMIT 1];
		ncpoli = [select Id, Importe__c, NotaCreditoId__c from NotaCredito_OLI__c LIMIT 1];
		ncp.Factura__c = pedido.Programacion__c;
		update ncp;

		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.creaConceptos();

		ncp.Id = ncpoli.NotaCreditoId__c;
		update ncp;
		ncc.creaConceptos();
		ncc.validaNotaTotal();
	}
	@isTest
	public static void cambiaImporte_test(){
		//coverage getListaConceptos, cambiaImporte
		ncp = [select Id, Factura__c, Importe__c, TipoNota__c from NotadeCredito__c LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		listaConceptos = new List<ADQ_NotaCreditoController.ConceptoFactura>();
		wrapper01 = new ADQ_NotaCreditoController.ConceptoFactura ('000001',  'VPN MPLS RENTA', 200, 200, 200, true,false);
		listaConceptos.add(wrapper01);
		System.debug(wrapper01);
		ncc.seleccionaTipoNota();
		ncc.getListaConceptos();
		ncc.cambiaImporte();
	}
	@isTest
	public static void validaImporte_test(){
		//coverage validaImporte
		ncp = [select Id, Factura__c,TipoNota__c, Importe__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.seleccionaTipoNota();

		ncp.TipoNota__c = 'Total';
		update ncp;
		ncc.validaImporte();

		ncp = [select Id, Factura__c,TipoNota__c,Importe__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.validaImporte();

	}
	@isTest
	public static void validaSeleccion_test(){
		//validaSeleccion
		ncp = [select Id, Factura__c,TipoNota__c, Importe__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.seleccionaTipoNota();
		ncc.validaSeleccion();

		ncp = [select Id, Factura__c,TipoNota__c, Importe__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);

		ncc.creaNotaParcial = false;
		ncc.validaSeleccion();

	}
	@isTest
	public static void actualizaPedidosPE_test(){
		//actualizaPedidosPE
		ncp = [select Id, Factura__c,TipoNota__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		ncpoli = [select Id, Importe__c, NotaCreditoId__c from NotaCredito_OLI__c LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.seleccionaTipoNota();
		wrapper01 = new ADQ_NotaCreditoController.ConceptoFactura ('000001', 'VPN MPLS RENTA', 200, 200, 200, true,false);
		ncc.actualizaPedidosPE(ncp.Id);

		ncp = [select Id, Factura__c,TipoNota__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.seleccionaTipoNota();
		ncc.creaNotaParcial = false;
		ncc.actualizaPedidosPE(ncp.Id);

	}
	@isTest
	public static void cancel_test(){
		//cancel
		ncp = [select Id, Factura__c,TipoNota__c, Importe__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);

		ncc.cancel();
	}
	@isTest
	public static void guardar_test(){
		//guardar
		ncp = [select Id, Factura__c,TipoNota__c, Importe__c from NotadeCredito__c where TipoNota__c = 'Parcial' LIMIT 1];
		sc = new ApexPages.StandardController(ncp);
		ncc = new ADQ_NotaCreditoController(sc);
		ncc.existeNotaCredito = false;
		ncc.validaImporte();
		ncc.validaSeleccion();

		ncc.guardar();

	}

}