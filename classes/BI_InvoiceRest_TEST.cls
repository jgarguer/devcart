@isTest
private class BI_InvoiceRest_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_InvoiceRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    17/09/2014              Pablo Oliva             Initial Version
    27/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception set to false to avoid test errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_InvoiceRest.getInvoice()
    History:
    
    <Date>              <Author>                <Description>
    17/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getInvoiceTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        //List <Region__c> lst_region = BI_DataLoad.loadRegions().values();
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
        
        //BI_Pickup_Option__c pickupOption = BI_DataLoadRest.loadPickUpOptions(lst_region);          							   			 
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <BI_Facturas__c> lst_inv = BI_DataLoadRest.loadInvoices(1, lst_region, lst_acc[0]);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/invoices/'+lst_inv[0].BI_Id_sistema_legado__c;  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;

        //INTERNAL_SERVER_ERROR
        BI_InvoiceRest.getInvoice();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST
        BI_TestUtils.throw_exception = false;
        BI_InvoiceRest.getInvoice();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        req.requestURI = '/accountresources/v1/invoices/idInvoice';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
        
        BI_InvoiceRest.getInvoice();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/accountresources/v1/invoices/'+lst_inv[0].BI_Id_sistema_legado__c;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_InvoiceRest.getInvoice();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}