public class BI_Campanas_CuentaMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Campanas_Cuenta trigger.
    History:
    
    <Date>            <Author>          <Description>
    13/11/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that updates the field BI_PER_Campana__c in the related accounts
    
    IN:            List<BI_Campanas_Cuenta__c> lst_camp
    			   Integer option: 0 -> Insert
    			   				   1 -> Delete
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    13/11/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateCampanaField(List<BI_Campanas_Cuenta__c> lst_camp, Integer option){
		
		try{
		
			Map<String, Set<String>> map_cc = new Map<String, Set<String>>();
				
			for(BI_Campanas_Cuenta__c cc:lst_camp){
				if(map_cc.containsKey(cc.Name.split('-')[0]))
					map_cc.get(cc.Name.split('-')[0]).add(cc.Name.split('-')[1]);
				else{
					Set<String> set_idCampaign = new Set<String>();
					set_idCampaign.add(cc.Name.split('-')[1]);
					map_cc.put(cc.Name.split('-')[0], set_idCampaign);
				}
			}
			
			if(!map_cc.isEmpty()){
				
				List<Account> lst_acc = new List<Account>();
			
				if(option == 0){
					
					for(Account acc:[select Id, BI_PER_Campana__c from Account where Id IN :map_cc.keySet()]){
						String campaigns = '';
						for(String camp:map_cc.get(acc.Id))
							campaigns += camp+',';
							
						campaigns = campaigns.substring(0, campaigns.length() - 1);
						
						if(acc.BI_PER_Campana__c == null || acc.BI_PER_Campana__c == '')
							acc.BI_PER_Campana__c = campaigns;
						else
							acc.BI_PER_Campana__c += ',' + campaigns;
							
						lst_acc.add(acc);
						
					}
					
				}else{
					
					for(Account acc:[select Id, BI_PER_Campana__c from Account where Id IN :map_cc.keySet()]){
						String campaigns = '';
						for(String camp:acc.BI_PER_Campana__c.split(',')){
							if(!map_cc.get(acc.Id).contains(camp))
								campaigns += camp+',';
						}
						
						if(campaigns != ''){
							campaigns = campaigns.substring(0, campaigns.length() - 1);
							acc.BI_PER_Campana__c = campaigns;
						}else
							acc.BI_PER_Campana__c = null;
							
						lst_acc.add(acc);
						
					}
					
				}
				
				update lst_acc;
				
			}
			
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_Campanas_CuentaMethods.updateCampanaField', 'BI_EN', Exc, 'Trigger');
		}
		
	}

}