/*-------------------------------------------------------------------
	Author:        Virgilio Utrera
	Company:       Salesforce.com
	Description:   Test class for BI_FOCODeleteErrorLogs_CTRL
	
	History
	<Date>          <Author>           <Change Description>
	03-Mar-2015     Virgilio Utrera    Initial Version
	04-Mar-2015     Virgilio Utrera    Added additional test methods
-------------------------------------------------------------------*/

@isTest
private class BI_FOCODeleteErrorLogs_TEST {

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for loading the available Error Logs on the picklist
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static testMethod void testLoadAvailableErrorLogs() {
		BI_FOCODeleteErrorLogs_CTRL controller;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		// Create available error log Ids and pending-for-deletion error log Ids
		createAvailableErrorLogIds(10);
		createErrorLogIdsPendingForDeletion(8);

		// Load the controller
		Test.startTest();
		controller = new BI_FOCODeleteErrorLogs_CTRL();
		Test.stopTest();

		// Verify there are 2 log Ids available to show
		System.assertEquals(2, controller.errorLogIdOptions.size());
		System.assertEquals(true, controller.showPicklist);
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for returning to the Registro Datos FOCO Error Log tab
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static testMethod void testGoBack() {
		BI_FOCODeleteErrorLogs_CTRL controller;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		PageReference page;
		String url = '';

		// Load the controller and run the goBack method and get the redirect url
		Test.startTest();
		controller = new BI_FOCODeleteErrorLogs_CTRL();
		page = controller.goBack();
		url = page.getUrl();
		Test.stopTest();

		// Verify the user was correctly redirected
		System.assertEquals('/' + BI_Registro_Datos_FOCO_Error_Log__c.sObjectType.getDescribe().getKeyPrefix(), url);
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for deleting an error log
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static testMethod void testDeleteErrorLog() {
		BI_FOCODeleteErrorLogs_CTRL controller;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		List<BI_Registro_Datos_FOCO_Error_Log__c> errorLogsList = new List<BI_Registro_Datos_FOCO_Error_Log__c>();
		Map<String, BI_FOCO_Available_Error_Logs__c> availableErrorLogIdsMap = new Map<String, BI_FOCO_Available_Error_Logs__c>();
		Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c> pendingErrorLogIdsMap = new Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c>();

		// Create available error log to delete
		createErrorLog(1000, 'ERROR_LOG_123456');

		// Load the controller and delete selected error log
		Test.startTest();
		controller = new BI_FOCODeleteErrorLogs_CTRL();
		controller.selectedValue = controller.errorLogIdOptions[0].getValue();
		controller.deleteFOCOErrorLog();
		Test.stopTest();

		// Verify selected error log has been deleted
		errorLogsList = [SELECT Id FROM BI_Registro_Datos_FOCO_Error_Log__c WHERE BI_Identificador_Log__c = 'ERROR_LOG_123456' LIMIT 1000 ];
		System.assertEquals(0, errorLogsList.size());

		availableErrorLogIdsMap = BI_FOCO_Available_Error_Logs__c.getAll();
		System.assertEquals(0, availableErrorLogIdsMap.size());

		pendingErrorLogIdsMap = BI_FOCO_Error_Logs_Pending_Deletion__c.getAll();
		System.assertEquals(0, pendingErrorLogIdsMap.size());
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for deleting an error log that is already being deleted
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static testMethod void testDeleteErrorLogAlreadyInDeletionProcess() {
		BI_FOCODeleteErrorLogs_CTRL controller;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		Map<String, BI_FOCO_Available_Error_Logs__c> availableErrorLogIdsMap = new Map<String, BI_FOCO_Available_Error_Logs__c>();

		// Create pending-for-deletion error log Ids
		createErrorLogIdsPendingForDeletion(1);

		// Load the controller and delete selected error log
		Test.startTest();
		controller = new BI_FOCODeleteErrorLogs_CTRL();
		controller.selectedValue = 'ERROR_LOG_ID_0';
		controller.deleteFOCOErrorLog();
		Test.stopTest();

		// Verify selected error log is not available for deletion
		availableErrorLogIdsMap = BI_FOCO_Available_Error_Logs__c.getAll();
		System.assertEquals(0, availableErrorLogIdsMap.size());

		// Verify error message is displayed
		System.assertEquals(true, ApexPages.hasMessages());
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for trying to delete an error log without selecting a value on the available error logs picklist
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static testMethod void testDeleteErrorLogNoSelectedValue() {
		BI_FOCODeleteErrorLogs_CTRL controller;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		Map<String, BI_FOCO_Available_Error_Logs__c> availableErrorLogIdsMap = new Map<String, BI_FOCO_Available_Error_Logs__c>();

		// Load the controller and delete selected error log
		Test.startTest();
		controller = new BI_FOCODeleteErrorLogs_CTRL();
		controller.selectedValue = '';
		controller.deleteFOCOErrorLog();
		Test.stopTest();

		// Verify error message is displayed
		System.assertEquals(true, ApexPages.hasMessages());
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Test method for exception handling while trying to delete an error log
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static testMethod void testDeleteErrorLogBadQueryBatchException() {
		BI_FOCODeleteErrorLogs_CTRL controller;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		// Load the controller and delete selected error log
		Test.startTest();
		controller = new BI_FOCODeleteErrorLogs_CTRL();
		controller.selectedValue = 'SELECT Id FROM BI_Registro_Datos_FOCO_Error_Log__c LIMIT 1';
		controller.deleteFOCOErrorLog();
		Test.stopTest();

		// Verify error message is displayed
		System.assertEquals(true, ApexPages.hasMessages());
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Method for creating available error log Ids
		IN:				Integer: quantity of error log Ids to be created
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static private void createAvailableErrorLogIds(Integer quantity) {
		BI_FOCO_Available_Error_Logs__c errorLogId;
		List<BI_FOCO_Available_Error_Logs__c> errorLogIdsList = new List<BI_FOCO_Available_Error_Logs__c>();
		Map<String, BI_FOCO_Available_Error_Logs__c> newErrorLogIdsMap = new Map<String, BI_FOCO_Available_Error_Logs__c>();

		for(Integer i = 0; i < quantity; i++) {
			errorLogId = new BI_FOCO_Available_Error_Logs__c(Name = 'ERROR_LOG_ID_' + i);
			errorLogIdsList.add(errorLogId);
		}

		try {
			insert errorLogIdsList;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCODeleteErrorLogs_CTRL, line ' + e.getLineNumber() + ': ' + e.getMessage());
		}

		newErrorLogIdsMap = BI_FOCO_Available_Error_Logs__c.getAll();
		System.assertEquals(quantity, newErrorLogIdsMap.size());
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Method for creating pending-for-deletion error log Ids
		IN:				Integer: quantity of error log Ids to be created
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static private void createErrorLogIdsPendingForDeletion(Integer quantity) {
		BI_FOCO_Error_Logs_Pending_Deletion__c errorLogId;
		List<BI_FOCO_Error_Logs_Pending_Deletion__c> errorLogIdsList = new List<BI_FOCO_Error_Logs_Pending_Deletion__c>();
		Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c> newErrorLogIdsMap = new Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c>();

		for(Integer i = 0; i < quantity; i++) {
			errorLogId = new BI_FOCO_Error_Logs_Pending_Deletion__c(Name = 'ERROR_LOG_ID_' + i);
			errorLogIdsList.add(errorLogId);
		}

		try {
			insert errorLogIdsList;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCODeleteErrorLogs_CTRL, line ' + e.getLineNumber() + ': ' + e.getMessage());
		}

		newErrorLogIdsMap = BI_FOCO_Error_Logs_Pending_Deletion__c.getAll();
		System.assertEquals(quantity, newErrorLogIdsMap.size());
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Method for creating multiple error log records that are part of a single error log
		IN:				Integer: quantity of error log Ids to be created
						String: error log Id
		OUT:
		History
		<Date>          <Author>           <Change Description>
		04-Mar-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static private void createErrorLog(Integer quantity, String errorLogId) {
		BI_Registro_Datos_FOCO_Error_Log__c errorLogRecord;
		List<BI_Registro_Datos_FOCO_Error_Log__c> errorLogRecordsList = new List<BI_Registro_Datos_FOCO_Error_Log__c>();
		List<BI_Registro_Datos_FOCO_Error_Log__c> newErrorLogRecordsList = new List<BI_Registro_Datos_FOCO_Error_Log__c>();

		for(Integer i = 0; i < quantity; i++) {
			errorLogRecord = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Identificador_Log__c = errorLogId);
			errorLogRecordsList.add(errorLogRecord);
		}

		try {
			insert errorLogRecordsList;
		}
		catch(Exception e) {
			System.debug('Error in Test Class BI_FOCODeleteErrorLogs_CTRL, line ' + e.getLineNumber() + ': ' + e.getMessage());
		}

		newErrorLogRecordsList = [ SELECT Id FROM BI_Registro_Datos_FOCO_Error_Log__c WHERE BI_Identificador_Log__c = :errorLogId LIMIT :quantity];
		System.assertEquals(quantity, newErrorLogRecordsList.size());
	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}