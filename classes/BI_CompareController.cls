public with sharing class BI_CompareController {
    
    private String s;
    private List<Id> listProdId = new List<Id>(); //Lista di ID dei prodotti
    private Integer nProduct;  //Numero di prodotti da comparare
    public Integer nListOfChar {get;set;}
    
    
    public BI_CompareController(){
        s = System.currentPagereference().getParameters().get('id');
        try{            
            listProdId = s.split(':', 0);
        }
        catch(Exception e){
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Invalid Id');
            ApexPages.addMessage(myMsg);
        }

        //START CRM 10/05/2017 Solve queries With No Where Or Limit Clause
        List<NE__Product__c> prods = [SELECT Id FROM NE__Product__c WHERE Id IN :listProdId];  
        listProdId.clear();   
        for(NE__Product__c p:prods){
            listProdId.add(p.Id);
        }        

        /*******OLD
        //Controlla se i prodotti nella lista esistono
        List<NE__Product__c> prods = [SELECT Id FROM NE__Product__c];
        Set<Id> ids = new Set<Id>();
        for(NE__Product__c p:prods){
            ids.add(p.Id);
        }
        List<Id> existId = new List<Id>();
        for(String l:listProdId){
            if(l != ''){
                if(ids.contains(l)){
                    existId.add(l);
                }
            }
        }
        listProdId = existId;
        */
        //END CRM 10/05/2017 Solve queries With No Where Or Limit Clause
                            
        nProduct = listProdId.size();
        nListOfChar = getListOfCar().size();
    }
    
    //Ritorna una lista con tutte le caratteristiche di tutti i prodotti confrontati
    public List<String> getListOfCar(){     
        List<NE__DocumentationCharacterist__c> dcs = [SELECT Name FROM  NE__DocumentationCharacterist__c WHERE NE__Product__c IN :listProdId ORDER BY NE__Sequence__c, Name ASC NULLS FIRST]; // OI 3.10                
        List<String> listOfCar = new List<String>();
        
        //Aggiunge ad una lista le caratteristiche di TUTTI i prodotti
        for(NE__DocumentationCharacterist__c dc:dcs){
            String s = String.valueOf(dc.get('Name'));
            s = s.substring(0,1).toUpperCase() + s.substring(1, s.length()).toLowerCase();
            listOfCar.add(s);         
        }
        
        //Elimina gli elementi duplicati dalla lista
        Set<String> myset = new Set<String>();
        List<String> result = new List<String>();
        for (String s : listOfCar) {
            if (myset.add(s)) {
                result.add(s);
            }
        }        
        return result;    
    }
    
    public List<NE__DocumentationCharacterist__c> getProduct_1(){
        return genChar(0);
    }
    public List<NE__DocumentationCharacterist__c> getProduct_2(){
        return genChar(1);
    }
    public List<NE__DocumentationCharacterist__c> getProduct_3(){
        return genChar(2);
    }
    public List<NE__DocumentationCharacterist__c> getProduct_4(){
        return genChar(3);
    }
    
    private List<NE__DocumentationCharacterist__c> genChar(Integer n){
        List<NE__DocumentationCharacterist__c> cs = [SELECT NE__Product__c, Name, NE__Value__c, NE__Ranking__c,NE__Visible__c, NE__Sequence__c FROM  NE__DocumentationCharacterist__c WHERE NE__Product__c =: listProdId.get(n) AND NE__Visible__c = true ];
        Map<String, NE__DocumentationCharacterist__c> mcs = new Map<String,NE__DocumentationCharacterist__c>();
        
        for(NE__DocumentationCharacterist__c c:cs){
            String name = c.Name.substring(0,1).toUpperCase() + c.Name.substring(1, c.Name.length()).toLowerCase(); 
            if(!mcs.containsKey(name)){
                mcs.put(name, c);   
            }               
            else{
                mcs.get(name).NE__Value__c = mcs.get(name).NE__Value__c + ', ' + c.NE__Value__c;
                mcs.get(name).NE__Ranking__c = null;
            }
        }
        
        //Aggiunge alla mappa un record se una data caratteristica (presente in altri prodotti) non è presente
        for(String s:getListOfCar()){
            if(!mcs.containsKey(s)){
                NE__DocumentationCharacterist__c dc = new NE__DocumentationCharacterist__c(Name = s, NE__Ranking__c = 0, NE__Value__c = '---');
                mcs.put(s, dc);
            }
        }
        
        //Converte la mappa in lista preservando l'ordinamento delle caratteristiche                
        List<NE__DocumentationCharacterist__c> carProd = new List<NE__DocumentationCharacterist__c>();
        for(String s:getListOfCar()){
            carProd.add(mcs.get(s));    
        }

        return carProd;     
    }
    
    //Metodo che cerca un'immagine per il prodotto nei suoi allegati 
    //(viene invocato dalla pagina VF solo se sono presenti immagini per quel prodotto)
    private String getImagesUrl(Integer n){
        List<NE__Product__c> prods = [SELECT Id, NE__Thumbnail_Image__c FROM NE__Product__c WHERE Id =: listProdId.get(n)];
        String imageUrl = prods.get(0).NE__Thumbnail_Image__c;
        return imageUrl;
    }
    
    private String getProductName(Integer n){
        String prod = [SELECT Name FROM NE__Product__c WHERE Id =: listProdId.get(n)].Name;
        return prod;
    }
    
    //Metodi get
    public Integer getNProduct(){
        return nProduct;
    }
    
    //Metodi set
    public void setListProdId(List<Id> ids){
        listProdId = ids;
    }
    public void setNProduct(){ 
        nProduct = listProdId.size();
    }
    
    private PageReference dettagliProdotto(Integer n) {             
        String newPageUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + listProdId.get(n);    
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage; 
    }
    
    //URL delle immagini dei prodotti
    public String getImageUrlProd_1(){
        return getImagesUrl(0);     
    }
    public String getImageUrlProd_2(){
        return getImagesUrl(1);     
    }
    public String getImageUrlProd_3(){
        return getImagesUrl(2);     
    }
    public String getImageUrlProd_4(){
        return getImagesUrl(3);     
    }
    
    //Nomi dei prodotti
    public String getNameProd_1(){
        return getProductName(0);       
    }
    public String getNameProd_2(){
        return getProductName(1);       
    }
    public String getNameProd_3(){
        return getProductName(2);       
    }
    public String getNameProd_4(){
        return getProductName(3);       
    }
    
    //PageReference alle pagine dettagli dei prodotti
    public PageReference getDettagliProdotto_1(){
        return dettagliProdotto(0); 
    }
    public PageReference getDettagliProdotto_2(){
        return dettagliProdotto(1); 
    }
    public PageReference getDettagliProdotto_3(){
        return dettagliProdotto(2); 
    }
    public PageReference getDettagliProdotto_4(){
        return dettagliProdotto(3); 
    }
    
    public class Caratteristica{
        
        public String nome {get;set;}
        public String valoreProdotto1 {get;set;}
        public String valoreProdotto2 {get;set;}
        public String valoreProdotto3 {get;set;}
        public String valoreProdotto4 {get;set;}
        
        public Caratteristica(String n, String p1, String p2) {
                this.nome = n;
                this.valoreProdotto1 = p1;
                this.valoreProdotto2 = p2;
        }
        
        public Caratteristica(String n, String p1, String p2, String p3) {
                this.nome = n;
                this.valoreProdotto1 = p1;
                this.valoreProdotto2 = p2;
                this.valoreProdotto3 = p3;
        }
        
        public Caratteristica(String n, String p1, String p2, String p3, String p4) {
                this.nome = n;
                this.valoreProdotto1 = p1;
                this.valoreProdotto2 = p2;
                this.valoreProdotto3 = p3;
                this.valoreProdotto4 = p4;
        }
    
    }           
    
    
}