public class TGS_fill_Value_Attribute {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta García
    Company:       Deloitte
    Description:   Field TGS_Value_Attribute fill
    
    History

    <Date>            <Author>                      <Description>
    30/09/2015        Marta García                  Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
    public static void value_attribute (Set<Id> newCI){
        
        if(Constants.firstRunAttribute){
            Constants.firstRunAttribute = false;
            
            String value;
            List<NE__OrderItem__c> listCiUpdate = new List<NE__OrderItem__c>();
        
            for(NE__OrderItem__c cis: [SELECT iD, TGS_Value_Attribute__c, (SELECT TGS_Key_Attribute__c, NE__Value__c FROM NE__Order_Item_Attributes__r WHERE TGS_Key_Attribute__c= true) FROM NE__OrderItem__c WHERE ID in :newCI]){
                
                 value='';
                 for(NE__Order_Item_Attribute__c attributes: cis.NE__Order_Item_Attributes__r){

                    
                         if(attributes.NE__Value__c!=null){
                             value = value + attributes.NE__Value__c + ';';
                         }else{
                             value = value + ' ;';
                         }
                     
                     
                       
                        
                     
                                
                 }
                
               if (String.isNotBlank(value)){
                    cis.TGS_Value_Attribute__c=value;
                    listCiUpdate.add(cis);
                } 
            }
        TGS_CallRodWs.inFutureContextCI = true;
        if (!listCiUpdate.isEmpty()){
            update listCiUpdate;
        }    
        TGS_CallRodWs.inFutureContextCI = false; 
        
    }
    
    
    
    
    
    
    }
        
        
        
}