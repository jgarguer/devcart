@isTest
private class BI_ISC_Edit_CTRL_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ApprovalStatus_CTRL class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    22/05/2014              Ignacio Llorca            Initial version
    18/11/2016              Adrián Caro               Apex CPU time limit exceeded
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static testMethod void assignISCTest() {
        
            Integer K = 25;
        
        Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_ISC2__c']){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }

        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> accountList = BI_DataLoad.loadAccounts(K, lst_pais);
        
        Set<Id> accountSet = new Set<Id>();
        list<BI_ISC2__c> lst_isc = new list<BI_ISC2__c>();
        BI_ISC2__c isc;
        BI_ISC2__c isc_termometro;
        
        for(Account item : accountList){
            accountSet.add(item.Id);
            
            isc = new BI_ISC2__c (BI_Cliente__c = item.Id, 
                                    BI_Fecha__c = Date.today().addDays(-15),
                                    BI_Satisfaccion_general_TEF__c = 9.12,
                                    RecordTypeId = MAP_NAME_RT.get('BI_CORE') );
            lst_isc.add(isc);                                

            isc_termometro = new BI_ISC2__c (BI_Cliente__c = item.Id, 
                        BI_Fecha__c = Date.today().addDays(-16), 
                        BI_CHI_Promedio_del_termometro__c = 2.5,
                        RecordTypeId = MAP_NAME_RT.get('BI_CHI_Termometro'),
                        
                        BI_CHI_10_Proceso_de_facturacion__c = '3.6',
                        BI_CHI_11_Sistema_de_cobranza__c = '3.6',
                        BI_CHI_12_Soporte_tecnico_fijo__c = '3.6',
                        BI_CHI_13_Soporte_tecnico_movil__c = '3.6',
                        BI_CHI_14_Recambio_de_equipos__c = '3.6',
                        BI_CHI_15_Atencion_post_venta_comercial__c = '3.6',
                        BI_CHI_16_Cobertura_movil__c = '3.6',
                        BI_CHI_17_Satisfaccion_general__c = '3.6',
                        
                        BI_CHI_8_Oferta_comercial_entregada__c = '3.6',
                        BI_CHI_9_Proceso_de_instalacion__c = '3.6'
            );
                             lst_isc.add(isc_termometro);           

        }
        insert lst_isc;
        BI_Plan_de_accion__c aPlan = new BI_Plan_de_accion__c(  
                                                      Name = 'Test Action Plan',
                                                      BI_Cliente__c = accountList[0].Id,
                                                      BI_Estado__c = Label.BI_PlanAccion_Estado_En_creacion, 
                                                      BI_Termometro_ISC__c = lst_isc[0].Id
                                                      );
        
       insert aPlan;

        ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_isc[0]);
        BI_ISC_Edit_CTRL class_ctrl = new BI_ISC_Edit_CTRL(ctr);
      
        system.assertequals(class_ctrl.response, 'ok');      
    }
}