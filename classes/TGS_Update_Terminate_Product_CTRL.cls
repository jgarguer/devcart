public with sharing class TGS_Update_Terminate_Product_CTRL {

    /*Page parameters*/
    public String pp_OrderId {get;set;}
    public String pp_OrderItemId {get;set;}

    /*Render controllers*/
    public Boolean bl_RenderUpperBlock {get;set;}
    public Boolean bl_RenderLowerBlock {get;set;}
    public Boolean bl_RenderLowerBlockSite {get;set;}
    public Boolean bl_RenderBSD {get;set;}
    public Boolean bl_RenderBED {get;set;}
    public Boolean bl_RenderFoundation {get;set;}
    public Boolean bl_RenderBilling {get;set;}

    /*Control Variables for Method Execution*/
    public String id_SelectedID {get;set;}
    public String str_SelectedField {get;set;}

    /*Fundation Data Assigner AuxObjects & Structures*/
    public NE__Order__c order {get;set;}
    public NE__Order__c prevorder {get;set;}
    public NE__Order__c orderRTOrder {get;set;}
    public List<NE__Order__c> list_Orders_toUpdate {get;set;}
    public List<NE__OrderItem__c> list_OrderItems_toUpdate {get;set;}

    public Account acc_MainAccount {get;set;}
    public Account acc_ServAccount {get;set;}
    public Account acc_BillAccount {get;set;}
    public BI_Punto_de_instalacion__c acc_Site {get;set;} /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
    public Map<String,Account> map_FoundationAccounts {get;set;}
    public Map<String,BI_Punto_de_instalacion__c> map_FoundationSites {get;set;} /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/

    public List<Account> list_AvailableAccounts {get;set;}
    public List<BI_Punto_de_instalacion__c> list_AvailableSites {get;set;} /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/

    /*Billing Data Assigner Aux Objects*/
    public NE__OrderItem__c orderitem {get;set;}
    public NE__OrderItem__c orderitemRTOrder {get;set;}
    public NE__OrderItem__c prevorderitem {get;set;}

    /*Audit Item Variables*/
    public List<TGS_Audit_Item__c> list_AuditItems {get;set;}

    /*Error Management Variables*/
    public Boolean error {get;set;}
    public String errorMSG {get;set;}
    public String success {get;set;}

    /*Static variables*/
    public static Map<String,Id> map_AccountRT {get;set;}
    public Id rt_Holding {get;set;}
    public Id rt_LegalEntity {get;set;}
    public Id rt_BusinessUnit {get;set;}
    public Id rt_CostCenter {get;set;}
    public Id rt_CustomerCountry {get;set;}

    /*Static block*/
    static
    {
        if(TGS_Update_Terminate_Product_CTRL.map_AccountRT==null)
        {
            TGS_Update_Terminate_Product_CTRL.map_AccountRT = TGS_RecordTypes_Util.getRecordTypeIdsByDeveloperName(Account.SObjectType);
        }
    }
    public TGS_Update_Terminate_Product_CTRL()
    {
        this.rt_Holding         = TGS_Update_Terminate_Product_CTRL.map_AccountRT.get(Constants.RECORD_TYPE_TGS_HOLDING);
        this.rt_LegalEntity     = TGS_Update_Terminate_Product_CTRL.map_AccountRT.get(Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
        this.rt_BusinessUnit    = TGS_Update_Terminate_Product_CTRL.map_AccountRT.get(Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
        this.rt_CostCenter      = TGS_Update_Terminate_Product_CTRL.map_AccountRT.get(Constants.RECORD_TYPE_TGS_COST_CENTER);
        this.rt_CustomerCountry = TGS_Update_Terminate_Product_CTRL.map_AccountRT.get(Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY);

        this.bl_RenderFoundation = false;
        this.bl_RenderBilling = false;

        this.bl_RenderUpperBlock = true;
        this.bl_RenderLowerBlock = false;

        this.list_AuditItems = new List<TGS_Audit_Item__c>();

        this.load();
    }
    public void load() /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
    {
        try
        {
            this.pp_OrderId     = ApexPages.currentPage().getParameters().get('ordId');
            this.pp_OrderItemId = ApexPages.currentPage().getParameters().get('ciId');
            this.error      = false;
            this.errorMSG   = '';
            if(pp_OrderId!=null)
            {
                this.bl_RenderFoundation        = true;
                this.bl_RenderBilling           = false;
                this.list_Orders_toUpdate       = new List<NE__Order__c>();
                this.list_OrderItems_toUpdate   = new List<NE__OrderItem__c>();
                Map<Id,NE__Order__c> map_Orders = new Map<Id,NE__Order__c>();
                map_Orders = new Map<Id,NE__Order__c>(
                    [
                        SELECT  Id, Name, RecordType.DeveloperName, 
                                NE__AccountId__c, NE__AccountId__r.RecordType.DeveloperName,NE__AccountId__r.Name,
                                NE__AccountId__r.TGS_Aux_Customer_Country__c,NE__AccountId__r.TGS_Aux_Holding__c,NE__AccountId__r.TGS_Aux_Legal_Entity__c,NE__AccountId__r.TGS_Aux_Business_Unit__c,
                                NE__ServAccId__c, NE__ServAccId__r.RecordType.DeveloperName,NE__ServAccId__r.Name,
                                NE__ServAccId__r.TGS_Aux_Customer_Country__c,NE__ServAccId__r.TGS_Aux_Holding__c,NE__ServAccId__r.TGS_Aux_Legal_Entity__c,NE__ServAccId__r.TGS_Aux_Business_Unit__c,
                                Cost_Center__c,NE__BillAccId__c, NE__BillAccId__r.RecordType.DeveloperName,NE__BillAccId__r.Name,
                                NE__BillAccId__r.TGS_Aux_Customer_Country__c,NE__BillAccId__r.TGS_Aux_Holding__c,NE__BillAccId__r.TGS_Aux_Legal_Entity__c,NE__BillAccId__r.TGS_Aux_Business_Unit__c,
                                NE__AssetEnterpriseId__c, Case__r.Order__c, NE__Asset__c,Case__r.TGS_Service__c,NE__Contract_Account__c, Case__c, Site__c, Site__r.BI_Cliente__r.Name, 
                                Site__r.BI_Cliente__r.TGS_Aux_Customer_Country__r.Name, Site__r.Name,
                                (
                                    SELECT  Id,
                                            NE__ProdId__c,
                                            NE__AssetItemEnterpriseId__c,
                                            TGS_Service_status__c,NE__Status__c,
                                            NE__Account__c,NE__Asset_Item_Account__c,
                                            NE__Service_Account__c,NE__Service_Account_Asset_Item__c,
                                            NE__Billing_Account__c,NE__Billing_Account_Asset_Item__c,
                                            TGS_billing_start_date__c, TGS_Billing_end_date__c,
                                            TGS_CSUID1__c, TGS_CSUID1_Old__c, TGS_CSUID2__c, TGS_CSUID2_Old__c
                                    FROM    NE__Order_Items__r
                                    WHERE   NE__Status__c != 'Cancelled'
                                            AND NE__Status__c != 'Disconnected'
                                            AND NE__Status__c != null
                                )
                        FROM    NE__Order__c 
                        WHERE   Id = :this.pp_OrderId
                                OR Case__r.Asset__c = :this.pp_OrderId
                    ]);
                this.order                  = map_Orders.get(this.pp_OrderId);
                this.list_AvailableAccounts = new List<Account>();
                this.list_AvailableSites = new List<BI_Punto_de_instalacion__c>();
                this.map_FoundationAccounts = new Map<String, Account>();
                this.map_FoundationSites = new Map<String, BI_Punto_de_instalacion__c>();
                for(Account ACC :
                    [
                    SELECT  Id,Name,TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c,TGS_Aux_Legal_Entity__c,TGS_Aux_Business_Unit__c 
                    FROM    Account
                    WHERE   Id = :this.order.NE__AccountId__c
                            OR Id = :this.order.NE__ServAccId__c
                            OR Id = :this.order.NE__BillAccId__c
                    ])
                {
                    if      (ACC.Id == this.order.NE__AccountId__c){map_FoundationAccounts.put('main',ACC);}
                    else if (ACC.Id == this.order.NE__ServAccId__c){map_FoundationAccounts.put('serv',ACC);}
                    else if (ACC.Id == this.order.NE__BillAccId__c){map_FoundationAccounts.put('bill',ACC);}
                }
                for(BI_Punto_de_instalacion__c SITE :
                    [
                    SELECT Id, Name, BI_Cliente__c, BI_Cliente__r.Name, BI_Cliente__r.TGS_Aux_Customer_Country__c, BI_Cliente__r.TGS_Aux_Customer_Country__r.Name
                    FROM BI_Punto_de_instalacion__c
                    WHERE Id = :this.order.Site__c
                    ])
                {
                    map_FoundationSites.put('site', SITE);
                }

                this.acc_MainAccount = map_FoundationAccounts.get('main');
                this.acc_ServAccount = map_FoundationAccounts.get('serv');
                this.acc_BillAccount = map_FoundationAccounts.get('bill');
                this.acc_Site = map_FoundationSites.get('site');
                this.prevorder = new NE__Order__c(
                    NE__AccountId__c = this.order.NE__AccountId__c,
                    NE__ServAccId__c = this.order.NE__ServAccId__c,
                    NE__BillAccId__c = this.order.NE__BillAccId__c,
                    Site__c = this.order.Site__c,
                    NE__AssetEnterpriseId__c = this.order.NE__AssetEnterpriseId__c
                    );
                this.orderRTOrder = map_Orders.get(this.order.NE__AssetEnterpriseId__c);
            }
            else
            {
                this.bl_RenderFoundation = false;
                this.bl_RenderBilling = true;
                this.orderitem =
                    [
                        SELECT  Id, Name,
                        NE__ProdId__c,
                        NE__Parent_Order_Item__c,
                        TGS_Service_status__c,NE__Status__c,
                        TGS_billing_start_date__c, TGS_Billing_end_date__c,
                        NE__AssetItemEnterpriseId__c, NE__OrderId__r.RecordType.DeveloperName, NE__OrderId__r.NE__AssetEnterpriseId__c,
                        NE__OrderId__r.Case__r.TGS_Service__c, NE__OrderId__r.NE__Asset__c,
                        TGS_CSUID1__c, TGS_CSUID1_Old__c, TGS_CSUID2__c, TGS_CSUID2_Old__c,
                        NE__Account__c,NE__Asset_Item_Account__c,
                        NE__Service_Account__c,NE__Service_Account_Asset_Item__c,
                        NE__Billing_Account__c,NE__Billing_Account_Asset_Item__c,
                        NE__OrderId__r.Case__r.Type /*Álvaro López 30/06/2017 - Added case type*/
                        FROM    NE__OrderItem__c 
                        WHERE   Id = :this.pp_OrderItemId
                        LIMIT 1
                       ];
                this.prevorderitem = new NE__OrderItem__c(
                    TGS_Billing_end_date__c     = this.orderitem.TGS_Billing_end_date__c,
                    TGS_billing_start_date__c   = this.orderitem.TGS_billing_start_date__c
                );
                this.loader_RenderValues_BillingDatesInputs();
            }
        }
        catch(Exception exc)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exc.getMessage()));
        }
    }
    public PageReference save()
    {
        PageReference reference;
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(this.pp_OrderId!=null)
            {
                Set<Id> set_IDS_ToRoD = new Set<Id>();
                if( this.prevorder.NE__AccountId__c != this.acc_MainAccount.Id
                    || this.prevorder.NE__ServAccId__c != this.acc_ServAccount.Id
                    || this.prevorder.NE__BillAccId__c != this.acc_BillAccount.Id
                    || this.prevorder.Site__c != this.acc_Site.Id)
                {
                    this.assigner_FundationData(set_IDS_ToRoD);
                    System.debug('AUDITITEMS ' + list_AuditItems);
                    if(!this.list_AuditItems.isEmpty())
                    {
                        update this.list_Orders_toUpdate;
                        update this.list_OrderItems_toUpdate;
                        // ALM3897 - 26-07-2017 - Manuel Ochoa - Added code to update accountId in cases
                        Case caseToUpd = new Case(
                            id = order.Case__c,
                            AccountId = order.NE__AccountId__c
                            );
                        update caseToUpd;                        
                        // ALM3897 - END
                        TGS_CallRodWs.invokeWebServiceOrderItemUpdate(set_IDS_ToRoD,true);
                        System.debug('Audit Item 2: ' + list_AuditItems);
                        insert this.list_AuditItems;
                        reference = new PageReference('/' + this.pp_OrderId);
                        reference.setRedirect(true);
                        return reference;
                    }
                    else
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,this.errorMSG));
                        return null;
                    }
                }
                else
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Los datos no han variado.'));
                    return null;
                }
            }
            else if(this.pp_OrderItemId!=null)
            {
                Set<Id> set_IDS_ToRoD = new Set<Id>();
                this.list_OrderItems_toUpdate = new List<NE__OrderItem__c>();
                if( this.prevorderitem.TGS_Billing_start_date__c != this.orderitem.TGS_Billing_start_date__c
                    || this.prevorderitem.TGS_Billing_end_date__c != this.orderitem.TGS_Billing_end_date__c)
                {
                    this.assigner_BillingData(set_IDS_ToRoD);
                    if(!this.list_AuditItems.isEmpty())
                    {
                        update this.list_OrderItems_toUpdate;
                        TGS_CallRodWs.invokeWebServiceOrderItemUpdate(set_IDS_ToRoD,true);
                        insert this.list_AuditItems;
                        reference = new PageReference('/' + this.pp_OrderItemId);
                        reference.setRedirect(true);
                        return reference;
                    }
                    else
                    {
                        System.debug('SYRA');
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,this.errorMSG));
                        return null;
                    }
                }
                else
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No ha modificado ningún valor'));
                    return null;
                }

            }
        }
        catch(Exception exc)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,exc.getMessage()));
            Database.rollback(sp);
            /* Álvaro López 26/07/2017 - Clear lists when something is wrong to avoid duplicated id in list */
            this.list_Orders_toUpdate.clear();
            this.list_OrderItems_toUpdate.clear();
            this.list_AuditItems.clear();
        }
        return null;
    }
    public PageReference cancel()
    {
        PageReference pag;
        if(this.pp_OrderId!= null){
            pag = new PageReference('/' + this.pp_OrderId);
        }
        else{
            pag = new PageReference('/' + this.pp_OrderItemId);
        }
        pag.setRedirect(true);
        return pag;
    }
    public static Boolean assigner_ContractAccount(NE__Order__c order, Account mainAccount)
    {
        List<NE__Contract_Account_Association__c> list_CAA = new List<NE__Contract_Account_Association__c>();
        list_CAA =  [
                        SELECT  Id
                        FROM    NE__Contract_Account_Association__c
                        WHERE   NE__Account__c = :mainAccount.Id
                    ];
        if(!list_CAA.isEmpty())
        {
            order.NE__Contract_Account__c = list_CAA[0].Id;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.TGS_FundationDataAssigner_ContractAccount_Success));
            return true;
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.TGS_FundationDataAssigner_ContractAccount_Error));
            return false;
        }
    }
    public void loader_AvailableAccounts()
    {
        List<Account> list_Accounts = new List<Account>();
        List<BI_Punto_de_instalacion__c> list_Sites = new List<BI_Punto_de_instalacion__c>();
        if(this.str_SelectedField.equalsIgnoreCase('main'))
        {
            list_Accounts =    
            [
                SELECT  Id,Name,
                        Owner.Name,
                        Parent.Name,
                        BI_Tipo_de_cuenta__c,
                        BI_No_Identificador_fiscal__c
                FROM    Account
                WHERE   TGS_Es_MNC__c = true
                        AND
                        (RecordTypeId   = :rt_Holding
                        OR RecordTypeId = :rt_LegalEntity
                        OR RecordTypeId = :rt_BusinessUnit)
                        AND
                        (TGS_Aux_Holding__c = :this.acc_MainAccount.TGS_Aux_Holding__c
                        OR TGS_Aux_Holding__c = :this.acc_MainAccount.Id)
                        AND TGS_Aux_Customer_Country__c = :this.acc_MainAccount.TGS_Aux_Customer_Country__c
            ];
            System.debug('M');
        }
        else if(this.str_SelectedField.equalsIgnoreCase('serv'))
        {
            list_Accounts =
            [
                SELECT  Id,Name,
                        Owner.Name,
                        Parent.Name,
                        BI_Tipo_de_cuenta__c,
                        BI_No_Identificador_fiscal__c
                FROM    Account
                WHERE   TGS_Es_MNC__c = true
                        AND RecordTypeId = :rt_BusinessUnit
                        AND 
                        (
                            (TGS_Aux_Holding__c = :this.acc_MainAccount.TGS_Aux_Holding__c
                            AND TGS_Aux_Customer_Country__c = :this.acc_MainAccount.TGS_Aux_Customer_Country__c
                            AND TGS_Aux_Legal_Entity__c = :this.acc_MainAccount.Id)
                            OR
                            (TGS_Aux_Holding__c = :this.acc_MainAccount.TGS_Aux_Holding__c
                            AND TGS_Aux_Customer_Country__c = :this.acc_MainAccount.TGS_Aux_Customer_Country__c
                            AND TGS_Aux_Legal_Entity__c = :this.acc_MainAccount.TGS_Aux_Legal_Entity__c)
                        )
            ];
            System.debug('S');
        }
        else if(this.str_SelectedField.equalsIgnoreCase('bill'))
        {
            list_Accounts = 
            [
                SELECT  Id,Name,
                        Owner.Name,
                        Parent.Name,
                        BI_Tipo_de_cuenta__c,
                        BI_No_Identificador_fiscal__c
                FROM    Account
                WHERE   TGS_Es_MNC__c = true
                        AND RecordTypeId = :rt_CostCenter
                        AND 
                        (
                            (TGS_Aux_Holding__c = :this.acc_MainAccount.TGS_Aux_Holding__c
                            AND TGS_Aux_Customer_Country__c = :this.acc_MainAccount.TGS_Aux_Customer_Country__c
                            AND TGS_Aux_Holding__c = :this.acc_ServAccount.TGS_Aux_Holding__c
                            AND TGS_Aux_Customer_Country__c = :this.acc_ServAccount.TGS_Aux_Customer_Country__c
                            AND TGS_Aux_Business_Unit__c = :this.acc_ServAccount.Id)
                        )
            ];
            System.debug('B');
        }
        else if(this.str_SelectedField.equalsIgnoreCase('site')) /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
        {
            list_Sites = 
            [
                SELECT  Id,Name,BI_Cliente__r.TGS_Aux_Customer_Country__r.Name, BI_Cliente__r.Name, BI_Sede__r.Name /*Álvaro López 28/11/2017 - Added BI_Sede__r*/
                FROM    BI_Punto_de_instalacion__c
                WHERE   BI_Cliente__r.TGS_Es_MNC__c = true
                        AND BI_Cliente__r.TGS_Aux_Customer_Country__c = :this.acc_MainAccount.TGS_Aux_Customer_Country__c
                        AND BI_Cliente__r.TGS_Aux_Holding__c = :this.acc_ServAccount.TGS_Aux_Holding__c
                        
            ];
            System.debug('SITE:'+list_Sites);   
        }
        if(!list_Accounts.isEmpty() || Test.isRunningTest())
        {
            this.list_AvailableAccounts = list_Accounts;
            this.bl_RenderLowerBlock = true;
        }
        if(!list_Sites.isEmpty() || Test.isRunningTest())
        {
            this.list_AvailableSites = list_Sites;
            this.bl_RenderLowerBlockSite = true;
        }
    }
    public void selectAccount()
    {
        if(this.str_SelectedField.equalsIgnoreCase('main'))
        {
            this.order.NE__AccountId__c = this.id_SelectedID;
            System.debug('Id selected 1: ' + this.id_SelectedID);
            this.acc_MainAccount =
            [
                SELECT  Id,Name,
                        TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c,TGS_Aux_Legal_Entity__c,TGS_Aux_Business_Unit__c
                FROM    Account
                WHERE   Id = :this.id_SelectedID
            ];
            this.bl_RenderLowerBlock = false;
            this.bl_RenderUpperBlock = true;
        }
        else if(this.str_SelectedField.equalsIgnoreCase('serv'))
        {
            this.order.NE__ServAccId__c = this.id_SelectedID;
            System.debug('Id selected 2: ' + this.id_SelectedID);
            this.acc_ServAccount =
            [
                SELECT  Id,Name,
                        TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c,TGS_Aux_Legal_Entity__c,TGS_Aux_Business_Unit__c
                FROM    Account
                WHERE   Id = :this.id_SelectedID
            ];
            this.bl_RenderLowerBlock = false;
            this.bl_RenderUpperBlock = true;
        }
        else if(this.str_SelectedField.equalsIgnoreCase('bill'))
        {
            this.order.NE__BillAccId__c = this.id_SelectedID;
            System.debug('Id selected 3: ' + this.id_SelectedID);
            this.acc_BillAccount =
            [
                SELECT  Id,Name,
                        TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c,TGS_Aux_Legal_Entity__c,TGS_Aux_Business_Unit__c
                FROM    Account
                WHERE   Id = :this.id_SelectedID
            ];
            this.bl_RenderLowerBlock = false;
            this.bl_RenderUpperBlock = true;
        }
        else if(this.str_SelectedField.equalsIgnoreCase('site')) /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
        {
            this.order.Site__c = this.id_SelectedID;
            System.debug('Id selected 4: ' + this.id_SelectedID);
            this.acc_Site =
            [
                SELECT  Id,Name,
                        BI_Cliente__c, BI_Cliente__r.TGS_Aux_Holding__c,BI_Cliente__r.TGS_Aux_Customer_Country__c
                FROM    BI_Punto_de_instalacion__c
                WHERE   Id = :this.id_SelectedID
            ];
            this.bl_RenderLowerBlock = false;
            this.bl_RenderUpperBlock = true;
        }
        
    }
    public void cancelSelection()
    {
        this.list_AvailableAccounts = new List<Account>();
        this.list_AvailableSites = new List<BI_Punto_de_instalacion__c>(); /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
        this.bl_RenderLowerBlock = false;
    }
    public String validateBillingDates()
    {
        /*Álvaro López 30/06/2017 - Added condition for mandatory BED in disconnected CIs and termination cases*/
        if((this.orderitem.NE__OrderId__r.Case__r.Type == Constants.TYPE_DISCONNECT || (this.orderitem.NE__Status__c == 'Disconnected' && this.orderitem.TGS_Service_status__c == 'Deleted')) && this.orderitem.TGS_Billing_end_date__c == null){
            return 'Disconnected CIs must be dated. Please fill the BED.';
        }
        /*MGC 08/08/17 - ALM3707 Added condition BED greater than BSD*/
        if(this.orderitem.TGS_billing_start_date__c > this.orderitem.TGS_billing_end_date__c)
        {
            return 'The BED must be later than the BSD.';
        }        
        if(this.orderitem.NE__Parent_Order_Item__c == null)
        {
            List<NE__OrderItem__c> list_OrderItems = new List<NE__OrderItem__c>();
            list_OrderItems =    
                [
                    SELECT  Id,
                            TGS_Billing_end_date__c,
                            TGS_billing_start_date__c
                    FROM    NE__OrderItem__c 
                    WHERE   NE__Parent_Order_Item__c = :this.orderitem.Id
                ];
            if(!list_OrderItems.isEmpty())
            {
                for(NE__OrderItem__c OI : list_OrderItems)
                {
                    if(OI.TGS_billing_start_date__c < this.orderitem.TGS_billing_start_date__c)
                    {
                        return 'The BSD is later than at least one of the child products.';
                    }
                    else if(OI.TGS_Billing_end_date__c > this.orderitem.TGS_Billing_end_date__c)
                    {
                        return 'The BED is prior to the BED of at least one of the child products.';
                    }
                }
            }
        }
        else if(this.orderitem.NE__Parent_Order_Item__c != null)
        {
            Map<Id,NE__OrderItem__c> map_OrderItems = new Map<Id,NE__OrderItem__c>();
            map_OrderItems = new Map<Id,NE__OrderItem__c>(   
                [
                    SELECT  Id,
                            TGS_Billing_end_date__c,
                            TGS_billing_start_date__c
                    FROM    NE__OrderItem__c 
                    WHERE   Id = :this.orderitem.NE__Parent_Order_Item__c
                    LIMIT   1
                ]);
            NE__OrderItem__c parentOrderItem = map_OrderItems.get(this.orderitem.NE__Parent_Order_Item__c);
                if(this.orderitem.TGS_billing_start_date__c < parentOrderItem.TGS_billing_start_date__c)
                {
                    return 'The BSD is prior to the BSD of the parent product.';
                }
                else if(this.orderitem.TGS_Billing_end_date__c > parentOrderItem.TGS_Billing_end_date__c)
                {
                    return 'The BED is later than the BED of the parent product.';
                }
        }
        return 'correct';
    }  
    /* Álvaro López 26/07/2017 - Fix assigner_FundationData method */
    public void assigner_FundationData(Set<Id> set_IDS_ToRoD)
    {
        System.debug('Error antes asset'+this.error);
        if(!assigner_ContractAccount(this.order,this.acc_MainAccount)) this.error = true;
        this.list_Orders_toUpdate.add(this.order);
        System.debug('Cuentas: ' + this.order.NE__AccountId__c + ' - ' + this.order.NE__ServAccId__c + ' - ' + this.order.NE__BillAccId__c);
        this.orderRTOrder.NE__AccountId__c = this.order.NE__AccountId__c;
        this.orderRTOrder.NE__ServAccId__c = this.order.NE__ServAccId__c;
        this.orderRTOrder.NE__BillAccId__c = this.order.NE__BillAccId__c;
        this.orderRTOrder.Site__c = this.order.Site__c; /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
        // ALM3900 - 27-07-2016 - Manuel Ochoa - Cost center update fix
        this.orderRTOrder.Cost_Center__c = this.order.NE__BillAccId__c;
        // ALM3900 -  END
        System.debug('Error antes order'+this.error);
        if(!assigner_ContractAccount(this.orderRTOrder,this.acc_MainAccount)) this.error = true;
        this.list_Orders_toUpdate.add(this.orderRTOrder);
        System.debug('Orders to update: '+list_Orders_toUpdate);
        System.debug('Audititems to update: '+list_Orders_toUpdate);
        for(NE__OrderItem__c OI : this.order.NE__Order_Items__r)
        {
            //MGC 10/08/17 ALM3931 Corrección cambio en cis de order
            if(set_IDS_ToRoD.contains(OI.NE__AssetItemEnterpriseId__c)){
                this.list_AuditItems.add(new TGS_Audit_Item__c(
                                        TGS_CreationDate__c             = Datetime.now(),
                                        TGS_User__c                     = UserInfo.getUserId(),
                                        TGS_Status__c                   = OI.NE__Status__c,
                                        TGS_Service_Status__c           = OI.TGS_Service_status__c,
                                        TGS_CSUID1__c                   = OI.TGS_CSUID1__c,
                                        TGS_CSUID1_Old__c               = OI.TGS_CSUID1_Old__c,
                                        TGS_CSUID2__c                   = OI.TGS_CSUID2__c,
                                        TGS_CSUID2_Old__c               = OI.TGS_CSUID2_Old__c,
                                        TGS_Account__c                  = OI.NE__Account__c,
                                        TGS_Configuration_Item__c       = OI.Id,
                                        TGS_ProdId__c                   = OI.NE__ProdId__c,
                                        TGS_Account_new__c              = this.order.NE__AccountId__c,
                                        TGS_Service_Account__c          = OI.NE__Service_Account__c,
                                        TGS_Service_Account_new__c      = this.order.NE__ServAccId__c,
                                        TGS_Billing_Account__c          = OI.NE__Billing_Account__c,
                                        TGS_Billing_Account_new__c      = this.order.NE__BillAccId__c,
                                        TGS_Billing_end_date_new__c     = OI.TGS_Billing_end_date__c,
                                        TGS_Billing_end_date_old__c     = OI.TGS_Billing_end_date__c,
                                        TGS_billing_start_date_new__c   = OI.TGS_billing_start_date__c,
                                        TGS_Billing_StartDate_old__c    = OI.TGS_billing_start_date__c
                                    ));
            //MGC 10/08/17 ALM3931 Corrección cambio en cis de order
            }
            OI.NE__Account__c                       = this.order.NE__AccountId__c;
            OI.NE__Asset_Item_Account__c            = this.order.NE__AccountId__c;
            OI.NE__Service_Account__c               = this.order.NE__ServAccId__c;
            OI.NE__Service_Account_Asset_Item__c    = this.order.NE__ServAccId__c;
            OI.NE__Billing_Account__c               = this.order.NE__BillAccId__c;
            OI.NE__Billing_Account_Asset_Item__c    = this.order.NE__BillAccId__c;
            OI.Installation_point__c                = this.order.Site__c; /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
            set_IDS_ToRoD.add(OI.Id); 
                       
        }
        this.list_OrderItems_toUpdate.addAll(this.order.NE__Order_Items__r);
        for(NE__OrderItem__c OI : this.orderRTOrder.NE__Order_Items__r)
        {
            //MGC 10/08/17 ALM3931 Corrección cambio en cis de order
            if(set_IDS_ToRoD.contains(OI.NE__AssetItemEnterpriseId__c))
            {
                this.list_AuditItems.add(new TGS_Audit_Item__c(
                                        TGS_CreationDate__c             = Datetime.now(),
                                        TGS_User__c                     = UserInfo.getUserId(),
                                        TGS_Status__c                   = OI.NE__Status__c,
                                        TGS_Service_Status__c           = OI.TGS_Service_status__c,
                                        TGS_CSUID1__c                   = OI.TGS_CSUID1__c,
                                        TGS_CSUID1_Old__c               = OI.TGS_CSUID1_Old__c,
                                        TGS_CSUID2__c                   = OI.TGS_CSUID2__c,
                                        TGS_CSUID2_Old__c               = OI.TGS_CSUID2_Old__c,
                                        TGS_Account__c                  = OI.NE__Account__c,
                                        TGS_Configuration_Item__c       = OI.Id,
                                        TGS_ProdId__c                   = OI.NE__ProdId__c,
                                        TGS_Account_new__c              = this.orderRTOrder.NE__AccountId__c,
                                        TGS_Service_Account__c          = OI.NE__Service_Account__c,
                                        TGS_Service_Account_new__c      = this.orderRTOrder.NE__ServAccId__c,
                                        TGS_Billing_Account__c          = OI.NE__Billing_Account__c,
                                        TGS_Billing_Account_new__c      = this.orderRTOrder.NE__BillAccId__c,
                                        TGS_Billing_end_date_new__c     = OI.TGS_Billing_end_date__c,
                                        TGS_Billing_end_date_old__c     = OI.TGS_Billing_end_date__c,
                                        TGS_billing_start_date_new__c   = OI.TGS_billing_start_date__c,
                                        TGS_Billing_StartDate_old__c    = OI.TGS_billing_start_date__c
                                    ));
            //MGC 10/08/17 ALM3931 Corrección cambio en cis de order
            }
            OI.NE__Account__c                       = this.orderRTOrder.NE__AccountId__c;
            OI.NE__Service_Account__c               = this.orderRTOrder.NE__ServAccId__c;
            OI.NE__Billing_Account__c               = this.orderRTOrder.NE__BillAccId__c;
            OI.Installation_point__c                = this.order.Site__c; /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
            set_IDS_ToRoD.add(OI.Id);         
        }
        System.debug('Error'+this.error);
        System.debug('OUPD'+this.list_OrderItems_toUpdate);
        System.debug('ASDF'+this.orderRTOrder.NE__Order_Items__r);
        this.list_OrderItems_toUpdate.addAll(this.orderRTOrder.NE__Order_Items__r);
        if(this.error)
        {
            this.list_AuditItems = new List<TGS_Audit_Item__c>();
            this.errorMSG = 'There\'s no contract available for your selection.';
            /* Álvaro López 26/07/2017 - Refreshing the show error controller and lists */
            this.error = false; 
            this.list_Orders_toUpdate.clear();
            this.list_OrderItems_toUpdate.clear();
            this.list_AuditItems.clear();
        }
    }
    public void assigner_BillingData(Set<Id> set_IDS_ToRoD)
    {
        List<TGS_Audit_Item__c> list_AuditItems = new List<TGS_Audit_Item__c>();
        String result = this.validateBillingDates();
        if(result.equalsIgnoreCase('correct'))
        {
            this.orderitemRTOrder = [SELECT Id, TGS_Billing_end_date__c, TGS_billing_start_date__c FROM NE__OrderItem__c WHERE Id = :this.orderitem.NE__AssetItemEnterpriseId__c];
            this.orderitemRTOrder.TGS_Billing_end_date__c   = this.orderitem.TGS_Billing_end_date__c;
            this.orderitemRTOrder.TGS_Billing_start_date__c = this.orderitem.TGS_Billing_start_date__c;
            this.list_OrderItems_toUpdate.add(this.orderitem);
            this.list_OrderItems_toUpdate.add(this.orderitemRTOrder);

            TGS_Audit_Item__c auditItem_order = new TGS_Audit_Item__c(
                TGS_Status__c                   = this.orderitem.NE__Status__c,
                TGS_Service_Status__c           = this.orderitem.TGS_Service_status__c,
                TGS_CreationDate__c             = Datetime.now(),
                TGS_User__c                     = UserInfo.getUserId(),
                TGS_ProdId__c                   = this.orderitem.NE__ProdId__c,
                TGS_Billing_end_date_new__c     = this.orderitem.TGS_Billing_end_date__c,
                TGS_Billing_end_date_old__c     = this.prevorderitem.TGS_Billing_end_date__c,
                TGS_billing_start_date_new__c   = this.orderitem.TGS_billing_start_date__c,
                TGS_Billing_StartDate_old__c    = this.prevorderitem.TGS_billing_start_date__c,
                TGS_Configuration_Item__c       = this.orderitemRTOrder.Id,
                TGS_CSUID1__c                   = this.orderitem.TGS_CSUID1__c,
                TGS_CSUID1_Old__c               = this.orderitem.TGS_CSUID1_Old__c,
                TGS_CSUID2__c                   = this.orderitem.TGS_CSUID2__c,
                TGS_CSUID2_Old__c               = this.orderitem.TGS_CSUID2_Old__c,
                TGS_Account__c                  = this.orderitem.NE__Account__c,
                TGS_Account_new__c              = this.orderitem.NE__Account__c,
                TGS_Service_Account__c          = this.orderitem.NE__Service_Account__c,
                TGS_Service_Account_new__c      = this.orderitem.NE__Service_Account__c,
                TGS_Billing_Account__c          = this.orderitem.NE__Billing_Account__c,
                TGS_Billing_Account_new__c      = this.orderitem.NE__Billing_Account__c
            );
            this.list_AuditItems.add(auditItem_order);
        }
        else
        {
            this.list_AuditItems = new List<TGS_Audit_Item__c>();
            this.errorMSG = result;
            this.list_AuditItems.clear();
        }
    }
    public void loader_RenderValues_BillingDatesInputs()
    {
        if(this.orderitem.TGS_Service_status__c == 'Received' || this.orderitem.TGS_Service_status__c == 'Deployed')
        {
            this.bl_RenderBSD = true;
        }
        else if(this.orderitem.TGS_Service_status__c == 'Deleted')
        {
            this.bl_RenderBED = true;
            this.bl_RenderBSD = true;
        }
    }
    public void loadMainAccounts()
    {
        this.str_SelectedField = 'main';
        this.loader_AvailableAccounts();
        this.bl_RenderLowerBlockSite = false;
    }
    public void loadServAccounts()
    {
        this.str_SelectedField = 'serv';
        this.loader_AvailableAccounts();
        this.bl_RenderLowerBlockSite = false;
    }
    public void loadBillAccounts()
    {
        this.str_SelectedField = 'bill';
        this.loader_AvailableAccounts();
        this.bl_RenderLowerBlockSite = false;
    }
    public void loadSites() /*Álvaro López 24/11/2017 - Added Site change to Fundation Data Assigner*/
    {
        this.str_SelectedField = 'site';
        this.loader_AvailableAccounts();
        this.bl_RenderLowerBlock = false; 
    }
}