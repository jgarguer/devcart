@isTest
private class BI_CHI_Formulario_despacho_CTRL_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alberto Fernández
Company:       Accenture
Description:   Test class to manage the coverage code for BI_CHI_Formulario_despacho_CTRL class

History: 

<Date>                  <Author>                <Change Description>
07/04/2017              Alberto Fernández        Initial Version
19/09/2017        		Angel F. Santaliestra 	 Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest
	static void constructor_TEST() {

		Account testAcc = new Account(Name = 'testAcc',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Country__c = 'Chile',
                                  BI_Segment__c = 'Soporte a la Venta',
								  BI_Subsegment_Regional__c = 'test',
								  BI_Territory__c = 'test',
                                  BI_Subsegment_local__c = 'Top 1'
                                  );
		insert testAcc;

		List<Account> lst_acc = new List<Account>();
		lst_acc.add(testAcc);

		List<NE__OrderItem__c> lst_oi = BI_DataLoad.loadOrderItem(1, testAcc);

		List<RecordType> lst_rt = new List<RecordType>();
		RecordType rt = [SELECT id FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' LIMIT 1];
		lst_rt.add(rt);

		List<Case> lst_case = BI_DataLoad.loadCases(lst_acc, null, lst_rt);

		BI_CHI_Formulario_despacho_CTRL formulario_ctrl = new BI_CHI_Formulario_despacho_CTRL();
		formulario_ctrl.caseId = lst_case[0].Id;
		formulario_ctrl.getData();

	}

}