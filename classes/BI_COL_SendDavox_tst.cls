@isTest
private class BI_COL_SendDavox_tst {
    public static Opportunity                                       objOppty;

    public static Account                                           objCuenta;

    public static List <BI_COL_Modificacion_de_Servicio__c>          lstModSer = new List <BI_COL_Modificacion_de_Servicio__c>();
    public static BI_COL_Modificacion_de_Servicio__c                objModSer;

    public static List<BI_COL_Descripcion_de_servicio__c>           lstDesSer;
    public static BI_COL_Descripcion_de_servicio__c                 objDesSer;
    public static BI_COL_Seguimiento_Quiebre__c      objSegMien;
    public static BI_Log__c           objLog;

    public static List<RecordType>                                  rtBI_FUN;
    public static List<String>          lstIdquiebre = new List<String>();
    public static String Enviada;
    public static String Respuesta;
    public static list <UserRole> lstRoles;
    public static User objUsuario;
    public static list<User> lstUsuarios;


    static void crearData() {

        BI_bypass__c objBibypass = new BI_bypass__c();
        objBibypass.BI_migration__c = true;
        insert objBibypass;

        objCuenta                     = new Account();
        objCuenta.Name                  = 'prueba';
        objCuenta.BI_Country__c             = 'Colombia';
        objCuenta.TGS_Region__c             = 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c  = 'NIT';
        objCuenta.CurrencyIsoCode             = 'GTQ';
        //20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            //BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
        objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';                                   
         //END
        insert objCuenta;
        System.debug('\n\n\n ======= CUENTA ======\n ' + objCuenta );
        objOppty                    = new Opportunity();
        objOppty.Name                   = 'TEST AVANXO OPPTY';
        objOppty.AccountId                = objCuenta.Id;
        //objOppty.TGS_Region__c            = 'América';
        objOppty.BI_Country__c              = 'Colombia';
        objOppty.CloseDate                = system.today().addDays(1);
        objOppty.StageName                = 'F6 - Prospecting';
        objOppty.BI_Ciclo_ventas__c           = Label.BI_Completo;
        insert objOppty;
        objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
        objDesSer.BI_COL_Oportunidad__c                     = objOppty.Id;
        //objDesSer.CurrencyIsoCode                           = 'COP';
        insert objDesSer;
        System.debug('\n\n\n ======= objDesSer ' + objDesSer);
        objModSer                                   = new BI_COL_Modificacion_de_Servicio__c();
        objModSer.BI_COL_Codigo_unico_servicio__c   = objDesSer.Id;
        objModSer.BI_COL_Clasificacion_Servicio__c  = 'ALTA';
        objModSer.BI_COL_Estado__c                  = 'Pendiente';
        objModSer.BI_COL_Fecha_instalacion_servicio_RFS__c  = system.today().addDays(1);
        objModSer.BI_COL_Oportunidad__c             = objOppty.Id;
        //objModSer.BI_COL_Direccion_IP__c            = i+'';
        //insert objModSer;
        lstModSer.add(objModSer);
        insert lstModSer;
        System.debug('\n\n\n ======= lstModSer ' + lstModSer);
        objSegMien          = new BI_COL_Seguimiento_Quiebre__c();
        objSegMien.BI_COL_Observaciones__c   = 'Prueba avanxo';
        objSegMien.BI_COL_Modificacion_de_Servicio__c = lstModSer[0].Id;
        objSegMien.BI_COL_Tipo_gestion__c   = 'Informativo';
        objSegMien.BI_COL_Transaccion__c   = null;
        objSegMien.BI_COL_Aplazar_MS__c    = false;
        objSegMien.BI_COL_Codigo_Quiebre_Sisgot__c = 'Fallido Conexion';
        objSegMien.BI_COL_Estado_de_Gestion__c  = 'Abierto';
        objSegMien.BI_COL_Estado_envio__c   = 'Creado SFDC';
        insert objSegMien;
        lstIdquiebre.add(objSegMien.Id);

        objLog              = new BI_Log__c();
        objLog.BI_COL_Informacion_recibida__c      = 'OK, Respuesta Procesada';
        //objLog.BI_COL_Contacto__c          = lstContacts[0].Id;
        objLog.BI_COL_Estado__c          = 'Pendiente';
        objLog.BI_COL_Interfaz__c         = 'NOTIFICACIONES';
        objLog.BI_COL_Informacion_Enviada__c      = 'GALLITO CANTOR S.A.S.ÕÕTR 60 114A 55ÕÕ7754676ÕÕNIÕJUÕ11001000ÕÕ0ÕGALLITO CANTOR S.A.S.Õa27g0000002itNcAAIÕCL 54 KR 23 65ÕNAÕ08001000Õ12345ÕEMPRESASÕMultinacionalesÕNO APLICAÕMAURICIO RODRIGUEZ GONZALEZÕa3yg00000005qGkAAIÕmauricio.rodriguezg@telefonica.comÕ3157801881Õ17754676ÕDTÕa3yg00000005qGkAAIÕ00180677Õ890500650ÕALTAÕ08/01/2015Õ12Õ08/31/2015ÕINTE125ÕDS-994759Õa27g0000002itNcAAIÕSEDE BARRANQUILLAÕCL 54 KR 23 65Õ08001000ÕPesoÕ0Õ460000ÕÕÕNÕÕÕÕÕ08/04/2015Õ0Õ0Õ07/02/2015Õ08001000ÕÕ2 MbpsÕ2 MbpsÕ0Õ08001000ÕÕÕÕÕInternet Diferenciado Principal - S Oro 2Mbps 60 36 Meses Router BasicoÕÕ0ÕÕÕOro PrincipalÕÕ0Õ0ÕÕ0ÕFibraÕÕa3yg00000005qGkAAIÕ0Õ0ÕÕÕ0Õ0Õ0Õ0Õ0Õ0Õ0ÕÕCARLOS CARVAJALÕÕÕÕ0ÕSin PreferenciaÕÕÕÕÕOP-0000774279ÕVencidoÕfalseÕÕÕÕNoÕÕÕÕÕ0Õ07/01/2018Õ36ÕNÕ0ÕÕ0';
        objLog.BI_COL_Informacion_recibida__c      = null;

        objLog.BI_COL_Tipo_Transaccion__c = 'SERV. CORPORATIVO - COBROS PARCIALES';



        //lstLog.add(objLog);
        insert objLog;
        BI_COL_RespuestasDavox__c objRespuesta = new  BI_COL_RespuestasDavox__c ();
        objRespuesta.Name = '573';
        objRespuesta.BI_COL_Descripcion_Codigo__c = '573';
        insert objRespuesta;
        objRespuesta = new  BI_COL_RespuestasDavox__c ();
        objRespuesta.Name = '580';
        objRespuesta.BI_COL_Descripcion_Codigo__c = '580';
        insert objRespuesta;

    }


    static testmethod void method1() {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            Test.startTest();
            Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
            crearData();
            BI_COL_SendDavox_cls objBatch = new BI_COL_SendDavox_cls(false, 2);
            objBatch.codPaquete = BI_COL_SendDavox_cls.getPackageCode();
            string strConsulta = 'SELECT ID, BI_COL_Informacion_Enviada__c,BI_COL_Informacion_recibida__c,BI_COL_Cobro_Parcial__c,BI_COL_Modificacion_Servicio__c,BI_COL_Estado__c' +
                                 ' FROM BI_Log__c ' +
                                 ' where isdeleted = false and BI_COL_Tipo_Transaccion__c IN (\'SERV. CORPORATIVO - COBROS PARCIALES\')' +
                                 ' and BI_COL_Estado__c in (\'Pendiente\', \'Devuelto\', \'Error Conexion\', \'Procesando \') order by createddate';
            objBatch.TipoNovedad = 'SERV. CORPORATIVO - NOVEDADES';
            objBatch.Query = strConsulta;
            System.debug('\n\n' + strConsulta + '\n\n');
            System.Debug('Inicia ejecución del batch');
            ID batchprocessid = Database.executeBatch(objBatch, 1);
            System.Debug('Finaliza ejecución del batch batchprocessid: ' + batchprocessid);
            List<BI_Log__c> novedades = new List<BI_Log__c> ();
            novedades.add(objLog);
            string strXmlRespuesta = '<document><result>success</result><item><SEPARATOR>@</SEPARATOR><Value>573@537</Value></item></document>';
            BI_COL_SendDavox_cls.ActualizaNovedades(novedades,  strXmlRespuesta);
            Test.stopTest();
        }
    }
}