@isTest
private class PCA_IdeasCornerController_TEST {
//(seeAllData=true)
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_IdeasCornerController class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    14/08/2014              Ana Escrich             Initial Version
    09/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception = false added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_IdeasCornerController.checkPermissions 
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    14/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_IdeasCornerController_catchTest() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        PCA_IdeasCornerController controller = new PCA_IdeasCornerController();
        PCA_IdeasCornerController.getRSSData(null) ;
        
        try{

            BI_TestUtils.throw_exception = true;
            controller.loadInfo2();
            controller.checkPermissions ();
            controller.checkPermissions2();
            controller.loadInfo ();
            controller.getChatterGroups();
            controller.inicializeIdea();
            controller.insertIdea();
            controller.detailsIdea();
            controller.refresh ();
            controller.goToDetails ();
            controller.obtainMyIdeas ();
            controller.obtainPopularIdeas ();
            controller.incrementNumberAllIdeasShow();
            controller.incrementNumberMyIdeasShow();
            controller.incrementNumberPopularIdeasShow();
            controller.getshowMoreNumberAllIdeas();
            controller.getMoreMyIdeas();
            controller.getMorePopularIdeas();
            controller.obtainAllIdeas ();
            controller.obtenerFAQ ();
            controller.recoverySolutions();
            controller.RSSSALEFORCE ();
            controller.getVideos();
            system.assert(true);

        }catch(Exception exc){
            system.assert(false);
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_IdeasCornerController.checkPermissions2
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    14/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_IdeasCornerController_try2_Test() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
         User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         usr.Pais__c='Argentina';
         update usr;
           Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
           Network netw = [Select Id from Network where Name= 'Empresas Platino'];
           Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];

        //insert usr;
        system.debug('usr: '+usr);
        User user1;
        system.runAs(usr){
 
            BI_Videoteca__c vid = new BI_Videoteca__c(Name='Video Test',
                                                    BI_Activo__c =true,
                                                    BI_URL__c = 'www.google.es');
        
            insert vid;
            
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                        
            Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=vid.Id, 
                                            ContentType='image/png');  
            
            insert attach;
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Sector__c='Marketing';
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);
        /*}
        
       NetworkMember netMem = new NetworkMember(NetworkId=netw.Id,MemberId=user1.Id);
       insert netMem;
       system.runAs(usr){*/
        
            system.runAs(user1){
                BI_Configuracion__c conf = new    BI_Configuracion__c(Name='URL_Videoteca',BI_Valor__c='http://B30547.cdn.telefonica.com/30547/');
                insert conf;
                //List<BI_Configuracion__c> conf = [SELECT Name,BI_Valor__c FROM BI_Configuracion__c where Name = 'URL_Videoteca'];
                Idea idea1 = new Idea(Body='Idea test',Title='Idea',CommunityId=comm.Id);
                insert idea1;
                
                BI_TestUtils.throw_exception = false;
                
                
                PCA_IdeasCornerDetailController controller1 = new PCA_IdeasCornerDetailController();
                PCA_IdeasCornerController controller = new PCA_IdeasCornerController(controller1);
				
				PCA_IdeasCornerController.Videos_wrapper vwrp=new PCA_IdeasCornerController.Videos_wrapper('Strins','String', 'string', 'String');                

                 //controller.testIdContact=user1.ContactId;
                 controller.checkPermissions2();
                 controller.inicializeIdea();
                 controller.insertIdea();
                 controller.detailsIdea();
                 controller.refresh();
                 controller.goToDetails();
                 controller.obtainMyIdeas();
                 controller.obtainPopularIdeas();
                 controller.incrementNumberAllIdeasShow();
                 controller.incrementNumberMyIdeasShow();
                 controller.incrementNumberPopularIdeasShow();
                 controller.getshowMoreNumberAllIdeas();
                 controller.getMoreMyIdeas();
                 controller.getMorePopularIdeas();
                 controller.obtainAllIdeas();
                 controller.getVideos();

                 system.assert(!controller.listAllIdeasWrapper.isEmpty());

            }
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_IdeasCornerController.checkPermissions2
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    14/08/2014              Antonio Moruno              Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_IdeasCornerController_try1_Test() { 
        
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
         User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         usr.Pais__c='Argentina';
         update usr;
           Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
           Network netw = [Select Id from Network where Name= 'Empresas Platino'];
           Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];

        //insert usr;
        system.debug('usr: '+usr);
        User user1;
        system.runAs(usr){

            BI_Videoteca__c vid = new BI_Videoteca__c(Name='Video Test',
                                                    BI_Activo__c =true,
                                                    BI_URL__c = 'www.google.es');
        
            insert vid;
            
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                        
            Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=vid.Id, 
                                            ContentType='image/png');  
            
            insert attach;
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Sector__c='Marketing';
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);
        /*}
        
       NetworkMember netMem = new NetworkMember(NetworkId=netw.Id,MemberId=user1.Id);
       insert netMem;
       system.runAs(usr){*/
            system.runAs(user1){
                BI_Configuracion__c conf = new    BI_Configuracion__c(Name='URL_Videoteca',BI_Valor__c='http://B30547.cdn.telefonica.com/30547/');
                insert conf;
                //List<BI_Configuracion__c> conf = [SELECT Name,BI_Valor__c FROM BI_Configuracion__c where Name = 'URL_Videoteca'];
                Idea idea1 = new Idea(Body='Idea test',Title='Idea',CommunityId=comm.Id);
                insert idea1;
                PCA_IdeasCornerDetailController controller1 = new PCA_IdeasCornerDetailController();
                PCA_IdeasCornerController controller = new PCA_IdeasCornerController(controller1);              
                 //controller.testIdContact=user1.ContactId;
                BI_TestUtils.throw_exception = false;
                 controller.checkPermissions();
                 controller.loadinfo2();
                 controller.checkPermissions2();
                 controller.inicializeIdea();
                 controller.insertIdea(); 
                 controller.detailsIdea();
                 controller.refresh();
                 controller.goToDetails();
                 controller.obtainMyIdeas();
                 controller.obtainPopularIdeas();
                 controller.recoverySolutions();
                 controller.RSSSALEFORCE();
                 //chan.channel();
                 controller.getChatterGroups();
                 controller.incrementNumberAllIdeasShow();
                 controller.incrementNumberMyIdeasShow();
                 controller.incrementNumberPopularIdeasShow();
                 controller.getshowMoreNumberAllIdeas();
                 controller.getMoreMyIdeas();
                 controller.getMorePopularIdeas(); 
                 controller.obtainAllIdeas();
                 controller.getVideos();

                 system.assert(!controller.listAllIdeasWrapper.isEmpty());
            }
        }
    }   
    
    static testMethod void PCA_IdeasCornerController_assorted() {
        PCA_IdeasCornerController controller = new PCA_IdeasCornerController();
        PCA_IdeasCornerController.ideasWrapper ideaWrap = new PCA_IdeasCornerController.ideasWrapper();
        if(ideaWrap.createdDate !=null) {}
        
        PCA_IdeasCornerController.RSSWrapper rssWrap = new PCA_IdeasCornerController.RSSWrapper();
        if(rssWrap.description!=null) {}
        if(rssWrap.link !=null){}
        if(rssWrap.publicado!=null){}
        if(rssWrap.strDate !=null) {}
        if(rssWrap.title!=null) {}
        
        PCA_IdeasCornerController.item item = new PCA_IdeasCornerController.item();
        item.getPublishedDate();
        item.getPublishedDateTime();
        
        List<PCA_IdeasCornerController.RssShow> rssShow = new List<PCA_IdeasCornerController.RSSShow>();
        rssShow.add(new PCA_IdeasCornerController.RssShow());
        if(rssShow[0].dateParse !=null) {}
        controller.listEventShow = new List<PCA_IdeasCornerController.RssShow>(rssShow);
	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}