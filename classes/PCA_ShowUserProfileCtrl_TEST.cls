@isTest  
private class PCA_ShowUserProfileCtrl_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_ShowUserProfileCtrl class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    30/09/2014              Micah Burgos 	        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_ShowUserProfileCtrl_TEST() {
    	 
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id; 
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false; 
	    		

	    		
	    		PageReference pageRef = new PageReference('PCA_ShowUserProfile');
             	Test.setCurrentPage(pageRef);
             	ApexPages.currentPage().getParameters().put('Id', user1.Id);
             
             
	    		PCA_ShowUserProfileCtrl tmpContr = new PCA_ShowUserProfileCtrl();

	    		tmpContr.checkPermissions();
	    		tmpContr.loadInfo();
	    		system.assert(tmpContr.URL != null && tmpContr.URL.contains(user1.Id));
	    		
	    		//CatchTest:
				BI_TestUtils.throw_exception = true;
	    			    		
	    		tmpContr = new PCA_ShowUserProfileCtrl();
	    		tmpContr.checkPermissions();
	    		tmpContr.loadInfo();
	
	    	}
	    }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}