@isTest
private class TGS_ButtonHandler_Order_CTRL_TEST {
	
	public TGS_ButtonHandler_Order_CTRL_TEST(){
	}
	
	@isTest static void redirectTEST () {

		TGS_ButtonHandler_Order_CTRL button1= new TGS_ButtonHandler_Order_CTRL();
		Case testCase = TGS_Dummy_Test_Data.dummyOrderCase();
		NE__Order__c testOi = TGS_Dummy_Test_Data.dummyOrder();
		testOi.Case__c = testCase.Id;
		update testOi;

		NE__Catalog_Item__c testCi= TGS_Dummy_Test_Data.dummyCatalogItem();
		NE__Catalog_Item__c testCi2= TGS_Dummy_Test_Data.dummyCatalogItem(); 

		List<NE__Catalog_Category__c> listCC = [select Id,Name from NE__Catalog_Category__c];

		for(NE__Catalog_Category__c CC : listCC)
		{
			CC.Name = Constants.GLOBAL_MOBILITY_FOR_MNCS;
		}
		update listCC;
		testCase.TGS_Service__c =  Constants.GLOBAL_MOBILITY_FOR_MNCS;
		update testCase;
		testCi2.NE__Root_Catalog_Item__c= testCi2.Id;
		
		ApexPages.StandardController controller  = new ApexPages.StandardController(testOi);
		TGS_ButtonHandler_Order_CTRL button= new TGS_ButtonHandler_Order_CTRL(controller);
		button.redirect();


		// Implement test code
	}
	
	
}