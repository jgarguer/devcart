/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Eduardo Ventura
Company:        Everis España
Description:    Clase controladora para el Gestor documental--> Contratos/licitaciones para Argentina

History:
 
<Date>                      <Author>                        <Change Description>
26/10/2016                  Eduardo Ventura               	Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



public class BI_GD_sincGestorDocumentalController{
    
    private final Contract cont;
    

    public String result {get;set;} 
    //public String idCont = cont.Id;
    
    public BI_GD_sincGestorDocumentalController(ApexPages.StandardController stdController){
        this.cont = (Contract)stdController.getRecord(); 
        result = cont.Id;
	}
    
    
    
}