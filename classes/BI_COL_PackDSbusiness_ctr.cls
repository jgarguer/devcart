/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-10      Daniel ALexander Lopez (DL)     Cloned Controller      
*************************************************************************************/

public with sharing class BI_COL_PackDSbusiness_ctr {
    public Map<String,List<Wrapempaquetar>> mapPaquetXDS {get;set;}
    public List<BI_COL_Descripcion_de_servicio__c> lstDs {get;set;}
    public Boolean actMensajes{get;set;}
    public List<String> lstPaquetes{get;set;}
    public Map<Id,BI_COL_Descripcion_de_servicio__c> mapIdXnuevaDS;
    public List<List<Wrapempaquetar>> lstPrincipal{get;set;}
    public List<Wrapempaquetar> lstMostrar;
    public List<BI_COL_Descripcion_de_servicio__c> lstDsEmpaquetar{get;set;}
    public Integer iterador;
    public Integer limite{get;set;}
    public boolean actRegion{get;set;}
    public Boolean disableBoton1{get;set;}
    public boolean disableBoton2{get;set;}
    public List<Selectoption> lstPaquetesSel{get;set;}
    public string opcEmpaquetar {get;set;}
    public string opcEmpaquetarNuevo {get;set;}
    public Map<String, BI_COL_Empaquetamiento__c> mapEmpaquetamiento;
    public Account cliente;
    public String segmento;
    public boolean habilitarPaquete {set;}
    public List<Account> lstCliente;
    public String Busqueda{get; set;}
    
    /** Paramétros del paginador */
    private List<Wrapempaquetar> pageEmpaquetar;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber;
    
    public void Buscar()
    {
        actRegion=false;
        actMensajes=false;
        disableBoton1=true;
        disableBoton2=true;
        lstDs=new List<BI_COL_Descripcion_de_servicio__c>();
        
        
        String idCliente = Apexpages.currentPage().getParameters().get('id');
        lstCliente=[select id,Name,BI_COL_Segmento_Telefonica__c,
                                    (Select id,Name,StageName
                                        From Opportunities 
                                        //where StageName IN ('F2 - Negotiation','F5 - Solution Definition') 
                                        //where StageName IN ('open') 
                                    ) 
                                    from Account where id= :idCliente limit 1];
        if(lstCliente==null||lstCliente.size()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.INFO, 'No existe oportunidad' );
            Apexpages.addMessage( pm );
            actMensajes=true;
        }
        else
        {   
            this.cliente = lstCliente.get(0); 
            System.debug('\n IDCLIENTE=======>>>'+lstCliente);
            segmento = this.cliente.BI_COL_Segmento_Telefonica__c;
            List<Opportunity> lstOportunidades = this.cliente.Opportunities;
            System.debug('\n OPORTUNIDADES=======>>>'+lstOportunidades);
            this.lstDS = getDS(this.cliente.Opportunities);
            //Opciones de paquetes
            mapEmpaquetamiento = BI_COL_Empaquetamiento__c.getAll();
            
            //Llenar lista con opc de paquetes
            System.debug('\n\n List DS '+lstDs);
            constructor(segmento,mapEmpaquetamiento.values(),lstDs);
            
            //Inicializar valores del paginador
            pageNumber = 0;
            totalPageNumber = 0;
            pageSize = 10;
            ViewData();     
        }
            
        system.debug('lst' + lstds);
        
    }
    
    public void constructor(String segmento, List<BI_COL_Empaquetamiento__c> lstEmp, List<BI_COL_Descripcion_de_servicio__c> lstDs)
    {
      //this.cliente.Opportunities
      
      
        if(lstDs==null||lstDs.size()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.INFO, 'No existen Ds para empaquetar' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            
        }
        else
        {
            lstPrincipal=new List<List<Wrapempaquetar>>();
            llenarPaquetes();
            if(lstPrincipal.size()>0)
            {
                iterador=0;
                lstMostrar=lstPrincipal.get(iterador);
                System.debug('\n\n lstPrincipal.size()='+lstPrincipal.size()+'\n\n');
                if(lstPrincipal.size()>1)
                {
                    iterador++;
                    disableBoton1=false;
                }   
                actRegion=true;
                llenarSelectOption(segmento,lstEmp,lstDS);
            }
            else
            {
                actMensajes=true;
                actRegion=false;
                Apexpages.addMessage(new ApexPages.Message(Apexpages.Severity.INFO,'No se han encontrado DS para la oportunidad'));
                
            }
        }
         
    }
    
    public PageReference ViewData()
    {
        totalPageNumber = 0;
        System.debug('\n\n Antes del BINdada \n\n');
        BindData(1);
        System.debug('\n\n Despues del BINdada \n\n');
        return null;
    }
    
    public class Wrapempaquetar
    {
        public id idDs {get;set;}
        public boolean seleccion {get;set;}
        public String dsName {get;set;}
        public String paquete{get;set;}
        public String linea {get;set;}
        public String producto{get;set;}
        public String descripcionRef{get;set;}
        public boolean disable{get;set;}
        public String NombrePaquete {get;set;}
        
        public Wrapempaquetar()
        {
            seleccion=false;
            paquete='Sin Empaquetar';
            NombrePaquete='Sin Empaquetar';
        }
        
    }
    
    public void llenarPaquetes()
    {
        mapIdXnuevaDS=new Map<id,BI_COL_Descripcion_de_servicio__c>();
        mapPaquetXDS=new Map<String,list<Wrapempaquetar>>();
        List <BI_COL_Descripcion_de_servicio__c>lsttmp=lstDs.clone();
        System.debug('\n\n tamLista '+lstDs.size()+'\n\n');
        List<Wrapempaquetar>lstAdd;
        Integer numListas=calculoListas(lstDs.size());
        integer contador2=0;
        Integer tamlistatmp=lstTmp.size();
        User usu=[Select CompanyName,Profile.Name from User where id=: userinfo.getuserid()];
        for(integer contador=0;contador<numListas;contador++)
        {
            System.debug('tamLista for'+lsttmp.size());
            lstadd=new List<Wrapempaquetar>();
            
            for(integer contador3=0;contador3<10;contador3++)
            {
                BI_COL_Descripcion_de_servicio__c DsTemp=lsttmp.get(contador2);
                mapIdXnuevaDs.put(DsTemp.id,DsTemp);
                Wrapempaquetar wrap=nuevoWrap(DsTemp,usu);
                lstadd.add(wrap);
                llenarPaquetes(wrap);
                contador2++;
                if(lsttmp.isEmpty()||contador2==tamlistatmp)
                {
                    break;
                }
            }
            lstPrincipal.add(lstadd);
            
        }
        actRegion=true;
    }
    
    public Integer calculoListas(Integer tam)
    {
        Decimal calculo=tam/10;
        Integer retorna=calculo.intValue();
        if(Math.mod(tam,10)>0)
        {
            retorna=retorna+1;
        }
        return retorna;         
    }
    
    public Wrapempaquetar nuevoWrap(BI_COL_Descripcion_de_servicio__c ds,User usu)
    {
        Wrapempaquetar wr=new Wrapempaquetar();
        wr.idDs=ds.id;
        wr.dsName=ds.name;

        // PENDIENTE DEFINIR DESCRIPCION DE SERVICIO

        wr.linea=ds.BI_COL_Producto_Telefonica__c!=null?ds.BI_COL_Producto_Telefonica__r.NE__ProdId__r.BI_COL_TipoAnexo__c:ds.BI_COL_Producto_Anterior__r.BI_COL_Linea__c;
        wr.producto=ds.BI_COL_Producto_Telefonica__c!=null?ds.BI_COL_Producto_Telefonica__r.NE__ProdId__r.Name:ds.BI_COL_Producto_Anterior__r.BI_COL_Producto__c;
        wr.descripcionRef=ds.BI_COL_Producto_Telefonica__c!=null?ds.BI_COL_Producto_Telefonica__r.NE__ProdId__r.BI_COL_LegadoID__c:ds.BI_COL_Producto_Anterior__r.BI_COL_Descripcion_referencia__c;
        
        
        Boolean bandera=false;
        if(ds.BI_COL_Oportunidad__r.StageName!='F1 - Cerrada/Legalizada')
        {
            bandera=true;
        }
        else if(usu.Profile.Name=='Admon Ventas Master')
        {
            bandera=true;
        }
        
        if(ds.BI_COL_Codigo_paquete__c==null && bandera==true)
        {
            wr.disable=false;
        }
        else
        {
            wr.disable=true;
            wr.paquete=ds.BI_COL_Codigo_paquete__c;
            wr.NombrePaquete=ds.BI_COL_Nombre_de_paquete__c;
        }
        return wr;
        
    }
    /*public List<Wrapempaquetar> getlstPaginador(){
            System.debug('\n\n ........... iterador   '+iterador);
        return lstPrincipal.get(iterador);
    }*/
    
    public Pagereference siguiente() 
    {
        System.debug('\n\n Ingreso a siguiente \n\n');
        this.lstMostrar=lstPrincipal.get(iterador);
        //lstMostrar=new List<Wrapempaquetar>();
        System.debug('\n\n iterador '+iterador+' \n\n');
        System.debug('\n\n lstMostrar='+lstMostrar.size()+' \n\n');
        iterador++;
        disableboton2=false;
        if(iterador==lstPrincipal.size())
        {
            disableboton1=true;
            iterador--;
        }
        System.debug('\n\n     .......................   lstMostrar  '+lstMostrar);
        System.debug('\n\n     .......................   lstPrincipal '+lstPrincipal);
        ViewData();//Scardona 06-03-2012 : Se agrega el metodo ViewData().
        return null;
    }
    
    public Pagereference anterior()
    {
        iterador--;
        lstMostrar=lstPrincipal.get(iterador);
        
        disableboton1=false;
        if(iterador==0)
        {
            disableboton2=true;
            iterador++;
        }
        ViewData();//Scardona 06-03-2012 : Se agrega el metodo ViewData().
        return null;
    }
    
    public PageReference empaquetar()
    {
        Double num;
        List<BI_COL_manage_cons__c> lmc;
        System.debug('\n\n Empaquetar \n\n'+opcEmpaquetar);
        if((opcEmpaquetar == null || opcEmpaquetar.trim().length()==0)
            &&(this.opcEmpaquetarNuevo == null||this.opcEmpaquetarNuevo.trim().length()==0))
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.ERROR, 'Debe selecionar o crear algún paquete' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            return null;
        }
        
        lmc=[Select m.Name, m.BI_COL_Numero_Viabilidad__c from BI_COL_manage_cons__c m where Name='ConsPaquetes'];
        num=lmc[0].BI_COL_Numero_Viabilidad__c+1;
        
        lstDsEmpaquetar=new List<BI_COL_Descripcion_de_servicio__c>();
        for(List<Wrapempaquetar> lstTemp:lstPrincipal)
        {
            String nombrePkg = (this.opcEmpaquetar == null || this.opcEmpaquetar.trim().length()==0) ? this.opcEmpaquetarNuevo : getNamePaquete(this.opcEmpaquetar);
            String CodPaquete = (this.opcEmpaquetar == null || this.opcEmpaquetar.trim().length()==0) ? 'PQ-'+num : this.opcEmpaquetar;
            buscarDsAempaquetar(lstTemp, nombrePkg,CodPaquete);
        }
        
        if(lstDsEmpaquetar.size()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.ERROR, 'No se seleccionarón DS para empaquetar' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            return null;
        }
        else
        {
            Savepoint sp;
            try
            {
                sp= Database.setSavepoint();
                update lstDsEmpaquetar;
                
                if(lmc!=null)
                {
                    lmc[0].BI_COL_Numero_Viabilidad__c=num;
                    database.update(lmc);
                }                   
                Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.CONFIRM, 'Se han empaquetado las DS seleccionadas en el paquete: '
                                        + this.opcEmpaquetarNuevo);
                Apexpages.addMessage( pm );
                actMensajes=true;
                this.opcEmpaquetar = '';
                constructor(this.segmento,this.mapEmpaquetamiento.values(),this.lstDS);
            }
            catch(System.exception e)
            {
                Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.INFO, 'se ha presentado un error informe al administrador del sistema: '+e.getMessage() );
                Apexpages.addMessage( pm );
                System.debug('\n\n Error al empaquetar: '+e+'\n\n');
                this.lstDS = getDS(this.cliente.Opportunities);
                constructor(this.segmento,this.mapEmpaquetamiento.values(),this.lstDS);
                actMensajes=true;
                Database.rollback(sp);
            }
        }
        return null;    
    }
    
    public List<BI_COL_Descripcion_de_servicio__c> getDS (List<Opportunity> lstOportunidades)
    {
        String consultaSOQL='SELECT n.Name, n.Id, n.BI_COL_Codigo_paquete__c, BI_COL_Producto_Telefonica__c,BI_COL_Producto_Anterior__c,BI_COL_Producto_Anterior__r.BI_COL_Descripcion_referencia__c, '+
                             ' BI_COL_Producto_Anterior__r.Name, BI_COL_Producto_Telefonica__r.NE__ProdId__r.Name, '+                      
                             ' BI_COL_Oportunidad__r.StageName,BI_COL_Producto_Telefonica__r.NE__ProdId__r.BI_COL_LegadoID__c, '+
                             ' BI_COL_Nombre_de_paquete__c, BI_COL_Producto_Telefonica__r.NE__ProdId__r.BI_COL_TipoAnexo__c, '+
                             ' BI_COL_Producto_Anterior__r.BI_COL_Linea__c, BI_COL_Producto_Anterior__r.BI_COL_Producto__c' +
                             ' FROM BI_COL_Descripcion_de_servicio__c n '+
                             ' WHERE BI_COL_Oportunidad__c = :lstOportunidades ';
              //' AND BI_COL_Producto_Telefonica__c <> null';
              //'AND BI_COL_Producto_Anterior__c <> null';

    System.debug('\n Busqueda=======>>>>'+Busqueda);
    if(Busqueda!=null && Busqueda!='' )
    {   
      String dsBuscar='';   
      if(Busqueda.contains(','))
      {
        List<String> lstDSB=Busqueda.split(',');
        for(String ls:lstDSB)
        {
          dsBuscar+='\''+ls+'\',';
        }
        dsBuscar=dsBuscar.subString(0,dsBuscar.length()-1);
      }
      else
      {
        dsBuscar='\''+Busqueda+'\'';
      }
      consultaSOQL+=' AND Name IN ('+dsBuscar+')';
      System.debug('\n dsBuscar=======>>>>'+dsBuscar);
      
    }

    System.debug('\n\n consultaSOQL----->>>>'+consultaSOQL+ '---->Busqueda: '+Busqueda+'  '+lstOportunidades);
    List<BI_COL_Descripcion_de_servicio__c> lstDS=new List<BI_COL_Descripcion_de_servicio__c>();  
    try
    {

        lstDS=Database.query(consultaSOQL);

        }catch(Exception e)
        {
            System.debug('Fallo en la consulta =======\n '+e.getMessage());    
        }
    
        //List<BI_COL_Descripcion_de_servicio__c> lstDS2 = [
        //        SELECT n.Name, n.Id, n.BI_COL_Codigo_paquete__c, BI_COL_Producto_Anterior__c,
        //                        BI_COL_Producto_Telefonica__r.NE__ProdId__r.Rama_Local__c, BI_COL_Producto_Telefonica__r.NE__ProdId__r.Name,
        //                        BI_COL_Oportunidad__r.StageName,
        //                        BI_COL_Nombre_de_paquete__c
        //                        FROM BI_COL_Descripcion_de_servicio__c n
        //                        WHERE BI_COL_Oportunidad__c = :lstOportunidades
        //                        //and BI_COL_Oportunidad__r.StageName in ('F2 - Ganada','E6 - Ganada')
        //                        and BI_COL_Oportunidad__r.StageName in ('open')
        //                        AND BI_COL_Producto_Anterior__c <> null 
        //                        AND BI_COL_Codigo_paquete__c = null
        //                            ];
    //System.debug('\n\n Total de DS2='+lstDS2.size()+' \n\n');
    System.debug('\n\n Resultado de la consulta ========== '+lstDS+' \n\n');

        return lstDS;
    }
    
    public void buscarDsAempaquetar(List<Wrapempaquetar> lstTemp, String opcEmpaquetar,String CodPaquete)
    {
        BI_COL_Descripcion_de_servicio__c dsTemp;
        for(Wrapempaquetar wr: lstTemp )
        {
            if(wr.seleccion && !wr.disable)
            {
                dsTemp=mapIdXnuevaDS.get(wr.idDs);
                wr.disable=true;
                wr.paquete=CodPaquete;
                wr.NombrePaquete=opcEmpaquetar;
                dsTemp.BI_COL_Codigo_paquete__c=wr.paquete;
                dsTemp.BI_COL_Nombre_de_paquete__c=wr.NombrePaquete;
                lstDsEmpaquetar.add(dsTemp);
            }
        }
    }
    
    public void llenarPaquetes(WrapEmpaquetar dsTemp)
    {
        if(mapPaquetXDS.containsKey(dsTemp.paquete))
        {
            mapPaquetXDS.get(dsTemp.paquete).add(dsTemp);
        }
        else
        {
            List <Wrapempaquetar> lstWrapemp=new List<Wrapempaquetar>();
            lstWrapemp.add(dsTemp);
            mapPaquetXDS.put(dsTemp.paquete,lstWrapemp);
        }
    }

    public void llenarSelectOption(String segmento, List<BI_COL_Empaquetamiento__c> lstEmp, List<BI_COL_Descripcion_de_servicio__c> lstDS)
    {
        /*

            TEMPORAL MIENTAS DEFINO LAS RELACIONES
        */
        //list<BI_COL_Descripcion_de_servicio__c> ndse = new list <BI_COL_Descripcion_de_servicio__c> ();
        //list<BI_COL_Descripcion_de_servicio__c> ndse = new list < BI_COL_Descripcion_de_servicio__c> ( [SELECT BI_COL_Codigo_paquete__c, BI_COL_Nombre_de_paquete__c FROM BI_COL_Descripcion_de_servicio__c WHERE Oportunidad__c IN :this.cliente.Opportunities ] );
            //AND BI_COL_Producto_Telefonica__c <> null] );
      
        Set<String> setPckg = new Set<String>();
        this.lstPaquetesSel=new List<SelectOption>();
        this.lstPaquetesSel.add(new SelectOption('','--Ninguno--'));
        this.opcEmpaquetarNuevo = '';
        
        //Paquetes creados
        for(BI_COL_Descripcion_de_servicio__c ds : lstDS)
        {
            if(ds.BI_COL_Codigo_paquete__c !=null)//&&ds.BI_COL_Codigo_paquete__c.trim().length()>0)
            {
                setPckg.add(ds.BI_COL_Codigo_paquete__c);
            }
        }
        
        //Iterar nuevos paquetes y agregarlos a la lista
        for(String opcPckg : setPckg)
        {
            this.lstPaquetesSel.add(new SelectOption(opcPckg,opcPckg));
        }
    }
    
    /**
    * Posiciona el registro segun el numero de pagina
    * @param newPageIndex Indice de la pagina
    * @return 
    */
    private void BindData(Integer newPageIndex)
    {
        try
        {
            pageEmpaquetar = new List<Wrapempaquetar>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            
            for(Wrapempaquetar wMS : lstMostrar)
            {
                counter++;
                if (counter > min && counter <= max)
                {
                    pageEmpaquetar.add(wMS);
                }
                    
            }
            pageNumber = newPageIndex;
            
            if (pageEmpaquetar == null || pageEmpaquetar.size() <= 0)
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Los daros no estan disponibles para visualizar'));
        }
        catch(Exception ex)
        {
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }   
    }
    
    /**
    * Acción para pasar a la siguiente página de registros
    * @return Pagereference
    */
    public PageReference nextBtnClick() {
            BindData(pageNumber + 1);
        return null;
    
    }
    
    /**
    * Acción para pasar a la anterior página de registros
    * @return Pagereference
    */
    public PageReference previousBtnClick() {
            BindData(pageNumber - 1);
        return null;
    
    }
    
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    public Integer getPageSize()
    {
        return pageSize;
    }
    
    /**
    * Obtiene el numer de paginas de la tabla 
    * @return numero de paginas
    */
    public Integer getTotalPageNumber()
    {
        if (totalPageNumber == 0 && lstMostrar !=null)
        {
            totalPageNumber = lstMostrar.size() / pageSize;
            Integer mod = lstMostrar.size() - (totalPageNumber * pageSize);
            if (mod > 0)
            totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (lstMostrar == null){    return true;}
        else{return ((pageNumber * pageSize) >= lstMostrar.size());}
    
    }
    
    public List<Wrapempaquetar> getLstMostrar()
    {
        return pageEmpaquetar;
    }
    
    public static String getNamePaquete(String CodPaquete)
    {
        List<BI_COL_Descripcion_de_servicio__c> lstDS= [SELECT BI_COL_Nombre_de_paquete__c
                                        FROM BI_COL_Descripcion_de_servicio__c
                                        WHERE BI_COL_Codigo_paquete__c=:CodPaquete LIMIT 1];
        if(lstDS.size()>0)
        {
            return lstDS[0].BI_COL_Nombre_de_paquete__c;
        }
        else
        {
            return 'N/A';
        }
    }
}