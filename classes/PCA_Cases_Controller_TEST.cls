@isTest
public class PCA_Cases_Controller_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_Cases_Controller class 

    <Date>                  <Author>                <Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Cases_Controller.init
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getloadInfo() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            List<Case> testCase = [SELECT Subject, toLabel(Status), Description, caseNumber, createdDate, RecordTypeId,RecordType.Name, Order__c FROM Case WHERE Id = :testOrder.Case__c];
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            controller.createTableRecordList(testCase);
            System.assertEquals('', controller.currentServiceName);
            Test.stopTest();
        }
        
    }
    static testMethod void getloadInfosId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id, Subject, CaseNumber FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
         /* Portal User Configuration */
            TGS_Portal_User_Configuration__c testPortalUserConfig = new TGS_Portal_User_Configuration__c(Contact__c = userTest.ContactId);
            insert testPortalUserConfig;
         /* Portal User Configuration Product */
            TGS_Portal_User_Configuration_Product__c testPortalUserConfigProduct = new TGS_Portal_User_Configuration_Product__c(Portal_User_Configuration__c = testPortalUserConfig.Id, Commercial_Product__c = testProduct.Id );
            insert testPortalUserConfigProduct;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('sId', testProduct.Id);
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            System.assertEquals(testProduct.Name, controller.currentServiceName);
            Test.stopTest();
        }
    }
    static testMethod void getloadInfosuId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id, Subject, CaseNumber FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
         /* Portal User Configuration */
            TGS_Portal_User_Configuration__c testPortalUserConfig = new TGS_Portal_User_Configuration__c(Contact__c = userTest.ContactId);
            insert testPortalUserConfig;
         /* Portal User Configuration Product */
            TGS_Portal_User_Configuration_Product__c testPortalUserConfigProduct = new TGS_Portal_User_Configuration_Product__c(Portal_User_Configuration__c = testPortalUserConfig.Id, Commercial_Product__c = testProduct.Id );
            insert testPortalUserConfigProduct;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            NE__OrderItem__c testOrderItem2 = [SELECT Name FROM NE__OrderItem__c WHERE NE__OrderId__c = :testOrder.Id LIMIT 1];
            System.assertEquals(testOrderItem2.Name, controller.currentServiceName);
            Test.stopTest();
        }
    }
    static testMethod void getloadInfoURLSuccess() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            ApexPages.currentPage().getParameters().put('success', 'true');
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            System.assertEquals(true, controller.success);
            Test.stopTest();
        }
    }
    static testMethod void getloadInfoURLError() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            ApexPages.currentPage().getParameters().put('error', 'true');
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            System.assertEquals(true, controller.error);
            Test.stopTest();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Cases_Controller.createFilterCondition
    
     <Date>                 <Author>                <Inquiry Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createFilterConditionTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id, Subject, CaseNumber FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
         /* Portal User Configuration */
            TGS_Portal_User_Configuration__c testPortalUserConfig = new TGS_Portal_User_Configuration__c(Contact__c = userTest.ContactId);
            insert testPortalUserConfig;
         /* Portal User Configuration Product */
            TGS_Portal_User_Configuration_Product__c testPortalUserConfigProduct = new TGS_Portal_User_Configuration_Product__c(Portal_User_Configuration__c = testPortalUserConfig.Id, Commercial_Product__c = testProduct.Id );
            insert testPortalUserConfigProduct;
            Test.startTest();
            String filter = PCA_Cases_Controller.createFilterCondition(PCA_Cases_Controller.MOSTRAR_OPEN_REQUESTS, testCase.Subject, testCase.CaseNumber, '', '', false, null);
            System.assertEquals(' AND Status <> \'Closed\' AND Subject LIKE \'%'+ testCase.Subject +'%\' AND CaseNumber LIKE \'%' + testCase.CaseNumber + '%\'', filter);
            DateTime dateTimeTest = dateTime.now();
            String dateTest = dateTimeTest.day()+'/'+dateTimeTest.month()+'/'+dateTimeTest.year();
            dateTimeTest = PCA_Cases_Controller.convertToDateTime(dateTest);
            filter = PCA_Cases_Controller.createFilterCondition(PCA_Cases_Controller.MOSTRAR_CLOSED_REQUESTS, '', '', dateTest, '', false, null);
            System.assertEquals(' AND Status = \'Closed\' AND createdDate >= ' + dateTimeTest.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''), filter);
            filter = PCA_Cases_Controller.createFilterCondition(PCA_Cases_Controller.MOSTRAR_MY_REQUESTS, testCase.Subject, '', '', dateTest, false, null);
            System.assertEquals(' AND ContactId = \''+ userTest.ContactId +'\' AND Subject LIKE \'%'+ testCase.Subject +'%\' AND createdDate < ' + dateTimeTest.addDays(1).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''), filter);
            filter = PCA_Cases_Controller.createFilterCondition(PCA_Cases_Controller.MOSTRAR_REQUIRE_ATTENTION, '', '', '', '', false, null);
            System.assertEquals(' AND ( Status = \'Pending\' OR Status = \'Resolved\' )', filter);
            Test.stopTest();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Cases_Controller.clearFilters
    
     <Date>                 <Author>                <Inquiry Description>
    11/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void clearFiltersTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            controller.subject = '000';
            System.assertEquals('000', controller.subject);
            controller.clearFilters();
            System.assertEquals(null, controller.subject);
            Test.stopTest();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Cases_Controller.convertToDateTime
    
     <Date>                 <Author>                <Inquiry Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void convertToDateTimeTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            DateTime dateTimeTest = dateTime.now();
            String dateTest = dateTimeTest.day()+'/'+dateTimeTest.month()+'/'+dateTimeTest.year();
            DateTime dateC = PCA_Cases_Controller.convertToDateTime(dateTest);
            System.assertEquals(dateTimeTest.date(), dateC.date());
            Test.stopTest();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Cases_Controller.clearsIdsuId
    
     <Date>                 <Author>                <Inquiry Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void clearsIdsuIdTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id, Subject, CaseNumber FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
         /* Portal User Configuration */
            TGS_Portal_User_Configuration__c testPortalUserConfig = new TGS_Portal_User_Configuration__c(Contact__c = userTest.ContactId);
            insert testPortalUserConfig;
         /* Portal User Configuration Product */
            TGS_Portal_User_Configuration_Product__c testPortalUserConfigProduct = new TGS_Portal_User_Configuration_Product__c(Portal_User_Configuration__c = testPortalUserConfig.Id, Commercial_Product__c = testProduct.Id );
            insert testPortalUserConfigProduct;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Cases_Controller controller = new PCA_Cases_Controller();
            controller.clearsIdsuId();
            System.assertEquals('', controller.currentServiceName);
            Test.stopTest();
        }
    }
    
  
    
    
  }