@isTest 
private class BI_EmailMessageMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro Hernando Gavilán
    Company:       Telefonica Global Technology
    Description:   Test class to manage the coverage code for BI_EmailMessageMethods class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    17/06/2015              Álvaro Hernando         Initial Creation
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro Hernando Gavilán
        Company:       Telefonica Global Technology
        Description:   Test method to manage the coverage code for fillFieldsARGCobranzas() method in BI_EmailMessageMethods
    
        History: 
        
        <Date>                     <Author>                <Change Description>
        17/06/2015                 Álvaro Hernando         Initial Version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
        static testMethod void fillFieldsARGCobranzas_TEST(){
        
            List<String> lstCountry = new List<String>{'Argentina'};
            List<Account> lstAcc = BI_DataLoad.loadAccounts(2,lstCountry);
            
            lstAcc[1].Name = 'Gestión Administrativa Cobranzas';
            update lstAcc;
            
            Contact dummyContactAHG = new Contact(LastName = 'noreplyAHG', Email = 'test2@abc.org');
            Contact dummyContactAHG2 = new Contact(LastName = 'noreplyAHG', Email = 'test2duplicated@abc.org');
            Contact dummyContactAHG2_duplicated = new Contact(LastName = 'noreplyAHG', Email = 'test2duplicated@abc.org');
            List<Contact> testContactLst = new List<Contact>();
            testContactLst.add(dummyContactAHG);
            testContactLst.add(dummyContactAHG2);
            testContactLst.add(dummyContactAHG2_duplicated);
            insert testContactLst;
            
            RecordType rType = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Caso_Comercial_Abierto' LIMIT 1];
            
            List<Case> lstCas = new List<Case>();
            Case casAux = new Case(
                Subject = 'Test Subject Acc AHG',
                //AccountId = lstAcc[0].Id,
                RecordTypeId = rType.Id,
                Type = Label.BI_CaseType_Request,
                Origin = 'Email To Case ARG - Cobranzas',
                BI_Country__c = lstAcc[0].BI_Country__c);
            lstCas.add(casAux);
            Case casAux2 = new Case(
                Subject = 'Test Subject Acc AHG 2',
                //AccountId = lstAcc[0].Id,
                RecordTypeId = rType.Id,
                Type = Label.BI_CaseType_Request,
                Origin = 'Email To Case ARG - Cobranzas',
                BI_Country__c = lstAcc[0].BI_Country__c);
            lstCas.add(casAux2);
            Case casAux2Duplicated = new Case(
                Subject = 'Test Subject Acc AHG 2 Duplicated',
                //AccountId = lstAcc[0].Id,
                RecordTypeId = rType.Id,
                Type = Label.BI_CaseType_Request,
                Origin = 'Email To Case ARG - Cobranzas',
                BI_Country__c = lstAcc[0].BI_Country__c);
            lstCas.add(casAux2Duplicated);
            Case casAux3 = new Case(
                Subject = 'Test Subject Acc AHG 3',
                //AccountId = lstAcc[0].Id,
                RecordTypeId = rType.Id,
                Type = Label.BI_CaseType_Request,
                Origin = 'Email To Case ARG - Cobranzas',
                BI_Country__c = lstAcc[0].BI_Country__c);
            lstCas.add(casAux3);
                
            insert lstCas;
            
            EmailMessage[] newEmail = new EmailMessage[0];
            newEmail.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = True, ToAddress= 'pagosempresas.ar@telefonica.com', Subject = 'Test email', TextBody = '23456', ParentId = lstCas[0].Id)); 
            EmailMessage[] newEmail2 = new EmailMessage[0];
            newEmail2.add(new EmailMessage(FromAddress = 'test2@abc.org', Incoming = True, ToAddress= 'pagosempresas.ar@telefonica.com', Subject = 'Test email', TextBody = '23456', ParentId = lstCas[1].Id));
            EmailMessage[] newEmail2duplicated = new EmailMessage[0];
            newEmail2duplicated.add(new EmailMessage(FromAddress = 'test2duplicated@abc.org', Incoming = True, ToAddress= 'pagosempresas.ar@telefonica.com', Subject = 'Test email', TextBody = '23456', ParentId = lstCas[2].Id));
            EmailMessage[] newEmail3 = new EmailMessage[0];
            newEmail3.add(new EmailMessage(FromAddress = 'test3@abc.org', Incoming = True, ToAddress= 'pagosempresas.ar@telefonica.com', Subject = 'Test email', TextBody = '23456', ParentId = lstCas[3].Id));
            
            Test.StartTest();
            BI_TestUtils.throw_exception = false;
            
            insert newEmail;
            List<Case> caso1 = [SELECT Id,Account.Name FROM Case WHERE Subject = 'Test Subject Acc AHG' LIMIT 1];
            System.assertEquals('Gestión Administrativa Cobranzas', caso1[0].Account.Name, 'Error value: Account Name must be Gestión Administrativa Cobranzas');
            
            insert newEmail2;
            List<Case> caso2 = [SELECT Id FROM Case WHERE Subject = 'Test Subject Acc AHG 2' LIMIT 1];
            List<EmailMessage> mail2 = [SELECT Id,ParentId FROM EmailMessage WHERE ParentId = :caso2[0].Id];
            System.assertEquals(false, mail2.isEmpty(), 'Error value: mail2 List must be not empty');
            
            insert newEmail2duplicated;
            List<Case> caso2duplicado = [SELECT Id,Account.Name FROM Case WHERE Subject = 'Test Subject Acc AHG 2 Duplicated' LIMIT 1];
            System.assertEquals('Gestión Administrativa Cobranzas', caso2duplicado[0].Account.Name, 'Error value: Account Name must be Gestión Administrativa Cobranzas');
            
            BI_TestUtils.throw_exception = true;
            insert newEmail3;
            List<Case> caso3 = [SELECT Id,AccountId FROM Case WHERE Subject = 'Test Subject Acc AHG 3' LIMIT 1];
            System.assertEquals(null, caso3[0].AccountId, 'Error value: Exception thrown, Account must be null');
            Test.StopTest();
            
        } 

        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        
        Company:       Aborda New Energy 
        Description:   Test method to manage the coverage code for fillE2CFields() method in BI_EmailMessageMethods
    
        History: 
        
        <Date>                     <Author>                <Change Description>
        17/06/2016                 Julian Gawron           Initial Version
		03/01/2017		 			 Marta Glez			REING-01-Adaptacion reingenieria de contactos
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
        @isTest static void fillE2CFields_TEST(){
         
            BI_TestUtils.throw_exception  = false;
            insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
            
             /* TESTSETUP */
            List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case'];
            
            //Crea cuenta de Peru con BI_DataLoad
            List<Account> lst_clients = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{Label.BI_Peru});
            
            Contact con = new Contact(
                LastName ='TestContact_0', 
                AccountId=lst_clients[0].Id,
                //BI_Tipo_de_contacto__c ='Autorizado',
                //REING-01-INI
                BI_Tipo_de_contacto__c ='General;Autorizado',
                  MailingState='Santiago',
                  FS_CORE_Codigo_Ciudad__c='123456',
                  MailingCity= 'Santiago',
                  MailingStreet= 'Urbinas',
                  FS_CORE_MailingNumber__c = '2',
                  FS_CORE_Barrio__c= 'BarrioFake',
                  BI_Tipo_de_documento__c = 'Otros', //JEG 23/11/2017
                  BI_Numero_de_documento__c = '00000000X', //JEG 23/11/2017
                  //REING-01-FIN
                Email = 'testdfghjkdfghj@test.test' //Label.BI_Atencion_Empresas_Peru.split(' ')[0]
            );
            insert con;
            
            /* TESTSETUP */
            List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName ='BI2_Caso_Padre'];
            Case cas = new Case(
                SuppliedEmail = con.Email, 
                Origin='Email2Case PER', 
                RecordTypeId=lst_rt[0].Id
            );
            insert cas;
    
            ///////////////////////Autorizado////////////////////////////////
            String sendEmail = Label.BI_Atencion_Empresas_Peru.split(' ')[0];
            Test.startTest();
            EmailMessage em = new EmailMessage();
            em.Incoming = true;
            em.FromAddress = con.Email;
            em.FromName = 'Test';
            em.ToAddress = sendEmail;
            em.ParentId = cas.Id;
            insert em;

            ///////////////////////No Autorizado/////////////////////////////
            con.BI_Tipo_de_contacto__c = null;
            update con;

            EmailMessage em1 = new EmailMessage();
            em1.Incoming = true;
            em1.FromAddress = con.Email ;
            em1.FromName = 'Test';
            em1.ToAddress = sendEmail;
            em1.ParentId = cas.Id;
            insert em1;

            /////////////////////// Sin contacto /////////////////////////////
            EmailMessage em2 = new EmailMessage();
            em2.Incoming = true;
            em2.FromAddress = 'allalalaalla@alalalalallala.lal' ;
            em2.FromName = 'Test';
            em2.ToAddress = 'sadfgasdgfsdfgasdf@asdfasdfasdfasdfasdf.asdfasdf';
            em2.CcAddress = sendEmail;
            em2.ParentId = cas.Id;            

            insert em2;

            Test.stopTest();
        } 

}