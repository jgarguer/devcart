@isTest
private class BI_DevolverDescuentoModelo_JOB_TEST {

	static{
    	
    	BI_TestUtils.throw_exception = false;
    }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Test Method that manage the code coverage of BI_DevolverDescuentoModelo_JOB
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    02/02/2016		  Guillermo Muñoz	Fix bugs with new validation rule
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void returnDiscountsLineaDeVenta() {
		// Implement test code

		BI_TestUtils.throw_exception=false;
		List <Recordtype> lst_rt = [SELECT Id,DeveloperName FROM Recordtype WHERE DeveloperName = 'BI_Modelo' OR DeveloperName = 'BI_Servicio'];
		List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});
		List <BI_Descuento_para_modelo__c> lst_dm = new List <BI_Descuento_para_modelo__c>();
		List <BI_Linea_de_Venta__c> lst_lv = new List <BI_Linea_de_Venta__c>();
		List <BI_Linea_de_Servicio__c> lst_ls = new List <BI_Linea_de_Servicio__c>();
		Id rtId;
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(
			BI_Estado__c = 'Abierto',
			BI_Cliente__c = lst_acc[0].id
		);
		System.runAs(usu){
			insert sol;
		}

		sol.BI_Estado_de_ventas__c = 'Aprobado';
		update sol;

		BI_Descuento__c descuento = new BI_Descuento__c(
			BI_Cliente__c = lst_acc[0].id,
			BI_Descuento_aplica__c = 'Línea de venta',
			BI_Oferta__c = 'OfertaTest',
			BI_Origen_de_descuento__c = 'Pricing',
			BI_Periodo_de_recambio__c = 12,
			BI_Estado_del_Proceso__c = 'Pricing'
		);
		insert descuento;

		for(Recordtype rt : lst_rt){
			if(rt.DeveloperName == 'BI_Modelo'){
				rtId=rt.id;
			}
		}

		BI_Modelo__c sim = new BI_Modelo__c(
				BI_Nombre_de_producto__c = 'sim',
				RecordTypeId = rtId,
				BI_Familia_Local__c = 'SIMCARD'
		);
		insert sim;

		BI_Modelo__c modelo = new BI_Modelo__c(
			BI_Nombre_de_producto__c = 'movil',
			RecordTypeId = rtId,
			BI_Tipo_de_SIM__c = sim.id,
			BI_Familia_Local__c = '03 Smartphone Entry'
		);
		insert modelo;

		for(Recordtype rt : lst_rt){
			if(rt.DeveloperName == 'BI_Servicio'){
				rtId=rt.id;
			}
		}

		BI_Modelo__c servicio = new BI_Modelo__c(
			BI_Nombre_de_producto__c = 'Servicio Smartphone',
			BI_Familia_Local__c = 'Smartphone',
			RecordTypeId = rtId
		);
		insert servicio;

		for(Integer i = 0; i < 4 ; i++){
				
			BI_Descuento_para_modelo__c descM = new BI_Descuento_para_modelo__c(
				BI_Cantidad__c = 100,
				BI_Cantidad_disponible__c = 100,
				BI_Descuento__c = descuento.Id,
				BI_Descuento_porcentaje__c = 20,
				BI_Estado__c = 'Activo',
				BI_ID_de_Modelo__c = modelo.Id
			);	
			lst_dm.add(descM);
		}

		for(Integer i = 0; i < 4 ; i++){
				
			BI_Descuento_para_modelo__c descM = new BI_Descuento_para_modelo__c(
				BI_Cantidad__c = 120,
				BI_Cantidad_disponible__c = 100,
				BI_Descuento__c = descuento.Id,
				BI_Descuento_porcentaje__c = 20,
				BI_Estado__c = 'Activo',
				BI_ID_de_Modelo__c = sim.Id
			);	
			lst_dm.add(descM);
		}

		insert lst_dm;

		for(Integer i = 0; i < 4; i++){

			BI_Linea_de_Venta__c lv = new BI_Linea_de_Venta__c(
				BI_Estado_de_linea_de_venta__c = 'Pendiente',
	            BI_Modalidad_de_venta__c = '1',
	            BI_Solicitud__c = sol.Id,
	            BI_Cantidad_de_equipos__c = 1,
	            BI_Inlcuir_tarjeta_SIM__c = 'Sí',
	            BI_Descuenta_puntos_Movistar_SIM__c = 'No',
	            BI_Descuenta_puntos_Movistar__c = 'No',
	            BI_Codigo_de_descuento_de_modelo__c = lst_dm[i].Id,
	            BI_Codigo_de_descuento_de_SIM__c = lst_dm[i+4].Id,
	            BI_Modelo__c = modelo.Id,
	            BI_ID_de_simcard__c = sim.Id,
	            BI_Plan__c = 'Plan en construcción',
	            BI_Nuevo_codigo_de_cliente__c = true,
	            BI_Codigo_de_cliente_final__c = lst_acc[0].Name,
	            BI_RUT_hijo__c = 'RutHijo',
	            BI_Nombre_persona__c = 'Patricia Arredondo',
	            BI_Codigo_cliente_hijo__c = 'codigoHijo',
	            BI_Tipo_de_Contrato__c = '91'
	        );

			lst_lv.add(lv);
		}
		
        insert lst_lv;

        for(Integer i = 0; i < 4; i++){
	    
	        BI_Linea_de_Servicio__c ls = new BI_Linea_de_Servicio__c(
	        	BI_Modelo__c = servicio.Id,
	        	BI_Linea_de_Venta__c = lst_lv[i].Id
	        );
        	lst_ls.add(ls);
    	}
        
        insert lst_ls;

        List <Id> ids = new List<Id>();
        ids.add(sol.Id);

        BI_DevolverDescuentoModelo_JOB.executeJobDevolverDescuentos(new List <Id> (ids));
		
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Test Method that manage the code coverage of BI_DevolverDescuentoModelo_JOB
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void returnDiscountsLineaDeRecambio() {
		
		BI_TestUtils.throw_exception=false;
		List <Recordtype> lst_rt = [SELECT Id,DeveloperName FROM Recordtype WHERE DeveloperName = 'BI_Modelo' OR DeveloperName = 'BI_Servicio'];
		List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});
		List <BI_Descuento_para_modelo__c> lst_dm = new List <BI_Descuento_para_modelo__c>();
		List <BI_Linea_de_Recambio__c> lst_lr = new List <BI_Linea_de_Recambio__c>();
		List <BI_Linea_de_Servicio__c> lst_ls = new List <BI_Linea_de_Servicio__c>();
		Id rtId;

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(
			BI_Estado__c = 'Abierto',
			BI_Cliente__c = lst_acc[0].id
		);
		insert sol;

		BI_Descuento__c descuento = new BI_Descuento__c(
			BI_Cliente__c = lst_acc[0].id,
			BI_Descuento_aplica__c = 'Línea de recambio',
			BI_Oferta__c = 'OfertaTest',
			BI_Origen_de_descuento__c = 'Pricing',
			BI_Periodo_de_recambio__c = 12,
			BI_Estado_del_Proceso__c = 'Pricing'
		);
		insert descuento;

		for(Recordtype rt : lst_rt){
			if(rt.DeveloperName == 'BI_Modelo'){
				rtId=rt.id;
			}
		}

		BI_Modelo__c sim = new BI_Modelo__c(
				BI_Nombre_de_producto__c = 'sim',
				RecordTypeId = rtId,
				BI_Familia_Local__c = 'SIMCARD'
		);
		insert sim;

		BI_Modelo__c modelo = new BI_Modelo__c(
			BI_Nombre_de_producto__c = 'movil',
			RecordTypeId = rtId,
			BI_Tipo_de_SIM__c = sim.id,
			BI_Familia_Local__c = '03 Smartphone Entry'
		);
		insert modelo;

		for(Recordtype rt : lst_rt){
			if(rt.DeveloperName == 'BI_Servicio'){
				rtId=rt.id;
			}
		}

		BI_Modelo__c servicio = new BI_Modelo__c(
			BI_Nombre_de_producto__c = 'Servicio Smartphone',
			BI_Familia_Local__c = 'Smartphone',
			RecordTypeId = rtId
		);
		insert servicio;

		for(Integer i = 0; i < 4 ; i++){
				
			BI_Descuento_para_modelo__c descM = new BI_Descuento_para_modelo__c(
				BI_Cantidad__c = 100,
				BI_Cantidad_disponible__c = null,
				BI_Descuento__c = descuento.Id,
				BI_Descuento_porcentaje__c = 20,
				BI_Estado__c = 'Activo',
				BI_ID_de_Modelo__c = modelo.Id
			);	
			lst_dm.add(descM);
		}

		for(Integer i = 0; i < 4 ; i++){
				
			BI_Descuento_para_modelo__c descM = new BI_Descuento_para_modelo__c(
				BI_Cantidad__c = 0,
				BI_Cantidad_disponible__c = null,
				BI_Descuento__c = descuento.Id,
				BI_Descuento_porcentaje__c = 20,
				BI_Estado__c = 'Activo',
				BI_ID_de_Modelo__c = sim.Id
			);	
			lst_dm.add(descM);
		}
		insert lst_dm;


		for(Integer i = 0; i < 4; i++){

			BI_Linea_de_Recambio__c lr = new BI_Linea_de_Recambio__c(
				BI_Estado_de_linea_de_recambio__c = 'Pendiente',
	            BI_Modalidad_de_venta__c = '1',
	            BI_Solicitud__c = sol.Id,
	            BI_Cantidad_de_equipos__c = 1,
	            BI_Descuenta_puntos_Movistar_SIM__c = 'No',
	            BI_Descuenta_puntos_Movistar__c = 'No',
	            BI_Codigo_de_descuento_de_modelo__c = lst_dm[i].Id,
	            BI_Codigo_de_descuento_de_SIM__c = lst_dm[i+4].Id,
	            BI_Modelo__c = modelo.Id,
	            BI_ID_de_simcard__c = sim.Id,
	            BI_Otro_Plan__c = 'Plan en construcción',
	            BI_Codigo_de_cliente_final__c = lst_acc[0].Name,
	            BI_RUT_hijo__c = 'RutHijo',
	            BI_Nombre_persona__c = 'Patricia Arredondo',
	            BI_Codigo_cliente_hijo__c = 'codigoHijo',
	            BI_Tipo_de_Contrato__c = '91',
	            BI_Razon_para_recambio__c = 'razoncilla'
	        );

			lst_lr.add(lr);
		}
		
       
        insert lst_lr;

        for(Integer i = 0; i < 4; i++){
	    
	        BI_Linea_de_Servicio__c ls = new BI_Linea_de_Servicio__c(
	        	BI_Modelo__c = servicio.Id,
	        	BI_Linea_de_Recambio__c = lst_lr[i].Id
	        );
        	lst_ls.add(ls);
    	}
        
        insert lst_ls;

        

        List <Id> ids = new List<Id>();
        ids.add(sol.Id);

        BI_DevolverDescuentoModelo_JOB.executeJobDevolverDescuentos(new List <Id> (ids));

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Test Method that manage the code coverage of BI_DevolverDescuentoModelo_JOB
    
    History:
    
    <Date>            <Author>          <Description>
    30/10/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void excepciones(){

		BI_TestUtils.throw_exception=true;
		BI_DevolverDescuentoModelo_JOB obj = new BI_DevolverDescuentoModelo_JOB();
		obj.divide(null, null);
		obj.nextExecute();
		obj.searchDiscounts(null);
		obj.updateModelos(null);
		BI_DevolverDescuentoModelo_JOB.executeJobDevolverDescuentos(null);
		BI_TestUtils.throw_exception=false;


	}
	
}