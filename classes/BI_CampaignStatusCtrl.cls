public without sharing class BI_CampaignStatusCtrl {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Controller class for BI_CampaignStatus page
    History:
    
    <Date>            <Author>          <Description>
    31/07/2014        Micah Burgos      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

 
    public Campaign Camp { get; set; }
    
    public Boolean displayApprovalBt { get; set; }

    public String StatusColor1 { get; set; }
    public String StatusColor2 { get; set; }
    public String StatusColor3 { get; set; }
    public String StatusColor4 { get; set; }
    public String StatusColor5 { get; set; }
    public map<String,list<String>> map_actions {get;set;}
    
    private final String red    = Label.BI_ColorRed;
    private final String green  = Label.BI_ColorGreen;
    private final String grey   = Label.BI_ColorGrey;
    private final String orange = Label.BI_ColorOrange;
    private final String blue   = Label.BI_ColorBlue;
    
    public Boolean error { get; set; }
    
    private final map<String,String> map_status_label = new map<String,String>();

    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Constructor Method (calculates the stages colors and elements displayed)
    
    IN:            ApexPages.StandardController (Campaign)
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    31/07/2014        Micah Burgos      Initial version
    05/05/2016        Antonio Pardo     New condition to show button if Venta productos and BI_Crear_oportunidad_contactos__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_CampaignStatusCtrl(ApexPages.StandardController controller){
    // PLL_PickList_Refactorizacion_Necesaria (Refactorizado por Micah Burgos)
        try{
            
            Camp = (Campaign)controller.getRecord();

            Camp = [select Name, Id, BI_Objetivo_de_la_accion__c, BI_Country__c,BI_Crear_oportunidad_contactos__c, Status from Campaign where Id = :Camp.Id limit 1];

            //Status=>Labels 
            map_status_label.put(Label.BI_EnPlanificacion,'BI_En_planificacion_Campana');
            map_status_label.put(Label.BI_EnAprobacion,'BI_En_aprobacion_campanas');
            map_status_label.put(Label.BI_EnCurso,'BI_En_curso_campanas');
            map_status_label.put(Label.BI_Finalizada,'BI_Finalizada_Campanas');
            map_status_label.put(Label.BI_Cancelada,'BI_Cancelada_Campanas');
            
            
            //Status Color  
            if(Camp.Status == Label.BI_EnPlanificacion){
                StatusColor1 = orange; 
                StatusColor2 = blue; 
                StatusColor3 = blue;
                StatusColor4 = blue;
                StatusColor5 = blue; 
            }else if(Camp.Status == Label.BI_EnAprobacion){
                StatusColor1 = green; 
                StatusColor2 = orange; 
                StatusColor3 = blue;
                StatusColor4 = blue;
                StatusColor5 = blue; 
            }else if(Camp.Status == Label.BI_EnCurso){
                StatusColor1 = green; 
                StatusColor2 = green; 
                StatusColor3 = orange;
                StatusColor4 = blue;
                StatusColor5 = blue;
            }else if(Camp.Status == Label.BI_Finalizada){
                StatusColor1 = green; 
                StatusColor2 = green; 
                StatusColor3 = green;
                StatusColor4 = orange;
                StatusColor5 = blue;
            }else if(Camp.Status == Label.BI_Cancelada){
                StatusColor1 = blue; 
                StatusColor2 = blue; 
                StatusColor3 = blue;
                StatusColor4 = blue;
                StatusColor5 = red;
            }else{
                StatusColor1 = grey; 
                StatusColor2 = grey; 
                StatusColor3 = grey;
                StatusColor4 = grey;
                StatusColor5 = grey;
            }
                
                    
                String userPermission = BI_UserMethods.getPermission();
                
                //Approval Button
                displayApprovalBt = true;
                if(Camp.Status == Label.BI_EnPlanificacion && (
                                (userPermission == Label.BI_Desarrollo_Comercial ||
                                 userPermission == Label.BI_Administrador_del_sistema ||
                                 userPermission == Label.BI_SuperUsuario)
                                )){
                    if(Camp.BI_Objetivo_de_la_accion__c == Label.BI_VentaProductos && Camp.BI_Crear_oportunidad_contactos__c == Label.BI_Si){
                        List<BI_Promociones__c> list_Promo = new List<BI_Promociones__c>([SELECT BI_Campanas__c FROM BI_Promociones__c WHERE BI_Campanas__c = :Camp.Id]);
                        if(list_Promo.isEmpty()){
                            displayApprovalBt = false;
                        } 
                    }
                    
                    List<CampaignMember> list_CampMemb = new List<CampaignMember>([SELECT Id,CampaignId FROM CampaignMember WHERE CampaignId = :Camp.Id]);
                    if(list_CampMemb.isEmpty()){
                        displayApprovalBt = false;
                    }
                }else {
                    displayApprovalBt = false;
                }
                
                
                
                error = false;
                
                
                //Actions 
                List<BI_CampaignAction__c> list_actions = new List<BI_CampaignAction__c>();
                if(Camp.BI_Country__c != null){
                    list_actions = [select Id,  BI_Content__c, BI_Pais__c, BI_Label__c, BI_Position__c from BI_CampaignAction__c where  BI_Pais__c= :  Camp.BI_Country__c OR BI_Pais__c= 'CORE' order by BI_Position__c];    
                }else{
                    list_actions = [select Id,  BI_Content__c, BI_Pais__c, BI_Label__c, BI_Position__c from BI_CampaignAction__c where BI_Pais__c = 'CORE' order by BI_Position__c];
                }

                map_actions = new map<String,list<String>>();
                system.debug('BI_CampaignStatusCtrl->list_actions: '+list_actions);
                if(!list_actions.isEmpty()){ 
                    for(BI_CampaignAction__c act :list_actions){
                        
                        
                        if(map_actions.containsKey(act.BI_Label__c)){
                            map_actions.get(act.BI_Label__c).add(act.BI_Content__c);
                        }else{
                            list<String>list_content = new list<String>();
                            list_content.add(act.BI_Content__c);
                            map_actions.put(act.BI_Label__c,list_content);
                        }   
                    }
                }
                system.debug('BI_CampaignStatusCtrl->map_actions: '+map_actions);
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignStatusCtrl.BI_CampaignStatusCtrl', 'BI_EN', Exc, 'Controladora VF');
        }
    
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Update the opportunity fields and send to approval (Stage 1.- Label.BI_EstadoAprobacion_Riesgo)
    
    IN:            void
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    22/05/2014        Pablo Oliva       Initial version
    28/03/2016        Guillermo Muñoz   NO_APPLICABLE_PROCESS exception message changed.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public void sendToApproval(){
        error = false;
        
        try{
        
            update Camp;
            
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        
            req1.setComments(Label.BI_EnviarAprobacion);  //'Sending for approval'
            req1.setObjectId(Camp.Id);   
     
            Approval.ProcessResult result = Approval.process(req1);
        }catch (exception Exc){
            error = true;
            
            if(Exc.getMessage().contains('ALREADY_IN_PROCESS')){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.BI_AlreadyInProcess);
                ApexPages.addMessage(myMsg);
            }else if(Exc.getMessage().contains('MANAGER_NOT_DEFINED')){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.BI_Gestor_user);
                ApexPages.addMessage(myMsg);
            }else if(Exc.getMessage().contains('NO_APPLICABLE_PROCESS')){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.BI_Error_campana_sin_proceso);
                ApexPages.addMessage(myMsg);
            }
            else{
                ApexPages.addMessages(Exc);
            }
        
           BI_LogHelper.generate_BILog('BI_ApprovalStatus_CTRL.sendToApproval', 'BI_EN', Exc, 'Controller');
        }
    }
    
    public Boolean getRenderPlanificacion(){
        return map_actions.containsKey(map_status_label.get(Label.BI_EnPlanificacion));
    }
    
    public Boolean getRenderAprobacion(){
        return map_actions.containsKey(map_status_label.get(Label.BI_EnAprobacion));
    }
    
    public Boolean getRenderEnCurso(){
        return map_actions.containsKey(map_status_label.get(Label.BI_EnCurso));
    }
    
    public Boolean getRenderFinalizada(){
        return map_actions.containsKey(map_status_label.get(Label.BI_Finalizada_Campanas));

    }
    
    public Boolean getRenderCancelada(){
        return map_actions.containsKey(map_status_label.get(Label.BI_Cancelada_Campanas));
    }
    
}