@isTest(seeAllData = false)
public class BIIN_CISearch_Creacion_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_CISearch_Creacion_Ctrl

    <Date>             <Author>                        <Change Description>
    11/2015            José María Martín               Initial Version
    11/05/2016         José Luis González Beltrán      Adapt TEST to UNICA interface modifications in tested class
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void test_BIIN_CISearch_Creacion_TEST() {
        BI_TestUtils.throw_exception = false;
        User ActualUser = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(), BI_DataLoad.searchAdminProfile());
        
        System.runAs(ActualUser)
        {      
            // Aquí empieza la construcción de lista de casos
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item : acc) {
                item.OwnerId = ActualUser.Id;
                accList.add(item);
            }
            update accList;
            List<Opportunity> oppList = new List<Opportunity>();
            oppList = BI_DataLoad.loadOpportunitiesContract(1, accList[0]);
            List<RecordType> rT = [SELECT Id, Name FROM Recordtype WHERE Name = 'Solicitud Incidencia Técnica'];
            List<Case> caseList = new List<Case>();
            caseList = BI_DataLoad.loadCases(accList, oppList, rT);
            update caseList;
            // Aquí acaba la construcción de lista de casos
            
            Case caso = new Case(accountId = accList[0].Id);
       	    insert caso;  
            
            Test.startTest(); 

            PageReference p = Page.BIIN_Detalle_Incidencia;
            p.getParameters().put('inputfield', 'Lg');
            p.getParameters().put('accountfield', accList[0].id);
            p.getParameters().put('sitefield', '');
            Test.setCurrentPageReference(p);
            
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CISearch'));  
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BIIN_CISearch_Creacion_Ctrl ceController = new BIIN_CISearch_Creacion_Ctrl();
            ceController.getHttpRequest();
            ceController.getHttpResponse();
            
            ceController.getCI();
            ceController.HabilitarIr();
            ceController.closePopUp();

            Test.stopTest();
        }
    }

}