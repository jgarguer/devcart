@isTest
public class TGS_ValidationBPHandler_Test {
    
    static testmethod void addErrorBP_Test(){
    
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               WHERE Name = :profile
                               LIMIT 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.id;
            insert uO;
        }
        
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            Test.StartTest();
            NE__OrderItem__c orderitem = TGS_Dummy_Test_Data.dummyConfigurationOrderCCAndBU();

        	NE__Order__c testOrder = [SELECT id, Name, NE__ServAccId__c, NE__BillAccId__c FROM NE__Order__c WHERE id =: orderitem.NE__OrderID__c LIMIT 1];
          
            Case testCase =[SELECT id, AccountId FROM Case WHERE Order__c =: testOrder.id LIMIT 1];
            
            NE__Product__c testProduct = [SELECT id,Name FROM NE__Product__c WHERE id =: orderitem.NE__ProdId__c LIMIT 1];
            
            Account testLegalEntity1 = [SELECT id, Name, ParentId FROM Account WHERE id =: testCase.AccountId LIMIT 1];
            Account testCustomerCountry1 = [SELECT id,Name FROM Account WHERE id =: testLegalEntity1.ParentId LIMIT 1];
            Account testCostCenter1 = [SELECT id,Name FROM Account WHERE Parent.ParentId =: testLegalEntity1.id LIMIT 1];
            Account testBusinessUnit1 = [SELECT id,Name FROM Account WHERE ParentId =: testLegalEntity1.id LIMIT 1];
            
            Account testLegalEntity2 = TGS_Dummy_Test_Data.dummyHierarchy();
            Account testCustomerCountry2 = [SELECT id,Name FROM Account WHERE id =: testLegalEntity2.ParentId LIMIT 1];
            Account testCostCenter2 = [SELECT id,Name,Parentid FROM Account WHERE Parent.ParentId =: testLegalEntity2.Id LIMIT 1];
            Account testBusinessUnit2 = [SELECT id,Name FROM Account WHERE ParentId =: testLegalEntity2.id LIMIT 1];

            NE__Billing_Profile__c testBillingProfile1 = new NE__Billing_Profile__c(NE__Account__c = testLegalEntity1.id);
           	insert testBillingProfile1;
            
            NE__Billing_Profile__c testBillingProfile2 = new NE__Billing_Profile__c(NE__Account__c = testLegalEntity1.id, TGS_Product__c = testProduct.id, TGS_Customer_Country__c = testCustomerCountry1.id);
           	insert testBillingProfile2;
            
            
           
     		testCase.Status = Constants.CASE_STATUS_RESOLVED;
            testCase.TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
            update testCase;           
            
            Test.stopTest();
        }
    }

	static testmethod void addErrorBP_errorDistribuido_Test(){
    
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               WHERE Name = :profile
                               LIMIT 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.id;
            insert uO;
        }
        
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            Test.startTest();
            
            NE__OrderItem__c orderitem = TGS_Dummy_Test_Data.dummyConfigurationOrderCCAndBU();

        	NE__Order__c testOrder = [SELECT id, Name, NE__ServAccId__c, NE__BillAccId__c FROM NE__Order__c WHERE id =: orderitem.NE__OrderID__c LIMIT 1];
          
            Case testCase =[SELECT id, AccountId FROM Case WHERE Order__c =: testOrder.id LIMIT 1];
            
            NE__Product__c testProduct = [SELECT id,Name FROM NE__Product__c WHERE id =: orderitem.NE__ProdId__c LIMIT 1];
            
            Account testLegalEntity1 =    [SELECT Id, Name, ParentId FROM Account WHERE Id =: testCase.AccountId LIMIT 1];
            Account testCustomerCountry1 = [SELECT Id, Name, ParentId FROM Account WHERE Id =: testLegalEntity1.ParentId LIMIT 1];
            Account testCostCenter1 =     [SELECT Id, Name FROM Account WHERE Parent.ParentId =: testLegalEntity1.Id LIMIT 1];
            Account testBusinessUnit1 =   [SELECT Id, Name FROM Account WHERE ParentId =: testLegalEntity1.Id LIMIT 1];

            // Crear otra rama en la jerarquía del holding
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY);
            Account testCustomerCountry2 = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Customer_Country + String.valueOf(Math.random()),
                RecordTypeId = rtId,
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                BI_Country__c = 'Spain',
                TGS_Es_MNC__c=true,
                TGS_Aux_Holding__c = testCustomerCountry1.ParentId,
                ParentId = testCustomerCountry1.ParentId);
            insert testCustomerCountry2;
            
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
            Account testLegalEntity2 = new Account(
                Name = 'Account Test Name - ' + Label.TGS_Legal_Entity + String.valueOf(Math.random()),
                RecordTypeId = rtId,
                ParentId = testCustomerCountry2.Id,
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c = 'Corporate',
                BI_Country__c = 'Spain',
                TGS_Es_MNC__c = true,
                TGS_Aux_Holding__c = testCustomerCountry2.ParentId);
            insert testLegalEntity2;
            
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
            Account testBusinessUnit2 = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Business_Unit + String.valueOf(Math.random()),
                RecordTypeId = rtId,
                ParentId = testLegalEntity2.Id,
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c = True,
                TGS_Aux_Holding__c = testCustomerCountry2.ParentId);
            insert testBusinessUnit2;
            
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
            Account testCostCenter2 = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Cost_Center + String.valueOf(Math.random()),
                RecordTypeId = rtId,
                ParentId = testBusinessUnit2.Id,
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c=true,
                TGS_Aux_Holding__c = testCustomerCountry2.ParentId,
                TGS_Aux_customer_Country__C = testCustomerCountry2.Id
                );
            insert testCostCenter2;
			
            // Asociar el CC de la orden con un CC de la nueva rama: el "país" (Customer Country) del CC será diferente al del LE de la orden
            testOrder.NE__BillAccId__c = testCostCenter2.Id;
            update testOrder;
            
           	
            try {
                testCase.Status = Constants.CASE_STATUS_RESOLVED;
                update testCase;
                System.assert(false, 'No debería permitir asetizar una orden con una CC de ddiferente país al país de la LE');
            } catch(Exception exc) {
            }
            
            Test.stopTest();
        }
    }    
    
}