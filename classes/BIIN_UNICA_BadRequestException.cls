public class BIIN_UNICA_BadRequestException extends Exception 
{
/*----------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González Beltrán
    Company:       HPE
    Description:   Bad Request Exception to handle rest services exceptions
      
    History:
      
    <Date>              <Author>                          	<Description>
    10/05/2016          José Luis González Beltrán          Initial version
----------------------------------------------------------------------------------------------------------------------*/
}