public without sharing class PCA_EquipoTelefonica_PopUpCtrl   extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show equipo telefonica details 
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public string userName          {get; set;}
    public string EmailUser         {get; set;}
    public string PhoneUser         {get; set;}
    public string MobilePhoneUser   {get; set;}
    public string Role              {get; set;}
    public string Bigphoto          {get; set;}
    
    public boolean haveError        {get; set;}
    
    public Id UserId                {get; set;}
    public string Type              {get; set;}
    public string Back              {get; set;}
    public string teamType;
        
    public string aboutme{get;set;}
    public string emailTo{get;set;}
    public string emailFrom {get; set;}
    public string emailSubject {get; set;}
    public string emailBody {get; set;}

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.checkPermissions', 'Portal Platino', e.getMessage(), 'Class');
           return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_EquipoTelefonica_PopUpCtrl(){}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of teams
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){ 
        
        List<User> UserifOwner = new List<User>();
        
        List<User> internalUser = new List<User>();
        
        List<String> listAux = new List<String>();
        
        List<Hierarchy__c> UserHierarchy = new List<Hierarchy__c>();
        
        List<Account> OwnAccount = new List<Account>();
        
        List<AccountTeamMember> listAccountTeam = new List<AccountTeamMember>();
        
        User user;
        
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            UserId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            
            Type = (apexpages.currentpage().getparameters().get('Type')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('Type')):null;
            
            Back = (apexpages.currentpage().getparameters().get('back')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('back')):null;
            
            teamType = (apexpages.currentpage().getparameters().get('clase')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('clase')):null;
            
            UserHierarchy = [Select Id, AboutMe__c, MobilePhone__c, ShowPersonalData__c, Description__c, User__c, NameUser__c, Contact__c, 
                                Email__c, Phone__c, Nivel_de_escalado__c, Role__c From Hierarchy__c where User__c =: UserId];
            
            if(Back != 'false' || Back == null){
                Back = 'true';
            }
            
            internalUser = [Select Id, Name, ContactId, Email,Contact.AccountId, phone, aboutme, MobilePhone, UserRole.Name From User where id = : UserId limit 1];
            
            UserifOwner = [Select Id, Name, ContactId, Email,Contact.AccountId, phone, aboutme, MobilePhone, UserRole.Name From User where id = :  Userinfo.getUserId() limit 1];       
            
            if(UserifOwner[0].Contact.AccountId != null){
                OwnAccount = [SELECT Owner.Id FROM Account where Id = : BI_AccountHelper.getCurrentAccountId()];            
            }
            
            listAccountTeam = [Select UserId, User.Name,User.Phone,User.MobilePhone,User.Email, User.AboutMe,User.UserRole.Name, toLabel(TeamMemberRole), Id, AccountAccessLevel 
                            From AccountTeamMember 
                            where UserId= : UserId];
            
            if(getPhotoConnectApi(UserId)!=null){
                listAux = getPhotoConnectApi(UserId);
                if(!listAux.isEmpty()){
                    Bigphoto = listAux[0];
                }
            }
            system.debug('*******internalUser: ' +internalUser);
            String escaladoStr = 'escalado';
            if(teamType == escaladoStr && (!UserHierarchy.isEmpty()) && (UserHierarchy[0].ShowPersonalData__c == false)){
                EmailUser = Label.PCA_NoDisponible;
                PhoneUser = Label.PCA_NoDisponible; 
                MobilePhoneUser = Label.PCA_NoDisponible;
            }
            else{
                EmailUser = internalUser[0].Email;
                PhoneUser = internalUser[0].phone; 
                MobilePhoneUser = internalUser[0].MobilePhone;
            }
            userName = internalUser[0].Name;
            aboutme = internalUser[0].aboutme;
            if(teamType=='Equipo'){
                system.debug('****UserifOwner[0].Contact.AccountId: ' +UserifOwner[0].Contact.AccountId);
                system.debug('*****UserId' +UserId);
                if(UserifOwner[0].Contact.AccountId != null && OwnAccount[0].Owner.Id == UserId){               
                    
                    Role = Label.PCA_Propietario;   
                
                }else{
                    Role = listAccountTeam[0].TeamMemberRole;}
            }else if(teamType == escaladoStr){
                Role = internalUser[0].UserRole.Name;
            }
            user = [Select Id, Name, ContactId, Email, phone, FullPhotoUrl, Contact.AccountId From User where id = : Userinfo.getUserId()];
            emailFrom = user.Email;
            system.debug('*****InternalUser: '+ internalUser);
            system.debug('*****userName: ' +userName);
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.loadInfo', 'Portal Platino', e.getMessage(), 'Class');
        }
            
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load chatter photo of the user, given user id
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public list<string> getPhotoConnectApi (string id){
        try{
            ConnectApi.Photo photo; 
            try{        
                photo = ConnectApi.ChatterUsers.getPhoto(null, Id);
                system.debug(photo.smallPhotoUrl);
            }catch(Exception e){
                system.debug(e.getMessage());
            }
            system.debug(photo);
            list<string> listString = new list<String>();
            if(photo!=null){
                listString.add(photo.largePhotoUrl);
            }
            system.debug(listString);
            return listString;
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.getPhotoConnectApi', 'Portal Platino', e.getMessage(), 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Send email or SMS depends on the type of operation
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void send(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            if(Type == 'SMS'){
                sendSMS();
            }else if(Type == 'EMAIL'){
                    sendEmail();
                }
        }catch (exception e){
           BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.send', 'Portal Platino', e.getMessage(), 'Class');
        }
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Send email to the user
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void sendEmail(){
        try{
            haveError = false;
            if(Test.isRunningTest()){
                emailBody = 'Body for Test PCA';        
                emailSubject = 'Subject for Test PCA'; 
                emailFrom = 'fromfake@pac.com';
                emailTo = 'tofake@pac.com';
            } 
            if(emailBody == '' || emailSubject == '' ||  emailFrom == '' ||  emailUser == ''){
                apexpages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Rellene todos los campos'));
                haveError = true;
                return;
            }
        


            Messaging.SingleEmailMessage mail3 = new Messaging.SingleEmailMessage();
            String userEmail = emailUser;
            //userEmail = 'diego.arenas@aborda.es';
            //String EmailToCase = 'diego.arenas@y-m2w0efx0oy6mimvpyhilo9zn8kgxgsjry1nlbnovdsciasew7.i-kfppea0.il.case.salesforce.com';
            //mail3.setBccAddresses(new String[] {EmailToCase});
            //mail3.setSenderDisplayName('Do Not Reply');
            mail3.setToAddresses(new String[] {userEmail});
            mail3.setSaveAsActivity(true);
            //mail3.setTargetObjectId('003i0000007ij3A');
            //mail3.setWhatId(user.Id);
            mail3.setReplyTo(emailTo);
            mail3.setSubject(emailSubject);
            mail3.setPlainTextBody(emailBody);
            system.debug('### enviarEmail: ' + mail3);
            List<Messaging.SendEmailResult> results =  Messaging.sendEmail(new Messaging.Email[] { mail3 });
            system.debug('### enviarEmail results: ' + results);
        }catch (exception e){
               BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.sendEmail', 'Portal Platino', e.getMessage(), 'Class');
        }
    }   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Send SMS to the user
    
    History:
    
    <Date>            <Author>              <Description>
    02/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void sendSMS(){
        try{
            haveError = false;
            if(Test.isRunningTest()){
                emailBody = 'Body for Test PCA';        
                emailSubject = 'Subject for Test PCA'; 
                emailFrom = 'fromfake@pac.com';
                emailTo = 'tofake@pac.com';
            } 
            if(emailBody == '' ||  emailFrom == '' ||  emailUser == ''){
                apexpages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Rellene todos los campos'));
                haveError = true;
                return;
            }
            
    
    
            Messaging.SingleEmailMessage mail3 = new Messaging.SingleEmailMessage();
            String userEmail = emailUser;
            //userEmail = 'diego.arenas@aborda.es';
            //String EmailToCase = 'diego.arenas@y-m2w0efx0oy6mimvpyhilo9zn8kgxgsjry1nlbnovdsciasew7.i-kfppea0.il.case.salesforce.com';
            //mail3.setBccAddresses(new String[] {EmailToCase});
            //mail3.setSenderDisplayName('Do Not Reply');
            mail3.setToAddresses(new String[] {userEmail});
            mail3.setSaveAsActivity(true);
            //mail3.setTargetObjectId('003i0000007ij3A');
            //mail3.setWhatId(user.Id);
            mail3.setReplyTo(emailTo);
            mail3.setSubject('SMS Account Team');
            mail3.setPlainTextBody(emailBody);
            system.debug('### enviarEmail: ' + mail3);
            List<Messaging.SendEmailResult> results =  Messaging.sendEmail(new Messaging.Email[] { mail3 });
            system.debug('### enviarEmail results: ' + results);
        }catch (exception e){
               BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.sendSMS', 'Portal Platino', e.getMessage(), 'Class');
        }
    }   
    
}