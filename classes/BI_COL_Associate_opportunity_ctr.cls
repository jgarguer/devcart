/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-20      Daniel ALexander Lopez (DL)     Cloned Class   
* @version   2.0    2015-08-19      Oscar Alejandro Jimenez (OJ)    Increase coverage
*************************************************************************************/

public with sharing class BI_COL_Associate_opportunity_ctr {

  public list<Opportunity> asociadas{get;set;}
  public list<WrapperOpt> optSeleccionadas{get;set;}
  private string id;

	public BI_COL_Associate_opportunity_ctr() 
	{
		
	    optSeleccionadas=new list<WrapperOpt>();
      asociadas = new list<Opportunity>();
	    
	    id = ApexPages.currentPage().getParameters().get('id');
	    
      if(!Test.isRunningTest())
      {
	        Opportunity optPadre=[select id,name,BI_Numero_id_oportunidad__c,accountid from Opportunity where id=:id];
          asociadas = [select id,name,BI_Numero_id_oportunidad__c from Opportunity where id!=:id and accountid=:optPadre.accountid and BI_Oportunidad_Padre__c=null];
      }
	    System.debug('asociadas ======= '+asociadas);
	    if(asociadas.isEmpty()){
	      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No se encontraron oportunidades disponibles'));
	    }
	    for(Opportunity optA:asociadas){
	      optSeleccionadas.add(new WrapperOpt(false,optA));
	    }

	}

	public Pagereference seleccionar(){
    
    list<Opportunity> oportunidadesA=new list<Opportunity>();    
    for(WrapperOpt w:optSeleccionadas){
      if(w.seleccion){
        w.oportunidad.BI_Oportunidad_Padre__c=id;
        oportunidadesA.add(w.oportunidad);    
      }
          
    }
    
    if(oportunidadesA.isEmpty()){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No ha seleccionado ninguna oportunidad'));
      return null;
    }
    try{
      update oportunidadesA;
      return cancelar();
    }catch(exception e){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error:'+e.getMessage()));  
    }
    
    return null;
  }
  
  public Pagereference cancelar(){
    
    PageReference paginaInicio= new PageReference('/'+Id);
        paginaInicio.setRedirect(true);
    
    return paginaInicio;
  }
  
  public class WrapperOpt{
    
    public Boolean seleccion{get;set;}
    public Opportunity oportunidad{get;set;}
    
    public WrapperOpt(Boolean seleccion,Opportunity oportunidad){
      this.seleccion=seleccion;
      this.oportunidad=oportunidad;
    }
  }
}