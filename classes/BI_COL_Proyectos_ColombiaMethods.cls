public with sharing class BI_COL_Proyectos_ColombiaMethods {
	

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Method that assign the Proyecto Colombia to the configurated owner
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    07/06/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setOwner(List <BI_COL_Proyectos_Colombia__c> news, List <BI_COL_Proyectos_Colombia__c> olds){

		try{

			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('Test');
			}

			Set <Id> set_idOpp = new Set <Id>();
			List <BI_COL_Proyectos_Colombia__c> toProcess = new List <BI_COL_Proyectos_Colombia__c>();
	
			for(BI_COL_Proyectos_Colombia__c pc : news){
				if(pc.BI_COL_Oportunidad__c != null){
					set_idOpp.add(pc.BI_COL_Oportunidad__c);
					toProcess.add(pc);
				}
			}
			//Select the segment of the accounts
			List <Opportunity> lst_opp = [SELECT Id, Account.BI_Segment__c FROM Opportunity WHERE Id IN: set_idOpp];
	
			if(!lst_opp.isEmpty()){
	
				Map <Id,String> map_segmento = new Map <Id, String>();
	
				for(Opportunity opp : lst_opp){
					map_segmento.put(opp.Id, opp.Account.BI_Segment__c);
				}
	
				List <BI_COL_Asignacion_proyectos__c> lst_cs = BI_COL_Asignacion_proyectos__c.getAll().values();
	
				if(!lst_cs.isEmpty()){
					//get the custom setting configuration to assign the owner
					Map <String, BI_COL_Asignacion_proyectos__c> map_ap = new Map <String, BI_COL_Asignacion_proyectos__c>();
					Set <String> set_colas = new Set <String>();
					Map <String, Id> map_colas = new Map<String, Id>();
	
					for(BI_COL_Asignacion_proyectos__c ap : lst_cs){
						map_ap.put(ap.BI_Segmento__c, ap);
						if(ap.BI_Cola__C){
							set_colas.add(ap.BI_Id_Usuario_o_cola__c);
						}
					}
	
					if(!set_colas.isEmpty()){
						//select the queues if exist
						List <Group> lst_queue = [SELECT Id,DeveloperName FROM Group WHERE DeveloperName IN: set_colas AND Type = 'Queue'];
						if(!lst_queue.isEmpty()){
	
							for(Group cola : lst_queue){
								map_colas.put(cola.DeveloperName, cola.Id);
							}
						}
					}
					//assign the BI_COL_Proyectos_Colombia__c to the configurated owner
					for(BI_COL_Proyectos_Colombia__c pc : toProcess){
	
						if(map_segmento.containsKey(pc.BI_COL_Oportunidad__c)){
	
							if(map_ap.containsKey(map_segmento.get(pc.BI_COL_Oportunidad__c))){
	
								BI_COL_Asignacion_proyectos__c cs = map_ap.get(map_segmento.get(pc.BI_COL_Oportunidad__c));
	
								if(cs.BI_Cola__c){
	
									if(map_colas.containsKey(cs.BI_Id_Usuario_o_cola__c)){
										pc.OwnerId = map_colas.get(cs.BI_Id_Usuario_o_cola__c);
									}
								}
								else{
									pc.OwnerId = cs.BI_Id_Usuario_o_cola__c;
								}
	
							}
						}
	
					}
	
				}
	
			}

		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_COL_Proyectos_ColombiaMethods.setOwner', 'BI_EN', Exc, 'Trigger');
		}

	}

}