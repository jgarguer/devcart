/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Miguel Cabrera
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_CaseMethods class caseMethod
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016          Miguel Cabrera             Initial Version
 20/10/2016          Fernando Arteaga           Added caseFillPreviousOwner method
 20/09/2017          Angel F. Santaliestra      Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
 25/09/2017          Jaime Regidor              Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_CaseMethods_TEST{
  
  static{
    BI_TestUtils.throw_exception = false;
  }

  //@isTest static void caseAsignGSE(){
  //  BI_TestUtils.throw_exception = false;
  //  TGS_User_Org__c uO = new TGS_User_Org__c();
  //  uO.TGS_Is_BI_EN__c = True;
  //  uO.SetupOwnerId = UserInfo.getUserId();
  //  insert uO;

  //  RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

  //  List<Account> lst_acc1 = new List<Account>();       
  //  Account acc1 = new Account(Name = 'test',
  //                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
  //                BI_Activo__c = Label.BI_Si,
  //                BI_Segment__c = 'Empresas',
  //                BI_Subsegment_local__c = 'Top 1',
  //                RecordTypeId = rt1.Id
  //                );    
  //  lst_acc1.add(acc1);
  //  insert lst_acc1;

  //  List<Account> lst_acc = new List<Account>();       
  //  Account acc = new Account(Name = 'test',
  //                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
  //                BI_Activo__c = Label.BI_Si,
  //                BI_Segment__c = 'Empresas',
  //                BI_Subsegment_local__c = 'Top 1',
  //                ParentId = lst_acc1[0].Id
  //                );    
  //  lst_acc.add(acc);
  //  insert lst_acc;

  //  RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

  //  Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
  //                    CloseDate = Date.today(),
  //                    StageName = Label.BI_F6Preoportunidad,
  //                    AccountId = lst_acc[0].Id,
  //                    BI_Ciclo_ventas__c = Label.BI_Completo, 
  //                    RecordTypeId = rt.Id
  //                    );
        
  //  insert opp;

  //  RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

  //  List<Case> lst_cases = new List<Case>();
  //  Case cas = new Case(Subject = 'Test Subject Opp',
  //            AccountId = opp.AccountId,
  //            RecordTypeId = rt2.Id,
  //            BI_Nombre_de_la_Oportunidad__c = opp.Id,
  //            BI_Country__c = opp.BI_Country__c,
  //            BI_Department__c = 'Ingeniería preventa',
  //            BI_Type__c = 'Solicitar Cotización',
  //            TGS_Address_Account__c = 'Value'
  //            );
  //  lst_cases.add(cas);

  //  Case cas1 = new Case(Subject = 'Test Subject Opp',
  //            AccountId = opp.AccountId,
  //            RecordTypeId = rt2.Id,
  //            BI_Nombre_de_la_Oportunidad__c = opp.Id,
  //            BI_Country__c = opp.BI_Country__c,
  //            BI_Department__c = 'Ingeniería preventa',
  //            BI_Type__c = 'Solicitar Propuesta',
  //            TGS_Address_Account__c = 'Value');

  //   lst_cases.add(cas1);
  //     AccountTeamMember aTM = new AccountTeamMember(UserId = UserInfo.getUserId(), 
  //                                                   AccountID = acc.Id);  
  //  Test.startTest();
  //  BI_TestUtils.throw_exception = true;
  //      insert aTM;
  //  insert lst_cases;
  //  update lst_cases;
  //      //BI_O4_CaseMethods.caseAsignGSE(lst_cases);
  //  Test.stopTest();
    
  //}

  //@isTest static void caseAsignLSE(){
  //  BI_TestUtils.throw_exception = false;
  //  TGS_User_Org__c uO = new TGS_User_Org__c();
  //  uO.TGS_Is_BI_EN__c = True;
  //  uO.SetupOwnerId = UserInfo.getUserId();
  //  insert uO;

  //  RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

  //  List<Account> lst_acc1 = new List<Account>();       
  //  Account acc1 = new Account(Name = 'test',
  //                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
  //                BI_Activo__c = Label.BI_Si,
  //                BI_Segment__c = 'Empresas',
  //                BI_Subsegment_local__c = 'Top 1',
  //                RecordTypeId = rt1.Id
  //                );    
  //  lst_acc1.add(acc1);
  //  insert lst_acc1;

  //  List<Account> lst_acc = new List<Account>();       
  //  Account acc = new Account(Name = 'test',
  //                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
  //                BI_Activo__c = Label.BI_Si,
  //                BI_Segment__c = 'Empresas',
  //                BI_Subsegment_local__c = 'Top 1',
  //                ParentId = lst_acc1[0].Id
  //                );    
  //  lst_acc.add(acc);
  //  insert lst_acc;

  //  RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' limit 1];

  //  Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
  //                    CloseDate = Date.today(),
  //                    StageName = Label.BI_F6Preoportunidad,
  //                    AccountId = lst_acc[0].Id,
  //                    BI_Ciclo_ventas__c = Label.BI_Completo, 
  //                    RecordTypeId = rt.Id
  //                    );
        
  //  insert opp;

  //  RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

  //  List<Case> lst_cases = new List<Case>();
  //  Case cas = new Case(Subject = 'Test Subject Opp',
  //            AccountId = opp.AccountId,
  //            RecordTypeId = rt2.Id,
  //            BI_Nombre_de_la_Oportunidad__c = opp.Id,
  //            BI_Country__c = opp.BI_Country__c,
  //            BI_Department__c = 'Ingeniería preventa',
  //            BI_Type__c = 'Solicitar Cotización',
  //            TGS_Address_Account__c = 'Value'
  //            );
  //  lst_cases.add(cas);

  //  Case cas1 = new Case(Subject = 'Test Subject Opp',
  //            AccountId = opp.AccountId,
  //            RecordTypeId = rt2.Id,
  //            BI_Nombre_de_la_Oportunidad__c = opp.Id,
  //            BI_Country__c = opp.BI_Country__c,
  //            BI_Department__c = 'Ingeniería preventa',
  //            BI_Type__c = 'Solicitar Cotización',
  //            TGS_Address_Account__c = 'Value'
  //            );
  //   lst_cases.add(cas1);
  //     AccountTeamMember aTM = new AccountTeamMember(UserId = UserInfo.getUserId(), 
  //                                                   AccountID = acc.Id);  
  //  Test.startTest();
  //      insert aTM;
  //  insert lst_cases;
  //  update lst_cases;
  //      //BI_O4_CaseMethods.caseAsignLSE(lst_cases);
  //  Test.stopTest();

  //}

  @isTest static void preventCloseChildCases(){

    BI_TestUtils.throw_exception = false;

    TGS_User_Org__c uO = new TGS_User_Org__c();
    uO.TGS_Is_BI_EN__c = True;
    uO.SetupOwnerId = UserInfo.getUserId();
    insert uO;

    Map<String, Id> rtMap = new Map<String, Id>();
    for (RecordType rt: [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_O4_Gen_rico_Preventa', 'BI_Caso_Interno', 'TGS_Legal_Entity', 'BI_Oportunidad_Regional')])
    {
      rtMap.put(rt.DeveloperName, rt.Id);
    }

    List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Country__c = 'Spain',
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_local__c = 'Top 1',
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  RecordTypeId = rtMap.get('TGS_Legal_Entity')
                  );    
    lst_acc1.add(acc1);
    insert lst_acc1;

    List<Account> lst_acc = new List<Account>();       
    User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
    System.runAs(currentUser){
    Account acc = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Country__c = 'Spain',
                  BI_Subsegment_local__c = 'Top 1',
                  ParentId = lst_acc1[0].Id
                  );    
    lst_acc.add(acc);
    insert lst_acc;
    }
    

    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      CloseDate = Date.today(),
                      StageName = Label.BI_F6Preoportunidad,
                      AccountId = lst_acc[0].Id,
                      BI_Ciclo_ventas__c = Label.BI_Completo, 
                      RecordTypeId = rtMap.get('BI_Oportunidad_Regional')
                      );
        
    insert opp;

    Map<Id, Case> mapCaso11 = new Map<Id, Case>();
    Map<Id, Case> mapCaso1 = new Map<Id, Case>();
    
    Case cas = new Case();
    Case cas1 = new Case();
    Case cas11 = new Case();

  Test.startTest();
  
    cas.Subject = 'Test Subject Opp';
    cas.AccountId = opp.AccountId;
    cas.RecordTypeId = rtMap.get('BI_Caso_Interno');
    cas.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas.BI_Country__c = opp.BI_Country__c;
    cas.BI_Department__c = 'Ingeniería preventa';
    cas.BI_Type__c = 'Solicitar Cotización';
    cas.Status = 'Aceptada';
    cas.ParentId = null;
    //JLA Field Doesnt exist in PROD cas.TGS_Address_Account__c = 'Value';
    insert cas;
    
    /*cas11.Subject = 'Test Subject Opp';
    cas11.AccountId = opp.AccountId;
    cas11.RecordTypeId = rt2.Id;
    cas11.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas11.BI_Country__c = opp.BI_Country__c;
    cas11.BI_Department__c = 'Ingeniería preventa';
    cas11.BI_Type__c = 'Solicitar Cotización';
    cas11.ParentId = cas.Id;
    cas11.Status = 'Closed';*/

     //Test.startTest();
    
    cas1.Subject = 'Test Subject Opp';
    cas1.AccountId = opp.AccountId;
    cas1.RecordTypeId = rtMap.get('BI_Caso_Interno');
    cas1.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas1.BI_Country__c = opp.BI_Country__c;
    cas1.BI_Department__c = 'Ingeniería preventa';
    cas1.BI_Type__c = 'Solicitar Cotización';
    cas1.ParentId = cas.Id;
    cas1.Status = 'Cotizada';
    ////JLA Field Doesnt exist in PROD cas1.TGS_Address_Account__c = 'Value';
    insert cas1;

    //mapCaso11.put(cas.Id, cas);
    //mapCaso1.put(cas1.Id, cas1);                
    
    BI_TestUtils.throw_exception = false;
    cas.Status = 'Closed';
    try{update cas;}catch(Exception exc){}

    //BI_O4_CaseMethods.preventCloseChildCases(mapCaso11, mapCaso1);
    Test.stopTest();
  }

  @isTest static void ddoCotizadosParentCotizado(){

    BI_TestUtils.throw_exception = false;

    TGS_User_Org__c uO = new TGS_User_Org__c();
    uO.TGS_Is_BI_EN__c = false;
    uO.TGS_Is_TGS__c = false;
    uO.SetupOwnerId = UserInfo.getUserId();
    insert uO;

    Map<String, Id> rtMap = new Map<String, Id>();
    for (RecordType rt: [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_O4_Gen_rico_Preventa', 'TGS_Legal_Entity')])
    {
      rtMap.put(rt.DeveloperName, rt.Id);
    }

    List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rtMap.get('TGS_Legal_Entity')
                  );    
    lst_acc1.add(acc1);
    insert lst_acc1;

        Opportunity opp = new Opportunity(Name = 'Test',
                      CloseDate = Date.today(),
                      StageName = Label.BI_F6Preoportunidad,
                      AccountId = acc1.Id,
                      BI_Ciclo_ventas__c = Label.BI_Completo
                      );

        insert opp;


    NE__Order__c ne_order = new NE__Order__c(NE__OptyId__c = opp.Id);
    insert ne_order;

    List<Case> lstCasos = new List<Case>();
    Case cas = new Case();
    cas.Subject = 'Test Subject Opp';
    cas.AccountId = acc1.Id;
    cas.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    //cas.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas.BI_O4_Identificativo_Origen__c = 'XXX';
    cas.Status = 'Pendiente Cotización';
    cas.BI_Department__c = 'Ingeniería preventa';
    cas.BI_Type__c = 'Solicitar Cotización';
    cas.Status = 'Closed';
    cas.ParentId = null;
    cas.BI_Oferta_asociada__c = ne_order.Id;
    cas.BI_O4_N_Sites__c = 18;
    cas.BI_O4_Tipo_oferta__c = 'Otro';
     //JLA Field Doesnt exist in PROD cas.TGS_Address_Account__c = 'Value';
    insert cas;

    Case cas1 = new Case();
    cas1.Subject = 'Test Subject Opp';
    cas1.AccountId = acc1.Id;
    cas1.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    cas1.ParentId = cas.Id;
    //cas1.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas1.BI_O4_Identificativo_Origen__c = 'XXX';
    cas1.Status = 'Pendiente Cotización';
    cas1.BI_Department__c = 'Ingeniería preventa';
    cas1.BI_O4_DDO__c = 'Global Pricing Office';
    cas1.BI_Oferta_asociada__c = ne_order.Id;
    cas1.BI_O4_N_Sites__c = 18;
    cas1.BI_O4_Tipo_oferta__c = 'Otro';
    //JLA Field Doesnt exist in PROD cas1.TGS_Address_Account__c = 'Value';
    lstCasos.add(cas1);

    Case cas2 = new Case();
    cas2.Subject = 'Test Subject Opp';
    cas2.AccountId = acc1.Id;
    cas2.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    cas2.ParentId = cas.Id;
    //cas2.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas2.BI_O4_Identificativo_Origen__c = 'XXX';
    cas2.Status = 'Cotizada';
    cas2.BI_Department__c = 'Ingeniería preventa';
    cas2.BI_O4_DDO__c = 'Global Pricing Office';
    cas2.BI_Oferta_asociada__c = ne_order.Id;
    cas2.BI_O4_N_Sites__c = 18;
    cas2.BI_O4_Tipo_oferta__c = 'Otro';
    //JLA Field Doesnt exist in PROD cas2.TGS_Address_Account__c = 'Value';
    lstCasos.add(cas2);


    List<Case> lstCasosN = new List<Case>();

    Case cas12 = new Case();
    cas12.Subject = 'Test Subject Opp';
    cas12.AccountId = acc1.Id;
    cas12.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    cas12.ParentId = cas.Id;
    //cas12.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas12.BI_O4_Identificativo_Origen__c = 'XXX';
    cas12.Status = 'Cotizada';
    cas12.BI_Department__c = 'Ingeniería preventa';
    cas12.BI_O4_DDO__c = 'Global Pricing Office';
    cas12.BI_Oferta_asociada__c = ne_order.Id;
    cas12.BI_O4_N_Sites__c = 18;
    cas12.BI_O4_Tipo_oferta__c = 'Otro';
    //JLA Field Doesnt exist in PROD cas12.TGS_Address_Account__c = 'Value';
    lstCasosN.add(cas12);

    Case cas13 = new Case();
    cas13.Subject = 'Test Subject Opp';
    cas13.AccountId = acc1.Id;
    cas13.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    cas13.ParentId = cas.Id;
    //cas13.BI_Nombre_de_la_Oportunidad__c = opp.Id;
    cas13.BI_O4_Identificativo_Origen__c = 'XXX';
    cas13.Status = 'Cotizada';
    cas13.BI_Department__c = 'Ingeniería preventa';
    cas13.BI_O4_DDO__c = 'Global Pricing Office';
    cas13.BI_Oferta_asociada__c = ne_order.Id;
    cas13.BI_O4_N_Sites__c = 18;
    cas13.BI_O4_Tipo_oferta__c = 'Otro';
    //JLA Field Doesnt exist in PROD cas13.TGS_Address_Account__c = 'Value';
    lstCasosN.add(cas13);
    

    Test.startTest();
    insert lstCasos;
    insert lstCasosN;
    uO.TGS_Is_BI_EN__c = true;
    update uO;
    BI_O4_CaseMethods.ddoCotizadosParentCotizado(lstCasosN, lstCasos);
    Test.stopTest();
  }

  @isTest static void caseFillPreviousOwner() {
    BI_TestUtils.throw_exception = false;
    TGS_User_Org__c uO = new TGS_User_Org__c();
    uO.TGS_Is_BI_EN__c = True;
    uO.SetupOwnerId = UserInfo.getUserId();
    insert uO;

    //RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' limit 1];

    Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];

    User usertest = new User(alias = 'testchg',
                      email = 'tchg@testorg.com',
                      emailencodingkey = 'UTF-8',
                      lastname = 'Test',
                      languagelocalekey = 'en_US',
                      localesidkey = 'en_US',
                      ProfileId = prof.Id,
                      BI_Permisos__c = Label.BI_Administrador,
                      timezonesidkey = Label.BI_TimeZoneLA,
                      username = 'usertestuser@testorg.com');

    insert usertest;

    Map<String, Id> rtMap = new Map<String, Id>();

    for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('TGS_Legal_Entity', 'BI_Caso_Interno')])
    {
      rtMap.put(rt.DeveloperName, rt.Id);
    }

    List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                              BI_Activo__c = Label.BI_Si,
                              BI_Segment__c = 'Empresas',
                              BI_Subsector__c = 'test', //25/09/2017
                              BI_Sector__c = 'test', //25/09/2017
                              BI_Subsegment_Regional__c = 'test',
                              BI_Territory__c = 'test',
                              BI_Subsegment_local__c = 'Top 1',
                              RecordTypeId = rtMap.get('TGS_Legal_Entity')
                              );
    insert acc1;

    List<Case> lst_cases = new List<Case>();
    Case cas = new Case(Subject = 'Test Subject Opp',
              AccountId = acc1.Id,
              RecordTypeId = rtMap.get('BI_Caso_Interno'),
              //BI_Nombre_de_la_Oportunidad__c = opp.Id,
              //BI_Country__c = opp.BI_Country__c,
              BI_Department__c = 'Ingeniería preventa',
              BI_Type__c = 'Solicitar Cotización'                                                               
              );

    insert cas;

    Test.startTest();

    // Test exception
    try {
      BI_O4_CaseMethods.fillPreviousOwner(null, null);  
    }
    catch (Exception e) {
      System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
    }

    // Test normal behaviour
    cas.OwnerId = usertest.Id;
    update cas;

    Test.stopTest();
    
    Case c = [SELECT Id, BI_O4_PriorOwner__c FROM Case WHERE Id = :cas.Id];
    System.assertEquals(c.BI_O4_PriorOwner__c, UserInfo.getUserId());

  }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       NEAborda
    Description:   Test method to manage the code coverage for BI_O4_CaseMethods.assignLegalCase
   
    History:
    
    <Date>                  <Author>                <Change Description>
    15/03/2017              Guillermo Muñoz         Initial Version       
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  @isTest static void assignLegalCase(){

    BI_TestUtils.throw_exception = false;

    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
    if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

        userTGS.TGS_Is_BI_EN__c = true;
        userTGS.TGS_Is_TGS__c = false;

        if(userTGS.Id != null){

            update userTGS;
        }
        else{

            insert userTGS;
        }
    }

    Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

    Id rtId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'].Id;

    List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String> {'United States'});

    Case cas = new Case(
      Subject = 'Test',
      AccountId = lst_acc[0].Id,
      RecordTypeId = rtId,
      BI_Department__c = 'Legal',
      BI_Type__c = 'Contrato',
      BI_Country__c = 'United States',
      Status = 'New'                                                        
    );
    insert cas;

    BI_MigrationHelper.disableBypass(mapa);

    Test.startTest();

    cas.Status = 'Assigned';
    update cas;

    Test.stopTest();

  }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Test fillGenericoPreventaId
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    26/06/2017                      Oscar Bartolo               Initial version
    25/09/2017                      Jaime Regidor              Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @isTest static void fillGenericoPreventaId(){

        BI_TestUtils.throw_exception = false;

        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c uO = new TGS_User_Org__c();
        uO.TGS_Is_BI_EN__c = True;
        uO.SetupOwnerId = UserInfo.getUserId();
        insert uO;

        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];

        User usertest = new User(alias = 'testchg',
                          email = 'tchg@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');

        insert usertest;

        Map<String, Id> rtMap = new Map<String, Id>();
        for (RecordType rt: [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_O4_Gen_rico_Preventa', 'TGS_Legal_Entity')])
        {
          rtMap.put(rt.DeveloperName, rt.Id);
        }

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                      BI_Activo__c = Label.BI_Si,
                      BI_Segment__c = 'Empresas',
                      BI_Subsector__c = 'test', //25/09/2017
                      BI_Sector__c = 'test', //25/09/2017
                      BI_Subsegment_Regional__c = 'test',
                      BI_Territory__c = 'test',
                      BI_Subsegment_local__c = 'Top 1',
                      RecordTypeId = rtMap.get('TGS_Legal_Entity')
                      );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        Opportunity opp = new Opportunity(Name = 'Test',
                      CloseDate = Date.today(),
                      StageName = Label.BI_F6Preoportunidad,
                      AccountId = acc1.Id,
                      BI_Ciclo_ventas__c = Label.BI_Completo
                      );

        insert opp;


        NE__Order__c ne_order = new NE__Order__c(NE__OptyId__c = opp.Id);
        insert ne_order;
		
        Test.startTest();
        List<Case> lstCasos = new List<Case>();
        Case cas = new Case();
        cas.Subject = 'Test Subject Opp';
        cas.AccountId = acc1.Id;
        cas.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
        cas.BI_O4_Identificativo_Origen__c = 'XXX';
        cas.Status = 'Pendiente Cotización';
        cas.BI_Department__c = 'Ingeniería preventa';
        cas.BI_Type__c = 'Solicitar Cotización';
        cas.Status = 'Closed';
        cas.ParentId = null;
        cas.BI_Oferta_asociada__c = ne_order.Id;
        cas.BI_O4_N_Sites__c = 18;
        cas.BI_O4_Tipo_oferta__c = 'Otro';
        insert cas;

        Case cas1 = new Case();
        cas1.Subject = 'Test Subject Opp';
        cas1.AccountId = acc1.Id;
        cas1.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
        cas1.ParentId = cas.Id;
        cas1.BI_O4_Identificativo_Origen__c = 'XXX';
        cas1.Status = 'Pendiente Cotización';
        cas1.BI_Department__c = 'Ingeniería preventa';
        cas1.BI_Oferta_asociada__c = ne_order.Id;
        cas1.BI_O4_N_Sites__c = 18;
        cas1.BI_O4_Tipo_oferta__c = 'Otro';
        lstCasos.add(cas1);

        Case cas2 = new Case();
        cas2.Subject = 'Test Subject Opp';
        cas2.AccountId = acc1.Id;
        cas2.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
        cas2.ParentId = cas.Id;
        cas2.BI_O4_Identificativo_Origen__c = 'XXX';
        cas2.Status = 'Cotizada';
        cas2.BI_Department__c = 'Ingeniería preventa';
        cas2.BI_Oferta_asociada__c = ne_order.Id;
        cas2.BI_O4_N_Sites__c = 18;
        cas2.BI_O4_Tipo_oferta__c = 'Otro';
        lstCasos.add(cas2);

        insert lstCasos;
        uO.TGS_Is_BI_EN__c = true;
        update uO;
        Test.stopTest();
    }

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Test closeChildCases
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    26/06/2017                      Oscar Bartolo               Initial version
    25/09/2017                      Jaime Regidor               Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

  @isTest static void closeChildCases(){

    BI_TestUtils.throw_exception = false;

    TGS_User_Org__c uO = new TGS_User_Org__c();
    uO.TGS_Is_BI_EN__c = True;
    uO.SetupOwnerId = UserInfo.getUserId();
    insert uO;

    Map<String, Id> rtMap = new Map<String, Id>();
    for (RecordType rt: [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_O4_Gen_rico_Preventa', 'BI_Caso_Interno', 'TGS_Legal_Entity', 'BI_Oportunidad_Regional')])
    {
      rtMap.put(rt.DeveloperName, rt.Id);
    }

    List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Country__c = 'Spain',
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_local__c = 'Top 1',
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  RecordTypeId = rtMap.get('TGS_Legal_Entity')
                  );    
    lst_acc1.add(acc1);
    insert lst_acc1;

    List<Account> lst_acc = new List<Account>();       
    User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
    System.runAs(currentUser){
    Account acc = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Country__c = 'Spain',
                  BI_Subsegment_local__c = 'Top 1',
                  ParentId = lst_acc1[0].Id
                  );    
    lst_acc.add(acc);
    insert lst_acc;
    }
	
    Map <Integer, BI_bypass__c> mapa;
	Case cas = new Case();
    Case cas1 = new Case();
        try{
            mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true, false, false, false);
            BI_MigrationHelper.skipAllTriggers();
            
            
        
            cas.Subject = 'Test Subject Opp';
            cas.AccountId = lst_acc[0].Id;
            cas.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
            cas.BI_Department__c = 'Ingeniería preventa';
            cas.BI_Type__c = 'Solicitar Cotización';
            cas.Status = 'Aceptada';
            cas.BI_O4_Tipo_oferta__c = 'Pedido parcial';
            cas.BI_O4_N_Sites__c = 1;
            cas.BI_O4_DDO__c = 'Asia';
            cas.BI_O4_Tipo_oferta__c = 'Pedido parcial';
            cas.ParentId = null;
            cas.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
            insert cas;
        
            cas1.Subject = 'Test Subject Opp';
            cas1.AccountId = lst_acc[0].Id;
            cas1.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
            cas1.BI_Department__c = 'Ingeniería preventa';
            cas1.BI_Type__c = 'Solicitar Cotización';
            cas1.ParentId = cas.Id;
            cas1.Status = 'Cotizada';
            insert cas1;
    	}
        catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_O4_CreateDDOCases.updateCaseSubject', 'BI_EN', exc, 'Clase');
        }
        finally{

            if(mapa != null){
                BI_MigrationHelper.disableBypass(mapa);
            }
            BI_MigrationHelper.cleanSkippedTriggers();
        }  
    Test.startTest(); 

    cas.Status = 'Rejected';
    cas.BI_O4_Responsable_Preventa_Global__c = currentUser.Id;
    cas.TGS_Assignee__c = currentUser.Id;
    try{update cas;}catch(Exception exc){}

    Test.stopTest();
  }
    
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Test updateAssignUserRole
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/07/2017                      Oscar Bartolo               Initial version
    25/09/2017                      Jaime Regidor               Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

  @isTest static void updateAssignUserRole(){

    BI_TestUtils.throw_exception = false;

    TGS_User_Org__c uO = new TGS_User_Org__c();
    uO.TGS_Is_BI_EN__c = True;
    uO.SetupOwnerId = UserInfo.getUserId();
    insert uO;

    Map<String, Id> rtMap = new Map<String, Id>();
    for (RecordType rt: [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_O4_Gen_rico_Preventa', 'BI_Caso_Interno', 'TGS_Legal_Entity', 'BI_Oportunidad_Regional')])
    {
      rtMap.put(rt.DeveloperName, rt.Id);
    }

    List<Account> lst_acc1 = new List<Account>();       
    Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Country__c = 'Spain',
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rtMap.get('TGS_Legal_Entity')
                  );    
    lst_acc1.add(acc1);
    insert lst_acc1;

    List<Account> lst_acc = new List<Account>();       
    User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
    System.runAs(currentUser){
    Account acc = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Country__c = 'Spain',
                  BI_Subsegment_local__c = 'Top 1',
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  ParentId = lst_acc1[0].Id
                  );    
    lst_acc.add(acc);
    insert lst_acc;
    }

    Case cas = new Case();
    Case cas1 = new Case();

    Test.startTest();
  
    cas.Subject = 'Test Subject Opp';
    cas.AccountId = lst_acc[0].Id;
    cas.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    cas.BI_Department__c = 'Ingeniería preventa';
    cas.BI_Type__c = 'Solicitar Cotización';
    cas.Status = 'Aceptada';
    cas.BI_O4_Tipo_oferta__c = 'Pedido parcial';
    cas.BI_O4_N_Sites__c = 1;
    cas.BI_O4_DDO__c = 'Asia';
    cas.BI_O4_Tipo_oferta__c = 'Pedido parcial';
    cas.ParentId = null;
    insert cas;
    
    cas1.Subject = 'Test Subject Opp';
    cas1.AccountId = lst_acc[0].Id;
    cas1.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
    cas1.BI_Department__c = 'Ingeniería preventa';
    cas1.BI_Type__c = 'Solicitar Cotización';
    cas1.ParentId = cas.Id;
    cas1.Status = 'Cotizada';
    insert cas1;

    cas.TGS_Assignee__c = currentUser.Id;
    try{update cas;}catch(Exception exc){}

    Test.stopTest();
  }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Test fillClienteInternoNombre
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    25/09/2017                      Oscar Bartolo               Initial version
    03/10/2017                      Jaime Regidor               Add the mandatory fields on account creation: BI_Subsegment_Regional__c, BI_Territory__c, BI_Subsector__c and BI_Sector__c
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @isTest static void fillClienteInternoNombre(){

        BI_TestUtils.throw_exception = false;

        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c uO = new TGS_User_Org__c();
        uO.TGS_Is_BI_EN__c = True;
        uO.SetupOwnerId = UserInfo.getUserId();
        insert uO;

        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];

        User usertest = new User(alias = 'testchg',
                          email = 'tchg@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');

        insert usertest;

        Map<String, Id> rtMap = new Map<String, Id>();
        for (RecordType rt: [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_O4_Gen_rico_Preventa', 'TGS_Legal_Entity')])
        {
          rtMap.put(rt.DeveloperName, rt.Id);
        }

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                      BI_Activo__c = Label.BI_Si,
                      BI_Segment__c = 'Empresas',
                      BI_Subsector__c = 'test', //03/10/2017
                      BI_Sector__c = 'test', //03/10/2017
                      BI_Subsegment_Regional__c = 'test', //03/10/2017
                      BI_Territory__c = 'test', //03/10/2017
                      BI_Subsegment_local__c = 'Top 1',
                      RecordTypeId = rtMap.get('TGS_Legal_Entity')
                      );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        Opportunity opp = new Opportunity(Name = 'Test',
                      CloseDate = Date.today(),
                      StageName = Label.BI_F6Preoportunidad,
                      AccountId = acc1.Id,
                      BI_Ciclo_ventas__c = Label.BI_Completo
                      );

        insert opp;


        NE__Order__c ne_order = new NE__Order__c(NE__OptyId__c = opp.Id);
        insert ne_order;
        
        Test.startTest();
        List<Case> lstCasos = new List<Case>();
        Case cas = new Case();
        cas.Subject = 'Test Subject Opp';
        cas.AccountId = acc1.Id;
        cas.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
        cas.BI_O4_Identificativo_Origen__c = 'XXX';
        cas.Status = 'Pendiente Cotización';
        cas.BI_Department__c = 'Ingeniería preventa';
        cas.BI_Type__c = 'Solicitar Cotización';
        cas.Status = 'Closed';
        cas.ParentId = null;
        cas.BI_Oferta_asociada__c = ne_order.Id;
        cas.BI_O4_N_Sites__c = 18;
        cas.BI_O4_Tipo_oferta__c = 'Otro';
        insert cas;
        
        Case cas2 = new Case();
        cas2.Subject = 'Test Subject Opp';
        cas2.AccountId = acc1.Id;
        cas2.RecordTypeId = rtMap.get('BI_O4_Gen_rico_Preventa');
        cas2.BI_O4_Identificativo_Origen__c = 'XXX';
        cas2.Status = 'Pendiente Cotización';
        cas2.BI_Department__c = 'Ingeniería preventa';
        cas2.BI_Type__c = 'Solicitar Cotización';
        cas2.Status = 'Closed';
        cas2.ParentId = cas.Id;
        cas2.BI_Oferta_asociada__c = ne_order.Id;
        cas2.BI_O4_N_Sites__c = 18;
        cas2.BI_O4_Tipo_oferta__c = 'Otro';
        insert cas2;

        insert lstCasos;
        uO.TGS_Is_BI_EN__c = true;
        update uO;
        Test.stopTest();
    }    
}