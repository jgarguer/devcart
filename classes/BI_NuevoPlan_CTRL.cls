public class BI_NuevoPlan_CTRL {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Unknown
    Company:       Aborda.es
    Description:   Controller for VF page BI_NuevoPlan. Only for Chile.
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    desc/2014              Micah Burgos             Initial Version     
    26/04/2016             Alvaro Sevilla           Add condition to CHILE: || Oppty.BI_Opportunity_Type__c  == Label.BI_TipoCSB --> linea 36
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    

    public Opportunity Oppty { get; set; }
    //public String url {get;set;}
    //public BI_Solicitud_de_plan__c sol {get; set;}
    //public String ordId {get; set;}

    public BI_NuevoPlan_CTRL(ApexPages.StandardController controller) {
        //ordId = ApexPages.currentPage().getParameters().get('id');
        Oppty = (Opportunity)controller.getRecord();
        Oppty = [SELECT Id, BI_CHI_Plan_Requerimiento_comercial__c, BI_Opportunity_Type__c, StageName, BI_Country__c, AccountId  FROM Opportunity WHERE Id =:Oppty.Id];
    }

    public void crearPlan(){
        
    
        List <User> usr = [SELECT Id, BI_Permisos__c FROM User WHERE Id =: UserInfo.getUserId()];
        List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE SobjectType ='BI_Solicitud_de_plan__c' AND DeveloperName ='BI_Plana'];
        //url = 'bb';
        // ---> Add condition to CHILE: || Oppty.BI_Opportunity_Type__c  == Label.BI_TipoCSB 
        if(Oppty.BI_Country__c == 'Chile'
         && //usr[0].BI_Permisos__c == Label.BI_Ejecutivo_de_Cliente &&
         (Oppty.BI_Opportunity_Type__c == Label.BI_TipoMixto || Oppty.BI_Opportunity_Type__c == Label.BI_TipoMovil || Oppty.BI_Opportunity_Type__c  == Label.BI_TipoCSB)){ 

            BI_Solicitud_de_plan__c solicitud = new BI_Solicitud_de_plan__c(BI_Cliente__c = Oppty.AccountId,
                                                                            RecordTypeId = lst_rt[0].Id, 
                                                                            BI_CHI_Oportunidad__c = Oppty.Id,
                                                                            OwnerId = UserInfo.getUserId());
            try{
                insert solicitud;
            }catch(exception exc){
                BI_LogHelper.generate_BILog('BI_NuevoPlan_CTRL.BI_NuevoPlan_CTRL', 'BI_EN', exc, 'VF');
            }
            system.debug(solicitud);

            //sol = solicitud;
//
            //url='/'+solicitud.Id;
            //system.debug(url);

            Oppty.BI_CHI_Plan_Requerimiento_comercial__c = solicitud.Id;

            update Oppty;

        }       
    
    }
}