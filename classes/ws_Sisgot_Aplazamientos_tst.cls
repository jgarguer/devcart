/**
* Avanxo Colombia
* @author           Oscar Alejandro Jimenez Forero
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-26      Oscar Alejandro Jimenez Forero (OAJF)   Class Created
*************************************************************************************************************/
@isTest
global class ws_Sisgot_Aplazamientos_tst implements WebServiceMock{
	
	global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		ws_Sisgot_Aplazamientos.processAplazamientoResponseType objresp = new ws_Sisgot_Aplazamientos.processAplazamientoResponseType();
		objresp.processAplazamientoResult = 'resp';

		response.put( 'response_x', objResp);
	}

	@isTest
	static void test_method_1() {
		
		ws_Sisgot_Aplazamientos.serviceAplazamientosPort webser = new ws_Sisgot_Aplazamientos.serviceAplazamientosPort();
		
		Test.startTest();		
		Test.setMock( WebServiceMock.class, new ws_Sisgot_Aplazamientos_tst() );
		webser.processAplazamiento('Info');
		Test.stopTest();		
	}
}