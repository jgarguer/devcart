public with sharing class BIIN_IncidenciaDetalle_Redirect_Ctrl {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ignacio Morales Rodríguez
        Company:       Deloitte
        Description:   Clase para hacer redirección del layout del Detalle de casos con RecordType Incidencia Técnica o Solicitud Incidencia Técnica
                         a la página BIIN_Detalle_Incidencia 
        
        History:
        
        <Date>              <Author>                                        <Description>
        10/12/2015       Ignacio Morales Rodríguez                          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    private ApexPages.StandardController controller;
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public Case objCase {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String accountID {get; set;}
    public String contactID {get; set;}
    public String caseId;
    public Map<String, Id> mapRecordTypes;
        
    public BIIN_IncidenciaDetalle_Redirect_Ctrl(ApexPages.StandardController controller) {
        this.controller = controller;   
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        rType = ApexPages.currentPage().getParameters().get('RecordType');     
        cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
        ent = ApexPages.currentPage().getParameters().get('ent');
        confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
        saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
        accountID = ApexPages.currentPage().getParameters().get('def_account_id');
        contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
        
        caseId = controller.getId();    
        objCase = [SELECT RecordTypeId, BI_Country__c FROM case WHERE Id =: caseId LIMIT 1 ];    
        System.debug('\n\n-=#=-\n' + 'objCase' + ': ' + objCase + '\n-=#=-\n');

        List<RecordType> lstRecordTypes = [SELECT Id
                                                 ,DeveloperName
                                             FROM RecordType
                                            WHERE DeveloperName IN: new Set<String>{Label.BI2_COL_Incidencia_tecnica, Label.BI2_COL_Solicitud_incidencia_tecnica}];
        mapRecordTypes = new Map<String, Id>();
        for(RecordType objRecodType : lstRecordTypes) {
            mapRecordTypes.put(objRecodType.DeveloperName, objRecodType.Id);
        }
        System.debug('\n\n-=#=-\n' + 'mapRecordTypes' + ': ' + mapRecordTypes + '\n-=#=-\n');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ignacio Morales Rodríguez
        Company:       Deloitte
        Description:   Función para redireccionar a BIIN_Detalle_Incidencia
        
        History:
        
        <Date>            <Author>                      <Description>
        12/12/2015     Ignacio Morales Rodríguez        Initial version
        18/02/2017     Antonio Torres - Avanxo          Modificación BI_EN Fase 2 Colombia - Redirect dinámico segun configuración
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    public PageReference redirect() {
        PageReference returnURL;

        /*if(Test.isRunningTest()) {
            objCase.RecordTypeId = '012w0000000hzMJ';
        }*/

        // INICIO: Modificación BI_EN Fase 2 Colombia - Redirect dinámico segun configuración
        if(String.isNotEmpty(objCase.BI_Country__c)) {
            List<BI2_Paginas_casos_tecnicos__mdt> lstPaginasCasosTecnicos = [SELECT BI2_Pagina_consulta__c
                                                                                   ,BI2_Pagina_consulta_solicitud__c
                                                                               FROM BI2_Paginas_casos_tecnicos__mdt
                                                                              WHERE DeveloperName =: objCase.BI_Country__c];
            if(!lstPaginasCasosTecnicos.isEmpty()) {
                if(objCase.RecordTypeId == mapRecordTypes.get(Label.BI2_COL_Incidencia_tecnica)) {
                    System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Redirecciona a página configurada - Incidencia: \'' + lstPaginasCasosTecnicos[0].BI2_Pagina_consulta__c + '\'   <<<<<<<<<<\n-=#=-\n');
                    PageReference objPageRef = new PageReference('/apex/' + lstPaginasCasosTecnicos[0].BI2_Pagina_consulta__c);
                    objPageRef.getParameters().put('Idcaso', caseId);
                    return objPageRef;      
                } else if(objCase.RecordTypeId == mapRecordTypes.get(Label.BI2_COL_Solicitud_incidencia_tecnica)) {
                    System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Redirecciona a página configurada - Solicitud: \'' + lstPaginasCasosTecnicos[0].BI2_Pagina_consulta_solicitud__c + '\'   <<<<<<<<<<\n-=#=-\n');
                    PageReference objPageRef = new PageReference('/apex/' + lstPaginasCasosTecnicos[0].BI2_Pagina_consulta_solicitud__c);
                    objPageRef.getParameters().put('Idcaso', caseId);
                    return objPageRef;
                } else {
                    returnURL = new PageReference('/'+ caseId);
                }
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'PAÍS NO CONFIGURADO - Redirecciona a página por defecto segun el tipo de registro' + '   <<<<<<<<<<\n-=#=-\n');
            }
        } else {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'NO HAY PAÍS - Redirecciona a página por defecto segun el tipo de registro' + '   <<<<<<<<<<\n-=#=-\n');
        }
        // FIN: Modificación BI_EN Fase 2 Colombia - Redirect dinámico segun configuración

        IF(objCase.RecordTypeId == mapRecordTypes.get(Label.BI2_COL_Incidencia_tecnica)) {  
            PageReference pageRef = Page.BIIN_Detalle_Incidencia;
            pageRef.getParameters().put('Idcaso', caseId);
            return PageRef;      
        }ELSE {
            IF(objCase.RecordTypeId == mapRecordTypes.get(Label.BI2_COL_Solicitud_incidencia_tecnica)) {        
                PageReference pageRef = Page.BIIN_Detalle_Solicitud;
                pageRef.getParameters().put('Idcaso', caseId);
                return PageRef;
                
            }ELSE{
                returnURL = new PageReference('/'+ caseId);
            }
        }
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', rType);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');
        
        If(Test.isRunningTest()){accountID = '1234';} 
        IF (accountID != null){
            returnURL.getParameters().put('def_account_id', accountID);
        }
        If(Test.isRunningTest()){contactID = '1234';} 
        IF (contactID != null){
            returnURL.getParameters().put('def_contact_id', contactID);
        }
        returnURL.setRedirect(true);
        return returnURL;
    }
}