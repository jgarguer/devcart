/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Infinity W4+5 helper methods for Quote object
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 08/08/2016          Fernando Arteaga          Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class BI_O4_QuoteHelper
{
	public static final String QUOTE_STATUS_GSE = 'Pending to assign GSE';
    public static final String QUOTE_STATUS_WORKING = 'Working on proposal';
    public static final String ERROR_NO_SELECTED = Label.BI_O4_No_Opportunities_Selected;
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Fernando Arteaga
	 Company:       New Energy Aborda
	 Description:   Returns Quote.Name for a given Name. If this name already exists, returns the name followed by a script (-) and a sequential name.
			        If not exists, returns the name followed by '-001'
	 Test Class:    
	 
	 History:
	  
	 <Date>              <Author>                   <Change Description>
	 08/08/2016          Fernando Arteaga           Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static String getNextQuoteName(String name) 
    {
        Integer counter;
        String likeName = (name == null)? '': name;
        likeName += '-%';
        
        List<Quote> listQuote = [SELECT Name FROM Quote 
                                 WHERE Name LIKE :likeName 
                                 ORDER BY Name desc limit 1];
                                 
        if ((listQuote == null) || (listQuote.isEmpty()))
        {
            counter = 1;
        }
        else
        {
            String newName = listQuote[0].Name;
            counter = Integer.ValueOf(newName.mid(newName.length()- 3, newName.length())) + 1;
        }
        return name + '-' + toString(counter, 3);
    }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Fernando Arteaga
	 Company:       New Energy Aborda
	 Description:   Returns the consecutive counter from Quote.BI_O4_Proposal_scenarios__c related to the opportunity passed
	 Test Class:    
	 
	 History:
	  
	 <Date>              <Author>                   <Change Description>
	 08/08/2016          Fernando Arteaga           Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Decimal getNextQuoteScenario (Id opportunityId)
    {
        Quote[] listQuote = [SELECT BI_O4_Proposal_scenarios__c FROM Quote 
                             WHERE BI_O4_Proposal_scenarios__c <> null
                             AND OpportunityId = :opportunityId
                             order by BI_O4_Proposal_scenarios__c desc limit 1];
                             
        if ((listQuote == null)     ||
            (listQuote.isEmpty())   || 
            (listQuote[0].BI_O4_Proposal_scenarios__c == null))
        {
            return 1.0;
        }
        
        return (Decimal) ((listQuote[0].BI_O4_Proposal_scenarios__c + 1).intValue());
    }
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Fernando Arteaga
	 Company:       New Energy Aborda
	 Description:   Returns the integer passed as a String, padded-left with so many zeros as len parameter says

	 Test Class:    
	 
	 History:
	  
	 <Date>              <Author>                   <Change Description>
	 08/08/2016          Fernando Arteaga           Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static String toString (Integer value, Integer len)
    {
        String out = String.valueOf(value);
        Integer diff = len - out.length();
        if (diff > 0) 
        {
            return '0'.repeat(diff) + out;
        }
        return out;
    }
    
/**
        Javier Tena
        Clona un objeto Quote
    **/
    
    public static Quote cloneQuote (Id quoteId) 
    {
        Quote quote = getAllFieldsQuote (QuoteId);
                
        if (quote == null)
        {
            return null;
        }
             
        Decimal newScenario = getNextQuoteScenarioCloned(quote.opportunityId, quote.BI_O4_Proposal_scenarios__c);
        if (newScenario == null) {return null;}
        
        Quote clonedQuote = quote.clone();
        String estado = quote.Status;
        clonedQuote.BI_O4_Proposal_scenarios__c = newScenario;

        /*
        Id IdGate2 = [SELECT Id FROM RecordType WHERE DeveloperName = 'Gate_2'].Id;
        Id IdGate3 = [SELECT Id FROM RecordType WHERE DeveloperName = 'Gate_3'].Id;
        
        List<Approval__c> propuestas = new List<Approval__c>();
        propuestas = [SELECT Id FROM Approval__c WHERE Proposal__c = :quote.Id];
        */
        
        String sem = 'GRIS';
        //clonedQuote.Semaforo_Gate_2__c = sem;
        //clonedQuote.Semaforo_Gate_3__c = sem;
        
        try 
        {
            insert clonedQuote;
        }
        
        catch(exception e) 
        {
            return null;
        }

        List<BI_O4_Solution__c> listClonedSolution = clonedListSolution( quote, clonedQuote );
        insert listClonedSolution;

        List<BI_O4_Proposal_Item__c> listClonedProposal = clonedListProposal( quote, clonedQuote, listClonedSolution);
        insert listClonedProposal;
        
        return clonedQuote;
    }

/**
        Javier Tena
        Devuelve el siguiente contador de Quote.Scenario__c  de un Quote Clonado
        Se pasa como parametro la oportunidad y el numero de Scenario para sacar su subgrupo
        la parte entera de Scenario representa el padre
        la parte decimal
    **/
    
    public static Decimal getNextQuoteScenarioCloned (Id opportunityId, Decimal parentScenario) 
    {
        if (parentScenario == null)
        {
            return getNextQuoteScenario(opportunityId);
        }
        
        Decimal minLimit = (Decimal)(parentScenario.intValue());
        Decimal maxLimit = minLimit + 1;
        
        List<Quote> listQuote = [Select BI_O4_Proposal_scenarios__c from Quote 
                                where BI_O4_Proposal_scenarios__c <> null
                                and OpportunityId = :opportunityId
                                and BI_O4_Proposal_scenarios__c > :minLimit
                                and BI_O4_Proposal_scenarios__c < :maxLimit
                                order by BI_O4_Proposal_scenarios__c desc limit 1];
        
        // primer hijo, tiene valor x.1
        if ((listQuote == null) || (listQuote.isEmpty()))
        {
            return ( (Decimal) (parentScenario.IntValue()) ) +  0.1;  
        }
        
        //La parte entera del nuevo escenario debe ser igual a la parte entera del padre
        Decimal result = listQuote[0].BI_O4_Proposal_scenarios__c + 0.1;
        if (result.IntValue() == parentScenario.IntValue())
        { 
            return result;
        }
        
        return null;
    }
    
    /**
        Javier Tena
        Clona todas las BI_O4_Solution__c de una quote
    **/
    
    private static List<BI_O4_Solution__c> clonedListSolution (Quote quote, Quote clonedQuote) 
    {
    	String query = BI_O4_Utils.getSOQLQueryAllFields(BI_O4_Solution__c.getSobjectType()) + ' WHERE BI_O4_Quote__c = \'' + quote.Id + '\'';
        List<BI_O4_Solution__c> listSolution = Database.query(query);
        List<BI_O4_Solution__c> listClonedSolution = new List<BI_O4_Solution__c>();
        
        for (BI_O4_Solution__c solution: listSolution)
        {
            BI_O4_Solution__c clonedSolution = solution.clone();
            clonedSolution.BI_O4_Quote__c = clonedQuote.Id;
            listClonedSolution.add(clonedSolution);
        }
        
        return listClonedSolution;
    }
    
    /**
        Javier Tena
        Clona todas las BI_O4_Proposal_Item__c de una quote
    **/
    
    private static List<BI_O4_Proposal_Item__c> clonedListProposal(Quote quote, Quote clonedQuote, List<BI_O4_Solution__c> listSolution)
    {
        map<String, Id> mapSolution = new map<String,Id>();
        
        for (BI_O4_Solution__c solution: listSolution)
        {
            mapSolution.put(solution.BI_O4_Service_line__c, solution.Id);
        }
        
        String sql = BI_O4_Utils.getSOQLQueryAllFields(BI_O4_Proposal_Item__c.getSobjectType()) + ' WHERE BI_O4_Quote__c = \'' + quote.Id + '\'';
        List<BI_O4_Proposal_Item__c> listPropItems = Database.query(sql);
        List<BI_O4_Proposal_Item__c> listClonedPropItems = new List<BI_O4_Proposal_Item__c>();
        
        for (BI_O4_Proposal_Item__c propItem: listPropItems) 
        {
            BI_O4_Proposal_Item__c clonedPropItem = propItem.clone();
            clonedPropItem.BI_O4_Quote__c = clonedQuote.Id;
            //clonedPropItem.BI_O4_Solution__c = mapSolution.get(propItem.BI_O4_Service_line__c);
            listClonedPropItems.add( clonedPropItem );
        }
        
        return listClonedPropItems;
    }
    
    /**
        Javier Tena
        devuelve el objeto Quote con todos sus campos de la id pasada como par?metro
    **/
    
    public static Quote getAllFieldsQuote(Id QuoteId) 
    {
        if (QuoteId == null) {return null;}
        String sql = BI_O4_Utils.getSOQLQueryAllFields(Quote.getSobjectType()) + ' WHERE Id = \'' + QuoteId + '\'';
        
        List<Quote> listQuote = Database.Query(sql);
        if (listQuote.isEmpty())
        {
            return null;
        }
        return listQuote[0];
    }
    
    public static string ChangeForceGates(id myId){
	    list <Quote> myQuote = [select id, BI_O4_Gates_required__c from Quote where id = :myId limit 1];
	    
	    if(myQuote[0].BI_O4_Gates_required__c == false)	myQuote[0].BI_O4_Gates_required__c=true;
	    else 									return('no se puede actualizar, su valor ya es true');
	    try{
	        update myQuote[0];
	    }catch (exception e){ return (e.getMessage());}
	    return('actualizado');
  	}
}