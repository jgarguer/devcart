global with sharing class NEBit2winApi {


    @future (callout=true)
    public static void doCalloutFromFuture(String CatalogId) {
        system.debug('CatalogId: '+CatalogId);
        String attchXml = NE.PublishCatalogButton.AttachmentPublication(CatalogId,'XmlMap');
    }   

    public static Decimal convertCurrency(Decimal inputToConvert,String sourceCurrency, String targetCurrency, map<String,Decimal> mapOfCurrsConversion)
    {
        Decimal convertedCurrency;
        
        Decimal sourceCurrencyValue =   mapOfCurrsConversion.get(sourceCurrency);
        Decimal targetCurrencyValue =   mapOfCurrsConversion.get(targetCurrency);
        
        convertedCurrency           =   (inputToConvert/sourceCurrencyValue)*targetCurrencyValue;
        
        return convertedCurrency;
    }
    
    WebService static String submit(String orderId, String submitType) 
    {
        String callStatus = 'OK';
        try
        {
            //Only if all the order items are == Pending
            NE__Order__c ord    =   [SELECT NE__AccountId__c, (SELECT id, NE__Status__c FROM NE__Order_Items__r WHERE NE__Status__c =: submitType) FROM NE__Order__c WHERE id =: orderId];
            if(ord.NE__Order_Items__r.size() > 0)
            {
                for(NE__OrderItem__c oi:ord.NE__Order_Items__r)
                    oi.NE__Status__c    =   'Enviado';
                    
                update ord.NE__Order_Items__r;
            }
            else
            {
                callStatus  =   'KO';
            }       
        }
        catch(Exception e)
        {
            callStatus  =   'KO';
        }
        
        
        return callStatus;
    }
    
    WebService static String cloneOpty (String oldOpty,String newOpty, String optytype) 
    { 
        system.debug('oldOpty: '+oldOpty);
        system.debug('newOpty: '+newOpty);
        system.debug('optytype: '+optytype);
        String newOrdId;
        String errorCode;
        String urlPage;
        
        try{
            List<NE__OrderItem__c> item;
            
            List<NE__Order__c> oldOrd = [SELECT Id,Name,NE__OpportunityId__c,NE__OptyId__c,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c
                                   FROM NE__Order__c WHERE NE__OptyId__c =: oldOpty AND RecordType.DeveloperName = 'Opty' ORDER BY NE__Version__c desc LIMIT 1];
            
            if(oldOrd != null && oldOrd.size() > 0){
                NE.DataMap mp = new NE.DataMap();
                //GMN 18/03/2016 - Desactivación de validaciones al duplicar la orden de la oportunidad para prevenir malfuncionamientos
                Map <Integer, BI_bypass__c> mapa;
                Map <String , String> gen_map;
                try{
                    mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
                    gen_map = mp.GenerateMapObjects('Order2Order', oldOrd[0].id);
                }catch(Exception e){
                }
                finally{
                    if(mapa != null){
                        BI_MigrationHelper.disableBypass(mapa);
                    }
                }
                //Fin GMN 18/03/2016
                
                if(gen_map!=null){ 
                    newOrdId = gen_map.get('ParentId');
                    errorCode    =  gen_map.get('ErrorCode'); 
                    system.debug('*ERRORCODE ' + errorCode);
                    if(errorCode.equalsIgnoreCase('0')){
                        NE__Order__c newOrd;    
                        newOrd = [SELECT Id,Name,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c FROM NE__Order__c WHERE Id =: newOrdId];
                                        
                        newOrd.NE__SerialNum__c             =   newOrd.Name;
                        newOrd.NE__OrderStatus__c           =   'Active';
                        newOrd.NE__ConfigurationStatus__c   =   'Valid';
                        newOrd.NE__OpportunityId__c         =   newOpty;
                        newOrd.NE__OptyId__c                =   newOpty;
                        //Decimal vers                        =   newOrd.NE__Version__c;
                        if(optytype == 'New Version'){
                            Decimal version                 =   oldOrd[0].NE__Version__c+1;
                            system.debug('version: '+version);
                            newOrd.NE__Version__c           =   version;
                            oldOrd[0].NE__OrderStatus__c    =   'Revised';
                            update oldOrd;
                            
                            /*
                            Opportunity opty                        = [SELECT id,BI_Oferta_tecnica__c, BI_Origen_de_la_oferta_tecnica__c,BI_Comite_de_negocios__c,BI_Oferta_economica__c FROM Opportunity WHERE id =: oldOpty]; 
                            opty.BI_Origen_de_la_oferta_tecnica__c  = ''; 
                            opty.BI_Oferta_economica__c             = ''; 
                            opty.BI_Comite_de_negocios__c           = '';   
                            opty.BI_Oferta_tecnica__c               = '';    
                            update opty; */
                            NEBit2winApi.resetOpty(oldOpty);
                            
                        }
                        else if(optytype == 'Renewal'){
                            newOrd.NE__Version__c = 1;
                            item = [SELECT Name, NE__Status__c,  NE__Action__c, NE__OrderId__c FROM NE__OrderItem__c WHERE NE__OrderId__c = :newOrdId];
                            
                            for(NE__OrderItem__c it: item){
                                it.NE__Status__c = 'Active';
                                it.NE__Action__c = 'None';
                            }
                            update item;                    
                        }   
                        else if(optytype == 'Clone'){
                            newOrd.NE__Version__c = 1;
                        }
                        update newOrd;    
                        
                        String prefix = Site.getPrefix();
        
                        if(prefix == null)
                            prefix  = '';   
                        
                        //urlPage =     prefix + '/' + newOrdId; 
                        urlPage =   newOrdId;  
                    }
                }
            }
            else{
                system.debug('No query matched!');
            }
        } 
        catch(Exception e)
        {
            urlPage =   '';
        }


        return urlPage; 
    } 
    
    WebService static String resetOpty (String optyId) 
    {    
        try{
            Opportunity opty                        = [SELECT id,BI_Descuento__c,BI_Oferta_tecnica__c,BI_Origen_de_la_oferta_tecnica__c,BI_Comite_de_negocios__c,BI_Oferta_economica__c FROM Opportunity WHERE id =: optyId]; 
            List<NE__Order__c> oldOrdList           = [SELECT Id,Name,NE__OpportunityId__c,NE__OptyId__c,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c
                                                        FROM NE__Order__c WHERE NE__OptyId__c =: opty.id ORDER BY NE__Version__c desc LIMIT 1];            

            if(oldOrdList.size() > 0)
            {
                NE__Order__c orderToCheck               =   oldOrdList.get(0);
                
                list<NE__OrderItem__c> listOfOi         =   [SELECT NE__OneTimeFeeOv__c , NE__RecurringChargeOv__c , id,NE__CatalogItem__r.NE__Base_OneTime_Fee__c ,NE__CatalogItem__r.NE__BaseRecurringCharge__c,NE__CatalogItem__r.NE__Technical_Behaviour_Opty__c,NE__BaseOneTimeFee__c FROM NE__OrderItem__c WHERE NE__OrderId__c =: orderToCheck.id];
                
                for(NE__OrderItem__c oi:listOfOi)
                {
                    String techB    =   oi.NE__CatalogItem__r.NE__Technical_Behaviour_Opty__c;
                    if(techB != null && techB != '')
                    {
                        Boolean contTechB   =   techB.contains('Factibilidad técnica');
                        if(contTechB)
                            opty.BI_Origen_de_la_oferta_tecnica__c  =   'Automática';
                    } 
                    //Fixed retrieve NE__BaseOneTimeFee__c SOQL query
                    //Check discount [Modificado para Colombia]
                    if( ((oi.NE__OneTimeFeeOv__c != oi.NE__CatalogItem__r.NE__Base_OneTime_Fee__c || oi.NE__RecurringChargeOv__c != oi.NE__CatalogItem__r.NE__BaseRecurringCharge__c ) && opty.BI_Country__c != 'Colombia' ) 
                        || (oi.NE__OneTimeFeeOv__c != oi.NE__BaseOneTimeFee__c && oi.NE__RecurringChargeOv__c != oi.NE__BaseRecurringCharge__c && opty.BI_Country__c == 'Colombia') )
                    {
                        opty.BI_Descuento__c            =   true;               
                        opty.BI_Oferta_economica__c     =   'Enviar a aprobación';                 
                    }else{
                        opty.BI_Descuento__c            =   false;      
                        opty.BI_Oferta_economica__c     =   Label.BI_SinDescuento;
                    }                    
                }
                
                opty.BI_Comite_de_negocios__c           =   ''; 
                opty.BI_Oferta_tecnica__c               =   '';
                update opty;    
            }
        }
        catch(Exception e){System.debug('NEBit2winApi.resetOpty [Exception] = ' + e);}
        
        return null;
    }
    
    WebService static String findActiveOptyVersion (String optyId) 
    {
        String activeVersionId  =   '';
        
        try
        {
            NE__Order__c activeVersion = [SELECT Id ,Name,NE__OpportunityId__c,NE__OptyId__c,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c
                                            FROM NE__Order__c WHERE NE__OptyId__c =: optyId ORDER BY NE__Version__c desc LIMIT 1];  
                                            
            activeVersionId =   activeVersion.id;
        }
        catch(Exception e){System.debug('NEBit2winApi.findActiveOptyVersion [Exception] = ' + e);}

                               
        return  activeVersionId;    
    }
    
    //NE RDF 24/06/2014: Add new method 
    WebService static String cloneOptyconfiguration (String oldOpty,String newOpty, String optytype) 
    { 
        String newOrdId;
        String errorCode;
        String urlPage;
        
        //try{
            List<NE__OrderItem__c> item;
            String contractHeader;
            List<NE__Order__c> oldOrd;
            List<NE__Order__c> oldOrdnew;

            /*if(optytype == 'Quote'){

                oldOrd = [SELECT NE__OptyId__r.NE__HaveActiveLineItem__c,NE__OptyId__r.NE__Order_Generated__c,NE__OptyId__r.AccountId,NE__OptyId__r.BI_Producto_bloqueado__c,NE__OptyId__r.BI_Acuerdo_Marco__c,NE__OptyId__r.BI_Ciclo_ventas__c,NE__OptyId__r.CurrencyIsoCode,Id,Name,NE__OpportunityId__c,NE__OptyId__c,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c
                                   FROM NE__Order__c WHERE Id =: oldOpty AND RecordType.DeveloperName = 'Quote' AND NE__OrderStatus__c = 'Active' ORDER BY NE__Version__c desc LIMIT 1];
            
                oldOrdnew = [SELECT NE__OptyId__c,NE__Version__c , Id,NE__OrderStatus__c  FROM NE__Order__c where NE__OptyId__c=:oldOrd[0].NE__OptyId__c  ORDER BY NE__Version__c desc LIMIT 1];

            }*/
            //else{

                oldOrd = [SELECT NE__OptyId__r.NE__HaveActiveLineItem__c,NE__OptyId__r.NE__Order_Generated__c,NE__OptyId__r.AccountId,NE__OptyId__r.BI_Producto_bloqueado__c,NE__OptyId__r.BI_Acuerdo_Marco__c,NE__OptyId__r.BI_Ciclo_ventas__c,NE__OptyId__r.CurrencyIsoCode,Id,Name,NE__OpportunityId__c,NE__OptyId__c,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c
                                       FROM NE__Order__c WHERE Id =: oldOpty ORDER BY NE__Version__c desc LIMIT 1];
                
                oldOrdnew = [SELECT NE__OptyId__c,NE__Version__c , Id,NE__OrderStatus__c  FROM NE__Order__c where NE__OptyId__c=:oldOrd[0].NE__OptyId__c  ORDER BY NE__Version__c desc LIMIT 1];
            //}
            
            if(oldOrd != null && oldOrd.size() > 0){
                    //Find the contract 
                    if(oldOrd[0].NE__OptyId__r.BI_Acuerdo_Marco__c != '') 
                    { 
                        List <NE__Contract_Account_Association__c> result = [SELECT Id, NE__Contract_Header__c FROM NE__Contract_Account_Association__c WHERE id =: oldOrd[0].NE__OptyId__r.BI_Acuerdo_Marco__c ]; 
                        
                        if(result.size()>0) 
                            contractHeader = '&contractHeaderId='+result[0].NE__Contract_Header__c ; 
                    }

                    if(oldOrd[0].NE__OptyId__r.BI_Producto_bloqueado__c!=true)
                    {
                        if(oldOrd[0].NE__OptyId__r.AccountId !=null)
                        {
                            if(oldOrd[0].NE__OptyId__r.NE__Order_Generated__c == '' || oldOrd[0].NE__OptyId__r.NE__Order_Generated__c == null ) 
                            {
                                if (oldOrd[0].NE__OptyId__r.NE__HaveActiveLineItem__c== true || optytype == 'Quote') 
                                {       
                                    Map <Integer, BI_bypass__c> mapa;
                                    Map <String , String> gen_map;
                                    NE.DataMap mp = new NE.DataMap();
                                    try{
                                        mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
                                        if(optytype == 'Quote'){
                                            gen_map = mp.GenerateMapObjects('BI_O4_MapOfQuote2Configuration', oldOrd[0].id);
                                        }
                                        else{
                                            gen_map = mp.GenerateMapObjects('Order2Order', oldOrd[0].id);
                                        }
                                    }catch(Exception e){
                                    }
                                    finally{
                                        if(mapa != null){
                                            BI_MigrationHelper.disableBypass(mapa);
                                        }
                                    }
                                    
                                    
                                    
                                    if(gen_map!=null){ 
                                        newOrdId = gen_map.get('ParentId');
                                        errorCode    =  gen_map.get('ErrorCode'); 
                                        system.debug('*ERRORCODE ' + errorCode);
                                        if(errorCode.equalsIgnoreCase('0')){
                                            NE__Order__c newOrd;    
                                            newOrd = [SELECT Id,Name,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c, BI_O4_Fecha_Entrega_oferta_comprometida__c, BI_O4_Fecha_entrega_oferta_ingenieria__c,
                                                            BI_O4_Fecha_de_vigencia_de_la_oferta__c, BI_O4_CAPEX_total__c, BI_O4_OPEX_recurrente_total__c, BI_O4_OPEX_no_recurrente_total__c FROM NE__Order__c WHERE Id =: newOrdId];
                                                            
                                            newOrd.NE__SerialNum__c             =   newOrd.Name;
                                            newOrd.NE__OrderStatus__c           =   'Active';
                                            newOrd.NE__ConfigurationStatus__c   =   'Valid';
                                            newOrd.NE__OpportunityId__c         =   oldOrd[0].NE__OptyId__c;
                                            newOrd.NE__OptyId__c                =   oldOrd[0].NE__OptyId__c;
                                            //Decimal vers                        =   newOrd.NE__Version__c;

                                            if(optytype == 'Quote'){
                                                newOrd.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Opty' AND sObjectType = 'NE__Order__c' LIMIT 1].Id;

                                                Decimal vers = 0;
                                                List <NE__Order__c> lst_ord = [SELECT NE__Version__c, NE__OrderStatus__c FROM NE__Order__c WHERE RecordType.DeveloperName = 'Opty' AND NE__OptyId__c =: newOrd.NE__OptyId__c ORDER BY NE__Version__c DESC LIMIT 1];
                                                if (!lst_ord.isEmpty()) {
                                                    newOrd.NE__Version__c = lst_ord[0].NE__Version__c + 1;
                                                    /*START Álvaro López 19/03/2018 - Forzar cambio de estado de la antigua varsión a Revised*/
                                                    lst_ord[0].NE__OrderStatus__c    =   'Revised';
                                                    System.debug('Status new: '+newOrd.NE__OrderStatus__c+ ' -old: '+ lst_ord[0].NE__OrderStatus__c);
                                                    update lst_ord[0];
                                                     /*END Álvaro López 19/03/2018*/
                                                }
                                                else {
                                                    newOrd.NE__Version__c = 1;
                                                }
                                                Opportunity opp = new Opportunity(Id = oldOrd[0].NE__OptyId__c, 
                                                                                BI_O4_Fecha_Entrega_oferta_comprometida__c = newOrd.BI_O4_Fecha_Entrega_oferta_comprometida__c,
                                                                                BI_O4_Fecha_entrega_oferta_ingenieria__c = newOrd.BI_O4_Fecha_entrega_oferta_ingenieria__c,
                                                                                BI_Fecha_de_vigencia__c = newOrd.BI_O4_Fecha_de_vigencia_de_la_oferta__c,
                                                                                BI_ARG_CAPEX_total__c = newOrd.BI_O4_CAPEX_total__c,
                                                                                BI_ARG_OPEX_recurrente_total__c = newOrd.BI_O4_OPEX_recurrente_total__c,
                                                                                BI_ARG_OPEX_no_recurrente_total__c = newOrd.BI_O4_OPEX_no_recurrente_total__c);
                                                update opp;
                                                
                                            }
                                            if(optytype == 'Opty2'){
                                                newOrd.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Quote' AND sObjectType = 'NE__Order__c' LIMIT 1].Id;
                                                newOrd.NE__OrderStatus__c = 'Draft';
                                                List <NE__Order__c> lst_version = [SELECT NE__Version__c FROM NE__Order__c WHERE NE__OptyId__c =: newOrd.NE__OptyId__c AND RecordType.DeveloperName = 'Quote' ORDER BY NE__Version__c DESC LIMIT 1];
                                                if(!lst_version.isEmpty()){
                                                    newOrd.NE__Version__c = lst_version[0].NE__Version__c + 1;
                                                }
                                                else{
                                                    newOrd.NE__Version__c = 1;
                                                }
                                                oldOrd[0].NE__OrderStatus__c = 'Active';
                                                update new List <NE__Order__c> {newOrd, oldOrd[0]};

                                            }

                                            if(optytype == 'New Version'){
                                                Decimal version                 =   oldOrdnew[0].NE__Version__c+1;
                                                system.debug('version: '+version);
                                                newOrd.NE__Version__c           =   version;
                                                if(oldOrdnew[0].Id == oldOrd[0].Id)
                                                {
                                                    oldOrd[0].NE__OrderStatus__c    =   'Revised';
                                                    update oldOrd[0];
                                                }
                                                else{
                                                    oldOrdnew[0].NE__OrderStatus__c    =   'Revised';
                                                    update oldOrdnew[0];
                                                }
                                                
                                                Opportunity opty                        = [SELECT id,BI_Oferta_tecnica__c,BI_Origen_de_la_oferta_tecnica__c,BI_Comite_de_negocios__c,BI_Oferta_economica__c FROM Opportunity WHERE id =: oldOrd[0].NE__OptyId__c]; 
                                                opty.BI_Origen_de_la_oferta_tecnica__c  = ''; 
                                                opty.BI_Oferta_economica__c             = ''; 
                                                opty.BI_Comite_de_negocios__c           = '';    
                                                opty.BI_Oferta_tecnica__c               = '';
                                                update opty;                    
                                                
                                            }
                                            else if(optytype == 'Renewal'){
                                                newOrd.NE__Version__c = 1;
                                                item = [SELECT Name, NE__Status__c,  NE__Action__c, NE__OrderId__c FROM NE__OrderItem__c WHERE NE__OrderId__c = :newOrdId];
                                                
                                                for(NE__OrderItem__c it: item){
                                                    it.NE__Status__c = 'Active';
                                                    it.NE__Action__c = 'None';
                                                }
                                                update item;                    
                                            }   
                                            else if(optytype == 'Clone'){
                                                newOrd.NE__Version__c = 1;
                                            }
                                            if(optytype != 'Opty2'){
                                                update newOrd;
                                            }
                                            
                                            String prefix = Site.getPrefix();
                            
                                            if(prefix == null)
                                                prefix  = '';   
                                            
                                            //urlPage =     prefix + '/' + newOrdId; 
                                            if(optytype != 'Quote' && optytype != 'Opty2'){
                                                urlPage =   prefix+'/apex/NE__NewConfiguration?ordId='+newOrd.Id; 
                                            }
                                            else{
                                                urlPage = newOrd.Id;
                                            }
                                        }
                                    }   
                                }
                                else{
                                        String prefix = Site.getPrefix();
                            
                                        if(prefix == null)
                                            prefix  = '';  
                                        
                                        String type=   'New'; 
                                        String  sType=  oldOrd[0].NE__OptyId__r.BI_Ciclo_ventas__c; 
                                        urlPage = prefix +'/apex/NE__NewConfiguration?accId='+oldOrd[0].NE__OptyId__r.AccountId+'&servAccId='+oldOrd[0].NE__OptyId__r.AccountId+'&billAccId='+oldOrd[0].NE__OptyId__r.AccountId+'&oppId='+oldOrd[0].NE__OptyId__c+'&cType=New&cSType='+sType+'&confCurrency='+oldOrd[0].NE__OptyId__r.CurrencyIsoCode+contractHeader; 
                                }   
                            }
                            else
                            {
                                urlPage='KO';
                            }       
                        }
                        else
                        {
                            urlPage='KO';
                        }
                    }
                    else
                    {
                        urlPage='KO';
                    }
            }
            else{
                system.debug('No query matched!');
            }
        /*} 
        catch(Exception e)
        {
            System.debug('NEBit2winApi.cloneOptyconfiguration [Exception] = ' + e);
            urlPage =   '';
        }*/


        return urlPage; 
    } 

}