/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   the Apex Language Reference for more information about Testing and Code Coverage. 
Test Class:    CWP_AttachmentComp_Controller.cls


History:

<Date>                  <Author>                <Change Description>
28/02/2016              Everis                  Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/


@isTest
private class CWP_AttachmentComp_Controller_TEST {
    
    @isTest
    private static void myUnitTest() {
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;
        Account accCustomerCountry;
        Account accLegalEntity;
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            User usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
            
        }
        
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        //ORDER
        
        //NE__Asset__c testAsset = CWP_TestDataFactory.createAsset();
        //insert testAsset;
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'Asset');        
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        list<NE__OrderItem__c> oiChildrenList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[0].id;
        oiChildrenList.add(newOI);
        
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[0].id;
        oiChildrenList.add(newOI);
        
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[1].id;
        oiChildrenList.add(newOI);
        
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[1].id;
        oiChildrenList.add(newOI);
        
        insert oiChildrenList;
        
        
        
        /*
private final set<string> REQUESTRT = new set<string>{'Order_Management_Case'};
private final set<string> CASERT = new set<string>{'TGS_Billing_Inquiry', 'TGS_Change', 'TGS_Complaint', 'TGS_Incident', 'TGS_Problem', 'TGS_Query'};
*/
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1.1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3a';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3aa';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1b';
        newCase.TGS_Product_Tier_2__c = 't2b';
        newCase.TGS_Product_Tier_3__c = 't3b';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[1].id;
        newCase.contactId = contactTest.id;                
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub8', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3a';
        newCase.TGS_Customer_Services__c = oiList[1].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub9', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3aa';
        newCase.TGS_Customer_Services__c = oiList[1].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('attchment body');
        attachment.Name = String.valueOf('test.txt');
        attachment.ParentId = accLegalEntity.Id; 
        insert attachment;
        
        insert listaDeCasos;        
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        list<NE__Order_Item_Attribute__c> oiaList = new list<NE__Order_Item_Attribute__c>();
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiChildrenList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiChildrenList[0].id, famProp.id);
        oiaList.add(oia1);
        
        insert oiaList;
        
        
        CWP_Panel__c controlPanel = new CWP_Panel__c();
        controlPanel.CWP_QueryLimit_Case__c = 200;
        insert controlPanel;
        //Case auxCaso =                
        Case c = [Select id From Case limit 1];
        // TO DO: implement unit test
        CWP_AttachmentComp_Controller.CWP_createAttachments('asd.docx', 'asd', 'UEs/vHeYl0+Qn3f+G74aELvxsTpikCA');
        CWP_AttachmentComp_Controller.CWP_createAttach(c.id,'asd.docx', 'asd', 'UEsDBBQ','UEsDBBQ');
    }
}