@isTest public with sharing class BI2_EmailMessageMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       Salesforce.com
     Description:   Test class for BI2_EmailMessageMethods
     
     History:

     <Date>         <Author>                        <Change Description>
     05/10/2015     Jose Miguel Fierro              Initial Version
     02/01/2017       Marta Glez                    REING-01-Adaptacion reingenieria de contactos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest public static void test_setAssignations_identificado1contacto() {
        BI_TestUtils.throw_exception  = false;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        Profile prof = [SELECT Id FROM Profile WHERE Name IN ('System Administrator', 'Administrador del Sistema') LIMIT 1];
        User asesor = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        /* TESTSETUP */
            List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case'];
        
            List<Account> lst_clients = new List<Account>();
            for(Integer iter = 0; iter < 2; iter++) {
                lst_clients.add(new Account(Name='TestAccount_0'+iter,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test'));
            }
            lst_clients[0].BI2_asesor__c=UserInfo.getUserId();
            lst_clients[1].BI2_cuadrilla__c = lst_colas[0].Queue.DeveloperName;
            insert lst_clients;
        
            System.runas(asesor) {
                List<Contact> pre_lst_contacts = new List<Contact>();
                for(Integer iter = 0; iter < lst_clients.size(); iter++) {
                    //pre_lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id));
                    /*REING-01-INI*/pre_lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id, BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '00000000X'));
                }
                //pre_lst_contacts[0].BI_tipo_de_contacto__c='Autorizado';
                pre_lst_contacts[0].BI_Tipo_de_contacto__c='General;Autorizado';
                //REING-01-FIN
                pre_lst_contacts[0].Email = 'josemi.fierro@example.com';
                insert pre_lst_contacts;
            }
        /* TESTSETUP */
        List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName ='BI2_Caso_Padre'];
        Case cas = new Case(SuppliedEmail='josemi.fierro@example.com', Origin='Correo Electrónico', RecordTypeId=lst_rt[0].Id);
        insert cas;

        String sendEmail = Label.BI2_Destinatario_Email_to_Case.split(' ')[0];
        Test.startTest();
        EmailMessage em = new EmailMessage();
        em.Incoming = true;
        em.FromAddress = 'josemi.fierro@example.com';
        em.FromName = 'Test';
        em.ToAddress = sendEmail;
        em.ParentId = cas.Id;

        insert em;
        Test.stopTest();
    }

    @isTest public static void test_exceptions() {
        BI_TestUtils.throw_exception  = true;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        BI2_EmailMessage.setAssignations(null);
        BI2_EmailMessage.setOwner(null, null, null);
        BI2_EmailMessage.sendEmailsAndTasks(null, null, null, null);
    }
    
    @isTest public static void test_setAssignations_noIdentificado() {
        BI_TestUtils.throw_exception  = false;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName ='BI2_Caso_Padre'];
        Case cas = new Case(RecordTypeId=lst_rt[0].Id);
        insert cas;

        Test.startTest();

        EmailMessage em = new EmailMessage();

        String sendEmail = Label.BI2_Destinatario_Email_to_Case.split(' ')[0];

        em.ToAddress = sendEmail;
        em.Incoming = true;
        em.FromAddress = 'josemi.fierro@example.com';
        em.FromName = 'Test';
        em.ParentId = cas.Id;

        insert em;
        Test.stopTest();
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        
     Company:       Salesforce.com
     Description:   
     
     History:

     <Date>         <Author>                        <Change Description>
     XX/XX/XXXX                                     Initial Version
     02/01/2017       Marta Glez                    REING-01-Adaptacion reingenieria de contactos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest public static void test_setAssignations_identificadoNcontactos() {
        BI_TestUtils.throw_exception  = false;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        Profile prof = [SELECT Id FROM Profile WHERE Name IN ('System Administrator', 'Administrador del Sistema') LIMIT 1];
        User asesor = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        /* TESTSETUP */
            List<Group> lst_grps = new List<Group> {
                new Group(Type='Queue', DeveloperName='TestQuad', Name='TestQuad')
            };
            insert lst_grps;
            System.runAs(new User(Id=UserInfo.getUserId())) {
                insert new List<GroupMember> {
                    new GroupMember(GroupId=lst_grps[0].Id, UserOrGroupId=UserInfo.getUserId())
                };
            }
            System.runAs(new User(Id=UserInfo.getUserId())) {
                insert new List<QueueSObject> {
                    new QueueSObject(QueueId = lst_grps[0].Id, SObjectType='Case')
                };
            }
        
            List<Account> lst_clients = new List<Account>();
            for(Integer iter = 0; iter < 4; iter++) {
                lst_clients.add(new Account(Name='TestAccount_0'+iter,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test'));
            }
            lst_clients[0].BI2_cuadrilla__c = lst_grps[0].Name;
            lst_clients[0].BI2_cuadrilla_id__c = lst_grps[0].Id;
            insert lst_clients;
        
            System.runas(asesor) {
                List<Contact> pre_lst_contacts = new List<Contact>();
                for(Integer iter = 0; iter < 4; iter++) {
                    pre_lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id));
                    //REING-01-INI
                    //pre_lst_contacts[iter].BI_tipo_de_contacto__c='Autorizado';
                    pre_lst_contacts[iter].BI_Tipo_de_contacto__c='General;Autorizado';
                    pre_lst_contacts[iter].BI_Tipo_de_documento__c = 'Otros';
                    pre_lst_contacts[iter].BI_Numero_de_documento__c = '00000000X';
                    //REING-01-FIN
                    pre_lst_contacts[iter].Email = (math.mod(iter, 2) == 0)? 'josemi.fierro@example.com' : 'josemi.fierro@example.es';
                }
                insert pre_lst_contacts;
            }
        /* TESTSETUP */

        List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName ='BI2_Caso_Padre'];
        List<Case> lst_cas = new List<Case> {
            new Case(SuppliedEmail='josemi.fierro@example.com', Origin='Correo Electrónico', RecordTypeId=lst_rt[0].Id),
            new Case(SuppliedEmail='josemi.fierro@example.es', Origin='Correo Electrónico', RecordTypeId=lst_rt[0].Id)
        };
        insert lst_cas;

        Test.startTest();

        String sendEmail = Label.BI2_Destinatario_Email_to_Case.split(' ')[0];

        List<EmailMessage> lst_ems = new List<EmailMessage> {
            new EmailMessage(Incoming = true, FromAddress = 'josemi.fierro@example.com',
                             FromName = 'Test.com', ToAddress = sendemail, ParentId = lst_cas[0].Id),
                
            new EmailMessage(Incoming = true, FromAddress = 'josemi.fierro@example.es',
                             FromName = 'Test.es', ToAddress = sendEmail, ParentId = lst_cas[1].Id)
        };

        insert lst_ems;
        Test.stopTest();
    }
    
    @isTest public static void test_setAssignations_identificado1contacto_noAuth() {
        BI_TestUtils.throw_exception  = false;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        Profile prof = [SELECT Id FROM Profile WHERE Name IN ('System Administrator', 'Administrador del Sistema') LIMIT 1];
        User asesor = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        /* TESTSETUP */
            List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case'];
        
            List<Account> lst_clients = new List<Account>();
            for(Integer iter = 0; iter < 2; iter++) {
                lst_clients.add(new Account(Name='TestAccount_0'+iter,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test'));
            }
            lst_clients[0].BI2_asesor__c=UserInfo.getUserId();
            lst_clients[1].BI2_cuadrilla__c = lst_colas[0].Queue.DeveloperName;
            insert lst_clients;
        
            System.runas(asesor) {
                List<Contact> pre_lst_contacts = new List<Contact>();
                for(Integer iter = 0; iter < lst_clients.size(); iter++) {
                    //pre_lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id));
                     /*REING-01-INI*/pre_lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id, BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '00000000X'));
                     /*REING-01-FIN*/
                }
                /*pre_lst_contacts[0].BI_tipo_de_contacto__c='General';*/   // Contacto no autorizado
                pre_lst_contacts[0].Email = 'josemi.fierro@example.com';
                insert pre_lst_contacts;
            }
        /* TESTSETUP */

        List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName ='BI2_Caso_Padre'];
        Case cas = new Case(SuppliedEmail='josemi.fierro@example.com', Origin='Correo Electrónico', RecordTypeId=lst_rt[0].Id);
        insert cas;

        Test.startTest();

        EmailMessage em = new EmailMessage();

        String sendEmail = Label.BI2_Destinatario_Email_to_Case.split(' ')[0];
        em.ToAddress = sendEmail;
        em.Incoming = true;
        em.FromAddress = 'josemi.fierro@example.com';
        em.FromName = 'Test';
        em.ParentId = cas.Id;

        insert em;
        Test.stopTest();
    }
    
    @isTest public static void test_setAssignations_identificadoNcontactos_noAuth() {
        BI_TestUtils.throw_exception  = false;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        Profile prof = [SELECT Id FROM Profile WHERE Name IN ('System Administrator', 'Administrador del Sistema') LIMIT 1];
        User asesor = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        /* TESTSETUP */
            List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case'];
        
            List<Account> lst_clients = new List<Account>();
            for(Integer iter = 0; iter < 2; iter++) {
                lst_clients.add(new Account(Name='TestAccount_0'+iter,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test'));
            }
            lst_clients[0].BI2_cuadrilla__c = lst_colas[0].Queue.DeveloperName;
            insert lst_clients;
        
            System.runas(asesor) {
                List<Contact> pre_lst_contacts = new List<Contact>();
                for(Integer iter = 0; iter < 2; iter++) {
                    pre_lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id));
                    /*pre_lst_contacts[iter].BI_tipo_de_contacto__c='General';*/ // Contacto no autorizado
                    pre_lst_contacts[iter].Email = 'josemi.fierro@example.com';
                }
                insert pre_lst_contacts;
            }
        /* TESTSETUP */

        List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName ='BI2_Caso_Padre'];
        Case cas = new Case(SuppliedEmail='josemi.fierro@example.com', Origin='Correo Electrónico', RecordTypeId=lst_rt[0].Id);
        insert cas;

        Test.startTest();
        EmailMessage em = new EmailMessage();

        String sendEmail = Label.BI2_Destinatario_Email_to_Case.split(' ')[0];
        em.ToAddress = sendEmail;
        em.Incoming = true;
        em.FromAddress = 'josemi.fierro@example.com';
        em.FromName = 'Test';
        //em.ToAddress = 'josemi.fierro@aborda.es';
        em.ParentId = cas.Id;

        insert em;
        Test.stopTest();
    }
}