@RestResource(urlMapping = '/categories')
global class API_AC_Categories {
	private static final String PARAMNAME			= 'name';
	private static final String PARAMCOUNTRY		= 'fields';
	private static final String PARAMCITY			= 'limit';
	private static final String COUNTRYCITY			= 'country-city';
	private static final String CONTRACTTERMS		= 'contract-term';

	private static final String REQUESTTYPEERROR	= 'Type requested invalid.';
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Method that manage the GET API call.
    
	History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static void doGet() {
		System.debug('API_AC_Categories##doGet');
		getCategories();
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Method that retrieve cities or contract terms.
    	
	History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static void getCategories() {
		System.debug('API_AC_Categories##getCategories');
		RestRequest request = RestContext.request;

		String type = request.params.get(PARAMNAME);

		System.debug('URL Params ==> type : ' + type);

		List<WrpCategoryType> listCategoryTypes = null;
		if (type == COUNTRYCITY) {
			listCategoryTypes = getCountryCityData();
		} else if (type == CONTRACTTERMS) {
			String country = request.params.get(PARAMCOUNTRY);
			String city = request.params.get(PARAMCITY);
			
			System.debug('URL Params ==> country : ' + country);
			System.debug('URL Params ==> city : ' + city);

			listCategoryTypes = getContractTermData(country,city);
		} else {
			genericError(REQUESTTYPEERROR);
			return;
		}
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listCategoryTypes));
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	
    	
	History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static List<WrpCategoryType> getCountryCityData() {
		List<AggregateResult> listItems = [	SELECT	CWP_AC_Country__c,
													CWP_AC_City__c
											FROM CWP_AC_ServicesCost__c
											GROUP BY CWP_AC_Country__c,CWP_AC_City__c];
			
		System.debug('SOQL Query ==> Country-City # listItems : ' + listItems);

		List<WrpCategoryType> listCategoryTypes = new List<WrpCategoryType>();
		for (AggregateResult item : listItems)
			listCategoryTypes.add(new WrpCategoryType(COUNTRYCITY,item.get('CWP_AC_Country__c') + '-' + item.get('CWP_AC_City__c')));
		return listCategoryTypes;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	
    	
	History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static List<WrpCategoryType> getContractTermData(String country,String city) {
		List<CWP_AC_ServicesCost__c> listItems = [	SELECT	CWP_AC_Contract_Term__c
													FROM	CWP_AC_ServicesCost__c
													WHERE	CWP_AC_Country__c = :country
														AND CWP_AC_City__c = :city];
			
		System.debug('SOQL Query ==> CWP_AC_ServicesCost__c # listItems : ' + listItems);
		
		Set<String> setStr = new Set<String>();
		for (CWP_AC_ServicesCost__c item : listItems)
			setStr.add('' + item.CWP_AC_Contract_Term__c);
		List<WrpCategoryType> listCategoryTypes = new List<WrpCategoryType>();
		for (String str : setStr)
			listCategoryTypes.add(new WrpCategoryType(CONTRACTTERMS,'' + str));
		System.debug(listCategoryTypes);
		return listCategoryTypes;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	
    	
	History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static void genericError(String errorType) {
		RestContext.response.responseBody = Blob.valueOf(errorType);
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Wrapper for the response.
    	
	History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global class WrpCategoryType {
		webservice String id;	// Se guarda si es 'country-city' o 'contract-term'.
		webservice String name;	// El valor.
		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    	Author:			Angel Felipe Santaliestra Pasias
    	Company:		Accenture
    	Description:	Constructor.
	
		History:
		<Date>			<Author>					<Description>
    	20/04/2018		Angel F Santaliestra		Initial version.
    	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
		public WrpCategoryType(String id, String name) {
			this.id = id;
			this.name = name;
		}
	}
}