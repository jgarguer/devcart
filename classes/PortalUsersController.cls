public with sharing class PortalUsersController {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Class to manage portal users
    Test: 		   BI_PortalUsersController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    15/07/2014       		 Micah Burgos      		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public list<BI_Contact_Customer_Portal__c> listCCP {get;set;}
	public boolean saveOK {get; set;}
	public String contactID {get; set;}
	private final User UserF;
	
	public PortalUsersController(ApexPages.StandardController stdController){
		this.UserF = (User)stdController.getRecord();
		saveOK = false;
		listCCP = getContactCustomerPortalOfUser();
		//system.debug('PortalUsersController-> constructor: listCCP[] '+listCCP);
	}

	
	
	
	public void save(){
		try{
			list<BI_Contact_Customer_Portal__c> listInsert = new list<BI_Contact_Customer_Portal__c>();
			list<BI_Contact_Customer_Portal__c> listDelete = new list<BI_Contact_Customer_Portal__c>();
			
			//system.debug('PortalUsersController-> save: listCCP[] '+listCCP);
	
			Boolean foundError;
			foundError = false;
			
			set<Id> idContactsToInsert = new set<Id>();
			
			for(BI_Contact_Customer_Portal__c CCP : listCCP){
	
				if(CCP.BI_Eliminar__c && CCP.Id != null){
					listDelete.add(CCP);				
				}else if (CCP.BI_Eliminar__c == false){
					
					if(idContactsToInsert.contains(CCP.BI_Contacto__c)){
						CCP.BI_Contacto__c.addError(Label.BI_ErrorContactoDuplicado);
						foundError = true;
					}else{
						listInsert.add(CCP);
					}
					
					idContactsToInsert.add(CCP.BI_Contacto__c);
				}
			}
			
			//system.debug('PortalUsersController-> save: listDelete[] '+listDelete);
			//system.debug('PortalUsersController-> save: listInsert[] '+listInsert);
			
			if(!listDelete.isEmpty() && !foundError){
				set<BI_Contact_Customer_Portal__c> setCCP = new set<BI_Contact_Customer_Portal__c>(listCCP);
				setCCP.removeAll(listDelete);
				listCCP = new list<BI_Contact_Customer_Portal__c>(setCCP);
				delete listDelete;
			}
			
			if(!listInsert.isEmpty() && !foundError){
	
				upsert listInsert;
			}
			
			if(!foundError){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, Label.BI_ActualizadoCorrectamente, Label.BI_CamposActualizados));
			}else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.BI_ErrorActualizar, Label.BI_ReviseDatos));
			}
		}catch (exception e){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.BI_ErrorActualizar, Label.BI_ReviseDatos));
		  		BI_LogHelper.generateLog('PortalUsersController.save', 'Portal Platino', e.getMessage(), 'Class');
		}
	}
	
	private map<Id,Id> getAccountsOfContacts(List<BI_Contact_Customer_Portal__c> ccpList){
			set<Id> contactsIds = new set<Id>();
			for(BI_Contact_Customer_Portal__c contList :ccpList){
				contactsIds.add(contList.BI_Contacto__c);
			}
			
			map<Id, Id>contactAccMap = new map<Id, Id>();
			list<Contact> contactAccList = [SELECT AccountId, Id from Contact Where Id IN :contactsIds];
			
			for(Contact conAcc :contactAccList){
				contactAccMap.put(conAcc.Id,conAcc.AccountId);
			}
			return contactAccMap;
	}
	
	public void addItem(){
			BI_Contact_Customer_Portal__c CCP = new BI_Contact_Customer_Portal__c();
			CCP.BI_User__c = this.UserF.Id; //UserInfo.getUserId();
			CCP.BI_Activo__c = true;
			listCCP.add(CCP);
	}
	
	public list<BI_Contact_Customer_Portal__c> getContactCustomerPortalOfUser(){
			List<BI_Contact_Customer_Portal__c> listCCP_user= [SELECT  BI_Cliente__c, BI_Activo__c,BI_BO_Pwd__c,BI_BO_User__c,BI_Contacto__c,BI_Eliminar__c,BI_Perfil__c,BI_SW_Pwd__c,BI_SW_User__c,BI_User__c
														FROM BI_Contact_Customer_Portal__c 
														WHERE BI_User__r.Id = :this.UserF.Id];
			return listCCP_user;
	}
	
	public void refreshAcc(){

		//system.debug('PortalUsersController-> refreshAcc(): listCCP[] INICIAL :'+listCCP);
	
		map<Id,Id> mapAccContact = getAccountsOfContacts(listCCP);
		//system.debug('PortalUsersController-> refreshAcc(): mapAccContact[]  :'+mapAccContact);
		for(BI_Contact_Customer_Portal__c CCP :listCCP){
			CCP.BI_Cliente__c = mapAccContact.get(CCP.BI_Contacto__c);
		}
		
		//system.debug('PortalUsersController-> refreshAcc(): listCCP[] FINAL :'+listCCP);
	}
}