@isTest(SeeAllData=true)
public class PCA_Knowledge_Controller_TEST {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Beatriz Garcés
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_Content_Controller class 

    <Date>                  <Author>                <Description>
    28/05/2015              Beatriz Garcés          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static testMethod void getloadInfo() {
        //User userTest = [SELECT Id, Name FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Community') AND isActive = true LIMIT 1];
        User userTest = [SELECT Id, Name FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS Customer Community') AND isActive = true LIMIT 1];
        System.runAs(userTest){
            Test.startTest();
            PCA_Knowledge_Controller controller = new PCA_Knowledge_Controller();
            controller.Search();
            Test.stopTest();
        }
        
    }
    
    
    

}