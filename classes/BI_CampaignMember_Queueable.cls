//07/12/2015        Guillermo Muñoz      Prevent the email send in Test class 
public without sharing class BI_CampaignMember_Queueable implements Queueable {
    
    public Set<Id> setId;
    public Integer option;
    //APC 06/10/2016 - 100 batch to prevent Apex CPU limit error
    //JRM 15/03/2017 - 25 batch to prevent Apex CPU limit error
    final Integer MAX_RECORDS = 25;

    Set<Id> set_id_records_to_process;
    Set<Id> set_id_records_to_future_process;
    Set<Id> setId_campToEmail;
    Set<Id> set_id_for_notify; //D295

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    final static Map<String,Id> MAP_NAME_EMAIL_TEMPLATE = new Map<String,Id>();
    
    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('Opportunity','NE__OrderItem__c','NE__Order__c')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }

       for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('Deployment_Started','Deployment_Completed')]){ //TO DO:
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }


    /*
    0. Convertir campañas en CamMemb y trabajar en éste método solo con CamMemb.

    1.Analizar el set de Ids que me vienen, con ello, porcesar los posibles con MAX_RECORDS y rellamar a esta clase con la misma lista de setId 
    pero sin los ya procesados para volver a procesar otra fracción.  
    
    2.En caso de que sea mayor volver a repetir el proceso, en caso de que sea menor es la última ejecución.
    

    Modification: D295
    BI_CampaignMethods.sendNotificationNewTasksToOwner(set_id_for_notify);
    After the creation of Opportunities and Tasks
    */

    public BI_CampaignMember_Queueable(Set<Id> setId_param, Integer option_param, Set<Id> setId_campToEmail_param) {
        //Creamos la copia, para enviar luego las notificaciones D295
        if(set_id_for_notify == null){
            set_id_for_notify = new Set<Id>(setId_param);
        }

        system.debug('CONSTRUCTOR : setId_param: ' + setId_param);
        

        setId = setId_param;
        system.debug('CONSTRUCTOR : setId: ' + setId);

        option = option_param;
        system.debug('CONSTRUCTOR : setId_campToEmail_param: ' + setId_campToEmail_param);
        Set<Id> aux = setId_campToEmail_param;
        system.debug('CONSTRUCTOR : aux: ' + setId_campToEmail_param);
        setId_campToEmail = aux;
        system.debug('CONSTRUCTOR : setId_campToEmail: ' + setId_campToEmail);

        if(option_param != 1){//Change Campaign Id to CampaingMembers Id.
            List<CampaignMember> lst_campMemId = [SELECT ID FROM CampaignMember WHERE CampaignId IN :setId];
            system.debug('CONSTRUCTOR : lst_campMemId: ' + lst_campMemId);
            setId.clear();
            for(CampaignMember cm_id :lst_campMemId){
                setId.add(cm_id.Id);
            }
        }else{
            setId_campToEmail.clear();
        }     

        system.debug('CONSTRUCTOR : splitArray - setId: ' + setId);
        Map<Integer,Set<Id>> map_records = splitArray(setId, MAX_RECORDS);

        set_id_records_to_process = map_records.get(0);
        set_id_records_to_future_process = map_records.get(1);



    }

    public void execute(QueueableContext context) {
        system.debug('START ***OPP***: set_id_records_to_process: ' + set_id_records_to_process);

        List<Opportunity> lst_o = new List<Opportunity>();
        try{
            List<Id> lst_account = new List<Id>();
            List<String> lst_cp = new List<String>();
            List<CampaignMember> lst_cm_opp = new List<CampaignMember>();
            List<Opportunity> lst_toCreateCatalog = new list<Opportunity>();
            
            List<Task> lst_tasks = new List<Task>();

            List<CampaignMember> lst_cm = [SELECT Id, CampaignId, Campaign.Name, Campaign.CurrencyISOcode, Campaign.BI_Country__c, Campaign.EndDate, Campaign.BI_Tipo_de_preoportunidad__c, 
                                            Campaign.BI_Crear_tareas_prospectos__c, Campaign.BI_Crear_oportunidad_contactos__c, Campaign.IsActive, Campaign.OwnerId,
                                            Campaign.BI_Opportunity_Type__c, Campaign.BI_SIMP_Opportunity_Type__c, Campaign.BI_Asunto_de_las_tareas__c,
                                            Campaign.LastModifiedById, Campaign.RecordType.DeveloperName, LeadId, Lead.OwnerId, ContactId, Contact.AccountId, Contact.Account.Name, 
                                            Contact.Account.BI_Riesgo__c, Contact.Account.BI_Fraude__c, Contact.Account.BI_Activo__c, Contact.Account.OwnerId, 
                                            Contact.Account.BI_Country__c FROM CampaignMember WHERE 
                                            ID IN :set_id_records_to_process 
                                            AND Campaign.IsActive = true 
                                            AND  Campaign.RecordType.DeveloperName != :Label.BI_Evento
                                            AND Contact.Account.BI_Holding__c != :Label.BI_Si
                                            AND Contact.Account.BI_Fraude__c =  false
                                            AND Contact.Account.BI_Riesgo__c != :Label.BI_BloqueoCliente
                                             ]; //DataBase.query(query);

                                             //ADD CONDITIONS FOR FILTER RULE ON ACCOUNT TO OPPORTUNITY [Micah Burgos 27/04]
            
            system.debug('SIZE lst_cm: ' + lst_cm.size());  
            
            Set<Id> set_camp_Id = new Set<Id>();
            Set<String> set_tasks_created = new set<String>();
            for(CampaignMember cmpMemb: lst_cm) {
                set_camp_Id.add(cmpMemb.CampaignId);
            }

            for(Task tsk:[select Id, WhoId, BI_CampaignId__c from Task where BI_CampaignId__c IN :set_camp_Id]){
                set_tasks_created.add(tsk.WhoId+'---'+tsk.BI_CampaignId__c); //Check if task has been created.
            }

            
            for(CampaignMember cmem:lst_cm){
                
                //if(cmem.Campaign.IsActive && cmem.Campaign.RecordType.DeveloperName != Label.BI_Evento ){ // ADD TO QUERY

                    if(cmem.LeadId != null){
                        if(cmem.Campaign.BI_Crear_tareas_prospectos__c == Label.BI_Si && !set_tasks_created.contains(cmem.LeadId+'---'+cmem.CampaignId)){
                            
                            //system.debug('***BI_CampaignMember_Queueable-->cmem.Campaign.BI_Crear_tareas_prospectos__c: ' + cmem.Campaign.BI_Crear_tareas_prospectos__c);
                            
                            lst_tasks.add(BI_CampaignMemberHelper.createTaskLead(cmem.Campaign.Name, cmem.Campaign.EndDate, cmem.Campaign.OwnerId, cmem.LeadId, cmem.CampaignId, cmem.Lead.OwnerId, cmem.Campaign.BI_Asunto_de_las_tareas__c));//Generates the task
                        }
                    }else{
                         if(cmem.Contact.Account.BI_Fraude__c != true && cmem.Contact.Account.BI_Riesgo__c != Label.BI_BloqueoCliente){
                            //system.debug('***BI_CampaignMember_Queueable-->cmem.Contact.Account.BI_Activo__c: ' + cmem.Contact.Account.BI_Activo__c);
                            //system.debug('***BI_CampaignMember_Queueable-->cmem.Campaign.BI_Crear_oportunidad_contactos__c: ' + cmem.Campaign.BI_Crear_oportunidad_contactos__c);
                            //system.debug('***BI_CampaignMember_Queueable-->cmem.Campaign.BI_Tipo_de_preoportunidad__c: ' + cmem.Campaign.BI_Tipo_de_preoportunidad__c);
                            
                            if(cmem.Contact.Account.BI_Activo__c == Label.BI_Si && cmem.Campaign.BI_Crear_oportunidad_contactos__c == Label.BI_Si){
                                
                                lst_account.add(cmem.Contact.AccountId);
                                lst_cp.add(cmem.CampaignId);
                                lst_cm_opp.add(cmem);

                                
                            }
                        }
                    }
            }

            Set<String> name_opp = new Set<String>();
            
            if(!lst_cm_opp.isEmpty()){//Generates the preOpportunity
                String DescProd ='';
                BI_Promociones__c [] lst_prom = [SELECT Id, Nombre_del_producto__c, BI_Campanas__c FROM BI_Promociones__c WHERE BI_Campanas__c IN :lst_cp];
                name_opp = new Set<String>();
                
                for(Opportunity opp:[select Id, AccountId, CampaignId from Opportunity where AccountId IN :lst_account AND CampaignId IN :lst_cp]){
                    name_opp.add(opp.AccountId+'---'+opp.CampaignId);
                }
                    
                 system.debug('***BI_CampaignMember_Queueable-->name_opp (list): ' + name_opp);
                 
                for(CampaignMember cm2:lst_cm_opp){
                    DescProd ='';
                    String name_aux = cm2.Contact.AccountId+'---'+cm2.CampaignId;
                    for(BI_Promociones__c prom: lst_prom){
                        if (cm2.CampaignId == prom.BI_Campanas__c){
                            if (DescProd==''){                          
                                DescProd += Label.BI_Productos_campana +' \''+cm2.Campaign.name+'\': '+prom.Nombre_del_producto__c;
                            }else{
                                DescProd += ' | ' + prom.Nombre_del_producto__c;                            
                            }
                        }
                    }
                    //system.debug('***BI_CampaignMember_Queueable-->name_opp + name_aux: ' + name_opp+'---'+name_aux);
                    //APC - 03/10/2016 - Added Licitacion required field for Peru, to prevent validation rule error 
                    if(!name_opp.contains(name_aux)){
                            Opportunity opp = new Opportunity(CloseDate = cm2.Campaign.EndDate,
                                                            StageName = Label.BI_F6Preoportunidad,
                                                            Probability = 0,
                                                            BI_Country__c = cm2.Contact.Account.BI_Country__c,
                                                            OwnerId = cm2.Contact.Account.OwnerId,
                                                            BI_Licitacion__c = Label.BI_No,
                                                            //Name = cm2.Campaign.Name+'-'+cm2.Contact.Account.Name+'-Preoportunidad',
                                                            Name = split_Name(cm2.Campaign.Name ,cm2.Contact.Account.Name ),
                                                            CampaignId = cm2.CampaignId,
                                                            AccountId = cm2.Contact.AccountId,
                                                            RecordTypeId = MAP_NAME_RT.get(Label.BI_Preoportunidad),
                                                            Description = DescProd,
                                                            CurrencyISOcode = cm2.Campaign.CurrencyISOcode,
                                                            NE__HaveActiveLineItem__c = true,
                                                            BI_Opportunity_Type__c = cm2.Campaign.BI_Opportunity_Type__c,  //Jaime Regidor añadir valor a la oportunidad
                                                            BI_SIMP_Opportunity_Type__c = cm2.Campaign.BI_SIMP_Opportunity_Type__c);  //Jaime Regidor añadir valor a la oportunidad
                                                            
                            lst_o.add(opp);
                            name_opp.add(name_aux);
                    }
                }
                
                system.debug('***BI_CampaignMember_Queueable-->lst_o1: ' + lst_o);
                //insert lst_o;
                
                lst_toCreateCatalog.addAll(lst_o);
                
            }
            
            insert lst_o;
            //createCatalog(lst_toCreateCatalog);
            system.debug('***BI_CampaignMember_Queueable-->lst_tasks(BEFORE): ' + lst_tasks);
            insert lst_tasks;
        
        }catch (exception  Exc){
            Id log_id = BI_LogHelper.generate_BILog('BI_CampaignMember_Queueable', 'BI_EN', Exc, 'Trigger - Queueable');
        }

        system.debug('START ***CATALOG***');
        ///////////////////// CREATE CATALOG /////////////////////////////

        //public static void createCatalog (list<Opportunity> newsOpp){
        try{
            
            //mapa ID, Opp  
            map<Id,Opportunity> map_newsOpp = new map<Id,Opportunity>();
            set<Id> set_catalogHeader = new Set<Id>();
            
            for(Opportunity oppo:[SELECT AccountId, CampaignId, Campaign.BI_Catalogo__c, Campaign.BI_Catalogo__r.NE__Catalog_Header__c, CurrencyISOCode FROM Opportunity WHERE Id IN :lst_o]){
                map_newsOpp.put(oppo.Id, oppo);
                set_catalogHeader.add(oppo.Campaign.BI_Catalogo__r.NE__Catalog_Header__c);
            }
            
            if(!map_newsOpp.keySet().isEmpty()){
                
                Map<Id, Id> map_catalog_commercial = new Map<Id, Id>();
                if(!set_catalogHeader.isEmpty()){
                    for(NE__Commercial_Model__c comm:[select Id, NE__Catalog_header__c from NE__Commercial_Model__c where NE__Catalog_header__c IN :set_catalogHeader])
                        map_catalog_commercial.put(comm.NE__Catalog_header__c, comm.Id);
                }
            
                list<NE__Order__c> lst_orders = new list<NE__Order__c>();
                set<Id> set_campId = new set<Id>();
                for(Opportunity item :map_newsOpp.values()){ 
                    //APC - Fixed null pointer added NE__One_Time_Fee_Total__c, NE__Recurring_Charge_Total__c
                    NE__Order__c ord = new NE__Order__c();
                    ord.NE__TotalRecurringFrequency__c = 'Monthly';
                    ord.NE__OptyId__c = item.Id;
                    ord.NE__OrderStatus__c = Label.BI_Active;
                    ord.NE__AccountId__c = item.AccountId;
                    ord.NE__Order_date__c = Datetime.now();
                    ord.CurrencyISOCode = item.CurrencyIsoCode;
                    ord.NE__One_Time_Fee_Total__c = 0;
                    ord.NE__Recurring_Charge_Total__c = 0;
                    if(item.Campaign != null){
                        ord.NE__CatalogId__c = item.Campaign.BI_Catalogo__c;
                        if(item.Campaign.BI_Catalogo__c != null)
                            ord.NE__CommercialModelId__c = map_catalog_commercial.get(item.Campaign.BI_Catalogo__r.NE__Catalog_Header__c);
                    }
                    ord.RecordTypeId = MAP_NAME_RT.get('Opty');
                      
                    lst_orders.add(ord);
                    
                    set_campId.add(item.CampaignId);
                }
                       
                //Insert NE_ORDER__c
                insert lst_orders;
                
                map<Id,Campaign> map_camp = new map<Id,Campaign>([SELECT Id, (SELECT CurrencyISOcode, BI_Ingreso_por_unica_vez__c, Recurrente_bruto_mensual__c, 
                                                                  BI_Producto__r.NE__ProductId__c, BI_Producto__c, 
                                                                  BI_Producto__r.NE__Base_OneTime_Fee__c, BI_Producto__r.NE__BaseRecurringCharge__c,BI_Producto__r.NE__Recurring_Charge_Frequency__c FROM BI_Promociones__r)  
                                                                  FROM Campaign WHERE Id IN :set_campId]);
              
                system.debug('BI_CampaignMemberHelper->map_camp: ' + map_camp);

                RecordType rtOrdItem = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__OrderItem__c' AND DeveloperName = 'StanStandarddard' limit 1];
                list<NE__OrderItem__c> lst_ordItem = new list<NE__OrderItem__c>();
    
                Map<Id, Double> map_FCV = new Map<Id, Double>();
                
                system.debug('BI_CampaignMemberHelper->lst_orders: ' + lst_orders);

                for(NE__Order__c ord :lst_orders){
                    
                    Double value = 0;
                    
                    system.debug('BI_CampaignMemberHelper->1: ' + map_newsOpp.get(ord.NE__OptyId__c));
                    system.debug('BI_CampaignMemberHelper->2: ' + map_camp.get(map_newsOpp.get(ord.NE__OptyId__c).CampaignId));
                    
                   /* ord.NE__One_Time_Fee_Total__c = 0;
                    ord.NE__Recurring_Charge_Total__c = 0;*/

                    if(!map_camp.get(map_newsOpp.get(ord.NE__OptyId__c).CampaignId).BI_Promociones__r.isEmpty()){
                    
                        for(BI_Promociones__c promo :map_camp.get(map_newsOpp.get(ord.NE__OptyId__c).CampaignId).BI_Promociones__r){
                            
                            NE__OrderItem__c ordItem = new NE__OrderItem__c();
                                                   
                            ordItem.NE__OrderId__c = ord.Id;                        
                            ordItem.NE__Account__c = ord.NE__AccountId__c;          
                            
                            ordItem.RecordTypeId = MAP_NAME_RT.get('StanStandarddard');
                            ordItem.NE__Qty__c = 1;
                            ordItem.NE__Status__c = 'Pending';
                            
                            if(promo.BI_Ingreso_por_unica_vez__c != null){ 
                                ordItem.NE__OneTimeFeeOv__c = promo.BI_Ingreso_por_unica_vez__c; 
                                ordItem.NE__RecurringChargeOv__c = promo.Recurrente_bruto_mensual__c;
                                ordItem.NE__Commitment_Period__c = 12;//promo.BI_Permanencia_meses__c;
                            }else{
                                //if(promo.BI_Producto__r)
                                ordItem.NE__OneTimeFeeOv__c = promo.BI_Producto__r.NE__Base_OneTime_Fee__c; 
                                ordItem.NE__RecurringChargeOv__c = promo.BI_Producto__r.NE__BaseRecurringCharge__c;
                                ordItem.NE__Commitment_Period__c = 12;//promo.BI_Permanencia_meses__c;
                            }
                            
                            ordItem.NE__RecurringChargeFrequency__c = promo.BI_Producto__r.NE__Recurring_Charge_Frequency__c;
                            ordItem.NE__ProdId__c = promo.BI_Producto__r.NE__ProductId__c;
                            ordItem.CurrencyISOcode = promo.CurrencyISOcode;
                            
                            ordItem.NE__CatalogItem__c = promo.BI_Producto__c;
                            
                            Double value_aux = ordItem.NE__OneTimeFeeOv__c + (ordItem.NE__RecurringChargeOv__c * 12);
                            
                            if(ordItem.CurrencyISOcode != ord.CurrencyIsoCode)
                                value_aux = BI_CurrencyHelper.convertCurrency(ordItem.CurrencyISOcode, ord.CurrencyIsoCode, value_aux);
                            
                            value += value_aux;
                            
                            ord.NE__One_Time_Fee_Total__c += BI_CurrencyHelper.convertCurrency(ordItem.CurrencyISOcode, ord.CurrencyIsoCode, Double.valueOf(ordItem.NE__OneTimeFeeOv__c));
                            ord.NE__Recurring_Charge_Total__c += BI_CurrencyHelper.convertCurrency(ordItem.CurrencyISOcode, ord.CurrencyIsoCode, Double.valueOf(ordItem.NE__RecurringChargeOv__c));
                            
                            lst_ordItem.add(ordItem);   
                        }
                    }
                    
                    //Values rounded
                    Decimal recurring_charge_total = ord.NE__Recurring_Charge_Total__c;
                    Decimal one_time_fee_total = ord.NE__One_Time_Fee_Total__c;

                    //map_newsOpp.get(ord.NE__OptyId__c).BI_Ingreso_por_unica_vez__c = ord.NE__One_Time_Fee_Total__c;
                    map_newsOpp.get(ord.NE__OptyId__c).BI_Ingreso_por_unica_vez__c = one_time_fee_total.setscale(2);

                    //map_newsOpp.get(ord.NE__OptyId__c).BI_Recurrente_bruto_mensual__c = ord.NE__Recurring_Charge_Total__c;
                    map_newsOpp.get(ord.NE__OptyId__c).BI_Recurrente_bruto_mensual__c = recurring_charge_total.setscale(2);

                    map_newsOpp.get(ord.NE__OptyId__c).BI_Duracion_del_contrato_Meses__c = 12;
                    
                    map_newsOpp.get(ord.NE__OptyId__c).BI_casilla_desarrollo__c = true;
                    
                    system.debug('TEST: '+ord.NE__One_Time_Fee_Total__c);
                    system.debug('TEST2: '+ord.NE__Recurring_Charge_Total__c);
                    
                    //map_FCV.put(ord.NE__OptyId__c, value);
                    
                }   
                
                //Insert NE_OrderItems__c 
                insert lst_ordItem;
                system.debug('AUX 1 : lst_ordItem: ' + lst_ordItem);
                update lst_orders;
                
                update map_newsOpp.values();
            }    
        }catch (exception Exc){
            Id log_id = BI_LogHelper.generate_BILog('BI_CampaignMember_Queueable - createCatalog', 'BI_EN', Exc, 'Trigger - Queueable');
        }
        
        ////////////////////////////////////////////////////////Guillermo Muñoz Nieto////////////////////////////////////////////////////////////////
        //Create Task for the campaign member if the campaign member is a Contact
        try{
            list<Task> lst_tsk= new list<Task>();
            set<Id> set_camp_Id= new set<id>();
            set<String> setTaskCreated = new set<String>();
            list<CampaignMember> lst_cm = [SELECT Id, CampaignId, Campaign.Name, Campaign.EndDate, Campaign.IsActive, Campaign.BI_Crear_Tareas_Contactos__c,
                                            Campaign.BI_Asunto_de_las_tareas__c, ContactId, Contact.AccountId, Contact.Account.BI_Riesgo__c, 
                                            Contact.Account.BI_Fraude__c, Contact.Account.BI_Activo__c, Contact.Account.OwnerId
                                            FROM CampaignMember WHERE 
                                            ID IN :set_id_records_to_process 
                                            AND Campaign.IsActive = true 
                                            AND  Campaign.RecordType.DeveloperName != :Label.BI_Evento
                                            AND Contact.Account.BI_Holding__c != :Label.BI_Si
                                            AND Contact.Account.BI_Fraude__c =  false
                                            AND Contact.Account.BI_Riesgo__c != :Label.BI_BloqueoCliente
                                             ];
            for(CampaignMember cmId : lst_cm){
                set_camp_Id.add(cmId.CampaignId);
            }
                                             
            for(Task tsk:[select Id, WhoId, BI_CampaignId__c from Task where BI_Campana_contacto__c=true AND BI_CampaignId__c IN :set_camp_Id  ]){
                setTaskCreated.add(tsk.whoId + '---' + tsk.BI_CampaignId__c);
            }   
                    
            for(CampaignMember cm : lst_cm){
                if(cm.Campaign.BI_Crear_Tareas_Contactos__c == Label.BI_Si&&cm.Campaign.IsActive&&!setTaskCreated.contains(cm.ContactId + '---' + cm.CampaignId) && cm.contactId!=null){
                    Task tsk = new Task();
                    tsk.whoId = cm.ContactId;
                    tsk.ownerId = cm.Contact.Account.OwnerId;
                    tsk.subject = cm.Campaign.BI_Asunto_de_las_tareas__c;
                    tsk.Status = 'Not Started'; //ehelp 02584842
                    tsk.Priority = 'Normal';
                    tsk.activityDate = cm.Campaign.EndDate;
                    tsk.BI_CampaignId__c = cm.CampaignId;
                    tsk.BI_Campana__c = cm.CampaignId;
                    tsk.WhatId=cm.campaignId;
                    tsk.BI_Campana_contacto__c = true;
                    
                    lst_tsk.add(tsk);
                    System.debug('##Tarea---->' + tsk);
                }
            }
            System.debug('##'+Label.BI_Si);
            System.debug('##Guardé');
            insert lst_tsk;
            
        }
        catch(Exception exc){
            System.debug('##Fallé');
            Id log_id = BI_LogHelper.generate_BILog('BI_CampaignMember_Queueable - createTaskContacts', 'BI_EN', Exc, 'Trigger - Queueable');
        }
        ////////////////////////////////////////////////////End Guillermo Muñoz Nieto///////////////////////////////////////////////////////////////

        system.debug('START ***RECALL***');

        try{
            BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(!set_id_records_to_future_process.isEmpty() && !bypass.BI_stop_job__c){
                system.debug('RECALL : set_id_records_to_future_process: ' + set_id_records_to_future_process);
                System.enqueueJob(new BI_CampaignMember_Queueable(set_id_records_to_future_process, 1, setId_campToEmail));
            }else{
                List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name FROM EmailTemplate WHERE DeveloperName = 'BI_Fin_Activacion_Campana'];
                if(!lst_tmpl.isEmpty() && setId_campToEmail != null && !setId_campToEmail.isEmpty()){


                    Contact dummyContact = new Contact(LastName = 'noreply', Email = 'noreply@telefonica.com');
                    List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();
                    
                    insert dummyContact;

                    system.debug('QUERY : setId_campToEmail: ' + setId_campToEmail);
                    List<Campaign> lst_campEmail = [SELECT Id, OwnerId ,Owner.Email, LastModifiedById, LastModifiedBy.Email FROM Campaign WHERE Id IN :setId_campToEmail];

                    for(Campaign camp :lst_campEmail){
                        
                        Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

                            //if(map_contacts.containsKey(camp.Owner.Email)){
                            //  mailOwner.setTargetObjectId(map_contacts.get(camp.Owner.Email).Id);
                            //  mailOwner.setWhatId(camp.Id);  
                            //}else{
                            //  lst_toCreateContact.add(camp.Owner.Email);
                            //}
                            //mailOwner.setTargetObjectId(camp.OwnerId);
                            mailOwner.setTargetObjectId(dummyContact.Id);
                                                        
                            mailOwner.setTemplateId(lst_tmpl[0].Id);
                            mailOwner.setWhatId(camp.Id);  
                            //mailOwner.setBccSender(false);
                            //mailOwner.setUseSignature(false);
                            mailOwner.setReplyTo('noreply@telefonica.com');
                            mailOwner.setSenderDisplayName('Telefónica BI_EN');
                            mailOwner.setSaveAsActivity(false);

                            List<String> lst_toBcc = new List<String>();
                            lst_toBcc.add(camp.LastModifiedBy.Email);
                            if(camp.LastModifiedById != camp.OwnerId){
                                lst_toBcc.add(camp.Owner.Email);
                            }
                            mailOwner.setToAddresses(lst_toBcc);

                            lst_mails.add(mailOwner);
                    }
                    system.debug('lst_mails' +lst_mails);
                    try{
                        if(!Test.isRunningTest()){
                            Messaging.sendEmail(lst_mails);
                        }
                    }catch(exception Exc){
                        BI_LogHelper.generate_BILog('BI_CampaignMember_Queueable.createCatalog.sendEmail', 'BI_EN', Exc, 'Trigger - Queueable');
                    }
                    
                    delete dummyContact;


                    BI_CampaignMethods.sendNotificationNewTasksToOwner(set_id_for_notify); //D295
                }
            }
        }catch(Exception exc){
             BI_LogHelper.generate_BILog('BI_CampaignMember_Queueable.recall_queueable', 'BI_EN', Exc, 'Trigger - Queueable');
        }
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that split list in 2 blocks, the first block size = max_regs
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    12/03/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Map<Integer,Set<Id>> splitArray(Set<Id> lst_param, Integer max_regs) {

        if(lst_param != null && !lst_param.isEmpty()){
            Map<Integer,Set<Id>> map_return = new Map<Integer,Set<Id>>();
            map_return.put(0,new Set<Id>());
            map_return.put(1,new Set<Id>());

            Integer i = 0;
            for(Id var :lst_param){
                if(i < max_regs){
                    map_return.get(0).add(var);
                }else{
                    map_return.get(1).add(var);
                }
                i++;
            }
            return map_return;
        }else{
            return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that make smaller the name of new Opp
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    21/04/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String split_Name(String name_camp, String name_acc) {
     
        if(name_acc != null && name_camp != null){

            String aux_str = name_camp+'-'+name_acc+'-Preoportunidad';

            if(aux_str.length()> 120){
                aux_str = '';
                aux_str +=(name_camp.length()>50)? name_camp.substring(0,50) + '.' : name_camp;
                aux_str +='-';
                aux_str +=(name_acc.length()>50)? name_acc.substring(0,50)+ '.' : name_acc;
                aux_str +='-Preoportunidad';
            }

            return aux_str;
        }else{
            return null;
        }
    }

}