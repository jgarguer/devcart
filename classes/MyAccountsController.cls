public class MyAccountsController {

    public List<Account> accounts {get;set;}
    
    public MyAccountsController() {
        Id userId = UserInfo.getUserId();      
        List<Id> accountIds = new List<Id>();
           
        for (AccountShare accShare :[SELECT Id, AccountId FROM AccountShare WHERE UserOrGroupId = :userId]) {
            accountIds.add(accShare.accountId);
        }
        
        accounts = [select id, Name, BI_Country__c from Account where ownerId != :userId and (parent.OwnerId = :userId or id in :accountIds)];
                
    }

}