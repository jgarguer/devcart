public class DashboardController {

    public List<Case> listaCases {get; set;}
    public String strJSON {get; set;}
    public String JSON_SLA {get; set;}
    public String JSON_Orphan {get; set;}
    public String JSON_Incidents {get; set;}
    public String JSON_Changes {get; set;}
    
    public List<SelectOption> listaSMCs {get; set;}
    public String selectedSMC {get; set;}
    public String rodAdminViewUrl {get;set;}
    
    //public List<Case> topFiveIncidents {get; set; }
    
    public static Map<String, String> colorMap {get{
        Map<String, String> cMap = new Map<String, String>();
        cMap.put('Order Management Case', '#5CADFF');
        cMap.put('Billing Inquiry', '#DB4DB8');
        cMap.put('Complaint', '#33FF33');
        return cMap;
    } set;}
    
    public static Map<String, String> slaColorMap {get{
        Map<String, String> cMap = new Map<String, String>();
        cMap.put('Green', '#D6FF85');
        cMap.put('Yellow', '#FFFF4D');
        cMap.put('Red', '#FF3300');
        return cMap;
    } set;}
    
    /*
    public static Map<String, String> priorityColorMap {get{
        Map<String, String> cMap = new Map<String, String>();
        cMap.put('Low', '#D6FF85');
        cMap.put('Medium', '#FFFF4D');
        cMap.put('High', '#FFCC66');
        cMap.put('Critical', '#FF3300');
        cMap.put('To be Calculated', '#E0F0FF');
        return cMap;
    } set;}
    */
    public DashboardController() {
    	NamedCredential nc=[SELECT endpoint FROM NamedCredential WHERE developerName='rod_AdminView' LIMIT 1];
        rodAdminViewUrl=nc.Endpoint;
        
        //getOwnerIds();
        loadSMCs();
        
        strJSON = getCasesByType(selectedSMC);
        JSON_SLA = getCasesBySLA(selectedSMC);
        JSON_Orphan = getOrphanCasesByType(selectedSMC);

        //JSON_Incidents = getIncidentsByPriority();
        //JSON_Changes = getChangesByPriority();
        
        //getTopFive();
    }
    
    public void loadSMCs() {
    
        listaSMCs = new List<SelectOption>();
        
        //List<Group> listaQueues = [SELECT Name FROM Group WHERE Type = 'Queue' AND Name LIKE 'SMC%' AND (NOT Name LIKE '%Technical%')];
        
        listaSMCs.add(new SelectOption('','(Select a SMC)'));
        listaSMCs.add(new SelectOption('TGS_SMC_Madrid', 'SMC Madrid'));
        listaSMCs.add(new SelectOption('TGS_SMC_Miami', 'SMC Miami'));
        listaSMCs.add(new SelectOption('TGS_SMC_Prague', 'SMC Prague'));
        listaSMCs.add(new SelectOption('TGS_SMC_EMEA', 'SMC EMEA'));
        listaSMCs.add(new SelectOption('TGS_SMC_APAC_L', 'SMC APAC'));
        listaSMCs.add(new SelectOption('TGS_SMC_APAC_EMEA_', 'SMC APAC+EMEA'));
        
        
        //listaSMCs.add(new SelectOption('Others', 'Others'));
        /*
        for (Group smc : listaQueues) {
            SelectOption so = new SelectOption(smc.Id, smc.Name);
            listaSMCs.add(so);
        }
        */
        selectedSMC = listaSMCs[0].getValue();
    }
    
    
    @RemoteAction
    public static String getCasesByType(String selectedSMC) {
    
        String JSONString = '';
        List<Id> ownerIds = getOwnerIds(selectedSMC);
        
        AggregateResult[] results = [SELECT RecordType.Name theName, COUNT(Id) theCount FROM Case WHERE RecordType.Name IN ('Order Management Case', 'Billing Inquiry', 'Complaint') AND OwnerId IN :ownerIds GROUP BY RecordType.Name ];
        System.debug('### Aggregate results: ' + results.size());
    
        JSONString += '[';
        List<String> colors = new List<String>();
        
        for (integer i = 0 ; i < results.size(); i++)  {
            AggregateResult ar = results.get(i);
            String label = (string)ar.get('theName');
            Integer value = (Integer)ar.get('theCount');
            JSONString +='{';
            
            JSONString +='"label": "' + label + '",';
            JSONString +=' "value": "' + value + '",';
            JSONString +=' "color": "' + colormap.get(label) + '"';
            
            JSONString +='}';
            if (i != results.size()-1) {
                JSONString += ',';
            }
        }
        
        /*if (results.size() == 0) {
            JSONString +='{';
            
            JSONString +='"label": "No data",';
            JSONString +=' "value": "1",';
            JSONString +=' "color": "#DDDDDD"';
            
            JSONString +='}';
        }*/

        JSONString += ']';
        return JSONString;
    }
    
    @RemoteAction
    public static String getCasesBySLA (String selectedSMC) {
    
        String JSONString = '';
        List<Id> ownerIds = getOwnerIds(selectedSMC);
        AggregateResult[] results = [SELECT TGS_SLA_Light__c theColor, COUNT(Id) theCount FROM Case WHERE RecordType.Name = 'Order Management Case' AND TGS_SLA_Light__c <> '' AND OwnerId IN :ownerIds  GROUP BY TGS_SLA_Light__c];
        System.debug('### Aggregate results: ' + results.size());    

        JSONString += '[';
        List<String> colors = new List<String>();
        
        String red = '#FF0000';
        String yellow = '#FFFF47';
        String green = '#99FF66';

        for (integer i = 0 ; i < results.size(); i++)  {
            AggregateResult ar = results.get(i);
            String label = (string)ar.get('theColor');
            Integer value = (Integer)ar.get('theCount');
            JSONString +='{';
            
            JSONString +='"label": "' + label + '",';
            JSONString +=' "value": "' + value + '",';
            JSONString +=' "color": "' + slaColorMap.get(label) + '"';        
            
            JSONString +='}';
            if (i != results.size()-1) {
                JSONString += ',';
            }
        }
        
        /*if (results.size() == 0) {
            JSONString +='{';
            
            JSONString +='"label": "No data",';
            JSONString +=' "value": "1",';
            JSONString +=' "color": "#DDDDDD"';
            
            JSONString +='}';
        }*/

        JSONString += ']';
        return JSONString;
    }
    
    @RemoteAction
    public static String getOrphanCasesByType (String selectedSMC) {
    
        String JSONString = '';
        List<Id> ownerIds = getOwnerIds(selectedSMC);
        AggregateResult[] results = [SELECT RecordType.Name theName, COUNT(Id) theCount FROM Case WHERE RecordType.Name IN ('Order Management Case', 'Billing Inquiry', 'Complaint') AND Owner.type LIKE 'Queue' AND TGS_Assignee__c = '' AND OwnerId IN :ownerIds GROUP BY RecordType.Name ];
        System.debug('### Aggregate results: ' + results.size());
    
        JSONString += '[';
        List<String> colors = new List<String>();

        for (integer i = 0 ; i < results.size(); i++)  {
            AggregateResult ar = results.get(i);
            String label = (string)ar.get('theName');
            Integer value = (Integer)ar.get('theCount');
            JSONString +='{';
            
            JSONString +='"label": "' + label + '",';
            JSONString +=' "value": "' + value + '",';
            JSONString +=' "color": "' + colorMap.get(label) + '"';
            
            JSONString +='}';
            if (i != results.size()-1) {
                JSONString += ',';
            }
        }
        
        /*if (results.size() == 0) {
            JSONString +='{';
            
            JSONString +='"label": "No data",';
            JSONString +=' "value": "1",';
            JSONString +=' "color": "#DDDDDD"';
            
            JSONString +='}';
        }*/
        
        JSONString += ']';
        return JSONString;
    }
        
    public static List<Id> getOwnerIds(String selectedSMC) {
        List<Id> ownerIds = new List<Id>();
        
        Map<Id, Group> mapQueues;
        
        if (selectedSMC != null && selectedSMC != '' && selectedSMC != 'Others') {
            String smc = selectedSMC + '%';
            mapQueues = new Map<Id, Group>([SELECT Name FROM Group WHERE Type = 'Queue'  AND DeveloperName LIKE :smc]);
        } else if (selectedSMC == 'Others') {
            mapQueues = new Map<Id, Group>([SELECT Name FROM Group WHERE Type = 'Queue'  AND DeveloperName LIKE 'TGS_SMC L%']);    // Others es un valor temporal para pruebas
        }
        
        if (mapQueues != null && mapQueues.size() > 0) {
            ownerIds.addAll(mapQueues.keySet());
            Map<Id, User> mapUsers = new Map<Id, User>([SELECT Id FROM User WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN :ownerIds)]);
            ownerIds.addAll(mapUsers.keySet());
        }
        
        return ownerIds;
    }
}