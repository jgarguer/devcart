@isTest
private class PCA_HomeController_TEST {


    static testMethod void getloadInfo() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	BI_TestUtils.throw_exception = false;
    	User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
      	system.runAs(usr){
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
              item.OwnerId = usr.Id;
              accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(2, accList);
           User user1 = new User (alias = 'stttt',
                                email= '0@testorg.com',
                                emailencodingkey='UTF-8',
                                lastname='Testing',
                                languagelocalekey='en_US',
                                localesidkey='en_US',
                                isActive = true,
                                CommunityNickname = '0testUser123',
                                ProfileId = BI_DataLoad.searchPortalProfile(),
                                BI_Permisos__c = Label.BI_PortalPlatino,
                                BI_Usuario_de_Portal_Platino__c = true,
                                ContactId = con[0].Id,
                                timezonesidkey=Label.BI_TimeZoneLA,
                                username= '00@testorg.com',
                                Country_Region__c=lst_pais[0]);                
		insert user1;
		
			
         	Test.setCurrentPage(Page.PCA_Login);
            
         	
			system.runAs(user1){
	        	BI_TestUtils.throw_exception = false;
               // BI_Contact_Customer_Portal__c usuCustport = PCA_HomeController.usuarioCustomerPortal;
                //User usuport = PCA_HomeController.usuarioPortal();
                //List <Account> lst_acc_port = PCA_HomeController.AccountsUser;
                
	        	PCA_HomeController home2 = new PCA_HomeController();
        	
        		home2.getThis();
                user userTest = home2.usuarioPortal;
                BI_Contact_Customer_Portal__c CCPTest = home2.usuarioCustomerPortal;
                List<Account> accTest = home2.AccountsUser;
        		/*home1.usuarioPortal;
        		home1.usuarioCustomerPortal;
        		home1.AccountsUser;*/
        		home2.enviarALoginComm();
                home2.getCurrentPage();
        	}

        	system.runAs(user1){
	        	BI_TestUtils.throw_exception = true;
	        	PCA_HomeController home3 = new PCA_HomeController();                
        	
        		home3.getThis();
        		user userTest = home3.usuarioPortal;
                BI_Contact_Customer_Portal__c CCPTest = home3.usuarioCustomerPortal;
                List<Account> accTest = home3.AccountsUser;
        		home3.enviarALoginComm();                
                home3.getCurrentPage();
        	}
      	}
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}