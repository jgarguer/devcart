public class ADQ_OpcionesFacturacionController {

	public List<List<Account>> matrizCuentas;
	public List<List<Account>> matrizCuentasBack;
	public List<Account> lc  = new List<Account>();
	public List<Account> listaCuentas = new List<Account>();
	public List<Account> listaCuentasNext = new List<Account>();
	
	public Map<String, String> mapCuentas = new Map<String, String>();
	public String cuentaSeleccionada {get; set;}
	
	public String clientIP = ApexPages.currentPage().getHeaders().get('x-original-remote-addr');
	
	public Integer next = 25, count = 25, totalCuentas = 0, indiceMatriz = 0;
    public boolean shownext, showprev, listaGenerada;
    public String labelTabla = '';
	 
	public void refresh(){
		next = count;
		totalCuentas = (cuentaSeleccionada != null)?totalCuentas:0;
		indiceMatriz = 0;
		shownext = false;
		showprev = false;
		generarListas();
	}

	public void generarListas(){
		try{
			labelTabla = '';
			listaCuentas = new List<Account>();
			listaCuentasNext = new List<Account>();
			matrizCuentas = new List<List<Account>>();
			
			//Guardamos una lista de las Cuentas que tienen sitios por facturar
			Map<String, Account> mapCuentas = new Map<String, Account>();
			
			//listaOrderEntry = [SELECT Id, Oportunidad__c, Cuenta__c, Cuenta__r.Name, Estatus__c FROM Order_Entry__c WHERE Estatus__c = 'RFB'];

			
			if(cuentaSeleccionada == null){
				Set<String> setOpp = new Set<String>();

				//JG Migracion UATFIX - Cambiadas referencias a BI_Tipo_de_oferta_Per__r y Region__c - Cambio de las picklist
				//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
				for ( NE__OrderItem__c  les : [SELECT Id ,NE__Qty__c, NE__OneTimeFeeOv__c, NE__Account__r.Id, NE__ProdId__r.Id, NE__ProdId__r.Name, NE__OrderId__r.Id, NE__OrderId__r.NE__OptyId__c,
													NE__OrderId__r.NE__OptyId__r.BI_Full_contract_value_neto_FCV__c, NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c,
													NE__RecurringChargeOv__c,Name, NE__Account__r.BI_No_Identificador_fiscal__c,Installation_point__c, Factura__c,
													NE__OrderId__r.NE__OptyId__r.BI_Opportunity_Type__c,NE__OrderId__r.NE__OptyId__r.BI_Country__c
												FROM NE__OrderItem__c 
												Where 
														NE__OrderId__r.NE__OptyId__r.BI_Opportunity_Type__c  IN ('Fijo','Digital')
														AND NE__OrderId__r.NE__OptyId__r.BI_Country__c =: Label.BI_Mexico 
														AND NE__OrderId__r.NE__OptyId__r.StageName = 'F1 - Closed Won' 
														And Factura__c = ''

											 ]){
					setOpp.add (les.NE__OrderId__r.NE__OptyId__c);
				}
				System.debug('>>> setOpp: ' + setOpp);


				for (Opportunity opp : [SELECT Id, Name,AccountId, Account.Name, Account.Fecha_ultima_facturacion__c FROM Opportunity Where  StageName = 'F1 - Closed Won' AND Id IN : setOpp
											Order By Account.Name asc
											]){

					Account acc = new Account(name = opp.Account.Name, Id = opp.AccountId, Fecha_ultima_facturacion__c = opp.Account.Fecha_ultima_facturacion__c);
						mapCuentas.put(opp.AccountId, acc);
						if(mapCuentas.size() == 200){
							matrizCuentas.add(mapCuentas.values());
							totalCuentas += mapCuentas.size();
							mapCuentas.clear();
						}

				}

				
				/*
				for(NE__Order__c lsNEOrder : [SELECT Id, NE__OptyId__c,Etapa__c, NE__AccountId__c, NE__AccountId__r.Name, NE__AccountId__r.Fecha_ultima_facturacion__c 
													FROM NE__Order__c 
													WHERE NE__OptyId__c IN: setOpp 
														AND Etapa__c = 'F1 - Ganada'
												Order By NE__AccountId__r.Name desc
												
													 ]){
						Account acc = new Account(name = lsNEOrder.NE__AccountId__r.Name, Id = lsNEOrder.NE__AccountId__c, Fecha_ultima_facturacion__c = lsNEOrder.NE__AccountId__r.Fecha_ultima_facturacion__c);
						mapCuentas.put(lsNEOrder.NE__AccountId__c, acc);
						if(mapCuentas.size() == 200){
							matrizCuentas.add(mapCuentas.values());
							totalCuentas += mapCuentas.size();
							mapCuentas.clear();
						}
				}
				*/
			}
			else{
				if(cuentaSeleccionada == 'Todas'){
					matrizCuentas = matrizCuentasBack.clone();
				}
				else{
					lc = new List<Account>();
					for(List<Account> l : matrizCuentasBack){
						for(Account a : l){
							if(a.Id == cuentaSeleccionada){
								lc.add(a);
								if(lc.size() == 200){
									matrizCuentas.add(lc);
									lc = new List<Account>();
								}
							}
						}
					}
					if(lc.size() > 0){
						matrizCuentas.add(lc);
					}
				}
			}
			
			
			if(mapCuentas.size() > 0){
				matrizCuentas.add(mapCuentas.values());
				totalCuentas += mapCuentas.size();
			}
			matrizCuentasBack = matrizCuentas.clone();
			
			if(matrizCuentas.size() > 0)listaCuentas = matrizCuentas[0];
			if(matrizCuentas.size() > 1 || listaCuentas.size() > count){
                for(Integer i=0; i<count; i++){
                	listaCuentasNext.add(listaCuentas[i]);
                	//labelTabla += i + ',';
                }
                shownext = true;
                labelTabla = 'Cuentas 1 a ' + count;
            }
            else{
            	listaCuentasNext = listaCuentas;
            	labelTabla = 'Cuentas 1 a ' + listaCuentas.size();// + ' - size' + ' - matrizSitiosSize: ' + matrizCuentas.size();
            }//
            if(listaCuentasNext.size() > 0)listaGenerada = true;
            //labelTabla += (cuentaSeleccionada != null && cuentaSeleccionada != 'Todas')?' de ' + totalSitiosOppSeleccionada + ' - ' + mapCuentas.get(cuentaSeleccionada):'';
		}
		catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			System.debug('Exception: '+e);
		}
	}
	
	public Integer getTotalCuentas(){
        return totalCuentas;
    }
	
	public List<Account> getListaCuentasNext(){
		if(listaGenerada != true)
			generarListas();
		return listaCuentasNext;
	}
	
	public List<SelectOption> getListaCuentas(){
		List<SelectOption> accs = new List<SelectOption>();
		
		accs.add(new SelectOption('Todas', 'Todas'));
		for(List<Account> la : matrizCuentas){
			for(Account a : la){
		//		if(accs.size() < 1000)
					//accs.add(new SelectOption(a.Id, a.Name));
			}
		}
		
 	 	return accs;
	}
	
	public void Next(){
        try{
            showprev = true;
            listaCuentasNext.clear();
            Integer limit1 = 0;
    		
    		if(next+count > listaCuentas.size() && listaCuentas.size() == 200){
				next = next - listaCuentas.size();
    			listaCuentas = matrizCuentas[indiceMatriz += 1];
    		}
    		
            if(next+count < listaCuentas.size())
                limit1 = next+count;
            else{
                limit1 = listaCuentas.size();
                shownext = (indiceMatriz+1 < matrizCuentas.size())?true:false;
            }
                
            for(Integer i=next; i<limit1; i++)
            	listaCuentasNext.add(listaCuentas[i]);
    		
    		Integer x = (matrizCuentas[0].size() * indiceMatriz);
            labelTabla = 'Cuentas ' + (x + next+1) + ' a ' + (x + limit1);
            //labelTabla += (cuentaSeleccionada != null && cuentaSeleccionada != 'Todas')?' de ' + totalSitiosOppSeleccionada + ' - ' + mapCuentas.get(cuentaSeleccionada):'';
            next+=count;
        }
        catch(Exception e){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			System.debug('Exception: '+e);
        }
    }
    
    public void Prev(){
        try{
            shownext = true;
            listaCuentasNext.clear();
            Integer limit1 = 0;   
            
            if(next-(count+count) < 0 && indiceMatriz>0){
    			listaCuentas = matrizCuentas[indiceMatriz -= 1];
    			next = next + listaCuentas.size();
    		}
                 
            if(next-(count+count) > 0)
                limit1 = next-count;
            else{
                limit1 = next-count; 
                showprev = (indiceMatriz-1 >= 0)?true:false;
            }
    
            for(Integer i=next-(count+count); i<limit1; i++)    
            	listaCuentasNext.add(listaCuentas[i]);
            
            Integer x = (matrizCuentas[0].size() * indiceMatriz);
            labelTabla = 'Cuentas ' + ((next-(count+count))+1+x) + ' a ' + (x + limit1);
            //labelTabla += (cuentaSeleccionada != null && cuentaSeleccionada != 'Todas')?' de ' + totalSitiosOppSeleccionada + ' - ' + mapCuentas.get(cuentaSeleccionada):'';
            Next-=count;
        }
        catch(Exception e){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			System.debug('Exception: '+e);
        }               
    }
    
    public Boolean getShownext(){return shownext;}
    
    public Boolean getShowprev(){return showprev;}
    
    public Boolean getListaGenerada(){return listaGenerada;}
    
    public String getLabelTabla(){return labelTabla;}
    
    public String getClientIP(){return clientIP;}
	

}