public with sharing class PCA_ReclamosPedidosCtrl extends PCA_HomeController
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Controller of Reclamos y pedidos page

        History:

        <Date>            <Author>              <Description>
        24/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public list<Account> AccPais {get;set;}

    public String nameFS                        {get; set;}
    public String nameObject                    {get; set;}
    public String accountFieldName;
    public String LastMod                       {get;set;}

    public String nameFS1;
    public String nameObject1                   {get; set;}
    public String accountFieldName1;

    public String nameFS2;
    public String nameObject2                   {get; set;}
    public String accountFieldName2;

    public String nameFS3						{get; set;}
    public String nameObject3                   {get; set;}
    public String accountFieldName3;

    public String searchFinal;

    public Id AccountId                         {get; set;}

    public String searchText                    {get; set;}
    public String searchField                   {get; set;}
    public String firstHeader                   {get; set;}
    public String statusHeader					{get; set;}
    public String parentNumHeader				{get; set;}

    public Integer index                        {get; set;}
    public Integer pageSize                     {get; set;}
    public Integer totalRegs                    {get; set;}

    public Boolean OptionA                      {get; set;}


    public List<SelectOption> searchFields      {get; set;}
    public String FieldOrder {get;set;}
    public String Field;

    public List<FieldSetHelper.FieldSetRecord> viewRecords  {
        get{
            System.debug('viewRecords:'+viewRecords);
            if(viewRecords!=null){
                for(FieldSetHelper.FieldSetRecord record:viewRecords){
                    System.debug('record:'+record);
                }

            }
        	return viewRecords;
        }
        set;
        }
    public FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    public static Map<String, String> fixedStatuses_static = new Map<String, String>{
        // BIEN2 Statuses
        'Assigned'              => 'In Progress',           'Asignado'              => 'En curso',
        'In Progress'           => 'In Progress',           'En curso'              => 'En curso',
        'Pending'               => 'Pending',               'Pendiente'             => 'Pendiente',
        'Resolved'              => 'Resolved',              'Resuelto'              => 'Resuelto',
        'Cancelled'             => 'Cancelled',             'Cancelado'             => 'Cancelado',
        'Closed'                => 'Closed',                'Cerrado'               => 'Cerrado',
        // BIIN Statuses
        'Error de validación'   => 'En curso',
        //'Being Processed'       => 'En curso',       'En progreso'           => 'En progreso', Solicitud de cambio por parte de Carlos Fernandez
        'Being Processed'       => 'In Progress',       'En progreso'   => 'En curso',
        //'Confirmation pending'  => 'Confirmation pending',  'Pdte Confirmación'     => 'Pdte Confirmación', Solicitud de cambio por parte de Carlos Fernandez
        'Confirmation pending'  => 'In Progress',  'Pdte Confirmación'     => 'En curso',
        null => ''
    };
    public Map<String, String> fixedStatuses	   { get {return fixedStatuses_static;} } // This list is not exhaustive; add additional mappings per usage


    public boolean onlyOrders {get; set;}
    public String fieldImage {get;set;}
    public String Mode {get;set;}
    public String PageValue {get;set;}

    public set <Id> IdsAtt {get;set;}
    public String countryName   {get; set;}

    public String pageIncidencia {get; set;}
    public String pageCreacionIncidencia {get; set;}
    /* CONSTRUCTOR */
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Constructor

    History:

    <Date>            <Author>              <Description>
    24/06/2014        Ana Escrich       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_ReclamosPedidosCtrl()
    {}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user

    History:

    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info

    History:

    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    14/10/2015        Jose Miguel Fierro   Added BI2 fiters
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            System.debug(LoggingLevel.ERROR, 'gfdksa');
            countryName = [SELECT Id, Pais__c FROM User WHERE Id = :UserInfo.getUserId()].Pais__c;

            this.index = 1;
            this.pageSize = 10;
            this.searchFinal = null;
            this.AccountId = BI_AccountHelper.getCurrentAccountId();
            system.debug('AccountId: '+AccountId);
            if(AccountId == null){
                return;
            }
            system.debug(this.AccountId);
            system.debug(AccountId);

            string countryAccount = BI_AccountHelper.getCountryAccount(AccountId);
            if(countryAccount != null){
            List<BI_Actualizaciones_integracion__c> regions = [select Id, BI_Entidad__c, BI_Country__c, BI_UltimaModificacion__c from BI_Actualizaciones_integracion__c where BI_Country__c= : countryAccount AND BI_Entidad__c=:Label.BI_OrdenesDeCompra];

            if(!regions.isEmpty()){
                LastMod = regions[0].BI_UltimaModificacion__c;
                system.debug('regions' +regions);
            }

            }
            this.OptionA = true;


            this.nameObject1 = 'Case';
            this.nameFS1 = 'BIIN_MainTableCase';
            this.accountFieldName1 = 'AccountId';

            this.nameObject2 = 'NE__Order__c';
            this.nameFS2 = 'PCA_MainTablePedidos';
            this.accountFieldName2 = 'NE__AccountId__c';


            this.nameObject3 = 'Case';
            this.nameFS3 = 'BI2_MainTableCase';
            this.accountFieldName3 = 'AccountId';

            this.nameFS = this.nameFS3;
            this.nameObject = this.nameObject3;
            this.accountFieldName = this.accountFieldName3;

            if(this.nameFS == this.nameFS3) {
                this.FieldOrder = 'BI_COL_Fecha_Radicacion__c';
            }
            pageDetail(countryName);
            defineSearch();
            defineRecords();
            defineViews();
            onlyOrders = false;
            string typeUrl = (Apexpages.currentPage().getParameters().get('type')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('type')):null;
            system.debug('typeUrl: ' + typeUrl);
            if (typeUrl != null && typeUrl == 'o'){
                onlyOrders = true;
                changeTab();
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method to change Tab

    History:

    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void changeTab()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }


            this.searchText = null;
            this.searchFinal = null;
            this.OptionA = this.OptionA ? false : true;
            if(countryName==Label.BI_Peru) {
                this.nameFS = (this.nameFS == this.nameFS3) ? this.nameFS2 : this.nameFS3;
                this.nameObject = (this.nameObject == this.nameObject3) ? this.nameObject2 : this.nameObject3;
                this.accountFieldName = (this.accountFieldName == this.accountFieldName3) ? this.accountFieldName2 : this.accountFieldName3;
            } else {
                this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
                this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
                this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
            }

            defineSearch();
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.changeTab', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Moruno
     Company:       Salesforce.com
     Description:   Defines search records

     History:

     <Date>            <Author>              <Description>
     05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineRecords()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            String accountValue = this.accountFieldName + ';' + this.AccountId;

            if(this.nameObject == this.nameObject2){
                this.searchFinal = this.searchFinal + '::RecordType.Name;order';
            } else {
                Mode = 'DESC';
                this.fieldImage = this.FieldOrder;
                if(this.searchFinal == null) {
                    this.searchFinal = this.FieldOrder;
                } else {
                    this.searchFinal = this.searchFinal + '::' + this.FieldOrder;
                }
            }
            //if(this.nameObject == this.nameObject2){
            //  this.searchFinal = 'RecordType.Name;order';
            //}

            this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.searchFinal, accountValue, Mode);

            if(this.searchText != null && Label.BI2_Estado_EnCurso.toLowerCase().contains(this.searchText.trim().toLowerCase())) {
                FieldSetHelper.FieldSetContainer fsc = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, 'Status;' + Label.BI2_In_Progress_Others + '::' + this.FieldOrder, accountValue, Mode);
                for(FieldSetHelper.FieldSetRecord fsr : fsc.regs) {
                    this.fieldSetRecords.regs.add(fsr);
                }
            }

            fillFixedStatus(this.fieldSetRecords);


            if (this.fieldSetRecords.regs.size() > 0)
            {
                if(this.nameObject == this.nameObject3) {
                    this.firstHeader = this.fieldSetRecords.regs[0].values[1].field;
                } else {
                    this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
                }

                // Use this loop to iterate over headers
                for(Integer iter = 0; iter < this.fieldSetRecords.regs[0].values.size(); iter++) {
                    String fld = this.fieldSetRecords.regs[0].values[iter].field;
                    if(fld == 'Status' || fld == 'Estado'){
                        statusHeader = fld;
                    }
                }
            }


            this.index = 1;
            this.totalRegs = this.fieldSetRecords.regs.size();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines views records

    History:

    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineViews()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
            Integer topValue = ((this.index + this.pageSize) > this.totalRegs) ? this.totalRegs : (this.index + this.pageSize - 1);

            for (Integer i = (this.index - 1); i < topValue; i++)
            {
                FieldSetHelper.FieldSetRecord iRecord = this.fieldSetRecords.regs[i];
                this.viewRecords.add(iRecord);
            }
            List<Id> IdOfCases = new List<Id>();
            for(FieldSetHelper.FieldSetRecord attchCase :this.viewRecords){
                IdOfCases.add(attchCase.Id);
            }
            system.debug('IdOfCases**:' +IdOfCases);
            List <Attachment> Attchmnt = [Select Id, ParentId from Attachment where ParentId =:IdOfCases];
            IdsAtt = new Set<Id>();
            for(Attachment Att :Attchmnt){
                system.debug('Attchmnt**:' +Att.ParentId);
                IdsAtt.add(Att.ParentId);
                system.debug('IdsAtt**:' +IdsAtt);
            }
            system.debug('*****IdsAtt: '+IdsAtt);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.defineViews', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Moruno
     Company:       Salesforce.com
     Description:   Defines Search records

     History:

     <Date>            <Author>              <Description>
     05/07/2014        Antonio Moruno       Initial version
     20/11/2015        Jose Miguel Fierro   Remove BI2_PER_Caso_Padre__c from the picklist
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineSearch()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            this.searchFields = FieldSetHelper.defineSearchFields(this.nameFS, this.nameObject);
            for(Integer iter = 0; iter < searchFields.size(); iter++) {
                SelectOption opt = searchFields.get(iter);
                if(opt.getValue() == 'BI2_PER_Caso_Padre__c') {
                    searchFields.remove(iter);
                    iter--;
                }
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.defineSearch', 'Portal Platino', Exc, 'Class');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines records in page

    History:

    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<SelectOption> getItemPage() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        options.add(new SelectOption('200','200'));
        system.debug('itemPage: '+ options);
        return options;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Search records

    History:

    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void searchRecords()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            this.searchFinal = this.searchField + ';' + this.searchText;
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.searchRecords', 'Portal Platino', Exc, 'Class');
        }
    }

    /*public Id getAccountId()
    {
        list<User> userList = [SELECT AccountId, ContactId FROM User WHERE Id = :usuarioPortal.id];

        if (!userList.isEmpty())
        {
            return userList[0].AccountId;
        }

        return '';
    }*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Previous record

    History:

    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasPrevious()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            return (this.index != 1);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.getHasPrevious', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Next record

    History:

    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasNext()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            return !((this.index + this.pageSize) > this.totalRegs);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.getHasNext', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Next page

    History:

    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Next()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            this.index += this.pageSize;
            system.debug('pagina siguiente: '+this.index);
            fillFixedStatus(this.fieldSetRecords);
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Next', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Previous page

    History:

    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Previous()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            this.pageSize = 10;
            this.index -= this.pageSize;
            fillFixedStatus(this.fieldSetRecords);
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Previous', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show more values in a page

    History:

    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void valuesInPage(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.pageSize = Integer.valueOf(PageValue);
            system.debug('pageSize*: '+this.pageSize);
            fillFixedStatus(this.fieldSetRecords);
            defineViews();

        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Next', 'Portal Platino', Exc, 'Class');
        }

    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Moruno
     Company:       Salesforce.com
     Description:   Order table with field

     History:

     <Date>            <Author>              <Description>
     17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Order() {
        try{
            /*
             this.searchText = null;
             this.searchFinal = null;
             this.OptionA = this.OptionA ? false : true;
             this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
             this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
             this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
            */
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            Mode = '';
            defineSearch();
            fieldImage = FieldOrder;
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            //if(this.nameObject == this.nameObject2){
            //}
            if(this.nameObject == this.nameObject2){
                this.searchFinal = 'RecordType.Name;order;'+FieldOrder;
                if(Field == FieldOrder){
                    this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.searchFinal, accountValue,  'DESC');
                    FieldOrder = '';
                    Mode = 'DESC';
                }else{
                    this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.searchFinal, accountValue,  'ASC');
                    Mode = 'ASC';
                }
            }
            if(this.nameObject != this.nameObject2){
                if(Field == FieldOrder){
                    this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'DESC');
                    FieldOrder = '';
                    Mode = 'DESC';
                }else{
                    this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'ASC');
                    Mode = 'ASC';
                }
            }
            if (this.fieldSetRecords.regs.size() > 0)
            {
                if(this.nameObject == this.nameObject3) {
                    this.firstHeader = this.fieldSetRecords.regs[0].values[1].field;
                } else {
                    this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
                }
            }

            this.index = 1;
            this.totalRegs = this.fieldSetRecords.regs.size();
            fillFixedStatus(this.fieldSetRecords);

            Field = this.FieldOrder;
            defineViews();

        }catch (exception Exc){
            System.debug(Exc.getStackTraceString());
            BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', Exc, 'Class');
        }
    }


    /*
    public void createNewCase()
    {
        Case newCase = new Case();
        newCase.AccountId = AccountId;
    }
    */

    public static void fillFixedStatus(FieldSetHelper.FieldSetContainer fieldSetRecords) {
        // Fill in all other statuses not covered by fixedStatuses_static
        if(fieldSetRecords.apiheaders.keySet().contains('Status')) {
            for(FieldSetHelper.FieldSetRecord rec : fieldSetRecords.regs) {
                for(FieldSetHelper.FieldSetResult res : rec.values) {
                    if(res.Field == 'Status' && !fixedStatuses_static.keyset().contains(res.Value)) {
                        fixedStatuses_static.put(res.Value, res.Value);
                    }
                }
            }
        }
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Rubén Suárez
    Company:        Avanxo
    Description:    Método para seleccionar la página de consulta de incidencias técnicas

    In:             País del usuario
    Out:            Página de consulta de incidencias

    <Date>          <Author>            <Description>
    28/02/2017      Rubén Suárez        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public void pageDetail(String strCountry){
        if(String.isNotEmpty(strCountry)) {
            List<BI2_Paginas_casos_tecnicos__mdt> mtdPaginasCasosTecnicos = [SELECT BI2_Pagina_consulta_portal_platino__c
                                                                             ,BI2_Pagina_creacion_portal_platino__c
                                                                         FROM BI2_Paginas_casos_tecnicos__mdt
                                                                        WHERE DeveloperName =: strCountry];
            //if(mtdPaginasCasosTecnicos != null) {
            if(mtdPaginasCasosTecnicos.size() > 0) {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Redirecciona a página configurada: \'' + mtdPaginasCasosTecnicos[0].BI2_Pagina_consulta_portal_platino__c + '\'   <<<<<<<<<<\n-=#=-\n');
                pageIncidencia          = '/empresasplatino/' + mtdPaginasCasosTecnicos[0].BI2_Pagina_consulta_portal_platino__c;
                pageCreacionIncidencia  = '/empresasplatino/' + mtdPaginasCasosTecnicos[0].BI2_Pagina_creacion_portal_platino__c;
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'PAÍS NO CONFIGURADO - Redirecciona a página por defecto \'PCA_Reclamo_PopUpDetail\'' + '   <<<<<<<<<<\n-=#=-\n');
                pageIncidencia          = '/empresasplatino/PCA_Reclamo_PopUpDetail';
                pageCreacionIncidencia  = '/empresasplatino/PCA_Reclamo_PopUpNew';
            }
        } else {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'NO HAY PAÍS - Redirecciona a página por defecto \'PCA_Reclamo_PopUpDetail\'' + '   <<<<<<<<<<\n-=#=-\n');
            pageIncidencia          = '/empresasplatino/PCA_Reclamo_PopUpDetail';
            pageCreacionIncidencia  = '/empresasplatino/PCA_Reclamo_PopUpNew';
        }
    }
}