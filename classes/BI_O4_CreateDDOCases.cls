/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Oscar Bartolo
     Company:       New Energy Aborda
     Description:   Queueable class to create ddo cases
     Test Class:    
     
     History:
      
     <Date>              <Author>                   <Change Description>
     20/09/2017          Oscar Bartolo              Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class BI_O4_CreateDDOCases implements Queueable{
    private List<Case> lstCase = new List<Case>();
	private String str_ddo = '';
    
    public BI_O4_CreateDDOCases(List<Case> lstCase, String str_ddo){
        this.lstCase = lstCase;
        this.str_ddo = str_ddo;
    }

    public void execute(QueueableContext context) {
        createCases();
    }

    public void createCases() {
        try{
            Id idCaso;
            Map<Id, String> mapCase = new Map<Id, String>();
            Set<Id> setCaseId = new Set<Id>();
            
            if(!this.lstCase.isEmpty()){
                for(Case item : this.lstCase){
                    if(this.str_ddo != null){
                        idCaso = item.Id;
                    }
                }
            }

            Set<String> setDDO = new Set<String>();
            List<String> lstDDO = this.str_ddo.split(';');
            setDDO.addAll(lstDDO);
            
            List<Group> lstQueue = [Select Id, Name From Group Where Name IN: setDDO And DeveloperName Like '%BI_O4%'];
            List<Case> newCases = new List<Case>(); 
            
            Map<String, BI_O4_DDO_Nemonico__c> mcs = BI_O4_DDO_Nemonico__c.getAll();
            Map<String, String> map_Nombre_Nemonico = new Map<String, String>();
            for (String name : mcs.keySet()) {
                map_Nombre_Nemonico.put(mcs.get(name).BI_O4_DDO_Name__c, mcs.get(name).BI_O4_DDO_Quotation__c);
            }
            
            if(!setDDO.isEmpty()){
                for(String s : setDDO){
                    for(Group item : lstQueue){
                        if(item.Name == s){
                            System.debug('Cola: ' + item.Id);
                            Case caso = new Case();
                            caso.OwnerId = item.Id;
                            caso.AccountId = this.lstCase[0].AccountId;
                            caso.ParentId = idCaso;
                            caso.RecordTypeId = this.lstCase[0].RecordTypeId;
                            caso.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                            caso.BI_O4_DDO__c = s;
                            caso.BI_O4_URL_Request__c = this.lstCase[0].BI_O4_URL_Request__c;
                            caso.BI_O4_URL_WANQtool__c = this.lstCase[0].BI_O4_URL_WANQtool__c;
                            caso.BI_O4_Oportunidad_Local__c = this.lstCase[0].BI_O4_Oportunidad_Local__c;
                            caso.BI_O4_Cliente_no_existente_nombre__c = this.lstCase[0].BI_O4_Cliente_no_existente_nombre__c;
                            caso.BI_O4_URL_Cotizadores__c = this.lstCase[0].BI_O4_URL_Cotizadores__c;
                            caso.Subject = 'A-' + map_Nombre_Nemonico.get(s) + '-';
                            caso.Status = 'Pendiente Cotización';
                            caso.Priority = this.lstCase[0].Priority;
                            caso.Description = this.lstCase[0].Description;
                            caso.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                            caso.BI_O4_Es_proyecto_especial__c = this.lstCase[0].BI_O4_Es_proyecto_especial__c;
                            caso.BI_O4_Proyecto_Especial__c = this.lstCase[0].BI_O4_Proyecto_Especial__c;
                            caso.BI_O4_Tipo_gen_rico_Preventa__c = this.lstCase[0].BI_O4_Tipo_gen_rico_Preventa__c;
                            caso.BI_O4_Tipo_de_Facturaci_n__c = this.lstCase[0].BI_O4_Tipo_de_Facturaci_n__c;
                            caso.BI_O4_Tipo_de_movimiento__c = this.lstCase[0].BI_O4_Tipo_de_movimiento__c;
                            caso.BI_O4_Periodo_de_contrataci_n_Meses__c = this.lstCase[0].BI_O4_Periodo_de_contrataci_n_Meses__c;
                            caso.BI_O4_Fecha_presentaci_n_oferta__c = this.lstCase[0].BI_O4_Fecha_presentaci_n_oferta__c;
                            caso.BI_O4_Identificativo_Origen__c = this.lstCase[0].BI_O4_Identificativo_Origen__c;
                            caso.BI_Country__c = this.lstCase[0].BI_Country__c;
                            caso.BI_Department__c = this.lstCase[0].BI_Department__c;
                            System.debug('ID Cola: ' + item.Id);
                            newCases.add(caso);
                        }
                    }
                }
                BI_GlobalVariables.stopTriggereHelpCase=false;
                insert newCases;
            }
            
            for (Case c: newCases)
                setCaseId.add(c.Id);
                
            updateCaseSubject(setCaseId);
            if(!String.valueOf(this.lstCase[0].OwnerId).StartsWith('00G')) {
                assignCaseTeam(setCaseId, this.lstCase[0].OwnerId);
            }
        }catch(Exception e){
            BI_LogHelper.generate_BILog('BI_O4_CreateDDOCases.createCases', 'BI_EN', e, 'Clase');
            Throw e;
        }
    }

    //@future (callout=false)
    private static void updateCaseSubject(Set<Id> setId) {   
        Map <Integer, BI_bypass__c> mapa;

        try{
            List<Case> listUpdateCase = new List<Case>();
            for (Case c: [SELECT Subject, CaseNumber
                          FROM Case
                          WHERE Id IN :setId
                          FOR UPDATE])
            {
                c.Subject += + c.CaseNumber;
                listUpdateCase.add(c); 
            }
            if (!listUpdateCase.isEmpty()){
                mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true, false, false, false);
                BI_MigrationHelper.skipAllTriggers();

                update listUpdateCase;
            }
        }
        catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_O4_CreateDDOCases.updateCaseSubject', 'BI_EN', exc, 'Clase');
        }
        finally{

            if(mapa != null){
                BI_MigrationHelper.disableBypass(mapa);
            }
            BI_MigrationHelper.cleanSkippedTriggers();
        }
    }
    
    @future (callout=false)
    private static void assignCaseTeam(Set<Id> setId, Id ownerId) {
        CaseTeamRole role = [SELECT Id FROM CaseTeamRole WHERE Name = 'Propietario Caso Padre' LIMIT 1];
        List<CaseTeamMember> listCTM = new List<CaseTeamMember>();
        
        for (Id caseId: setId)
        {
            CaseTeamMember ctm = new CaseTeamMember(ParentId = caseId,
                                                    MemberId = ownerId,
                                                    TeamRoleId = role.Id);
            listCTM.add(ctm);
        }
        
        if (!listCTM.isEmpty())
            insert listCTM;
    }
}