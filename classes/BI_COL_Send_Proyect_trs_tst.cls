/************************************************************************************
* Avanxo Colombia
* @author           <@avanxo.com>
* Proyect:          Telefonica
* Description:
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción
*           -----   ----------      ---------------------------     ---------------
* @version   1.0    2015-08-12      Daniel ALexander Lopez (DL)     Created Class
                    20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_Send_Proyect_trs_tst {

    public static list<Opportunity> lstOppAsociadas                         = new list<Opportunity>();
    public static List<BI_COL_Send_Proyect_trs_ctr.WrapOportunidad> lstWrapper = new List<BI_COL_Send_Proyect_trs_ctr.WrapOportunidad>();
    public static Account           objCuenta;
    public static Opportunity       objOppty;
    public static list<BI_COL_manage_cons__c>  lstConfigPerson;
    public static list <UserRole> lstRoles;
    public static User objUsuario;
    public static list<User> lstUsuarios;


    static void crearData() {
        objCuenta                                       = new Account();
        objCuenta.Name                                  = 'prueba';
        objCuenta.BI_Country__c                         = 'Colombia';
        objCuenta.TGS_Region__c                         = 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
        objCuenta.CurrencyIsoCode                       = 'GTQ';
        objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
        insert objCuenta;

        System.debug('\n\n\n ======= CUENTA ======\n ' + objCuenta );

        BI_COL_manage_cons__c ObjConfigPerson_CP = new BI_COL_manage_cons__c();

        ObjConfigPerson_CP.Name                          = 'ConsecutivoProyecto';
        ObjConfigPerson_CP.BI_COL_Numero_Viabilidad__c   = 46;
        ObjConfigPerson_CP.BI_COL_ConsProyecto__c        = 1;
        ObjConfigPerson_CP.BI_COL_ValorConsecutivo__c    = 1;
        lstConfigPerson                                  = new list<BI_COL_manage_cons__c >();

        lstConfigPerson.add(ObjConfigPerson_CP);

        insert lstConfigPerson;

        for (Integer i = 0; i < 10; i++) {
            objOppty                                        = new Opportunity();
            objOppty.Name                                   = 'TEST AVANXO OPPTY' + i;
            objOppty.AccountId                              = objCuenta.Id;
            objOppty.BI_Country__c                          = 'Colombia';
            objOppty.CloseDate                              = System.today().addDays(i);
            objOppty.StageName                              = 'F6 - Prospecting';
            objOppty.BI_Ciclo_ventas__c                     = Label.BI_Completo;
            objOppty.BI_COL_complejidad_implantacion__c     = 'ALTA';
            //objOppty.BI_Numero_id_oportunidad__c          = 'test'+i;
            lstOppAsociadas.add(objOppty);
        }

        System.debug('\n\n\n ======= lstOppyAsociadas ======\n ' + lstOppAsociadas );
        System.debug('\n\n\n ======= BI_Numero_id_oportunidad__c ======\n ' + lstOppAsociadas[0].BI_Id_Interno_de_la_Oportunidad__c );

        insert lstOppAsociadas;


    }
    static testMethod void test_method_one() {
        // TO DO: implement unit test
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            crearData();

            PageReference pageRef = Page.BI_COL_Send_Proyect_trs_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
            ApexPages.StandardController  controler             = new ApexPages.StandardController(objCuenta);
            BI_COL_Send_Proyect_trs_ctr controller              = new BI_COL_Send_Proyect_trs_ctr(controler);
            BI_COL_Send_Proyect_trs_ctr.WrapOportunidad wrapper = new BI_COL_Send_Proyect_trs_ctr.WrapOportunidad();
            ws_TrsMasivo.crearProyectoResponse          wsresp  = new ws_TrsMasivo.crearProyectoResponse();
            wsresp.mensajeError = 'test';
            wsresp.estado       = 'OK';
            wsresp.id           = lstOppAsociadas[0].id;
            List<ws_TrsMasivo.crearProyectoResponse> lstResp    = new List<ws_TrsMasivo.crearProyectoResponse>();
            lstResp.add(wsresp);

            wrapper.oportunidad                      = lstOppAsociadas[0];
            wrapper.seleccionado                     = true;

            lstWrapper.add(wrapper);
            controller.lstWrap                       = lstWrapper;
            controller.lstImpresion                  = lstWrapper;
            controller.lstOportunidades              = lstOppAsociadas;
            controller.pageSize                      = 1;
            controller.pageNumber                    = 1;
            controller.cliente                       = objCuenta;
            controller.strOpptys                     = 'test1,test2';

            controller.consultaOportunidades();
            controller.action_seleccionarTodos();
            controller.actualizaOpt();
            controller.validarEnvioTRS();
            controller.getPreviousButtonEnabled();
            controller.getNextButtonDisabled();
            controller.getTotalPageNumber();
            controller.nextBtnClick();
            controller.previousBtnClick();
            BI_COL_Send_Proyect_trs_ctr.creaLogTransaccion('Test', 'Test', 'Test', lstOppAsociadas);
            BI_COL_Send_Proyect_trs_ctr.creaLogTransaccion(lstResp, lstOppAsociadas);
            System.debug('Lst proyectos =========' + controller.lstProyectos);

            Test.stopTest();
        }
    }

    static testMethod void test_method_two() {
        // TO DO: implement unit test
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            crearData();

            PageReference pageRef = Page.BI_COL_Send_Proyect_trs_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
            ApexPages.StandardController  controler             = new ApexPages.StandardController(objCuenta);
            BI_COL_Send_Proyect_trs_ctr controller              = new BI_COL_Send_Proyect_trs_ctr(controler);
            BI_COL_Send_Proyect_trs_ctr.WrapOportunidad wrapper = new BI_COL_Send_Proyect_trs_ctr.WrapOportunidad();
            ws_TrsMasivo.crearProyectoResponse          wsresp  = new ws_TrsMasivo.crearProyectoResponse();
            wsresp.mensajeError = 'test';
            wsresp.estado       = 'OK';
            wsresp.id           = lstOppAsociadas[0].id;
            List<ws_TrsMasivo.crearProyectoResponse> lstResp    = new List<ws_TrsMasivo.crearProyectoResponse>();
            lstResp.add(wsresp);

            wrapper.oportunidad                      = lstOppAsociadas[0];
            wrapper.seleccionado                     = true;

            lstWrapper.add(wrapper);

            controller.lstWrap                       = lstWrapper;
            System.debug('\n\n##### controller.lstWrap=>' + controller.lstWrap);
            controller.actualizaOpt();


            Test.stopTest();
        }
    }
}