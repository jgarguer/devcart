/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   BI_O4_NewQuoteController for create new quotes on Opportunity
    Test Class:    BI_O4_NewQuoteController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Fernando Arteaga         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_NewQuoteController
{
	public Opportunity opp {get; set;}
    
    public BI_O4_NewQuoteController(ApexPages.standardController controller)
    {
    	opp = (Opportunity) controller.getRecord();
    }
    
    public PageReference newQuote()
    {
    	String result;
    	String oppId = opp.Id;
    	String versionId;
    	List<NE__OrderItem__c> listNewOI = new List<NE__OrderItem__c>();
    	NE__Order__c newOrd;
    	NE__Quote__c quote;
    	Decimal version = 0;
    	Savepoint sp;
    	
    	try
    	{
    		sp = Database.setSavepoint();    		
	    	//List<NE__Order__c> listVersions = Database.query(BI_O4_Utils.getSOQLQueryAllFields(NE__Order__c.getSobjectType()) + ' WHERE NE__OptyId__c = :oppId AND NE__OrderStatus__c = \'Active\'');
	    	List <NE__Order__c> listVersions = [SELECT Id FROM NE__Order__c WHERE NE__OptyId__c = :oppId AND NE__OrderStatus__c = 'Active' AND RecordType.DeveloperName = 'Opty' ORDER BY NE__Version__c DESC LIMIT 1];
	    	if (!listVersions.isEmpty())
	    	{	
	    		System.debug('prostituta Id--->' + listVersions[0].Id);
	    		String idOrd = NEBit2winApi.cloneOptyconfiguration(listVersions[0].Id, null, 'Opty2');
    			/*List<RecordType> listRT = [SELECT Id FROM RecordType WHERE sObjectType = 'NE__Order__c' AND DeveloperName = 'Quote'];
				
				List<AggregateResult> listAR = [SELECT NE__OptyId__c, MAX(NE__Version__c) maxVersion
											    FROM NE__Order__c
											    WHERE NE__OptyId__c = :opp.Id
											    AND RecordType.DeveloperName = 'Quote'
											    GROUP BY NE__OptyId__c];
				if (!listAR.isEmpty())
					version = (Decimal) listAR[0].get('maxVersion') + 1;
				else
					version = 1;
				
	    		// 1. Clone the opty active version
				newOrd = listVersions[0].clone(false, true, false, true);
				newOrd.RecordTypeId = listRT[0].Id;
				newOrd.NE__OrderStatus__c = 'Draft';
				newOrd.NE__Version__c = version;
				newOrd.BI_O4_Fecha_Entrega_oferta_comprometida__c = opp.BI_O4_Fecha_Entrega_oferta_comprometida__c;
				
				insert newOrd;
				
				// 2. Clone the order items of the active version
				versionId = listVersions[0].Id;
				
				List<NE__OrderItem__c> listOI = Database.query(BI_O4_Utils.getSOQLQueryAllFields(NE__OrderItem__c.getSobjectType()) + ' WHERE NE__OrderId__c = :versionId');
				
				for (NE__OrderItem__c oi : listOI)
				{
					NE__OrderItem__c newOI = oi.clone(false, true, false, true);
					newOI.NE__OrderId__c = newOrd.Id;
					if (newOI.BI_Probabilidad_exito__c != null && newOI.BI_Probabilidad_exito__c > 100) {
						newOI.BI_Probabilidad_exito__c = newOI.BI_Probabilidad_exito__c / 100;
					}
					listNewOI.add(newOI);
				}
				
				insert listNewOI;
			
				// 3. Create the NE__Quote__c record
				quote = new NE__Quote__c(NE__AccountId__c = newOrd.NE__AccountId__c,
										 NE__Opportunity__c = opp.Id,
										 NE__Orders__c = newOrd.Id);
				
				insert quote;
				
				newOrd.NE__Quote__c = quote.Id;
				update newOrd;
				
				String retUrl = URL.getSalesforceBaseUrl().toExternalForm() + Page.NE__NewConfiguration.getUrl() + '?ordId=' + newOrd.Id;
	    		*/
	    		String retUrl;
	    		if(idOrd != 'KO'){
	    			retUrl = URL.getSalesforceBaseUrl().toExternalForm() + Page.NE__NewConfiguration.getUrl() + '?ordId=' + idOrd;
	    		}
	    		return new PageReference(retUrl);
	    	}
	    	else
	    	{
	    		String retUrl = System.URL.getSalesforceBaseUrl().toExternalForm() + Page.NE__NewConfiguration.getUrl();
				PageReference p = new PageReference(retUrl);
				p.getParameters().put('accId', this.opp.AccountId);
				p.getParameters().put('servAccId', this.opp.AccountId);
				p.getParameters().put('billAccId', this.opp.AccountId);
				p.getParameters().put('oppId', this.opp.Id);
				p.getParameters().put('cType', 'New');
				p.getParameters().put('cSType', this.opp.BI_Ciclo_ventas__c);
				p.getParameters().put('confCurrency', this.opp.CurrencyIsoCode);
				List<RecordType> rt = [Select Id from RecordType where RecordType.Name = 'Quote'];
				System.debug('id quote: '+rt[0].Id);
				p.getParameters().put('configurationRecordTypeId', rt[0].Id);
				System.debug('retUrl:' + p.getURL());
				
				return p;
	    	}
    	}
    	catch (Exception exc)
    	{
			System.debug(exc);
			Database.rollback(sp);
			BI_LogHelper.generate_BILog('BI_O4_NewQuoteController.newQuote', 'BI_EN', exc, 'Clase');
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, exc.getMessage()));
			return null;
    	}
    }
    
	public PageReference cancel()
	{
		return new PageReference('/' + this.opp.Id);
	}
}