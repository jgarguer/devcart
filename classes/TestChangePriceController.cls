//ET 04/07/2014
@isTest
private class TestChangePriceController {

    static testMethod void myUnitTest() {
      
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
      NE__Order__c ord = new NE__Order__c();
      insert ord;
            
      ApexPages.StandardController sc = new ApexPages.standardController(ord);
      ChangePriceController controller = new ChangePriceController(sc);
      
      
      NE__Product__c prod = new NE__Product__c();
      prod.Name = 'Prod';
      insert prod;
      
      NE__OrderItem__c oi = new NE__OrderItem__c();
      oi.NE__OrderId__c = ord.Id;
      oi.NE__ProdId__c = prod.Id;
      oi.NE__Qty__c= 2;
      insert oi;
      
      controller.save();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}