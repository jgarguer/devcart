@RestResource(urlMapping='/customeraccounts/v1/accounts/*/contracts')
global class BI_ContractsForAccountRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Account's related contracts Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Retrieves the list of contracts associated to the specific account
    
    IN:            Void
    OUT:           BI_RestWrapper.ContractsListInfoType structure
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.ContractsListInfoType getContracts() {
		
		BI_RestWrapper.ContractsListInfoType conListInfoType;
		
		try{
		
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				//MULTIPLE CONTACTS
				conListInfoType = BI_RestHelper.getContractsForAccount(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				RestContext.response.statuscode = (conListInfoType == null)?404:200;//404 NOT_FOUND, 200 OK
				
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContractsForAccountRest.getContracts', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return conListInfoType;
		
	}
	
}