public with sharing class BIIN_Helper {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	  Author:        Guillermo Muñoz
	  Company:       Aborda
	  Description:   Helper Class for BIIN.
	    
	  History: 
	    
	  <Date>            <Author>                    				<Change Description>
	  25/01/2016        Guillermo Muñoz             				Initial version
	  17/06/2016        José Luis González Beltrán          Adapt to UNICA
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static final Map <String, List<BIIN_Tabla_Correspondencia__c>> map_tc = new Map <String, List<BIIN_Tabla_Correspondencia__c>>();

	static{
		for(BIIN_Tabla_Correspondencia__c tc : [SELECT BIIN_RoD_ID__c, BIIN_Valor_Salesforce__c, BIIN_Valor_Remedy__c, BIIN_Tipo_Tabla__c FROM BIIN_Tabla_Correspondencia__c]){

			List <BIIN_Tabla_Correspondencia__c> lst_tcAux = new List <BIIN_Tabla_Correspondencia__c>();
			if(map_tc.containsKey(tc.BIIN_Tipo_Tabla__c)){

				lst_tcAux = map_tc.get(tc.BIIN_Tipo_Tabla__c);
				lst_tcAux.add(tc);
			}
			else{

				lst_tcAux.add(tc);
			}
			map_tc.put(tc.BIIN_Tipo_Tabla__c, lst_tcAux);
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that transalate values between salesforce and RoD using 'BIIN_Tabla_Correspondencia__c' custome setting records.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/01/2016                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static String traducirValor(String field , String value){

		String toReturn;

		if(map_tc.containsKey(field)){
			for(BIIN_Tabla_Correspondencia__c tc : map_tc.get(field)){
				if(tc.BIIN_RoD_ID__c == value){
					toReturn = tc.BIIN_Valor_Salesforce__c;
					break;
				}
				else if(tc.BIIN_Valor_Salesforce__c == value){
					toReturn = tc.BIIN_RoD_ID__c;
					break;
				}
				else if(tc.BIIN_Valor_Remedy__c == value){
					toReturn = tc.BIIN_RoD_ID__c;
					break;
				}
			}
		}

		return toReturn;

	}

}