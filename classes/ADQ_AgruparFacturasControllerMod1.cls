public  class ADQ_AgruparFacturasControllerMod1 {

	public Factura__c factura{get;set;}									//Encabezado de facturaci�n elegido
	public List<Programacion__c> listaProgramaciones;						//lista de programaciones para el encabezado de facturaci�n (Las programaciones no deben estar migradas ni exportadas)
	
	public List<WrapperProgramacion> listaWrapperProgramaciones;
	public List<List<Programacion__c>> matrizProgramaciones;
	public ADQ_DivisaUtility divisaUtility;
	
	public List<Pedido_PE__c> listaPedidosPEClon = new List<Pedido_PE__c>();
	public List<Pedido__c> listaPedClon = new List<Pedido__c>();
	public Boolean success{get;set;}
	
		
	public ADQ_AgruparFacturasControllerMod1(){
		String idFactura = ApexPages.currentPage().getParameters().get('fid');
		divisaUtility = new ADQ_DivisaUtility();
		factura = new  Factura__c ();
		if(idFactura != null){
			//recupero los datos de la factura (Encabezadod de facturacion)
			for (Factura__c fac01: [SELECT Tipo_de_Factura__c, Requiere_Anexo__c, Name, Migrado__c, Inicio_de_Facturaci_n__c, Id,
									IVA__c, Fecha_Inicio__c, Fecha_Fin__c, CurrencyIsoCode, Condiciones_de_Pago__c, Activo__c,
									//datos del cliente (Account)
									Cliente__c
							FROM Factura__c 
							WHERE Id = :idFactura LIMIT 1] ){
					factura	= fac01;		
			}
			/*
			factura = [SELECT Tipo_de_Factura__c, Requiere_Anexo__c, Name, Migrado__c, Inicio_de_Facturaci_n__c, Id,
									IVA__c, Fecha_Inicio__c, Fecha_Fin__c, CurrencyIsoCode, Condiciones_de_Pago__c, Activo__c,
									//datos del cliente (Account)
									Cliente__c
							FROM Factura__c 
							WHERE Id = :idFactura LIMIT 1];
			*/				
			

			//Recupero las programaciones pedndientes de factura seleccionada
			listaProgramaciones = [SELECT Id, Name, CurrencyIsoCode, Fecha_de_inicio_de_cobro__c, 
										Inicio_del_per_odo__c, Fin_del_per_odo__c, Importe__c, IVA__c, Total__c
									FROM Programacion__c
									WHERE Factura__c =: factura.Id AND Facturado__c = false 
										AND Exportado__c = false AND Numero_de_factura_SD__c = ''
										AND Bloqueo_por_agrupaci_n__c = false AND Bloqueo_por_facturaci_n__c = false 
									ORDER BY Inicio_del_per_odo__c ASC];
									
								
		}else{
			factura = null;
		}
	}
	
	public List<WrapperProgramacion> getListaWrapperProgramaciones(){
		if(listaWrapperProgramaciones == null){
			if(listaProgramaciones != null && listaProgramaciones.size() > 0){
				listaWrapperProgramaciones = new List<WrapperProgramacion>();
				for(Programacion__c p : listaProgramaciones){
					listaWrapperProgramaciones.add(new WrapperProgramacion(p, false));
				}
			}
		}
		return listaWrapperProgramaciones;
	}
	
	public void crearProgramaciones(){
		this.creaMatrizAgrupaciones();
		if(validarAgrupacion()){
			creaNuevasProgramaciones();
			
		}
	}
	
	/*
	* Valida que las programaciones de cada lista en la matrizProgramaciones sea de la misma moneda
	*/
	public Boolean validarAgrupacion(){
		String msg = '';
		if (matrizProgramaciones != null){
			for(List<Programacion__c> lp : matrizProgramaciones){
				String currencyIsoCode = '';
				for(Programacion__c p : lp){
					if(currencyIsoCode == '') currencyIsoCode = p.CurrencyIsoCode;
					else if(currencyIsoCode != p.CurrencyIsoCode){
						msg += 'La moneda de las programaciones por agrupar debe ser la misma.';
					}
				}
				msg = (msg!='')?'La moneda de las programaciones por agrupar debe ser la misma.':msg;
			}
		}
		/*if(msg != ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, msg));
			success = false;
			return false;
		}*/
		return true;
	}
	
	/*
	* Crea una matriz con las programaciones seleccionadas para agrupar, valida que sean programaciones consecutivas
	*/
	public void creaMatrizAgrupaciones(){
		matrizProgramaciones = new List<List<Programacion__c>>();
		//System.debug('>>>>listaWrapperProgramaciones.size(): '+ listaWrapperProgramaciones.size());
		List<Programacion__c> lp = new List<Programacion__c>();
		if (listaWrapperProgramaciones != null){
			for(WrapperProgramacion wp : listaWrapperProgramaciones){
				if(wp.selected == true){
					if(lp.size() == 0){
						lp.add(wp.programacion);
						//matrizProgramaciones.add(lp.deepClone(true));
						System.debug('>>> 1: wp.programacion  :  '+wp.programacion );
					}
					else{
						if(lp[lp.size()-1].Fin_del_per_odo__c.addDays(1) == wp.programacion.Inicio_del_per_odo__c){
							lp.add(wp.programacion);
							//matrizProgramaciones.add(lp.deepClone(true));
							System.debug('>>> 2: wp.programacion  :  '+wp.programacion );
						}
						else{
							//se guarda la lista si tiene almenos 2 programaciones
							if(lp.size() > 1) {
							//matrizProgramaciones.add(lp.deepClone(true));
							}
							//lp = new List<Programacion__c>();
							lp.add(wp.programacion);
							System.debug('>>> 3: wp.programacion  :  '+wp.programacion );
						}
					}
				}
			}
		}
		if(lp.size() > 1)matrizProgramaciones.add(lp.deepClone(true));
		System.debug('>>>>>1 matrizProgramaciones.size() '+matrizProgramaciones.size()+ ' ::  matrizProgramaciones 1 '+ matrizProgramaciones );
	}
	
	public void creaNuevasProgramaciones(){
		
		
		String idFactura = ApexPages.currentPage().getParameters().get('fid');
		if(matrizProgramaciones.size() > 0){
			System.debug('>>>>>1 matrizProgramaciones.size() '+matrizProgramaciones.size()+ ' ::  matrizProgramaciones 1 '+ matrizProgramaciones );
			for(List<Programacion__c> listaProgramaciones : matrizProgramaciones){
				// obtengo la primera programacion y la clono....
				
				Programacion__c primeraProgramacion = listaProgramaciones[0].clone(true, true);
				
				//obtengo la ultima programacion y la clono.
				Programacion__c ultimaProgramacion = listaProgramaciones[listaProgramaciones.size()-1].clone(true, true);
				
				//actualizo las fechas de la �ltima programacion, ya que en ella se van a unir todos los pedidos
				ultimaProgramacion.Inicio_del_per_odo__c = primeraProgramacion.Inicio_del_per_odo__c;
				ultimaProgramacion.Fecha_de_inicio_de_cobro__c = (factura.Tipo_de_Factura__c == 'Anticipado')?ultimaProgramacion.Inicio_del_per_odo__c:ultimaProgramacion.Fin_del_per_odo__c.addDays(1);
				ultimaProgramacion.Fecha__c = Date.today();
				ultimaProgramacion.id = null;
				ultimaProgramacion.Agrupaci_n__c = true;
				ultimaProgramacion.Factura__c = factura.Id;
				// insertamos la ultima nueva programacion a la que se le van a agrupar los Pedidos PE
				insert ultimaProgramacion;
			
				//Actualizo el id de programacion de los pedidos y el importe de la porgramaci�n
				Double importe = 0;
				Double ieps =0;
				for(List<Pedido_PE__c> listaPedidosPE : [SELECT Id, Precio_original__c, CurrencyIsoCode, Name,Inicio_del_per_odo__c,Fin_del_per_odo__c,
														 	Precio_convertido__c, Moneda_de_conversi_n__c, Concepto__c ,
														 	Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c 
														 FROM Pedido_PE__c 
														 WHERE Programacion__c IN :listaProgramaciones]){
													 	
				// itero sobre la lista de los pedidos PE para agregarlos a la nueva programacion creada 										 	
					 
					for(Pedido_PE__c ppe : listaPedidosPE){
						Pedido_PE__c pedClon = ppe.clone(true, true);
						System.debug('>>>ppe:  ' + ppe.name  + '   ppe: ' + ppe.Concepto__c);
						pedClon.Concepto__c = ppe.Concepto__c;
						pedClon.id = null;
						//pedClon.Inicio_del_per_odo__c = ultimaProgramacion.Inicio_del_per_odo__c;
						//pedClon.Fin_del_per_odo__c =ultimaProgramacion.Fin_del_per_odo__c ;
						pedClon.Programacion__c = ultimaProgramacion.Id;
						pedClon.Moneda_de_conversi_n__c = factura.CurrencyIsoCode;
						pedClon.Precio_convertido__c = divisaUtility.round(divisaUtility.transformCurrency(ppe.CurrencyIsoCode, pedClon.Moneda_de_conversi_n__c,  Double.valueOf(''+pedClon.Precio_original__c)), 2);
						pedClon.Lleva_IEPS__c = (Date.today()>=Date.newInstance(2010, 1, 1))?pedClon.Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c:false;
						pedClon.IEPS__c = (pedClon.Lleva_IEPS__c)? Double.valueOf(''+pedClon.Precio_convertido__c)*0.03:0;
						importe += pedClon.Precio_convertido__c;
						ieps += pedClon.IEPS__c;
						
						listaPedidosPEClon.add(pedClon);
						
					}
					insert listaPedidosPEClon;
											
				}
				// creamos un nuevo pedio y lo actalizamos con la nueva programacion..
				for(List<Pedido__c> listaPedidos : [SELECT Id, Importe__c, CurrencyIsoCode 
													FROM Pedido__c 
													WHERE Programacion__c IN :listaProgramaciones]){
					for(Pedido__c p : listaPedidos){
						Pedido__c pclone  = p.clone(true,true);
						pclone.Id =  null;
						pclone.Programacion__c = ultimaProgramacion.Id;
						listaPedClon.add(pclone);
					}
					insert listaPedClon;
				}
				
				try{
					ultimaProgramacion.Importe__c = importe;
					ultimaProgramacion.IEPS__c = ieps;
					update ultimaProgramacion;
				
					//remueve de la lista la ultima programacion..
					//listaProgramaciones.remove(listaProgramaciones.size()-1);
					
					//quito las programaciones del encabezado de facturaci�n
					for(Programacion__c p : listaProgramaciones){
						//p.Borrar_por_agrupacion__c = true;
						p.Bloqueo_por_facturaci_n__c = true;
						p.Bloqueo_por_agrupaci_n__c = true;
						p.Motivo_de_bloqueo__c = 'Esta progrmacion ha sido agrupada en la nueva prog: ' + ultimaProgramacion.Id + '  por: '+Userinfo.getName();
						p.Agrupaci_n__c = false;
					}
					update listaProgramaciones;
					
					/*
					* insertar en el objeto de relacion de agrupacion las programaciones padres y precedentes..
					*/ 
					
					
					for(Programacion__c prog : listaProgramaciones){
						Registro_de_agrupacion__c registro = new Registro_de_agrupacion__c();
						registro.Programaci_n_Precedente__c =prog.id;
						registro.Programaci_n_Padre__c  = ultimaProgramacion.Id;
						registro.Name =  ultimaProgramacion.Id + ' vs '+ prog.Id;
						registro.Encabezado_de_facturaci_n__c = idFactura;
						registro.activo__c = true;
						insert registro;
						System.debug('>>>> Registro insertado<<<<');
					}
					
					
					// desactivamos el borrado de las programaciones precedentes...
					//delete listaProgramaciones;
					
					success = true;
				}
				catch(Exception e){
					success = false;
					ApexPages.addMessages(e);
					//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'ProgramacionId: ' + ultimaProgramacion.Id));
				}
			}
		}
		else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'No ha seleccionado facturas para agrupar o las facturas seleccionadas no tienen per�odos consecutivos.'));
			success = false;
		}
	}
	
	public Integer getTotalMatrizProgramaciones(){
		if(matrizProgramaciones == null) return 0;
		return matrizProgramaciones.size();
	}
	
	public PageReference cancelar(){
    	return new Apexpages.Pagereference('/' + factura.Id);
    }
    public void cubre (){
    	Integer i=0;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    	i++;
    }
	
	public class WrapperProgramacion{
		public Programacion__c programacion{get;set;}
		public Boolean selected{get;set;}
		
		public WrapperProgramacion(Programacion__c programacion, Boolean selected){
			this.programacion = programacion;
			this.selected = selected;
		}
	}
	 
	 
	
	

}