@isTest(seeAllData = false)
public class BIIN_Detalle_Solicitud_TEST {
    
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales Rodríguez
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_CreacionIncidencia_Ctrl
            
     <Date>                 <Author>                        <Change Description>
    11/2015              Ignacio Morales Rodríguez          Initial Version
	06/07/2016			 José Luis Gonzáles Beltrán			Adapt test to UNICA modifications
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void BIIN_Detalle_Solicitud_TEST() {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];
        
        System.runAs(actualUser){
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
        	RecordType rt=[SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName='BIIN_Solicitud_Incidencia_Tecnica'];
        	Account acc=BIIN_Test_Data_Load.createAccount('ACME');
            acc.OwnerId=actualUser.id;
            insert acc;
        	Contact cont=BIIN_Test_Data_Load.createContact('Pachamama', acc);
        	insert cont;
        	Case caso = new Case(RecordTypeId = rt.id, AccountId = acc.id, OwnerId= actualUser.id, ContactId = cont.id, TGS_Urgency__c = '2-High');
        	insert caso;        
            Attachment attach = new Attachment(Name='Attachment Test',ParentId=caso.id ,Body=Blob.valueOf('Unit Test Attachment Body'));
            insert attach;
            Test.startTest();
            
            PageReference p = Page.BIIN_Detalle_Solicitud;
            p.getParameters().put('Idcaso', caso.Id);
            Test.setCurrentPage(p); 
            
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BIIN_Detalle_Solicitud_Ctrl ceController = new BIIN_Detalle_Solicitud_Ctrl(controller);
			ceController.modBoolean=true;  
            
            ceController.Modificar();
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
            ceController.GuardarModif();
            ceController.Cancelar();
            ceController.attach_Id=attach.id;
            ceController.showAttachment();
            //nuevas
            ceController.MostrarPopUpEstado();
            ceController.NOOKdetalle();
            ceController.OKdetalle();
            ceController.TimeOUT();
            Test.stopTest();
        }
        
	}
    
	@isTest static void BIIN_Detalle_Solicitud_TEST_fail() 
    {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];
        
        System.runAs(actualUser)
        {
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
            RecordType rt=[SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName='BIIN_Solicitud_Incidencia_Tecnica'];
            Account acc=BIIN_Test_Data_Load.createAccount('ACME');
            acc.OwnerId=actualUser.id;
            insert acc;
            Contact cont=BIIN_Test_Data_Load.createContact('Pachamama', acc);
            insert cont;
            Case caso = new Case(RecordTypeId = rt.id, AccountId = acc.id, OwnerId= actualUser.id, ContactId = cont.id, TGS_Urgency__c = '2-High');
            insert caso;        
            Attachment attach = new Attachment(Name='Attachment Test',ParentId=caso.id ,Body=Blob.valueOf('Unit Test Attachment Body'));
            insert attach;
            
            Test.startTest();
            
            PageReference p = Page.BIIN_Detalle_Solicitud;
            p.getParameters().put('Idcaso', caso.Id);
            Test.setCurrentPage(p); 
            
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BIIN_Detalle_Solicitud_Ctrl ceController = new BIIN_Detalle_Solicitud_Ctrl(controller);
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
            ceController.GuardarModif();
            
            Test.stopTest();
        }
    }
    
}