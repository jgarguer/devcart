/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to check CWP_MonitoringCtrl 
History:

<Date>                  <Author>                <Change Description>
18/05/2017              Everis                  Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class CWP_MonitoringCtrl_TEST {
    
    @testSetup 
    private static void dataModelSetup() {  
        
         //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
            set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
           //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
            
          
         //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        contactTest.BI_Cuenta_activa_en_portal__c = accLegalEntity.id;
        insert contactTest;
        
        User usuario;
        BI_Service_Tracking_URL_PP__c service1= new  BI_Service_Tracking_URL_PP__c(); 
        BI_Service_Tracking_URL_PP__c service2= new  BI_Service_Tracking_URL_PP__c(); 
        BI_Service_Tracking_URL_PP__c service3= new  BI_Service_Tracking_URL_PP__c();
        BI_Service_Tracking_URL_PP__c service4= new  BI_Service_Tracking_URL_PP__c(); 
        
        BI_Contact_Customer_Portal__c newRecordCCP = new BI_Contact_Customer_Portal__c();         
        
        
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            
            
            insert usuario;  
            
            TGS_User_Org__c setting = new TGS_User_Org__c();
            setting.TGS_Is_TGS__c = true;
            insert setting;
            
            newRecordCCP.BI_User__c =usuario.Id;
            newRecordCCP.BI_Cliente__c = accLegalEntity.id;
            newRecordCCP.BI_SW_User__c = 'abcd';
            newRecordCCP.BI_SW_Pwd__c = 'abcd1234';
            insert newRecordCCP;
            
            service1.BI_Country__c = 'Spain';
            service1.CWP_App_Name__c='Consolidated View'; 
            service1.CWP_TGS_Users__c = true;
            service1.CWP_isPublic__c = true;            
            insert service1; 
            
            service2.BI_Country__c = 'Spain';
            service2.CWP_App_Name__c='Top Performance';     
            service2.CWP_TGS_Users__c = true;
            service2.CWP_isPublic__c = true;        
            insert service2;   
            
            service3.BI_Country__c = 'Spain';
            service3.CWP_App_Name__c='Network Quality';  
            service3.CWP_TGS_Users__c = true;
            service3.CWP_isPublic__c = true;            
            insert service3; 
            
            service4.BI_Country__c = 'Spain';
            service4.CWP_App_Name__c='Traffic Management'; 
            service4.CWP_TGS_Users__c = true;
            service4.CWP_isPublic__c = true;             
            insert service4; 
           
        }      
          
                                                       
        
        
        
      
      
    }   
    
 
    
     @isTest
    private static void getAplisTest() {     
        system.debug('test 1 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_MonitoringCtrl.getAplis();   
                          
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getMapaBIENTest() {     
        system.debug('test 2 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_MonitoringCtrl.getMapaBIEN();                  
            Test.stopTest();
        }       
        
    } 
    
       @isTest
    private static void getTgsTest() {     
        system.debug('test 3 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_MonitoringCtrl.getTgs();                  
            Test.stopTest();
        }       
        
    } 
    
    
       @isTest
    private static void getBienTest() {     
        system.debug('test 4 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_MonitoringCtrl.getBien();                  
            Test.stopTest();
        }       
        
    }
    
       @isTest
    private static void invokeAppsTest() {     
        system.debug('test 5 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_MonitoringCtrl.invokeApps();
            Map<String, String> appMap = new Map<String, String>();
            appMap.put('appName', 'appName');
            Cache.Session.put('local.CWPexcelExport.AppValues', appMap);
            CWP_MonitoringCtrl.invokeApps();               
            Test.stopTest();
        }       
        
    }
    @isTest
    private static void CWP_MonitoringCtrlTest(){
        Test.startTest(); 
        CWP_MonitoringCtrl aux = new CWP_MonitoringCtrl();
        Test.stopTest(); 
    }
  
    @isTest
    private static void getProfileTest() {     
        system.debug('test 4 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();         
            CWP_MonitoringCtrl aux = new CWP_MonitoringCtrl();            
            aux.getProfile();                  
            Test.stopTest();
        }       
        
    }
  
}