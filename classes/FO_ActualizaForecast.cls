/********************************************
 * Author: Javier Lopez Andradas
 * Description: Class to controll the refresh of the Forecast Items and the view of forecast
 * 
 * 
 * 
 * 
 * 
 * *****************************************/
public class FO_ActualizaForecast {
    /*****************************************
     * Author:  JAvier Lopez Andradas
     * Company: gCTIO
     * Description: Helper class to refresh all FOI data in a given Forecast 
     * 
     * <date>		<version>		<Description>
     * 12/04/2018	1.1				avoid the FOi of Opps in F6
     * 
     * ***************************************/
    @AuraEnabled
    public static String actualizaFO(String idFore){
        FO_Forecast__c fore = [Select Id,FO_Estado__c,CurrencyIsoCode,FO_Periodo__r.BI_FVI_Activo__c,FO_Periodo__r.BI_FVI_Fecha_Inicio__c,FO_Periodo__r.BI_FVI_Fecha_Fin__c,FO_Nodo__c,RecordType.DeveloperName,FO_Commit_Pipeline__c ,FO_Upside_Pipeline__c from FO_Forecast__c where Id=:idFore];
        if(fore.FO_Estado__c.equals('Enviado') || fore.FO_Periodo__r.BI_FVI_Activo__c==false){
            return 'OK';
        }
        List<CurrencyType> lst_cr = [Select IsoCode,ConversionRate from CurrencyType where isActive=true];
     	Map<String,double> mp_curr = new Map<String,double>();
    	for(CurrencyType curr:lst_cr)
        	mp_curr.put(curr.IsoCode,curr.ConversionRate);
        System.debug('No esta en Enviado');
        if(fore.RecordType.DeveloperName.equals('FO_Ejecutivo')){
            //Es de tipo semanal
            List<FO_Forecast_Item__c> lst_FOI = [select Id,FO_Opportunity__c,FO_StageName__c,FO_NAV__c from FO_Forecast_Item__c where FO_Forecast_Padre__c=:idFore ];
            Map<String,FO_Forecast_Item__c> mp_FOI = new Map<String,FO_Forecast_Item__c>();
            for(FO_Forecast_Item__c foi:lst_FOI){
                mp_FOI.put(foi.FO_Opportunity__c,foi);
            }
            System.debug('Hemos sacado los FOI');
            List<BI_FVI_Nodo_Cliente__c> lst_ncl = [Select BI_FVI_IdCliente__c from BI_FVI_Nodo_Cliente__c where BI_FVI_IdNodo__c=:fore.FO_Nodo__c ];
            List<String> lst_acc = new List<String>();
            for(BI_FVI_Nodo_Cliente__c ncl : lst_ncl)
                lst_acc.add(ncl.BI_FVI_IdCliente__c);
            System.debug('lista cuentas: '+lst_acc);
            System.debug('Inicio: '+fore.FO_Periodo__r.BI_FVI_Fecha_Inicio__c);
            System.debug('Fin: '+fore.FO_Periodo__r.BI_FVI_Fecha_Fin__c);
            Schema.DescribeFieldResult stageNameField = Opportunity.StageName.getDescribe();
            List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
            List<String> val_def = new List<String>();
            for(Schema.PicklistEntry entry :values){
                if(entry.getValue().contains('Closed Lost')==true || entry.getValue().contains('Cancelled | Suspended') || entry.getValue().contains('F1') ==true ||  entry.getValue().contains('F6') ==true)
                    val_def.add(entry.getValue());
            }
            System.debug('StagesNames: '+val_def);
            System.debug('Hemos sacado los clientes');
            List<Opportunity> lst_Opp = [select currencyIsoCode,Id,StageName,CloseDate,BI_Net_annual_value_NAV__c from Opportunity where CloseDate>=:fore.FO_Periodo__r.BI_FVI_Fecha_Inicio__c AND CloseDate<=:fore.FO_Periodo__r.BI_FVI_Fecha_Fin__c AND AccountId IN:lst_acc AND ( NOT StageName IN :val_def )AND REcordType.DeveloperName='BI_Ciclo_completo'];
            System.debug('lista Ops'+lst_Opp);
            System.debug('mp_foi'+mp_foi);
            List<FO_Forecast_Item__c> lst_upd = new List<FO_Forecast_Item__c>();
            List<FO_Forecast_Item__c> lst_ins = new List<FO_Forecast_Item__c>();
            List<FO_Forecast_Item__c> lst_del = new List<FO_Forecast_Item__c>();
            for(Opportunity opp : lst_Opp){
                FO_Forecast_Item__c foi = mp_foi.get(opp.Id);
                System.debug(foi);
                if(foi!=null){
                    if(foi.FO_NAV__c!=opp.BI_Net_annual_value_NAV__c || foi.FO_StageName__c!=opp.StageName.subString(0,2)){
                        foi.FO_NAV__c=opp.BI_Net_annual_value_NAV__c;
                        foi.FO_StageName__c=opp.StageName.subString(0,2);
                        lst_upd.add(foi);
                    }
                    mp_foi.remove(opp.Id);
                }else{
                    foi = new FO_Forecast_Item__c();
                    foi.FO_StageName__c=opp.StageName.subString(0,2);
                     foi.FO_CloseDate__c=opp.CloseDate;
                    foi.FO_NAV__c=opp.BI_Net_annual_value_NAV__c;
                    foi.FO_Opportunity__c=opp.Id;
                    foi.FO_Forecast_Padre__c=idFore;
                    foi.CurrencyIsoCode=opp.currencyIsoCode;
                    lst_ins.add(foi);
                    
                }
            }   
            System.debug('lst_in'+lst_ins);
            if(lst_upd.isEmpty()==false){
                try{
                    update(lst_upd);
                }catch(Exception e){
                    return  Label.FO_Error_Update_FOI_New+e.getStackTraceString();
                }
           
            }
            if(lst_ins.isEmpty()==false){
                try{
                    insert(lst_ins);
                }catch(Exception e){
                    return Label.FO_New_Opps_FOI_refresh+e.getMessage();
                }
            }
            if(mp_foi.values().isEmpty()==false){
                for(String ops : mp_FOI.keySet()){
                    lst_del.add(mp_FOI.get(ops));
                }
                try{
                  delete(lst_del);
                }catch(Exception e){
                    return Label.FO_Deleted_Opps_FOI_refresh+e.getStackTraceString();
                }
            }
            return 'OK';
        }else{
            //Es manager hya que buscar los Forecast hijos
            //
            Map<Id,FO_Forecast__c> lst_sons = new Map<Id,FO_Forecast__c>([select Id,FO_Estado__c,FO_Forecast_Padre__c,FO_Commit_Pipeline__c,FO_Upside_Pipeline__c,CurrencyIsoCode  from FO_Forecast__c where FO_Nodo__r.BI_FVI_NodoPadre__c=:fore.FO_Nodo__c AND FO_Periodo__c=:fore.FO_Periodo__c]);
            List<FO_Forecast_Futuro__c> lst_futFo =  [Select Id,FO_Forecast_Padre__c,FO_Ajuste_Forecast_Manager__c,FO_Total_Forecast_Ajustado__c,FO_Periodo__c,CurrencyIsoCode FROM FO_Forecast_Futuro__c where (FO_Forecast_Padre__r.FO_Nodo__r.BI_FVI_NodoPadre__c=:fore.FO_Nodo__c  AND FO_Forecast_Padre__r.FO_Periodo__c = :fore.FO_Periodo__c) OR FO_Forecast_Padre__c=:fore.Id];
            Map<String,Map<String,FO_Forecast_Futuro__c>> mp_futFo = new Map<String,Map<String,FO_Forecast_Futuro__c>>();
            for(FO_Forecast_Futuro__c foFut:lst_futFo){
                Map<String,FO_Forecast_Futuro__c> mp_aux = mp_futFo.get(foFut.FO_Forecast_Padre__c);
                if(mp_aux==null)
                    mp_aux = new Map<String,FO_Forecast_Futuro__c> ();
                mp_aux.put(foFut.FO_Periodo__c,foFut);
                mp_futFo.put(foFut.FO_Forecast_Padre__c,mp_aux);
            }
            List<FO_Forecast__c> lst_upd = new List<FO_Forecast__c>();
            List<FO_Forecast_Futuro__c> lst_creFut = new List<FO_Forecast_Futuro__c>();
            Map<Id,FO_Forecast_Futuro__c> mp_updFut = new Map<Id,FO_Forecast_Futuro__c>();
            if(lst_sons.keySet().size()!=0){
                for(String key : lst_sons.keySet()){
                    FO_Forecast__c act = lst_sons.get(key);
                    
                    if(act.FO_Forecast_Padre__c != fore.Id){
                        
                        if(act.FO_Estado__c.equals('Enviado')){
                            
                            fore.FO_Commit_Pipeline__c +=act.FO_Commit_Pipeline__c*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(act.CurrencyIsoCode) ;
                            fore.FO_Upside_Pipeline__c+=act.FO_Upside_Pipeline__c*mp_curr.get(fore.CurrencyIsoCode)/mp_curr.get(act.CurrencyIsoCode);
                            //No hace falta actualizar un padre porque no deberia tener otro padre != al actula a no ser que
                            //este sea nulo
                            Map<String,FO_Forecast_Futuro__c> mp_act = mp_futFo.get(act.Id);
                            Map<String,FO_Forecast_Futuro__c> mp_fore = mp_futFo.get(fore.Id);
                            for(String per : mp_act.keySet()){
                                FO_Forecast_Futuro__c futAct = mp_act.get(per);
                                FO_Forecast_Futuro__c futFore = mp_fore.get(per);
                              /* JLA code not neeeded if(futFore==null){
                                    futFore = new FO_Forecast_Futuro__c();
                                    futFore.FO_Ajuste_Forecast_Manager__c=futAct.FO_Ajuste_Forecast_Manager__c;
                                    futFore.FO_Total_Forecast_Ajustado__c=futAct.FO_Total_Forecast_Ajustado__c;
                                    futFore.FO_Periodo__c=per;
                                    futFore.FO_Forecast_Padre__c=fore.Id;
                                    mp_fore.put(per, futFore);
                                    lst_creFut.add(futFore);
                                }else{*/
                                    futFore.FO_Ajuste_Forecast_Manager__c+=futAct.FO_Ajuste_Forecast_Manager__c*mp_curr.get(futFore.CurrencyIsoCode)/mp_curr.get(futAct.CurrencyIsoCode);
                                    futFore.FO_Total_Forecast_Ajustado__c+=futAct.FO_Total_Forecast_Ajustado__c*mp_curr.get(futFore.CurrencyIsoCode)/mp_curr.get(futAct.CurrencyIsoCode);
                                    mp_updFut.put(futFore.Id,futFore);
                                //}
                            }
                        }
                        act.FO_Forecast_Padre__c = fore.Id;
                        lst_upd.add(act);
                    }
                    
                }
                lst_upd.add(fore);
                
                System.debug('JLA mp actualizar: '+lst_upd);
                if(lst_upd.isEmpty()==false){
                    update(lst_upd);
                }
                System.debug('JLA crea los futuros que no esten creados');
                if(lst_creFut.isEmpty()==false){
                   	insert (lst_creFut);    
                }
                if(mp_updFut.values().isEmpty()==false){
                    update(mp_updFut.values());
                }
                    
                
            }
            
            return 'OK';
        }
        
    }
	    
}