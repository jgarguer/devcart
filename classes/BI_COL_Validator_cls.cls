global class BI_COL_Validator_cls 
{
	/**
	* 	Esta clase permite controlar los tipos de Registro del sistema, 									*
	* 	variables globales y funciones generales de acceso global											*
	*	Desarrollado por: AVANXO Sucursal Colombia															*
	*	Versión: 1.0																						*
	*	Release Notes: 																						*
	*	(1.0) WPA: Definición de variables globales para casos e implementación de Funciones Generales.		*
	*/
	
	// Bandera para marca del Log de Transacciones
	private static boolean blnCasoGeneradoSFDC = false;
		
	// ================================================================
	// FUNCIONES INTERFACES CON GESDAR, SISGOT, DAVOXPLUS
	// ================================================================
			
	// Permite determinar que el Caso es generado desde Salesforce // Uso: Interfaz SFDC - GESDAR
	public static void setCasoGeneradoSFDC(){
		blnCasoGeneradoSFDC = true;
	}
	public static boolean getCasoGeneradoSFDC(){
		return blnCasoGeneradoSFDC;
	}
	
	// Bandera para marca del Caso
	private static boolean blnCasoActualizado = false;
				
	// Permite determinar que el Caso es actualizado desde el trigger y evitar loops infinitos
	public static void setCasoActualizado(){
		blnCasoActualizado = true;
	}
	public static void setCasoNOActualizado(){
		blnCasoActualizado = false;
	}
	public static boolean getCasoActualizado(){
		return blnCasoActualizado;
	}
	
	// fn_DS_NoEnviarDAVOXPLUS: Valida si el tipo de Registro de la DS está en el grupo de DS que NO se deben enviar a DavoxPlus.
	// INPUT: 	id idTipoRegistroDS = ID del tipo de registro de la DS.
	// OUTPUT: 	boolean = true/false. Retorna TRUE si el id está en la lista de NO enviar a DavoxPlus
	/*public static boolean fn_DS_NoEnviarDAVOXPLUS(id idTipoRegistroDS){
		System.Debug('INDICADOR NO ENVIAR A DAVOX EN FN_DS_NOENVIAR> ' + idTipoRegistroDS);
		boolean respuesta = false;
		set<id> TiposRegistroNoEnviar_set = new set<id>();
		TiposRegistroNoEnviar_set.add(idDSLargaDistancia);
		TiposRegistroNoEnviar_set.add(idDS01800Internacional);
		TiposRegistroNoEnviar_set.add(idDS018000Nacional);
		TiposRegistroNoEnviar_set.add(idDSPuestoDeVoz);
		
		respuesta = TiposRegistroNoEnviar_set.contains(idTipoRegistroDS);
		
		System.Debug('Valor respuesta en no enviar a davox: ' + respuesta);
		return respuesta;
	}*/
	
	// ================================================================
	// FUNCIONES GENERALES
	// ================================================================
	
	// fn_FechasPredeterminadas: Permite cargar fechas predeterminadas según el formato requerido.
	// INPUT: 	string strNombreFecha = Nombre de la Fecha en Español (HOY, AYER, MAÑANA, ESTE_AÑO, ESTE_MES, AHORA).
	//			string strFormato = Formato con el cual se desea obtener la Fecha solicitada.
	// OUTPUT: 	string = retorna la fecha solicitada con el formato indicado como texto.
	// Ej1: string miFechaHora = fn_FechasPredeterminadas('HOY', 'dd/MM/yyyy'); ==> 30/06/2009 (Si la fecha es Jun30/2009)
	// Ej2: string miFechaHora = fn_FechasPredeterminadas('AHORA', 'MM/dd/yyyy hh:mm:ss'); ==> 06/30/2009 14:07:33 (Si la fecha es Jun30/2009)
	public static string fn_FechasPredeterminadas(string strNombreFecha, string strFormato){
		// Para consultar los formatos posibles ver: http://java.sun.com/j2se/1.4.2/docs/api/java/text/SimpleDateFormat.html
		string strRespuesta = '';
		datetime miFechaHora = system.now();
		if(strNombreFecha.equalsIgnoreCase('HOY'))	
			strRespuesta = miFechaHora.format(strFormato);
		if(strNombreFecha.equalsIgnoreCase('AYER')){
			miFechaHora = miFechaHora.addDays(-1);
			strRespuesta = miFechaHora.format(strFormato);
		}
		if(strNombreFecha.equalsIgnoreCase('MAÑANA')){
			miFechaHora = miFechaHora.addDays(1);
			strRespuesta = miFechaHora.format(strFormato);
		}
		if(strNombreFecha.equalsIgnoreCase('ESTE_AÑO'))
			strRespuesta = miFechaHora.format('yyyy');
		if(strNombreFecha.equalsIgnoreCase('ESTE_MES'))
			strRespuesta = miFechaHora.format('MM');
		if(strNombreFecha.equalsIgnoreCase('AHORA'))
			strRespuesta = miFechaHora.format(strFormato);
		return strRespuesta;
	}	
	
	// fn_QuitarTildes: Función eliminar las tildes de una cadena de texto y dejar su caracter normal. (sólo para letras vocales).
	// INPUT: 	string strFieldValue = Valor del campo en TEXTO.
	// OUTPUT: 	string = retorna la letra en mayúscula o minúscula en reemplazo de su similar con tilde.
	public static string fn_QuitarTildes(string strFieldValue){
		// Elimina las tildes
		if(strFieldValue!=null)
		{
			strFieldValue = strFieldValue.replaceAll('[\\xc0-\\xc6]','A');
			strFieldValue = strFieldValue.replaceAll('[\\xe0-\\xe6]','a');
			strFieldValue = strFieldValue.replaceAll('[\\xc8-\\xcb]','E');
			strFieldValue = strFieldValue.replaceAll('[\\xe8-\\xeb]','e');
			strFieldValue = strFieldValue.replaceAll('[\\xcc-\\xcf]','I');
			strFieldValue = strFieldValue.replaceAll('[\\xec-\\xef]','i');
			strFieldValue = strFieldValue.replaceAll('[\\xd2-\\xd6]','O');
			strFieldValue = strFieldValue.replaceAll('[\\xf2-\\xf6]','o');
			strFieldValue = strFieldValue.replaceAll('[\\xd9-\\xdc]','U');
			strFieldValue = strFieldValue.replaceAll('[\\xf9-\\xfc]','u');
		}
		return strFieldValue;
	}
	
	// fn_ConvertirNs: Permite convertir las eñes (Ñ) en enes (N) contenidas en una cadena de texto.
	// INPUT: 	string strFieldValue = Valor del campo en TEXTO.
	// OUTPUT: 	string = retorna la n en mayúscula o minúscula en reemplazo de la letra Ñ.
	public static string fn_ConvertirNs(string strFieldValue){
		strFieldValue = strFieldValue.replace('ñ', 'n');
		strFieldValue = strFieldValue.replace('Ñ', 'N');
		return strFieldValue;
	}
	
	// fn_getStringIdsFromList: Permite convertir un LIST de Ids a una cadena de Ids para utilizarlo en un Query Dinámico. 
	// INPUT: 	list<id> IdsProcesar_List = Lista de Ids.
	// OUTPUT: 	string = cadena de ids separados por comas.  Ej: Valor retornado = ('id 1', 'id 2', ..., 'id n')
	public static string fn_getStringIdsFromList(list<id> IdsProcesar_List){
		string strCadenaIds = '(';
		for(id idItem : IdsProcesar_List){
			if(strCadenaIds.length() > 1) // Si ya tiene valores agrega separador antes de agregar el nuevo valor
				strCadenaIds += ','; 
			strCadenaIds += '\'' + idItem + '\'';
		}
		strCadenaIds += ')';
		return strCadenaIds;
	}
	
	// fn_getStringIdsFromList: Permite convertir un LIST de strings a una cadena de strings para utilizarlo en un Query Dinámico. 
	// INPUT: 	list<string> strProcesar_List = Lista de strings.
	// OUTPUT: 	string = cadena de strings separados por comas.  Ej: Valor retornado = ('id 1', 'id 2', ..., 'id n')
	public static string fn_getStringIdsFromStringList(list<string> strProcesar_List){
		string strCadenaIds = '(';
		for(string strItem : strProcesar_List){
			if(strCadenaIds.length() > 1) // Si ya tiene valores agrega separador antes de agregar el nuevo valor
				strCadenaIds += ','; 
			strCadenaIds += '\'' + strItem + '\'';
		}
		strCadenaIds += ')';
		return strCadenaIds;
	}
	
	// Permite obtener el valor del campo
	// campo = nombre API del campo
	// s = Registro
    public static string fn_ObtenerValor(string campo, SObject s){
    	String[] cadenaSeparada;
    	string valorCampo = ''; //, objetoRelacion = '', campoRelacion = '';
    	if(campo.indexOf('.') > -1){ // Valida si se trata de un campo de tipo relación
			cadenaSeparada = campo.split('\\.');
			SObject objetoCampo = s;
			SObject objetoCampoHijo;
			for(integer i = 0; i < cadenaSeparada.size() - 1; i++){
				objetoCampoHijo = objetoCampo.getSObject(cadenaSeparada[i]);
				objetoCampo = objetoCampoHijo;
				if(objetoCampoHijo == null)
					break;
			}
			if(objetoCampo != null)
				valorCampo=''+objetoCampo.get(cadenaSeparada[cadenaSeparada.size() - 1]);
   		}else{
   			valorCampo=''+s.get(campo);
   		}
   		system.debug('**** campo: '+campo+'. valorCampo: '+valorCampo);
       	return valorCampo;
    }
	
	// fn_getStringIdsFromSet: Permite convertir un SETT de Ids a una cadena de Ids para utilizarlo en un Query Dinámico. 
	// INPUT: 	set<id> IdsProcesar_set = Set de Ids. (puede ser un objeto Set o el keyset de un objeto Map).
	// OUTPUT: 	string = cadena de ids separados por comas. Ej: Valor retornado = ('id 1', 'id 2', ..., 'id n')
	public static string fn_getStringIdsFromSet(set<id> IdsProcesar_set){
		string strCadenaIds = '(';
		for(id idItem : IdsProcesar_set){
			if(strCadenaIds.length() > 1) // Si ya tiene valores agrega separador antes de agregar el nuevo valor
				strCadenaIds += ','; 
			strCadenaIds += '\'' + idItem + '\'';
		}
		strCadenaIds += ')';
		return strCadenaIds;
	}
	
	// fn_getStringFromSet: Permite convertir un SET de Strings a una cadena de Strings para utilizarlo en un Query Dinámico. 
	// INPUT: 	set<string> DatosProcesar_set = Set de strings. (puede ser un objeto Set o el keyset de un objeto Map).
	// OUTPUT: 	string = cadena de strings separados por comas. Ej: Valor retornado = ('id 1', 'id 2', ..., 'id n')
	public static string fn_getStringFromSet(set<string> DatosProcesar_set){
		string strCadenaIds = '';
		for(string strItem : DatosProcesar_set){
			if(strCadenaIds.length() > 1) // Si ya tiene valores agrega separador antes de agregar el nuevo valor
				strCadenaIds += ','; 
			strCadenaIds += strItem;
		}
		return strCadenaIds;
	}
	
	// fn_FormatearValor: Permite formatear un valor según su tipo de Dato.
	// INPUT: 	String datoCampo = cualquier valor pero CONVERTIDO a texto, 
	// 			string strTipoCampo = Indica el tipo de dato a procesar (Numérico, Fecha, Lógico, Texto), 
	// 			string strFormato = Indica el formato a dar al valor rcibido (Para Lógico [Sin las comillas]= "1 : 0", "TRUE : FALSE", "Y : N", "S : N" // 
	// 								Para Numérico [Sin las comillas]= "#,###.##", "#.###,##", "###.##", "###"), 
	//			double dbDecimales = Cantidad de digitos después del separador decimal, 
	//			string separadorDecimal = Indica el caracter que identifica el separador decimal ( punto "." ó coma "," ) APLICA sólo para tipo Numérico), 
	// 			boolean ConvertirMayus = Si es Texto indica que el valor recibido se debe convertir a Mayúsculas.
	// OUTPUT: 	String (valor formateado pero de tipo string).
	// Ej: 12324232 ====> 12.324.232,00 // true  ===> 1 // pEdRo  ===> PEDRO // 2009-06-12 ===> 12/06/2009
    public static string fn_FormatearValor(String datoCampo, string strTipoCampo, string strFormato, Double dbDecimales, string separadorDecimal, boolean ConvertirMayus){
		string strValorFormateado = '';
		try 
		{
			//integer intEscala = 0;
			strValorFormateado = datoCampo; // Toma el valor por defecto antes de formatear
			if(strTipoCampo.equalsIgnoreCase('Numérico')){
				Decimal valorDecimal = 0.00;
				valorDecimal = Decimal.valueOf(String.valueOf(datoCampo));
				strValorFormateado = fn_FormatNumber(valorDecimal, strFormato, dbDecimales, separadorDecimal);
			}else if(strTipoCampo.equalsIgnoreCase('Fecha')){
				datetime miFechaHora = system.now();
				miFechaHora = Datetime.valueOf(String.valueOf(datoCampo));
				if(strFormato<>null){
					strValorFormateado = miFechaHora.format(strFormato);
				}else{
					strValorFormateado = String.valueOf(datoCampo);
				}
			}else if(strTipoCampo.equalsIgnoreCase('Lógico')){
				Boolean blnValorBooleano = false;
				if(strFormato<>null){
					if(strFormato.equalsIgnoreCase('1 : 0')){
						if(strValorFormateado=='true'){	
							strValorFormateado = '1';
						}else{
							strValorFormateado = '0';
						}
					}else if(strFormato.equalsIgnoreCase('TRUE : FALSE')){
						strValorFormateado = strValorFormateado.ToUpperCase();
					}else if(strFormato.equalsIgnoreCase('S : N')){
						if(strValorFormateado=='true'){	
							strValorFormateado = 'S';
						}else{
							strValorFormateado = 'N';
						}
					}else if(strFormato.equalsIgnoreCase('Y : N')){
						if(strValorFormateado=='true'){	
							strValorFormateado = 'Y';
						}else{
							strValorFormateado = 'N';
						}
					}
				}
			}else if(strTipoCampo.equalsIgnoreCase('Texto')){
				strValorFormateado = String.valueOf(datoCampo);
				if(ConvertirMayus){
					strValorFormateado = strValorFormateado.toUpperCase();
				}
			}
			//system.debug('VALOR OBTENIDO formateado: '+strValorFormateado);
		}catch(System.exception ex){
			system.debug('\n\n[fn_FormatearValor]: Parámetros recibidos: datoCampo= '+ datoCampo +'. strTipoCampo: '+strTipoCampo+'. strFormato: '+strFormato+'. dbDecimales: '+dbDecimales+'. separadorDecimal: '+separadorDecimal+'. ConvertirMayus: '+ConvertirMayus);
			strValorFormateado = 'ERROR: [fn_FormatearValor] Se presentó la siguiente excepción: Tipo: '+ex.getTypeName()+'. Mensaje: '+ex.getMessage();
			system.debug(strValorFormateado);
		}
		return strValorFormateado;
	}
	
	
	// fn_FormatNumber: Esta función permite formatear un valor numérico y lo retorna con el formato deseado como tipo Texto
	// INPUT:	Decimal dcNumber: Número a formatear CONVERTIDO a tipo Decimal, string strFormato, double dbPosicionesDecimales, string strSeparadorDecimal 	 
	// 			string strFormato = Indica el formato a dar al valor recibido. [Sin las comillas]= "#,###.##", "#.###,##", "###.##", "###"), 
	//			double dbPosicionesDecimales = Cantidad de digitos después del separador decimal, 
	//			string strSeparadorDecimal = Indica el caracter que identifica el separador decimal ( punto "." ó coma "," ) para el resultado, 
	// OUTPUT: 	String (valor formateado pero de tipo string).
	// Ej: 12324232 ====> 12.324.232,00
	public static string fn_FormatNumber(Decimal dcNumber, string strFormato, Double dbPosicionesDecimales, string strSeparadorDecimal){
		string valorCampo = '';
		try{
			integer intEscala = 0;
			// Formatea el campo numérico
			if(strFormato!=null){
				if(strFormato.equalsIgnoreCase('#,###.##')){
					valorCampo = dcNumber.format();
					valorCampo = valorCampo.replace(',',';');
					valorCampo = valorCampo.replace('.',',');
					valorCampo = valorCampo.replace(';','.');
				}else if(strFormato.equalsIgnoreCase('#.###,##')){
					valorCampo = dcNumber.format();
				}else if(strFormato.equalsIgnoreCase('###.##')){
					valorCampo = String.valueOf(dcNumber.toPlainString()); // Muestra el valor sin notación científica .00 si viene en el campo
				}else if(strFormato.equalsIgnoreCase('###')){ // Carga el valor sin decimales pero NO redondea
					valorCampo = String.valueOf(dcNumber.toPlainString()); 
					valorCampo = valorCampo.split('\\.')[0];
				}
				if(dbPosicionesDecimales!=null && !strFormato.equalsIgnoreCase('###')){
					intEscala = dbPosicionesDecimales.intValue();
					String[] strDecimales = valorCampo.split('\\'+strSeparadorDecimal);
					if(strDecimales.size()>1){
						if(strDecimales[strDecimales.size()-1].length() < intEscala){	// Si la cant. de decimales es menor, completa con CEROS
							for(integer q=strDecimales[strDecimales.size()-1].length(); q < intEscala;q++)
								valorCampo += '0';
						}else{ // Si la cant. de decimales es mayor, TRUNCA a la cantidad requerida
							valorCampo = strDecimales[0] + strSeparadorDecimal + strDecimales[strDecimales.size()-1].substring(0,intEscala);
						}
					}else{
						valorCampo += strSeparadorDecimal;
						for(integer q=0; q < intEscala;q++)
							valorCampo += '0';
					}
				}
			}
		}catch(System.exception ex){
			system.debug('\n\n[fn_FormatNumber] Parámetros recibidos: '+dcNumber+' Formato: '+strFormato+' Posiciones Decimales: '+dbPosicionesDecimales+'. Separador Decimal: '+strSeparadorDecimal);
			valorCampo = 'ERROR: [fn_FormatNumber] Se presentó la siguiente excepción: Tipo: '+ex.getTypeName()+'. Mensaje: '+ex.getMessage();
			system.debug(valorCampo);
		}
		return valorCampo;
	}
	
	// fn_formatFechaSFDC: Permite obtener el formato de Fecha que se requiere en Salesforce para almacenar el dato a partir de una cadena. 
	// INPUT:  	string strFecha: Fecha en formato texto. (yyyy/MM/dd)
	// 			string strSeparadorFecha: Determina el caracter separador de los componentes de la fecha recibida. ("/", "-").
	// OUTPUT: 	Date yyyy-MM-dd
	// Ej: yyyy/MM/dd ===> yyyy-MM-dd
	public static Date fn_formatFechaSFDC(string strFecha, string strSeparadorFecha, string strFormatoDato){
    	date dtFecha = system.today();
    	try{
	    	String[] ArrayFecha;
	    	String[] ArrayFormato;
	    	integer indYear = 0, indMonth = 0, indDay = 0;
	    	ArrayFormato = strFormatoDato.split(strSeparadorFecha);
	    	for(integer i = 0; i < ArrayFormato.size(); i++){
	    		if(ArrayFormato[i].EqualsIgnoreCase('yyyy')){
	    			indYear = i;
	    		}else if(ArrayFormato[i].EqualsIgnoreCase('MM')){
	    			indMonth = i;
	    		}else if(ArrayFormato[i].EqualsIgnoreCase('dd')){
	    			indDay = i;
	    		}
	    	}
	    	ArrayFecha = strFecha.split(strSeparadorFecha);
	    	dtFecha = Date.newInstance(Integer.valueOf(ArrayFecha[indYear]), Integer.valueOf(ArrayFecha[indMonth]), Integer.valueOf(ArrayFecha[indDay]));
    	}catch(System.exception ex){
			system.debug('\n\n[fn_formatFechaSFDC] Parámetros recibidos: strFecha= '+strFecha+' strSeparadorFecha= '+strSeparadorFecha+'. strFormatoDato: '+strFormatoDato);
			system.debug('ERROR: [fn_formatFechaSFDC] Se presentó la siguiente excepción: Tipo: '+ex.getTypeName()+'. Mensaje: '+ex.getMessage());
		}
    	return dtFecha;
    }
    
    // fn_formatFechaHoraSFDC: Permite obtener el formato de Fecha/Hora que se requiere en Salesforce para almacenar el dato a partir de una cadena. 
	// INPUT:  	string strFecha: Fecha en formato texto. (yyyy/MM/dd hh:mm:ss)
	// 			string strSeparadorFecha: Determina el caracter separador de los componentes de la fecha recibida. ("/", "-").
	// OUTPUT: 	Datetime yyyy-MM-ddThh:mm:ssZ
	// Ej: yyyy/MM/dd hh:mm:ss ===> yyyy-MM-ddThh:mm:ss // 01/12/2009 19:05:23 ===> 2009-12-01T19:05:23Z
	public static Datetime fn_formatFechaHoraSFDC(string strFechaHora, string strSeparadorFecha, string strFormatoDato){
    	datetime dttFechaHora = system.now();
    	try{
	    	String[] ArrayFecha;
	    	String[] ArrayHora;
	    	String[] ArrayFormatoFecha;
	    	String[] ArrayFormatoHora;
	    	String[] ArrayFormato;
	    	String[] ArrayDatos;
	    	integer intYear = 0, intMonth = 0, intDay = 0, intHora = 0, intMinuto = 0, intSegundo = 0;
	    	ArrayFormato = strFormatoDato.trim().split(' '); // Separa la fecha y la hora por el espacio intermedio
	    	ArrayDatos = strFechaHora.trim().split(' '); // Separa la fecha y la hora por el espacio intermedio
	    	ArrayFormatoFecha = ArrayFormato[0].split(strSeparadorFecha);
	    	ArrayFecha = ArrayDatos[0].split(strSeparadorFecha);
	    	ArrayFormatoHora = ArrayFormato[1].split(':');
	    	ArrayHora  = ArrayDatos[1].split(':');
	    	// Procesa la parte de la Fecha
	    	for(integer i = 0; i < ArrayFormatoFecha.size(); i++){
	    		system.debug('Formato Fecha['+i+']: '+ArrayFormatoFecha[i]);
	    		if(ArrayFormatoFecha[i].EqualsIgnoreCase('yyyy')){
	    			intYear = Integer.valueOf(ArrayFecha[i]);
	    		}else if(ArrayFormatoFecha[i] == 'MM'){
	    			intMonth = Integer.valueOf(ArrayFecha[i]);
	    		}else if(ArrayFormatoFecha[i].EqualsIgnoreCase('dd')){
	    			intDay = Integer.valueOf(ArrayFecha[i]);
	    		}
	    	}
	    	// Procesa la parte de la Hora
	    	for(integer j = 0; j < ArrayFormatoHora.size(); j++){
				if(ArrayFormatoHora[j].EqualsIgnoreCase('hh')){
	    			intHora = Integer.valueOf(ArrayHora[j]);
	    		}else if(ArrayFormatoHora[j] == 'mm'){
	    			intMinuto = Integer.valueOf(ArrayHora[j]);
	    		}else if(ArrayFormatoHora[j].EqualsIgnoreCase('ss')){
	    			intSegundo = Integer.valueOf(ArrayHora[j]);
	    		}
			}
	    	dttFechaHora = Datetime.newInstanceGmt(intYear, intMonth, intDay, intHora, intMinuto, intSegundo);
	    	//system.debug('\n\nParámetros recibidos: strFechaHora= '+strFechaHora+' strSeparadorFecha= '+strSeparadorFecha+'. strFormatoDato: '+strFormatoDato+' Fecha calculada: '+dttFechaHora);
    	}catch(System.exception ex){
			system.debug('\n\n[fn_formatFechaHoraSFDC] Parámetros recibidos: strFechaHora= '+strFechaHora+' strSeparadorFecha= '+strSeparadorFecha+'. strFormatoDato: '+strFormatoDato);
			system.debug('ERROR: [fn_formatFechaHoraSFDC] Se presentó la siguiente excepción: Tipo: '+ex.getTypeName()+'. Mensaje: '+ex.getMessage());
		}
    	return dttFechaHora;
    }
    /***
    * @Author: Javier Tibamoza
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public Static String armadoXML( String nroIdentificacion, string sDeveloper, string packCode, string tipo )
    {
        XmlStreamWriter xmlWriter = new XmlStreamWriter();
        xmlWriter.writeStartDocument('utf-8','1.0');
        xmlWriter.writeStartElement(null, 'WS', null);

        xmlWriter.writeStartElement(null, 'USER', null);
        xmlWriter.writeCharacters('USER');
        xmlWriter.writeEndElement();
            
        xmlWriter.writeStartElement(null, 'PASSWORD', null);
        xmlWriter.writeCharacters('PASSWORD');
        xmlWriter.writeEndElement();
            
        xmlWriter.writeStartElement(null, 'DATEPROCESS', null);
        xmlWriter.writeCharacters(String.valueOf(System.Today()));
        xmlWriter.writeEndElement();
            
        xmlWriter.writeStartElement(null, 'PACKCODE', null);
        xmlWriter.writeCharacters(packCode);
        xmlWriter.writeEndElement();
            
        xmlWriter.writeStartElement(null, 'SEPARATOR', null);
        xmlWriter.writeCharacters('Õ');
        xmlWriter.writeEndElement();
            
        xmlWriter.writeStartElement(null, 'TYPE', null);
        xmlWriter.writeCharacters(tipo);
        xmlWriter.writeEndElement();
            
        xmlWriter.writeStartElement(null, 'COUNT', null);
        xmlWriter.writeCharacters('1');
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'VALUES', null);
            xmlWriter.writeStartElement(null, 'VALUE', null);
            xmlWriter.writeCharacters(nroIdentificacion);
            xmlWriter.writeEndElement();
        xmlWriter.writeEndElement();

        xmlWriter.writeEndElement();

        String tramaDavox = xmlWriter.getXmlString();
        tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
        
        xmlWriter = new XmlStreamWriter();
        return tramaDavox;
    }
}