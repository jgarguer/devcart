/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Infinity W4+5 methods executed by OpportunityTeamMember trigger
 Test Class:    BI_O4_OpportunityTeamMemberMethods_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 30/08/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_OpportunityTeamMemberMethods
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Erase BI_O4_Regional_PresalesTEXT__c and BI_O4_GSE__c fields values if Regional Presales or GSE Principal roles were deleted from the Opportunity Team
    
    In:            oldOTM: OpportunityTeamMember records from Trigger.old (DELETE)
    History:
     
    <Date>                  <Author>                <Change Description>
    30/08/2016              Fernando Arteaga        Initial version
	11/10/2017				Oscar Bartolo			Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateQuotesWithOpptyTeamMembersDelete(List<OpportunityTeamMember> oldOTM)
	{
		try
		{
			 if (BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }

			List<Quote> listQuotes = new List<Quote>();
			Map<Id, List<OpportunityTeamMember>> mapOpptyListOTM = new Map<Id, List<OpportunityTeamMember>>();
			
			for (OpportunityTeamMember otm: oldOTM)
			{
				if (otm.TeamMemberRole == 'Regional Presales' || otm.TeamMemberRole == 'GSE Principal')
				{
			    	if (!mapOpptyListOTM.containsKey(otm.OpportunityId))
						mapOpptyListOTM.put(otm.OpportunityId, new List<OpportunityTeamMember>{otm});
					else
						mapOpptyListOTM.get(otm.OpportunityId).add(otm);					
				}
	    	}
	    	
			for (Quote quote: [SELECT Id, OpportunityId, BI_O4_Regional_PresalesTEXT__c, BI_O4_GSE__c, BI_O4_USER_GSE__c
		    					   FROM Quote WHERE OpportunityId IN :mapOpptyListOTM.keySet()
		    					   AND Status NOT IN ('Proposal ready', 'Accepted by the customer', 'Proposal cancelled')])
	    	{
	    		for (OpportunityTeamMember otm: mapOpptyListOTM.get(quote.OpportunityId))
	    		{
                    if (otm.TeamMemberRole == 'Regional Presales') {
	    				quote.BI_O4_Regional_PresalesTEXT__c = null;
                	} else {
	    				quote.BI_O4_GSE__c = null;
                    	quote.BI_O4_USER_GSE__c = null;
                    }    
	    		}
	    		listQuotes.add(quote);
	    	}
		    	
	    	List<Database.SaveResult> listSR = Database.update(listQuotes, false);
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembersDelete', 'BI_EN O4', exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Fills BI_O4_Regional_PresalesTEXT__c and BI_O4_GSE__c fields with the Regional Presales and GSE Principal roles if they were added in the Opportunity Team
    
    In:            newOTM: OpportunityTeamMember records from Trigger.new
                   oldOTM: OpportunityTeamMember records from Trigger.old 
    History:
     
    <Date>                  <Author>                <Change Description>
    30/08/2016              Fernando Arteaga        Initial version
    07/09/2016              Jose Miguel Fierro      Added support for "Global Solutions Engineer Principal" as an alias for "GSE Principal"
	11/10/2017				Oscar Bartolo			Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public static void updateQuotesWithOpptyTeamMembers(List<OpportunityTeamMember> newOTM, Map<Id, OpportunityTeamMember> mapOldOTM)
    {
    	try
    	{
    		if (BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }
            
	    	Set<Id> setOppId = new Set<Id>();
	    	List<Quote> listQuotes = new List<Quote>();
	    	Set<Id> setUserId = new Set<Id>();
	    	String regionalPresales, gsePrincipal;
            Id gsePrincipalId;
	    	Boolean hasRegionalPresales, hasGsePrincipal, hadRegionalPresales, hadGsePrincipal;
			Map<Id, List<OpportunityTeamMember>> mapOpptyListOTM = new Map<Id, List<OpportunityTeamMember>>();

	    	for (OpportunityTeamMember otm: newOTM)
	    	{
	    		System.debug('otm:' + otm);
	    		if (((otm.TeamMemberRole == 'Regional Presales' || otm.TeamMemberRole == 'GSE Principal' || otm.TeamMemberRole == 'Global Solutions Engineer Principal') && mapOldOTM == null) ||
					(mapOldOTM != null && (mapOldOTM.get(otm.Id).TeamMemberRole != otm.TeamMemberRole && (otm.TeamMemberRole == 'Regional Presales' || otm.TeamMemberRole == 'GSE Principal' || otm.TeamMemberRole == 'Global Solutions Engineer Principal'))) ||
					(mapOldOTM != null && (mapOldOTM.get(otm.Id).TeamMemberRole != otm.TeamMemberRole && (mapOldOTM.get(otm.Id).TeamMemberRole == 'Regional Presales' || mapOldOTM.get(otm.Id).TeamMemberRole == 'GSE Principal' || mapOldOTM.get(otm.Id).TeamMemberRole == 'Global Solutions Engineer Principal'))) ||
					(mapOldOTM != null && (mapOldOTM.get(otm.Id).UserId != otm.UserId && (otm.TeamMemberRole == 'Regional Presales' || otm.TeamMemberRole == 'GSE Principal' || otm.TeamMemberRole == 'Global Solutions Engineer Principal' || mapOldOTM.get(otm.Id).TeamMemberRole == 'Regional Presales' || mapOldOTM.get(otm.Id).TeamMemberRole == 'GSE Principal' || mapOldOTM.get(otm.Id).TeamMemberRole == 'Global Solutions Engineer Principal'))))


				{
					setUserId.add(otm.UserId);
					//setOppId.add(otm.OpportunityId);
					if (!mapOpptyListOTM.containsKey(otm.OpportunityId))
						mapOpptyListOTM.put(otm.OpportunityId, new List<OpportunityTeamMember>{otm});
					else
						mapOpptyListOTM.get(otm.OpportunityId).add(otm);
				}
	    	}
	    	
	    	Map<Id, User> mapUsers = new Map<Id, User>([SELECT Id, Name
	    					 				   			FROM User
	    					 				   			WHERE Id IN :setUserId]);
	    					 				   			
			for (Opportunity opp: [SELECT Id, /*(SELECT Id, UserId, TeamMemberRole
											   FROM OpportunityTeamMembers),*/
											  (SELECT Id, OpportunityId, BI_O4_Regional_PresalesTEXT__c, BI_O4_GSE__c, BI_O4_USER_GSE__c
	    					   				   FROM Quotes
	    					   				   WHERE Status NOT IN ('Proposal ready', 'Accepted by the customer', 'Proposal cancelled')
	    					   				   AND IsSyncing = TRUE
	    					   				   )
								   FROM Opportunity
								   WHERE Id IN :mapOpptyListOTM.keySet()])
								   //WHERE Id IN :setOppId])
			{
				if (opp.Quotes != null)
				{
					regionalPresales = null;
					gsePrincipal = null;
                    gsePrincipalId = null;
					hasRegionalPresales = hasGsePrincipal = hadRegionalPresales = hadGsePrincipal = false;

					//for (OpportunityTeamMember otm: opp.OpportunityTeamMembers)
					for (OpportunityTeamMember otm: mapOpptyListOTM.get(opp.Id))
					{
						System.debug('otm.TeamMemberRole: ' + otm.TeamMemberRole);
						if (otm.TeamMemberRole == 'Regional Presales'){
							if(mapUsers.containsKey(otm.UserId)){
								regionalPresales = mapUsers.get(otm.UserId).Name;
							}
							hasRegionalPresales = true;
						}
						else if (otm.TeamMemberRole == 'GSE Principal' || otm.TeamMemberRole == 'Global Solutions Engineer Principal'){
							if(mapUsers.containsKey(otm.UserId)) {
								gsePrincipal = mapUsers.get(otm.UserId).Name;
                                gsePrincipalId = otm.UserId;
							}
							hasGsePrincipal = true;
						}
						else {
							if (mapOldOTM.get(otm.Id).TeamMemberRole == 'GSE Principal' || mapOldOTM.get(otm.Id).TeamMemberRole == 'Global Solutions Engineer Principal')
								hadGsePrincipal = true;

							if (mapOldOTM.get(otm.Id).TeamMemberRole == 'Regional Presales')
								hadRegionalPresales = true;
						}
						System.debug('hasRegionalPresales: ' + hasRegionalPresales + ' | hasGsePrincipal: ' + hasGsePrincipal + ' | hadRegionalPresales: ' + hadRegionalPresales + ' | hadGsePrincipal: ' + hadGsePrincipal);
					}
					
					for (Quote quote: opp.Quotes)
					{
						if((hasRegionalPresales && regionalPresales != null)){
							quote.BI_O4_Regional_PresalesTEXT__c = regionalPresales;
						}
						if((hasGsePrincipal && gsePrincipal != null)) {
							quote.BI_O4_GSE__c = gsePrincipal;
                            quote.BI_O4_USER_GSE__c = gsePrincipalId;
						}
						if (hadGsePrincipal && !hasGsePrincipal) {
							quote.BI_O4_GSE__c = null;
                            quote.BI_O4_USER_GSE__c = null;
						}
						if (hadRegionalPresales && !hasRegionalPresales) {
							quote.BI_O4_Regional_PresalesTEXT__c = null;
						}
						listQuotes.add(quote);
					}
				}
			} 

	    	List<Database.SaveResult> listSR = Database.update(listQuotes, false);
    	}
    	catch (Exception exc)
    	{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembers', 'BI_EN O4', exc, 'Trigger');
    	}
    }
}