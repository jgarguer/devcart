@isTest
private class BI_NEMobileOptyConfirmControllerTest {
    
     @isTest(SeeAllData=true) static void myUnitTest() {
       try{
            BI_MarketController mc0 = new BI_MarketController(null);
        }catch(Exception e){}
        String compareUrl;
        Boolean lvvar;
        String                                   redSum;             
        Boolean                                  mobile;      
        String                                   catName;
        String                                   psess;
        

       
        NE.NewConfigurationController ncc;
          ncc= new NE.NewConfigurationController();
         try{
             BI_NEMobileOptyConfirmController nmocc;
             try{
                nmocc = new BI_NEMobileOptyConfirmController();
             }catch(Exception e){}
             
             Datetime myT = datetime.newInstance(2012, 5, 1, 12, 30, 2);
            
            NE__Catalog_Header__c ch=new NE__Catalog_Header__c(NE__Name__c='myUnitTest');
            insert ch;
            System.debug('insert ch1');//
            
            NE__Catalog__c ct = new NE__Catalog__c(Name='myUnitTest', NE__StartDate__c=myT, NE__Catalog_Header__c=ch.id);
            insert ct;
            System.debug('insert ct1');//
            system.debug(System.LoggingLevel.WARN,'MRIG ct.Id: '+ct.Id);
            
            NE__Catalog_Category__c categoria = new NE__Catalog_Category__c(Name = ct.Name,NE__CatalogId__c = ct.Id);
            insert categoria;
            System.debug('insert categoria1');//
            NE__Commercial_Model__c commodel = new NE__Commercial_Model__c(Name = ct.Name, NE__Catalog_Header__c = ct.NE__Catalog_Header__c,
            NE__ProgramName__c = NE.JS_RemoteMethods.SFDC_HTMLENCODE(ct.Name.replaceAll(' ','_')));
            insert commodel;
            System.debug('insert commodel1');//
            NE__Catalog_Category__c cc=new NE__Catalog_Category__c(Name='testcc',NE__CatalogId__c=ct.id, NE__Parent_Category_Name__c = categoria.Id);
            insert cc;
            System.debug('insert cc1');//
            NE__Product__c prod1=new NE__Product__c(Name='prod1');
            insert prod1;
            NE__Product__c prod2=new NE__Product__c(Name='prod2');
            insert prod2;
            NE__Product__c prod3=new NE__Product__c(Name='prod3');
            insert prod3;
            NE__Product__c prod4=new NE__Product__c(Name='prod4');
            insert prod4;       
            NE__Product__c prod5=new NE__Product__c(Name='prod5');
            insert prod5;
            System.debug('insert prods1');//
            List<NE__Catalog_Item__c> lisct = new List<NE__Catalog_Item__c>();  
            NE__Catalog_Item__c item= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item;
            lisct.add(item);
            NE__Catalog_Item__c item2= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item2;
            lisct.add(item2);
            NE__Catalog_Item__c item3= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item3;
            lisct.add(item3);
            NE__Catalog_Item__c item4= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item4;
            lisct.add(item4);
            NE__Catalog_Item__c item5= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod5.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item5;
            lisct.add(item5);
            System.debug('insert items1');//
            NE__Promotion__c prom= new NE__Promotion__c (Name = 'Prova', NE__Start_Date__c = myT, NE__Catalog_Id__c = ct.id);
            insert prom;
            System.debug('insert prom1');//
            // 20/09/2017        Angel F. Santaliestra Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
            Account acc = new Account (Name = 'Account Classe di Test',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
            // 20/09/2017        Angel F. Santaliestra
            insert acc;
            System.debug('insert acc1');//
            NE__Contract_Header__c contractHeader = new NE__Contract_Header__c(NE__name__c='TestCCloneHeader');
            insert contractHeader;
            System.debug('insert contractHeader1');//
            NE__Contract__c contractToUse   =   new NE__Contract__c(NE__Contract_Header__c = contractHeader.id, NE__Version__c = 1, NE__Start_Date__c = DateTime.Now()-5000, NE__Status__c = 'Active');
            insert contractToUse;
            System.debug('insert contractToUse1');//
            NE__Contract_Line_Item__c cLine =   new NE__Contract_Line_Item__c(NE__Base_OneTime_Fee__c = 10, NE__Base_Recurring_Charge__c = 10, NE__Commercial_Product__c = prod5.id, NE__Contract__c = contractToUse.id);
            insert cLine;
            System.debug('insert cLine1');//
            NE__Contract_Account_Association__c cAssoc  =   new NE__Contract_Account_Association__c(NE__Contract_Header__c = contractHeader.id, NE__Account__c = acc.id);
            insert cAssoc;
            System.debug('insert cAssoc1');//
            
            List<Account> accAux=[SELECT Id,Name From Account where Name='Account Classe di Test']; 
             System.debug('select id: '+accAux[0].Id);
             System.debug('select name: '+accAux[0].Name);
            
            RecordType recordTypeId= [SELECT CreatedById,DeveloperName,Id,Name,NamespacePrefix,SobjectType 
            FROM RecordType 
            WHERE (SobjectType=:'NE__Order__c' or SobjectType=:'Order__c') AND Name='Opty' LIMIT 1];

            String probab = Opportunity.BI_Probabilidad_de_exito__c.getDescribe().getPicklistValues().get(0).getValue();
            String curr= Opportunity.CurrencyIsoCode.getDescribe().getPicklistValues().get(0).getValue();
            String stage= Opportunity.stageName.getDescribe().getPicklistValues().get(0).getValue();
            String country= Opportunity.BI_Country__c.getDescribe().getPicklistValues().get(0).getValue();

            Opportunity optyTest = new Opportunity(Name='OpportunityTest',AccountId=accAux[0].Id, CloseDate =System.today(),StageName='F6 - Prospecting', RecordTypeId='012w0000000hzMWAAY' ,BI_Probabilidad_de_exito__c=probab,CurrencyIsoCode=curr,BI_Country__c=country,  BI_Duracion_del_contrato_Meses__c=12,BI_Plazo_estimado_de_provision_dias__c=12);
            insert optyTest;
            
            System.debug('insert optyTest1');//
            
            NE__Order__c ord = new NE__Order__c (NE__AccountId__c = acc.Id, NE__OpportunityId__c=optyTest.Id,  NE__OptyId__c=optyTest.Id, RecordType=recordTypeId, NE__Contract_Account__c=cAssoc.id, NE__Contract_Header__c=contractHeader.id, NE__One_Time_Fee_Total__c=100,NE__Recurring_Charge_Total__c=100,NE__TotalRecurringFrequency__c='Monthly');
 
            insert ord;
            System.debug('insert ord1');//
             
             PageReference pageRef = Page.BI_NEMobileOptyConfirm;
             try{
                  Test.setCurrentPage(pageRef);
                  pageRef.getParameters().put('accId',acc.id);
                 pageRef.getParameters().put('orderId',ord.id);
                 
                 nmocc = new BI_NEMobileOptyConfirmController();
                 nmocc.configurationCreated=ord;
                 nmocc.selectedAccount=acc;
                 nmocc.saveAndCreateOpty();
             }catch(Exception e){}
             
             try{
                 pageRef.getParameters().put('accType','DUMMY');
                 nmocc = new BI_NEMobileOptyConfirmController();
                 nmocc.saveAndCreateOpty();
             }catch(Exception e){}
         }
         catch(Exception er){}
     }
    
    static testMethod void myUnitTest2() {
        try{
             BI_NEMobileOptyConfirmController nmocc;
             try{
                nmocc = new BI_NEMobileOptyConfirmController();
             }catch(Exception e){}
             
             Datetime myT = datetime.newInstance(2012, 5, 1, 12, 30, 2);
            
            NE__Catalog_Header__c ch=new NE__Catalog_Header__c(NE__Name__c='myUnitTest');
            insert ch;
            System.debug('insert ch2');//
            NE__Catalog__c ct = new NE__Catalog__c(Name='myUnitTest', NE__StartDate__c=myT, NE__Catalog_Header__c=ch.id);
            insert ct;
            System.debug('insert ct2');//
            system.debug(System.LoggingLevel.WARN,'MRIG ct.Id: '+ct.Id);
            
            NE__Catalog_Category__c categoria = new NE__Catalog_Category__c(Name = ct.Name,NE__CatalogId__c = ct.Id);
            insert categoria;
            
            NE__Commercial_Model__c commodel = new NE__Commercial_Model__c(Name = ct.Name, NE__Catalog_Header__c = ct.NE__Catalog_Header__c,
            NE__ProgramName__c = NE.JS_RemoteMethods.SFDC_HTMLENCODE(ct.Name.replaceAll(' ','_')));
            insert commodel;
            
            NE__Catalog_Category__c cc=new NE__Catalog_Category__c(Name='testcc',NE__CatalogId__c=ct.id, NE__Parent_Category_Name__c = categoria.Id);
            insert cc;
            
            NE__Product__c prod1=new NE__Product__c(Name='prod1');
            insert prod1;
            NE__Product__c prod2=new NE__Product__c(Name='prod2');
            insert prod2;
            NE__Product__c prod3=new NE__Product__c(Name='prod3');
            insert prod3;
            NE__Product__c prod4=new NE__Product__c(Name='prod4');
            insert prod4;       
            NE__Product__c prod5=new NE__Product__c(Name='prod5');
            insert prod5;
            
            List<NE__Catalog_Item__c> lisct = new List<NE__Catalog_Item__c>();  
            NE__Catalog_Item__c item= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item;
            lisct.add(item);
            NE__Catalog_Item__c item2= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item2;
            lisct.add(item2);
            NE__Catalog_Item__c item3= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item3;
            lisct.add(item3);
            NE__Catalog_Item__c item4= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item4;
            lisct.add(item4);
            NE__Catalog_Item__c item5= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod5.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item5;
            lisct.add(item5);
            
            NE__Promotion__c prom= new NE__Promotion__c (Name = 'Prova', NE__Start_Date__c = myT, NE__Catalog_Id__c = ct.id);
            insert prom;
            // 20/09/2017        Angel F. Santaliestra Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
            Account acc = new Account (Name = 'Account Classe di Test',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
            // 20/09/2017        Angel F. Santaliestra
            insert acc;
            
            NE__Contract_Header__c contractHeader = new NE__Contract_Header__c(NE__name__c='TestCCloneHeader');
            insert contractHeader;
            NE__Contract__c contractToUse   =   new NE__Contract__c(NE__Contract_Header__c = contractHeader.id, NE__Version__c = 1, NE__Start_Date__c = DateTime.Now()-5000, NE__Status__c = 'Active');
            insert contractToUse;
            NE__Contract_Line_Item__c cLine =   new NE__Contract_Line_Item__c(NE__Base_OneTime_Fee__c = 10, NE__Base_Recurring_Charge__c = 10, NE__Commercial_Product__c = prod5.id, NE__Contract__c = contractToUse.id);
            insert cLine;
            NE__Contract_Account_Association__c cAssoc  =   new NE__Contract_Account_Association__c(NE__Contract_Header__c = contractHeader.id, NE__Account__c = acc.id);
            insert cAssoc;
 
             List<Account> accAux=[SELECT Id,Name From Account where Name='Account Classe di Test']; 
             System.debug('select id: '+accAux[0].Id);
             System.debug('select name: '+accAux[0].Name);
            
            RecordType recordTypeId= [SELECT CreatedById,DeveloperName,Id,Name,NamespacePrefix,SobjectType 
            FROM RecordType 
            WHERE (SobjectType=:'NE__Order__c' or SobjectType=:'Order__c') AND Name='Opty' LIMIT 1];
            
            String probab = Opportunity.BI_Probabilidad_de_exito__c.getDescribe().getPicklistValues().get(0).getValue();
            String curr= Opportunity.CurrencyIsoCode.getDescribe().getPicklistValues().get(0).getValue();
            String stage= Opportunity.stageName.getDescribe().getPicklistValues().get(0).getValue();
            String country= Opportunity.BI_Country__c.getDescribe().getPicklistValues().get(0).getValue();
            Opportunity optyTest = new Opportunity(Name='OpportunityTest',AccountId=accAux[0].Id, CloseDate =System.today(),StageName='F6 - Prospecting',RecordTypeId='012w0000000hzMWAAY', BI_Probabilidad_de_exito__c=probab,CurrencyIsoCode=curr,BI_Country__c=country,  BI_Duracion_del_contrato_Meses__c=12,BI_Plazo_estimado_de_provision_dias__c=12);
            insert optyTest;
                  
            
            NE__Order__c ord = new NE__Order__c (NE__AccountId__c = acc.Id,  RecordType=recordTypeId,NE__OpportunityId__c=optyTest.Id, NE__OptyId__c=optyTest.Id, NE__Contract_Account__c=cAssoc.id, NE__Contract_Header__c=contractHeader.id, NE__One_Time_Fee_Total__c=100,NE__Recurring_Charge_Total__c=100,NE__TotalRecurringFrequency__c='Monthly');
            insert ord;
             System.debug('insert ord');
             PageReference pageRef = Page.BI_NEMobileOptyConfirm;
             try{
                  Test.setCurrentPage(pageRef);
                  pageRef.getParameters().put('accId',acc.id);
                 pageRef.getParameters().put('orderId',ord.id);
                 pageRef.getParameters().put('accType','DUMMY');
                 
                 nmocc = new BI_NEMobileOptyConfirmController();
                 nmocc.configurationCreated=ord;
                 nmocc.selectedAccount=acc;
                 nmocc.saveAndCreateOpty();
             }catch(Exception e){
                 System.debug('excep pageref'+e.getMessage());//
             }
         
         }
         catch(Exception er){             
             System.debug('excep myunittest'+er.getMessage());//
         }
     }

}