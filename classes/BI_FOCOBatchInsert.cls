global class BI_FOCOBatchInsert implements Database.Batchable<sObject>, Database.Stateful {
    
    private final String country;
    global Integer failed;
    //global String errMsg;
    private String currencyCode;
    private Map<Date, Id> allPeriodoMap;
    private Map<String, Id> allRamaMap;
    private Map<String, BI_FOCO_Pais__c> mcs;
    private String errorLogId;
    private Integer numberOfStageRecords;
    private Integer numberOfMasterRecords;

    global BI_FOCOBatchInsert(String c) {
        numberOfStageRecords = 0;
        numberOfMasterRecords = 0;
        failed=0;
        this.country = c;
        getErrorLogId();
        mcs =  BI_FOCO_Pais__c.getAll();
        currencyCode = mcs.get(country).Currency_ISO_Code__c;

        List<BI_Periodo__c> allPeriodoList = [SELECT Id, BI_Fecha_Inicio_Periodo__c from BI_Periodo__c];
        if(allPeriodoList == null || allPeriodoList.size() ==0 ){
            throw new BI_FOCO_Exception('No Periodos available in BI_Periodo__c for processing');
        }
        allPeriodoMap = new Map<Date, Id>();
        for(BI_Periodo__c r: allPeriodoList){
            allPeriodoMap.put(r.BI_Fecha_Inicio_Periodo__c, r.Id);
        }

        List<BI_Rama__c> allRamaList = [SELECT Id, BI_Nombre__c from BI_Rama__c];

        if(allRamaList == null || allRamaList.size() ==0 ){
            throw new BI_FOCO_Exception('No Ramas available in BI_Rama__c for processing');
        }

        allRamaMap = new Map<String, Id>();
        for(BI_Rama__c r: allRamaList){
            allRamaMap.put(r.BI_Nombre__c, r.Id);
        }
        System.debug('-------------------------------------------BI_FOCOBatchInsert.constructor(): Country: '+country);
    }

    /*
    global Iterable<sObject> start(Database.BatchableContext BC) {
        System.debug('-------------------------------------------BI_FOCOBatchInsert.start()');

        List<BI_Registros_Datos_FOCO_Stage__c> sList = [SELECT Id, BI_Cliente__c, BI_Monto__c, BI_Country__c ,BI_Periodo__c ,BI_Rama_Local__c , BI_Tipo__c from BI_Registros_Datos_FOCO_Stage__c where BI_Country__c =:country];
        System.debug('-------------------------------------------BI_FOCOBatchInsert.start - Going to Process '+sList.size() +' records from Stage for country: '+country);
        return sList;
    }
    */

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, BI_Cliente__c, BI_Monto__c, BI_Country__c ,BI_Periodo__c ,BI_Rama_Local__c , BI_Tipo__c from BI_Registros_Datos_FOCO_Stage__c where BI_Country__c =:country');
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
 //PLL_PickList_Refactorizacion_Necesaria

        List<BI_Registro_Datos_FOCO__c> fList = new List<BI_Registro_Datos_FOCO__c> ();
        List<BI_Registro_Datos_FOCO_Error_Log__c> eList = new List<BI_Registro_Datos_FOCO_Error_Log__c>();
        Map<Id, String> clientesMap = new Map<Id, String>();
        Map<Id, String> periodosMap = new Map<Id, String>();
        Map<Id, String> ramasMap = new Map<Id, String>();

        System.debug('-------------------------------------------BI_FOCOBatchInsert.execute(): Start batch');

        for(sObject o : scope) {
            numberOfStageRecords++;

            BI_Registros_Datos_FOCO_Stage__c s = (BI_Registros_Datos_FOCO_Stage__c)o;
            BI_Registro_Datos_FOCO_Error_Log__c e;

            //Boolean isGood = true;
            Account[] acc = [SELECT Id from Account where BI_Validador_Fiscal__c=:s.BI_Cliente__c];

            if(acc.size() > 0 && acc[0] != null) {
                Id periodoId;
                Id ramaId;
                Decimal amt;
                Date periodo;

                clientesMap.put(acc[0].Id, s.BI_Cliente__c);

                // Validate periodo
                try{
                    periodo = Date.parse(s.BI_Periodo__c);
                }
                catch(Exception ex) {
                    failed++;

                    e = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Cliente__c = s.BI_Cliente__c,
                                                                BI_Descripcion__c = 'El Periodo "' + s.BI_Periodo__c + '" no se encuentra registrado en BIEN',
                                                                BI_Identificador_Log__c = errorLogId,
                                                                BI_Monto__c = s.BI_Monto__c,
                                                                BI_Pais__c = s.BI_Country__c,
                                                                BI_Periodo__c = s.BI_Periodo__c,
                                                                BI_Rama_Local__c = s.BI_Rama_Local__c,
                                                                BI_Tipo__c = s.BI_Tipo__c);
                    eList.add(e);
                    continue;
                }

                if(periodo != null && allPeriodoMap.get(periodo.toStartOfMonth()) != null){
                    periodoId = allPeriodoMap.get(periodo.toStartOfMonth());
                    periodosMap.put(periodoId, s.BI_Periodo__c);
                }
                else {
                    failed++;

                    e = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Cliente__c = s.BI_Cliente__c,
                                                                BI_Descripcion__c = 'El Periodo "' + s.BI_Periodo__c + '" no se encuentra registrado en BIEN',
                                                                BI_Identificador_Log__c = errorLogId,
                                                                BI_Monto__c = s.BI_Monto__c,
                                                                BI_Pais__c = s.BI_Country__c,
                                                                BI_Periodo__c = s.BI_Periodo__c,
                                                                BI_Rama_Local__c = s.BI_Rama_Local__c,
                                                                BI_Tipo__c = s.BI_Tipo__c);
                    eList.add(e);
                    continue;
                }

                // Validate rama
                if(s.BI_Rama_Local__c != null && allRamaMap.get(s.BI_Rama_Local__c) != null) {
                    ramaId = allRamaMap.get(s.BI_Rama_Local__c);
                    ramasMap.put(ramaId, s.BI_Rama_Local__c);
                }
                else {
                    failed++;

                    e = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Cliente__c = s.BI_Cliente__c,
                                                                BI_Descripcion__c = 'La Rama "' + s.BI_Rama_Local__c + '" no se encuentra registrada en BIEN',
                                                                BI_Identificador_Log__c = errorLogId,
                                                                BI_Monto__c = s.BI_Monto__c,
                                                                BI_Pais__c = s.BI_Country__c,
                                                                BI_Periodo__c = s.BI_Periodo__c,
                                                                BI_Rama_Local__c = s.BI_Rama_Local__c,
                                                                BI_Tipo__c = s.BI_Tipo__c);
                    eList.add(e);
                    continue;
                }

                // Validate amount
                try {
                    amt = Decimal.valueOf(s.BI_Monto__c);
                }
                catch(TypeException te) {
                    //isGood = false;
                    failed++;
                    //errMsg = errMsg+'Invalid value for Monto: '+s.BI_Monto__c;
                    e = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Cliente__c = s.BI_Cliente__c,
                                                                BI_Descripcion__c = 'El monto "' + s.BI_Monto__c + '" no es válido',
                                                                BI_Identificador_Log__c = errorLogId,
                                                                BI_Monto__c = s.BI_Monto__c,
                                                                BI_Pais__c = s.BI_Country__c,
                                                                BI_Periodo__c = s.BI_Periodo__c,
                                                                BI_Rama_Local__c = s.BI_Rama_Local__c,
                                                                BI_Tipo__c = s.BI_Tipo__c);
                    eList.add(e);
                    continue;
                }


                //if(isGood) {
                BI_Registro_Datos_FOCO__c f = new BI_Registro_Datos_FOCO__c (BI_Cliente__c = acc[0].Id,
                                                                                BI_Monto__c = amt, 
                                                                                CurrencyIsoCode = currencyCode,
                                                                                BI_Periodo__c = periodoId, 
                                                                                BI_Rama__c = ramaId, 
                                                                                BI_Country__c = s.BI_Country__c, //
                                                                                BI_Tipo__c = s.BI_Tipo__c);
                fList.add(f);
                //}
            }
            else {
                failed++;
                //errMsg = errMsg+ 'No matching Accounts exist for  |' + s.BI_Cliente__c + '| \n';
                e = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Cliente__c = s.BI_Cliente__c,
                                                            BI_Descripcion__c = 'No existe un cliente registrado con el Identificador Fiscal "' + s.BI_Cliente__c + '"',
                                                            BI_Identificador_Log__c = errorLogId,
                                                            BI_Monto__c = s.BI_Monto__c,
                                                            BI_Pais__c = s.BI_Country__c, // Fix bugs from picklist Code refactoring. eHelp 01423701. 
                                                            BI_Periodo__c = s.BI_Periodo__c,
                                                            BI_Rama_Local__c = s.BI_Rama_Local__c,
                                                            BI_Tipo__c = s.BI_Tipo__c);
                eList.add(e);
            }
        }


        List<Database.SaveResult> dsrs;

        try {
            dsrs = Database.insert(fList, false);
        }
        catch(DmlException dmle) {
            throw new BI_FOCO_Exception('Error while batch inserting FOCO records', dmle);
        }
        catch(Exception e) {
            throw new BI_FOCO_Exception('Error while batch inserting FOCO records', e);
        }

        //for(Database.SaveResult dsr : dsrs) {
        for(Integer i = 0; i < dsrs.size(); i++) {  
            System.debug('-------------------------------------------BI_FOCOBatchInsert.Sucess: ' + dsrs[i].isSuccess());
            BI_Registro_Datos_FOCO_Error_Log__c r;

            //if(!dsr.isSuccess()) {
            if(!dsrs[i].isSuccess()) {  
                failed++;

                r = new BI_Registro_Datos_FOCO_Error_Log__c(BI_Cliente__c = clientesMap.get(fList[i].BI_Cliente__c),
                                                            BI_Descripcion__c = dsrs[i].getErrors()[0].getStatusCode() + ': ' + dsrs[i].getErrors()[0].getMessage(),
                                                            BI_Identificador_Log__c = errorLogId,
                                                            BI_Monto__c = String.valueOf(fList[i].BI_Monto__c),
                                                            BI_Pais__c = country,
                                                            BI_Periodo__c = periodosMap.get(fList[i].BI_Periodo__c),
                                                            BI_Rama_Local__c = ramasMap.get(fList[i].BI_Rama__c),
                                                            BI_Tipo__c = fList[i].BI_Tipo__c);
                eList.add(r);
                //
                //for(Database.Error err : dsr.getErrors()) {
                //  errMsg = errMsg + err.getStatusCode() + ': ' + err.getMessage() + '\n';
                //}
                
            }
            else {
                numberOfMasterRecords++;
                System.debug('-------------------------------------------BI_FOCOBatchInsert.Number of Master Records: ' + numberOfMasterRecords);
            }           
        }

        // Insert list of errors
        if(eList.size() > 0) {
            try{
                Database.insert(eList, false);
            }
            catch(DmlException dmle){
                throw new BI_FOCO_Exception('Error while batch inserting FOCO Error Log records', dmle);
            }
        }
    
        System.debug('-------------------------------------------BI_FOCOBatchInsert.execute(): End batch');
        
    }
    
    
    global void finish(Database.BatchableContext BC) {
         // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        /*
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        FROM AsyncApexJob WHERE Id =
        :BC.getJobId()];


        //query and verify main table
        Integer countStage = [select count() from BI_Registros_Datos_FOCO_Stage__c where BI_Country__c=:country];
        Integer countMain = [select count() from BI_Registro_Datos_FOCO__c where BI_Country__c=:country];


        if( countMain > 0){
            //Update FOCO table with opportunities based on the just concluded data load
            BI_FOCO_BatchUpdFOCOFromOppty batchUp = new BI_FOCO_BatchUpdFOCOFromOppty(country);
            Id updBatchId = Database.executeBatch(batchUp, (Integer)mcs.get(country).Batch_Size__c);
            //delete from stage table
            BI_FOCOSBatchDelete batchStageDel = new BI_FOCOSBatchDelete(country, 'BI_Registros_Datos_FOCO_Stage__c');
            Id delStageBatchId = Database.executeBatch(batchStageDel, (Integer)mcs.get(country).Batch_Size__c);
            if(countMain < countStage){
                BI_FOCOScheduler.notifyByEmail('FOCO main table load from stage failed for '+failed+' records for :'+country, '', 0, '' , country);
            }
        }
        */

        //BI_FOCOBatchInsert.getCount(BC.getJobId(), country, failed, mcs);

        if(numberOfMasterRecords > 0) {
            //Update FOCO table with opportunities based on the just concluded data load
            //BI_FOCO_BatchUpdFOCOFromOppty batchUp = new BI_FOCO_BatchUpdFOCOFromOppty(country);
            //Id updBatchId = Database.executeBatch(batchUp, (Integer)mcs.get(country).Batch_Size__c);
            
            // Calculate the range of dates for the inserted FOCO data
            BI_FOCO_CountryDateHelperBatch batchDateHelper = new BI_FOCO_CountryDateHelperBatch(country);
            Id dateHelperBatchId = Database.executeBatch(batchDateHelper, (Integer)mcs.get(country).Batch_Size__c);

            //if(countMain < countStage){
            
        }

        //delete from stage table
        BI_FOCOSBatchDelete batchStageDel = new BI_FOCOSBatchDelete(country, 'BI_Registros_Datos_FOCO_Stage__c');
        Id delStageBatchId = Database.executeBatch(batchStageDel, (Integer)mcs.get(country).Batch_Size__c);

        if(failed > 0) {    
            notifyByEmail('Ha fallado el procesamiento de '+ failed +' registro(s) durante la carga de datos de FOCO para ' + country + '. Revise el objeto "Registro Datos FOCO Error Log" para obtener más detalles.', country);
        }
    }


    private void notifyByEmail(String eMsg, String c) {
        String message = eMsg;
        String theTime = '' + System.now();

        String subject = String.format('Telefónica BI_EN: Falla en Carga de Datos de FOCO {0}', new List<String>{ c });
        String body = String.format('Mensaje: {0}\nHora: {1}\n', new List<String>{ message, theTime });

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        Map<String, BI_FOCO_Pais__c> mcs = BI_FOCO_Pais__c.getAll();
        String toList = mcs.get(c).Notify_Email_List__c;
        if(toList != null){

            String[] toAddresses = toList.split(',', -1) ;
            mail.setToAddresses(toAddresses);
            mail.setSubject(subject);
            mail.setUseSignature(false);
            mail.setPlainTextBody(body);

            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); //Check límits and target to userId 
            BI_SendMails_Helper.sendMails(new Messaging.SingleEmailMessage[] { mail }, false, 'FOCO');
        }
    }

    private void getErrorLogId() {
        String countryISOCode;
        String day;
        String month;
        String year;
        String hour;
        String minute;
        String second;

        countryISOCode = BI_FOCOUtil.getCountryISOCode(country);

        if(System.now().day() < 10)
            day = '0' + System.now().day();
        else
            day = String.valueOf(System.now().day());

        if(System.now().month() < 10)
            month = '0' + System.now().month();
        else
            month = String.valueOf(System.now().month());

        if(System.now().hour() < 10)
            hour = '0' + System.now().hour();
        else
            hour = String.valueOf(System.now().hour());

        if(System.now().minute() < 10)
            minute = '0' + System.now().minute();
        else
            minute = String.valueOf(System.now().minute());

        if(System.now().second() < 10)
            second = '0' + System.now().second();
        else
            second = String.valueOf(System.now().second());

        year = String.valueOf(System.now().year());

        errorLogId = countryISOCode + ' ' + day + '/' + month + '/' + year + ' ' + hour + ':' + minute+ ':' + second;
    }
    //@RemoteAction
    //@ReadOnly
    /*
    public static void getCount(string bcID, string country, integer failed, Map<String, BI_FOCO_Pais__c> mcs) {
        //AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
        //TotalJobItems, CreatedBy.Email
        //FROM AsyncApexJob WHERE Id =
        //:bcID];

        //query and verify main table
        //Integer countStage = [select count() from BI_Registros_Datos_FOCO_Stage__c where BI_Country__c=:country];
        //Integer countMain = [select count() from BI_Registro_Datos_FOCO__c where BI_Country__c=:country];


        //if( countMain > 0){
        if(numberOfMasterRecords > 0) {
            //Update FOCO table with opportunities based on the just concluded data load
            BI_FOCO_BatchUpdFOCOFromOppty batchUp = new BI_FOCO_BatchUpdFOCOFromOppty(country);
            Id updBatchId = Database.executeBatch(batchUp, (Integer)mcs.get(country).Batch_Size__c);
            //delete from stage table
            BI_FOCOSBatchDelete batchStageDel = new BI_FOCOSBatchDelete(country, 'BI_Registros_Datos_FOCO_Stage__c');
            Id delStageBatchId = Database.executeBatch(batchStageDel, (Integer)mcs.get(country).Batch_Size__c);
            if(countMain < countStage){
                BI_FOCOScheduler.notifyByEmail('FOCO main table load from stage failed for '+failed+' records for :'+country, '', 0, '' , country);
            }
        }

    }
    */

}