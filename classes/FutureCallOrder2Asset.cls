global class FutureCallOrder2Asset 
{
    global FutureCallOrder2Asset() {        
    }

    global Static String callOrder2AssetMethod(String confId, String caseId) 
    {
        system.debug('[class.FutureCallOrder2Asset.callOrder2Asset] Call future with order id: ' + confId+ ' caseid: ' + caseId);
        TGS_CallRodWs.inFutureContext   = true;
        TGS_CallRodWs.inFutureContextCI = true;
        TGS_CallRodWs.inFutureContextCIAtt = true;
        
        system.debug('[class.FutureCallOrder2Asset.callOrder2Asset] TGS_CallRodWs.inFutureContext: ' + TGS_CallRodWs.inFutureContext);
        //Call setgenerateCase in order to avoid to create more than 1 Case
        NETriggerHelper.setgenerateCase(false);
        String assetGeneratedId = NE.JS_RemoteMethods.order2asset(confId);
        NETriggerHelper.setgenerateCase(true);

        NETriggerHelper.setTriggerFired('CallOrder2Asset');
        Case updCase            =   [SELECT Asset__c, Order__r.TGS_SIGMA_ID__c, Type FROM Case WHERE id =: caseId];
        updCase.Asset__c        =   assetGeneratedId;
        update updCase;
        
        NE__Order__c assetGenerated             =   [SELECT Id, Case__c, NE__Asset__c, NE__OrderStatus__c
                                                            //TGS_RFB_date__c, TGS_RFS_date__c Deloitte (GAB) 17/04/2014 - Changed field for a formula one
                                                     FROM NE__Order__c 
                                                     WHERE Id = :assetGeneratedId];
        assetGenerated.Case__c                  =   updCase.Id; 
        //assetGenerated.TGS_RFB_date__c          =   updCase.TGS_RFB_date__c; Deloitte (GAB) 17/04/2014 - Changed field for a formula one
        //assetGenerated.TGS_RFS_date__c          =   updCase.TGS_RFS_date__c; Deloitte (GAB) 17/04/2014 - Changed field for a formula one
        
        list<NE__OrderItem__c> assetItemGenerated =		[SELECT NE__Status__c, TGS_Service_status__c, NE__Action__c, NE__AssetItemEnterpriseId__c, 
                                                            (SELECT Id, NE__AttrEnterpriseId__c
                                                            FROM NE__Order_Item_Attributes__r)
                                                     	FROM NE__OrderItem__c
                                                  		WHERE NE__OrderId__c =: assetGenerated.Id];
        
        NE__Order__c config = [SELECT NE__ServAccId__c, NE__BillAccId__c, NE__AssetEnterpriseId__c 
                               FROM NE__Order__c 
                               WHERE Id =: confId];
        
        map<String,NE__OrderItem__c> mapOrdItems = new map<String,NE__OrderItem__c>();
        map<String,NE__Order_Item_Attribute__c> mapOrdItAttr = new map<String,NE__Order_Item_Attribute__c>();
        
        if(config.NE__AssetEnterpriseId__c == null)
        {
            mapOrdItems = new map<String,NE__OrderItem__c>([SELECT Id, NE__AssetItemEnterpriseId__c FROM NE__OrderItem__c WHERE NE__OrderId__c =: confId]);
        	mapOrdItAttr = new map<String,NE__Order_Item_Attribute__c>([SELECT Id, NE__AttrEnterpriseId__c FROM NE__Order_Item_Attribute__c WHERE NE__Order_Item__c IN: mapOrdItems.keySet()]);
        }
        
        list<NE__OrderItem__c> confItemsToUpd   			=   new list<NE__OrderItem__c>();
        list<NE__Order_Item_Attribute__c> confItemAttrToUpd	=	new list<NE__Order_Item_Attribute__c>();
        /* Send Integration with in ROD in modification and termination 
        Set<Id> confitemsIds = new Set<Id>();
        for(NE__OrderItem__c ci:assetItemGenerated){
            
            confitemsIds.add(ci.Id);
        }
        TGS_CallRoDWs.invokeWebServiceRODOrderItem2(confitemsIds,null, false); 
        */
        
        for(NE__OrderItem__c assetItem : assetItemGenerated)
        {
            if(updCase.Type == 'Disconnect')
            {
                assetItem.NE__Status__c = 'Disconnected';
                assetItem.TGS_Service_status__c = 'Deleted';
            }
            else if(updCase.Type == 'Change')
            {
                if(assetItem.NE__Status__c == 'Active')
                {
                    assetItem.TGS_Service_status__c = 'Deployed';
                }
                else
                {
                    assetItem.NE__Status__c = 'Disconnected';
                    assetItem.NE__Action__c = 'Remove';
                    assetItem.TGS_Service_status__c = 'Deleted';
                }
            }
            else
            {
                assetItem.NE__Status__c = 'Active';
                assetItem.TGS_Service_status__c = 'Received';
                
                NE__OrderItem__c ordIt = mapOrdItems.get(assetItem.NE__AssetItemEnterpriseId__c);
                if(ordIt != null)
                {
                    ordIt.NE__AssetItemEnterpriseId__c = assetItem.Id;
                    confItemsToUpd.add(ordIt);
                    
                    for(NE__Order_Item_Attribute__c assetItAtt : assetItem.NE__Order_Item_Attributes__r)
                    {
                        NE__Order_Item_Attribute__c ordItAtt = mapOrdItAttr.get(assetItAtt.NE__AttrEnterpriseId__c);
                        if(ordItAtt != null)
                        {
                            ordItAtt.NE__AttrEnterpriseId__c = assetItAtt.Id;
                            confItemAttrToUpd.add(ordItAtt);
                        }
                    }
                }
            }
            
            assetItem.NE__Service_Account__c = config.NE__ServAccId__c;
            assetItem.NE__Billing_Account__c = config.NE__BillAccId__c;
            
            confItemsToUpd.add(assetItem);
        }
        
        update confItemsToUpd;
        update confItemAttrToUpd;
        
        if(updCase.Type == 'Disconnect')
            assetGenerated.NE__OrderStatus__c = 'Disconnected';
        else if(updCase.Type == 'Change')
            assetGenerated.NE__OrderStatus__c = 'Active';
        else
        {
            assetGenerated.NE__OrderStatus__c = 'In progress';
            config.NE__AssetEnterpriseId__c = assetGenerated.Id;
            update config;
        }
        
        assetGenerated.TGS_SIGMA_ID__c = updCase.Order__r.TGS_SIGMA_ID__c;
        
        update assetGenerated;
        
        NE__Asset__c commAsset          =   [SELECT NE__Disable_Button__c, NE__Status__c, TGS_RFB_date__c, TGS_RFS_date__c 
                                             FROM NE__Asset__c 
                                             WHERE Id = :assetGenerated.NE__Asset__c];
        commAsset.TGS_RFB_date__c = updCase.TGS_RFB_date__c; 
        commAsset.TGS_RFS_date__c = updCase.TGS_RFS_date__c; 
        commAsset.NE__Disable_Button__c =   '';
        if(updCase.Type == 'Disconnect')
            commAsset.NE__Status__c = 'Disconnected';

        update commAsset;

        TGS_CallRodWs.inFutureContext   = false;
        TGS_CallRodWs.inFutureContextCI = false;
        
        return assetGenerated.id;
    }
    
    @future(callout=true)
    global Static void callOrder2Asset(String confId, String caseId, String sessionId) 
    {
        String addr =   URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/RESTCallOrder2Asset';
        System.debug('DEBUG url RESTCallOrder2Asset: '+addr);
        Map<String,String> postBody = new Map<String,String>();

        HttpRequest req = new HttpRequest();
        req.setEndpoint( addr );
        req.setTimeout(50000);
        req.setMethod('POST');
        req.setHeader('Authorization', 'OAuth ' + sessionId);
        req.setHeader('Content-Type','application/json');
        
        postBody.put('confId',confId);
        postBody.put('caseId',caseId);
        
        String reqBody = JSON.serialize(postBody);
        req.setBody(reqBody);
        Http http = new Http();
        HttpResponse response = http.send(req);
        
        System.debug('UserInfo.getSessionId(): '+sessionId);
        System.debug('DEBUG REQUEST: '+req);
        System.debug('DEBUG RESPONSE HTTP: '+response);
        System.debug('DEBUG RESPONSE HTTP BODY: '+response.getBody());
        
        String assetId = response.getBody().replace('"','');
        list<NE__OrderItem__c> confItems = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: assetId];
        
        
        TGS_CallRodWs.inFutureContext   = false;
        TGS_CallRodWs.inFutureContextCI = false;
         /* Send Integration with in ROD in modification and termination */
        Set<Id> confitemsIds = new Set<Id>();
        for(NE__OrderItem__c ci:confItems){
            
            confitemsIds.add(ci.Id);
        }
        TGS_CallRodWs.invokeWebServiceRODOrderItem(confitemsIds, false); 
       
    }
}