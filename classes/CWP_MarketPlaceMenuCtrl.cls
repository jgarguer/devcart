public class CWP_MarketPlaceMenuCtrl {
	private static User currentUser;
	
	@AuraEnabled
    public static map<string, string> getMainImage(){
        currentUser = CWP_LHelper.getCurrentUser(null);
        System.debug('@@$'+currentUser);
        map<string, string> retMap;
        retMap= new map<string, string>();
        retMap.put('title', label.PCA_MarketPlace);
        retMap.put('informationMenu', label.PCA_MarketPlaceText);
        retMap.put('imgId', null);
           
        Account currentAccount = CWP_LHelper.getCurrentAccount(currentUser.Contact.BI_Cuenta_activa_en_portal__c);
        system.debug('llamada a la funcion que recoge la imagen del fondo  ' + currentUser);
        if(currentAccount != null){
        	map<string, string> rcvMap = CWP_LHelper.getPortalInfo('MarketPlace', currentUser, currentAccount);
	        if(!rcvMap.isEmpty()){
	            retMap.clear();
	            retMap.putAll(rcvMap);
	        }
        }
        
        
        system.debug('mapa de datos que van de vuelta' + retMap);
        return retMap;
    }
}