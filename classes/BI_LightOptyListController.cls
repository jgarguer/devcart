public class BI_LightOptyListController 
{
    public Apexpages.Standardcontroller controller;
    public String   newConfId{get;set;}
    public NE__Order__c activeConfiguration{get;set;}
    public String   discountLine{get;set;}
    
    public String   activeId{get;set;}
    public Opportunity optyToCheck{get;set;}
    
    public Boolean isMulticurrency;
    public map<String,sObject> mapOfOrderItemsCurrs;
    
    public String confCurrency;
    
    public Map<String, Decimal> mapOfCurrsConversion = new Map<String,Decimal>();
    
    public BI_LightOptyListController(Apexpages.Standardcontroller controller)
    {
        activeConfiguration =   new NE__Order__C();
        this.controller     =   controller;
        newConfId           =   '';
        discountLine        =   '';
        
        
        optyToCheck         =   [SELECT id, NE__Status__c, CloseDate FROM Opportunity WHERE id =: controller.getId()];
        
        list<NE__Order__c> listOfConf   =   [SELECT id,  NE__OrderStatus__C
                                             FROM NE__Order__c 
                                             where NE__OptyId__c=:controller.getId() 
                                                and NE__OrderStatus__C = 'Active'
                                                and recordtype.name ='Opty'
                                            Order by NE__Version__c DESC
                                            LIMIT 1];
        if(listOfConf.size() > 0)
            activeConfiguration =   listOfConf.get(0);
        
        activeId    =   activeConfiguration.id;
        mapOfCurrsConversion=createMapDate(optyToCheck.CloseDate);
    }    
    
    
    public void reviseOpty()
    {      
        String optyId   =   controller.getId();
        system.debug('optyId: '+optyId);
        
        String pageUrl  =   NE.JS_RemoteMethods.reviseOpty(optyId);
        
        list<String> urlSplitted    =   pageUrl.split('ordId=');
        system.debug('urlSplitted: '+urlSplitted);
        
        newConfId   =   urlSplitted[1];
        system.debug('newConfId: '+newConfId);
    }
    
    public void generateOrder()
    {      
        String optyId   =   controller.getId();
        system.debug('optyId: '+optyId);
        
        String pageUrl  =   NE.JS_RemoteMethods.generateOrderFromOpty(optyId);
        
        list<String> urlSplitted    =   pageUrl.split('ordId=');
        system.debug('urlSplitted: '+urlSplitted);
        
        newConfId   =   urlSplitted[1];
        system.debug('newConfId: '+newConfId);
    }
    
    // GCASTIELLO - 20151202 - Added
    public void generateQuote()
    {      
        String optyId   =   controller.getId();
        system.debug('optyId: '+optyId);
        
        String pageUrl  =   NE.JS_RemoteMethods.generateQuoteFromOpty(optyId);
        
        list<String> urlSplitted    =   pageUrl.split('ordId=');
        system.debug('urlSplitted: '+urlSplitted);
        
        newConfId   =   urlSplitted[1];
        system.debug('newConfId: '+newConfId);
    }   
    /**************************************/
    public void applyDiscount()
    {
        String orderId  =   activeConfiguration.id;
        
        NE__Order__c    confOrder   =   [SELECT NE__Contract_Header__c,NE__OrderStatus__c,NE__Recurring_Charge_Total__c,NE__One_Time_Fee_Total__c, NE__TotalRecurringFrequency__c, NE__OptyId__c FROM NE__Order__C WHERE id =: orderId];
        map<String, NE__OrderItem__c>   mapOfDiscountLines  =   new map<String, NE__OrderItem__c>([SELECT id,NE__Qty__c, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c FROM NE__OrderItem__C WHERE NE__OrderId__c =: orderId]);
        system.debug('discountLine: '+discountLine);
        list<NE__OrderItem__c> oiToUpd  =   new list<NE__OrderItem__c>();
        if(discountLine != null && discountLine != '')
        {
            list<String> discountRow    =   discountLine.split('=');
            
            for(String dRow:discountRow)
            {
                list<String> discountCol    =   dRow.split(';');
                system.debug('discountCol: '+discountCol);
                try
                {
                    String productId    =   discountCol.get(0);
                    system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@productId: '+productId);
                    NE__OrderItem__c    lineItem    =   mapOfDiscountLines.get(productId);
                    system.debug('lineItem: '+lineItem);
                    if(lineItem != null)
                    {
                        boolean addDiscount =   false;
                        
                        if(discountCol.size() >= 2)
                        {
                            if(discountCol[1] != null && discountCol[1] != '')
                            {
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base totale iniziale: '+confOrder.NE__One_Time_Fee_Total__c);
                                
                                confOrder.NE__One_Time_Fee_Total__c -=  lineItem.NE__OneTimeFeeOv__c*lineItem.NE__Qty__c;
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base totale dopo sottrazione: '+confOrder.NE__One_Time_Fee_Total__c);
                                
                                lineItem.NE__OneTimeFeeOv__c        =   Decimal.valueof(discountCol[1]);
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base linea: '+lineItem.NE__OneTimeFeeOv__c);

                                system.debug('lineItem.NE__OneTimeFeeOv__c: '+lineItem.NE__OneTimeFeeOv__c);
                                confOrder.NE__One_Time_Fee_Total__c +=  lineItem.NE__OneTimeFeeOv__c*lineItem.NE__Qty__c;
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base totale dopo addizione: '+confOrder.NE__One_Time_Fee_Total__c);

                           //     optyToCheck.NE__Status__c =   'Approval required';
                                
                                addDiscount                 =   true;
                            }
                        }
                        if(discountCol.size() >= 3)
                        {                        
                            if(discountCol[2] != null && discountCol[2] != '')
                            {
                                String recurring    =   discountCol[2];
                                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo receurring totale iniziale: '+confOrder.NE__Recurring_Charge_Total__c);
                               
                                //confOrder.NE__Recurring_Charge_Total__c   -=  lineItem.NE__OneTimeFeeOv__c*lineItem.NE__Qty__c;
                                confOrder.NE__Recurring_Charge_Total__c -=  lineItem.NE__RecurringChargeOv__c*lineItem.NE__Qty__c;
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo receurring totale dopo sottrazione: '+confOrder.NE__Recurring_Charge_Total__c);

                                lineItem.NE__RecurringChargeOv__c   =   Decimal.valueof(recurring);
                                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ prezzo recurring linea lineItem.NE__RecurringChargeOv__c: '+lineItem.NE__RecurringChargeOv__c);
                                //confOrder.NE__Recurring_Charge_Total__c   +=  lineItem.NE__OneTimeFeeOv__c*lineItem.NE__Qty__c;
                                confOrder.NE__Recurring_Charge_Total__c +=  lineItem.NE__RecurringChargeOv__c*lineItem.NE__Qty__c;
                                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo receurring totale dopo addizione: '+confOrder.NE__Recurring_Charge_Total__c);
                                
                              //  optyToCheck.NE__Status__c =   'Approval required';
                                addDiscount                 =   true;
                            }
                        }
                        
                        if(addDiscount)
                            oiToUpd.add(lineItem);
                    }
                    
                    
                    //update oiToUpd;
                    //update optyToCheck;                    
                    
                    // JGL 26/09/2016 changes acording Telefonica request, after applying new prices, not to go to Opty page 
                    //newConfId   =   controller.getId();
                	newConfId   =  'refresh';
                    // End JGL 26/09/2016
                }
                catch(Exception e)
                {
                    System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Scattata un eccezione');
                    newConfId   =   e.getMessage();
                }
            }
            
            try{
                update oiToUpd;
                update optyToCheck;
                update confOrder;
                recreateStdOpty(confOrder.id,confOrder);
            }catch(DMLException dmle){
                System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@EccezioneDML '+dmle.getMessage());
                //newConfId =   dmle.getMessage();
            }
        }
    }   
    /********************************************************************************************/
    
    
    /*****************************************************************************************/
    
   /* public void applyDiscount()
    {
        String orderId  =   activeConfiguration.id;
        
        NE__Order__c    confOrder   =   [SELECT NE__Contract_Header__c,NE__OrderStatus__c,NE__Recurring_Charge_Total__c,NE__One_Time_Fee_Total__c, NE__TotalRecurringFrequency__c, NE__OptyId__c FROM NE__Order__C WHERE id =: orderId];
        map<String, NE__OrderItem__c>   mapOfDiscountLines  =   new map<String, NE__OrderItem__c>([SELECT id,NE__Qty__c, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c FROM NE__OrderItem__C WHERE NE__OrderId__c =: orderId]);
        system.debug('discountLine: '+discountLine);
        list<NE__OrderItem__c> oiToUpd  =   new list<NE__OrderItem__c>();
        if(discountLine != null && discountLine != '')
        {
            list<String> discountRow    =   discountLine.split('=');
            
            for(String dRow:discountRow)
            {
                list<String> discountCol    =   dRow.split(';');
                system.debug('discountCol: '+discountCol);
                try
                {
                    String productId    =   discountCol.get(0);
                    system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@productId: '+productId);
                    NE__OrderItem__c    lineItem    =   mapOfDiscountLines.get(productId);
                    system.debug('lineItem: '+lineItem);
                    if(lineItem != null)
                    {
                        boolean addDiscount =   false;
                        
                        if(discountCol.size() >= 2)
                        {
                            if(discountCol[1] != null && discountCol[1] != '')
                            {
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base totale iniziale: '+confOrder.NE__One_Time_Fee_Total__c);
                                
                                confOrder.NE__One_Time_Fee_Total__c -=  convertCurrency(lineItem.NE__OneTimeFeeOv__c,optyToCheck.CurrencyIsoCode,UserInfo.getDefaultCurrency())*lineItem.NE__Qty__c;
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base totale dopo sottrazione: '+confOrder.NE__One_Time_Fee_Total__c);
                                
                                lineItem.NE__OneTimeFeeOv__c        =   Decimal.valueof(discountCol[1]);
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base linea: '+lineItem.NE__OneTimeFeeOv__c);

                                system.debug('lineItem.NE__OneTimeFeeOv__c: '+lineItem.NE__OneTimeFeeOv__c);
                                confOrder.NE__One_Time_Fee_Total__c +=  convertCurrency(lineItem.NE__OneTimeFeeOv__c,optyToCheck.CurrencyIsoCode,UserInfo.getDefaultCurrency())*lineItem.NE__Qty__c;
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo base totale dopo addizione: '+confOrder.NE__One_Time_Fee_Total__c);

                           //     optyToCheck.NE__Status__c =   'Approval required';
                                
                                addDiscount                 =   true;
                            }
                        }
                        if(discountCol.size() >= 3)
                        {                        
                            if(discountCol[2] != null && discountCol[2] != '')
                            {
                                String recurring    =   discountCol[2];
                                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo receurring totale iniziale: '+confOrder.NE__Recurring_Charge_Total__c);
                               
                                //confOrder.NE__Recurring_Charge_Total__c   -=  lineItem.NE__OneTimeFeeOv__c*lineItem.NE__Qty__c;
                                confOrder.NE__Recurring_Charge_Total__c -=  convertCurrency(lineItem.NE__RecurringChargeOv__c,optyToCheck.CurrencyIsoCode,UserInfo.getDefaultCurrency())*lineItem.NE__Qty__c;
                            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo receurring totale dopo sottrazione: '+confOrder.NE__Recurring_Charge_Total__c);

                                lineItem.NE__RecurringChargeOv__c   =   Decimal.valueof(recurring);
                                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ prezzo recurring linea lineItem.NE__RecurringChargeOv__c: '+lineItem.NE__RecurringChargeOv__c);
                                //confOrder.NE__Recurring_Charge_Total__c   +=  lineItem.NE__OneTimeFeeOv__c*lineItem.NE__Qty__c;
                                confOrder.NE__Recurring_Charge_Total__c +=  convertCurrency(lineItem.NE__RecurringChargeOv__c,optyToCheck.CurrencyIsoCode,UserInfo.getDefaultCurrency())*lineItem.NE__Qty__c;
                                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@prezzo receurring totale dopo addizione: '+confOrder.NE__Recurring_Charge_Total__c);
                                
                              //  optyToCheck.NE__Status__c =   'Approval required';
                                addDiscount                 =   true;
                            }
                        }
                        
                        if(addDiscount)
                            oiToUpd.add(lineItem);
                    }
                    
                    
                    //update oiToUpd;
                    //update optyToCheck;                    
                    
                    newConfId   =   controller.getId();
                }
                catch(Exception e)
                {
                    System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Scattata un eccezione');
                    newConfId   =   e.getMessage();
                }
            }
            
            try{
                update oiToUpd;
                update optyToCheck;
                update confOrder;
                recreateStdOpty(confOrder.id,confOrder);
            }catch(DMLException dmle){
                System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@EccezioneDML '+dmle.getMessage());
                //newConfId =   dmle.getMessage();
            }
        }
    }   */
    
    
    
    /********************************************************************************************/
        //GC 3.4.6 set/get/convert currency method
    public String getCurrency(sObject cObj)
    {
        String currencySelected;
        try
        {
            currencySelected    =   (String)cObj.get('currencyIsoCode');
        }
        catch (Exception e)
        {
            
        }
        
        return currencySelected;
    }
    
    public void convertOrderItemCurrency(NE.Item itOiCurrency)
    {
        try
        {
            String oiCurrency   =   UserInfo.getDefaultCurrency();
            sObject oiObject    =   mapOfOrderItemsCurrs.get(itOiCurrency.orderItem.id);
            if(oiObject != null) 
                oiCurrency  =   (String)oiObject.get('currencyIsoCode');
            
            if(oiCurrency != itOiCurrency.catalogItem.NE__Currency__c)
            {
                setCurrency((sObject)itOiCurrency.orderItem, itOiCurrency.catalogItem.NE__Currency__c);      
                //Calculate the price
                convertCurrency(itOiCurrency.orderItem.NE__OneTimeFeeOv__c,oiCurrency, itOiCurrency.catalogItem.NE__Currency__c);
                convertCurrency(itOiCurrency.orderItem.NE__RecurringChargeOv__c,oiCurrency, itOiCurrency.catalogItem.NE__Currency__c);
                
                itOiCurrency.orderItem.NE__BaseOneTimeFee__c        =   itOiCurrency.orderItem.NE__OneTimeFeeOv__c;
                itOiCurrency.orderItem.NE__BaseRecurringCharge__c   =   itOiCurrency.orderItem.NE__RecurringChargeOv__c;     
            }   
        }
        catch(Exception e){}    
    }
    
    public Decimal convertCurrency(Decimal inputToConvert,String sourceCurrency, String targetCurrency)
    {
        Decimal convertedCurrency;
         
        Decimal sourceCurrencyValue =   mapOfCurrsConversion.get(sourceCurrency);
        Decimal targetCurrencyValue =   mapOfCurrsConversion.get(targetCurrency);
        
        system.debug('sourceCurrency: '+sourceCurrency+' targetCurrency: '+targetCurrency);
        system.debug('inputToConvert: '+inputToConvert+' sourceCurrencyValue: '+sourceCurrencyValue+' targetCurrencyValue: '+targetCurrencyValue);
        try
        {    
                convertedCurrency           =   (inputToConvert/sourceCurrencyValue)*targetCurrencyValue;
        }
        catch(Exception e){
            convertedCurrency=0.00;
        }
        return convertedCurrency;
    }
    
    public void setCurrency(sObject cObj, String currencyIso)
    {
        try
        {
            cObj.put('currencyIsoCode', currencyIso);
        }
        catch(Exception e)
        {
            System.debug('Exception found: '+e+' at line:'+e.getLineNumber()); 
        }
    }
    

public void recreateStdOpty(String idOrdine, NE__Order__c ord)
    {
        
       
        //GC 3.4.6 manage multicurrency
        //Try to load conversion rate
        String confCurrency;
        isMulticurrency         =   false;
       // mapOfCurrsConversion    =   new map<String,decimal>();
        mapOfOrderItemsCurrs    =   new map<String,sObject>();
        
        try
        {
            String orderId                      =   idOrdine; 
            sObject orderObj                    =   Database.query('SELECT id, currencyIsoCode FROM NE__Order__c WHERE id =: orderId');
            confCurrency                        =   getCurrency(orderObj);
            
            system.debug('confCurrency: '+confCurrency);
            if(confCurrency != '')
            {
               
                //Load the order items
                    if(orderId != null && orderId != '')
                        mapOfOrderItemsCurrs    =   new map<String,sObject>(Database.query('Select id,currencyIsoCode from NE__OrderItem__c where NE__OrderId__c =: orderId'));
                
                isMultiCurrency =   true;
            }
            else
                isMultiCurrency =   false;
        }       
        catch(Exception e)
        {
            system.debug('Multicurrency not active');
            isMultiCurrency =   false;
        }       
        
        list<NE__OrderItem__c> oiToUpd  =   new list<NE__OrderItem__c>();
        
        
        
       
        update oiToUpd;
       
        Map < String , Integer> freqValues  =   new Map < String , Integer>();
        freqValues.put('Monthly',12);
        freqValues.put('Bi-Monthly',6);
        freqValues.put('Quarterly',4);
        freqValues.put('Four-Monthly',3);
        freqValues.put('Semiannual',2);
        freqValues.put('Annual',1);         
        
        List<NE__OrderItem__c> orderitm = [SELECT NE__CatalogItem__r.NE__Currency__c,NE__Root_Order_Item__r.NE__Qty__c, NE__OrderItemPromoId__r.NE__Qty__c,NE__prodId__c,NE__ProdId__r.NE__ForecastProd__c, NE__Onetimefeeov__c, NE__RecurringChargeOv__c, NE__RecurringChargeFrequency__c, NE__Qty__c 
                                       FROM NE__OrderItem__c 
                                       WHERE NE__OrderId__c = :idOrdine AND NE__Action__c != 'Remove' ]; 
                                       
         

        ord.NE__One_Time_Fee_Total__c       =   0; 
        ord.NE__Recurring_Charge_Total__c   =   0;
        
        for(NE__OrderItem__c oiOrderCalc: orderitm)
        {
            Decimal multQty =   1;

            if(oiOrderCalc.NE__Root_Order_Item__r.NE__Qty__c != null)
                multQty =   multQty*oiOrderCalc.NE__Root_Order_Item__r.NE__Qty__c;
                
            if(oiOrderCalc.NE__OrderItemPromoId__r.NE__Qty__c != null)
                multQty =   multQty*oiOrderCalc.NE__OrderItemPromoId__r.NE__Qty__c; 
            
            //GC 3.4.6 manage multicurrency
            decimal oneTime;
            
            oneTime     =   oiOrderCalc.NE__Onetimefeeov__c;

                
            decimal recurring;

            recurring   =   oiOrderCalc.NE__RecurringChargeOv__c;

            if(isMulticurrency && oiOrderCalc.NE__CatalogItem__r.NE__Currency__c != null && confCurrency != null)
            {
                try
                {    
                        oneTime             =   convertCurrency(oneTime,oiOrderCalc.NE__CatalogItem__r.NE__Currency__c, confCurrency);  
                }
                catch(Exception e){ 
                }
               
                try
                {
                    recurring           =   convertCurrency(recurring,oiOrderCalc.NE__CatalogItem__r.NE__Currency__c, confCurrency);                                
                }
                catch(Exception e){}
            }
            
            if(oiOrderCalc.NE__Onetimefeeov__c != null)
                ord.NE__One_Time_Fee_Total__c           +=  oneTime*oiOrderCalc.NE__Qty__c*multQty;
             if((oiOrderCalc.NE__RecurringChargeOv__c != null)&&(oiOrderCalc.NE__RecurringChargeFrequency__c!=null))
                ord.NE__Recurring_Charge_Total__c       +=  recurring*freqValues.get(oiOrderCalc.NE__RecurringChargeFrequency__c)*oiOrderCalc.NE__Qty__c*multQty;
        }
        
        //GC 3.4.
        Integer totalDivisor                =   freqValues.get(ord.NE__TotalRecurringFrequency__c);
        ord.NE__Recurring_Charge_Total__c       =   ord.NE__Recurring_Charge_Total__c/totalDivisor;
        

        update ord;
        
        //Update related opty
        if(ord.RecordType.name == 'Opty' && ord.NE__OptyId__c != null)  
        {
            
            Opportunity relOpty = [SELECT Id,priceBook2Id,Name,amount,NE__HaveActiveLineItem__c, NE__Contract_Duration_Months__c FROM Opportunity WHERE Id =: ord.NE__OptyId__c];
            
            // ET v. 3.3 forecast and update cart management opportunity line items
            //GC 3.4 contract duration
            //find the monthly amount
            if(relOpty.NE__Contract_Duration_Months__c == 0 || relOpty.NE__Contract_Duration_Months__c == null)
                relOpty.NE__Contract_Duration_Months__c =   12;
             decimal monthlyAmount;     
        
                monthlyAmount   =   (ord.NE__Recurring_Charge_Total__c * freqValues.get(ord.NE__TotalRecurringFrequency__c))/12;
            
            decimal optyOneTime     =   ord.NE__One_Time_Fee_Total__c;
            //GC 3.4.6 multicurrency management      
            sObject optyObject;      
            String optyCurrency;    
            if(isMulticurrency == true)  
            {
                String optyId   =   relopty.id;
                optyObject      =   Database.query('SELECT id, currencyIsoCode FROM Opportunity WHERE id =: optyId');
                optyCurrency    =   getCurrency(optyObject);
                if(optyCurrency != confCurrency)
                {
                    try
                    {
                        optyOneTime             =   convertCurrency(optyOneTime,confCurrency, optyCurrency);
                    }
                    catch(Exception e){}
                    
                    try{
                        monthlyAmount           =   convertCurrency(monthlyAmount,confCurrency, optyCurrency);
                    }
                    catch(Exception e){}
                }
            }
            
            relOpty.amount                  =   optyOneTime + (monthlyAmount * relOpty.NE__Contract_Duration_Months__c);
            
            update(relOpty);         
            System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@ totale opty '+relOpty.amount);
            List<OpportunityLineItem> opptyItem = new List<OpportunityLineItem>([SELECT Id
                                                                                FROM OpportunityLineItem WHERE OpportunityId= : relOpty.Id]);
       
            system.debug('*RELOPTYPricebook ' + relOpty.pricebook2Id);
            List<PriceBook2> pricebook = new List<PriceBook2>();
            if(relOpty.pricebook2Id == null){
                 pricebook = [SELECT Id, Name, (SELECT Id, UnitPrice, Product2Id FROM PriceBookEntries) 
                                         FROM PriceBook2 WHERE Name = 'Standard'];
            }
            else{
                 pricebook = [SELECT Id, Name, (SELECT Id, UnitPrice, Product2Id FROM PriceBookEntries)
                                         FROM PriceBook2 
                                         WHERE id= :relOpty.priceBook2Id  ];
            }
            system.debug('*PRICEBOOK' + pricebook);
            if(pricebook.size() > 0)
            {                           
                list<PriceBookEntry> entries    =   pricebook[0].PriceBookEntries;
                    
                map<String,String> mapOfEntryProducts   =   new map<String,String>();
                for(PriceBookEntry pbe:entries)
                {
                    mapOfEntryProducts.put(pbe.Product2Id, pbe.Id);
                    system.debug('*MAP ENTRIES '+ mapOfEntryProducts);  
                }                   
                                    

                system.debug('*ORDERITMLIST ' + orderitm);                             
                if(orderitm.size() > 0){
                    List<OpportunityLineItem> l_itemList = new List<OpportunityLineItem>();  
                    system.debug('*ORDERITEM '+ orderitm);                                                   
                    for(NE__OrderItem__c oi: orderitm){                 
    
                        if(oi.NE__ProdId__r.NE__ForecastProd__c != null)
                        {
                            String productEntry  = mapOfEntryProducts.get(oi.NE__ProdId__r.NE__ForecastProd__c);
                            system.debug('*PRODUCTENRTY ' + productEntry);  
                            if(productEntry != null){
                                OpportunityLineItem l_item = new OpportunityLineItem();
                                l_item.OpportunityId = relOpty.Id;
                                
                                //GC 3.4 use the contract duration
                                decimal monthlyOptyItem     =   (oi.NE__RecurringChargeOv__c * freqValues.get(oi.NE__RecurringChargeFrequency__c))/12;
                                decimal onetimeoi           =   oi.NE__Onetimefeeov__c;
                                
                                //GC 3.4.6 multicurrency management
                                if(isMulticurrency == true)  
                                {           
                                    if(optyCurrency != oi.NE__CatalogItem__r.NE__Currency__c && oi.NE__CatalogItem__r.NE__Currency__c != null)
                                    {
                                        try{
                                            onetimeoi               =   convertCurrency(onetimeoi,oi.NE__CatalogItem__r.NE__Currency__c, optyCurrency);
                                        }
                                        catch(Exception e){}
                                        
                                        try{
                                            monthlyOptyItem         =   convertCurrency(monthlyOptyItem,oi.NE__CatalogItem__r.NE__Currency__c, optyCurrency);
                                        }
                                        catch(Exception e){}
                                    }
                                }
                                
                                l_item.UnitPrice            =   onetimeoi + (monthlyOptyItem * relOpty.NE__Contract_Duration_Months__c);
                                l_item.PricebookEntryId     =   productEntry;
                                
                                //GC 3.4: calculate the qty 
                                l_item.Quantity             =   oi.NE__Qty__c;
                                
                                if(oi.NE__Root_Order_Item__r.NE__Qty__c != null)
                                    l_item.Quantity =   l_item.Quantity*oi.NE__Root_Order_Item__r.NE__Qty__c;
                                    
                                if(oi.NE__OrderItemPromoId__r.NE__Qty__c != null)
                                    l_item.Quantity =   l_item.Quantity*oi.NE__OrderItemPromoId__r.NE__Qty__c;                              
                                
                                
                                l_itemList.add(l_item);
                            }
                        }
                    }
                   
                    insert l_itemList;
                   
                    
                    //GC 3.4.10 delete the old items after the insert 
                    system.debug('*OPPTYITEMS ' + opptyItem);                                                                   
                    if(opptyItem.size() > 0){ 
                                                                                  
                        delete opptyItem;
                      
                    }                     
                }   
                            
            }
        }
        

     }    
     
     //return currency  to a specific date range || standard currency
                public map<String, decimal> createMapDate(Date rangeDate)
                {
                               map<String, decimal> dateCurrency= new map <String, decimal>(); 
        try
        {
            if(UserInfo.isMultiCurrencyOrganization())
            {
                List<sObject> convActiveObj= Database.query('SELECT ConversionRate,Id,IsoCode,NextStartDate,StartDate FROM DatedConversionRate WHERE (StartDate <=: rangeDate AND NextStartDate >: rangeDate)');
                
                 if(convActiveObj.size()!=0)
                 {
                    for(sObject dateRateObj: convActiveObj)
                    {
                        dateCurrency.put((String)dateRateObj.get('IsoCode'),(Decimal)dateRateObj.get('ConversionRate'));
                    }
                 }else
                 {
                        list <sObject>   listOfCurrencieType    =   Database.query('SELECT ConversionRate,CreatedById,CreatedDate,DecimalPlaces,Id,IsActive,IsCorporate,IsoCode,LastModifiedById,LastModifiedDate,SystemModstamp FROM CurrencyType');
                        
                        for(sObject sobj: listOfCurrencieType)
                        {
                            dateCurrency.put((String)sobj.get('IsoCode'),(Decimal)sobj.get('ConversionRate'));
                        }   
                }
                return dateCurrency;
            }else
            {
                dateCurrency.put(userinfo.getDefaultCurrency(),1.0);
            }
            
            
        }catch(Exception e){}
        
        return dateCurrency;
    }
     
                // convert currency OPTY 
        public Decimal convertCurrencyOPTY(Decimal inputToConvert, Decimal OrderCurrency, Decimal OrderItemCurrency)
        { 
                       Decimal resultTotal;         
                       try
                       {
                         resultTotal = (inputToConvert/OrderItemCurrency)*OrderCurrency;   
                       
                       }catch(Exception e)
                       {
                           System.debug('Exception found: '+e+' at line:'+e.getLineNumber());
                       }       
            return  resultTotal; 
        }
     /*********************************************************************************/
     
}