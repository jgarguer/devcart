@isTest
private class BI_Tasa_de_cambio_presupuestaria_TEST {

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Test method to manage the code coverage for 
    			   BI_Tasa_de_cambio_presupuestaria.BI_actualiza_fecha_fin_anterior

   
    History:
    <Date>                  <Author>                <Change Description>
    08/11/2016              Gawron, Julián          Initial Version  D317
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 @isTest static void BI_actualiza_fecha_fin_anterior_TEST(){

        BI_TestUtils.throw_exception=false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;


        //Insertamos una tasa
        BI_Tasa_de_cambio_presupuestaria__c t1 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c =  Date.newInstance(2016, 11, 1),
           BI_Ratio_divisa_a_euro__c = 1.0,
           CurrencyIsoCode = 'ARS' );
    
        insert t1;

        //Desactivamos el booleano del trigger, para que funcionen las transacciones
        NETriggerHelper.setTriggerFiredTest('BI_Tasa_de_cambio_presupuestaria', false);
        BI_Tasa_de_cambio_presupuestaria__c t2 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c = Date.newInstance(2016, 11, 4),
           BI_Ratio_divisa_a_euro__c = 2.0,
           CurrencyIsoCode = 'ARS' );

        insert t2;

        //Desactivamos el booleano del trigger, para que funcionen las transacciones
        NETriggerHelper.setTriggerFiredTest('BI_Tasa_de_cambio_presupuestaria', false);
        BI_Tasa_de_cambio_presupuestaria__c t3 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c = Date.newInstance(2016, 11, 5),
           BI_Ratio_divisa_a_euro__c = 2.0,
           CurrencyIsoCode = 'ARS' );

        insert t3;
        
        NETriggerHelper.setTriggerFiredTest('BI_Tasa_de_cambio_presupuestaria', false);
        t3.BI_Fecha_inicio__c = Date.today();

        update t3;

       List<BI_Tasa_de_cambio_presupuestaria__c> tasas = [
        Select BI_Fecha_inicio__c, BI_Fecha_fin__c from BI_Tasa_de_cambio_presupuestaria__c 
        where Id = :t1.Id];

       System.assertEquals(tasas[0].BI_Fecha_fin__c, t2.BI_Fecha_inicio__c);

        tasas = [
        Select BI_Fecha_inicio__c, BI_Fecha_fin__c from BI_Tasa_de_cambio_presupuestaria__c 
        where Id = :t2.Id];
       System.assertEquals(tasas[0].BI_Fecha_fin__c, Date.today());

    }

}