public with sharing class BI2_EmailMessage {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Methods executed by EmailMessage Triggers
	 Test Class:	BI2_EmailMessageMethods_TEST
	 History:

	 <Date>			<Author>						<Change Description>
	 05/10/2015		Jose Miguel Fierro				Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	private static BusinessHours DEFAULT_HOURS = [select Id from BusinessHours where IsDefault=true];
    public  static final List<BusinessHours> BUSINESS_HOURS = [SELECT Id, Name FROM BusinessHours];

	public static final List<EmailTemplate> templates = [SELECT Id, DeveloperName, Body FROM EmailTemplate WHERE DeveloperName IN ('BI2_Notificar_Nuevo_Caso_email2caseAuto' , 'BI2_Notificar_Nuevo_Caso_email2caseNoAuto')];

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Assigns the automatically created cases to the correct User/Queue

	 History:
	 <Date>			<Author>						<Change Description>
	 05/10/2015		Jose Miguel Fierro 				Initial Version
	 21/10/2015		Jose Miguel Fierro				Added ContactId to Case query and now allows multiple valid destination emails
	 09/05/2016		Antonio Pardo 					checked if the valids email addresses are into ToAddress and CcAddress fields on emailMessage
	 06/02/2017     Jose Miguel Fierro              Prevent reassigning the case in later operations
	 06/02/2017     Oscar Jimenez 		            Remove comment for reassigning the case in later operations
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setAssignations(List<EmailMessage> news) {
		/* Asigna el Owner según Contacto[Email]==EmailMessage[FromAddress]
			Si:
				- FromAddress no corresponde a ningún Contacto (Contacto no existe) -> Añadir a cola 'Correo no identificado'
				- FromAddress corresponde a un Contacto:
				- Contacto[BI2_IsAutorized__c]  == false -> Añadir a cola 'Correos identificados'
				- Contacto[BI2_IsAutorized__c] == true
				- Contacto en 2+ Clientes como autorizado -> ????
				- Contacto en 1 sólo Cliente -> ??
				- 05/10/2015: Fuera de BusinessHours -> Cola 'PER_EMP_Pool 24 x 7'
		*/
		try {
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

			System.debug(LoggingLevel.ERROR, '{-----} 1 {-----} Entered EmailMessageTrigger!!!!');

			List<Id> lst_ems = new List<Id>();
			for(EmailMessage mail : news) {
				lst_ems.add(mail.Id);
			}
			future_setAssignations(lst_ems);
		} catch (Exception ex) {
			System.debug(LoggingLevel.ERROR, '{-----} 2 {-----}: Where: BI2_EmailMessage.setAssignations Type: ' + ex.getTypeName() + ' Reason: ' + ex.getMessage());
			BI_LogHelper.generate_BILog('BI2_EmailMessage.setAssignations', 'BI_EN', ex, 'Trigger');
		}System.debug('\n\tSOQL Queries : ' + Limits.getQueries() + ' out of ' + Limits.getLimitQueries());
	}

	@future
	public static void future_setAssignations(List<Id> emailMessages) {
		try {
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

			System.debug(LoggingLevel.ERROR, '{-----} 3 {-----} Entered EmailMessageFuture!!!!');
			
			BusinessHours bhPeru = null;
            for(BusinessHours bh : BUSINESS_HOURS) {
                if(bh.Name == 'Horario oficina PER') {
                    bhPeru = bh;
                }
            }

			Set<Id> set_caseIds = new Set<Id>(); // Ids of the pre-created cases
			Set<String> set_case_emailaddr = new Set<String>();
			
			Set<String> set_valid_destinations = new Set<String>(Label.BI2_Destinatario_Email_to_Case.split(' '));
			
			//APC 09/05/2016 - checked if the valids emails address are into ToAddress and CcAddress fields in the emailMessage
			
			for(EmailMessage mail : [SELECT Id, ParentId, ToAddress, Incoming, FromAddress, CcAddress FROM EmailMessage WHERE Id IN :emailMessages]) {
				
				System.debug('CCADRESSES --> ' + mail.CcAddress);
				System.debug('TOADRESSES --> ' + mail.ToAddress);
			
				if(mail.Incoming){
					for(String str : set_valid_destinations){

						if(mail.ToAddress != null){
							if(mail.ToAddress.contains(str)){
								System.debug('CONTENGOADRESS');
								set_caseIds.add(mail.ParentId);
								set_case_emailaddr.add(mail.FromAddress);
								break;
							}
						}

						if(mail.CcAddress != null){
							if(mail.CcAddress.contains(str)){
								System.debug('CONTENGOCC');
								set_caseIds.add(mail.ParentId);
								set_case_emailaddr.add(mail.FromAddress);
								break;
							}	
						}
					}	
				}

			//	if(mail.Incoming == true && set_valid_destinations.contains(mail.ToAddress)/*'asesor.servicioperu@telefonica.com'*/){
			//		System.debug('Paraprocesar');
			//		set_caseIds.add(mail.ParentId);
			//		set_case_emailaddr.add(mail.FromAddress);
			//	}
			}

			if (!set_caseIds.isEmpty()) {
				List<Case> set_cases = [SELECT Id, SuppliedEmail, Origin, ContactId, AccountId, BusinessHoursId, OwnerId FROM Case WHERE Id IN :set_caseIds AND RecordType.DeveloperName = 'BI2_Caso_Padre']; // lista de casos a procesar
				
				/*BusinessHours*/
				Set<Id> setAccounts = new Set<Id>();
				for(Case cas : set_cases) {
					if(cas.AccountId != null){
                		setAccounts.add(cas.AccountId);
					}
            	}
            	Map<Id, String> map_accounts = new Map<Id, String>();
				if(!setAccounts.isEmpty()){
					for (Account acc: [select Id, BI_Country__c from Account where Id IN :setAccounts]) {
						map_accounts.put(acc.Id, acc.BI_Country__c);
					}
				}
				/*BusinessHours*/
				
				Id id_no_identificado, id_identificado, id_24x7;
				for(Group grp : [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN ('BI2_PER_Correo_no_identificado','BI2_PER_Correo_identificado', 'BI2_PER_EMP_Pool_24_x_7')]) {
					if(grp.DeveloperName=='BI2_PER_Correo_no_identificado') {
						id_no_identificado = grp.Id;
					}
					if(grp.DeveloperName=='BI2_PER_Correo_identificado') {
						id_identificado = grp.Id;
					}
					if(grp.DeveloperName=='BI2_PER_EMP_Pool_24_x_7') {
						id_24x7 = grp.Id;
					}
				}

				Set<String> set_duplicate_cont_emailaddrs = new Set<String>();
				Map<String, Contact> map_unique_email2contact = new Map<String, Contact>();
				Map<String, List<Contact>> map_email2contacts = new Map<String, List<Contact>>();
				
				Map<Id, String> map_contacts = new Map<Id, String>();

				if(!set_case_emailaddr.isEmpty()) {
					for(Contact con : [SELECT Id, Email, BI2_IsAutorized__c, Account.BI2_asesor__c, Account.BI2_cuadrilla_id__c, OwnerId, BI_Country__c FROM Contact WHERE Email IN :set_case_emailaddr]){ // 1Contact->1Client assumed
						if(!map_email2contacts.keyset().contains(con.Email)) {
							map_email2contacts.put(con.Email, new List<Contact>());
						}
						map_email2contacts.get(con.Email).add(con);
						map_contacts.put(con.Id, con.BI_Country__c);
					}
				}

				List<Task> lst_tasks2insert = new List<Task>();

				List<Case> lst_cases_contAuth = new List<Case>();
				List<Case> lst_cases_contNoAuth = new List<Case>();
				Map<Id, Contact> map_case2NoAuthContact = new Map<Id, Contact>();
				List<Case> lst_cases_noIdentificados = new List<Case>();

				for (Case cas : set_cases){
					// JMF 16/02/2017 - Temporarily commented-out until user confirmation -- Remove this comment then
					// JMF 06/02/2017 - If the case was already assigned to someone, do not re-assign it
					// OAJF 13/11/2017 - Remove temporal comment "if" - put by JMF 16/02/2017
					if(cas.OwnerId  != UserInfo.getUserId()) {
						continue;
					}
					// Business Hours
	                if((bhPeru != null && cas.ContactId != null && map_contacts.get(cas.ContactId) != null && map_contacts.get(cas.ContactId) == Label.BI_Peru)
	                   || (bhPeru != null && cas.ContactId == null && cas.AccountId != null && map_accounts.get(cas.AccountId) == Label.BI_Peru)) {
	                        cas.BusinessHoursId = bhPeru.Id;
	                        System.debug(LoggingLevel.ERROR, 'BusinessHours: ' + bhPeru);
	                    }
					
					System.debug(LoggingLevel.ERROR, '{-----} CASEID {-----}: ' + cas.Id);
					cas.BI_Confidencial__c = false;	// Definitivo?
					if(!map_email2contacts.keySet().contains(cas.SuppliedEmail)) { // BI2_PER_Correo_no_identificado
						System.debug(LoggingLevel.ERROR, '{-----} 4 {-----}: Email "' + cas.SuppliedEmail + '" was NOT a contact email. Case should be assigned to "Correos No identificados"');
						cas.OwnerId = id_no_identificado;
						lst_cases_noIdentificados.add(cas);
					} else {
						List<Contact> contacts = map_email2contacts.get(cas.SuppliedEmail);
						if(contacts.size() == 1) { // Not duplicate contact
							Contact con = contacts[0];
							if(!con.BI2_IsAutorized__c){ //BI2_PER_Correo_identificado
								System.debug(LoggingLevel.ERROR, '{-----} 5 {-----}: Contact "' + cas.SuppliedEmail + '" was not authorized! Case should be assigned to "Correos identificados"');
								cas.OwnerId = id_identificado;
								//  cas.ContactId = con.Id;
								lst_cases_contNoAuth.add(cas);
							} else {
								System.debug(LoggingLevel.ERROR, '{-----} 6 {-----}: Contact "' + cas.SuppliedEmail + '" was authorized! Case should be assigned to contact.');
								
								System.debug(LoggingLevel.ERROR, '{-----} 88 {-----}: Contact "' + cas );
								
								//Business Hour
								if(bhPeru != null && map_contacts.get(con.Id) != null && map_contacts.get(con.Id) == Label.BI_Peru){
                      				cas.BusinessHoursId = bhPeru.Id;
                        			System.debug(LoggingLevel.ERROR, 'BusinessHours: ' + bhPeru);
                    			}
								
								System.debug(LoggingLevel.ERROR, '{-----} 99 {-----}: Contact "' + BusinessHours.isWithin(cas.BusinessHoursId, Datetime.now()) );
								
								if(BusinessHours.isWithin(cas.BusinessHoursId, Datetime.now())) {
									setOwner(cas, con.Account.BI2_asesor__c, con.Account.BI2_cuadrilla_id__c);
								} else {
									cas.OwnerId = id_24x7;
									Task tas = new Task(OwnerId=con.Account.BI2_asesor__c, Subject='Correo fuera de horario', WhoId=con.Id, WhatId=cas.Id, Description='Se recibió un correo fuera de las horas de oficina.');
									lst_tasks2insert.add(tas);
								}
								lst_cases_contAuth.add(cas);
								map_case2NoAuthContact.put(cas.Id, con);
							}
						} else { // Duplicate contact (in 2+ Clients)
							System.debug(LoggingLevel.ERROR, '{-----} 7 {-----}: Contact "' + cas.SuppliedEmail + '" was in multiple clients. ');
							Boolean found = false;
							for(Contact con : contacts) {
								if(con.BI2_IsAutorized__c) {
									System.debug(LoggingLevel.ERROR, '{-----} 8 {-----}: Email "' + cas.SuppliedEmail + '" had multiple contacts. Associating case with User "' + con.Account.BI2_Asesor__c +'"');
									found = true;
									
									//Business Hour
									if(bhPeru != null && map_contacts.get(con.Id) != null && map_contacts.get(con.Id) == Label.BI_Peru){
	                      				cas.BusinessHoursId = bhPeru.Id;
	                        			System.debug(LoggingLevel.ERROR, 'BusinessHours: ' + bhPeru);
	                    			}
									
									if(BusinessHours.isWithin(cas.BusinessHoursId, Datetime.now())) {
										setOwner(cas, con.Account.BI2_asesor__c, con.Account.BI2_cuadrilla_id__c);
										Task tas = new Task(OwnerId=con.Account.BI2_asesor__c, Subject='Llamar para confirmar cliente', WhoId=con.Id, WhatId=cas.Id, Description='Se creó un caso para un contacto que está asociado a varios clientes. Llame para confirmar el cliente.');
										lst_tasks2insert.add(tas);
									} else {
										cas.OwnerId = id_24x7;
										Task tas = new Task(OwnerId=con.Account.BI2_asesor__c, Subject='Correo fuera de horario', WhoId=con.Id, WhatId=cas.Id, Description='Se recibió un correo fuera de las horas de oficina.');
										lst_tasks2insert.add(tas);
									}
									cas.ContactId = con.Id;
									lst_cases_contAuth.add(cas);
									map_case2NoAuthContact.put(cas.Id, con);
									///////////////////////////////////////////
									
									break;
								}
							}
							if(!found) {
								System.debug(LoggingLevel.ERROR, '{-----} 9 {-----}: Contact "' + cas.SuppliedEmail + '" was not authorized with any client! Case should be assigned to "Correos identificados"');
								cas.OwnerId = id_identificado;
								lst_cases_contNoAuth.add(cas);
							}
						}
					}
				}
				update set_cases;
				insert lst_tasks2insert;
				sendEmailsAndTasks(lst_cases_contAuth, lst_cases_contNoAuth, map_case2NoAuthContact, lst_cases_noIdentificados);
			}
		} catch (Exception ex) {
			System.debug(LoggingLevel.ERROR, '{-----} 10 {-----}: Where: BI2_EmailMessage.setAssignations Type: ' + ex.getTypeName() + ' Reason: ' + ex.getMessage());
			BI_LogHelper.generate_BILog('BI2_EmailMessage.future_setAssignations', 'BI_EN', ex, 'Trigger');
		}System.debug('\n\tSOQL Queries : ' + Limits.getQueries() + ' out of ' + Limits.getLimitQueries());
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Sets the owner of the case

	 History:
	 <Date>			<Author>						<Change Description>
	 06/10/2015		Jose Miguel Fierro 				Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testVisible private static void setOwner(Case cas, Id acc_asesor, String acc_cuadrilla) {
		try {
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
			
			if(acc_asesor != null) {
				System.debug('*** ENTRÓ EN IF(ASESOR): ');
				cas.OwnerId = acc_asesor;
			}else if(acc_cuadrilla != null && acc_cuadrilla != '') {
				System.debug('*** ENTRÓ EN IF(CUADRILLA): ');
				cas.OwnerId = acc_cuadrilla;
			}
			//else {cas.OwnerId = null;}
		} catch (Exception ex) {
			BI_LogHelper.generate_BILog('BI2_EmailMessage.setOwner', 'BI_EN', ex, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Jose Miguel Fierro
	 Company:       Salesforce.com
	 Description:   Sets the owner of the case

	 History:
	 <Date>         <Author>                        <Change Description>
	 06/10/2015     Jose Miguel Fierro              Initial Version
	 07/12/2015		Guillermo Muñoz					Prevent the email send in Test class
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testVisible private static void sendEmailsAndTasks(List<Case> lst_auth, List<Case> lst_noauth, Map<Id, Contact> map_case2NoAuthContact, List<Case> lst_noIden) {
		/* Un email que venga de un contacto autorizado, hay que hacer dos cosas:
			- email de acuse de recibo, según la plantilla BI2 Notificar Nuevo Caso email2case Auto;
			- crearle una tarea de tipo llamada al Asesor de la cuenta

			Email2case que venga de un contacto no autorizado
			- Enviarle el email según la plantilla BI2 Notificar Nuevo Caso email2case No Auto
		*/
		try { 
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

			Id id_tempAuth, id_tempNoAuth;
			String str_noIden;
			for(EmailTemplate et : templates) {
				if(et.DeveloperName == 'BI2_Notificar_Nuevo_Caso_email2caseAuto')
					id_tempAuth = et.Id;
				if(et.DeveloperName == 'BI2_Notificar_Nuevo_Caso_email2caseNoAuto'){
					id_tempNoAuth = et.Id;
					str_noIden = et.Body;
				}
			}

			List<Messaging.SingleEmailMessage> lst_msgs = new List<Messaging.SingleEmailMessage>();
			for(Case cas : lst_auth) { 
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				
				//if(cas.ContactId != null)
					email.setTargetObjectId(cas.ContactId);
				//else 
				//  email.setTargetObjectId(map_case2NoAuthContact.get(cas.Id).Id);

				email.setTemplateId(id_tempAuth);
				email.setWhatId(cas.Id);
				lst_msgs.add(email);
			}
			for(Case cas : lst_noauth) { 
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

				//if(cas.ContactId != null)
					email.setTargetObjectId(cas.ContactId);
				//else
				//  email.setTargetObjectId(map_case2NoAuthContact.get(cas.Id).Id);

				email.setTemplateId(id_tempNoAuth);
				lst_msgs.add(email);
			}
			for(Case cas : lst_noIden) {
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				email.setToAddresses(new List<String>{cas.SuppliedEmail});
				email.setPlainTextBody(str_noIden);
				email.setWhatId(cas.Id);
				lst_msgs.add(email);
			}

			System.debug(loggingLevel.Error, '{***} lst_msgs {***}: ' + lst_msgs);
			if(!Test.isRunningTest()){
				List<Messaging.SendEmailResult> res = Messaging.sendEmail(lst_msgs);
			}

		} catch (Exception ex) {
			BI_LogHelper.generate_BILog('BI2_EmailMessage.sendEmailsAndTasks', 'BI_EN', ex, 'Trigger');
		}
	}
}