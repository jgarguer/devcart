/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:       Javier Almirón García
 Company:       New Energy Aborda
 Description:   Test for BI_O4_Test_UpdateCIs
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 21/01/2018          Javier Almirón García      Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public with sharing class BI_O4_Test_UpdateCIs_TEST {
	
	@isTest static void BI_O4_Test_UpdateCIs_TEST() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
		userTGS.TGS_Is_BI_EN__c = true;
		insert userTGS;

		 Map<Integer, BI_bypass__c> mapa;

		List <Recordtype> rType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_Ciclo_completo','TGS_Legal_Entity','TGS_Holding')];


         Map <String, Id> rT = new Map <String, Id>();

         for ( Recordtype r : rType){

         	rT.put (r.DeveloperName , r.Id);
         }

	    BI_MigrationHelper.skipAllTriggers();
	    mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, false, false, false);
	    //BI_MigrationHelper.disableBypass(mapa);

        //try{
			Account testAccount = new Account(Name = 'testAcc', 
											  BI_Country__c = 'Argentina',
											  BI_Segment__c = 'test',
											  BI_Subsegment_Regional__c = 'test',
											  BI_Territory__c = 'test',
											  RecordTypeId =  rT.get('TGS_Holding'),
											  TGS_Account_Category__c = 'GA'
											  );
      		insert testAccount;    

      		DateTime dtNow = DateTime.now();
		    Test.startTest();

		   	Opportunity oppLocal = new Opportunity(Name = 'TestSubop1' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = 'F1 - Closed Won',
                                          AccountId = testAccount.Id,
                                          RecordTypeId = rT.get('BI_Ciclo_completo'),
                                          BI_Fecha_de_cierre_real__c = Date.today()
                                          ); 

		   	insert oppLocal;

		    NE__Order__c ord = new NE__Order__c(NE__AccountId__c = testAccount.Id,
                                                NE__BillAccId__c = testAccount.Id,
                                                NE__OptyId__c = oppLocal.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = testAccount.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
	        insert ord;
	        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

	        NE__Product__c pord = new NE__Product__c();
	         pord.Offer_SubFamily__c = 'a-test;';
	         pord.Offer_Family__c = 'b-test';
	         pord.BI_Rama_Digital__c = 'asdfasdf';
	        
	        insert pord; 

	        NE__Catalog__c cat = new NE__Catalog__c();
	        insert cat;
	        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
	            NE__Catalog_Id__c = cat.Id, 
	            NE__ProductId__c = pord.Id, 
	            NE__Change_Subtype__c = 'test', 
	            NE__Disconnect_Subtype__c = 'test');
	        insert catIt;
	               
	        //Insertamos el order item asociado a la order
	        NE__OrderItem__c oi1 = new NE__OrderItem__c(
	            NE__OrderId__c = ord.Id,
	            NE__Account__c = testAccount.Id,
	            NE__ProdId__c = pord.Id,
	            NE__CatalogItem__c = catIt.Id,
	            NE__qty__c = 1,
	            BI_O4_Parent_Qty__c = 1,
	            NE__OneTimeFeeOv__c = 1000,
	            BI_Ingreso_Recurrente_Anterior_Producto__c = 0,
	            NE__RecurringChargeOv__c = 3000,
	            BI_O4_TdCP__c = 1,
	            //BI_Tasa_RecurringChargeOv__c = 100,
	            CurrencyISOCode = 'ARS'        
        	);

        	List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
	        lst_itemToInsert.add(oi1);

        	NE__OrderItem__c oi2 = new NE__OrderItem__c(
	            NE__OrderId__c = ord.Id,
	            NE__Account__c = testAccount.Id,
	            NE__ProdId__c = pord.Id,
	            NE__CatalogItem__c = catIt.Id,
	            NE__qty__c = 1,
	            BI_O4_Parent_Qty__c = 1,
	            NE__OneTimeFeeOv__c = 1000,
	            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
	            //BI_Tasa_RecurringChargeOv__c = 100,
	            NE__RecurringChargeOv__c = 3000,
	            BI_O4_TdCP__c = 1,
	            CurrencyISOCode = 'ARS'        
        	);
      
	        lst_itemToInsert.add(oi2);

	        	insert lst_itemToInsert;
	
	        	BI_O4_Test_UpdateCIs updateCIs = new BI_O4_Test_UpdateCIs(); 
				updateCIs.query2 ='and (NE__OrderId__r.NE__OptyId__r.BI_Fecha_de_cierre_real__c >= 2017-01-01 and NE__OrderId__r.NE__OptyId__r.BI_Fecha_de_cierre_real__c <= TODAY)';
				Id batchId = Database.executeBatch(updateCIs);
				
				//List <NE__OrderItem__c> oiCheck = new List <NE__OrderItem__c>();
	
    //    		oiCheck = [SELECT id, NE__OrderItem__c.BI_O4_Parent_Qty__c,  BI_O4_MRC_Product_Qt_Presup__c, BI_Tasa_OneTimeFeeOv__c ,BI_O4_NRC_Product_Qt_Presup__c,BI_Tasa_RecurringChargeOv__c , BI_Ingreso_Recurrente_Anterior_Producto__c from NE__OrderItem__c];
	
    //    		for (NE__OrderItem__c oi : oiCheck){
	
    //    			System.Debug ('BBB oi.BI_O4_MRC_Product_Qt_Presup__c = ' + oi.BI_O4_MRC_Product_Qt_Presup__c);
    //    			System.Debug ('BBB oi.BI_O4_NRC_Product_Qt_Presup__c = ' + oi.BI_O4_NRC_Product_Qt_Presup__c);
    //    			System.Debug ('BBB oi.BI_Tasa_RecurringChargeOv__c = ' + oi.BI_Tasa_RecurringChargeOv__c);
    //    			System.Debug ('BBB oi.BI_O4_Parent_Qty__c = ' + oi.BI_O4_Parent_Qty__c);
	
    //    			//System.assertEquals(oi.BI_O4_MRC_Product_Qt_Presup__c, oi.BI_Tasa_RecurringChargeOv__c * oi.BI_O4_Parent_Qty__c);
    //    			System.assertEquals(oi.BI_O4_NRC_Product_Qt_Presup__c, oi.BI_Tasa_OneTimeFeeOv__c * oi.BI_O4_Parent_Qty__c);
    //    		}

        		Test.stopTest();
	}
}