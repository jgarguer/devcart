@isTest
global class TGS_CallSigma_TEST  implements HttpCalloutMock {

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
         
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"foo":"bar"}');
        res.setStatusCode(200);
        return res;
    }


    
    @isTest static void sentToSigmaCase_test() {

        Case caseTest = TGS_Dummy_Test_Data.dummyOrderCase();
        String status = 'Resolved';
        String statusReason = '';
        Date RFS_Date = Date.today();
        String sigmaID = '12345';
        String Sigma_WH_Site_ID = '00000000-TEST-00000000';

        Map<Id,String> oldStatus = new Map<Id,String>{caseTest.Id=>'In Progress'};
        Map<Id,String> oldStatusReason = new Map<Id,String>{caseTest.Id=>'In Provision'};

        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;

        System.runAs(admin){

            Test.startTest();

            // Set mock callout class
            Test.setMock(HttpCalloutMock.class, new TGS_CallSigma_TEST());

            /*STATUS 1*/
            TGS_CallSigma.sentToSigmaCase(caseTest.Id, status, statusReason, oldStatus, oldStatusReason, RFS_Date, sigmaID, Sigma_WH_Site_ID);

            /*STATUS 2*/
            status = 'Closed';
            oldStatus.clear();
            oldStatus.put(caseTest.Id, 'Resolved');
            oldStatusReason.clear();
            oldStatusReason.put(caseTest.Id, '');

            TGS_CallSigma.sentToSigmaCase(caseTest.Id, status, statusReason, oldStatus, oldStatusReason, RFS_Date, sigmaID, Sigma_WH_Site_ID);

            /*STATUS 3*/
            status = 'In Progress';
            statusReason = 'In Provision';
            oldStatus.clear();
            oldStatus.put(caseTest.Id, 'Resolved');
            oldStatusReason.clear();
            oldStatusReason.put(caseTest.Id, '');

            TGS_CallSigma.sentToSigmaCase(caseTest.Id, status, statusReason, oldStatus, oldStatusReason, RFS_Date, sigmaID, Sigma_WH_Site_ID);

            /*STATUS 4*/
            status = 'Pending';
            statusReason = 'In Provision';
            oldStatus.clear();
            oldStatus.put(caseTest.Id, 'In Progress');
            oldStatusReason.clear();
            oldStatusReason.put(caseTest.Id, 'In Provision');

            TGS_CallSigma.sentToSigmaCase(caseTest.Id, status, statusReason, oldStatus, oldStatusReason, RFS_Date, sigmaID, Sigma_WH_Site_ID);

            /*STATUS 5*/
            status = 'In Progress';
            statusReason = 'In Provision';
            oldStatus.clear();
            oldStatus.put(caseTest.Id, 'Closed');
            oldStatusReason.clear();
            oldStatusReason.put(caseTest.Id, '');

            TGS_CallSigma.sentToSigmaCase(caseTest.Id, status, statusReason, oldStatus, oldStatusReason, RFS_Date, sigmaID, Sigma_WH_Site_ID);
            Test.stopTest();
        }
    }

    @isTest static void sentToSigmaCase2_test() {

        Case caseTest = TGS_Dummy_Test_Data.dummyOrderCase();
        String status = 'In Progress';
        String statusReason = 'In Provision';
        Date RFS_Date = Date.today();
        String sigmaID = '12345';
        String Sigma_WH_Site_ID = '00000000-TEST-00000000';

        Map<Id,String> oldStatus = new Map<Id,String>{caseTest.Id=>'Pending'};
        Map<Id,String> oldStatusReason = new Map<Id,String>{caseTest.Id=>'In Provision'};

        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;

        System.runAs(admin){

            Test.startTest();

            // Set mock callout class
            Test.setMock(HttpCalloutMock.class, new TGS_CallSigma_TEST());

            /*STATUS 1*/
            TGS_CallSigma.sentToSigmaCase(caseTest.Id, status, statusReason, oldStatus, oldStatusReason, RFS_Date, sigmaID, Sigma_WH_Site_ID);

            Test.stopTest();
        }
    }
    
    @isTest static void sentToSigmaWI_test() {
        
        String sigmaID = '12345';
        String Sigma_WH_Site_ID = '00000000-TEST-00000000';
        String note = 'Test';

        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;

        System.runAs(admin){
            Test.startTest();
            // Set mock callout class
            Test.setMock(HttpCalloutMock.class, new TGS_CallSigma_TEST());

            TGS_CallSigma.sentToSigmaWI(sigmaID, Sigma_WH_Site_ID, note);
            Test.stopTest();
        }
    }

    @isTest static void actualizacionIdOrdering_test() {
        
        String externalID = '12345';
        String Sigma_WH_Site_ID = '00000000-TEST-00000000';
        String SRM_Request_ID = '00000000-TEST-00000001';
        String caseNumber = '000001';

        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;

        System.runAs(admin){
            Test.startTest();
            // Set mock callout class
            Test.setMock(HttpCalloutMock.class, new TGS_CallSigma_TEST());

            TGS_CallSigma.actualizacionIdOrdering(externalID, Sigma_WH_Site_ID, SRM_Request_ID, caseNumber);
            Test.stopTest();
        }
    }

    @isTest static void setAttachments_test() {
        
        Case caseTest = TGS_Dummy_Test_Data.dummyOrderCase();

        TGS_Work_Info__c workInfoTest = new TGS_Work_Info__c(TGS_Case__c = caseTest.Id , TGS_Description__c = 'Test workinfo description', TGS_Public__c = false);
        insert workInfoTest;
        List<Attachment> listAtt = new List<Attachment>();
        Attachment attachmentTest = new Attachment(Name = 'Test Attachment', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
        listAtt.add(attachmentTest);
        Attachment attachmentTest2 = new Attachment(Name = 'Test Attachment2', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
        listAtt.add(attachmentTest2);
        Attachment attachmentTest3 = new Attachment(Name = 'Test Attachment3', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
        listAtt.add(attachmentTest3);

        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;

        System.runAs(admin){
            Test.startTest();
            TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO arrayFullAtt = TGS_CallSigma.setAttachments(listAtt);
            Test.stopTest();
        }
    }

    @isTest static void setAssetAccountsFromOrderInClosed_test() {

        Case caseTest = TGS_Dummy_Test_Data.dummyOrderCase();

        // Level 1: Holding
        //Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.TGS_Holding).getRecordTypeId();
        /*Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        Account holding = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Holding + String.valueOf(Math.random()),
            RecordTypeId = rtId,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True);
        insert holding;*/
        
        // Level 2: Customer Country
        //rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.TGS_Customer_Country).getRecordTypeId();
        /*rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY);
        Account customerCountry = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Customer_Country + String.valueOf(Math.random()),
            RecordTypeId = rtId,
            BI_Country__c = 'Spain', // JMF 19/10/2016 - Add Country
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id,
            ParentId = holding.Id);
        insert customerCountry;*/
        
        // Level 3: Legal Entity
        //rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.TGS_Legal_Entity).getRecordTypeId();
        /*rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
        Account legalEntity = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Legal_Entity + String.valueOf(Math.random()),
            RecordTypeId = rtId,
            ParentId = customerCountry.Id,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id);
        insert legalEntity;*/
        
        // Level 4: Business Unit
        //rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.TGS_Business_Unit).getRecordTypeId();
        /*rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
        Account businessUnit = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Business_Unit + String.valueOf(Math.random()),
            RecordTypeId = rtId,
            ParentId = legalEntity.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id);
        insert businessUnit;*/

        // Level 5: Cost Center
        //rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.TGS_Cost_Center).getRecordTypeId();
        //24/10/2016 JCT Added related accounts fields.Lines 263-265.
        /*rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
        Account costCenter = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Cost_Center + String.valueOf(Math.random()),
            RecordTypeId = rtId,
            ParentId = businessUnit.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id,
            TGS_Aux_Business_Unit__c = businessUnit.Id,
            TGS_Aux_Customer_Country__c = customerCountry.Id,
            TGS_Aux_Legal_Entity__c = legalEntity.Id
            );
        insert costCenter;

        BI_Punto_de_instalacion__c site = TGS_Dummy_Test_Data.dummyPuntoInstalacion(businessUnit.Id);
        NE__Order__c orderTest = TGS_Dummy_Test_Data.dummyOrder();*/

        //Asset
        /*Id rtIdAsset = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Asset');
        NE__Order__c assetTest = new NE__Order__c(RecordTypeId = rtIdAsset, NE__Type__c = 'Asset');
        insert assetTest;

        NE__Asset__c commAsset = new NE__Asset__c();
        insert commAsset;
        assetTest.NE__Asset__c = commAsset.Id;
        update assetTest;

        orderTest.NE__AccountId__c = legalEntity.Id; //LE
        orderTest.NE__BillAccId__c = costCenter.Id; //CC
        orderTest.NE__ServAccId__c = businessUnit.Id; //BU
        orderTest.Site__c = site.Id; //Site
        orderTest.TGS_Holding_Name__c = holding.Name;
        orderTest.NE__Asset__c = commAsset.Id;*/

        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;

        System.runAs(admin){
            Test.startTest();
            /*update orderTest;

            caseTest.Order__c = orderTest.Id;
            caseTest.Asset__c = assetTest.Id;
            caseTest.TGS_RFB_date__c = Date.today();
            caseTest.TGS_RFS_date__c = Date.today();
            update caseTest;*/

            TGS_CallSigma.setAssetAccountsFromOrderInClosed(caseTest.Id);
            Test.stopTest();
        }
    }
    
}