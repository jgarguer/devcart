public class NE_OrderItemTriggerHelper {
	private static Map<Id, String> map_RTOI;
	private static Map <Id, Profile> map_PROFILES;
	private static Set<Id> set_IdProduct = new Set<Id>();
	private static Map<String,NE__Product__c> map_IdProduct;

	public static Map<Id, String> getMapRTOI(){
		if(map_RTOI == null){
			map_RTOI = new Map<Id,String>();
			for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'NE__OrderItem__c']){
                map_RTOI.put(rt.Id,rt.DeveloperName);
            }
		}
		return map_RTOI;
	}

	public static Map<Id, Profile> getMapProfiles(){
		if(map_PROFILES == null){
			map_PROFILES = new Map <Id, Profile>([SELECT Id,Name FROM Profile]);
		}
		return map_PROFILES;
	}

	public static Map<String,NE__Product__c> getIdProduct(List<Id> lst_ids){

		if(!set_IdProduct.containsAll(lst_ids)){
			set_IdProduct.addAll(lst_ids);
			map_IdProduct = new map<String,NE__Product__c>([SELECT id,recordType.DeveloperName, BI_COT_MEX_Analisis_Economico__c  FROM NE__Product__c WHERE id in :set_IdProduct]);
		} 
		return map_IdProduct;
	}
}