@isTest
private class ADQ_Agrupar_TEST {
	
	@isTest static void  testAgruparFacturasController(){
		ADQ_AgruparFacturasControllerMod1 afc1 = new ADQ_AgruparFacturasControllerMod1();
		
		
			
			List<List<Programacion__c>> matrizProgramaciones = new List<List<Programacion__c>> (); 
			List<Programacion__c> lp = new List<Programacion__c>();
			Factura__c factura1= new Factura__c ();
			insert factura1;
			
			Programacion__c progra = new Programacion__c ();
			progra.Factura__c =  factura1.Id;
			insert progra;
			
			Programacion__c progra2 = new Programacion__c ();
			progra2.Factura__c =  factura1.Id;
			insert progra2;
			
			lp.add (progra);
			lp.add (progra2);
			//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
			Account acc01  = new Account ();
				acc01.Name = 'Cuenta de prueba testCosumos2';
				acc01.BI_Segment__c = 'test';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				//acc01.Raz_n_social__c = 'Cuenta de prueba testCosumos SA CV';
			insert acc01; 
					
			Factura__c Factura = new Factura__c ();
				Factura.Activo__c = true;
				Factura.Cliente__c = acc01.Id;
				Factura.Fecha_Inicio__c = Date.today();
				Factura.Fecha_Fin__c = Date.today() + 20;
				Factura.Inicio_de_Facturaci_n__c = Date.today();
				Factura.IVA__c = '16%';
				Factura.Requiere_Anexo__c = 'SI';
				Factura.RecordTypeId =Schema.SObjectType.Factura__c.getRecordTypeInfosByName().get('Recurrente').getRecordTypeId();
				
			insert Factura;
			
			Programacion__c programacion = new Programacion__c ();
				programacion.Factura__c = Factura.Id;
				programacion.Sociedad_Facturadora_Real__c = 'GTM';
				programacion.Fecha__c = Date.today();
				programacion.Inicio_del_per_odo__c = Date.today();
				programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
			insert programacion;

			Pedido__c pedido = new Pedido__c(Programacion__c = programacion.Id, Name = '10235019', Concepto__c = 'Recurrente mensual', Importe__c = 22181.76, Migrado__c = 'no');
			insert pedido;

			Pedido_PE__c  pedidoPE = new Pedido_PE__c(Programacion__c = programacion.Id, Precio_original__c = 168036, Precio_convertido__c = 168036,
														 CurrencyIsoCode = 'MXN', Concepto__c = 'Primera factura');
			insert pedidoPE;
			
			Programacion__c programacion2 = new Programacion__c ();
				programacion2.Factura__c = Factura.Id;
				programacion2.Sociedad_Facturadora_Real__c = 'GTM';
				programacion2.Fecha__c = Date.today();
				programacion2.Inicio_del_per_odo__c = Date.today();
				programacion2.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion2.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
			insert programacion2;
			
			lp.add (programacion);
			lp.add (programacion2);
			
			Test.setCurrentPageReference(new PageReference('Page.ADQ_AgruparFacturas'));
			System.currentPageReference().getParameters().put('fId', factura.Id);
			
			matrizProgramaciones.add (lp);
		
			afc1.success = false;
			
			afc1.matrizProgramaciones = matrizProgramaciones;
			Test.setCurrentPageReference(new PageReference('Page.ADQ_AgruparFacturas'));
			System.currentPageReference().getParameters().put('fid', factura.Id);
			ApexPages.currentPage().getParameters().put('fid', factura.Id);
			ADQ_AgruparFacturasControllerMod1 afc = new ADQ_AgruparFacturasControllerMod1();
			afc.matrizProgramaciones = matrizProgramaciones;

			afc.validarAgrupacion();
			afc.getListaWrapperProgramaciones();
			afc.getListaWrapperProgramaciones()[0].selected = true;
			afc.creaNuevasProgramaciones();
			afc.crearProgramaciones();
			afc.getTotalMatrizProgramaciones();
			afc.cancelar();
			afc.validarAgrupacion();
			afc.creaMatrizAgrupaciones();
		try{	
		}catch (Exception ex){
  			System.debug(ex);
		}
	}


}