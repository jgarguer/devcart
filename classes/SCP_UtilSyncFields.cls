global class SCP_UtilSyncFields {

    global static void syncAccount(List<Account> accounts) {
        
        List<String> countries = new List<String>();
        List<Id> regionIds = new List<Id>();
                
        for (Account a :accounts) {
            a.Certa_SCP__Sector__c = a.BI_Ambito__c;
            a.industry = a.BI_Sector__c;
            a.Certa_SCP__Micro_Industry__c = a.BI_Subsector__c;                         

            countries.add(a.BI_country__c);
        }

        Map<String,Id> regionSCPMap = new Map<String,Id>();
        
        For (Certa_SCP__Region__c region :[Select id, Name From Certa_SCP__Region__c where Name in :countries]) {
            regionSCPMap.put(region.Name, region.Id);
        }
        
        for (Account a :accounts) {                   
            Id regionId = regionSCPMap.get(a.BI_country__c);
            if (regionId != null) {
                a.Certa_SCP__Region__c = regionId;
            
            }
        }
    
    }
    
    
    global static void syncContacts(List<Contact> contacts) {
        for (Contact c :Contacts) {
            c.Certa_SCP__active__c = c.BI_Activo__c;
        }
    }    

}