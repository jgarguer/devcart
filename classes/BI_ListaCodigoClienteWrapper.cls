//Generated by wsdl2apex

public class BI_ListaCodigoClienteWrapper {
    public class CodigoVO {
        public String codCliente;
        public String codSituacion;
        public String fecAlta;
        private String[] codCliente_type_info = new String[]{'codCliente','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] codSituacion_type_info = new String[]{'codSituacion','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] fecAlta_type_info = new String[]{'fecAlta','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'codCliente','codSituacion','fecAlta'};
    }
    public class ListaCodigoClienteInVO {
        public String rut;
        private String[] rut_type_info = new String[]{'rut','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'rut'};
    }
    public class ListaCodigoClienteOutMsg {
        public BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutVO obtenerListaCodClienteReturn;
        private String[] obtenerListaCodClienteReturn_type_info = new String[]{'obtenerListaCodClienteReturn','http://movistar.cl/CIR/vo/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'obtenerListaCodClienteReturn'};
    }
    public class ListaCodigoClienteInMsg {
        public BI_ListaCodigoClienteWrapper.ListaCodigoClienteInVO listaCodigoClienteInVO;
        private String[] listaCodigoClienteInVO_type_info = new String[]{'listaCodigoClienteInVO','http://movistar.cl/CIR/vo/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'listaCodigoClienteInVO'};
    }
    public class ListaCodigoClienteOutVO {
        public String errorcode;
        public String errormessage;
        public BI_ListaCodigoClienteWrapper.CodigoVO[] lista;
        private String[] errorcode_type_info = new String[]{'errorcode','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] errormessage_type_info = new String[]{'errormessage','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] lista_type_info = new String[]{'lista','http://movistar.cl/CIR/vo/',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'errorcode','errormessage','lista'};
    }
}