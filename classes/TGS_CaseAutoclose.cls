/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Jose Miguel Fierro
 Company:       New Energy Aborda
 Description:   Class that automatically closes cases that have been resolved for a while

 History

 <Date>            <Author>                      <Description>
 18/11/2016        Jose Miguel Fierro            Initial version
 09/01/2017        Jose Miguel Fierro            Modified to not re-process cases constantly
-------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class TGS_CaseAutoclose implements Schedulable {

    public static final String[] SERVICES_NO_PROCESS = new String[] {'mWan', 'mWan - Internet'};


    public void execute(SchedulableContext sc) {
        System.debug('Enqueueing batch job: ' + TGS_CaseAutoclose.launchAutoclose(''));
    }

    // method for easier calling
    // JMF 09/01/2017 - Pass the last Id being processed
    public static Id launchAutoclose(String lastId) {
        lastId = lastId != null ? lastId : '';
        if(Test.isRunningTest()) {
            new ProcessAutoClose(lastId).execute(null);
            return null;
        } else {
            return System.enqueueJob(new ProcessAutoClose(lastId));
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Job that does the closing of the cases (A class cannot be both Schedulable and Queueable)

     History

     <Date>            <Author>                      <Description>
     18/11/2016        Jose Miguel Fierro            Initial version
     11/01/2017		   Jose Miguel Fierro            Added sorting based on creation date, to always start from the oldest, because of Id filter
    -------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class ProcessAutoClose implements Queueable {

        public String query;
        public String backDate;
        public String lastId; // JMF 09/01/2016 - Store the last Id, to avoid reprocessing

        public ProcessAutoClose(String lastId) {
            this.lastId = lastId;
        }

        public void execute(QueueableContext qc) {
            try {
                if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
                
                String noProcessServices = String.join(SERVICES_NO_PROCESS, '\',\'');
                Datetime d = Datetime.now().addDays(-Integer.valueOf(Label.TGS_Autoclose_date));

                backDate = d.formatGmt('yyyy-MM-dd');

                // JMF 09/01/2017 - If lastId does not have an Id, it should be an empty string
                // JMF 09/01/2017 - Id > '' is essentially the same as 1=1
                // JMF 11/01/2017 - Sort by CreateedDate, select only 'Order Management Case' cases
                query = 'SELECT Id FROM Case WHERE Id > \'' +  this.lastId + '\' AND (Status = \'Resolved\' AND Status != \'Closed\') AND TGS_Service__c NOT IN (\'' +noProcessServices+ '\') AND TGS_Resolve_Date__c <= ' + backDate + ' AND RecordType.DeveloperName = \'Order_Management_Case\' ORDER BY CreatedDate ASC LIMIT 1';
                Map<Id, Case> map_case = new Map<Id, Case>((List<Case>)Database.query(query));

                System.debug('Processing ' + map_case.size() + ' cases: ' + map_case.keySet());
                if(!map_case.isEmpty()) {
                    try {
                        for(Case cas : map_case.values()) {
                            cas.Status = 'Closed';
                            cas.TGS_Casilla_Desarrollo__c = true;
                        }

                        // Block un-needed queueables from running, since the limit for child jobs is 1
                        NETriggerHelper.setTriggerFired('TGS_Survey_Launcher_Handler');
                        Constants.isQueueableContext = true;
                        update map_case.values();

                    } catch(Exception exc) {
                        BI_LogHelper.generate_BILog('TGS_CaseAutoclose.execute(QueueableContext)', 'TGS', exc, 'Apex Job');
                    }
                    // JMF 09/01/2017 - size() is always at least 1
                    System.debug('Rescheduling batch job: ' + TGS_CaseAutoclose.launchAutoclose(map_case.values()[map_case.values().size()-1].Id));
                } else {
                    System.debug('Done Processing queue!');
                }
                
            } catch(Exception exc) {
                BI_LogHelper.generate_BILog('TGS_CaseAutoclose.execute(QueueableContext)', 'TGS', exc, 'Apex Class');
                //throw exc;
            }
        }
    }

}