public class BI_Duplicate_ProjectOnOpportunity {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from Milestone1_Project__c where Name LIKE: searchKey order by createdDate DESC limit 10';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static String getuser(){
        String userui;
        userui=UserInfo.getUiThemeDisplayed();
        return userui;
    }
    @AuraEnabled
    public static void getProjectbyNameAndEdit(String Name, String newName,String idOpp){
       System.debug('NAME--'+name+' NEW NAME--'+newName+' ID OPP--> '+ idOpp);
      String searchName = Name;
		 integer deltaday = 0;
       Milestone1_Project__c project = [SELECT Id,
                      Name,
                      Deadline__c,
                      Description__c,
                      Status__c,
                      Total_Expense_Budget__c,
                      Total_Hours_Budget__c,
                      KickOff__c,
                      BI_Project_Type__c,
                      BI_Adherence_HO_model__c,
                      BI_Country__c,
                      BI_O4_Leading_Channel__c,
                                        BI_O4_Project_Customer__c
               FROM Milestone1_Project__c
               WHERE Name =: searchName limit 1
              ];
       System.debug('Project to copy::--'+project);
        
       List<Milestone1_Milestone__c> milestones = [SELECT Id,
                             Name,
                             Parent_Milestone__c,
                             Complete__c,
                             Deadline__c,
                             Description__c,
                             Expense_Budget__c,
                             Hours_Budget__c,
                             KickOff__c,
                            BI_country__c,
                            Predecessor_Milestone__c             
                      FROM Milestone1_Milestone__c
                      WHERE Project__c = :project.Id
                     ];
      	  System.debug('MILESTONES (hitos) to copy::--'+project);
          List<Milestone1_Task__c> tasks= [SELECT Id,
                        Name,
                        Project_Milestone__c,
                        Complete__c,
                        Description__c,
                        Start_Date__c,
                        Assigned_To__c,
                        Due_Date__c,
                        Priority__c,
                        Task_Stage__c,
                        Class__c,
                        BI_Assigned_Role__c,
                        Blocked__c,
                        Blocked_Reason__c,
                        Last_Email_Received__c,
                        Estimated_Hours__c,
                        Estimated_Expense__c,
                 		BI_O4_Activity__c,
                        Predecessor_Task__c
                 FROM Milestone1_Task__c
                 WHERE Project_Milestone__r.Project__c = :project.Id
                ];
        
            if(project.Kickoff__c != null && project.BI_O4_Project_Customer__c != 'passclasstest'){
                deltaday = project.Kickoff__c.daysbetween(system.today());
            }
            
            if (deltaday != 0 && project.Kickoff__c != null){
                project.Kickoff__c = project.Kickoff__c.addDays(deltaday);
            }
                          
            if (deltaday != 0 && project.Deadline__c != null){
              project.Deadline__c = project.Deadline__c.addDays(deltaday);
            }
         System.debug('TAREAS (task) to copy::--'+tasks);
         project.Name = newName;
         Milestone1_Project__c newProj = Milestone1_Clone_Utility.cloneProject(project);
         newProj.BI_Adherence_HO_model__c = '';
       	 newProj.BI_Oportunidad_asociada__c=idOpp;
         System.debug('PROJECT COPIED::--'+newProj);
         insert newProj;
        
        
        //milestones(hitos)
        List<Milestone1_Milestone__c> topMilestones = new List<Milestone1_Milestone__c>();
        List<Milestone1_Milestone__c> bottomMilestones = new List<Milestone1_Milestone__c>();

            for(Milestone1_Milestone__c oldMS : milestones){
               	 if(deltaday != 0 && oldMS.Kickoff__c != null){
                    oldMS.Kickoff__c = oldMS.Kickoff__c.addDays(deltaday);
                 }
                 if(deltaday != 0 && oldMs.Deadline__c != null){
                   oldMs.Deadline__c = oldMs.Deadline__c.addDays(deltaday);
                 }
                if(oldMS.Parent_Milestone__c == null){
                    oldMS.OwnerId =userInfo.getUserId(); 
                    topMilestones.add(oldMS);
                } else {
                    oldMS.OwnerId =userInfo.getUserId();
                    bottomMilestones.add(oldMS);
                }
            }   
         //clone and insert top milestone records
            Map<String, Milestone1_Milestone__c> newTopMilestoneMap = Milestone1_Clone_Utility.cloneMilestonesIntoMap(topMilestones);
            for(Milestone1_Milestone__c newMS : newTopMilestoneMap.values()){
                newMS.Project__c = newProj.Id;
                newMS.Parent_Milestone__c = null;
                newMS.Complete__c = false;
                newMS.Description__c = null;
                newMS.Project__c=newProj.Id;
               
            }
            insert newTopMilestoneMap.values();
            
            //clone and insert sub milestone records
            Map<String, Milestone1_Milestone__c> newBottomMilestoneMap = Milestone1_Clone_Utility.cloneMilestonesIntoMap(bottomMilestones);
            for(Milestone1_Milestone__c newMS : newBottomMilestoneMap.values()){
                newMS.Project__c = newProj.Id;
                newMS.Parent_Milestone__c = newTopMilestoneMap.get(newMS.Parent_Milestone__c).Id;
                newMS.Complete__c = false;
                newMS.Description__c = null;
                newMS.Project__c=newProj.Id;
            }
            insert newBottomMilestoneMap.values();
        	
         //collect all milestones into one map
            Map<String, Milestone1_Milestone__c> allNewMilestoneMap = new Map<String, Milestone1_Milestone__c>();
            allNewMilestoneMap.putAll(newTopMilestoneMap);
            allNewMilestoneMap.putAll(newBottomMilestoneMap);
        
         //clone and insert task records
        	List<Milestone1_Task__c> taskwithNOpredecesor=new List<Milestone1_Task__c>();
            List<Milestone1_Task__c> taskwithpredecesor=new List<Milestone1_Task__c>();
            for(Milestone1_Task__c task : tasks) {
              task.Assigned_To__c = userInfo.getUserId();          
            }
            //List<Milestone1_Task__c> newTasks = Milestone1_Clone_Utility.cloneTasksIntoList(tasks);
            
            List<Milestone1_Task__c> newTasks =new  List<Milestone1_Task__c>();
        	Map<Id,Milestone1_Task__c>mapcv=new Map<Id,Milestone1_Task__c>();
        	
        	  for(Milestone1_Task__c rec : tasks){
                    Milestone1_Task__c newRec = rec.clone(false);
                    newTasks.add(newRec);
                   if(rec.Predecessor_Task__c==null){
                    mapcv.put(rec.id,newRec);
               	   }
       		  }	
            for(Milestone1_Task__c newTask : newTasks){
				// N0001
                if(deltaday != 0 && newTask.Start_Date__c != null){
                    newTask.Start_Date__c = newTask.Start_Date__c.addDays(deltaday);
                }
                
                 if(deltaday != 0 && newTask.Due_Date__c != null){
                    newTask.Due_Date__c = newTask.Due_Date__c.addDays(deltaday);
                }
                newTask.Complete__c = false;
                newTask.Description__c = null;
                // N0001
                System.debug('TAREA'+newTask.Name);
                System.debug('predecesor'+newTask.Predecessor_Task__c);
                System.debug('hito de la tarea-'+ newTask.Project_Milestone__c);
                System.debug('proyecto--'+newProj.Id);
                sYSTEM.debug('HITO NUEVO -'+allNewMilestoneMap.get(newTask.Project_Milestone__c).id);        
                newTask.Project_Milestone__c = allNewMilestoneMap.get(newTask.Project_Milestone__c).Id;
                
                
                System.debug('Tarea -'+newTask.Name+'-hito nuevo-'+ newTask.Project_Milestone__c);
                if(newTask.Predecessor_Task__c==null){
                    taskwithNOpredecesor.add(newTask);    
                }
               
                if(newTask.Predecessor_Task__c!=null){
                    taskwithpredecesor.add(newTask);
                }
                //newTask.Project__c=newProj.Id;
            }
        System.debug('mapcv-->'+mapcv);
        	 System.debug('task with NO PREDE--'+taskwithNOpredecesor);
        		insert taskwithNOpredecesor;
        
        
        	System.debug('task with PREDE--'+taskwithpredecesor);
    
        	FOR(Milestone1_Task__c ttt:taskwithpredecesor){
                System.debug('previous predecesor-'+  ttt.Predecessor_Task__c);
                ttt.Predecessor_Task__c=mapcv.get(ttt.Predecessor_Task__c).id;
                 System.debug('new predecesor-'+  ttt.Predecessor_Task__c);
            }
           		insert taskwithpredecesor;
     
    }
	@AuraEnabled 
    public static void saveProject(Milestone1_Project__c con){
        insert con;
    }
    @AuraEnabled
    public static List < sObject > fetchLookUpValuesByTemplate(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        system.debug('KEY WORD template-->' + searchKeyWord);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'SELECT Id, Name FROM Milestone1_Project__c WHERE Name LIKE: searchKey AND BI_O4_Template__c = true ORDER BY createdDate DESC LIMIT 10';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}