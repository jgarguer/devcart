public class BI_COT_MEX_PREORDER_APP_CTRL {   
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Pantoja    
    Company:       Accenture NEA
    Description:   Methods To App Cotizador Mexico PreOrder Lightning Page 
    Test Class:    COT_MEX_PREORDER_APP_CTRL_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    13/02/2017              Alejandro Pantoja          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Pantoja
    Company:       Accenture NEA
    Description:   Method to load Install Points List in to the component. 
    
    IN:            String of Account Id
    OUT:           List of BI_Punto_de_instalacion__c
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/02/2017              Alejandro Pantoja          Initial Version         
    ----------------------------------------------------------------------------------------------------------------------------------------*/

    @AuraEnabled
    public static List<BI_Punto_de_instalacion__c> getInstallPoints(String accId)
    {
            List<BI_Punto_de_instalacion__c> listIP = [Select Id, Name, BI_Sede__c, BI_Sede__r.Name, BI_COT_MEX_Direccion_Completa__c, BI_Sede__r.BI_Provincia__c, 
            BI_Sede__r.BI_Country__c, BI_Sede__r.BI_Codigo_postal__c, BI_COT_MEX_Zona__c, BI_COT_MEX_Selec_Cot__c
            from BI_Punto_de_instalacion__c where BI_Cliente__c =:accId];
        
        return listIP;
    }

   

    //-------------------------------------------------------------------------------------------------------------------------------------------------------
    //Author:        Alejandro Pantoja
    //Company:       Accenture NEA
    //Description:   Method that clean the install points asociated to Oportunity and create the news install points. 
    
    //IN:            String of Account Id, String of Opportunity Id, List of BI_Punto_de_instalacion__c
    //OUT:           Void
    
    //History: 
    
    //<Date>                  <Author>                <Change Description>
    //13/02/2017              Alejandro Pantoja          Initial Version         
    //----------------------------------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static void procesarSedes(List<BI_Punto_de_instalacion__c> lstSedes,String cuenta, String oportunidad)
    {
        List<BI_COT_MEX_PuntoDeInstalacion_AccOpp__c> lst_newSedes = new List<BI_COT_MEX_PuntoDeInstalacion_AccOpp__c>();

        List<BI_COT_MEX_PuntoDeInstalacion_AccOpp__c> lstEliminar = [Select Id, Name from BI_COT_MEX_PuntoDeInstalacion_AccOpp__c where BI_COT_MEX_Opportunity__c =:oportunidad];

        
        if(lstEliminar.size()>0){
            delete lstEliminar;
        }


        for(Integer i=0;i<lstSedes.size();i++){

            lst_newSedes.add(new BI_COT_MEX_PuntoDeInstalacion_AccOpp__c(BI_COT_MEX_Opportunity__c = oportunidad, BI_COT_MEX_Sede__c = lstSedes.get(i).Id));

        }

        if(lst_newSedes.size()>0){

            insert lst_newSedes;
        }

    }


    /* -------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Pantoja
    Company:       Accenture NEA
    Description:   Method that return the install point recorded inside base data-
    
    IN:            String of Account Id, String of Opportunity Id
    OUT:           List of BI_Punto_de_instalacion__c
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/02/2017              Alejandro Pantoja          Initial Version         
    ----------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static List<BI_Punto_de_instalacion__c> getListSelected(String accId, String oppId){

        List<BI_Punto_de_instalacion__c> sedes=new List<BI_Punto_de_instalacion__c>();
        List<String> lstId= new List<String>();
        List<BI_COT_MEX_PuntoDeInstalacion_AccOpp__c> lstRel = [Select Id,BI_COT_MEX_Opportunity__c,BI_COT_MEX_Sede__c from BI_COT_MEX_PuntoDeInstalacion_AccOpp__c where BI_COT_MEX_Opportunity__c =:oppId];

        if(lstRel.size()>0)
        {
            for(Integer p = 0; p < lstRel.size(); p++){

                String aux = lstRel.get(p).BI_COT_MEX_Sede__c+'';
                lstId.add(aux);
                System.debug('Sede: ' + lstRel.get(p).BI_COT_MEX_Sede__c);
            }

            try{

                sedes=[Select Id, Name, BI_Sede__c, BI_Sede__r.Name, BI_COT_MEX_Direccion_Completa__c, BI_Sede__r.BI_Provincia__c, 
                BI_Sede__r.BI_Country__c, BI_Sede__r.BI_Codigo_postal__c, BI_COT_MEX_Zona__c, BI_COT_MEX_Selec_Cot__c
                from BI_Punto_de_instalacion__c where Id in :lstId];
                System.debug('Sedes ANTIGUAS: ' +sedes.size());
                }catch(Exception e){

                    
                }

        }

        return sedes;

    }

     /* -------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Pantoja
    Company:       Accenture NEA
    Description:   Method that return the name of Account. 
    
    IN:            String of Account Id
    OUT:           String Name Account
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/02/2017              Alejandro Pantoja          Initial Version         
    ----------------------------------------------------------------------------------------------------------------------------------------*/

    @AuraEnabled
    public static String getAccname(String varId){

        Account aux = [Select Id,Name from Account where Id =:varId limit 1];
        String cadena = aux.Name;
        return cadena;


    }

      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Pantoja
    Company:       Accenture NEA
    Description:   Method that return the Name of Opportunity
    
    IN:            String of Opportunity Id
    OUT:           String Name of Opportunity
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/02/2017              Alejandro Pantoja          Initial Version         
    ----------------------------------------------------------------------------------------------------------------------------------------*/

    @AuraEnabled
    public static String getOppname(String varId){

        Opportunity aux = [Select Id,Name from Opportunity where Id =:varId limit 1];
        String cadena = aux.Name;
        return cadena;

    }


}