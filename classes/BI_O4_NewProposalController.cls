/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Class BI_O4_NewProposalController for new proposal visualforce
    Test Class:    BI_O4_NewProposalController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Fernando Arteaga          Initial version
    05/10/2016              Fernando Arteaga          New method showSave called when an opportunity is checked/unchecked
    16/11/2016              Alvaro García             Changed showSave
    17/11/2016              Alvaro García             Changed BI_O4_NewProposalController and doValidations
	11/10/2017				Oscar Bartolo			  Add field BI_O4_USER_GSE__c and method getIdOppTeamMember
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_NewProposalController
{
    public Boolean selectAllCheckbox {get; set;}
    public Opportunity parentOpportunity {get; set;}
    public List<OpportunityWrapper> listOpportunity {get; set;}
    private Set<Id> setSelectedOpptyIds {get; set;}
    public Boolean allowProposal {get; set;}
    public Boolean allowSave {get; set;}
    private String retURL;
    private List<OpportunityTeamMember> listOTM;
    private Opportunity opp;
    private Boolean error;
    
    private static final Map<String, RecordType> mapRecordTypes;
    static {
        mapRecordTypes = new Map<String, RecordType>();
        for(RecordType rt : [SELECT Id, DeveloperName
                             FROM RecordType
                             WHERE SObjectType = 'Quote']) {
            mapRecordTypes.put(rt.DeveloperName, rt);
        }
    }
    
    public BI_O4_NewProposalController (ApexPages.StandardController controller) 
    {
        this.error = false;
        this.allowSave = false;
        try 
        {
            if (BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }

            this.parentOpportunity = (Opportunity) controller.getRecord();
            this.allowProposal = this.parentOpportunity.StageName.startsWith('F4');
            this.listOTM = [SELECT TeamMemberRole, UserId, User.Name
                            FROM OpportunityTeamMember
                            WHERE OpportunityId = :controller.getId()];
        }
        catch (Exception e) 
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, BI_O4_QuoteHelper.ERROR_NO_SELECTED));
            return;
        }
        
        opp = [SELECT Id, (SELECT Id, Name, CurrencyIsoCode, BI_Country__c, BI_Productos_numero__c, BI_O4_Service_Line__c, BI_O4_Service_Lines__c, 
                BI_O4_Countries_America__c, BI_O4_Countries_EMEA__c, BI_O4_Countries_Rest_of_the_world__c, Amount, BI_Ingreso_por_unica_vez__c, 
                BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c FROM Oportunidades__r), (SELECT Id, RecordType.DeveloperName FROM Opportunity_Qualify__r) 
                FROM Opportunity WHERE Id = :this.parentOpportunity.Id LIMIT 1];

        
        this.listOpportunity = new List<OpportunityWrapper>();
        
        //for (Opportunity childOpp : [SELECT Id, Name, CurrencyIsoCode, BI_Country__c, BI_Productos_numero__c, BI_O4_Service_Line__c, BI_O4_Service_Lines__c, BI_O4_Countries_America__c, BI_O4_Countries_EMEA__c, BI_O4_Countries_Rest_of_the_world__c, Amount, BI_Ingreso_por_unica_vez__c, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c
        //                             FROM Opportunity 
        //                             WHERE BI_Oportunidad_Padre__c = :this.parentOpportunity.Id ] ) {
        for (Opportunity childOpp : opp.Oportunidades__r) {
        
            this.listOpportunity.add(new OpportunityWrapper(childOpp));
        }
    }

    public PageReference doValidations()
    {
        /*if (!this.allowProposal)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You cannot create a proposal if stage is not F4'));
            this.allowSave = false;
        }
        else */if ((this.parentOpportunity.BI_O4_Opportunity_Type__c == 'Centralized' || this.parentOpportunity.BI_O4_Opportunity_Type__c == 'Global Distributed') && !hasOppQualifyRecord())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.BI_O4_Qualification_Needed));
            this.error = true;
            //this.allowSave = false;
        }
        else if (opp.Opportunity_Qualify__r.size() == 1 && opp.Opportunity_Qualify__r[0].RecordType.DeveloperName == 'BI_O4_Not_Organic_Growth') {
            
            BI_O4_Opportunity_Qualify__c oppQual = [SELECT Id, (SELECT Id FROM Customer_Decision_Making_Models__r), (SELECT Id FROM Risk_Assessmenta__r) 
                                                    FROM BI_O4_Opportunity_Qualify__c WHERE Id = :opp.Opportunity_Qualify__r[0].Id LIMIT 1];
            
            if (oppQual.Customer_Decision_Making_Models__r.size() != 0 && oppQual.Risk_Assessmenta__r.size() == 0) {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.BI_O4_RiskAssetsNeeded));
                this.error = true;
                //this.allowSave = false;
            }
            else if (oppQual.Customer_Decision_Making_Models__r.size() == 0 && oppQual.Risk_Assessmenta__r.size() != 0) {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.BI_O4_DecisionMakingNeeded));
                this.error = true;
                //this.allowSave = false;
            }
            else if (oppQual.Customer_Decision_Making_Models__r.size() == 0 && oppQual.Risk_Assessmenta__r.size() == 0) {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.BI_O4_Decision_y_Risk_Needed));
                this.error = true;
                //this.allowSave = false;
            }
        }
        return null;
    }
    //public PageReference reValidate() {
    //    this.allowSave = true;
    //    return doValidations();
    //}

    private Set<String> getOppsWithoutProducts(List<Opportunity> listSubOpp)
    {
        Set<String> setOppFail = new Set<String>();
        for (Opportunity opp: listSubOpp)
        {
            System.debug('productos:' + opp.BI_Productos_numero__c);
            if (opp.BI_Productos_numero__c == null || opp.BI_Productos_numero__c == 0)
                setOppFail.add(opp.Name);
        }
        return setOppFail;
    }
    
    private Boolean hasOppQualifyRecord()
    {
        Integer records = [SELECT COUNT()
                           FROM BI_O4_Opportunity_Qualify__c
                           WHERE BI_O4_Opportunity__c = :this.parentOpportunity.Id];
        
        return records > 0 ? true : false;  
    }
    
    public Pagereference saveButton()
    {   system.debug( '############################# Save Button entra');
        System.Savepoint savePoint;
        List<Opportunity> listSelectedOpportunity = getListSelectedOpportunity();
        
        if (listSelectedOpportunity.isEmpty())
        {   system.debug('No Opportunity');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, BI_O4_QuoteHelper.ERROR_NO_SELECTED));
            return null;
        }
        
        try
        {
            savePoint = Database.setSavepoint();
            
            if (BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }

            
            
            Set<String> setOppNames = getOppsWithoutProducts(listSelectedOpportunity);
            if (!setOppNames.isEmpty())
            {
                for (String oppName: setOppNames)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, oppName + ' ' + Label.BI_O4_Opps_Without_Products));
                }
                this.allowSave = false;
                return null;
            }
        
            Quote quote = newQuote(listSelectedOpportunity);
            insert quote;
            
            List<BI_O4_Proposal_Item__c> listProposal = newListProposal(quote, listSelectedOpportunity);
            insert listProposal;
            
            List<Quote> myQuotesFromOpportunity = [select Id from Quote where Opportunityid= :parentOpportunity.id AND id != :quote.Id];
            if(myQuotesFromOpportunity.isEmpty() ){
                parentOpportunity.SyncedQuoteId=quote.Id;
                update this.parentOpportunity;
            }
            
            return new PageReference('/' + quote.Id);
        }
        
        catch (Exception e)
        {
            Database.rollback(savePoint);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            this.allowSave = false;
            return null;
        }
    }

    private List<Opportunity> getListSelectedOpportunity()
    {
        List<Opportunity> listSubOpportunity = new List<Opportunity>();
        setSelectedOpptyIds = new Set<Id>();
        for (OpportunityWrapper wrapper: this.listOpportunity)
        {
            if (wrapper.checked)
            {
                listSubOpportunity.add(wrapper.opportunity);
                this.setSelectedOpptyIds.add(wrapper.opportunity.Id);
            }
        }
        
        return listSubOpportunity;
    }
    
    private Quote newQuote(List<Opportunity> listSubOpp) 
    {   
        Set<String> setCountries = new Set<String>();
        Set<String> setServiceLines = new Set<String>();

        for (Opportunity opp: listSubOpp)
        {
            if (opp.BI_Country__c != null)
                setCountries.add(opp.BI_Country__c);
                
            if (opp.BI_O4_Service_Lines__c != null)
                setServiceLines.addAll(opp.BI_O4_Service_Lines__c.split(';'));
        }
        
        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(this.parentOpportunity.Name);
        quote.RecordTypeId = mapRecordTypes.get('BI_O4_Pending_assign_to_GSE').Id;
        quote.OpportunityId = this.parentOpportunity.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(this.parentOpportunity.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = String.join(new List<String>(setCountries), ';');
        quote.BI_O4_Service_Lines__c = !setServiceLines.isEmpty() ? String.join(new List<String>(setServiceLines), ';') : null;
        quote.BI_O4_Regional_PresalesTEXT__c = getOppTeamMember('Regional Presales');
        quote.BI_O4_GSE__c = getOppTeamMember('GSE Principal') != null ? getOppTeamMember('GSE Principal') : getOppTeamMember('Global Solutions Engineer Principal');
        quote.BI_O4_USER_GSE__c = getIdOppTeamMember('GSE Principal') != null ? getIdOppTeamMember('GSE Principal') : getIdOppTeamMember('Global Solutions Engineer Principal');

        return quote;
    }

    private List<BI_O4_Proposal_Item__c> newListProposal (Quote quote, List<Opportunity> listOpp)
    {
        List<BI_O4_Proposal_Item__c> listProposal = new List<BI_O4_Proposal_Item__c>();
        
        for (Opportunity opp : listOpp)
        {
            BI_O4_Proposal_Item__c propItem = new BI_O4_Proposal_Item__c();
            propItem.BI_O4_SubOpportunity__c = opp.Id;
            propItem.BI_O4_Quote__c = quote.Id;
            propItem.BI_O4_Service_Lines__c = opp.BI_O4_Service_Lines__c;
            propItem.BI_O4_Ingreso_Recurrente_Anterior_Config__c = opp.BI_Recurrente_bruto_mensual_anterior__c;
            propItem.BI_O4_Total_Ingresos_por_nica_vez__c = opp.BI_Ingreso_por_unica_vez__c;
            propItem.BI_O4_Total_Recurrente_bruto__c = opp.BI_Recurrente_bruto_mensual__c;
            propItem.BI_O4_Full_Contract_Value_FCV__c = opp.Amount;
            propItem.CurrencyIsoCode = opp.CurrencyIsoCode;
            listProposal.add(propItem);
        }
        
        return listProposal;
    }
    
    private String getOppTeamMember(String role)
    {
        for (OpportunityTeamMember otm: this.listOTM)
        {
            if (String.valueOf(otm.TeamMemberRole) == role)
                return otm.User.Name;
        }
        
        return null;
    }
    
    private String getIdOppTeamMember(String role)
    {
        for (OpportunityTeamMember otm: this.listOTM)
        {
            if (String.valueOf(otm.TeamMemberRole) == role)
                return otm.UserId;
        }
        
        return null;
    }
    
    public PageReference cancelButton()
    {
        String url;
        if ((this.retURL == null) || (this.retURL == ''))
        {
            url = '/' + this.parentOpportunity.Id;
        }
        else
        {
            url = this.retURL;
        }
        return new PageReference(url);
    }
    
    public class OpportunityWrapper
    {
        public Boolean checked          {get; set;}
        public Opportunity opportunity  {get; set;}
        
        public OpportunityWrapper(Opportunity opportunity)
        {
            this.checked = false;
            this.opportunity = opportunity;
        }
    }
    
    public PageReference selectAll()
    {
        for (OpportunityWrapper wrapper : listOpportunity)
        {
            wrapper.checked = selectAllCheckbox;
        }
        return showSave(); 
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Sets allowSave property to true when an opportunity is checked/unchecked

    In:
    Out:           PageReference: page to return
    
    History: 
    
    <Date>          <Author>                <Change Description>
    05/10/2016      Fernando Arteaga        Initial Version
    17/11/2016      Alvaro García           change the conditions to improve functinality
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference showSave()
    {
        Boolean isCheck = false;
        for (OpportunityWrapper wrapper : listOpportunity) {
            if (wrapper.checked) {
                isCheck = true;
                break;
            }
        }

        if (!isCheck || this.error) {
            this.allowSave = false;
        }
        else {
            this.allowSave = true;
        }

        //if (!this.allowSave && !((this.parentOpportunity.BI_O4_Opportunity_Type__c == 'Centralized' || this.parentOpportunity.BI_O4_Opportunity_Type__c == 'Global Distributed') && !hasOppQualifyRecord()))
        //    this.allowSave = true;


        System.debug(LoggingLevel.FINEST, this.allowSave);
        return null;
    }
}