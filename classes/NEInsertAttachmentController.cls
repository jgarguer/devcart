public with sharing class NEInsertAttachmentController {
    
    public String orderId {get;set;}
    public String createOpty {get;set;}
    public String siteId {get;set;}
    public list<NE__Catalog_Item__c> catItems {get;set;}
    public list<NE__OrderItem__c> ordItems {get;set;}
    public list<orderItemWrapper> ordItemWrapList {get;set;}
    public list<orderItemWrapper> ordItemListWithAttach {get;set;}
    public Boolean error {get;set;}
    public Map<String,String> pageParameters;
    public Boolean profileTGS;
    public Case ordCase;
    
    
    public NEInsertAttachmentController() {
        orderId = Apexpages.currentPage().getParameters().get('orderId');
        pageParameters = ApexPages.currentPage().getParameters();
        
        NE__Order__c ord = [SELECT NE__CatalogId__c, Case__c, Owner_Profile__c FROM NE__Order__c WHERE Id =: orderId];
        catItems = [SELECT Id, NE__ProductId__r.Name, NE__Technical_Behaviour__c 
                    FROM NE__Catalog_Item__c 
                    WHERE NE__Catalog_Id__c =: ord.NE__CatalogId__c AND NE__Technical_Behaviour__c includes ('Attachment Required')];
        ordItems = [SELECT Id, Name, NE__ProdId__r.Name 
                    FROM NE__OrderItem__c 
                    WHERE NE__OrderId__c =: orderId AND NE__CatalogItem__c IN :catItems];
        
        ordItemWrapList = new list<orderItemWrapper>();
        ordItemListWithAttach = new list<orderItemWrapper>();
        
        if(ordItems.size() > 0) {
            
            for(NE__OrderItem__c oi : ordItems) {
                orderItemWrapper oiwrap = new orderItemWrapper(oi);
                if(oiwrap.attachment.Body == null)
                    ordItemWrapList.add(oiwrap);
                else
                    ordItemListWithAttach.add(oiwrap);
            }
            
            if(ordItemListWithAttach.size() == 0)
                error = false;
            else
                error = true;
        }
                
        profileTGS = NETriggerHelper.getTGSTriggerFired(ord.Owner_Profile__c);
        ordCase = new Case();
        if(ord.Case__c != null)
            ordCase = [SELECT Id, TGS_Product_Configured__c,TGS_Needs_Configuration__c FROM Case WHERE Id =: ord.Case__c];
        }

    
    
    // If there are not order items with 'Attachment Required' technical behavior OR all order items have already
    // an attachment, skip InsertAttachment page and redirect to order summary.
    public PageReference pageRedirect() {
        system.debug('ordItems.size() = '+ordItems.size()+'; ordItemListWithAttach.size() = '+ordItemListWithAttach.size());
        
        if(ordItems.size() == 0 || (ordItemListWithAttach.size() == ordItems.size())) {
            system.debug('Page redirecting to order summary');
            if(profileTGS == true && ordCase != null )
            {
                ordCase.TGS_Needs_Configuration__c = false;
                update ordCase;
            }
            PageReference page = new PageReference('/apex/NEPageRedirect');
            page.getParameters().putAll(pageParameters);
            page.setRedirect(true);
            return page;
        }
        
        return null;
    }
    
    
    public PageReference nextPage() {
        list<Attachment> attachToInsert = new list<Attachment>();
        try 
        {
            for(orderItemWrapper oiwrap : ordItemWrapList) {            
                if(oiwrap.attachment.Body != null) {
                    oiwrap.attachment.OwnerId = UserInfo.getUserId();
                    oiwrap.attachment.ParentId = oiwrap.orderitem.Id;
                    oiwrap.attachment.isPrivate = false;

                    attachToInsert.add(oiwrap.attachment);
                }
            }
            insert attachToInsert;
            system.debug('Order items without attachment: '+(ordItemWrapList.size()-attachToInsert.size()));
            
            PageReference page;
            if(ordItemWrapList.size() == attachToInsert.size()) {
                //Page will be redirected to order summary
                page = new PageReference('/apex/NEPageRedirect');
                page.getParameters().putAll(pageParameters);
                if(profileTGS == true && ordCase != null && ordCase.TGS_Needs_Configuration__c)
                {
                    ordCase.TGS_Needs_Configuration__c = false; // Set false, when save the configuration
                    update ordCase;
                }
            } else {
                page = new PageReference(ApexPages.currentPage().getUrl());
            }
            page.setRedirect(true);
            return page;
        }
        catch (Exception e)
        {
            System.debug('Exception found: '+e+' at line:'+e.getLineNumber()); 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' At line: '+e.getLineNumber()));
        }

        return null;
    }
    
    
    // Wrapper class with order item's information and related attachments
    public with sharing class orderItemWrapper {
        
        public NE__OrderItem__c orderitem {get;set;}
        public Attachment attachment {get;set;}
        
        public orderItemWrapper(NE__OrderItem__c ordIt) {
            orderitem = ordIt;
            list<Attachment> att = [SELECT Id, Body, BodyLength, ContentType, Description, Name, OwnerId, ParentId, IsPrivate 
                                    FROM Attachment 
                                    WHERE ParentId =: orderitem.Id];
            if(att.size() > 0)
                attachment = att[0];
            else
                attachment = new Attachment();
        }
        
    }
    
    
}