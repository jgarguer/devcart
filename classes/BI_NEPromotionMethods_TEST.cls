@isTest

private class BI_NEPromotionMethods_TEST { 
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Campaign Triggers 
    Test Class:    BI_NE_PromotionMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    05/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
           
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
    
    History: 	   Method that checks if NE__Promotion__c.Not_available_for_campaigns__c has changed to false and changes the related Campaign isActive to false
    
    <Date>                  <Author>                <Change Description>
    05/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void notAvailableTest (){
		String inProgress = 'En curso';
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		NE__Catalog__c cat = new NE__Catalog__c (Name = 'test catalog',
												 NE__StartDate__c = Datetime.now());
		insert cat;
		
		List<NE__Promotion__c> lst_promo = BI_DataLoad.loadPromotions(200, cat.Id);
		
		List<Campaign> lst_camp = BI_DataLoad.loadCampaignsforPromo(lst_promo);
		
		for (Campaign camp:lst_camp){
			camp.IsActive = true;
			camp.Status = inProgress;
		}
		Test.startTest();
		update lst_camp;
		
		for (NE__Promotion__c promo:lst_promo){
			promo.Not_available_for_campaigns__c = true;
		}
		update lst_promo;
		
		Campaign [] camp = [SELECT Id, NE__Promotion__c, IsActive FROM Campaign WHERE NE__Promotion__c in :lst_promo];
		
		for (Campaign camp0:camp){
			system.assertequals(camp0.IsActive, false);
		}
		Test.stopTest();
		
	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}