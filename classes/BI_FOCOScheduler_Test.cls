@isTest
private class BI_FOCOScheduler_Test {
  public static String CRON_EXP = '0 0 0 15 9 ? 2022';

  @isTest static void test_method_one() {

    BI_FOCO_Pais__c cs = new BI_FOCO_Pais__c(Name='Testx001',Batch_Size__c=10,BI_BatchDelete_Size__c=10, Currency_ISO_Code__c='ARS', Opportunity_Probability__c=50, Notify_Email_List__c='testing@somebody.com'); 
    upsert cs;
    //Region__c pais = new Region__c(CurrencyIsoCode = 'ARS', Name='Testx001');
    //upsert pais;

    BI_Periodo__c per = new BI_Periodo__c(BI_Mes_Ano__c='01-2015', Name='Enero 2015');
    upsert per;

    BI_Rama__c rama = new BI_Rama__c(BI_Nombre__c='Testx001');
    upsert rama;  

    Test.startTest();


    String jobId = System.schedule('ScheduleApexClassTest',
      CRON_EXP, 
      new BI_FOCOScheduler());

      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
      NextFireTime
      FROM CronTrigger WHERE id = :jobId];      

         // Verify the expressions are the same
         System.assertEquals(CRON_EXP, 
          ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-09-15 00:00:00', 
        String.valueOf(ct.NextFireTime));
      // Verify the scheduled job hasn't run yet.

      String now = '' + String.valueOf(DateTime.now());
      List<Account>  aList= new list<Account>();

      //Create Test Accounts
      // 20/09/2017        Angel F. Santaliestra Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
      Account a1 = new Account(Name='Testx001', /*BI_Pais_ref__c  = pais.Id*/ BI_Country__c = 'Testx001', BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test',BI_Validador_Fiscal__c='Testx001');
      // END 20/09/2017        Angel F. Santaliestra
      aList.add(a1);
      
      insert aList;
      List<BI_Registros_Datos_FOCO_Stage__c> sList = new List<BI_Registros_Datos_FOCO_Stage__c> ();

      aList = [Select BI_Validador_Fiscal__c from Account];
      for(Account a: aList){
        BI_Registros_Datos_FOCO_Stage__c s = new BI_Registros_Datos_FOCO_Stage__c(BI_Cliente__c=a.BI_Validador_Fiscal__c, BI_Monto__c='1000', BI_Country__c='Testx001', BI_Periodo__c='1/1/2015', BI_Tipo__c='Backlog (Recurrente)');

        sList.add(s);
      }
      insert sList;
      

            
      //count = [select count() from BI_Registros_Datos_FOCO_Stage__c where BI_Cliente__c like :match];
      //System.assertEquals(10, count);
      
      //Create Opportunities to match

      Test.stopTest();
      
      // Now that the scheduled job has executed after Test.stopTest(), verify that the records are loaded
      
      //count = [SELECT count() FROM BI_Registro_Datos_FOCO__c WHERE BI_Pais__c = 'Argentina' AND  BI_Cliente__c IN (select Id from Account where Name like :match)];

      BI_FOCOScheduler.notifyByEmail('Test Execution', '', 0, '', 'Testx001');
      
      //System.assertEquals(7, count );
      //count = [SELECT count() from BI_Registro_Datos_FOCO__c WHERE BI_Pais__c = 'Perú' AND BI_Cliente__c IN (select Id from Account where Name like '%Test%')];
      //System.assertEquals(3, count);

    }

    @isTest static void test_method_two() {
        // Implement test code
      }

    }