/*************************************************************
 * Author: Javier Lopez Andradas
 * Comapny: gCTIO
 * Description: Batch to deactivate outside periods 
 * 
 * 
 * 
 * <date>		<version>		<desciption>
 * 03/04/2018	1.0				Initial version
 * **********************************************************/
global class FO_Deactivate_periods_job implements Schedulable {
    global void execute(SchedulableContext sc) {
        //MyBatchClass b = new MyBatchClass();
        //database.executebatch(b);
        //
        List<BI_Periodo__c> lst_per = [Select Id,BI_FVI_Fecha_Inicio__c,BI_FVI_Fecha_Fin__c from BI_Periodo__c where FO_Type__c != null] ;
        Date hoy = Date.today();
        for(BI_Periodo__c per : lst_per){
            if(per.BI_FVI_Fecha_Inicio__c<=hoy && per.BI_FVI_Fecha_Fin__c>=hoy){
                per.BI_FVI_ACtivo__c=true;
            }else{
                per.BI_FVI_Activo__c=false;
            }
        }
        update lst_per;
    }
}