@isTest
private class PCA_CustomCalendarEventItem_TEST {

    static testMethod void loadInfoTest() {
        
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        
        User users = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        
         system.runAs(users){ 
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            
            List<Account> accounts2 = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: accounts2){
                    item.OwnerId = users.Id;
                    accList.add(item);
            }
            update accList;
            List<Contact> con = BI_DataLoad.loadPortalContacts(2, accounts2);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
            DateTime day = Datetime.NOW();
            DateTime day10 = day.AddMinutes(10);
            
            system.runAs(user1){
                eventWrapper event1 = new eventWrapper('test','WrapperTest',day,day10,true);
                PCA_CustomCalendarEventItem constructor = new PCA_CustomCalendarEventItem(event1);  
                BI_TestUtils.throw_exception = false;               
                constructor.getEv();
                constructor.getFormatedDate();
                system.assertEquals(constructor.getEv(), event1);
            }
                
            system.runAs(user1){
                eventWrapper event1 = new eventWrapper('test','WrapperTest',day,day10,true);
                PCA_CustomCalendarEventItem constructor = new PCA_CustomCalendarEventItem(event1);
                BI_TestUtils.throw_exception = true;
                constructor.getEv();
                constructor.getFormatedDate();
            }
            system.runAs(user1){
                eventWrapper event1 = new eventWrapper('test','WrapperTest',day,day10,true);
                PCA_CustomCalendarEventItem constructor = new PCA_CustomCalendarEventItem(event1);
                constructor.getEv();
                BI_TestUtils.throw_exception = true;
                constructor.getFormatedDate();
            }
         }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}