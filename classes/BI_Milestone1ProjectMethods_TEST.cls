@isTest
private class BI_Milestone1ProjectMethods_TEST {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_Milestone1ProjectMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/04/2014              Ignacio Llorca          Initial Version
    02/02/2017				Pedro Párraga			Increase coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca 
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_Milestone1ProjectMethods.addOTM
   
    History:
    
    <Date>                  <Author>                <Change Description>
    20/08/2014              Ignacio Llorca             Initial Version   
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void addOTMTest() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
	    	Id idProfile =BI_DataLoad.searchAdminProfile();
	    	
	    	List <User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);	   
		    system.runAs(lst_user[0]){
		    	
		    	List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);

		        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		         
		        List <BI_Tarea_Obligatoria__c> lst_to = BI_DataLoad.loadTareaObligatoria (lst_pais);

			        List<Opportunity> lst_opp = new List<Opportunity>();
			        
			        for(Integer j = 0; j < 6; j++){
			            Opportunity opp = new Opportunity(Name = 'Test'+j,
			                                              CloseDate = Date.today(),
			                                              StageName = Label.BI_F6Preoportunidad,
			                                              AccountId = lst_acc[0].Id,
			                                              BI_Ciclo_ventas__c = Label.BI_Completo,
			                                              BI_Country__c = lst_pais[0]);
			            
			            lst_opp.add(opp);
			        }
			        
			        insert lst_opp;
		        
	    
			        /*
			        for(Integer i = 0; i < 1; i++){
			            Opportunity opp = new Opportunity(Name = 'Test'+i,
			                                              CloseDate = Date.today(),
			                                              StageName = 'F6 – Preoportunidad',
			                                              AccountId = lst_acc[0].id,
			                                              BI_Ciclo_ventas__c = 'Completo');
			            
			            lst_opp.add(opp);
			        }*/
				
				
				//[SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMember WHERE OpportunityId IN :set_mpOpp]
				List <OpportunityTeamMember> lst_otm = BI_DataLoad.loadOpportunityTeamMembers(lst_opp.size(), lst_user[0].Id ,lst_opp);
		        system.debug('BI_Milestone1ProjectMethods_TEST.addOTMTest-> lst_otm: '+lst_otm);
		        List <Milestone1_Project__c> lst_proj = new List<Milestone1_Project__c>();
		         
		        for(Opportunity opp:lst_opp){
		        	for(Integer i=0;i<5;i++){
			        	Milestone1_Project__c proj = new Milestone1_Project__c ();   		
			       		proj.Name = 'TEST PROJ ' + Datetime.now().getTime();
			        	proj.BI_Oportunidad_asociada__c = opp.Id;
			        	proj.BI_Etapa_del_proyecto__c = 'Inicio';
			        	proj.BI_Country__c = lst_pais[0];
			        	proj.BI_Complejidad_del_proyecto__c='Complejo';
			        	lst_proj.add(proj);        	
		        	}
		        }
		        
		        system.debug('BI_Milestone1ProjectMethods_TEST.addOTMTest-> lst_proj: '+lst_proj);
		        insert lst_proj;

		        BI_Configuracion_de_documentacion__c config = new BI_Configuracion_de_documentacion__c (BI_Obligatoriedad__c = Label.BI_Si,
																										BI_Objeto__c = 'Proyecto',
																										BI_Country__c= lst_proj[0].BI_Country__c,
																										BI_Complejidad_del_proyecto__c= lst_proj[0].BI_Complejidad_del_proyecto__c,
																										BI_Etapa_del_proyecto__c=lst_proj[0].BI_Etapa_del_proyecto__c);

				insert config;

		        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
	        
		        Blob bodyBlob2=Blob.valueOf('UnitTestAttachmentBody 2');
		        
		        Attachment attach=new Attachment(Name='Unit Test Attachment',
		        								body=bodyBlob,
		        								parentId=lst_proj[0].Id, 
		        								ContentType='image/png');  
		        
		        insert attach;

				lst_proj[0].BI_Etapa_del_proyecto__c = 'Kickoff de cierre';
				update lst_proj[0];

				List <BI_Recurso__c> lst_rec = [SELECT Id FROM BI_Recurso__c];
		        system.assertequals(lst_rec.size(), 180);  
		        system.assertequals(config.BI_Nombre_del_documento__c, attach.Description);   


		        BI_Milestone1ProjectMethods.preventDeleteTemplate(lst_proj);
		        BI_Milestone1ProjectMethods.isProfileSysAdmin(UserInfo.getProfileId());
		    }    
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}