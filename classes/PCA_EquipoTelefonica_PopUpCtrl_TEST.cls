@isTest
private class PCA_EquipoTelefonica_PopUpCtrl_TEST {

    static testMethod void getloadInfo() {
    	
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1); 
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;                        
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_EquipoTelefonica_PopUp');
            Test.setCurrentPage(pageRef);
            Hierarchy__c hierar1 = new Hierarchy__c(ShowPersonalData__c=false,User__c=user1.Id,Contact__c=con[0].Id);
            insert hierar1;
            
            system.runAs(user1){
            	ApexPages.currentPage().getParameters().put('id',  user1.Id);
            	ApexPages.currentPage().getParameters().put('Type',  'SMS');
            	ApexPages.currentPage().getParameters().put('back',  'true');
            	ApexPages.currentPage().getParameters().put('clase',  'Equipo');
                BI_TestUtils.throw_exception = false;
            	PCA_EquipoTelefonica_PopUpCtrl constructor = new PCA_EquipoTelefonica_PopUpCtrl();
            	constructor.checkPermissions();
            	constructor.loadInfo(); 
            	constructor.send();                        
                system.assertEquals(user1.Id, ApexPages.currentPage().getParameters().get('id'));
            	
            }
            system.runAs(user1){
            	ApexPages.currentPage().getParameters().put('id',  user1.Id);
            	ApexPages.currentPage().getParameters().put('Type',  'EMAIL');
            	ApexPages.currentPage().getParameters().put('back',  'false');
            	ApexPages.currentPage().getParameters().put('clase',  'escalado');
                BI_TestUtils.throw_exception = false;
            	PCA_EquipoTelefonica_PopUpCtrl constructor = new PCA_EquipoTelefonica_PopUpCtrl();
            	constructor.checkPermissions();
            	constructor.loadInfo(); 
            	constructor.send();
            	
            }
            system.runAs(user1){
            	ApexPages.currentPage().getParameters().put('id',  user1.Id);
            	ApexPages.currentPage().getParameters().put('Type',  'EMAIL');
            	ApexPages.currentPage().getParameters().put('back',  'false');
            	ApexPages.currentPage().getParameters().put('clase',  'escalado');
            	PCA_EquipoTelefonica_PopUpCtrl constructor = new PCA_EquipoTelefonica_PopUpCtrl();
                BI_TestUtils.throw_exception = true;
            	constructor.checkPermissions();
            	constructor.loadInfo(); 
            	constructor.send();
            	
            }
            system.runAs(user1){
            	ApexPages.currentPage().getParameters().put('id',  user1.Id);
            	ApexPages.currentPage().getParameters().put('Type',  'EMAIL');
            	ApexPages.currentPage().getParameters().put('back',  'false');
            	ApexPages.currentPage().getParameters().put('clase',  'escalado');
            	PCA_EquipoTelefonica_PopUpCtrl constructor = new PCA_EquipoTelefonica_PopUpCtrl();
            	constructor.checkPermissions();
                BI_TestUtils.throw_exception = true;
            	constructor.loadInfo(); 
            	constructor.send();
            }
            system.runAs(user1){
            	ApexPages.currentPage().getParameters().put('id',  user1.Id);
            	ApexPages.currentPage().getParameters().put('Type',  'EMAIL');
            	ApexPages.currentPage().getParameters().put('back',  'false');
            	ApexPages.currentPage().getParameters().put('clase',  'escalado');
            	PCA_EquipoTelefonica_PopUpCtrl constructor = new PCA_EquipoTelefonica_PopUpCtrl();
            	constructor.checkPermissions();
            	constructor.loadInfo(); 
                BI_TestUtils.throw_exception = true;
            	constructor.send();
            }
        }
    	
    }
    
    static testMethod void getloadInfo2() {
    	   	User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
       /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;                        
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_EquipoTelefonica_PopUp');
            Test.setCurrentPage(pageRef);
            Hierarchy__c hierar1 = new Hierarchy__c(ShowPersonalData__c=false,User__c=user1.Id,Contact__c=con[0].Id);
            insert hierar1;
            
            system.runAs(user1){
		    	ApexPages.currentPage().getParameters().put('id',  usr.Id);
		    	ApexPages.currentPage().getParameters().put('Type',  'EMAIL');
		    	ApexPages.currentPage().getParameters().put('back',  'false');
		    	ApexPages.currentPage().getParameters().put('clase',  'Equipo');
		        BI_TestUtils.throw_exception = false;            	
		    	PCA_EquipoTelefonica_PopUpCtrl constructor = new PCA_EquipoTelefonica_PopUpCtrl();
		    	constructor.checkPermissions();
		    	constructor.loadInfo(); 
		    	constructor.send();
        }
    }
}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}