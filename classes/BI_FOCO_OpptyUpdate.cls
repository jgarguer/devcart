public without sharing class BI_FOCO_OpptyUpdate {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Bejoy Babu
    Company:       Salesforce.com
    Description:   Takes care of the update of FOCO table based on Opportunities. Handles 2 scenarios
    					1. When the FOCO table is mass-loaded: Brings in all the ooportunities that match the criteria and creates FOCO records
    					2. When Opportunities are inserted/updated/deleted: inserts/updates/deletes the FOCO table
    
    History:
    
    <Date>            <Author>              <Description>
    09/26/2014        Bejoy Babu           Initial version
    10/20/2014		  Bejoy Babu 		   Added Custom Labels for Static values
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    private static final String DEFAULT_CURRENCY;

    static{
    	CurrencyType[] ctl = [SELECT IsoCode FROM CurrencyType WHERE IsCorporate = true LIMIT 1];
    	if(ctl.size()>0)
    	DEFAULT_CURRENCY = String.valueOf(ctl[0].IsoCode);
    } 


    public BI_FOCO_OpptyUpdate(){

    }



    public void updateFOCOs(List<Opportunity> oppList){
		/*-----------------------------------------------------------------------------------
		|	updateFOCOs																		|
		|	------------																	|
		|																					|
		|	Called when Opportunities get updated and therefore is affects the FOCO records.|													
		-------------------------------------------------------------------------------------*/
		

		//materialize a list of FOCO objects for the opportunities in Question
		List<BI_Registro_Datos_FOCO__c> insFocoList = BI_FOCO_OpptyUpdate.generateFOCOsForOppties(oppList);
		//delete the existing focos for the opportunities
		String[] idsToDel = new String[oppList.size()];
		Integer i = 0;
		for(Opportunity o: oppList){
			idsToDel[i++] = String.valueOf(o.Id);
		}

		deleteFOCOs(idsToDel);
		
		Integer failed=0;
		String errMsg='';

		Database.SaveResult[] srs;
		//now, insert the new focos generated
		if(insFocoList!=null && insFocoList.size() >0){
			try{
				srs = Database.insert(insFocoList, false);
				//get errors for insert operation
				for(Database.SaveResult sr : srs){
					if(!sr.isSuccess()){
						failed++;
						for(Database.Error err : sr.getErrors()) {
							errMsg = errMsg + err.getStatusCode() + ': ' + err.getMessage() + '\n';
						}
					}
				}
			}
			catch(DmlException dmle){
				throw new BI_FOCO_Exception('Error while upserting FOCO records', dmle);

			}
			catch(Exception e){
				throw new BI_FOCO_Exception('Error while upserting FOCO records', e);
			}


		}
		if(failed > 0){
			throw new BI_FOCO_Exception('FOCO Update failed for '+failed +' records. Details below - \n '+errMsg);
		}


	}

	public void deleteFOCOs(String[] idsToDel){
		/*-----------------------------------------------------------------------------------
		|	deleteFOCOs																		|
		|	------------																	|
		|																					|
		|	Called when Opportunities get deleted and therefore the corresponding  FOCO 	|
		|	records need to go.																|											
		-------------------------------------------------------------------------------------*/


		if(idsToDel != null && idsToDel.size()>0){

			//get a list of FOCO records for the opportunities in Question
			List<BI_Registro_Datos_FOCO__c> fList = [SELECT Id from BI_Registro_Datos_FOCO__c WHERE BI_Oportunidad__c IN: idsToDel];
			system.debug('###mkh: fList: ' + fList);

			if(fList != null && fList.size() >0 ){

				Integer failed=0;
				String errMsg='';


				//now, delete everything
				Database.DeleteResult[] drs;

				try{
					system.debug('###mkh: delete FOCO');

					drs = Database.delete(fList, false);

					for(Database.DeleteResult dr : drs){
						if(!dr.isSuccess()){
							failed++;
							for(Database.Error err : dr.getErrors()) {
								errMsg = errMsg + err.getStatusCode() + ': ' + err.getMessage() + '\n';
							}
						}
					}
				}
				catch(DmlException dmle){
					throw new BI_FOCO_Exception('Error while deleting FOCO records', dmle);

				}
				catch(Exception e){
					throw new BI_FOCO_Exception('Error while deleting FOCO records', e);
				}	 
				if(failed > 0){
					throw new BI_FOCO_Exception('Delete operation failed for '+failed +' records. Details below - \n '+errMsg);
				}
			}
		}
	}


	public static List<BI_Registro_Datos_FOCO__c> generateFOCOsForOppties (List<Opportunity> oList){
		/*-----------------------------------------------------------------------------------
		|	generateFOCOsForOppties															|
		|	------------																	|
		|																					|
		|	Method that generates FOCO records based on the opportunities passed to it 		|
		-------------------------------------------------------------------------------------*/

		System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Start');

		
		List<BI_Registro_Datos_FOCO__c> fList = new List<BI_Registro_Datos_FOCO__c>();

		
		
		//Max and min date of Oppty to be considered would the first and last day of the 
		//first and last period in the FOCO table respectively
		//Get all the start and enddates into a Map with country name as the Key: to avoic quering the foco table 
		//for each Opp record
		Map<String, Date> dateRangeMap = new Map<String, Date>();
		
		
		try{



			map<string, BI_FOCO_Dates_by_Country__c> countryMap = BI_FOCO_Dates_by_Country__c.getAll();

			for (string country : countryMap.keySet()) {
				Date xd = countryMap.get(country).Max_Date__c;
				if(xd != null){
					//Get last day of the last period
					dateRangeMap.put(country, xd.addMonths(1).toStartOfMonth().addDays(-1));
				}
			}




		} catch(QueryException qe) {
			throw new BI_FOCO_Exception('Error while calculating Date range from BI_Registro_Datos_FOCO__c', qe);
		}





		//[TODO]Get rec Type Ids
		Id REC_TYPE_ID_RCC, REC_TYPE_ID_RCI, REC_TYPE_ID_RCR;

		RecordType[] recTypeIds;

		try{
			recTypeIds = [SELECT Id, Name FROM RecordType WHERE Name IN('Renegociación ciclo completo', 'Renegociación ciclo licitaciones', 'Renegociación ciclo rápido')];

		}
		catch(QueryException qe){
			throw new BI_FOCO_Exception('Error while getting Opportunity Record Types. Check if the following record types exist: | Renegociación ciclo completo | Renegociación ciclo licitaciones | Renegociación ciclo rápido | ', qe);
		}


		for(RecordType recTypeId: recTypeIds){
			if('Renegociación ciclo completo'.equals(recTypeId.Name))
			REC_TYPE_ID_RCC = recTypeId.Id;
			if('Renegociación ciclo licitaciones'.equals(recTypeId.Name))
			REC_TYPE_ID_RCI = recTypeId.Id;
			if('Renegociación ciclo rápido'.equals(recTypeId.Name))
			REC_TYPE_ID_RCR = recTypeId.Id;
		}

		if(REC_TYPE_ID_RCC == null || REC_TYPE_ID_RCI == null || REC_TYPE_ID_RCR == null){
			throw new BI_FOCO_Exception('Error while getting Opportunity Record Types. Check if the following record types exist: | Renegociación ciclo completo | Renegociación ciclo licitaciones | Renegociación ciclo rápido | ');
		}

		//get all the Ramas in a Map : #bulkify
		List<BI_Rama__c> allRamaList = [SELECT Id, BI_Nombre__c from BI_Rama__c];

		if(allRamaList == null || allRamaList.size() ==0 ){
			throw new BI_FOCO_Exception('No Ramas available in BI_Rama__c for processing');
		}

		Map<String, Id> allRamaMap = new Map<String, Id>();
		for(BI_Rama__c r: allRamaList){
			allRamaMap.put(r.BI_Nombre__c, r.Id);
		}


		//get all the periodo records in a Map : #bulkify
		List<BI_Periodo__c> allPeriodoList = [SELECT Id, BI_Fecha_Inicio_Periodo__c from BI_Periodo__c];
		if(allPeriodoList == null || allPeriodoList.size() ==0 ){
			throw new BI_FOCO_Exception('No Periodos available in BI_Periodo__c for processing');
		}
		Map<Date, Id> allPeriodoMap = new Map<Date, Id>();
		for(BI_Periodo__c r: allPeriodoList){
			allPeriodoMap.put(r.BI_Fecha_Inicio_Periodo__c, r.Id);
		}

		//Get the conversion rates
		Map<String, Decimal> currencyMap = new Map<String, Decimal>();
		//START CRM 10/05/2017 Solve queries With No Where Or Limit Clause
		for(CurrencyType ct: BI_CurrencyHelper.LST_CURRENCYTYPE){
			currencyMap.put(ct.IsoCode , ct.ConversionRate);
		}
		
		/*******OLD
		CurrencyType[] ctl = [ SELECT ConversionRate, isocode FROM CurrencyType ];
		for(CurrencyType ct: ctl){
			currencyMap.put(ct.IsoCode , ct.ConversionRate);
		}
		*/
		//END CRM 10/05/2017 Solve queries With No Where Or Limit Clause 

		//AggregateResult[] ramaAgg = [SELECT NE__OrderId__r.NE__OptyId__c oppID, COUNT(Id) countID FROM NE__OrderItem__c WHERE NE__OrderId__r.NE__OptyId__c in :oList and (NE__OrderId__r.NE__OrderStatus__c='Active' OR NE__OrderId__r.NE__OrderStatus__c = 'Active - Rápido')GROUP BY NE__ProdId__r.Rama_Local__c, NE__OrderId__r.NE__OptyId__c];
		AggregateResult[] ramaAgg = [
										SELECT NE__OrderId__r.NE__OptyId__c oppID, COUNT_DISTINCT(NE__ProdId__r.Rama_Local__c) countID
										FROM NE__OrderItem__c
										WHERE NE__OrderId__r.NE__OptyId__c in :oList
										AND (NE__OrderId__r.NE__OrderStatus__c = 'Active' OR NE__OrderId__r.NE__OrderStatus__c = 'Active - Rápido')
										GROUP BY NE__OrderId__r.NE__OptyId__c
									];


		map<id, AggregateResult> ramaCountMap = new map<id, AggregateResult>();

		for (AggregateResult ar : ramaAgg) {
			if (ar.get('oppID') != null) {
				ramaCountMap.put((id)ar.get('oppID'), ar);
			}
		}


		NE__OrderItem__c[] productList = [SELECT NE__ProdId__r.Rama_Local__c,
											  NE__Qty__c, NE__RecurringChargeFrequency__c,
											  currencyisocode, 
											  NE__OneTimeFeeOv__c,
											  NE__RecurringChargeOv__c,
											  NE__OrderId__r.NE__OptyId__c
									   FROM NE__OrderItem__c
									   WHERE NE__OrderId__r.NE__OptyId__c in :oList AND (NE__OrderId__r.NE__OrderStatus__c='Active' or NE__OrderId__r.NE__OrderStatus__c='Active - Rápido')];


		Map<id, List<NE__OrderItem__c>> oppOrderItemMap = new Map<id, List<NE__OrderItem__c>>();

		for (NE__OrderItem__c oi : productList) {
			if (oppOrderItemMap.get(oi.NE__OrderId__r.NE__OptyId__c) == null) {
				oppOrderItemMap.put(oi.NE__OrderId__r.NE__OptyId__c, new List<NE__OrderItem__c>());
			}

			oppOrderItemMap.get(oi.NE__OrderId__r.NE__OptyId__c).add(oi);
		}


		for (Opportunity o: oList){
			
			try{

				//Get the products in the opportunity 
				//AggregateResult[] ramaArr = [SELECT COUNT(Id) FROM NE__OrderItem__c WHERE NE__OrderId__r.NE__OptyId__c = :o.Id and (NE__OrderId__r.NE__OrderStatus__c='Active' OR NE__OrderId__r.NE__OrderStatus__c = 'Active - Rápido')GROUP BY NE__ProdId__r.Rama_Local__c];
				Integer ramaCount;

				if (o.id != null && ramaCountMap.get(o.id) != null) {
					ramaCount = (Integer)ramaCountMap.get(o.id).get('countID');
				}

				//if (ramaArr.size()<=0){
				if (ramaCount == null || ramaCount <=0){	
					throw new BI_FOCO_Exception('No products in the current Opportunity to add Foco. Opportunity: '+o);
				}
				

				//NE__OrderItem__c[] products = [SELECT NE__ProdId__r.Rama_Local__c, NE__Qty__c, NE__RecurringChargeFrequency__c , currencyisocode, NE__OneTimeFeeOv__c  ,NE__RecurringChargeOv__c FROM NE__OrderItem__c WHERE NE__OrderId__r.NE__OptyId__c =:o.Id  and (NE__OrderId__r.NE__OrderStatus__c='Active' or NE__OrderId__r.NE__OrderStatus__c='Active - Rápido')];	
				NE__OrderItem__c[] products = oppOrderItemMap.get(o.id);
				
				//get the difference of previous and current amount, to be used for renegocacion record Type
				//Total difference for the Oppty divided by the number of Ramas
				Decimal diffAmtPerRama;
				if(o.RecordTypeId == REC_TYPE_ID_RCC || o.RecordTypeId == REC_TYPE_ID_RCI || o.RecordTypeId == REC_TYPE_ID_RCR){

					if (o.BI_Recurrente_bruto_mensual__c ==null || o.BI_Recurrente_bruto_mensual_anterior__c == null){

						throw new BI_FOCO_Exception('Current and previous values absent for Renegocación');
					} 
					else {
						//diffAmtPerRama = (o.BI_Recurrente_bruto_mensual__c - o.BI_Recurrente_bruto_mensual_anterior__c)/ramaArr.size();
						diffAmtPerRama = (o.BI_Recurrente_bruto_mensual__c - o.BI_Recurrente_bruto_mensual_anterior__c)/ramaCount;
						System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): o.BI_Recurrente_bruto_mensual__c ' + o.BI_Recurrente_bruto_mensual__c + ' BI_Recurrente_bruto_mensual_anterior__c ' + o.BI_Recurrente_bruto_mensual_anterior__c + ' ramaCount ' + ramaCount + ' diffAmtPerRama ' + diffAmtPerRama);
					}
				}

				//find the period to be considered for recurring record type
				Date startDt = o.CloseDate.toStartOfMonth();
				Date endDt = dateRangeMap.get(o.Account.BI_Country__c);
				if(endDt == null){
					throw new BI_FOCO_Exception('Unable to find Max Date period for Country:'+ o.Account.BI_Country__c+' from FOCO table');
				}

				// If the start date in the opportunity is greater than the opp close date
				if(o.BI_Comienzo_estimado_de_facturacion__c != null && o.BI_Comienzo_estimado_de_facturacion__c.toStartOfMonth() > startDt){
					startDt = o.BI_Comienzo_estimado_de_facturacion__c.toStartOfMonth();
				}

				//if the end date of the opportunity falls before the FOCO range end date
				Integer duration = 0;
				if(o.BI_Duracion_del_contrato_Meses__c != null)
				duration = (Integer)o.BI_Duracion_del_contrato_Meses__c;

				if(startDt.addMonths(duration-1).toStartOfMonth() < endDt){
					endDt = startDt.addMonths(duration-1).toStartOfMonth();
				}

				//total months in the recurring opportunity
				Integer monthsbtw = startDt.monthsBetween(endDt) +1 ;
				System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Recurring - Start Period:'+startDt+' | end Period:'+endDt+' | Months in between:'+monthsbtw);

				Map<String, BI_Registro_Datos_FOCO__c> oneTimeFocoMap = new Map<String, BI_Registro_Datos_FOCO__c>();
				Map<String, Map<Date, BI_Registro_Datos_FOCO__c>> recurrFocoMap = new Map<String, Map<Date, BI_Registro_Datos_FOCO__c>>();

				for(NE__OrderItem__c item: products){

					//if one time charges > 0, create a FOCO record for the Rama
					if(item.NE__OneTimeFeeOv__c>0.0){
						BI_Registro_Datos_FOCO__c foco = oneTimeFocoMap.get(item.NE__ProdId__r.Rama_Local__c);
						Decimal amt;
						if(currencyMap.get(item.CurrencyIsoCode)!=null && currencyMap.get(item.CurrencyIsoCode) >0.0)
						amt = item.NE__OneTimeFeeOv__c*item.NE__Qty__c/currencyMap.get(item.CurrencyIsoCode);
						if(foco == null){
							//create a new foco record
							foco =  new BI_Registro_Datos_FOCO__c(
								BI_Country__c = o.BI_Country__c,
								BI_Oportunidad__c = o.Id, 
								BI_Cliente__c = o.AccountId, 
								BI_Monto__c = amt, 
								CurrencyIsoCode = DEFAULT_CURRENCY,
								BI_Periodo__c = allPeriodoMap.get(startDt), 
								BI_Rama__c = allRamaMap.get(item.NE__ProdId__r.Rama_Local__c), 
								BI_Tipo__c ='Oportunidades (No Recurrente)');
							oneTimeFocoMap.put(item.NE__ProdId__r.Rama_Local__c, foco);
							System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Creating \'No Recurrente\' FOCO object: '+foco);
						}
						else{
							//Add to the amount
							foco.BI_Monto__c +=  amt;
							System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Adding amount: '+amt+' to \'No Recurrente\' FOCO object: '+foco);
						}
					} 

					//if recurring charges >0, need to create records for each month in the duration
					if(item.NE__RecurringChargeOv__c > 0.0) { 
						
						Decimal amt;
						Decimal divisor = 1.0;
						

						//if renegocación RecordType
						if(o.RecordTypeId == REC_TYPE_ID_RCC || o.RecordTypeId == REC_TYPE_ID_RCI || o.RecordTypeId == REC_TYPE_ID_RCR){
							if(currencyMap.get(o.CurrencyIsoCode) != null && currencyMap.get(o.CurrencyIsoCode) > 0.0)
							divisor = currencyMap.get(o.CurrencyIsoCode);
							amt = diffAmtPerRama / divisor;
						}
						//if non-renegocacion record type
						else {
							if(currencyMap.get(item.CurrencyIsoCode) != null && currencyMap.get(item.CurrencyIsoCode) > 0.0)
							divisor = currencyMap.get(item.CurrencyIsoCode);
							amt = item.NE__RecurringChargeOv__c*item.NE__Qty__c/divisor;
						}

						Map<Date, BI_Registro_Datos_FOCO__c> focos = recurrFocoMap.get(item.NE__ProdId__r.Rama_Local__c);
						if(focos == null) {
							focos = new Map<Date, BI_Registro_Datos_FOCO__c>();
							
							//create a FOCO record for each applicable month
							for(Integer m=0; m<monthsbtw; m+=getFrequency(item.NE__RecurringChargeFrequency__c) ){
								BI_Registro_Datos_FOCO__c foco =  new BI_Registro_Datos_FOCO__c(
									BI_Country__c = o.BI_Country__c,
									BI_Oportunidad__c = o.Id, 
									BI_Cliente__c = o.AccountId, 
									BI_Monto__c = amt, 
									CurrencyIsoCode = DEFAULT_CURRENCY,
									BI_Periodo__c = allPeriodoMap.get(startDt.addMonths(m).toStartOfMonth()), 
									BI_Rama__c = allRamaMap.get(item.NE__ProdId__r.Rama_Local__c), 
									BI_Tipo__c ='Oportunidades (Recurrente)');
								focos.put(startDt.addMonths(m).toStartOfMonth(), foco);
								System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Creating \'Recurrente\' FOCO object: '+foco);

							}
							recurrFocoMap.put( item.NE__ProdId__r.Rama_Local__c,focos);
						}
						else {
							// If non-renegociacion, add to the amount if foco exists (renegociacion only is added once per rama)
							if(o.RecordTypeId != REC_TYPE_ID_RCC && o.RecordTypeId != REC_TYPE_ID_RCI && o.RecordTypeId != REC_TYPE_ID_RCR) {
							//for each applicable month
								for(Integer m=0; m<monthsbtw; m+=getFrequency(item.NE__RecurringChargeFrequency__c) ){
									//check if a foco record exists
									BI_Registro_Datos_FOCO__c foco = focos.get(startDt.addMonths(m).toStartOfMonth());
									//create a foco if the foco doesn't exit
									if(foco == null ){
										foco =  new BI_Registro_Datos_FOCO__c(
											BI_Country__c = o.BI_Country__c,
											BI_Oportunidad__c = o.Id, 
											BI_Cliente__c = o.AccountId, 
											BI_Monto__c = amt, 
											CurrencyIsoCode = DEFAULT_CURRENCY,
											BI_Periodo__c = allPeriodoMap.get(startDt.addMonths(m).toStartOfMonth()), 
											BI_Rama__c = allRamaMap.get(item.NE__ProdId__r.Rama_Local__c), 
											BI_Tipo__c ='Oportunidades (Recurrente)');
										focos.put(startDt.addMonths(m).toStartOfMonth(), foco);
										System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Creating \'Recurrente\' FOCO object: '+foco);
									}

									//add to the amt if the foco exists
									else {
										foco.BI_Monto__c += amt;
										System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): Adding amount: '+amt+' to \'Recurrente\' FOCO object: '+foco);
									}
								}
							}
						}
					}
				}
				fList.addAll(oneTimeFocoMap.values() );

				for(Map<Date, BI_Registro_Datos_FOCO__c> fMap: recurrFocoMap.values()){
					fList.addAll(fMap.values());
				}
			}
			catch(Exception e){
				System.debug('----------------------------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties: Exception while processing Opportunity: '+o+ '\n'+ e+'\n'+e.getStackTraceString());
				continue;
			}
		}

		System.debug('-------------------------------------------BI_FOCO_OpptyUpdate.generateFOCOsForOppties(): End. Created '+fList.size()+' FOCO records');

		return fList;
	}

	private static Integer getFrequency(String f){
		Integer freq = 1;
		//[TODO calculate frequency]
		if('Monthly'.equals(f))
		freq = 1;
		if('Bi-Monthly'.equals(f))
		freq = 2;
		if('Quarterly'.equals(f))
		freq = 3;
		if('Four-Monthly'.equals(f))
		freq = 4;
		if('Semiannual'.equals(f))
		freq = 6;
		if('Annual'.equals(f))
		freq = 12;
		return freq;
	}

	public static Boolean checkOpptyForFOCO(Opportunity o){

		/*-----------------------------------------------------------------------------------
		|	checkOpptyForFOCO																|
		|	----------------																|
		|																					|
		|	Utility Method to check whether Oppty satisfies entry criteria 					|
		-------------------------------------------------------------------------------------*/


		Boolean belongsInFOCO = false;

		//Max and min date of Oppty to be considered would the first and last day of the 
		//first and last period in the FOCO table respectively
		//AggregateResult[] ar = [SELECT max(BI_Fecha_Inicio_Periodo__c) maxDate, min(BI_Fecha_Inicio_Periodo__c) minDate from BI_Registro_Datos_FOCO__c];
		//Date xd = (Date)ar[0].get('maxDate');
		//Date nd = (Date)ar[0].get('minDate');

		map<string, BI_FOCO_Dates_by_Country__c> countryMap = BI_FOCO_Dates_by_Country__c.getAll();

		Date nd;
		Date xd;

		for (string s : countryMap.keySet()) {
			Date minD = countryMap.get(s).Min_Date__c;
			Date maxD = countryMap.get(s).Max_Date__c;

			if (nd == null || nd > minD) {
				nd = minD;
			}

			if (xd == null || xd < maxD) {
				xd = maxD;
			}

		}
		
		//Get first day of the month
		Date startDate = nd;
		
		//Get last day of the month
		Date endDate = xd.addMonths(1).addDays(-1);

		//Get the valid record Type Id
		Id recTypeId = [SELECT Id from RecordType where name = :System.Label.BI_Opp_RecType_Agrupacion][0].Id;
		if((true) ||  //actually, compare dates
			(System.Label.BI_F5DefSolucion.equals(o.Stagename)) ||
			(System.Label.BI_DesarrolloOferta.equals(o.Stagename)) ||
			(System.Label.BI_F3OfertaPresentada.equals(o.Stagename)) ||
			(System.Label.BI_F2Negociacion.equals(o.Stagename))||
			(recTypeId != null && recTypeId == o.RecordTypeId)
			){

			belongsInFOCO = true;
		} 

		return belongsInFOCO;


	}


}