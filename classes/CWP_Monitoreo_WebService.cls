/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Everis
	Company:		Everis
	Description:	Controller class for testing CWP_IncModCreationController controller
	Test Class:		CWP_IncModCreationController
	
	History:
	
	<Date>					<Author>				<Change Description>
	14/03/2017              Everis                  Initial Version
    17/04/2017              Everis                  Named Credentials - CWP 001
	17/04/2017              Everis                  Nodes decomposition refactoring - CWP 002
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class CWP_Monitoreo_WebService {
    public static List<List<String>> getApplications(String user){
        /* Http Request Body Setup */
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:inf="http://infinity.telefonica.com/"><soapenv:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:Username>'+'{!$Credential.UserName}'+'</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+'{!$Credential.Password}'+'</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><inf:getUserInformation><!--Optional:--><userName>'+user+'</userName></inf:getUserInformation></soapenv:Body></soapenv:Envelope>';
        
        /* Process Request */
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse result = new HttpResponse();
        
        req.setEndpoint(/* INI CWP 001 */ 'callout:CWP_PortalWS' /* FIN CWP 001 */);
        req.setMethod('POST');
        
        req.setHeader('Content-Type', 'text/xml; charset=utf-8'); 
        req.setBody(body);
        
        if(Test.isRunningTest()){
            CWP_Monitoreo_WebService_Mock ws=new CWP_Monitoreo_WebService_Mock();
            result = ws.getTicketsMock(user);
        }
        else{
            result= http.send(req);
            System.debug(result.getBody());
        }

        Dom.Document doc = new Dom.Document();        
        doc.load(result.getBody());        
        //Retrieve the root element for this document.        
        Dom.XMLNode Envelope = doc.getRootElement();
        /*22/01/2018 Álvaro López - Cahnged from Envelope.getChildElements()[0] to Envelope.getChildElements()[1]*/        
        Dom.XMLNode bodyEnv= Envelope.getChildElements()[1];		//getResponse 
        Dom.XMLNode child1= bodyEnv.getChildElements()[0];   		//Fault
        
        List<List<String>> appAccess = new List<List<String>>();
        if(child1.getName() == 'Fault'){
            Dom.XMLNode faultCode= child1.getChildElement('faultcode', null);  
            Dom.XMLNode faultString= child1.getChildElement('faultstring', null);
            List<String> auxiliarCode = new List<String>();
            auxiliarCode.add('ko');
            auxiliarCode.add(faultString.getText());
            appAccess.add(auxiliarCode);
        }else{
            
            Dom.XMLNode PortalWSResponse= child1.getChildElements()[0];
            Dom.XMLNode applications= PortalWSResponse.getChildElements()[1];

            List<String> auxiliarCode = new List<String>();
            auxiliarCode.add('ok');
            appAccess.add(auxiliarCode);
            
            /* INI CWP 002 */
            for(Dom.XmlNode node: PortalWSResponse.getChildren()){
                
                Dom.XMLNode name= node.getChildElements()[2];
                Dom.XMLNode url= node.getChildElements()[3];
                List<String> auxiliarApp = new List<String>();
                
                auxiliarApp.add(name.getText());
                auxiliarApp.add(url.getText());
                appAccess.add(auxiliarApp);
            }
            /* FIN CWP 002 */
        }
        return appAccess;
    } 
    
    /*@future(callout=true)
    public static void callWs(String endPoint, String body){  
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8'); 
        req.setBody(body);
        req.setMethod('POST');
        if(Test.isRunningTest()){
            CWP_Monitoreo_WebService_Mock ws=new CWP_Monitoreo_WebService_Mock();
            result=ws.getTicketsMock();
        }
        else{
            system.debug('before send');
            result= http.send(req);
            system.debug('before send 2');
            system.debug('response ' + result.getBody());
        }
    }*/
}