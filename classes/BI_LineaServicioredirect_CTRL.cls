public class BI_LineaServicioredirect_CTRL {
	
    public BI_Linea_de_Servicio__c sol_serv;
    public String url { get; set; }
    
    public BI_LineaServicioredirect_CTRL(ApexPages.StandardController controller){
        sol_serv = (BI_Linea_de_Servicio__c) controller.getRecord();
        
        sol_serv = [select Id, BI_Linea_de_recambio__c, BI_Linea_de_recambio__r.BI_Solicitud__c, BI_Linea_de_venta__c, BI_Linea_de_venta__r.BI_Solicitud__c, BI_Solicitud__c from BI_Linea_de_Servicio__c where Id = :sol_serv.Id limit 1];
        
        if(sol_serv.BI_Solicitud__c != null){
            url = '/apex/BI_LineaServicio?id='+sol_serv.BI_Solicitud__c+'&idServ='+sol_serv.Id;
        }else if(sol_serv.BI_Linea_de_recambio__c != null){
            url = '/apex/BI_LineaServicio?id='+sol_serv.BI_Linea_de_recambio__r.BI_Solicitud__c+'&idServ='+sol_serv.Id;
        }else{
            url = '/apex/BI_LineaServicio?id='+sol_serv.BI_Linea_de_venta__r.BI_Solicitud__c+'&idServ='+sol_serv.Id;
        }
    }
}