public class IntegrationException extends Exception {

	// The IntegrationException that stores the error
	private TGS_Error_Integrations__c errorIntegration;
		
	public IntegrationException(String message, String TGSInterfaceC) {
		this(message);
		errorIntegration = new TGS_Error_Integrations__c();
		this.errorIntegration.TGS_Interface__c = TGSInterfaceC;
	}
	
	public IntegrationException(String message, String TGSInterfaceC, String TGSErrorTypeC) {
        this(message, TGSInterfaceC);
        this.errorIntegration.TGS_ErrorType__c = TGSErrorTypeC;
    }
    
    public IntegrationException(String message, String TGSInterfaceC, String TGSErrorTypeC, String TGSOperationC) {
        this(message, TGSInterfaceC, TGSErrorTypeC);
        this.errorIntegration.TGS_Operation__c = TGSOperationC;
    }
	
	public IntegrationException(String message, String TGSInterfaceC, String TGSOperationC, String TGSRecordIdC, 
								String TGSRequestInfoC, String TGSErrorTypeC) {
		this(message, TGSInterfaceC, TGSErrorTypeC, TGSOperationC);
		this.errorIntegration.TGS_RecordId__c = TGSRecordIdC;
		this.errorIntegration.TGS_RequestInfo__c = TGSRequestInfoC;
	}
	
	/** Function that returns the IntegrationException in where the error will be stored **/
	public TGS_Error_Integrations__c getErrorIntegration() { return this.errorIntegration; }
	
	/** Method that sets the IntegrationException in where the error will be stored **/
	public void setErrorIntegration(TGS_Error_Integrations__c errorIntegration) { this.errorIntegration = errorIntegration; }
	
}