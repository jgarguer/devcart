/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for Lightning Component CWP_ReportSection
    Test Class:    CWP_ReportsLightningController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    21/04/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


public with sharing class CWP_ReportsLightningController{
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos
Company:       Aborda
Description:   Controller of service tracking page

History:

<Date>            <Author>              <Description>
26/06/2014        Micah Burgos       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Set<Id> reportIds = new  Set<Id>();
    public static String tokenApex{get;set;}
    public static BI_ImageHome__c ImgBack {get;set;}
    public static Attachment AttImg {get;set;}
    public static BI_ImageHome__c RelationImage {get;set;}
    public static BI_ImageHome__c RelationImageMovi {get;set;}
    public static BI_ImageHome__c RelationImageTel {get;set;}
    public static List<Reportes_PP__c> reportePP{get;set;}
    //public static List<WrapperReport> reports{get;set;}
    
    
    //public String repId { get ; set ; }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos
Company:       Aborda
Description:   Constructor
History:

<Date>            <Author>              <Description>
26/06/2014        Micah Burgos       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public CWP_ReportsLightningController(){        
        
    }
    

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Aborda
Description:   Return the image and description of the global page

History:

<Date>                    <Author>               <Description>
17/12/2014                Antonio Moruno         Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
    
     @auraEnabled    
    public static String getNumTotalResult(){
        try{
            
            Id ActAccId = BI_AccountHelper.getCurrentAccountId();
            system.debug('LOAD INFO - ActAccId' + ActAccId);
            List<Account> SegAccount = [Select Id, BI_Segment__c, Name, BI_Country__c FROM Account Where Id=:ActAccId];
            system.debug('LOAD INFO - SegAccount' + SegAccount);
            //String stringMapReports;
            List<Report> ReportsList = new List<Report>();
            String stringReportsList;
            Map<Id, Report> mapReports;
            
            
            List<BI_ImageHome__c> RelImg = new List<BI_ImageHome__c>();
            
            if(!SegAccount.IsEmpty()){
                if(SegAccount[0].BI_Segment__c!=null){                
                    
                    RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Service Tracking' AND  RecordType.Name= 'General' AND BI_Segment__c=: SegAccount[0].BI_Segment__c LIMIT 1];
                    system.debug('LOAD INFO - RelImg: ' + RelImg);
                    
                    //===Consigue los reportes referenciados en los Reportes PP y rellena los wrappers con la info conjunta===
                    //reports=new List<WrapperReport>();
                    Set<Id> IdsClientes = new Set<Id>();                
                    
                    List<BI_Cliente_PP__c> ClientPP = [Select Id, Name, BI_Activo__c, BI_Cliente__c, BI_Imagen_inicio_PP__c, BI_Reportes__c, BI_Country_2__c, BI_Segment_2__c FROM BI_Cliente_PP__c where BI_Cliente__c =: ActAccId AND BI_Activo__c = true AND (BI_Country_2__c != null OR BI_Segment_2__c != null)];
                    system.debug('LOAD INFO - ClientPP: ' + ClientPP);
                    
                    for(BI_Cliente_PP__c Client :ClientPP){
                        IdsClientes.add(Client.BI_Reportes__c);
                    }
                    system.debug('LOAD INFO - IdsClientes: ' + IdsClientes);
                    
                    reportePP=[SELECT BI_Activo__c,BI_Country__c,BI_Descripcion__c,BI_Segment__c,BI_URL_del_reporte__c,Name  FROM Reportes_PP__c WHERE (((BI_Country__c =: SegAccount[0].BI_Country__c OR BI_Country__c = null) AND BI_Activo__c = true AND (BI_Segment__c = :SegAccount[0].BI_Segment__c OR BI_Segment__c= null) AND (BI_Country__c != null OR BI_Segment__c != null)) OR Id =:IdsClientes) ORDER BY BI_Posicion__c ASC, Name ASC];
                    system.debug('LOAD INFO - reportePP: ' + reportePP);
                    
                    system.debug('LOAD INFO - PRE reportIds');
                    for(Reportes_PP__c r: reportePP){
                        reportIds.add(r.BI_URL_del_reporte__c);
                        System.debug('++**r.BI_URL_del_reporte__c: ' + r.BI_URL_del_reporte__c);
                    }
                    system.debug('LOAD INFO - reportIds: ' + reportIds);
                    
                    mapReports=new Map<Id, Report>([SELECT Id FROM Report WHERE Id IN:reportIds]);                     
                }
                
            }
            
            String str = null;
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeFieldName('ghi');          
            gen.writeStartObject();
            gen.writeObjectField('reportIds', reportIds);
            gen.writeEndObject();           
            gen.writeNumberField('mapSize',  mapReports.values().size());
            gen.writeEndObject();
            String pretty = gen.getAsString();
            
             System.debug('JSON GENERATOR: ' + pretty);
             
           return pretty; 
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.loadInfo', 'Portal Platino', Exc, 'Class');
            return null;        
        }
    }   
    
     @auraEnabled    
    public static String getReportList(Integer offset, Integer numTotalResult , List<String> reports, String orderBy, Boolean descendente){
        
         System.debug('OFFSET: ' + offset);
         System.debug('NumTotalResult: ' + numTotalResult);
        
         System.debug('Reports: ' + reports);
         List<String> reportString = new List<String>();
         for( String r: reports){
                        reportString.add( '\''+ r + '\'');
         }
         System.debug('ReportIds: ' + reportString);
         
         if(descendente==true){
            orderBy=orderBy + ' DESC';
         }
         
        try{
            List<Report> ReportsList = new List<Report>();
            String stringReportsList; 
            String query='SELECT Id,Name, Description, LastModifiedDate FROM Report WHERE Id IN ' + reportString + ' ORDER BY ' + orderBy + ' LIMIT ' + numTotalResult + ' OFFSET ' + offset;
            System.debug('QUERY : ' + query);
            List<Report> sobjList = Database.query(query);
            Map<Id, Report> mapReports=new Map<Id, Report>(sobjList); 
            
            ReportsList.addAll(mapReports.values());
            System.debug('LOAD INFO - ReportsList: ' + ReportsList);
            stringReportsList  = JSON.serialize(ReportsList);    
            
            system.debug('LOAD INFO - JSON.serialize(stringReportsList): ' + stringReportsList);        
            return stringReportsList;
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.loadInfo', 'Portal Platino', Exc, 'Class');
            return null;        
        }
    }   
}