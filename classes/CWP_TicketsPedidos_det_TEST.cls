/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Julio Alberto Asenjo
Company:       Everis
Description:   Test class for CWP_ServiceTracking_TicketsPedidos_det class caseMethod
Test Class:    

History:

<Date>              <Author>                   <Change Description>
11/05/2017          Julio Asenjo             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CWP_TicketsPedidos_det_TEST {
    @testSetup 
    private static void dataModelSetup() {               
        
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        User usuario;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 'Catalogo padre';
        producto.TGS_CWP_Tier_2__c = 'Catalogo hijo';
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Product', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
        
        //ORDER
        
        //NE__Asset__c testAsset = CWP_TestDataFactory.createAsset();
        //insert testAsset;
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'Asset');        
        //NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        //insert testOrder;
        
        
        
        //private final set<string> REQUESTRT = new set<string>{'Order_Management_Case'};
        //private final set<string> CASERT = new set<string>{'TGS_Billing_Inquiry', 'TGS_Change', 'TGS_Complaint', 'TGS_Incident', 'TGS_Problem', 'TGS_Query'};
        
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;                
        listaDeCasos.add(newCase);
        insert listaDeCasos;        
        
        
        // ORDER   
        NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder1.Case__c = listaDeCasos[1].id;
        insert testOrder1;
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI2;        
        newOI2 = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
        insert newOI2;
        
        NE__OrderItem__c newOI;   
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        newOI.NE__Status__c='Activo';
        List<Id> a= new List<Id>();
        a.add(newOI2.id);
        newOI.NE__Parent_Order_Item__c=newOI2.id;
        insert newOI;
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia1.Name = 'picked';
        oia1.NE__Value__c = 'filter';
        insert oia1;
        NE__Order_Item_Attribute__c oia3 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia3.Name = 'picked2';
        oia3.NE__Value__c = 'filter';
        insert oia3;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia2.Name = 'picked2';
        oia2.NE__Value__c = 'filter';
        insert oia2;      
        
        // PRE QUERY
        // Record Types Map
        /* 06/07/2017 */
        List<RecordType> rTCatItemConf = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'CWP_CatalogItemConfiguration__c'];
        Map<String, Id> rTCatItemConfMap = new Map<String, Id>();
        for(RecordType rt:rTCatItemConf){
            rTCatItemConfMap.put(rt.DeveloperName, rt.Id);
        }
        
        List<CWP_CatalogItemConfiguration__c> listToInsert = new List<CWP_CatalogItemConfiguration__c>();
        CWP_CatalogItemConfiguration__c prequery = new CWP_CatalogItemConfiguration__c();
        prequery.CWP_Query__c = 'NE__Order_Item__r.TGS_billing_start_date__c = NEXT_YEAR';
        prequery.CWP_QueryLabel__c = 'test prequery';
        prequery.CWP_CatalogItem__c = catalogoItem.Id;
        prequery.RecordTypeId = rTCatItemConfMap.get('CWP_CustomQuery');
        listToInsert.add(prequery);
        CWP_CatalogItemConfiguration__c prequery2 = new CWP_CatalogItemConfiguration__c();
        prequery2.CWP_Query__c = 'Id!=null';
        prequery2.CWP_QueryLabel__c = 'prequery test';
        prequery2.CWP_Prequery_Type__c='Attribute_Prequery';
        prequery2.CWP_CatalogItem__c = catalogoItem.Id;
        prequery2.RecordTypeId = rTCatItemConfMap.get('CWP_CustomQuery');
        CWP_CatalogItemConfiguration__c prequery3 = new CWP_CatalogItemConfiguration__c();
        prequery3.CWP_Query__c = 'Id!=null';
        prequery3.CWP_QueryLabel__c = 'prequery test';
        prequery3.CWP_Prequery_Type__c='Order_Item_Prequery';
        prequery3.CWP_CatalogItem__c = catalogoItem.Id;
        prequery3.RecordTypeId = rTCatItemConfMap.get('CWP_CustomQuery');
        listToInsert.add(prequery3);
        
        CWP_CatalogItemConfiguration__c fieldsToQuery = new CWP_CatalogItemConfiguration__c();
        fieldsToQuery.CWP_CatalogItemOrderField__c = catalogoItem.Id;
        fieldsToQuery.CWP_FieldAPIName__c = 'TGS_RFS_date__c; NE__Country__c; NE__City__c; NE__Status__c';
        fieldsToQuery.RecordTypeId = rTCatItemConfMap.get('CWP_OrderField');
        listToInsert.add(fieldsToQuery);
        
        
        insert listToInsert;
        system.debug('prequerys insertadas: ' + listToInsert);
        
        
        TGS_GME_GM_Product_Categorization__c aTGS_GME= new  TGS_GME_GM_Product_Categorization__c();
        aTGS_GME.name='001';  
        aTGS_GME.TGS_Service__c='Producto Comercial';
        
        insert aTGS_GME;
        
    }
    @isTest
    private static void Test1() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest(); 
            
            Case c= [SELECT id,CaseNumber,TGS_Ticket_Site__c FROM Case Limit 1];
            CWP_ServiceTracking_TicketsPedidos_det.getOrderItemFromCaseId(c.CaseNumber);
            NE__OrderItem__c oi=[SELECT id, Name FROM NE__OrderItem__c WHERE NE__Parent_Order_Item__c !=null Limit 1];
            CWP_ServiceTracking_TicketsPedidos_det.getOrderItem(String.valueOf(oi.Name));
            CWP_ServiceTracking_TicketsPedidos_det.getAccountHierarchy(String.valueOf(oi.Name));
            CWP_ServiceTracking_TicketsPedidos_det.getDetailsAttributes(String.valueOf(oi.Name));
            CWP_ServiceTracking_TicketsPedidos_det.getDetailsAttributes2(String.valueOf(oi.Name));    
            CWP_ServiceTracking_TicketsPedidos_det.hasSecondLevel(String.valueOf(oi.Name)); 
            CWP_ServiceTracking_TicketsPedidos_det.getProductsNames(String.valueOf(oi.Name)); 
            CWP_ServiceTracking_TicketsPedidos_det.getDetailsAttributes2Detailed(String.valueOf(oi.Name)); 
            Test.stopTest();
        }       
        
    } //
}