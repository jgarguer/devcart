/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for schedule class BI_COL_MasterScheduler_sch
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-27      Raul Mora (RM)				    Create Class      
*************************************************************************************/
@isTest
private class BI_COL_MasterScheduler_sch_tst 
{
	
    static testMethod void myUnitTest()
    {
        Test.startTest();
			SchedulableContext sc;
			BI_COL_MasterScheduler_sch class_sch = new BI_COL_MasterScheduler_sch();			
			BI_COL_MasterScheduler_sch class_sch2 = new BI_COL_MasterScheduler_sch( 1 );
			
			class_sch.execute(sc);
			
			class_sch.EliminarSch();
			
			String CRON_EXP = '0 0 0 15 3 ? 2022';

			String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP,new BI_COL_MasterScheduler_sch());
			String jobId1 = System.schedule('ScheduleApexClassTest1',CRON_EXP,new BI_COL_MasterScheduler_sch());
			
		Test.stopTest();
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, state FROM CronTrigger WHERE id = :jobId];
		System.debug('\nCron===>'+ct);
		System.abortJob(jobId);
		class_sch.EliminarSch();
    }
}