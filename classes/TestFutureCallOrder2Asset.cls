@isTest(SeeAllData=true)
private class TestFutureCallOrder2Asset {
    
    static testMethod void FutureCallOrder2Asset_testNew()
    {        
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        
        System.runAs(u) 
        {            
        
        NE__Catalog_Header__c catHeader = new NE__Catalog_Header__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Enable_Global_Cart__c=false, NE__Name__c='CatalogTest', NE__Start_Date__c=system.today()-1);
        insert catHeader;
        
        NE__Catalog__c catalog = new NE__Catalog__c(CurrencyIsoCode='ARS', Name='TGSOL Catalog', NE__Active__c=true, NE__Catalog_Header__c = catHeader.Id, NE__Enable_for_oppty__c=false,  NE__Version__c=1.0, NE__Visible_web__c=false);
        insert catalog;
        
        NE__Catalog_Category__c catCategory = new NE__Catalog_Category__c(Name='TGSOL Catalog', CurrencyIsoCode='ARS', NE__CatalogId__c=catalog.Id);
        insert catCategory;
        
        NE__Commercial_Model__c commModel = new NE__Commercial_Model__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Catalog_Header__c=catHeader.Id, NE__Offline__c=false, NE__ProgramName__c='CatalogTest', NE__Status__c='Pending');
        insert commModel;
            
        NE__Product__c prod = new NE__Product__c(Name='WIFI', CurrencyIsoCode='ARS');
        insert prod;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__ProductId__c=prod.Id, NE__Catalog_Id__c=catalog.Id, NE__Catalog_Category_Name__c=catCategory.Id, NE__Start_Date__c=system.today()-1, NE__BaseRecurringCharge__c=10.00, NE__Base_OneTime_Fee__c=100.00, NE__Min_Qty__c=0, NE__Max_Qty__c=99);
        insert catItem;
        
        RecordType RecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding' LIMIT 1];            
        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
        insert acc;
        
        Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
        insert CaseContact;
        
        
            NE__Asset__c asset = new NE__Asset__c(NE__AccountId__c=acc.Id, NE__BillAccId__c=acc.Id, NE__ServAccId__c=acc.Id);
            insert asset;
            asset = [SELECT Id, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c FROM NE__Asset__c WHERE Id=:asset.Id];
            
            NE__Order__c ord = new NE__Order__c(NE__Asset__c=asset.Id, NE__AccountId__c=acc.Id, NE__Configuration_Type__c='New', NE__Type__c='InOrder');
            insert ord;
            ord = [SELECT Id, NE__AccountId__c, NE__Configuration_Type__c, NE__Type__c FROM NE__Order__c WHERE Id=:ord.Id];
            
            NE__OrderItem__c ordIt = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__Qty__c=1);
            insert ordIt;
            
            ApexPages.StandardController contr = new ApexPages.StandardController(ord);
            NENewOrderExtension newordpage = new NENewOrderExtension(contr);
            newordpage.next();
                    
            Case newCase = [SELECT Id, Asset__c, Order__c, Status, Subject, Type, TGS_Service__c, ContactId, AccountId 
                            FROM Case 
                            WHERE Order__c =: ord.Id LIMIT 1];
            Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());
            Test.startTest();
            
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
            NETriggerHelper.setTriggerFiredTest('Order2AssetAfterUpdate', false);
            
            newCase.ContactId = CaseContact.Id;
            newCase.AccountId = CaseContact.AccountId;
            newCase.Status = 'Resolved';
            update newCase;
             
            //Test.startTest();
            //NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
            //NETriggerHelper.setTriggerFiredTest('Order2AssetAfterUpdate', false);
            
            //newCase.Asset__c = ord.Id;
            //newCase.Status = 'Closed';
            //update newCase;
            Test.stopTest();
            
        }
    }
    
    
    static testMethod void FutureCallOrder2Asset_testChange(){        
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        
        System.runAs(u) {
                
        NE__Catalog_Header__c catHeader = new NE__Catalog_Header__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Enable_Global_Cart__c=false, NE__Name__c='CatalogTest', NE__Start_Date__c=system.today()-1);
        insert catHeader;
        
        NE__Catalog__c catalog = new NE__Catalog__c(CurrencyIsoCode='ARS', Name='TGSOL Catalog', NE__Active__c=true, NE__Catalog_Header__c = catHeader.Id, NE__Enable_for_oppty__c=false,  NE__Version__c=1.0, NE__Visible_web__c=false);
        insert catalog;
        
        NE__Catalog_Category__c catCategory = new NE__Catalog_Category__c(Name='TGSOL Catalog', CurrencyIsoCode='ARS', NE__CatalogId__c=catalog.Id);
        insert catCategory;
        
        NE__Commercial_Model__c commModel = new NE__Commercial_Model__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Catalog_Header__c=catHeader.Id, NE__Offline__c=false, NE__ProgramName__c='CatalogTest', NE__Status__c='Pending');
        insert commModel;
            
        NE__Product__c prod = new NE__Product__c(Name='WIFI', CurrencyIsoCode='ARS');
        insert prod;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__ProductId__c=prod.Id, NE__Catalog_Id__c=catalog.Id, NE__Catalog_Category_Name__c=catCategory.Id, NE__Start_Date__c=system.today()-1, NE__BaseRecurringCharge__c=10.00, NE__Base_OneTime_Fee__c=100.00, NE__Min_Qty__c=0, NE__Max_Qty__c=99);
        insert catItem;
        
        RecordType RecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding' LIMIT 1];            
        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
        insert acc;
        
         
        Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
        insert CaseContact;
            NE__Order__c ord = new NE__Order__c(NE__AccountId__c=acc.Id, NE__Configuration_Type__c='Change', NE__Type__c='ChangeOrder');
            insert ord;
            ord = [SELECT Id, NE__AccountId__c, NE__Configuration_Type__c, NE__Type__c FROM NE__Order__c WHERE Id=:ord.Id];
            
            NE__OrderItem__c ordIt = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__Qty__c=1);
            insert ordIt;
            
            NE__Asset__c asset = new NE__Asset__c(NE__AccountId__c=acc.Id, NE__BillAccId__c=acc.Id, NE__ServAccId__c=acc.Id);
            insert asset;
            asset = [SELECT Id, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c FROM NE__Asset__c WHERE Id=:asset.Id];
            
            RecordType assetRT = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Order__c' AND Name = 'Asset' LIMIT 1];            
            NE__Order__c ordAsset = new NE__Order__c(RecordTypeId=assetRT.Id, NE__Asset__c=asset.Id, NE__AccountId__c=acc.Id, NE__Configuration_Type__c='Change', NE__Type__c='Asset');
            insert ordAsset;
            
            NE__OrderItem__c ordItAsset = new NE__OrderItem__c(NE__OrderId__c=ordAsset.Id, NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__Qty__c=1);
            insert ordItAsset;
            
            RecordType caseRT = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND Name = 'Order Management Case' LIMIT 1];
            Case changeCase = new Case(Order__c=ord.Id, Asset__c=ordAsset.Id, Status='Assigned', Type='Change', Subject='Order Management Case', TGS_Service__c=ordIt.NE__ProdId__r.Name, Origin='Order', AccountId=acc.Id, RecordTypeId=caseRT.Id, ContactId=CaseContact.id);
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
            insert changeCase;
            
            ord.Case__c = changeCase.Id;
            ordAsset.Case__c = changeCase.Id;
            update ord;
            update ordAsset;
             
            
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', true);
            changeCase.Status = 'Resolved';
            update changeCase;
            
            Test.startTest();            
            
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
            NETriggerHelper.setTriggerFiredTest('Order2AssetAfterUpdate', false);
            
            changeCase.Status = 'Cancelled';
            update changeCase;
            
            Test.stopTest();
        }
    }
}