public class CWP_InventoryController {

    @auraEnabled
    public static list<NE__Asset__c> getAssetList(){
        User currentUser = CWP_LHelper.getCurrentUser(null);
        system.debug('llamada a obtención de assets del usuario	' + currentUser);
        list<NE__Asset__c> retList = new list<NE__Asset__c>();
        retList = [SELECT Name, Description__c, Cantidad__c,  CurrencyIsoCode, Id FROM NE__Asset__c WHERE NE__AccountId__c =: currentUser.AccountId order by CreatedDate DESC];
        system.debug('lista de asset recuperada	'	+ retList);
        return retList;
    }
}