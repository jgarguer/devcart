/*------------------------------------------------------------ 
Author:         Ana Cirac 
Company:        Deloitte
Description:    Class to create/update WorkInfos
History
<Date>          <Author>        <Change Description>
24-Feb-2015     Ana Cirac        Initial Version
18-may-2015     Ana Cirac        Add operations 'UDO_SF_ADD_WORKINFO' and 'UDO_SF_MODIFY_WORKINFO'
------------------------------------------------------------*/
global with sharing class TGS_ModTicketWI {
    
    static final Set<String> ROD_SF_CREATE_WI = new Set<String>{'ROD_SF_ADD_WORKINFO','UDO_SF_ADD_WORKINFO'};
    static final Set<String> ROD_SF_MODIFY_WI = new Set<String>{'ROD_SF_MODIFY_WORKINFO','UDO_SF_MODIFY_WORKINFO'};
    
    /* Global classes*/
    global class Result{ 
    
        webservice string Operation_Status;
        webservice string Error_Msj;
        webservice string ID_WI;
        
    }
        
    /*------------------------------------------------------------ 
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    WS to create/update WorkInfos
    History
    <Date>          <Author>        <Change Description>
    24-Feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    webservice static Result upsertWI(String Ticket_ID, String WI_ID, String Operation, String Contact_Email, String Work_Info_Notes, String Work_Info_View_Access,
                                                String Attachment_Name1, String Attachment_Name2, String Attachment_Name3, Blob Attachment_Body1, Blob Attachment_Body2, Blob Attachment_Body3){
    
    Savepoint savePointCheckout = Database.setSavepoint();  
        Result resul = new Result();
    try{
        if (ROD_SF_CREATE_WI.contains(Operation)){
            
            resul = createWI(Ticket_ID, WI_ID,Contact_Email, Work_Info_Notes, Work_Info_View_Access, Attachment_Name1, Attachment_Name2, Attachment_Name3, Attachment_Body1, Attachment_Body2, Attachment_Body3);
            
        }else if (ROD_SF_MODIFY_WI.contains(Operation)){
            
            resul = updateWI(Ticket_ID, WI_ID,Contact_Email, Work_Info_Notes, Work_Info_View_Access, Attachment_Name1, Attachment_Name2, Attachment_Name3, Attachment_Body1, Attachment_Body2, Attachment_Body3);
            
        }else{
            
            throw new IntegrationException('Invalid Operation');
        }
    }catch(Exception e){
        resul.Operation_Status = 'ERROR'; 
        resul.Error_Msj = e.getMessage();
        system.debug('Exception found: '+e+' At line: '+e.getLineNumber());
        DataBase.rollback(savePointCheckout);
        //add a tgs_error_integrations_c object
        TGS_Error_Integrations__c error= new TGS_Error_Integrations__c();
        error.TGS_Interface__c = 'ROD'; 
        error.TGS_Operation__c = Operation;
        error.TGS_RecordId__c = WI_ID;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        error.TGS_RequestInfo__c = 'Message: '+e.getMessage();
        insert error;
        
    }
    return resul;                                               
    
    }       
    
    /*------------------------------------------------------------ 
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Clas to create WorkInfos
    History
    <Date>          <Author>        <Change Description>
    24-Feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    static Result createWI(String Ticket_ID, String WI_ID, String Contact_Email, String Work_Info_Notes, String Work_Info_View_Access,
                                                String Attachment_Name1, String Attachment_Name2, String Attachment_Name3, Blob Attachment_Body1, Blob Attachment_Body2, Blob Attachment_Body3){
    

    Result resul = new Result();
    resul.operation_status = 'OK';

    TGS_Work_Info__c workInfo = new TGS_Work_Info__c();
    if (String.isNotBlank(Ticket_ID)){
        
        List<Case> cases = [SELECT Id FROM Case WHERE TGS_Ticket_Id__c=:Ticket_ID];
        if ( cases.size()==0){
            
            throw new IntegrationException('Invalid Ticket_ID');
        } 
        workInfo.TGS_Case__c = cases.get(0).Id;
    }else{
        throw new IntegrationException('Ticket_ID is Mandatory');
        
    }
    if (String.isNotBlank(WI_ID)){
        workInfo.TGS_ExternalId__c = WI_ID;
    }else{
        
        throw new IntegrationException('WI_ID is Mandatory');
    }
    
    
    workInfo.TGS_Contact_Email__c = Contact_Email;
    if(Work_Info_Notes.length() > 32768){
        Work_Info_Notes = Work_Info_Notes.abbreviate(32768);
    }
    workInfo.TGS_Description__c = Work_Info_Notes;
    
    if (String.isNotBlank(Work_Info_View_Access)){
         boolean public_view = false;
        if (!Work_Info_View_Access.equals(TGS_ModTicket.PUBLIC_ACCESS) && !Work_Info_View_Access.equals(TGS_ModTicket.INTERNAL_ACCESS)){
            
            throw new IntegrationException('Invalid Work_Info_View_Access parameter');
        }
        if (Work_Info_View_Access.equals(TGS_ModTicket.PUBLIC_ACCESS)){
             public_view = true;
        }
        workInfo.TGS_Public__c = public_view;
    }   
    
    insert workinfo;
    resul.ID_WI = workinfo.Id;
    /* Attachments for WorkInfo */
    List<Attachment> attachs = new List<Attachment>();
    if (Attachment_Body1!=null){
        if (String.isNotBlank(Attachment_Name1)){
            Attachment attachment = new Attachment();
            attachment.ParentId = workinfo.Id;
            attachment.Description = TGS_IntegrationUtils.UNO;
            attachment.Name = Attachment_Name1;
            attachment.Body = Attachment_Body1;
            attachs.add(attachment);
        }else{
            throw new IntegrationException('Attachment_Name1 is Mandatory');
            
        }
        
    }
    if (Attachment_Body2!=null){
        if (String.isNotBlank(Attachment_Name2)){
            Attachment attachment = new Attachment();
            attachment.ParentId = workinfo.Id;
            attachment.Name = Attachment_Name2;
            attachment.Description =  TGS_IntegrationUtils.DOS;
            attachment.Body = Attachment_Body2;
            attachs.add(attachment);
        }else{
            throw new IntegrationException('Attachment_Name2 is Mandatory');
            
        }
        
    }
    if (Attachment_Body3!=null){
        if (String.isNotBlank(Attachment_Name3)){
            Attachment attachment = new Attachment();
            attachment.ParentId = workinfo.Id;
            attachment.Name = Attachment_Name3;
            attachment.Description =  TGS_IntegrationUtils.TRES;
            attachment.Body = Attachment_Body3;
            attachs.add(attachment);
        }else{
            throw new IntegrationException('Attachment_Name3 is Mandatory');
            
        }
        
    }
    if (attachs.size()!=0){
        insert attachs;
    }

    return resul;                                               
    
    }   
    
    /*------------------------------------------------------------ 
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Clas to update WorkInfos
    History
    <Date>          <Author>        <Change Description>
    24-Feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    static Result updateWI(String Ticket_ID, String WI_ID, String Contact_Email, String Work_Info_Notes, String Work_Info_View_Access,
                                                String Attachment_Name1, String Attachment_Name2, String Attachment_Name3, Blob Attachment_Body1, Blob Attachment_Body2, Blob Attachment_Body3){
    

    Result resul = new Result();
    resul.operation_status = 'OK';
    TGS_Work_Info__c workInfo =  new TGS_Work_Info__c();
    if (String.isNotBlank(Ticket_ID)){
         
        List<TGS_Work_Info__c> worksInfos = [SELECT Id, TGS_Contact_Email__c, TGS_Description__c, TGS_Public__c FROM TGS_Work_Info__c WHERE TGS_ExternalId__c =:WI_ID];
        if(worksInfos.size()==0){
            
            throw new IntegrationException('Invalid WI_ID');
        }
        workInfo = worksInfos.get(0);
        System.debug('workInfo: '+workInfo);
    }else{
        throw new IntegrationException('WI_ID is Mandatory');
    }   
    /* Field ContactEmail */
    if (String.isNotBlank(Contact_Email)){
        workInfo.TGS_Contact_Email__c = Contact_Email;
    }
     /* Field Work_Info_Notes */
    if (String.isNotBlank(Work_Info_Notes)){
        if(Work_Info_Notes.length() > 32768){
           Work_Info_Notes = Work_Info_Notes.abbreviate(32768);
        }
        workInfo.TGS_Description__c = Work_Info_Notes;
    }   
    /* Field Work_Info_View_Access */
    if (String.isNotBlank(Work_Info_View_Access)){
         boolean public_view = false;
        if (!Work_Info_View_Access.equals(TGS_ModTicket.PUBLIC_ACCESS) && !Work_Info_View_Access.equals(TGS_ModTicket.INTERNAL_ACCESS)){
            
            throw new IntegrationException('Invalid Work_Info_View_Access parameter');
        }
        if (Work_Info_View_Access.equals(TGS_ModTicket.PUBLIC_ACCESS)){
             public_view = true;
        }
        workInfo.TGS_Public__c = public_view;
    }   
    
    update workinfo;
    /* Attachments to update */
    List<Attachment> attachs = new List<Attachment>();
    /* Attachments to delete */
    List<Attachment> attachsDelete = new List<Attachment>();
    /* Attachments to create */
    List<Attachment> attachsCreate = new List<Attachment>();
    
    List<Attachment> attachments1  = [SELECT Id, Description FROM Attachment WHERE Parent.Id=:workInfo.Id and Description LIKE '1.%'];
    System.debug('1:' +attachments1);
    if (Attachment_Body1!=null){
        if (String.isNotBlank(Attachment_Name1)){         
            if (attachments1.size()==0){
                 Attachment attachment = new attachment();
                 attachment.Name = Attachment_Name1;
                 attachment.Description =  TGS_IntegrationUtils.UNO;
                 attachment.Body = Attachment_Body1;
                 attachment.ParentId = workinfo.Id;
                 attachsCreate.add(attachment);
            }else{
                
                Attachment attachment = attachments1.get(0);
                attachment.Name = Attachment_Name1;
                attachment.Body = Attachment_Body1;
                attachs.add(attachment);
            }
        }else{
            throw new IntegrationException('Attachment_Name1 is Mandatory');
            
        }
        
    }else{
        if (attachments1.size()!=0){
            system.debug('Delete 1');
            attachsDelete.add(attachments1.get(0));
        }
    }
    List<Attachment> attachments2  = [SELECT Id, Description FROM Attachment WHERE Parent.Id=:workInfo.Id and Description LIKE '2.%'];
    System.debug('2:' +attachments2);
    if (Attachment_Body2!=null){
        if (String.isNotBlank(Attachment_Name2)){           
           if (attachments2.size()==0){
                 Attachment attachment = new attachment();
                 attachment.Name = Attachment_Name2;
                 attachment.Description =  TGS_IntegrationUtils.DOS;
                 attachment.Body = Attachment_Body2;
                 attachment.ParentId = workinfo.Id;
                 attachsCreate.add(attachment);
            }else{
                
                Attachment attachment = attachments2.get(0);
                attachment.Name = Attachment_Name2;
                attachment.Body = Attachment_Body2;
                attachs.add(attachment);
                
            }
        }else{
            throw new IntegrationException('Attachment_Name2 is Mandatory');
            
        }
        
    }else{
        if (attachments2.size()!=0){
            system.debug('Delete 2');
            attachsDelete.add(attachments2.get(0));
        }
    }
    List<Attachment> attachments3  = [SELECT Id, Description FROM Attachment WHERE  Parent.Id=:workInfo.Id and Description LIKE '3.%'];
    System.debug('3:' +attachments3);
    if (Attachment_Body3!=null){
        if (String.isNotBlank(Attachment_Name3)){
            if (attachments3.size()==0){
                 Attachment attachment = new attachment();
                 attachment.Name = Attachment_Name3;
                 attachment.Description =  TGS_IntegrationUtils.TRES;
                 attachment.Body = Attachment_Body3;
                 attachment.ParentId = workinfo.Id;
                 attachsCreate.add(attachment);
            }else{
                
                Attachment attachment = attachments3.get(0);
                attachment.Name = Attachment_Name3;
                attachment.Body = Attachment_Body3;
                attachs.add(attachment);
                
            }
           
        }else{
            throw new IntegrationException('Attachment_Name3 is Mandatory');
            
        }
        
    }else{
        if (attachments3.size()!=0){            
            attachsDelete.add(attachments3.get(0));
        }
    }
    System.debug('Crear: '+attachsCreate);
    if (attachsCreate.size()!=0){
        insert attachsCreate;
    }
    System.debug('Actaulizar: '+attachs);
    if (attachs.size()!=0){
        update attachs;
    }
    System.debug('Borar: '+attachsDelete);
    if (attachsDelete.size()!=0){
        delete attachsDelete;
    }
    resul.ID_WI= workinfo.Id;
    return resul;                                               
    
    }   

}