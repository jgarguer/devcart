@isTest
private class BI_OrderRest_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_OrderRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    17/09/2014              Pablo Oliva             Initial Version
    10/11/2015          	Fernando Arteaga        BI_EN - CSB Integration
    02/02/2017              Pedro Párraga           Increase coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_OrderRest.getOrder()
    History:
    
    <Date>              <Author>                <Description>
    17/09/2014          Pablo Oliva             Initial version
    10/10/2017          Gawron, Julián          Adding BI_MigrationHelper.skipAllTriggers();
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getOrderTest() {
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
       	//Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        BI_MigrationHelper.skipAllTriggers();
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');          							   			 
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null);
        
        List <NE__Order__c> lst_orders = BI_DataLoadRest.loadOrders(1, lst_acc[0], lst_opp[0]);
        
        List <NE__OrderItem__c> lst_oi = BI_DataLoadRest.loadOrderItem(lst_acc[0], lst_orders);

        BI_MigrationHelper.cleanSkippedTriggers();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/orders/'+lst_orders[0].BI_Id_legado__c;  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;
        
        //INTERNAL_SERVER_ERROR
        BI_OrderRest.getOrder();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c where BI_Asunto__c = 'BI_OrderRest.getOrder' order by CreatedDate DESC limit 1];

        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST
        BI_TestUtils.throw_exception = false;
        BI_OrderRest.getOrder();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        req.requestURI = '/accountresources/v1/orders/idInvoice';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
        
        BI_OrderRest.getOrder();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/accountresources/v1/orders/'+lst_orders[0].BI_Id_legado__c;
        RestContext.request = req;
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        BI_OrderRest.getOrder();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_OrderRest.updateOrder()
    History:
    
    <Date>              <Author>                <Description>
    10/11/2015          Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateOrderTest() {

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        BI_TestUtils.throw_exception = false;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true,true,false,false);
        
        if (BI_CSB_Informacion_Caso__c.getAll().isEmpty())
        {
	        BI_CSB_Informacion_Caso__c infoCaso = new BI_CSB_Informacion_Caso__c(Name = 'Argentina',
	        																	 BI_CSB_Country__c = 'Argentina',
	        																	 BI_CSB_Department__c = 'Ingeniería',
	        																	 BI_CSB_Solicitation_Subtype__c = 'Avance a Provisión SSII',
	        																	 BI_CSB_Solicitation_Type__c = 'Aceptación oferta técnica');
	        																	 
			insert infoCaso;
        }
        
       	//Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
		List<RecordType> lst_rt = [select Id from RecordType where SobjectType IN ('NE__Order__c') AND DeveloperName IN ('Order', 'Opty') ORDER BY DeveloperName limit 2];
		System.debug('lst_rt:' + lst_rt);
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');          							   			 
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null);

        //List <NE__Order__c> lst_orders = BI_DataLoadRest.loadOrders(1, lst_acc[0], lst_opp[0]);
        
        List <NE__Order__c> lst_orders = new List <NE__Order__c>();
        
        NE__Order__c ne_order = new NE__Order__c(RecordTypeId = lst_rt[0].Id,
        										 BI_Id_legado__c = '1000',
        										 NE__OptyId__c = lst_opp[0].Id,
        										 NE__OrderStatus__c = 'Pending',
        										 NE__AccountId__c = lst_acc[0].Id,
        										 NE__Recurring_Charge_Total__c = 100,
        										 NE__One_Time_Fee_Total__c = 200);
												   
		lst_orders.add(ne_order);
		
		insert lst_orders;

        NE__Order_Header__c header = new NE__Order_Header__c(NE__OrderId__c = lst_orders[0].Id);
        insert header;

        BI_Log__c trans = new BI_Log__c(BI_Asunto__c = Label.BI_CSB_Transaccion,
                                        BI_CSB_Record_Id__c = String.valueOf(lst_orders[0].Id).substring(0, 15),
                                        BI_Descripcion__c = 'Baja:' + lst_acc[0].Name + ' | Id ' + lst_acc[0].Id,
                                        BI_COL_Estado__c = Label.BI_COL_lblPendiente); 
        insert trans;
        
        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
	        
        NE__Product__c prod1 = new NE__Product__c();
        prod1.Name = 'Prod1';
        prod1.NE__Recurring_Cost__c = 100;
        prod1.NE__One_Time_Cost__c = 25;
        
        NE__Product__c prod2 = new NE__Product__c();
        prod2.Name = 'Prod2';
        prod2.NE__Recurring_Cost__c = 100;
        prod2.NE__One_Time_Cost__c = 25;
        
        NE__Product__c prod3 = new NE__Product__c();
        prod3.Name = 'Prod3';
        prod3.NE__Recurring_Cost__c = 100;
        prod3.NE__One_Time_Cost__c = 25;
        
        insert new List<NE__Product__c>{prod1, prod2, prod3};
        
	    NE__Catalog_Item__c catItem1 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod1.Id, BI_CSB_Code__c = 'OFFICE_365');
	    NE__Catalog_Item__c catItem2 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod2.Id, BI_CSB_Code__c = 'OFFICE_365_2');
	    NE__Catalog_Item__c catItem3 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod3.Id);

		insert new List<NE__Catalog_Item__c>{catItem1, catItem2, catItem3};
        
		List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();

		NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = lst_orders[0].Id,
											    	NE__Qty__c = 2,
											    	NE__Asset_Item_Account__c = lst_acc[0].Id,
											    	NE__Account__c = lst_acc[0].Id,
											    	NE__Status__c = 'Active',
											    	NE__ProdId__c = prod1.Id,
											    	NE__CatalogItem__c = catItem1.Id,
											    	NE__OneTimeFeeOv__c = 100,
											    	NE__RecurringChargeOv__c = 50);
											    	
		NE__OrderItem__c oit2 = new NE__OrderItem__c(NE__OrderId__c = lst_orders[0].Id,
											    	NE__Qty__c = 2,
											    	NE__Asset_Item_Account__c = lst_acc[0].Id,
											    	NE__Account__c = lst_acc[0].Id,
											    	NE__Status__c = 'Active',
											    	NE__ProdId__c = prod2.Id,
											    	NE__CatalogItem__c = catItem2.Id,
											    	NE__OneTimeFeeOv__c = 100,
											    	NE__RecurringChargeOv__c = 50);

		NE__OrderItem__c oit3 = new NE__OrderItem__c(NE__OrderId__c = lst_orders[0].Id,
											    	NE__Qty__c = 2,
											    	NE__Asset_Item_Account__c = lst_acc[0].Id,
											    	NE__Account__c = lst_acc[0].Id,
											    	NE__Status__c = 'Active',
											    	NE__ProdId__c = prod3.Id,
											    	NE__CatalogItem__c = catItem3.Id,
											    	NE__OneTimeFeeOv__c = 100,
											    	NE__RecurringChargeOv__c = 50);
											    	
        insert new List<NE__OrderItem__c>{oit, oit2, oit3};

        BI_MigrationHelper.disableBypass(mapa);
        
		lst_orders[0].RecordTypeId = lst_rt[1].Id;
		
		Test.startTest();
		
		update lst_orders[0];
        
		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        //Test.startTest();
        
        req.requestURI = '/accountresources/v1/orders/';  
        req.httpMethod = 'PUT';

		Blob body = Blob.valueof('{"transaction_x": { "transactionId": "1000", "transactionStatus": {"transactionStatus": "Success"}}, "resourceData":{"subscriptionId":"10", "accountId":"' + lst_acc[0].BI_Id_CSB__c + '", "orderId": "001", "subscriptionStatus":"Activated", "channel":{"ChannelType":"CRM"}, "products":[{"id":"1234", "name":"prod1", "description":null, "productStatus":"On", "additionalInfo":null}]}}');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.response = res;
        
        BI_OrderRest.updateOrder();

        system.assertEquals(200,RestContext.response.statuscode);
        
        body = Blob.valueof('{"transaction_x": { "transactionId": "1000", "transactionStatus": {"transactionStatus": "Fail","error": {"exceptionId":"SVC1008", "exceptionText":"Error forzado en prueba"}}}, "resourceData":{"subscriptionId":"10", "accountId":"' + lst_acc[0].BI_Id_CSB__c + '", "orderId": "001", "subscriptionStatus":"Activated", "channel":{"ChannelType":"CRM"}, "products":[{"id":"1234", "name":"prod1", "description":null, "productStatus":"On", "additionalInfo":null}]}}');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.response = res;
        
        BI_OrderRest.updateOrder();

        system.assertEquals(200,RestContext.response.statuscode);        

        body = Blob.valueof('{"transaction_x": { "transactionId": "7777", "transactionStatus": {"transactionStatus": "Fail","error": {"exceptionId":"SVC1008", "exceptionText":"Error forzado en prueba"}}}, "resourceData":{"subscriptionId":"10", "accountId":"' + lst_acc[0].BI_Id_CSB__c + '", "orderId": "001", "subscriptionStatus":"Activated", "channel":{"ChannelType":"CRM"}, "products":[{"id":"1234", "name":"prod1", "description":null, "productStatus":"On", "additionalInfo":null}]}}');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.response = res;
        
        BI_OrderRest.updateOrder();

        system.assertEquals(404,RestContext.response.statuscode); 
        try
        {
        	BI_CSB_OrderMethods.createInternalCases(null, null);
        }
        catch(Exception e)
        {
        	System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
        }
        
        Test.stopTest();
    }
}