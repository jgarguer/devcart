public with sharing class NE_ParcueListController 
{
    public    String accId {get;set;}
    public    String recordTypeId {get;set;}
    
    public NE_ParcueListController(ApexPages.StandardController controller) {
         accId  = Apexpages.Currentpage().getParameters().get('Id');
         RecordType recordlist= [select Id,Name from RecordType WHERE Name='Asset' LIMIT 1]; 
         recordTypeId = recordlist.Id ; 
    }

    
}