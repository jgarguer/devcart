@isTest
public class VE_venta_express_ctrl_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Test class for VE_venta_express_ctrl
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    27/09/2017        Jaime Regidor      Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void testGetAccount(){
        String expectedResult = 'TestAccount';

        Account a = new Account();
        a.Name=expectedResult;
        a.BI_Segment__c = 'test'; //27/09/2017
        a.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        a.BI_Territory__c = 'test'; //27/09/2017
        insert a;
        
        String aId = a.Id;
        
        Test.startTest();
        
        Account resultAccount = VE_venta_express_ctrl.getAccount(aId);
        
        Test.stopTest();
        
        System.assertEquals(resultAccount.Name, expectedResult);
    }
    
    static testMethod void testGetOpportunity(){
        String expectedResult = 'TestOppty';

        Opportunity a = new Opportunity();
        a.Name=expectedResult;
        a.stagename='f5';
        a.closedate=date.today();

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

        insert a;

        BI_MigrationHelper.disableBypass(mapa);
        
        String aId = a.Id;
        
        Test.startTest();
        
        Opportunity resultAccount = VE_venta_express_ctrl.getOpportunity(aId);
        
        Test.stopTest();
        
        System.assertEquals(resultAccount.Name, expectedResult);
    }
    
    
    static testMethod void testNewOppty(){        
        
        Test.startTest();
        
        Opportunity resultAccount = VE_venta_express_ctrl.newOppty();
        
        Test.stopTest();
        
        System.assert(resultAccount!=null);
    }
    
    static testMethod void testInsertOpportunity1(){
        String expectedResult = 'TestOppty';

        Opportunity a = new Opportunity();
        a.Name=expectedResult;
        a.stagename='f5';
        a.closedate=date.today();
        
        Account ac = new Account();
        ac.Name='testAcount';
        ac.BI_Segment__c = 'test'; //27/09/2017
        ac.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        ac.BI_Territory__c = 'test'; //27/09/2017
        
        Test.startTest();
        
        Opportunity resultAccount = VE_venta_express_ctrl.insertOppty(a,ac);
        
        Test.stopTest();
        
        System.assertEquals(resultAccount.Name, expectedResult);
    }
    
    static testMethod void testInsertOpportunity2(){
        String expectedResult = null;

        Opportunity a = new Opportunity();        
        
        Account ac = new Account();
        ac.Name='testAcount';
        ac.BI_Segment__c = 'test'; //27/09/2017
        ac.BI_Subsegment_Regional__c = 'test'; //27/09/2017 
        ac.BI_Territory__c = 'test'; //27/09/2017
        
        Test.startTest();
        
        Opportunity resultAccount = VE_venta_express_ctrl.insertOppty(a,ac);
        
        Test.stopTest();
        
        System.assertEquals(resultAccount.Name, expectedResult);
    }

    
    static testMethod void testforwardOpportunity1(){
        String tipoCierre = 'F1 - Perdida';
        String fechaRealCierre = '2016-11-11';

        Opportunity o = new Opportunity();
        o.Name='expectedResult';
        o.stagename='f5';
        o.closedate=date.today();        
        
        Test.startTest();
        
        String result = VE_venta_express_ctrl.forwardOpportunity(o, tipoCierre, fechaRealCierre);
        
        Test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testforwardOpportunity2(){
        String tipoCierre = 'F1 - Cancelled | Suspended';
        String fechaRealCierre = '2016-11-11';

        Opportunity o = new Opportunity();
        o.Name='expectedResult';
        o.stagename='F4 - Offer Development';
        o.closedate=date.today();        
        
        Test.startTest();
        
        String result = VE_venta_express_ctrl.forwardOpportunity(o, tipoCierre, fechaRealCierre);
        
        Test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testforwardOpportunity3(){
        String tipoCierre = 'F1 - Closed Won';
        String fechaRealCierre = '2016-11-11';

        Opportunity o = new Opportunity();
        o.Name='expectedResult';
        o.stagename='F4 - Offer Development';
        o.closedate=date.today();        
        
        Test.startTest();
        
        String result = VE_venta_express_ctrl.forwardOpportunity(o, tipoCierre, fechaRealCierre);
        
        Test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testforwardOpportunity4(){
        String tipoCierre = 'F1 - Closed Won';
        String fechaRealCierre = '2016-11-11';

        Opportunity o = new Opportunity();
        o.Name='expectedResult';
        o.stagename='F3 - Offer Presented';
        o.closedate=date.today();        
        
        Test.startTest();
        
        String result = VE_venta_express_ctrl.forwardOpportunity(o, tipoCierre, fechaRealCierre);
        
        Test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testforwardOpportunity5(){
        String tipoCierre = 'F1 - Closed Won';
        String fechaRealCierre = '2016-11-11';

        Opportunity o = new Opportunity();
        o.Name='expectedResult';
        o.stagename='F2 - Negotiation';
        o.closedate=date.today();        
        
        Test.startTest();
        
        String result = VE_venta_express_ctrl.forwardOpportunity(o, tipoCierre, fechaRealCierre);
        
        Test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testforwardOpportunity6(){
        String tipoCierre = 'F1 - Closed Won';
        String fechaRealCierre = '2016-11-11';

        Opportunity o = new Opportunity();
        o.stagename='F1 - Closed Lost';
        
        Test.startTest();
        
        String result = VE_venta_express_ctrl.forwardOpportunity(o, tipoCierre, fechaRealCierre);
        
        Test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testTrigger(){
        Opportunity o = new Opportunity();
        o.Name='expectedResult';
        o.ve_express__c=true;
        o.stagename='Fx - Solution Definition';
        o.BI_Productos_numero__c=1;
        o.CloseDate=date.today();        

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
        
        insert o;

        o.StageName='F5 - Solution Definition';
        
        Test.startTest();
        
        update o;

        BI_MigrationHelper.disableBypass(mapa);
        
        Test.stopTest();
        System.assert(o!=null);
    }
    
    static testMethod void testTakeOpportunity1(){
        Opportunity o = new Opportunity();
        o.Name='expectedResult';

        o.CloseDate=date.today();   

        o.StageName = 'F5 - Solution Definition'; 

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
        
        insert o;

        BI_MigrationHelper.disableBypass(mapa);
        
        String tipoCierre='Aceptar';
            
        Test.startTest();
        
        String resul = VE_venta_express_ctrl.takeOpportunity(o, tipoCierre);
        
        Test.stopTest();
        System.assert(resul!=null);
    }
    
    static testMethod void testTakeOpportunity2(){
        Opportunity o = new Opportunity();
        o.Name='expectedResult';

        o.CloseDate=date.today(); 

        o.StageName = 'F5 - Solution Definition';
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);       
        
        insert o;

        BI_MigrationHelper.disableBypass(mapa);
        
        String tipoCierre='Rechazar';
            
        Test.startTest();
        
        String resul = VE_venta_express_ctrl.takeOpportunity(o, tipoCierre);
        
        Test.stopTest();
        System.assert(resul!=null);
    }


    @isTest
    static void stageValues_TEST() {

        Test.startTest();
        
        List <VE_venta_express_ctrl.PickListValue> lst_plvalues = VE_venta_express_ctrl.stageValues();
        
        Test.stopTest();

        System.assert(!lst_plvalues.isEmpty());

    }


    @isTest
    static void getF1Stages_TEST() {

        Test.startTest();
        
        List <String> lst_f1stages = VE_venta_express_ctrl.getF1Stages();
        
        Test.stopTest();

        System.assert(!lst_f1stages.isEmpty());

    }


    @isTest
    static void getSIMPOppType_TEST() {

        Test.startTest();
        
        List <String> lst_SIMP = VE_venta_express_ctrl.getSIMPOppType();
        
        Test.stopTest();

        System.assert(!lst_SIMP.isEmpty());

    }


    @isTest
    static void getOppType_TEST() {

        List<String> country = new List<String>();
        country.add('Argentina');

        List<Account> acc = BI_DataLoad.loadAccounts(1, country);

        Test.startTest();
        
        List <String> lst_OppType = VE_venta_express_ctrl.getOppType(acc.get(0).Id);
        
        Test.stopTest();

        System.assert(!lst_OppType.isEmpty());

    }


    @isTest
    static void getOppLostReason_TEST() {

        Test.startTest();
        
        List <String> lst_reason = VE_venta_express_ctrl.getOppLostReason('F1 - Closed Lost');
        
        Test.stopTest();

        System.assert(!lst_reason.isEmpty());

    }
    
}