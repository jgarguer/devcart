public with sharing class BI_NewObjectButtonRegionCtrl {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos
Company:       Salesforce.com
Description:   Controller of BI_New****Button pages
Test Class:    BI_NewObjectButtonRegionCtrl_TEST

History:
 
<Date>                  <Author>                <Change Description>
06/08/2014              Micah Burgos            Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public String   user_regionName     {get; set;}  
    public String   user_regionId       {get; set;}
    public String   user_country       {get; set;}
        
    public String   acc_regionId        {get; set;}
    //public String   acc_regionName      {get; set;}
    public String   acc_country         {get; set;}
    public String   acc_Name            {get; set;}
    public String   acc_Id              {get; set;}
        
    public String   opp_Id              {get; set;}
    public String   opp_Name            {get; set;}
    public String   opp_Stage           {get; set;}
    //public Boolean  showOppPais         {get; set;}
    
    public String   recordType          {get; set;}
    public String   recordTypeName      {get; set;}

    public String   ag_name      {get; set;}
    public Id       ag_Id        {get; set;}
    public String   name_ag      {get; set;}
    
    //public String   regionId            {get; set;}
    //public String   regionName          {get; set;}
    //public String   country          {get; set;}
    
    public String   currencyType        {get; set;}
    
    public String camp_Id               {get; set;}
    public String camp_Name             {get; set;}
    
    public String proyect_Id            {get; set;}
    public String proyect_Name          {get; set;}
    
    public String otherParams           {get; set;}
    
    public String def_parent_id         {get; set;}
    public String def_parent_Name       {get; set;}
    
    public String def_contact_id        {get; set;}
        
    public String   keyPrefix           {get; set;}
    private string sObjectTypeRecord;
    
    public map<string,string> dynamicIds {get; set;}

    public String rtype;
    
    private set<String>  setFieldToId;
    private list<Milestone1_Project__c>  lst_project;   
    
    //OBP-27/06/2017
    public String subjectPresales {get; set;}
    
    // START JMF 22/09/2016 - Variables para impedir crear un registro
    public Boolean canCreateRecord {get; set;}
    public String  backURL         {get; set;} // URL al que te devuelve si no se puede crear un registro
    public String  errorMSG        {get; set;} // Si hay no se puede crear un registro, mostrar este mensaje de error
    // END JMF 22/09/2016

    public BI_NewObjectButtonRegionCtrl(ApexPages.StandardController std){
        system.debug('url: '+URL.getCurrentRequestUrl());
        sObjectTypeRecord = std.getRecord().getSObjectType().getDescribe().getName();
        keyPrefix = Schema.getGlobalDescribe().get(sObjectTypeRecord).getDescribe().getKeyPrefix();
        
        System.debug('--> sObjectTypeRecord2: ' + sObjectTypeRecord);
        System.debug('--> keyPrefix: ' + keyPrefix);
        
        
        setFieldToId = new set<String>();
        
        if(sObjectTypeRecord == 'Account'){
            //setFieldToId.add('BI_Pais_Ref__c');
            setFieldToId.add('BI_Country__c');
            setFieldToId.add('BI_Segment__c');
        }
        if(sObjectTypeRecord == 'Opportunity'){
            //setFieldToId.add('BI_Pais_Ref__c');
            setFieldToId.add('BI_Country__c');
            setFieldToId.add('BI_Oportunidad_Padre__c');
        }
        
        if(sObjectTypeRecord == 'Milestone1_Project__c'){
            setFieldToId.add('BI_Country__c');
            setFieldToId.add('BI_Oportunidad_asociada__c');
            setFieldToId.add('HO_Agrupador_Proyecto__c');
        }
        if(sObjectTypeRecord == 'Contact'){
            //setFieldToId.add('BI_Paises_ref__c');
            setFieldToId.add('BI_Country__c');
        }

        if(sObjectTypeRecord == 'Case'){
            //setFieldToId.add('BI_Paises_ref__c');
            setFieldToId.add('BI_Country__c');
            setFieldToId.add('BI_Cabecera_de_factura__c');
            setFIeldToId.add('BI_Nombre_de_la_Oportunidad__c');
            setFieldToId.add('ContactId');
        }
        if(sObjectTypeRecord == 'Campaign'){
            //setFieldToId.add('BI_Pais_Ref__c');
            setFieldToId.add('BI_Country__c');
        }
        if(sObjectTypeRecord == 'Lead'){
            //setFieldToId.add('BI_Pais_Ref__c');
            setFieldToId.add('BI_Country__c');
            setFieldToId.add('BI_Segment__c');
        }

        if(sObjectTypeRecord == 'BI_Punto_de_instalacion__c'){
            setFieldToId.add('BI_Cliente__c');
        }

        if(sObjectTypeRecord == 'BI_Plan_de_accion__c'){
            setFieldToId.add('BI_Cliente__c');
            //setFieldToId.add('BI_Pais_Ref__c');
            setFieldToId.add('BI_Country__c');
        }

        
        canCreateRecord = true; // By default, cases can be created
        backURL = '';
        errorMSG = '';
    } 


    public PageReference actionFunction(){
        try{
            TGS_User_Org__c curUserAccess = TGS_User_Org__c.getInstance();

            dynamicIds = BI_DynamicFieldId.getDynamicField(sObjectTypeRecord, setFieldToId);
            System.debug('DYNAMICS: ' + dynamicIds);

            String mapDebugStr = '{';
            for(String fieldname : dynamicIds.keyset()){
                mapDebugStr += fieldname + '=' + dynamicIds.get(fieldname) + ', ';
            }
            System.debug('Fields: ' + mapDebugStr + '}');
            mapDebugStr = '{';
            for(String name : ApexPages.currentPage().getParameters().keyset()) {
                mapDebugStr += name + '=' + ApexPages.currentPage().getParameters().get(name) + ', ';
            }
            System.debug('PageParemeters: ' + mapDebugStr + '}');
            
            currencyType = (ApexPages.currentPage().getParameters().get('currencyType') != null)?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('currencyType')):'';
            recordType = (ApexPages.currentPage().getParameters().get('recordType') != null)?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('recordType')):'';
            ag_name = (ApexPages.currentPage().getParameters().get('retURL') != null)?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('retURL')):'';
            if(ag_name.split('/').size()>1){
                ag_name = ag_name.split('/')[1];
             //   ag_Id= Id.valueOf(ag_name);
                List<HO_Agrupadores_Project__c> lst_agr = [Select Name from HO_Agrupadores_Project__c WHERE id = :ag_name];
                if(lst_agr.isEmpty()==false)
                  name_ag= lst_agr.get(0).Name;
                System.debug('ID DEL AG: ' + name_ag);
            }

//            regionId = (ApexPages.currentPage().getParameters().get('regionId') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('regionId')):'';
            //regionName = (ApexPages.currentPage().getParameters().get('regionName') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('regionName')):'';
            //country = (ApexPages.currentPage().getParameters().get('country') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('country')):'';
            system.debug('recordtypedebug'+recordType);
            def_parent_id = (ApexPages.currentPage().getParameters().get('def_parent_id') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('def_parent_id')):'';
            if(def_parent_id != ''){
                String nameQuery = (sObjectTypeRecord == 'Case')?'CaseNumber':'Name';
                String query = 'SELECT '+nameQuery+' FROM ' +sObjectTypeRecord+ ' WHERE ID = \''+def_parent_id+'\' limit 1';
                system.debug('***BI_NewObjectButtonRegionCtrl->Constructor: queryCASES ->' + query);
                 sObject def_sObject = Database.query(query);
                 if(def_sObject != null){
                    def_parent_Name = (def_sObject.get(nameQuery) != null)? EncodingUtil.urlEncode(String.valueOf(def_sObject.get(nameQuery)), 'UTF-8'): '';
                 }
            }
            
            otherParams = (ApexPages.currentPage().getParameters().get('otherParams') != null)?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('otherParams')):'';
            if(otherParams != ''){
                otherParams = otherParams.replace('->','=').replace('$$','&');
            }
            
            system.debug('*** sObjectTypeRecord: ' + sObjectTypeRecord);
            system.debug('*** BI_Cabecera_de_factura__c: ' + dynamicIds);
            if(sObjectTypeRecord == 'Case' && dynamicIds.containsKey('BI_Cabecera_de_factura__c')){
                    String fact_Id = (ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Cabecera_de_factura__c') + '_lkid') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Cabecera_de_factura__c') + '_lkid')):'';
                    String fact_Name = (ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Cabecera_de_factura__c')) != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Cabecera_de_factura__c'))):'';
                    fact_Name = (fact_Name != null)?EncodingUtil.urlEncode(fact_Name, 'UTF-8'):'';
                    
                    def_contact_id = (ApexPages.currentPage().getParameters().get('def_contact_id') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('def_contact_id')):'';
            }
            

            
            if(sObjectTypeRecord == 'Milestone1_Project__c'){
                opp_Id = (ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Oportunidad_asociada__c') + '_lkid') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Oportunidad_asociada__c') + '_lkid')):'';
                opp_Name = (ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Oportunidad_asociada__c')) != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Oportunidad_asociada__c'))):'';
                //opp_Name = (opp_Name != null)?EncodingUtil.urlEncode(opp_Name, 'UTF-8'):'';
            } else{
                opp_Id = (ApexPages.currentPage().getParameters().get('opp_Id') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('opp_Id')):'';
                system.debug('*****OPP: ID 2: ' + opp_Id);
            }


            opp_Stage = (ApexPages.currentPage().getParameters().get('opp_Stage') != null)?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('opp_Stage')):'';
            
            acc_Name = (ApexPages.currentPage().getParameters().get('acc_Name') != null )?String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('acc_Name')):'';
            acc_Name = EncodingUtil.urlEncode(acc_Name, 'UTF-8');

//            acc_regionId = '';
//            acc_regionName = '';
            acc_country = '';
            acc_Id = '';
            
            if(ApexPages.currentPage().getParameters().get('def_account_id') != null){
                System.debug('***BI_NewObjectButtonRegionCtrl->Constructor: getAcc_Id of -> def_account_id');
                acc_Id = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('def_account_id'));
            }
            if(ApexPages.currentPage().getParameters().get('acc_Id') != null){
                System.debug('***BI_NewObjectButtonRegionCtrl->Constructor: getAcc_Id of -> acc_Id');
                acc_Id = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('acc_Id'));
            }
            if(ApexPages.currentPage().getParameters().get('accid') != null){
                System.debug('***BI_NewObjectButtonRegionCtrl->Constructor: getAcc_Id of -> accid');
                acc_Id = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('accid'));
            }
            System.debug('***BI_NewObjectButtonRegionCtrl->Constructor: acc_Id -> '+ acc_Id);
            
            if(acc_Id != ''){
                    //                List<Account> lst_acc = [SELECT Id, Name, BI_Pais_Ref__c,BI_Pais_Ref__r.Name FROM Account WHERE Id = :acc_Id limit 1];
                    List<Account> lst_acc = [SELECT Id, Name, BI_Country__c FROM Account WHERE Id = :acc_Id limit 1];
                    System.debug('***BI_NewObjectButtonRegionCtrl->Constructor: lst_acc -> '+ lst_acc);
                    if(!lst_acc.isEmpty()){
                        //acc_regionId = lst_acc[0].BI_Pais_Ref__c;
                        //acc_regionName = (lst_acc[0].BI_Country__c != null)? EncodingUtil.urlEncode(lst_acc[0].BI_Country__c, 'UTF-8'): '';
                        acc_country = (lst_acc[0].BI_Country__c != null)? EncodingUtil.urlEncode(lst_acc[0].BI_Country__c, 'UTF-8'): '';
                        acc_Name = (lst_acc[0].Name != null)? EncodingUtil.urlEncode(lst_acc[0].Name, 'UTF-8'): '';
                    }
            }


            if(opp_Id != null && opp_Id != ''){
                system.debug('*****OPP: ID 3: ' + opp_Id);
                list<Opportunity> lst_opp = [SELECT Account.BI_Country__c, Name FROM Opportunity WHERE Id = :opp_Id];
                if(!lst_opp.isEmpty()){
                    acc_country  = (lst_opp[0].Account.BI_Country__c != null)?EncodingUtil.urlEncode(lst_opp[0].Account.BI_Country__c,'UTF-8'):'';
                    opp_Name = (lst_opp[0].Name != null)?EncodingUtil.urlEncode(lst_opp[0].Name, 'UTF-8'):'';
                    system.debug('*****OPP: NAME 4: ' + opp_Name);
                }
            }

            if(sObjectTypeRecord == 'Case' && def_parent_id != ''){
                Case cas = [SELECT Account.Name, Account.Id, BI_Nombre_de_la_Oportunidad__r.Id, BI_Nombre_de_la_Oportunidad__r.Name FROM Case WHERE Id = :def_parent_id limit 1];
                acc_Id = cas.Account.Id;
                acc_Name = (cas.Account.Name != null)? EncodingUtil.urlEncode(cas.Account.Name, 'UTF-8'): '';
                opp_Id = cas.BI_Nombre_de_la_Oportunidad__r.Id;
                opp_Name = (cas.BI_Nombre_de_la_Oportunidad__r.Name != null)? EncodingUtil.urlEncode(cas.BI_Nombre_de_la_Oportunidad__r.Name, 'UTF-8'): '';

            }
                    
/*            if(regionId != null && regionName == ''){
                List<Region__c> lst_region = [SELECT Name FROM Region__c WHERE Id = :regionId limit 1];
                if(!lst_region.isEmpty()){
                    regionName = (aaaaaa != null)? EncodingUtil.urlEncode(lst_region[0].Name, 'UTF-8'): '';
                }
            }
*/            
            if(recordType != null){
                List<RecordType> lst_rt = [SELECT Name FROM RecordType WHERE Id = :recordType limit 1];
                if(!lst_rt.isEmpty()){
                    recordTypeName = (lst_rt[0].Name != null)? EncodingUtil.urlEncode(lst_rt[0].Name, 'UTF-8'): '';
                }
            }

            if(sObjectTypeRecord == 'Task' || sObjectTypeRecord == 'Event'){
                Map<String, Id> map_rt = new Map<String,Id>();

                //Fix on Query JEG
                for(RecordType rt :[SELECT Id, Name, DeveloperName FROM RecordType WHERE sObjectType = :sObjectTypeRecord]){
                    map_rt.put(rt.DeveloperName, rt.Id);
                }
                
                if(sObjectTypeRecord == 'Task'){
                    recordType = (map_rt.containsKey('BI_Tarea'))?map_rt.get('BI_Tarea'):'';
                }else{
                    recordType = (map_rt.containsKey('BI_Evento'))?map_rt.get('BI_Evento'):'';
                }
            }
             
            
//            user_regionName = '';
//            user_regionId = '';
            user_country = '';
            
            List<User> lst_user = [SELECT Pais__c FROM User WHERE Id = : Userinfo.getUserId() limit 1];
            
            system.debug('***BI_NewObjectButtonRegionCtrl->Constructor: lst_user: ' + lst_user);
            if(!lst_user.isEmpty()){
//                user_regionName = (aaaaaa != null)? EncodingUtil.urlEncode(lst_user[0].Pais__c, 'UTF-8'): '';
                user_country = (lst_user[0].Pais__c != null)? EncodingUtil.urlEncode(lst_user[0].Pais__c, 'UTF-8'): '';
            }
//            system.debug('***BI_NewObjectButtonRegionCtrl->Constructor: user_regionName: ' + user_regionName);
            
            if(user_country != null && acc_country == ''){ //Si no encuentra Acc asociada, coje los datos del usuario.
                acc_country = user_country; //NOT ENCODED BECAUSE user_regionName HAS BEEN ENCODED 
                System.debug('METO usuario');
            }

            
//            system.debug('***BI_NewObjectButtonRegionCtrl-> Constructor: user_regionName: ' + user_regionName);
//            system.debug('***BI_NewObjectButtonRegionCtrl-> Constructor: user_regionId: ' + user_regionId);

//            system.debug('***BI_NewObjectButtonRegionCtrl-> Constructor: acc_regionId: ' + user_regionName);
//            system.debug('***BI_NewObjectButtonRegionCtrl-> Constructor: acc_regionName: ' + acc_regionName);
//            
                //String tipoRegistroId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BIIN_Solicitud_Incidencia_Tecnica' limit 1].Id; 
                //String accountID = ApexPages.currentPage().getParameters().get('def_account_id');
                //String contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
                //tipoRegistroId = tipoRegistroId.substring(0, 15); 
                //IF(recordType == tipoRegistroId) {
                    //    PageReference redirectURL=new PageReference('/apex/BIIN_CreacionIncidencia');
                    //    IF (accountID != null){
                    //        redirectURL.getParameters().put('def_account_id', accountID);
                    //    }
                    // 
                    //    IF (contactID != null){
                    //       redirectURL.getParameters().put('def_contact_id', contactID);
                    //    }    

                    //    //INICIO-MODIFICACION :añadir que recoja todos los parametros que manda el boton y se los envie a la pagina destino. Ejemplo:
                    //    String subject  = ApexPages.currentPage().getParameters().get('cas14'); //Recoges lo que viene
                    //    String status = ApexPages.currentPage().getParameters().get('cas7');
                    //    String caseorigin = ApexPages.currentPage().getParameters().get('cas11');
                    //    String priority  = ApexPages.currentPage().getParameters().get('cas8');
                    //    redirectURL.getParameters().put('subject',subject); 
                    //    redirectURL.getParameters().put('status',status); 
                    //    redirectURL.getParameters().put('caseOrigin',caseorigin); 
                    //    redirectURL.getParameters().put('priority',priority); 
                    //  
                    //    //FIN MODIFICACION

                    //    redirectURL.setRedirect(true);

                    //    return redirectURL; 
                //}

                //inicio modificacion 2
                //String tipoRegistro_agrupador = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI2_Caso_Padre' limit 1].Id; 
                //String accountID_agrupador = ApexPages.currentPage().getParameters().get('cas4_lkid');   
                //String contactID_agrupador = ApexPages.currentPage().getParameters().get('cas3_lkid');
                //String subject_agrupador  = ApexPages.currentPage().getParameters().get('cas14');
                //String status_agrupador = ApexPages.currentPage().getParameters().get('cas7');
                //String caseOrigin_agrupador = ApexPages.currentPage().getParameters().get('cas11');
                //String priority_agrupador  = ApexPages.currentPage().getParameters().get('cas8');
                //String tipocaso_agrupador  = ApexPages.currentPage().getParameters().get('cas5');
                //system.debug('-Origen: '+caseOrigin_agrupador+'-Nombre cliente: '+accountID_agrupador+'-Nombre contacto: '+contactID_agrupador+'-Case Origen: '+caseOrigin_agrupador+'-Priority: '+priority_agrupador);
                ////system.debug('url1: '+URL.getUrl());  
                //system.debug('url2: '+URL.getCurrentRequestUrl());
                //tipoRegistro_agrupador = tipoRegistro_agrupador.substring(0, 15);
                //IF(recordType == tipoRegistro_agrupador) {

                    //    PageReference redirectURL=new PageReference('/500/e?&nooverride=1');
                    //    system.debug('redircturl--'+redirectURL);
                    //    IF (accountID_agrupador != null){
                    //       redirectURL.getParameters().put('cas4_lkid', accountID_agrupador);
                    //    }
                       
                    //    IF (contactID_agrupador != null){
                    //       redirectURL.getParameters().put('cas3_lkid', contactID_agrupador);
                    //    }  

                    //    IF (subject_agrupador != null){
                    //       redirectURL.getParameters().put('cas14', subject_agrupador);
                    //    }  

                    //    IF (status_agrupador != null){
                    //       redirectURL.getParameters().put('cas7', status_agrupador);
                    //    } 

                    //    IF (caseOrigin_agrupador != null){
                    //       redirectURL.getParameters().put('cas11', caseOrigin_agrupador);
                    //    }   

                    //    IF (priority_agrupador != null){
                    //       redirectURL.getParameters().put('cas18', priority_agrupador);
                    //    }

                    //    IF (tipocaso_agrupador != null){
                    //       redirectURL.getParameters().put('cas5', tipocaso_agrupador);
                    //    }
                        
                    //   /*    cas4_lkid={!Case.AccountId}&" 
                    //    + "cas3_lkid={!Case.ContactId}&" 
                    //    + "cas28={!Case.CaseNumber}&" 
                    //    + "cas11={!Case.Origin}&" 
                    //    + "cas5={!Case.Type}&" 
                    //    + "cas6={!Case.Reason}&" 
                    //    + "cas8={!Case.Priority}&" 
                    //    + "cas14={!Case.Subject}&"*/
                        
                    //    redirectURL.setRedirect(true);
                    //    return redirectURL;      
                    //    /*
                    //    "cas4_lkid={!Case.AccountId}&" 
                    //    + "cas3_lkid={!Case.ContactId}&" 
                    //    + "cas28={!Case.CaseNumber}&" 
                    //    + "cas11={!Case.Origin}&" 
                    //    + "cas5={!Case.Type}&" 
                    //    + "cas6={!Case.Reason}&" 
                    //    + "cas8={!Case.Priority}&" 
                    //    + "cas14={!Case.Subject}&" 
                    //    */
                //}
                //fin modificacion 2


                // MOD 3
                if(sObjectTypeRecord == 'Case') {


                if(!Test.isRunningTest()){
                        rtype = BI2_CaseMethods.MAP_RT_NAME.get(recordType);
                }


                    PageReference redirURL = null;

                    // BEG Standard fields
                        String caseAccId  = ApexPages.currentPage().getParameters().get('cas4_lkid');   
                        String caseContId = ApexPages.currentPage().getParameters().get('cas3_lkid');
                        String caseSubj   = ApexPages.currentPage().getParameters().get('cas14');
                        String caseStat   = ApexPages.currentPage().getParameters().get('cas7');
                        String caseOrig   = ApexPages.currentPage().getParameters().get('cas11');
                        String casePrio   = ApexPages.currentPage().getParameters().get('cas8');
                        String caseType   = ApexPages.currentPage().getParameters().get('cas5');
                        String caseReas   = ApexPages.currentPage().getParameters().get('cas6');
                        String casePadr   = ApexPages.currentPage().getParameters().get('cas28');
                        String def_account_id = ApexPages.currentPage().getParameters().get('def_account_id');
                        String def_contact_id = ApexPages.currentPage().getParameters().get('def_contact_id');
                        String def_parent_id  = ApexPages.currentPage().getParameters().get('def_parent_id' );
                    // END Standard Fields

                    // BEG Custom fields
                        String caseFechaSolic = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_COL_Fecha_Radicacion__c')        );
                        String casePais       = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Country__c')                     );
                        String caseLoB        = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Line_of_Business__c')            );
                        String caseProdServ   = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI_Product_Service__c')             );
                        String caseProdDesc   = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI2_PER_Descripcion_de_producto__c'));
                        String caseTipol      = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI2_per_tipologia__c')              );
                        String caseSubTipol   = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI2_per_subtipologia__c')           );
                        String caseTerritorio = ApexPages.currentPage().getParameters().get(dynamicIds.get('BI2_PER_Territorio_caso__c')        );
                    // END Custom Fields

                    // BEG Other Fields
                        
                        String biin_subj = ApexPages.currentPage().getParameters().get('');
                        String biin_stat = ApexPages.currentPage().getParameters().get('');
                        String biin_orig = ApexPages.currentPage().getParameters().get('');
                        String biin_prio = ApexPages.currentPage().getParameters().get('');
                    // END Other Fields
                    
                    if(rtype == 'BI2_Caso_Padre' || rtype == 'BI2_caso_comercial' || rtype == 'BIIN_Incidencia_Tecnica' /*|| rtype == 'BI_Caso_Interno'*/ || rtype == 'BI_Caso_Comercial_Abierto') {
                        redirURL=new PageReference('/500/e?&nooverride=1');
                        redirURL.getParameters().put('RecordType', recordType);

                        if(def_account_id   != null && def_account_id   != '') redirURL.getParameters().put('def_account_id', (def_account_id));
                        else if(caseAccId  != null && caseAccId  != '') redirURL.getParameters().put('cas4_lkid', caseAccId );

                        if(def_contact_id   != null && def_contact_id   != '') redirURL.getParameters().put('def_contact_id', (def_contact_id));
                        else if(caseContId != null && caseContId != '') redirURL.getParameters().put('cas3_lkid', caseContId);

                        if(def_parent_id != null && def_parent_id != '')  redirURL.getParameters().put('def_parent_id', (def_parent_id));

                        if(caseSubj   != null && caseSubj   != '') redirURL.getParameters().put('cas14',     caseSubj  );
                        if(caseStat   != null && caseStat   != '') redirURL.getParameters().put('cas7',      caseStat  );
                        if(caseOrig   != null && caseOrig   != '') redirURL.getParameters().put('cas11',     caseOrig  );
                        if(casePrio   != null && casePrio   != '') redirURL.getParameters().put('cas8',      casePrio  );
                        if(caseType   != null && caseType   != '') redirURL.getParameters().put('cas5',      caseType  );
                        if(caseReas   != null && caseReas   != '') redirURL.getParameters().put('cas6',      caseReas  );
                        if(casePadr   != null && casePadr   != '') redirURL.getParameters().put('cas28',     casePadr  );

                        if(caseFechaSolic != null && caseFechaSolic != '') redirURL.getParameters().put(dynamicIds.get('BI_COL_Fecha_Radicacion__c')        , caseFechaSolic);
                        if(casePais       != null && casePais       != '') redirURL.getParameters().put(dynamicIds.get('BI_Country__c')                     , casePais      );
                        if(caseLoB        != null && caseLoB        != '') redirURL.getParameters().put(dynamicIds.get('BI_Line_of_Business__c')            , caseLoB       );
                        if(caseProdServ   != null && caseProdServ   != '') redirURL.getParameters().put(dynamicIds.get('BI_Product_Service__c')             , caseProdServ  );
                        if(caseProdDesc   != null && caseProdDesc   != '') redirURL.getParameters().put(dynamicIds.get('BI2_PER_Descripcion_de_producto__c'), caseProdDesc  );
                        if(caseTipol      != null && caseTipol      != '') redirURL.getParameters().put(dynamicIds.get('BI2_per_tipologia__c')              , caseTipol     );
                        if(caseSubTipol   != null && caseSubTipol   != '') redirURL.getParameters().put(dynamicIds.get('BI2_per_subtipologia__c')           , caseSubTipol  );
                        if(caseTerritorio != null && caseTerritorio != '') redirURL.getParameters().put(dynamicIds.get('BI2_PER_Territorio_caso__c')        , caseTerritorio);

                        // Agregado fase 2 de BI_EN Colombia
                        if((rtype == 'BI2_Caso_Padre' || rtype == 'BI2_caso_comercial') && user_country == Label.BI_Colombia) {

                            // Pre-carga el país en el caso a crear
                            redirURL.getParameters().put(dynamicIds.get('BI_Country__c'), Label.BI_Colombia);

                            // Pre-carga el horario de oficina en el caso a crear
                            List<BusinessHours> lstBusinessHours = [SELECT Id, Name FROM BusinessHours WHERE Name =: Label.BI2_COL_Horario_de_oficina AND IsActive = true LIMIT 1];
                            if(!lstBusinessHours.isEmpty()) {
                                redirURL.getParameters().put(dynamicIds.get('BusinessHoursId'), lstBusinessHours[0].Id);
                                redirURL.getParameters().put(dynamicIds.get('BusinessHours'), lstBusinessHours[0].Name);
                            }
                        }
                    }
                    else if(rtype == 'BIIN_Solicitud_Incidencia_Tecnica') {
                        String strCountry = String.isNotEmpty(acc_country) ? acc_country : user_country;

                        if(String.isNotEmpty(strCountry)) {
                            List<BI2_Paginas_casos_tecnicos__mdt> mtdPaginasCasosTecnicos = [SELECT BI2_Pagina_creacion__c FROM BI2_Paginas_casos_tecnicos__mdt WHERE DeveloperName =: strCountry];
                            if(!mtdPaginasCasosTecnicos.isEmpty()) {
                                redirURL = new PageReference('/apex/' + mtdPaginasCasosTecnicos[0].BI2_Pagina_creacion__c);
                            } else {
                                redirURL = new PageReference('/apex/BIIN_CreacionIncidencia');
                            }
                        } else {
                            redirURL = new PageReference('/apex/BIIN_CreacionIncidencia');
                        }

                        if(def_account_id   != null && def_account_id   != '' || caseAccId  != null && caseAccId  != '') redirURL.getParameters().put('def_account_id', (def_account_id != null ? def_account_id : caseAccId ));
                        if(def_contact_id   != null && def_contact_id   != '' || caseContId != null && caseContId != '') redirURL.getParameters().put('def_contact_id', (def_contact_id != null ? def_contact_id : caseContId));

                        if(def_parent_id != null && def_parent_id != '')  redirURL.getParameters().put('def_parent_id', (def_parent_id));
                        
                        String ParentNumber = ApexPages.currentPage().getParameters().get('cas28');
                        redirURL.getParameters().put('cas28', ParentNumber );
                        
                        if(biin_subj   != null && biin_subj   != '' || caseSubj != null && caseSubj != '') redirURL.getParameters().put('subject',    (biin_subj != null ? biin_subj : caseSubj));
                        if(biin_stat   != null && biin_stat   != '' || caseStat != null && caseStat != '') redirURL.getParameters().put('status',     (biin_stat != null ? biin_stat : caseStat));
                        if(biin_orig   != null && biin_orig   != '' || caseOrig != null && caseOrig != '') redirURL.getParameters().put('caseorigin', (biin_orig != null ? biin_orig : caseOrig));
                        if(biin_prio   != null && biin_prio   != '' || casePrio != null && casePrio != '') redirURL.getParameters().put('priority',   (biin_prio != null ? biin_prio : casePrio));

                        redirURL.setRedirect(true);
                    }
                    // START JMF 22/09/2016 - Disallow creating a 'Order Management Case' from the 'New Case' button
                    else if(rtype == 'Order_Management_Case' && curUserAccess.TGS_Is_TGS__c) {
                        redirURL = null;
                        canCreateRecord = false;
                        backURL = '/500';
                        errorMSG = Label.TGS_CreacionDirectaCasoOrderManagementCase;
                    }
                    // END JMF 22/09/2016
					//OBP-27/06/2017 
                    else if(rtype == 'BI_O4_Gen_rico_Preventa') {
						subjectPresales = '&' + dynamicIds.get('Subject') + '=To+be+automatically+filled';
                    }
                    //OBP-27/06/2017
                    System.debug('URLLLL: ' + redirURL);
                    return redirURL;
                }
                // FIN 3
            
        }catch (exception exc){
           BI_LogHelper.generate_BILog('BI_NewObjectButtonRegionCtrl.actionFunction', 'BI_EN', exc , 'Controller VF');
        }
        return null;
    }
}