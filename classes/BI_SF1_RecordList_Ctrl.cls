/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Controller for the component BI_SF1_RecordList, used to configure custom views, search bar, and the header

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_SF1_RecordList_Ctrl {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Return a list of objects filtered by the given params

    IN:             String filter - fields separated by ';' used in the where clause
                    String objectType - ApiName of the object
                    String textValue - Value entered by the user in the search bar
                    String descriptionField - fields to be displayed as a description
                    
    OUT:            List<Object>  - returns a list with the records
                    null - when no records are found for that filter

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static List<Object> lookUpRecords(String filter, String objectType, String textValue, String descriptionField){
        
        //Check if value is null, because is typed by the user in an input text
         if(textValue!=''){
                            
            String whereClause = '';
            //filter can contain multiple API names separated by ;
            if(filter.contains(';')){
                
                for(String field : filter.split(';')){
                    textValue = String.escapeSingleQuotes(textValue);
                    whereClause += field + ' LIKE \'%' + textValue + '%\' OR ';
                }
                whereClause = whereClause.removeEnd('OR ');
            }else{
				textValue = String.escapeSingleQuotes(textValue);
                whereClause = filter + ' LIKE \'%' + textValue + '%\'';

            }
            
            if(descriptionField!=null && descriptionField.contains(';')){

                descriptionField = descriptionField.replaceAll(';',',');
            }

            String objectQuery = '';
            //Dynamic query to retrieve the objects Name always required to print a row with at least the name of the record
            //"if" needed because Case does not have name so, I'm using CaseNumber
            if(objectType!='Case'){
                objectQuery = 'SELECT Id, Name' + (descriptionField!=null?', ' + descriptionField : '') + ' FROM ' + objectType + ' WHERE ' + whereCLause + ' LIMIT 10';
            } else{
                objectQuery = 'SELECT CaseNumber' + (descriptionField!=null?', ' + descriptionField : '') + ' FROM ' + objectType + ' WHERE ' + whereCLause + ' LIMIT 10';
            }
            System.debug(objectQuery);

            List<SObject> lst_records = Database.query(objectQuery);

            if(!lst_records.isEmpty()){
                //Calling transformSobject to modify the object passed to javaScript
                return transformSobject(lst_records, descriptionField, objectType);
            }else{
                return null;
            }

        }else{
            return null;
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Return a list of objects filtered by the given params

    IN:             String objectType - ApiName of the object
                    String descriptionField - fields to be displayed as a description
                    String whereClause - the "where" condition for the view
                    
    OUT:            List<Object>  - returns a list with the records
                    null - when no records are found for that filter

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static List<Object> customViews(String objectType, String descriptionField, String whereClause){
        
        //DO NOT FORGET TO PUT AT LEAST "LIMIT" IN THE CUSTOM METADATA custom tab listView config in the field for the where clause

        if(descriptionField.contains(';')){
            descriptionField = descriptionField.replaceAll(';',',');
        }
        DateTime yesterday = DateTime.now().addDays(-1);
        String userId = UserInfo.getUserId();

        String query = 'SELECT Id, Name, ' + descriptionField + ' FROM ' + objectType;
        String whereString = whereClause!=null ? ' ' + whereClause : '';

        String objectQuery = query + whereString;
        System.debug(objectQuery);
        List<SObject> lst_records = Database.query(objectQuery);
        if(!lst_records.isEmpty()){
            return transformSobject(lst_records, descriptionField, objectType);
        }else{
            return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Return a map with the information needed to configure the component recordList.

    IN:             List<SObject> lst_records - List of records retrieved by a query
                    String descriptionField - fields to be displayed as a description, under the name 
                    String objectType - Object apiName
                    
    OUT:            List<Map<String, Object>>  - returns record information modified, to make javaScript coding easier

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static List<Map<String, Object>> transformSobject(List<SObject> lst_records, String descriptionField, String objectType){
        
        List<Map<String, Object>> lst_map_return = new List<Map<String, Object>>();
        Map<String, String> map_descriptionInfo;

        System.debug('transformSobject');
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(objectType);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

        for(SObject record : lst_records){
            
            Map<String, Object> map_objectInfo = new Map<String, Object>();
            map_objectInfo.put('recordInfo', record);

            List<Map<String, String>> list_descriptionInfo = new List<Map<String, String>>();

            if(descriptionField!=null){
                //Cheking if there're multiple description fields
                if(descriptionField.contains(',')){

                    for(String fieldName : descriptionField.split(',')){

                        String label;
                        String value;

                        map_descriptionInfo = new Map<String, String>();
                        
                        //If fieldName contains a dot, that means we've a relationSHip 'Account.Name', so we get Account first then get the value name for Account
                        if(fieldName.contains('.')){
                            List<String> fieldRelation = fieldName.split('\\.');
                            //The Owner object does not exist that's why if I encounter owner in the relationShip I replace it with User instead 
                            label = Schema.getGlobalDescribe().get(fieldRelation[0]=='Owner' ? 'User' : fieldRelation[0]).getDescribe().fields.getMap().get(fieldRelation[1]).getDescribe().getLabel();
                            value = record.getSobject(fieldRelation[0]).get(fieldRelation[1])!=null ? record.getSobject(fieldRelation[0]).get(fieldRelation[1]) + ' ' : '';
                        }else{
                            label = fieldMap.get(fieldName).getDescribe().getLabel();
                            value = (String)record.get(fieldName);
                        }

                        //Generic names like Label and value used in JavaScript to make this dynamic
                        map_descriptionInfo.put('Label', label);
                        map_descriptionInfo.put('value', value);
                        list_descriptionInfo.add(map_descriptionInfo);
                    }
                    map_objectInfo.put('descriptionInfo', list_descriptionInfo);
                    lst_map_return.add(map_objectInfo);
                
                }else{
                    
                    String label;
                    String value;
                    
                    map_descriptionInfo = new Map<String, String>();
                    if(descriptionField.contains('.')){
                        List<String> fieldRelation = descriptionField.split('\\.');
                        label = Schema.getGlobalDescribe().get(fieldRelation[0]).getDescribe().fields.getMap().get(fieldRelation[1]).getDescribe().getLabel();
                        value = record.getSobject(fieldRelation[0]).get(fieldRelation[1])!=null ? record.getSobject(fieldRelation[0]).get(fieldRelation[1]) + ' ' : '';
                    }else{
                        label = fieldMap.get(descriptionField).getDescribe().getLabel();
                        value = (String)record.get(descriptionField);
                    }
                    map_descriptionInfo.put('Label', label);
                    map_descriptionInfo.put('value', value);
                    list_descriptionInfo.add(map_descriptionInfo);
                    map_objectInfo.put('descriptionInfo', list_descriptionInfo);
                    lst_map_return.add(map_objectInfo);
                    
                }
                
            } else{
                lst_map_return.add(map_objectInfo);
            }
        }
        return lst_map_return;

    }
}