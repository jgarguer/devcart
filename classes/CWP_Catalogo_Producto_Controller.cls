public with sharing class CWP_Catalogo_Producto_Controller {
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Create task to the customer executive
    Test Class:    CWP_Catalogo_Producto_Controller_TEST.cls
	
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno        Initial version
    07/12/2015        Guillermo Muñoz       Prevent the email send in Test class 
    10/03/2017 		  Everis				createTask() method duplicated from PCA_IdeasCornerController class in order to keep the functionality after the Lightning migration
                       						Creates a task related with the product shown in CWP_Catalogo_Producto component
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String createTask(String prodId, String productJSON){
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            
            BI_Catalogo_PP__c product = (BI_Catalogo_PP__c)System.JSON.deserialize(productJSON, BI_Catalogo_PP__c.class);
            Task newTask = new Task();
            newTask.Subject = Label.BI_Contratacion+' '+ product.Name;
            Id accId = BI_AccountHelper.getCurrentAccountId();
            newTask.OwnerId = BI_AccountHelper.getCurrentAccount(accId).Owner.Id;
            system.debug('OwnerID**: '+newTask.OwnerId);
            List<BI_Contact_Customer_Portal__c> CCP = [SELECT BI_Contacto__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: BI_AccountHelper.getCurrentAccountId() LIMIT 1];
            
            if(!CCP.IsEmpty()){
                
                newTask.WhoId = CCP[0].BI_Contacto__c;
                system.debug('WhoId:' + newTask.WhoId);
            }
            system.debug('CCP** :' +CCP);
            if(prodId!=null){
                newTask.WhatId = prodId;
            } 
            newTask.Status = Label.BI_NoIniciada;
            newTask.Priority = 'Normal';
            
            system.debug('newTask****: ' + newTask);
            Database.Saveresult resul= Database.insert(newTask,false);
            system.debug('result****: ' + resul);
            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='PCA_comunicacion_nueva_tarea'];
            
            
            if(!templates.isEmpty()){
                List<Messaging.SingleEmailMessage> emailSend = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
              
                mail.settargetObjectId(newTask.OwnerId); 
                string body = templates[0].Body;
                system.debug('body: '+body);
                body = body.replace('{!Task.Subject}', newTask.Subject);
                body = body.replace('{!Task.Status}', newTask.Status);
                body = body.replace('{!Task.Priority}', newTask.Priority);
              
                system.debug('mail: '+mail);
                mail.setSubject(templates[0].Subject);
                mail.setPlainTextBody(body);
                mail.saveAsActivity = false;
                emailSend.add(mail);
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(emailSend);
                }
            }
              
            ConnectApi.ChatterMessage Msg = ConnectAPI.ChatterMessages.sendMessage('El usuario '+UserInfo.getName() +' ha pedido información acerca del producto '+ product.Name+'. ', newTask.OwnerId);
            System.debug('**Mensaje Chatter: '+Msg );
            
            String alert= 'Nuestro equipo se pondrá en contacto con usted para procesar su pedido';
            return alert;
        }
        catch (exception Exc){
            system.debug('error');
            BI_LogHelper.generate_BILog('PCA_CatalogoProdController.createTask', 'Portal Platino', Exc, 'Class');
            return 'error';
        }
    }
}