/**
* Avanxo Colombia
* @author           Oscar Angel
* Project:          Telefonica
* Description:      Clase tipo WebServiceMock del servicio ws_wwwTelefonicaComTelefonicaservices.
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2015-08-19      Oscar Angel             Creación de clase
            1.1     2015-08-27      OJCB                    Increase coverage
*************************************************************************************************************/
@isTest
global class ws_wwwTelefonicaComTelefonicaservice_tst {

    
    public static testMethod void test_method_1(){
        ws_wwwTelefonicaComTelefonicaservices servicio=new ws_wwwTelefonicaComTelefonicaservices();
        ws_wwwTelefonicaComTelefonicaservices.GetUserExternalLoginResponse_element a=new ws_wwwTelefonicaComTelefonicaservices.GetUserExternalLoginResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ErrorType b=new ws_wwwTelefonicaComTelefonicaservices.ErrorType();
        ws_wwwTelefonicaComTelefonicaservices.TechnicalViability_element c=new ws_wwwTelefonicaComTelefonicaservices.TechnicalViability_element();
        ws_wwwTelefonicaComTelefonicaservices.TelefonicaServicesSOAP servicio1= new ws_wwwTelefonicaComTelefonicaservices.TelefonicaServicesSOAP();
        ws_wwwTelefonicaComTelefonicaservices.LegalizeMobileInternetResponse_element d=new ws_wwwTelefonicaComTelefonicaservices.LegalizeMobileInternetResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.AddUserSkillOIM_element e =new ws_wwwTelefonicaComTelefonicaservices.AddUserSkillOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.RemoveUserRoleOIMResponse_element f =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserRoleOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmCollection_element g =new ws_wwwTelefonicaComTelefonicaservices.ConfirmCollection_element();
        ws_wwwTelefonicaComTelefonicaservices.AddUserRoleOIMResponse_element h =new ws_wwwTelefonicaComTelefonicaservices.AddUserRoleOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMIncidenceResponse_element i =new ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMIncidenceResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.SetupMobileInternet_element j =new ws_wwwTelefonicaComTelefonicaservices.SetupMobileInternet_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMProfileType k =new ws_wwwTelefonicaComTelefonicaservices.OIMProfileType();
        ws_wwwTelefonicaComTelefonicaservices.RemoveUserRoleOIM_element l =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserRoleOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMSkillsType m =new ws_wwwTelefonicaComTelefonicaservices.OIMSkillsType();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMIncidence_element n =new ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMIncidence_element();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMProvisioning_fault_element o =new ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMProvisioning_fault_element();
        ws_wwwTelefonicaComTelefonicaservices.SetUserSupervisionOIMResponse_element p =new ws_wwwTelefonicaComTelefonicaservices.SetUserSupervisionOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMSkillType q =new ws_wwwTelefonicaComTelefonicaservices.OIMSkillType();
        ws_wwwTelefonicaComTelefonicaservices.CheckStatusSVA_element r =new ws_wwwTelefonicaComTelefonicaservices.CheckStatusSVA_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMUserType s =new ws_wwwTelefonicaComTelefonicaservices.OIMUserType();
        ws_wwwTelefonicaComTelefonicaservices.AddUserProfileOIM_element t =new ws_wwwTelefonicaComTelefonicaservices.AddUserProfileOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.RemoveUserProfileOIM_element u =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserProfileOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.CreateUserOIM_element v =new ws_wwwTelefonicaComTelefonicaservices.CreateUserOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMRoleType w =new ws_wwwTelefonicaComTelefonicaservices.OIMRoleType();
        ws_wwwTelefonicaComTelefonicaservices.RemoveUserSkillOIM_element x =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserSkillOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.TechnicalViabilityResponse_element y =new ws_wwwTelefonicaComTelefonicaservices.TechnicalViabilityResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmCollectionResponse_element z =new ws_wwwTelefonicaComTelefonicaservices.ConfirmCollectionResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ActivateOperationResponse_element aa =new ws_wwwTelefonicaComTelefonicaservices.ActivateOperationResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.SetUserSupervisionOIM_element bb =new ws_wwwTelefonicaComTelefonicaservices.SetUserSupervisionOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMRolesType cc =new ws_wwwTelefonicaComTelefonicaservices.OIMRolesType();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMProvisioningResponse_element dd =new ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMProvisioningResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.AddUserSkillOIMResponse_element ee =new ws_wwwTelefonicaComTelefonicaservices.AddUserSkillOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.PutPurchaseOrder_element ff =new ws_wwwTelefonicaComTelefonicaservices.PutPurchaseOrder_element();
        ws_wwwTelefonicaComTelefonicaservices.PutPurchaseOrderResponse_element gg =new ws_wwwTelefonicaComTelefonicaservices.PutPurchaseOrderResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.RemoveUserSkillOIMResponse_element hh =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserSkillOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ProvisioningSVAResponse_element ii =new ws_wwwTelefonicaComTelefonicaservices.ProvisioningSVAResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.AddUserProfileOIMResponse_element jj =new ws_wwwTelefonicaComTelefonicaservices.AddUserProfileOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.CreateUserOIMResponse_element kk =new ws_wwwTelefonicaComTelefonicaservices.CreateUserOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.SetupMobileInternetResponse_element ll =new ws_wwwTelefonicaComTelefonicaservices.SetupMobileInternetResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.AddUserRoleOIM_element mm =new ws_wwwTelefonicaComTelefonicaservices.AddUserRoleOIM_element();
        ws_wwwTelefonicaComTelefonicaservices.RemoveUserProfileOIMResponse_element nn =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserProfileOIMResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMProvisioning_element oo =new ws_wwwTelefonicaComTelefonicaservices.ConfirmBRMProvisioning_element();
        ws_wwwTelefonicaComTelefonicaservices.GetUserExternalLogin_element pp =new ws_wwwTelefonicaComTelefonicaservices.GetUserExternalLogin_element();
        ws_wwwTelefonicaComTelefonicaservices.CancellationResponse_element qq =new ws_wwwTelefonicaComTelefonicaservices.CancellationResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.CheckStatusSVAResponse_element rr =new ws_wwwTelefonicaComTelefonicaservices.CheckStatusSVAResponse_element();
        ws_wwwTelefonicaComTelefonicaservices.ProvisioningSVA_element ss =new ws_wwwTelefonicaComTelefonicaservices.ProvisioningSVA_element();
        ws_wwwTelefonicaComTelefonicaservices.ActivateOperation_element tt =new ws_wwwTelefonicaComTelefonicaservices.ActivateOperation_element();
        ws_wwwTelefonicaComTelefonicaservices.Cancellation_element uu =new ws_wwwTelefonicaComTelefonicaservices.Cancellation_element();
        ws_wwwTelefonicaComTelefonicaservices.OIMProfilesType vv =new ws_wwwTelefonicaComTelefonicaservices.OIMProfilesType();
        ws_wwwTelefonicaComTelefonicaservices.LegalizeMobileInternet_element ww =new ws_wwwTelefonicaComTelefonicaservices.LegalizeMobileInternet_element();
         
        
    }
    
    public static testMethod void test_method_2()
      {
        
        Test.startTest();
        Test.setMock( WebServiceMock.class, new ws_wwwTelefonicaComTelefonicaservice_mws() );
        //llamar a la clase que consume el método del servicio web:
        ejecutaMetodos(); 
        Test.stopTest();
      }
    
    public static void ejecutaMetodos(){
        ws_wwwTelefonicaComTelefonicaservices.TelefonicaServicesSOAP servicio1= new ws_wwwTelefonicaComTelefonicaservices.TelefonicaServicesSOAP();
        servicio1.ActivateOperation(1,2,'colUserName','colUserLastname','colUserEmail','colAgentId',3,4,'ttClientName','ttClientLastname','ttClientSegment','ttClientSubsegment','ttAccountId','ttAccountCycle','ttPSStructure');
        servicio1.SetUserSupervisionOIM(1,2,3);
        servicio1.TechnicalViability('pathType',1,'firstPathCharacters','secondPathCharacters','pathZone','crossPathType',2,'firstCrossPathCharacters','secondCrossPathCharacters','crossPathZone','addressNumber','addressCharacters',3,4,5,6,7,'blockSide',8,9,10,11,'complement1','complement2',12,'subCity','TipoConsulta');
        servicio1.ProvisioningSVA('provider','commercialOperation','licenseType','username','password','clientName','clientDocument','clientEmail','clientSecondEmail','phoneNumber','serviceReference','productServiceCode'); 
        servicio1.RemoveUserProfileOIM(1,new ws_wwwTelefonicaComTelefonicaservices.OIMProfilesType());
        servicio1.ConfirmCollection(1,2,'ConfirmationFlag','Description','Date_x');
        servicio1.Cancellation(1,2,'colUserName','colUserLastname','colUserEmail','colAgentId',3,4,'ttClientName','ttClientLastname','ttClientSegment','ttClientSubsegment','ttAccountId','ttAccountCycle',5);
        servicio1.AddUserSkillOIM(new ws_wwwTelefonicaComTelefonicaservices.OIMSkillsType(),1);
        servicio1.RemoveUserSkillOIM(1,new ws_wwwTelefonicaComTelefonicaservices.OIMSkillsType());
        servicio1.CreateUserOIM(new ws_wwwTelefonicaComTelefonicaservices.OIMUserType());
        servicio1.RemoveUserRoleOIM(1,new ws_wwwTelefonicaComTelefonicaservices.OIMRolesType());
        servicio1.SetupMobileInternet('clientIdType','clientIdNumber','clientCode',1,'vendorCode');
        servicio1.AddUserProfileOIM(new ws_wwwTelefonicaComTelefonicaservices.OIMProfilesType(),1);
        servicio1.AddUserRoleOIM(new ws_wwwTelefonicaComTelefonicaservices.OIMRolesType(),1);
        servicio1.CheckStatusSVA('clientId','accountId','idpc');
        servicio1.LegalizeMobileInternet('clientIdType','clientIdNumber','clientCode',1,2,'phoneNumber',3,'imei','icc','observation');
        servicio1.PutPurchaseOrder(1,'tokenId',2,'ttPSStructure');
        servicio1.GetUserExternalLogin(1,2,'colUserName','colUserLastname','colUserEmail','colAgentId',3,4,'ttClientName','ttClientLastname','ttClientSegment','ttClientSubsegment','ttAccountId','ttAccountCycle','ttPSStructure');
        servicio1.ConfirmBRMProvisioning('idPc','codigoPlan','nit','operacionComercial','idError','descripcionError');
        servicio1.ConfirmBRMIncidence('idPc','codigoPlan','numeroIncidencia','cierreIncidencia','codigoApertura','observaciones');
    }
}