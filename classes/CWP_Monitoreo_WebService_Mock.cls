public class CWP_Monitoreo_WebService_Mock {

    public HttpResponse getTicketsMock(String usuario){  
      HttpResponse res = new Httpresponse();
      /*Álvaro López 25/01/2018 - Added <S:Header/>*/  
      String pass = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Header/><S:Body><ns2:getUserInformationResponse xmlns:ns2="http://infinity.telefonica.com/"><applications><application><description>Interactive data visualizations and dashboards.</description><displayName>Tableau</displayName><name>Tableau</name><url>https://sso-des.identity.telefonica.com/oamfed/idp/initiatesso?providerid=Tableau</url></application><application><description>Central Cost Manager is a Telecom Expense Management service that gives you control on your telco costs and allows you to cut them, providing a complete set of customizable reports.</description><displayName>CCM</displayName><name>Navita</name><url>https://mnc-dev.navita.com.br/sso/home</url></application><application><description>Web portal for authorized customers, where you can check the status of your orders / queries / incidences and or open new ones.</description><displayName>SalesForce</displayName><name>SalesForce</name><url>https://uat-telefonicab2b.cs81.force.com/empresasplatino/login</url></application></applications><userName>mnc.cacmdv01@cspad-premad.wh.telefonica</userName></ns2:getUserInformationResponse></S:Body></S:Envelope>';
      String error = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Header/><S:Body><ns2:Fault xmlns:ns2="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns3="http://www.w3.org/2003/05/soap-envelope"><faultcode xmlns:ns0="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">ns0:FailedAuthentication</faultcode><faultstring>FailedAuthentication : The security token cannot be authenticated.</faultstring></ns2:Fault></S:Body></S:Envelope>';
	
        if(usuario == 'failTest'){
            res.setBody(error);
        	res.setStatusCode(500);
        }else{
            res.setBody(pass);
        	res.setStatusCode(201);
        }
        
        return res;
     }
}