/*-------------------------------------------------------------------
    Author:         Virgilio Utrera
    Company:        Salesforce.com
    Description:    Custom controller for the FOCO Product Amount vs Goal chart
    Test Class:     BI_FOCOProductAmountVsGoalChart_TEST
    History
    <Date>          <Author>           <Change Description>
    17-Oct-2014     Virgilio Utrera    Initial Version
    20-Oct-2014		Virgilio Utrera    Adjusted getChartData
    22-Oct-2014		Virgilio Utrera    Adjusted getChartData
    11-Mar-2015		Virgilio Utrera    Refactored getChartData to handle large data volumes
-------------------------------------------------------------------*/

public class BI_FOCOProductAmountVsGoalChart_CTRL {
	
	public transient Boolean renderErrorMessage { get; set; }
    public transient String chartData { get; set; }
    public transient String errorMessage { get; set; }
    public transient String userDefaultCurrency { get; set; }

    /*-------------------------------------------------------------------
        Author:         Virgilio Utrera
        Company:        Salesforce.com
        Description:    Class constructor
        IN:
        OUT:
        History
        <Date>          <Author>           <Change Description>
        17-Oct-2014     Virgilio Utrera    Initial Version
    -------------------------------------------------------------------*/

	public BI_FOCOProductAmountVsGoalChart_CTRL() {
		errorMessage = '';
		chartData = '';
		getChartData();

		if(errorMessage.length() > 0)
			renderErrorMessage = true;
		else
			renderErrorMessage = false;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets the chart data for the running user
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		17-Oct-2014     Virgilio Utrera    Initial Version
		20-Oct-2014     Virgilio Utrera    Moved queries to BI_FOCOUtil class and added currency conversion
		22-Oct-2014		Virgilio Utrera    Added all user goals to the chart data regardless of what FOCO data contains
		11-Mar-2015		Virgilio Utrera    Refactored method to handle large data volumes
	-------------------------------------------------------------------*/

	private void getChartData() {
		Decimal amount;
		Decimal goal;
		Decimal totalAmount;
		Decimal totalGoal;
		Id userId;
		List<AggregateResult> focoResults = new List<AggregateResult>();
		List<BI_Objetivo_Comercial__c> goalResults = new List<BI_Objetivo_Comercial__c>();
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		Map<String, Decimal> amountsMap = new Map<String, Decimal>();
		Map<String, Decimal> gapsMap = new Map<String, Decimal>();
		Map<String, Decimal> goalsMap = new Map<String, Decimal>();
		Map<String, Double> conversionRatesMap = new Map<String, Double>();
		Set<Id> subordinateIds;
		String corporateCurrency;

		amount = 0;
		goal = 0;
		totalAmount = 0;
		totalGoal = 0;
		userId = UserInfo.getUserId();
		userDefaultCurrency = UserInfo.getDefaultCurrency();
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);
		currenciesList = BI_FOCOUtil.getCurrenciesList();

		// Fills up map with conversion rates, sets the currency corporate code
		for(CurrencyType currencyType : currenciesList) {
			conversionRatesMap.put(currencyType.IsoCode, currencyType.ConversionRate);

			if(currencyType.IsCorporate)
				corporateCurrency = currencyType.IsoCode;
		}

		// Get Ids from all subordinate users
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);

		if(!subordinateIds.isEmpty())
			// There are subordinates, query FOCO records from user and subordinates for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByProduct(userId, subordinateIds);
		else
			// There are no subordinates, query FOCO records from user for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByProduct(userId, null);

		// Stop the execution if there are no FOCO records to show
		if(focoResults.isEmpty()) {
			errorMessage = 'No se han encontrado Registros de Datos FOCO para el año en curso';
			System.debug(errorMessage);
			return;
		}

		if(!subordinateIds.isEmpty())
			// There are subordinates, query commercial goals from user and subordinates for current year
			goalResults =  BI_FOCOUtil.getProductGoalsFromCurrentYear(userId, subordinateIds);
		else
			// There are no subordinates, query commercial goals from user for current year
			goalResults =  BI_FOCOUtil.getProductGoalsFromCurrentYear(userId, null);

		// Stop the execution if there are no commercial goal records to show
		if(goalResults.isEmpty() || goalResults[0].BI_Objetivo__c == null) {    
			errorMessage = 'No se han encontrado Objetivos Comerciales para el año en curso';
			System.debug(errorMessage);
			return;
		}

		// Populate amounts map
		for(AggregateResult ar : focoResults) {
			// FOCO results' BI_Monto__c is automatically returned in the org's currency because the query contains a SUM() aggregate function with a GROUP BY clause
			if(userDefaultCurrency == corporateCurrency)
				// No currency conversion needed
				amount = ((Decimal)ar.get('BI_Monto__c')).setScale(2, System.RoundingMode.CEILING);
			else
				// Convert amount into user's currency
				amount = (((Decimal)ar.get('BI_Monto__c')) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);

			if((String)ar.get('BI_Nombre__c') != null) {
				if(amountsMap.get((String)ar.get('BI_Nombre__c')) != null)
					amountsMap.put((String)ar.get('BI_Nombre__c'), amountsMap.get((String)ar.get('BI_Nombre__c')) + amount);
				else
					amountsMap.put((String)ar.get('BI_Nombre__c'), amount);
			}
			else {
				if(amountsMap.get('-') != null)
					amountsMap.put('-', amountsMap.get('-') + amount);
				else
					amountsMap.put('-', amount);
			}

			totalAmount = totalAmount + amount;
		}

		// Populate goals map
		for(BI_Objetivo_Comercial__c oc : goalResults) {
			if(oc.CurrencyIsoCode == userDefaultCurrency)
				// Currency is the same as user's currency
				goal = oc.BI_Objetivo__c.setScale(2, System.RoundingMode.CEILING);
			else {
				if(oc.CurrencyIsoCode == corporateCurrency)
					// Currency is corporate currency, amount is converted into user's default currency
					goal = (oc.BI_Objetivo__c * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
				else
					// Currency is other than user's or corporate
					goal = ((oc.BI_Objetivo__c / conversionRatesMap.get(oc.CurrencyIsoCode)) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
			}

			if(goalsMap.get(oc.BI_Rama__r.BI_Nombre__c) != null)
				goalsMap.put(oc.BI_Rama__r.BI_Nombre__c, goalsMap.get(oc.BI_Rama__r.BI_Nombre__c) + goal);
			else
				goalsMap.put(oc.BI_Rama__r.BI_Nombre__c, goal);

			totalGoal = totalGoal + goal;
		}

		// Populate gaps map
		for(String product : amountsMap.keySet()) {
			if(goalsMap.get(product) != null)
				gapsMap.put(product, goalsMap.get(product) - amountsMap.get(product));
			else
				gapsMap.put(product, (0 - amountsMap.get(product)));
		}

		// Build the chart data for each product
		chartData = '[';

		for(String product : amountsMap.keySet()) {
			if(product == '-' || goalsMap.get(product) == null)
				chartData = chartData + '[\'' + product + '\', ' + amountsMap.get(product) + ', 0.00, ' + gapsMap.get(product) + '], ';
			else
				chartData = chartData + '[\'' + product + '\', ' + amountsMap.get(product) + ', ' + goalsMap.get(product) + ', ' + gapsMap.get(product) + '], ';
		}

		chartData = chartData + '[\'Total\', ' + totalAmount + ', ' + totalGoal + ', ' + String.valueOf(totalGoal - totalAmount) + ']]';
	}
}