public class SEG_Zippex
{    
    private Map<String, FileObject> zipFileMap = new Map<String, FileObject>{};
    protected String zipFileString = '';  // stores the Hex version of the file blob

    private final String endCentralDirSignature  = '504b0506'; //Little Endian formatted signature (4 bytes)
    private String offsetOfStartOfCentralDir     = '00000000'; //(4 bytes)
    
    
    public SEG_Zippex(){}
   
   
    public SEG_Zippex(Blob fileData)
    {
        zipFileString = EncodingUtil.convertToHex(fileData);
        Integer offsetOfEndCentralDirSignature = zipFileString.lastIndexOf(endCentralDirSignature);
        
        Integer numberOfFiles = SEG_HexUtils.hexToIntLE(zipFileString.mid(offsetOfEndCentralDirSignature+10*2, 2*2));    //offset 10, 2 bytes
        
        offsetOfStartOfCentralDir = zipFileString.mid(offsetOfEndCentralDirSignature+16*2, 4*2); // 4 bytes
        
        Integer offset = SEG_HexUtils.hexToIntLE(offsetOfStartOfCentralDir);
        
        for (Integer fileLoop = 0; fileLoop < numberOfFiles; fileLoop++)
        {
            FileObject tempFile = new FileObject(zipfileString, offset);
            zipFileMap.put(EncodingUtil.convertFromHex(tempFile.fileName).toString(), tempFile);
            offset = tempFile.c_offsetToNextRecord;
        }
    }
    
    //  Returns a set of filenames from the current Zip archive.
    public Set<String> getFileNames()
    {
        return zipFileMap.keySet().clone();
    }

    public Blob getFile(String fileName)
    {
        if (!zipFileMap.containsKey(fileName)) {return null;}

        FileObject tempFileObject = zipFileMap.get(fileName);

        if (tempFileObject.fileDataInZipStr) {
            tempFileObject.readLocalFileHeader(zipFileString, SEG_HexUtils.hexToIntLE(tempFileObject.offsetOfLH));
        }

        if (tempFileObject.compressionMethod == '0000')
        {
            return EncodingUtil.convertFromHex(tempFileObject.compressedFileData);
        }
        else if (tempFileObject.compressionMethod == '0800') //DEFLATED
        {
            return EncodingUtil.convertFromHex(new SEG_Puff(tempFileObject.compressedFileData,
                             SEG_HexUtils.hexToIntLE(tempFileObject.compressedSize),
                             SEG_HexUtils.hexToIntLE(tempFileObject.uncompressedSize)).inflate());
        }
        else {
            System.assert(false,'Unsupported compression method: ' + SEG_HexUtils.hexToIntLE(tempFileObject.compressionMethod));
            return null;
        }
    }

    // Local File Header Signature
    private final static String LFHSignature = '504b0304';      
    
    // Central Directory Signature
    private final static String CFHSignature = '504b0102';

    // Data Descriptor Signature
    private final static String DDSignature  = '504b0708';


    public class FileObject
    {
        public String creatorVersion      = '0A00';     // (2 bytes) likely Windows NT
        public String minExtractorVersion = '0A00';     // (2 bytes) likely Windows NT
        public String gPFlagBit           = '0000';     // (2 bytes) general purpose flag bit
        public String compressionMethod   = '0000';     // (2 bytes) 0 = no compression
        public String lastModTime         = '0000';     // (2 bytes) 
        public String lastModDate         = '0000';     // (2 bytes) 
        public String crc32               = null;       // (4 bytes) 
        public String compressedSize      = '00000000'; // (4 bytes), creates (n)
        public String uncompressedSize    = '00000000'; // (4 bytes) 
        public String fileNameLength      = '00000000'; // (2 bytes), creates (f)
        public String extraFieldLength    = '0000';     // (2 bytes), creates (e)
        public String fileCommentLength   = '0000';     // (2 bytes), creates (c)
        public String diskNumStart        = '0000';     // (2 bytes) 
        public String internalFileAtt     = '0000';     // (2 bytes) 
        public String externalFileAtt     = '00000000'; // (4 bytes) 
        public String offsetOfLH          = '00000000'; // (4 bytes) 
        public String fileName            = '';         // (f bytes) 
        public String extraField          = '';         // (e bytes) 
        public String fileComment         = '';         // (c bytes) 
        public String compressedFileData  = '';         // (n bytes)

        public Integer c_offsetToNextRecord = 0;  //offsetToNext Central Dir Record
        public Integer l_offsetToNextRecord = 0;  //offsetToNext Local Header Record
        public Boolean fileDataInZipStr     = false;

    
        // Constructor
        public FileObject(){
            fileDataInZipStr = false;
        }

        //Reading Central Directory File Header
        public FileObject (String zipFileString, Integer offset)
        {
            offset *= 2;

            creatorVersion      = zipFileString.mid(offset+4*2,  2*2);// (2 bytes) likely Windows NT  Offset 4
            minExtractorVersion = zipFileString.mid(offset+6*2,  2*2);// (2 bytes) likely Windows NT  Offset 6
            gPFlagBit           = zipFileString.mid(offset+8*2,  2*2);// (2 bytes) general purpose flag bit  Offset 8
            compressionMethod   = zipFileString.mid(offset+10*2, 2*2);// (2 bytes) no compression  Offset 10
            lastModTime         = zipFileString.mid(offset+12*2, 2*2);// (2 bytes)   Offset 12
            lastModDate         = zipFileString.mid(offset+14*2, 2*2);// (2 bytes)   Offset 14
            crc32               = zipFileString.mid(offset+16*2, 4*2);// (4 bytes)   Offset 16
            compressedSize      = zipFileString.mid(offset+20*2, 4*2);// (4 bytes)   creates (n) Offset 20
            uncompressedSize    = zipFileString.mid(offset+24*2, 4*2);// (4 bytes)   Offset 24
            fileNameLength      = zipFileString.mid(offset+28*2, 2*2);// (2 bytes)   creates (f) Offset 28
            extraFieldLength    = zipFileString.mid(offset+30*2, 2*2);// (2 bytes)   creates (e) Offset 30
            fileCommentLength   = zipFileString.mid(offset+32*2, 2*2);// (2 bytes)   creates (c) Offset 32
            diskNumStart        = zipFileString.mid(offset+34*2, 2*2);// (2 bytes)   Offset 34
            internalFileAtt     = zipFileString.mid(offset+36*2, 2*2);// (2 bytes)   Offset 36
            externalFileAtt     = zipFileString.mid(offset+38*2, 4*2);// (4 bytes)   Offset 38
            offsetOfLH          = zipFileString.mid(offset+42*2, 4*2);// (4 bytes)   Offset 42
            
            offset /= 2;

            Integer theStart = offset+46;
            Integer theEnd = theStart + SEG_HexUtils.hexToIntLE(fileNameLength);
            fileName = zipFileString.substring(theStart*2, theEnd*2);// (f bytes)       Offset 46
            theStart = theEnd;
            theEnd = theStart + SEG_HexUtils.hexToIntLE(extraFieldLength);
            extraField = zipFileString.substring(theStart*2, theEnd * 2);// (e bytes)        Offset 46 + fileNameLength
            theStart = theEnd;
            theEnd = theStart + SEG_HexUtils.hexToIntLE(fileCommentLength);
            fileComment = zipFileString.substring(theStart*2, theEnd*2);// (c bytes)        Offset 46 + fileNameLength + extraFieldLength
            c_offsetToNextRecord = theEnd;
            fileDataInZipStr = true; //true because we are reading an existing Zip archive
        }

        //Reading Local File Header (which also contains the data)
        public void readLocalFileHeader (String zipFileString, Integer offset)
        {
            Integer strOffset = offset *2;

            minExtractorVersion  = zipFileString.mid(strOffset+4*2,  2*2);// (2 bytes) likely Windows NT  Offset 4
            gPFlagBit            = zipFileString.mid(strOffset+6*2,  2*2);// (2 bytes) general purpose flag bit  Offset 6
            compressionMethod    = zipFileString.mid(strOffset+8*2,  2*2);// (2 bytes) no compression  Offset 8
            lastModTime          = zipFileString.mid(strOffset+10*2, 2*2);// (2 bytes)  Offset 10
            lastModDate          = zipFileString.mid(strOffset+12*2, 2*2);// (2 bytes)  Offset 12
            
            //System.debug('gPFlagBit '+gPFlagBit);
            if ((SEG_HexUtils.hexToIntLE(gPFlagBit.left(2)) & 8 ) == 0) {
                crc32            = zipFileString.mid(strOffset+14*2, 4*2);// (4 bytes)  Offset 14
                compressedSize   = zipFileString.mid(strOffset+18*2, 4*2);// (4 bytes), creates (n) Offset 18
                uncompressedSize = zipFileString.mid(strOffset+22*2, 4*2);// (4 bytes)  Offset 22
            }

            fileNameLength       = zipFileString.mid(strOffset+26*2, 2*2);// (2 bytes), creates (f)     Offset 26
            extraFieldLength     = zipFileString.mid(strOffset+28*2, 2*2);// (2 bytes), creates (e)     Offset 28

            //offset = offset /2;

            Integer theStart = offset+30;
            Integer theEnd = theStart + SEG_HexUtils.hexToIntLE(fileNameLength);
            fileName = zipFileString.substring(theStart*2, theEnd*2);// (f bytes)       Offset 30
            theStart = theEnd;
            theEnd = theStart + SEG_HexUtils.hexToIntLE(extraFieldLength);
            extraField = zipFileString.substring(theStart*2, theEnd*2);// (e bytes)        Offset 30 + fileNameLength
            theStart = theEnd;
            theEnd = theStart + SEG_HexUtils.hexToIntLE(compressedSize);
            compressedFileData = zipFileString.substring(theStart*2, theEnd*2);// (c bytes)        Offset 30 + fileNameLength + extraFieldLength
            l_offsetToNextRecord = theEnd;
            
            if ((SEG_HexUtils.hexToIntLE(gPFlagBit.left(2)) & 8 ) != 0) {
                l_offsetToNextRecord *= 2;
                String signature = zipFileString.mid(l_offsetToNextRecord+0*2, 4*2); // (4 bytes)
                if (signature != DDSignature) {l_offsetToNextRecord -= 4*2;}
                crc32            = zipFileString.mid(l_offsetToNextRecord+4*2, 4*2); // (4 bytes)  Offset 0/4
                compressedSize   = zipFileString.mid(l_offsetToNextRecord+8*2, 4*2); // (4 bytes), creates (n)     Offset 4/8
                uncompressedSize = zipFileString.mid(l_offsetToNextRecord+12*2, 4*2);// (4 bytes)  Offset 8/12
                l_offsetToNextRecord /= 2;
                l_offsetToNextRecord += 16; 
            }
        }
    }
}