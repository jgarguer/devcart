public class BIIN_UNICA_UserMethods 
{
/*----------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González
  Company:       HPE
  Description:   
                 
  IN:            ApexTrigger.new
  OUT:           Void
      
  History: 
  
  <Date>              <Author>                            <Change Description>
  22/06/2016          José Luis González                  Initial versión
  14/06/2016          José Luis González                  Retrieve AccountId
  ----------------------------------------------------------------------------------------------------------------------*/
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José Luis gonzález Beltrán
    Company:       HPE
    Description:   
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History:     
    
    <Date>                  <Author>                        <Change Description>
    22/06/2016              José Luis gonzález Beltrán      UNICA integrate Contacts with RoD
    06/07/2016              Julio Laplaza Soria             Avoid calls when test is executing
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateRoDContact(List<User> lst_users_new, List<User> lst_users_old)
    {
      List<Id> contactIds = new List<Id>();
      for(User us : lst_users_new)
      {
        contactIds.add(us.ContactId);
      }

      List<Contact> contacts = [SELECT Id, BI_Tipo_de_contacto__c, FirstName, LastName, Estado__c, Email, AccountId 
        FROM Contact WHERE Id IN :contactIds];

      if (!contacts.isEmpty()) 
      {
        for(Contact con : contacts)
        {
          if (!Test.isRunningTest())
          {
            BIIN_UNICA_ContactHandler.afterInsert(JSON.serialize(con));
          }
        }
      }
    }
  }