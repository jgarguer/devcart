public with sharing class BI_LineaDeServicioMethods {
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Methods executed by the BI_Linea_de_Recambio__c trigger.
   
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca     Initial version
    20/01/2016        Guillermo Muñoz    Method 'substractLine' added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for Insert BI_Linea_de_Venta__c
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertLineaDeServicioIMP(list<BI_Linea_de_Servicio__c> lNew){ 
    	
	    	set<string> auxIdNameSol = new set<string>{};
	    	set<string> auxIdNameMod = new set<string>{};
	    	set<string> auxIdNameAcc = new set<string>{};


	    	
	    	for (BI_Linea_de_Servicio__c aux: lNew){

					if(aux.BI_Catalogo_de_Servicio_1_TEM__c != null){
	    				aux.BI_Catalogo_de_Servicio_1_TEM__c = aux.BI_Catalogo_de_Servicio_1_TEM__c.split(' - ')[0];
	    			}

					system.debug('auxx: '+aux);

	    		
    	    		if ((aux.BI_Solicitud__c == null) && aux.BI_Solicitud_TEMP__c != '' && aux.BI_Solicitud_TEMP__c != null){
    	    			auxIdNameSol.add(aux.BI_Solicitud_TEMP__c);
    	    		}	  		
    	    		
    	    		if ((aux.BI_Modelo__c == null) && aux.BI_Catalogo_de_Servicio_1_TEM__c != '' && aux.BI_Catalogo_de_Servicio_1_TEM__c != null){
    	    			auxIdNameMod.add(aux.BI_Catalogo_de_Servicio_1_TEM__c);
    	    		}
                
	    	}
	    	//Buscar Id Solicitud para asociar las Lineas de Recambio
	    	if (auxIdNameSol.size()>0){ 
		    	list<BI_Solicitud_envio_productos_y_servicios__c> lauxSol = [SELECT Id,Name FROM BI_Solicitud_envio_productos_y_servicios__c  WHERE Id IN :auxIdNameSol OR Name IN :auxIdNameSol ORDER BY Id, Name];
				if (lauxSol.size()>0){
					for (BI_Solicitud_envio_productos_y_servicios__c auxSol: lauxSol){
						for (BI_Linea_de_Servicio__c aux: lNew){
							if ((aux.BI_Solicitud_TEMP__c == String.valueOf(auxSol.Id)) || (aux.BI_Solicitud_TEMP__c == auxSol.Name)){
								 aux.BI_Solicitud__c = auxSol.Id;
							}
						}
					}
				}
	    	}
	    	//****************************************************
	    	//Buscar Id Modelo para asociar las Líneas de Recambio
	    	if (auxIdNameMod.size()>0){ 
	    		list<RecordType> lRT = new list<RecordType>([SELECT Id,DeveloperName FROM RecordType WHERE sObjectType = 'BI_Modelo__c' AND DeveloperName='BI_Servicio' ORDER BY Id]);
	    		if (lRT.size()>0){
		    		list<BI_Modelo__c> lauxMod = [SELECT Id,Name FROM BI_Modelo__c  WHERE RecordTypeId=:lRT[0].Id AND ( Id IN :auxIdNameMod OR Name IN :auxIdNameMod ) ORDER BY Id, Name];
		    		if (lauxMod.size()>0){
						for (BI_Modelo__c auxMod: lauxMod){
							for (BI_Linea_de_Servicio__c aux: lNew){
                                system.debug('#Debug aux.BI_Catalogo_de_Servicio_1_TEM__c '+ aux.BI_Catalogo_de_Servicio_1_TEM__c);
                                system.debug('#Debug auxMod.Name '+ auxMod.Name);
                                system.debug('#Debug String.valueOf(auxMod.Id) '+ String.valueOf(auxMod.Id));
								if ((aux.BI_Catalogo_de_Servicio_1_TEM__c == String.valueOf(auxMod.Id)) || (aux.BI_Catalogo_de_Servicio_1_TEM__c == auxMod.Name)){
									 aux.BI_Modelo__c = auxMod.Id;
								}
							}
						}	    		
		    		}
	    		}
	    	}    
            for(BI_Linea_de_Servicio__c aux:lNew){
            	
            	system.debug(aux);
            	
                if (aux.BI_Modelo__c == null /*|| aux.BI_Solicitud__c == null*/ || aux.BI_Accion__c == null /*|| aux.BI_Numero_de_telefono__c == null*/)
                    aux.addError('Por favor, rellene los campos obligatorios');
            }	
	    	  	
    }   

        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for Insert BI_Linea_de_Venta__c
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void countRequests(list<BI_Linea_de_Servicio__c> news){       	
    	Set <Id> set_solicitudes = new Set <Id>();
	    	
    	for (BI_Linea_de_Servicio__c aux: news){	    		
    		if (aux.BI_Solicitud__c != null) {
    			set_solicitudes.add(aux.BI_Solicitud__c);
    		}	  		
    	}	
    	if(!set_solicitudes.IsEmpty()){
	    	List <BI_Solicitud_envio_productos_y_servicios__c> lst_solicitud = [SELECT Id, BI_Numero_de_lineas_de_servicio__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id IN :set_solicitudes];
	    	if(!lst_solicitud.IsEmpty()){
		    	for(BI_Solicitud_envio_productos_y_servicios__c solicitud:lst_solicitud){
		    		for (BI_Linea_de_Servicio__c aux: news){	    		
			    		if (aux.BI_Solicitud__c == solicitud.Id) {
			    			if(solicitud.BI_Numero_de_lineas_de_servicio__c !=null){
			    				solicitud.BI_Numero_de_lineas_de_servicio__c = solicitud.BI_Numero_de_lineas_de_servicio__c + 1;
			    			}else{
								solicitud.BI_Numero_de_lineas_de_servicio__c = 1;
			    			}
			    		}	  		
			    	}
		    	}
		    }
		    update lst_solicitud;
	    }	
	    
	    	  	
    }   

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   /* FAR 05/06/2015: Comentado ya que no se usa
	public static void eliminarLineaDeServicio(list<BI_Linea_de_Servicio__c> lLineaServicio){
		try{
			if (lLineaServicio.size()>0) delete lLineaServicio;
        }catch (exception exc){
			BI_LogHelper.generate_BILog('BI_LineaDeServicioHelper.eliminarLineaDeServicio', 'BI_EN', exc, 'Trigger');
		}		
	}
	*/

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:  
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /* FAR 05/06/2015: Comentado ya que no se usa
    public static void callBatchWS(list<Id> lst_linea, List <Id> lst_users){
    	//Database.executeBatch(new BI_LineaDeVenta_JOB(lst_linea,lst_users), 50);
    	
    }
    */

      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:  
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    public static void validateBackOffice(List<BI_Linea_de_Servicio__c> news, List<BI_Linea_de_Servicio__c> olds){
    	
    	List<Id> lst_ven = new List<Id>();
    	
    	Integer i = 0;
    	for(BI_Linea_de_Servicio__c lven:news){
    		if(lven.BI_Estado_de_linea_de_servicio__c == 'Activado' && olds[i].BI_Estado_de_linea_de_servicio__c != 'Activado' && lven.BI_Solicitud__c != null)
    			lst_ven.add(lven.Id);
    		i++;
    	}
    	
    	if(!lst_ven.isEmpty())
    		BI_Siscel_Helper.BackOfficeServicio_Mass(UserInfo.getUserEmail(), lst_ven, 0, null);
    	
    } 

      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:  
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static void ValidateMass(Map<Id, BI_Linea_de_Servicio__c> news){
    	
    	if (BI_GlobalVariables.stopTriggerIMPExcel == false){
    		
			List<Id> lst_ven = new List<Id>();
			lst_ven.addAll(news.keySet());
				
			//FUTURE
			BI_Siscel_Helper.LineaServicio_Mass(UserInfo.getUserEmail(), lst_ven, 0, null);
			
    	}
    	
    } 

      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:  
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static void updateSol(List<BI_Linea_de_Servicio__c> news, List<BI_Linea_de_Servicio__c> olds){
    	
    	Set<Id> set_sol = new Set<Id>();
    	
    	Integer i = 0;
    	for(BI_Linea_de_Servicio__c serv:news){
    		if(serv.BI_Solicitud__c != null && 
    		   (serv.BI_Estado_de_linea_de_servicio__c != olds[i].BI_Estado_de_linea_de_servicio__c ||  
    		    serv.BI_Solicitud_descargada_BackOffice__c != olds[i].BI_Solicitud_descargada_BackOffice__c)
    		  )
    		{
    			set_sol.add(serv.BI_Solicitud__c);
    		}	
    		i++;
    	}
    	
    	if(!set_sol.isEmpty()){
    		
    		List<BI_Solicitud_envio_productos_y_servicios__c> lst_sol = new List<BI_Solicitud_envio_productos_y_servicios__c>();
    		
    		Set<String> set_status = new Set<String>{'Activado', 'Desactivado'};
    		
    		for(BI_Solicitud_envio_productos_y_servicios__c sol:[select Id, (select Id, BI_Solicitud_descargada_BackOffice__c, BI_Estado_de_linea_de_servicio__c from 
    															 Linea_de_Servicio__r) from BI_Solicitud_envio_productos_y_servicios__c where Id IN :set_sol])
    		{
    			
    			if(!sol.Linea_de_Servicio__r.isEmpty()){
    				
    				Boolean sol_desc = true;
    				Boolean sol_closed = true;
    				Boolean sol_incomplete = true;
    				
    				for(BI_Linea_de_Servicio__c serv:sol.Linea_de_Servicio__r){
    					if(!serv.BI_Solicitud_descargada_BackOffice__c)
    						sol_desc = false;
    					if(!set_status.contains(serv.BI_Estado_de_linea_de_servicio__c))
    						sol_closed = false;
    					else if(serv.BI_Estado_de_linea_de_servicio__c != 'No ejecutado')
    						sol_incomplete = false;
    				}
    				
    				if(sol_incomplete){
    					sol.BI_Estado_de_servicios__c = 'Incompleto';
    					lst_sol.add(sol);
    				}else if(sol_closed){
    					sol.BI_Estado_de_servicios__c = 'Cerrado';
    					lst_sol.add(sol);
    				}else if(sol_desc){
    					sol.BI_Estado_de_servicios__c = 'Activación';
    					lst_sol.add(sol);
    				}
    				
    			}
    			
    		}
    		
    		try{
    			update lst_sol;
    		}catch(Exception exc){
    			BI_LogHelper.generate_BILog('BI_LineaDeServicioMethods.updateSol', 'BI_EN', exc, 'Trigger');
    		}
    		
    	}
    	
    }     

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that substract the value of BI_Numero_de_lineas_de_servicio__c when a Línea de servicio is deleted.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        20/01/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void substractLine(List<BI_Linea_de_Servicio__c> news, List<BI_Linea_de_Servicio__c> olds){

        List <BI_Linea_de_Servicio__c> toProcess = new List <BI_Linea_de_Servicio__c>();
        Set <Id> set_idSol = new Set <Id>();
        Map <Id, BI_Solicitud_envio_productos_y_servicios__c> map_solToUpdate = new Map <Id, BI_Solicitud_envio_productos_y_servicios__c>();

        for(BI_Linea_de_Servicio__c serv : olds){

            if(serv.BI_Solicitud__c != null){

                toProcess.add(serv);
                set_idSol.add(serv.BI_Solicitud__c);
            }
        }

        if(!set_idSol.isEmpty()){

            Map <Id, BI_Solicitud_envio_productos_y_servicios__c> map_sol = new Map<Id, BI_Solicitud_envio_productos_y_servicios__c>([SELECT Id, BI_Numero_de_lineas_de_servicio__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id in: set_idSol]);

            if(!map_sol.isEmpty()){

                for(BI_Linea_de_Servicio__c serv : toProcess){

                    if(map_sol.containsKey(serv.BI_Solicitud__c)){

                        Decimal numServs = map_sol.get(serv.BI_Solicitud__c).BI_Numero_de_lineas_de_servicio__c;
                        if(numServs != null && numServs != 0){

                            numServs--;
                            map_sol.get(serv.BI_Solicitud__c).BI_Numero_de_lineas_de_servicio__c = numServs;
                            map_solToUpdate.put(serv.BI_Solicitud__c,map_sol.get(serv.BI_Solicitud__c));
                        }
                    }
                }
            }
        }

        if(!map_solToUpdate.isEmpty()){
            update map_solToUpdate.values();
        }

    }
   
}