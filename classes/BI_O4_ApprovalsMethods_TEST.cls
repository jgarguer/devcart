@isTest
private class BI_O4_ApprovalsMethods_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test class to manage the coverage code for BI_O4_ApprovalsMethods class 
    
    History: 
    
    <Date> 					<Author> 				<Change Description>
    18/01/2017      		Alvaro García	    		Initial Version
    20/09/2017        Angel F. Santaliestra 		Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
    	BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method of BI_O4_ApprovalsMethods.createEvent()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García                Initial Version
    18/02/2017						Guillermo Muñoz				Fix bugs on deploy
	11/10/2017						Oscar Bartolo			  	Add field BI_O4_USER_GSE__c
    17/10/2017						Gawron, Julián				Add BI_MigrationHelper.skipAllTriggers();
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void createEvent_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

	BI_MigrationHelper.skipAllTriggers(); 
		String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});

		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);

		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id, BI_O4_GSE__c = 'yo mismo', BI_O4_USER_GSE__c = UserInfo.getUserId());
	   	insert quote;

	   	BI_O4_Thursday_Limit__c thursdayLimit = new BI_O4_Thursday_Limit__c(Name = 'Number of Days', BI_O4_Event_Day__c = 'Thursday', BI_O4_Need_Day_Restriction__c = true,
	   																		BI_O4_Number_of_days__c = 180, BI_O4_Slot_Timing__c = 15);
	   	insert thursdayLimit;

		List <RecordType> lst_rt = [SELECT Id, sObjectType, Developername  FROM RecordType WHERE Developername IN ('BI_O4_Gate_3','BI_O4_Gate_2')];

		Map <String, Id> map_rt = new Map <String, Id>();

		for(RecordType rt : lst_rt){

			map_rt.put(rt.DeveloperName, rt.Id);
		}

		BI_O4_Approvals__c appgate2 = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_2'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Other', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert appgate2;

		BI_O4_Approvals__c app = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_3'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));

		BI_O4_Approvals__c app2 = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_3'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));

	BI_MigrationHelper.cleanSkippedTriggers();
		Test.startTest();

			insert app;

			Event ev = [SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
						FROM Event WHERE WhatId = :app.Id LIMIT 1];
			System.assertEquals(app.Id, ev.WhatId);
			System.debug('!!!ev: ' + ev);

			insert app2;

			ev = [SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
						FROM Event WHERE WhatId = :app2.Id LIMIT 1];
			System.assertEquals(app2.Id, ev.WhatId);
			System.debug('!!!ev2: ' + ev);
		
		Test.stopTest();
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method of BI_O4_ApprovalsMethods.updateDRB_Event()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García                Initial Version
    18/02/2017						Guillermo Muñoz				Fix bugs on deploy
	11/10/2017						Oscar Bartolo			  	Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void updateDRB_Event_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});

		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);

		BI_MigrationHelper.disableBypass(mapa);

		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id, BI_O4_GSE__c = 'yo mismo', BI_O4_USER_GSE__c = UserInfo.getUserId());
	   	insert quote;

	   	BI_O4_Thursday_Limit__c thursdayLimit = new BI_O4_Thursday_Limit__c(Name = 'Number of Days', BI_O4_Event_Day__c = 'Thursday', BI_O4_Need_Day_Restriction__c = true,
	   																		BI_O4_Number_of_days__c = 180, BI_O4_Slot_Timing__c = 15);
	   	insert thursdayLimit; 

		List <RecordType> lst_rt = [SELECT Id, sObjectType, Developername  FROM RecordType WHERE Developername IN ('BI_O4_Gate_3','BI_O4_Gate_2')];

		Map <String, Id> map_rt = new Map <String, Id>();

		for(RecordType rt : lst_rt){

			map_rt.put(rt.DeveloperName, rt.Id);
		}

		BI_O4_Approvals__c appgate2 = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_2'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Other', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert appgate2;

		BI_O4_Approvals__c app = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_3'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert app;

		Test.startTest();
			System.debug('!!!app: ' + app);
			app.BI_O4_DRB_requested_date__c = System.today().addDays(25);
			System.debug('!!!Entra al update de app');
			update app;

			BI_O4_Approvals__c appUPdate = [SELECT Id, BI_O4_DRB_requested_date__c FROM BI_O4_Approvals__c WHERE Id = :app.Id LIMIT 1];
			System.debug('!!!appUPdate: ' + appUPdate);
		
		Test.stopTest();
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method of BI_O4_ApprovalsMethods.deleteDRB_Event()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García                Initial Version
    18/02/2017						Guillermo Muñoz				Fix bugs on deploy
	11/10/2017						Oscar Bartolo			  	Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void deleteDRB_Event_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});

		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);

		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id, BI_O4_GSE__c = 'yo mismo', BI_O4_USER_GSE__c = UserInfo.getUserId());
	   	insert quote;

	   	BI_O4_Thursday_Limit__c thursdayLimit = new BI_O4_Thursday_Limit__c(Name = 'Number of Days', BI_O4_Event_Day__c = 'Thursday', BI_O4_Need_Day_Restriction__c = true,
	   																		BI_O4_Number_of_days__c = 180, BI_O4_Slot_Timing__c = 15);
	   	insert thursdayLimit;

		List <RecordType> lst_rt = [SELECT Id, sObjectType, Developername  FROM RecordType WHERE Developername IN ('BI_O4_Gate_3','BI_O4_Gate_2')];

		Map <String, Id> map_rt = new Map <String, Id>();

		for(RecordType rt : lst_rt){

			map_rt.put(rt.DeveloperName, rt.Id);
		}

		BI_O4_Approvals__c appgate2 = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_2'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Other', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));

		Test.startTest();
		insert appgate2;

		BI_O4_Approvals__c app = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_3'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert app;
			
			app.BI_O4_DRB_request_status__c = 'Slot Reject';
			update app;

			Event ev;
			try{
				ev = [SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
					FROM Event WHERE WhatId = :app.Id LIMIT 1];
			} catch (Exception exc) {}
			
			System.assertEquals(null, ev);

		
		Test.stopTest();
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method of BI_O4_ApprovalsMethods.updateEvent_confirmedDate()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García                Initial Version
    18/02/2017						Guillermo Muñoz				Fix bugs on deploy
	11/10/2017						Oscar Bartolo			  	Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void updateEvent_confirmedDate_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_MigrationHelper.skipAllTriggers();
		String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});

		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);

		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id, BI_O4_GSE__c = 'yo mismo', BI_O4_USER_GSE__c = UserInfo.getUserId());
	   	insert quote; 

	   	BI_O4_Thursday_Limit__c thursdayLimit = new BI_O4_Thursday_Limit__c(Name = 'Number of Days', BI_O4_Event_Day__c = 'Thursday', BI_O4_Need_Day_Restriction__c = true,
	   																		BI_O4_Number_of_days__c = 180, BI_O4_Slot_Timing__c = 15);
	   	insert thursdayLimit;

		List <RecordType> lst_rt = [SELECT Id, sObjectType, Developername  FROM RecordType WHERE Developername IN ('BI_O4_Gate_3','BI_O4_Gate_2')];

		Map <String, Id> map_rt = new Map <String, Id>();

		for(RecordType rt : lst_rt){

			map_rt.put(rt.DeveloperName, rt.Id);
		}

		BI_O4_Approvals__c appgate2 = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_2'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Other', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert appgate2;

		BI_MigrationHelper.cleanSkippedTriggers();
		
		BI_O4_Approvals__c app = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_3'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert app;

		Test.startTest();

			app.BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(22);
			update app;

			Event ev = [SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
					FROM Event WHERE WhatId = :app.Id LIMIT 1];
			System.assertEquals(ev.BI_O4_DRB_Confirmed_Date__c, app.BI_O4_DRB_confirmed_DateTime__c);
		
		Test.stopTest();
	}


	@isTest
	static void sendG3Notification_TEST() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});
        List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);




			 List<UserRole> roles = [SELECT Id FROM UserRole 
    				                 WHERE DeveloperName = 'BI_O4_TGS_FinancialControllerSpain_OffNet_France' 
    				                 OR DeveloperName = 'BI_O4_TNA_PricingManager'];
            Profile profile = [select id from profile where profile.name = 'BI_Standard_ARG' LIMIT 1];

            //Usuario inicial que pertenece al grupo por Id
            User usuario = new User (alias = 'ej1',
                                    email='user1@testing.com',
                                    emailencodingkey='UTF-8',
                                    lastname='initial_user_Id' ,
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    ProfileId = profile.id,
                                    BI_Permisos__c='Ejecutivo de Cliente',
                                    timezonesidkey=Label.BI_TimeZoneLA,
                                    username= 'example1@example1.example1',
                                    //BI_CodigoUsuario__c = String.valueOf(externalId),
                                    IsActive = true);
   
			if(roles.size() < 1){
             UserRole RoleUser = new UserRole();
			System.runAs(new User(Id=UserInfo.getUserId())){
		     RoleUser.DeveloperName = 'BI_O4_TNA_PricingManager';
             RoleUser.Name = 'BI_O4_TNA_PricingManager';
             insert RoleUser;
			  }





             usuario.UserRoleId = RoleUser.Id;
            }else{
				usuario.UserRoleId = roles[0].Id;
			}

			System.runAs(new User(Id=UserInfo.getUserId())){
		     insert usuario;
			}
	

        //Javier Almirón - 02/10/2017- He introducido el Test.starTest()/Test.stopTest() para evitar Too Many queries	
		Test.startTest();

		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id, BI_O4_GSE__c = 'yo mismo', BI_O4_USER_GSE__c = UserInfo.getUserId());
	   	insert quote; 

	   	List <RecordType> lst_rt = [SELECT Id, sObjectType, Developername  FROM RecordType WHERE Developername = 'BI_O4_Gate_3'];

	   	System.debug('RT QUE RECUPERO: ' + lst_rt.get(0));

		Map <String, Id> map_rt = new Map <String, Id>();

		for(RecordType rt : lst_rt){

			map_rt.put(rt.DeveloperName, rt.Id);
		}

		System.debug('RT QUE INTENTO PONER. ' + map_rt.get('BI_O4_Gate_3'));

	   	BI_O4_Approvals__c appGate3 = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = map_rt.get('BI_O4_Gate_3'), 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert appGate3;

		Test.stopTest();
	}



    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test to covert the exceptions	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García               Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void exception_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_TestUtils.throw_exception = true;

		Test.startTest();
			
			BI_O4_ApprovalsMethods.createEvent(null);
			BI_O4_ApprovalsMethods.updateDRB_Event(null, null);
            BI_O4_ApprovalsMethods.deleteDRB_Event(null, null);
            BI_O4_ApprovalsMethods.updateEvent_confirmedDate(null, null);
		
		Test.stopTest();
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Almirón
    Company:       Aborda
    Description:   Test method of BI_O4_ApprovalsMethods.migAppOpp()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/07/2017                      Javier Alrmirón             Initial Version
    28/09/2017						Javier Alrmirón             Add method "BI_MigrationHelper.skipAllTriggers()" to avoid Too many queries
	11/10/2017						Oscar Bartolo			  	Add field BI_O4_USER_GSE__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @isTest
    static void migAppOppTest (){


       Profile prof = [SELECT Id FROM Profile WHERE Name IN ('BI_O4_TGS_Standard') LIMIT 1];

        User u = new User(
            Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                Alias = 'testuser', Email = 'abortest@email.com', Profileid = prof.id, TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey='UTF-8', BI_Permisos__c = 'Ejecutivo de Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');

        insert u;

        Map<Integer, BI_bypass__c> mapa;
        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
       
        List <Recordtype> rType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_Oportunidad_Regional','BI_Ciclo_completo', 'BI_O4_Proposal_Ready', 
                         									                      'BI_O4_Gate_3_Closed', 'BI_O4_Organic_Growth', 'TGS_Legal_Entity')];

        Map <String, Id> rT = new Map <String, Id>();

         for ( Recordtype r : rType){

         	rT.put (r.DeveloperName , r.Id);
         }


        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'Test2',
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'TEST2',
                                  BI_Subsegment_local__c = 'Top 1',
                                  BI_Subsegment_Regional__c = 'test',
									BI_Territory__c = 'test',
                                  RecordTypeId = rT.get('TGS_Legal_Entity')
                                  );    
        lst_acc1.add(acc1);
        System.runAs(usu){

        	TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        	if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
	
        	    userTGS.TGS_Is_BI_EN__c = true;
        	    userTGS.TGS_Is_TGS__c = false;
	
        	    if(userTGS.Id != null){
        	        update userTGS;
        	    }
        	    else{
        	        insert userTGS;
        	    }
        	}


        	
        	insert lst_acc1;
        	
	
        	List<Account> lst_acc = new List<Account>();       
        	Account acc = new Account(Name = 'Test2',
        	                          BI_Activo__c = Label.BI_Si,
        	                          BI_Segment__c = 'Test',
        	                          BI_Subsegment_Regional__c = 'test',
										BI_Territory__c = 'test',
        	                          BI_Subsegment_local__c = 'Top 1',
        	                          ParentId = lst_acc1[0].Id
        	                          );    
        	lst_acc.add(acc);

        	BI_MigrationHelper.skipAllTriggers();

        	insert lst_acc;

        	Opportunity opp = new Opportunity(Name = 'Test2' + system.now(),
        	                                  CloseDate = Date.today(),
        	                                  StageName = Label.BI_F6Preoportunidad,
        	                                  AccountId = lst_acc[0].Id,
        	                                  RecordTypeId =rT.get('BI_Oportunidad_Regional')
        	                                  );    
        	
        	insert opp;
	
	
        	OpportunityTeamMember teamMember = new OpportunityTeamMember ();
        	teamMember.TeamMemberRole = 'Global Solutions Engineer Principal';
        	teamMember.User = u;
        	teamMember.Opportunity = opp;
        	teamMember.OpportunityId = opp.ID;
        	teamMember.UserId = u.Id;
	
        	insert teamMember;
	
        	Set <Id> oppSet = new Set <Id>();
        	oppSet.add(opp.Id);
	
	
        	Quote proposalSyncOne = new Quote();
        	proposalSyncOne.RecordTypeId =  rT.get('BI_O4_Proposal_Ready');
        	proposalSyncOne.Name = 'Test';
        	proposalSyncOne.OpportunityId = opp.Id;
        	proposalSyncOne.BI_O4_GSE__c = 'TestUser UserTest';
            proposalSyncOne.BI_O4_USER_GSE__c = UserInfo.getUserId();
        	proposalSyncOne.BI_O4_Proposal_FCV_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_FCV_TGS__c = 10;
        	proposalSyncOne.BI_O4_Proposal_iCOST_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_iCOST_TGS__c = 10;
        	proposalSyncOne.BI_O4_Proposal_iCAPEX_TGS__c = 10;
        	proposalSyncOne.BI_O4_Proposal_iCAPEX_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_NRR_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_NRR_TGS__c = 10;
        	proposalSyncOne.BI_O4_Proposal_NRC_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_NRC_TGS__c = 10;
        	proposalSyncOne.BI_O4_Proposal_MRR_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_MRR_TGS__c = 10;
        	proposalSyncOne.BI_O4_Proposal_MRC_TEF__c = 10;
        	proposalSyncOne.BI_O4_Proposal_MRC_TGS__c = 15;
        	proposalSyncOne.BI_O4_URL_to_OneDrive__c = 'y';
	
        	insert proposalSyncOne;
	
 	
        	Set <Id> proposalSyncOneSet = new Set <Id>();
        	proposalSyncOneSet.add (proposalSyncOne.ID);
	
	
        	BI_O4_Approvals__c approvalGo = new BI_O4_Approvals__c();
        	approvalGo.RecordTypeId =  rT.get('BI_O4_Gate_3_Closed');
        	approvalGo.BI_O4_Opportunity__c = opp.ID;
        	approvalGo.BI_O4_Proposal__c = proposalSyncOne.ID;
        	approvalGo.BI_O4_Financial_Gate_Approved_by__c = 'DRB';
        	approvalGo.BI_O4_Gate_status__c = 'GO';
        	approvalGo.BI_O4_FCV__c = 5; 
        	approvalGo.BI_O4_FCV_Off_Net__c = 5; 
        	approvalGo.BI_O4_NRR__c = 5; 
        	approvalGo.BI_O4_NRR_TGS__c = 5;
        	approvalGo.BI_O4_MRR_TEF__c = 5; 
        	approvalGo.BI_O4_MRR_TGS__c = 5;
        	approvalGo.BI_O4_NRC_TEF__c = 5; 
        	approvalGo.BI_O4_NRC_TGS__c = 5;
        	approvalGo.BI_O4_MRC_TEF__c = 5; 
        	approvalGo.BI_O4_MRC_TGS__c = 5;
        	approvalGo.BI_O4_iCOST_TGS__c = 5; 
        	approvalGo.BI_O4_Incremental_Cost__c = 5;
        	approvalGo.BI_O4_CAPEX__c = 5; 
        	approvalGo.BI_O4_CAPEX_TGS__c = 5;
	
            Test.startTest();
        	
            insert approvalGo;
	
	
        	BI_MigrationHelper.cleanSkippedTriggers();
	
	
        	    
        	    opp.SyncedQuoteId = proposalSyncOne.ID;
        	    opp.StageName = 'F5 - Solution Definition';
        	    update  opp;
	
        	    List <BI_O4_Approvals__c> approvTest = [SELECT ID, BI_O4_Gate_status__c, BI_O4_Proposal__c
        	                                        	FROM BI_O4_Approvals__c 
        	                                        	WHERE BI_O4_Proposal__c IN: proposalSyncOneSet
        	                                        	AND BI_O4_Opportunity__c IN: oppSet];
	
	
        	    BI_O4_ApprovalsMethods.migAppOpp(approvTest, new Map<Id, BI_O4_Approvals__c>{approvalGo.Id => approvalGo} );
	
	
        	    List <BI_O4_Approvals__c> approvTestCon = [SELECT ID, BI_O4_Gate_status__c, BI_O4_Opportunity__c, BI_O4_FCV__c, BI_O4_FCV_Off_Net__c, 
        	                                        	   BI_O4_NRR__c, BI_O4_NRR_TGS__c,
        	                                        	   BI_O4_MRR_TEF__c, BI_O4_MRR_TGS__c,
        	                                        	   BI_O4_NRC_TEF__c, BI_O4_NRC_TGS__c,
        	                                        	   BI_O4_MRC_TEF__c, BI_O4_MRC_TGS__c,
        	                                        	   BI_O4_iCOST_TGS__c, BI_O4_Incremental_Cost__c,
        	                                        	   BI_O4_Incremental_OIBDA__c, BI_O4_oibda_tgs__c,
        	                                        	   BI_O4_Incremental_OIBDA_Percent__c, BI_O4_Incremental_oibda_tgs__c,
        	                                        	   BI_O4_CAPEX__c, BI_O4_CAPEX_TGS__c,
        	                                        	   BI_O4_Incremental_OI__c, BI_O4_iOI_TGS__c,
        	                                        	   BI_O4_Incremental_OI_Percent__c, BI_O4_Incremental_OI_tgs__c
        	                                        	   FROM BI_O4_Approvals__c 
        	                                        	   WHERE BI_O4_Proposal__c IN: proposalSyncOneSet
        	                                        	   AND BI_O4_Opportunity__c IN: oppSet];
	
	
	
        	    List <Opportunity> oppTest = [SELECT ID, 
        	                              BI_O4_FCV_Business_Case__c, BI_O4_BC_FCV_TGS__c, 
        	                              BI_O4_Opportunity_Won_BC_NRR_TEF__c, BI_O4_BC_NRR_TGS__c,
        	                              BI_O4_Opportunity_Won_BC_MRR_TEF__c, BI_O4_BC_MRR_TGS__c,
        	                              BI_O4_Opportunity_Won_BC_NRC_TEF__c, BI_O4_BC_NRC_TGS__c,
        	                              BI_O4_Opportunity_Won_BC_MRC_TEF__c, BI_O4_BC_MRC_TGS__c,
        	                              BI_O4_Opportunity_Won_BC_iCOST_TEF__c, BI_O4_BC_iCOST_TGS__c,
        	                              BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c, BI_O4_BC_iCAPEX_TGS__c
        	                              FROM Opportunity
        	                              WHERE ID IN: oppSet];
	
	
        	   System.assertEquals(approvTestCon[0].BI_O4_FCV__c, oppTest[0].BI_O4_FCV_Business_Case__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_FCV_Off_Net__c, oppTest[0].BI_O4_BC_FCV_TGS__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_NRR__c, oppTest[0].BI_O4_FCV_Business_Case__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_NRR_TGS__c, oppTest[0].BI_O4_BC_NRR_TGS__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_MRR_TEF__c, oppTest[0].BI_O4_Opportunity_Won_BC_NRR_TEF__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_MRR_TGS__c, oppTest[0].BI_O4_BC_MRR_TGS__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_NRC_TEF__c, oppTest[0].BI_O4_Opportunity_Won_BC_NRC_TEF__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_NRC_TGS__c, oppTest[0].BI_O4_BC_NRC_TGS__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_MRC_TEF__c, oppTest[0].BI_O4_Opportunity_Won_BC_MRC_TEF__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_MRC_TGS__c, oppTest[0].BI_O4_BC_iCOST_TGS__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_iCOST_TGS__c, oppTest[0].BI_O4_FCV_Business_Case__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_Incremental_Cost__c, oppTest[0].BI_O4_Opportunity_Won_BC_iCOST_TEF__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_CAPEX__c, oppTest[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c);
        	   System.assertEquals(approvTestCon[0].BI_O4_CAPEX_TGS__c, oppTest[0].BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c);
	
        	 Test.stopTest();
	
        }                                                  

    }

	
}