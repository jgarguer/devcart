/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-23      Daniel ALexander Lopez (DL)     Create Class      
                    19/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c

*************************************************************************************/

@isTest
private class BI_COL_ConsultCoverage_tst
{

	public static list<Opportunity>                                 lstOppAsociadas = new list<Opportunity>();
    public static Opportunity                                       objOppty;
    
   // public static List<BI_COL_FUN_Demo_ctr.wrappMs>               lstWrapper = new List<BI_COL_FUN_Demo_ctr.wrappMs>();
  
    public static Account                                           objCuenta;

    public static BI_COL_Anexos__c                                  objAnexos;

    public static List<BI_COL_Modificacion_de_Servicio__c>          lstModSer;
    public static BI_COL_Modificacion_de_Servicio__c                objModSer;

    public static List<BI_COL_Descripcion_de_servicio__c>           lstDesSer;
    public static BI_COL_Descripcion_de_servicio__c                 objDesSer;

    public static List<RecordType>                                  rtBI_FUN;

    public static BI_Sede__c                                        direccion;

   
   static testMethod void test() {
    
    BI_COL_ConsultCoverage_ctr cc=new BI_COL_ConsultCoverage_ctr();
    cc.getItemsCobertura();
    cc.ejecutar();
    cc.cobertura='Cobertura LB y BA';
    
    //objetosPruebas_cls misObjetos=new objetosPruebas_cls();
    
    Account cliente = new Account();//misObjetos.ObjAccount();
        cliente.Name                                  = 'prueba';
        cliente.BI_Country__c                         = 'Colombia';
        cliente.TGS_Region__c                         = 'América';
        cliente.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
        cliente.CurrencyIsoCode                       = 'GTQ';

        cliente.BI_Segment__c                         = 'test';
        cliente.BI_Subsegment_Regional__c             = 'test';
        cliente.BI_Territory__c                       = 'test';
    insert cliente;
    
    
    direccion                                            = new BI_Sede__c();
        
          //direccion.BI_COL_Ciudad_Departamento__c = ciudad.Id;
          direccion.BI_Direccion__c = 'Calle 36 d DUR nr 75 25';
          direccion.BI_Localidad__c = 'Test Local';
          direccion.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
          direccion.BI_COL_Sucursal_en_uso__c = 'Libre';
          direccion.BI_Country__c = 'Colombia';
          direccion.Name = 'Calle 36 d DUR nr 75 25';
          direccion.BI_Codigo_postal__c = '12356';
        direccion.BI_COL_Direccion_Split__c ='AV|6 | | | |CL |23 | | |N';
        direccion.BI_COL_Direccion_Text__c='2237';
        
    insert direccion;
    
    PageReference pageRef0 = Page.BI_COL_ConsultCoverage_pag;
      Test.setCurrentPage(pageRef0);
        ApexPages.currentPage().getParameters().put('idSucursal', direccion.id);
    
    cc.ejecutar();
    
    cc=new BI_COL_ConsultCoverage_ctr();
    
    cc.cobertura='Cobertura LB y BA';
    direccion.BI_COL_Direccion_Split__c='AV |6 |';
    update direccion;
    
    
      Test.setCurrentPage(pageRef0);
        ApexPages.currentPage().getParameters().put('idSucursal', direccion.id);
    
    cc.ejecutar();
   
    }
    
    static testMethod void test2() {
    
    BI_COL_ConsultCoverage_ctr cc=new BI_COL_ConsultCoverage_ctr();
    cc.getItemsCobertura();
    cc.ejecutar();
    cc.cobertura='Cobertura Conectividad Básica';
    
    //objetosPruebas_cls misObjetos=new objetosPruebas_cls();
    
    Account cliente = new Account();//misObjetos.ObjAccount();
        cliente.Name                                  = 'prueba';
        cliente.BI_Country__c                         = 'Colombia';
        cliente.TGS_Region__c                         = 'América';
        cliente.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
        cliente.CurrencyIsoCode                       = 'GTQ';
        cliente.BI_Segment__c                         = 'test';
        cliente.BI_Subsegment_Regional__c             = 'test';
        cliente.BI_Territory__c                       = 'test';
    insert cliente;
    
     BI_Col_Ciudades__c ciu=new BI_Col_Ciudades__c();
    ciu.BI_COL_Codigo_DANE__c='11001000';
    ciu.Name='BOGOTA DC / CUNDINAMARCA';
    insert ciu;

    direccion                                            = new BI_Sede__c();
        
          //direccion.BI_COL_Ciudad_Departamento__c = ciudad.Id;
          direccion.BI_Direccion__c = 'Calle 36 d DUR nr 75 25';
          direccion.BI_Localidad__c = 'Test Local';
          direccion.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
          direccion.BI_COL_Sucursal_en_uso__c = 'Libre';
          direccion.BI_Country__c = 'Colombia';
          direccion.Name = 'Calle 36 d DUR nr 75 25';
          direccion.BI_Codigo_postal__c = '12356';
        direccion.BI_COL_Direccion_Split__c ='AV|6 | | | |CL |23 | | |N';
        direccion.BI_COL_Direccion_Text__c='2237';
        direccion.BI_COL_Ciudad_Departamento__c=ciu.id;
        
    insert direccion;
    
    PageReference pageRef0 = Page.BI_COL_ConsultCoverage_pag;
      Test.setCurrentPage(pageRef0);
        ApexPages.currentPage().getParameters().put('idSucursal', direccion.id);
    
    cc.ejecutar();
    
    cc=new BI_COL_ConsultCoverage_ctr();
    
    cc.cobertura='Cobertura Conectividad Básica';
    direccion.BI_COL_Direccion_Split__c='AV |6 |';
    update direccion;
    
    
      Test.setCurrentPage(pageRef0);
        ApexPages.currentPage().getParameters().put('idSucursal', direccion.id);
    
    cc.ejecutar();
   
    }

    static testMethod void test3() 
    {
    
        BI_COL_ConsultCoverage_ctr cc=new BI_COL_ConsultCoverage_ctr();
        cc.cobertura='Cobertura Conectividad Básica';
        cc.getItemsCobertura();
        cc.ejecutar();
    }

     static testMethod void test4() 
    {
    
        BI_COL_ConsultCoverage_ctr cc=new BI_COL_ConsultCoverage_ctr();
        cc.cobertura='';
        cc.getItemsCobertura();
        cc.ejecutar();
    }

	   

}