/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_ChangeMassiveBranch_ctr
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*			 1.2	2015-09-03		Cristian Mejia (CM)		Se crea la lista lstModifServicio y objDescServ para terminar de cubrir el metodo actualizaObjetos
*			 1.3	2016-02-24		Javier Lopez (JavLo)	Se añade condicion para que pase la regla de valiacion COLCONTRA09
*		     1.4    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country , Adding Contact Values
					13/03/2017		Marta Gonzalez(Everis)	REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*					20/09/2017      Angel F. Santaliestra	Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
		
*************************************************************************************************************/
@isTest
private class BI_COL_ChangeMassiveBranch_tst 
{
	public static BI_COL_ChangeMassiveBranch_ctr objCMassBranch;
	public static BI_COL_Modificacion_de_Servicio__c objMS;
	public static Account objCuenta;
	public static Account objCuenta2;
	public static Contact objContacto;
	public static Opportunity objOportunidad;
	public static BI_COL_Anexos__c objAnexos;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	public static Contract objContrato;
	public static Campaign objCampana;
	public static BI_Col_Ciudades__c objCiudad;
	public static BI_Sede__c objSede;
	public static User objUsuario;
	public static BI_Punto_de_instalacion__c objPuntosInsta;
	public static BI_Log__c objBiLog;
	
	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list <Campaign> lstCampana;
	public static list<BI_COL_manage_cons__c> lstManege;
	public static List<RecordType> lstRecTyp;
	public static List<User> lstUsuarios;
	public static List<BI_COL_Modificacion_de_Servicio__c> lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c> lstDS;
	public static list<BI_Log__c> lstLog = new list <BI_Log__c>();

	public static void crearData()
	{
		////perfiles
		//lstPerfil             			= new list <Profile>();
		//lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		//system.debug('datos Perfil '+lstPerfil);
				
		////usuarios
		////lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

		////ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstPerfil.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;
        
  //      lstUsuarios = new list<User>();
  //      lstUsuarios.add(objUsuario);

		//System.runAs(lstUsuarios[0])
		//{		
			////Roles
			//lstRoles                = new list <UserRole>();
			//lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
			//system.debug('datos Rol '+lstRoles);
			
			//Cuentas
			objCuenta 										= new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

			objCuenta2                                       = new Account();
			objCuenta2.Name                                  = 'prueba';
			objCuenta2.BI_Country__c                         = 'Colombia';
			objCuenta2.TGS_Region__c                         = 'América';
			objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta2.CurrencyIsoCode                       = 'COP';
			objCuenta2.BI_Fraude__c                          = true;
			objCuenta2.BI_Segment__c                         = 'test';
			objCuenta2.BI_Subsegment_Regional__c             = 'test';
			objCuenta2.BI_Territory__c                       = 'test';
			insert objCuenta2;
			

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
			 
            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
        	objContacto.FirstName 							= 'firstname';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '57123456';
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
        	//REING-INI
        	objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
       		objContacto.BI_Tipo_de_contacto__c          = 'General';
        	//objContacto.BI_Tipo_de_contacto__c              = 'Autorizado';
        	//REING_FIN
            objContacto.BI_Tipo_de_documento__c             = 'RUT';    
        	objContacto.BI_Numero_de_documento__c           = '123456789';
            Insert objContacto;
			
			//catalogo
			NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
			objCatalogo.Name 								= 'prueba Catalogo';
			objCatalogo.BI_country__c             			= 'Colombia';
			insert objCatalogo; 
			
			//Campaña
			objCampana 										= new Campaign();
			objCampana.Name 								= 'Campaña Prueba';
			objCampana.BI_Catalogo__c 						= objCatalogo.Id;
			objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
			objCampana.BI_Country__c                        = 'Colombia';
      		objCampana.BI_Opportunity_Type__c               = 'Fijo';
      		objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
			insert objCampana;
			lstCampana 										= new List<Campaign>();
			lstCampana 										= [select Id, Name from Campaign where Id =: objCampana.Id];
			system.debug('datos Cuenta '+lstCampana);
			
			//Tipo de registro
			lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato 									= new Contract ();
			objContrato.AccountId 							= objCuenta.Id;
			objContrato.StartDate 							=  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
			//JavLo CoE Added to pass New ValidationRule
			objContrato.BI_COL_Tipo_contrato__c 			= 'Contrato Telefónica';
			objContrato.ContractTerm 						= 12;
			objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
			objContrato.BI_Indefinido__c					= 'Si';
			objContrato.BI_COL_Cuantia_Indeterminada__c 	= true;
			objContrato.BI_COL_Duracion_Indefinida__c       = true;
			objContrato.StartDate							= System.today().addDays(+5);

			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);
			
			//FUN
			objAnexos               						= new BI_COL_Anexos__c();
			objAnexos.Name          						= 'FUN-0041414';
			objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			//Configuracion Personalizada
			BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
			objConPer.Name                  				= 'Viabilidades';
			objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
			objConPer.BI_COL_ConsProyecto__c     			= 1;
			objConPer.BI_COL_ValorConsecutivo__c  			=1;
			lstManege                      					= new list<BI_COL_manage_cons__c >();        
			lstManege.add(objConPer);
			insert lstManege;
			System.debug('======= configuracion personalizada ======= '+lstManege);
			
			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objCuenta.Id;
			objOportunidad.BI_Country__c                          	= 'Colombia';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			//objOportunidad.OwnerId                                	= lstUsuarios[0].id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			list<Opportunity> lstOportunidad 	= new list<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Direccion
			objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '12356';


			insert objSede;
			System.debug('Datos Sedes ===> '+objSede);
			
			//Sede
			objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name                = 'Prueba Campaign';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               			= 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstDS);
			
			//MS
			objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= 'Activa';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			lstMS = [select Name, BI_COL_Codigo_unico_servicio__c,BI_COL_Producto__r.Name,BI_COL_Descripcion_Referencia__c,
							BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c, 
							BI_COL_Sucursal_Origen__c, /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c from BI_COL_Modificacion_de_Servicio__c
					]; //10/11/2016 GSPM: Se comentó el campo antiguo y se adiciono nuevo campo davox1
			System.debug('datos lista MS ===> '+lstMS);

			objBiLog											= new BI_Log__c();
			objBiLog.BI_COL_Informacion_recibida__c						= 'OK, Respuesta Procesada';
			//objBiLog.BI_COL_Contacto__c 									= lstContacts[0].Id;
			objBiLog.BI_COL_Estado__c										= 'FALLIDO';
			objBiLog.BI_COL_Interfaz__c									= 'NOTIFICACIONES';
			lstLog.add(objBiLog);	
			insert lstLog;	

			
		//}
	}

	static testMethod void test_method_1() 
	{
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objPuntosInsta);
			PageReference                 pageRef           = Page.BI_COL_ChangeMassiveBranch_pag;
			Test.setCurrentPage(pageRef);

			Test.startTest();
				pageRef.getParameters().put('Id',objPuntosInsta.Id);

				objCMassBranch = new BI_COL_ChangeMassiveBranch_ctr (controler);

				//objCMassBranch.sucursal 	= objPuntosInsta;
				objCMassBranch.ms 			= objMS;
				objCMassBranch.lstMS		= lstMS;
				List<BI_COL_Modificacion_de_Servicio__c> lstMoS = objCMassBranch.lstMS;
				objCMassBranch.lstDS		= lstDS;
				List<BI_COL_Descripcion_de_servicio__c> lstDeS	= objCMassBranch.lstDS;

				BI_Punto_de_instalacion__c sucursal = [select Id,Name, BI_Cliente__c,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c from BI_Punto_de_instalacion__c where Id=:objPuntosInsta.Id limit 1];
				System.debug('Data sucursales 1 =======> '+sucursal);
				List<BI_Punto_de_instalacion__c> lstSucCliente=[select Id,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c from BI_Punto_de_instalacion__c where BI_Cliente__c=:sucursal.BI_Cliente__c and Id !=: sucursal.Id];
				System.debug('Data sucursales 2 =======> '+ lstSucCliente);
				
				objCMassBranch.consultaObj ();
				objCMassBranch.actualizaObjetos();
				objCMassBranch.enviarInfoCambio();
				objCMassBranch.creaLogTransaccion(objPuntosInsta,lstMS, lstDS);

				
				/*map<Id,objPuntosInsta> mapPuntos = objCMassBranch.idSucursalesCliente;
				objCMassBranch.consultaObj ();*/
			Test.stopTest();
		}
	}

	static testMethod void test_method_2() 
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			// TO DO: implement unit test
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objPuntosInsta);
			PageReference                 pageRef           = Page.BI_COL_ChangeMassiveBranch_pag;
			Test.setCurrentPage(pageRef);
			
			objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor4EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '12356';
			objSede.BI_COL_Estado_callejero__c		= 'Validado por callejero';
			insert objSede;

			BI_Sede__c objSede2 								= new BI_Sede__c();
			objSede2.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede2.BI_Direccion__c 				= 'Calle 36D sur Nro 75A calle 68';
			objSede2.BI_Localidad__c 				= 'Test Local';
			objSede2.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor4EstadoCallejero;
			objSede2.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede2.BI_Country__c 					= 'Colombia';
			objSede2.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede2.BI_Codigo_postal__c 			= '12356';
			objSede2.BI_COL_Estado_callejero__c		= 'Validado por callejero';
			insert objSede2;

			System.debug('Datos Sedes ===> '+objSede);

			BI_Punto_de_instalacion__c sucursal1 = new BI_Punto_de_instalacion__c();
			sucursal1.BI_Cliente__c       = objCuenta.Id;
			sucursal1.BI_Sede__c          = objSede.Id;
			sucursal1.BI_Contacto__c      = objContacto.id;
			sucursal1.BI_Cliente__c = objCuenta.Id;
			insert sucursal1;

			BI_Punto_de_instalacion__c sucursal2 = new BI_Punto_de_instalacion__c();
			sucursal2.BI_Cliente__c       = objCuenta.Id;
			sucursal2.BI_Sede__c          = objSede2.Id;
			sucursal2.BI_Contacto__c      = objContacto.id;
			sucursal2.BI_Cliente__c = objCuenta.Id;
			insert sucursal2;
			Test.startTest();
				pageRef.getParameters().put('Id',objPuntosInsta.Id);

				objCMassBranch = new BI_COL_ChangeMassiveBranch_ctr (controler);

				//objCMassBranch.sucursal 	= objPuntosInsta;
				objCMassBranch.ms 			= objMS;
				objCMassBranch.lstMS		= lstMS;
				List<BI_COL_Modificacion_de_Servicio__c> lstMoS = objCMassBranch.lstMS;
				objCMassBranch.lstDS		= lstDS;
				List<BI_COL_Descripcion_de_servicio__c> lstDeS	= objCMassBranch.lstDS;

				BI_Punto_de_instalacion__c sucursal = [select Id,Name, BI_Cliente__c,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c from BI_Punto_de_instalacion__c where Id=:objPuntosInsta.Id limit 1];
				System.debug('Data sucursales 1 =======> '+sucursal);
				List<BI_Punto_de_instalacion__c> lstSucCliente=[select Id,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c from BI_Punto_de_instalacion__c where BI_Cliente__c=:sucursal.BI_Cliente__c and Id !=: sucursal.Id];
				System.debug('Data sucursales 2 =======> '+ lstSucCliente);
				
				objCMassBranch.consultaObj ();
				pageRef.getParameters().put('Id',sucursal1.Id);
				objCMassBranch.actualizaObjetos();
				objCMassBranch.enviarInfoCambio();
				objCMassBranch.creaLogTransaccion(objPuntosInsta,lstMS, lstDS);
				ApexPages.Standardcontroller controller;
	     		ApexPages.currentPage().getParameters().put('Id',sucursal1.Id);         
	    		BI_COL_ChangeMassiveBranch_ctr ctr=new BI_COL_ChangeMassiveBranch_ctr(controller);
	    		//List<BI_COL_Modificacion_de_Servicio__c> lstMS=ctr.lstMS;
	     		//List<BI_COL_Descripcion_de_servicio__c> lstDS=ctr.lstDS;

				ctr.ms.BI_COL_Sucursal_de_Facturacion__c=sucursal2.Id;
	     		ctr.actualizaObjetos();

	     		ctr.sucursal = sucursal;
	     		BI_COL_Modificacion_de_Servicio__c  objModifServicio = new BI_COL_Modificacion_de_Servicio__c ();
	     		objModifServicio.BI_COL_Sucursal_de_Facturacion__c = sucursal.id;
	     		objModifServicio.BI_COL_Sucursal_Origen__c = sucursal.id;

	     		List<BI_COL_Modificacion_de_Servicio__c> lstModifServicio = new List<BI_COL_Modificacion_de_Servicio__c>();
	     		lstModifServicio.add(objModifServicio);
	     		ctr.lstMS = lstModifServicio;

	     		BI_COL_Descripcion_de_servicio__c objDescServ 		= new BI_COL_Descripcion_de_servicio__c();
	     		objDescServ.BI_COL_Oportunidad__c                     = objOportunidad.Id;
				objDescServ.CurrencyIsoCode               			= 'COP';
	     		objDescServ.BI_COL_Sede_Destino__c = sucursal.Id;
	     		objDescServ.BI_COL_Sede_Origen__c = sucursal.Id;
	     		insert objDescServ;

	     		ctr.actualizaObjetos();	
				/*map<Id,objPuntosInsta> mapPuntos = objCMassBranch.idSucursalesCliente;
				objCMassBranch.consultaObj ();*/
			Test.stopTest();
		}

	}
    
}