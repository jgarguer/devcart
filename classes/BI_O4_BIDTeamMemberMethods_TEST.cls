/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test for BI_O4_BIDTeamMemberMethods class
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    20/09/2017              Antonio Mendivil        Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    25/09/2017		  		Jaime Regidor		   	Fields BI_Subsector__c and BI_Sector__c added  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_BIDTeamMemberMethods_TEST {

	static{
	    BI_TestUtils.throw_exception = false;
	}
	
	@isTest static void addBIDTeamMemberUserToOpp() {
		
		RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional'];
		RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_O4_BID_Team' limit 1];
		RecordType rt2 = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_O4_Other_BID_Team' limit 1];
		RecordType rtAcc = [SELECT Id FROM RecordType WHERE DeveloperName != 'TGS_Business_Unit' AND SObjectType = 'Account' limit 1];
		User user = [SELECT Id, IsActive FROM User WHERE IsActive = true LIMIT 1];

		List<Account> lst_acc = new List<Account>();       
				Account acc = new Account(Name = 'test',
											BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
											BI_Activo__c = Label.BI_Si,
											BI_Segment__c = 'Empresas',
											BI_Subsector__c = 'test', //25/09/2017
											BI_Sector__c = 'test', //25/09/2017
											BI_Subsegment_local__c = 'Top 1',
											RecordTypeId = rtAcc.Id,
											BI_Subsegment_Regional__c = 'test',
       										BI_Territory__c = 'test'
											);
				lst_acc.add(acc);
		User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
        System.runAs(usu){
				insert lst_acc;
		}

		Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
													CloseDate = Date.today(),
													StageName = Label.BI_F6Preoportunidad,
													AccountId = lst_acc[0].Id,
													BI_Ciclo_ventas__c = Label.BI_Completo, 
													RecordTypeId = rt1.Id
													);
						
		insert opp;

		OpportunityTeamMember otm = new OpportunityTeamMember(UserId = usu.Id, OpportunityId = opp.Id);
		insert otm;

		List<BI_O4_BID_team_member__c> lstBIDTm = new List<BI_O4_BID_team_member__c>();
		BI_O4_BID_team_member__c member = new BI_O4_BID_team_member__c();
		member.RecordTypeId = rt.Id;
		member.BI_O4_Name2__c = user.Id;
		member.BI_O4_Opportunity__c = opp.Id;
		member.BI_O4_Bid_team_role2__c = 'Big Deals – Richard Hill';

		lstBIDTm.add(member);

		/*BI_O4_BID_team_member__c member2 = new BI_O4_BID_team_member__c();
		member2.RecordTypeId = rt.Id;
		member2.BI_O4_Opportunity__c = opp.Id;
		member2.BI_O4_Bid_team_role2__c = 'Big Deals – Richard Hill';
		lstBIDTm.add(member2);*/
		insert lstBIDTm;
	}
}