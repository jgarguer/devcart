global class BI_User_JOB implements Schedulable {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Scheduled class that updates User fields.
    Test Class:    BI_User_JOB_TEST

    History: 
    
     <Date>                     <Author>                <Change Description>
    20/11/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global Id Id_LastUser;

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that executes the job, and checks diferences between BI_LastPasswordChange__c and LastPasswordChangeDate

    History: 
    
     <Date>                     <Author>                <Change Description>
    20/11/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sch) {
        try{
            system.abortJob(sch.getTriggerId());
    		
            List<User> lst_usrToUpd = new List <User>();
			List <User> lst_usr = new List <User>();
			Integer limite =(Test.isRunningTest())?1:9000;

            

            if (Id_LastUser==null){
                lst_usr = [SELECT Id, BI_LastPasswordChange__c, BI_ContadorReset__c, LastPasswordChangeDate FROM User WHERE IsActive = true and Pais__c <> null and BI_Permisos__c <> null ORDER BY Id ASC limit :limite ];
            }else{
            	lst_usr = [SELECT Id, BI_LastPasswordChange__c, BI_ContadorReset__c, LastPasswordChangeDate FROM User WHERE Id > :Id_LastUser AND IsActive = true and Pais__c <> null and BI_Permisos__c <> null ORDER BY Id ASC limit :limite ];
            }

            System.debug('USUARIOS QUERY [lst_usr]: '+ lst_usr);

            if(!lst_usr.isEmpty()){
            	//Filter users
        		for (User usr :lst_usr){	
                    System.debug('usr.BI_LastPasswordChange__c: '+ lst_usr);
                    System.debug('usr.LastPasswordChangeDate: '+ lst_usr);
                	if(usr.BI_LastPasswordChange__c != usr.LastPasswordChangeDate){
                		lst_usrToUpd.add(usr);
                	}
                	Id_LastUser = usr.Id;
        		}
        		System.debug('Id_LastUser: '+ Id_LastUser);
                System.debug('lst_usrToUpd: '+ lst_usrToUpd);

        		//Update Filtered users
				if(!lst_usrToUpd.isEmpty()){
					for(User usr :lst_usrToUpd){
						usr.BI_LastPasswordChange__c = usr.LastPasswordChangeDate;
                        system.debug('BI_ContadorReset__c: ' + usr.BI_ContadorReset__c);
						if(usr.BI_ContadorReset__c == null){
							usr.BI_ContadorReset__c = 1; //Initialize and update at the same time
                            system.debug('BI_ContadorReset__c -> INICIALICE: ' + usr.BI_ContadorReset__c);
						}else{
                            system.debug('BI_ContadorReset__c -> INCREMENT: ' + usr.BI_ContadorReset__c);
							usr.BI_ContadorReset__c = usr.BI_ContadorReset__c + 1;
						}
					}
					update lst_usrToUpd;
				}

				//Rerun Job
                System.debug('Limite: ' + limite + 'List size: ' + lst_usr.size() );
                system.debug('RECALL: Id_LastUser = ' + Id_LastUser);
                if(lst_usr.size() == limite){
                    system.debug('BI_Jobs.updateUsers(ID, false);');
                    BI_Jobs.updateUsers(Id_LastUser, false);
                }else{
                    system.debug('BI_Jobs.updateUsers(ID, true);');
                    BI_Jobs.updateUsers(Id_LastUser, true);
                }
                
            }else{
                system.debug('BI_Jobs.updateUsers(null, true);');
                BI_Jobs.updateUsers(null, true);
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_User_JOB.Schedulable', 'BI_EN', Exc, 'Schedulable Job');
        }
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Constructor method
    
    IN:            User Id
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    20/11/2014        Micah Burgos       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_User_JOB(Id idjob){
        Id_LastUser = idjob;
        system.debug('CONSTRUCTOR: Id_LastUser = ' + Id_LastUser);
    }
}