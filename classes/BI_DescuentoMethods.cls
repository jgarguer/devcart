public with sharing class BI_DescuentoMethods {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Account Triggers 
    Test Class:    BI_AccountMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockEdition(List <BI_Descuento__c> news, List <BI_Descuento__c> olds){
        try{
            if(BI_GlobalVariables.stopTriggerDesc == false){
                Integer i = 0;
                for(BI_Descuento__c disc:olds){
                    if (disc.BI_Estado_del_Proceso__c == label.BI_BolsaDineroCancelado){
                        news[i].addError(label.BI_DescuentoBloqueadoEstado);
                    }
                    i++;
                }
            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoMethods.validateId', 'BI_EN', exc, 'Trigger');
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateAcc(List <BI_Descuento__c> news, List <BI_Descuento__c> olds){
        try{
            Map <Id, Id> map_disc_acc = new Map <Id, Id>();
            Map <Id, String> map_acc = new Map <Id, String>();
            Set <Id> set_acc = new Set <Id>();
            Integer i = 0;
            for(BI_Descuento__c disc:news){
                if (disc.BI_Cliente__c != olds[i].BI_Cliente__c){
                     map_disc_acc.put(disc.Id, disc.BI_Cliente__c);
                     set_acc.add(disc.BI_Cliente__c);
                }
                i++;               
            }
            for(Account acc:[SELECT Id, Name FROM Account WHERE Id IN :set_acc]){
                map_acc.put(acc.Id, acc.Name);
            }

            List <BI_Descuento_para_modelo__c> lst_dm = [SELECT Id, BI_Descuento__c, BI_Cliente_aux__c FROM BI_Descuento_para_modelo__c WHERE BI_Descuento__c IN :map_disc_acc.keyset()];
            for(BI_Descuento_para_modelo__c discm:lst_dm){
                discm.BI_Cliente_aux__c = map_acc.get(map_disc_acc.get(discm.BI_Descuento__c));
            }
            update lst_dm;
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoMethods.updateAcc', 'BI_EN', exc, 'Trigger');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    08/04/2015              Pablo Oliva             Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateDescMod(List <BI_Descuento__c> news, List <BI_Descuento__c> olds){
        try{
            
            Set<Id> set_descmod = new Set<Id>();
            Integer i = 0;
            for(BI_Descuento__c disc:news){
                if(disc.BI_Estado_del_Proceso__c == 'Aprobado' && olds[i].BI_Estado_del_Proceso__c != disc.BI_Estado_del_Proceso__c)
                    set_descmod.add(disc.Id);
                
                i++;
            }
            
            if(!set_descmod.isEmpty()){
                List<BI_Descuento_para_modelo__c> lst_descmod = new List<BI_Descuento_para_modelo__c>();
                for(BI_Descuento__c disc:[select Id, (select Id, BI_Cantidad__c from BI_Descuento_para_modelo__r) from BI_Descuento__c where Id IN :set_descmod]){
                    if(!disc.BI_Descuento_para_modelo__r.isEmpty()){
                        for(BI_Descuento_para_modelo__c descmod:disc.BI_Descuento_para_modelo__r){
                            descmod.BI_Estado__c = 'Activo';
                            descmod.BI_Cantidad_disponible__c = descmod.BI_Cantidad__c;
                            lst_descmod.add(descmod);
                        }
                    }
                }
                BI_GlobalVariables.stopTriggerDesc = true;
                
                update lst_descmod;
            }
            
        }catch(exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoMethods.updateDescMod', 'BI_EN', exc, 'Trigger');
        }
    }
   
}