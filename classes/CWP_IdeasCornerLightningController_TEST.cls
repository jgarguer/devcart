/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to comprobe failure coverage 
The Apex Language Reference for more information about Testing and Code Coverage.
Test Class:    CWP_IdeasCornerLightningController.cls

History:

<Date>                  <Author>                <Change Description>
3/05/2017              Everis          		Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_IdeasCornerLightningController_TEST {
     @testSetup 
    private static void dataModelSetup() {  
        
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(new User(id=userInfo.getUserId())){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
         //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        
        
        
          //ORDER
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        
        //CASE
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
		insert newCase;       
        
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        /*System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            insert usuarioNoTGS;
        }*/
         

        
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
        
        BI_Catalogo_PP__c ProductoJSON = new BI_Catalogo_PP__c();
        ProductoJSON.BI_Familia_del_producto__c=newMarketplace.Id;
        ProductoJSON.Name = 'JSONproduct';
        insert ProductoJSON;
        
         BI_Catalogo_PP__c ProductoJSON1 = new BI_Catalogo_PP__c();
        ProductoJSON1.BI_Familia_del_producto__c=newMarketplace.Id;
        ProductoJSON1.Name = 'JSONproduct1';
        insert ProductoJSON1;

       
        
        Idea ideaPrueba = new Idea();
        ideaPrueba.Title ='prueba';
        ideaPrueba.CommunityId = [SELECT Id FROM Community WHERE Name =: 'Portal Platinum comunidad Ideas' LIMIT 1].id;
        insert ideaPrueba;
        
        
        IdeaComment comentario = new IdeaComment();
        comentario.CommentBody ='CommentBody';
        comentario.IdeaId = ideaPrueba.id;
        insert comentario;
        
        
        CollaborationGroup grupo = new CollaborationGroup(Name='Grupo Test', CollaborationType='Public'); 
        grupo.NetworkId =[Select Id from Network where Name= 'Empresas Platino'].id;
        insert grupo;
     
       
        FeedItem prueba = new FeedItem();
        prueba.Title ='prueba';
        prueba.Body='body';
        prueba.ParentId=accLegalEntity.id;
        prueba.Type='LinkPost';
        prueba.LinkUrl='www.youtube.com';
        insert prueba;
        
         
     
        
     }
    

    
       @isTest
    private static void getLastNewsTest() {     
        system.debug('test 1');
        BI_TestUtils.throw_exception=false;  
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
          CWP_IdeasCornerLightningController.RSSWrapper rss = new CWP_IdeasCornerLightningController.RSSWrapper();
        rss.title = 'title';
        rss.link='link';
        rss.description='description';
        rss.publicado='publicado';
        rss.strDate='strDate';
        
        
         CWP_IdeasCornerLightningController.RssShow rssS = new CWP_IdeasCornerLightningController.RssShow();
        rssS.dateParse = 'dateParse';
        
         CWP_IdeasCornerLightningController.channel channel1 = new CWP_IdeasCornerLightningController.channel();
        channel1.author = 'author';
        
        
        
        
        CWP_IdeasCornerLightningController.Videos_wrapper video = new CWP_IdeasCornerLightningController.Videos_wrapper ('name', 'description', 'image', 'url', 'src');
        
    
        
        CWP_IdeasCornerLightningController.item unItem = new CWP_IdeasCornerLightningController.item();
        unItem.pubDate = null;
        date hoy = unItem.getPublishedDate();
        dateTime tiempo = unItem.getPublishedDateTime();
       
        CWP_IdeasCornerLightningController.ideasWrapper  ideas = new CWP_IdeasCornerLightningController.ideasWrapper();
        ideas.Title='Title';
        ideas.VoteTotal='VoteTotal';
        ideas.createdDate='createdDate';
        ideas.parsedDate='parsedDate';
        ideas.id='id';
        ideas.Body='Body';
        ideas.Category='Category';
        ideas.Community='Community';
        ideas.IdeaInAction='IdeaInAction';
        ideas.CreatedBy='CreatedBy';
        
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_IdeasCornerLightningController.getLastNews();   
            Test.stopTest();
        }       
        
    } 
    
       @isTest
    private static void getVideosWrapperTest() {     
        system.debug('test 2');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
       
        System.runAs(usuario){                          
            Test.startTest();  
			BI_Configuracion__c conf = new BI_Configuracion__c(Name='URL_Videoteca',BI_Valor__c='http://B30547.cdn.telefonica.com/30547/'); 

			insert conf;


            CWP_IdeasCornerLightningController.getVideosWrapper();   
            Test.stopTest();
        }       
        
    } 
    
    
     @isTest
    private static void getVideosWrapperTest2() {     
        system.debug('test 2.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
       
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_IdeasCornerLightningController.getVideosWrapper();   
            Test.stopTest();
        }       
        
    } 
    
    
        @isTest
    private static void getChatterGroupsTest() {     
        system.debug('test 3');
         BI_TestUtils.throw_exception=false; 
        User userName = [SELECT id,ContactId FROM User WHERE FirstName =: 'nombre1']; 
        
        
        Contact contactTest = [SELECT id FROM Contact WHERE Name =: 'nombreDeTest'];
        CollaborationGroup grupo = [SELECT id, Name FROM CollaborationGroup WHERE Name =: 'Grupo Test'];
        Network netw = [Select Id from Network where Name= 'Empresas Platino'];

       
         CWP_IdeasCornerLightningController.RSSWrapper rss = new CWP_IdeasCornerLightningController.RSSWrapper();
        rss.title = 'title';
        rss.link='link';
        rss.description='description';
        rss.publicado='publicado';
        rss.strDate='strDate';
        
        FeedItem prueba = [SELECT Body, CreatedDate, Id, LikeCount, LinkUrl, NetworkScope, ParentId, Title, Type, Visibility, RelatedRecordId FROM FeedItem WHERE Title =: 'prueba'];
      
        
       
        System.runAs(userName){                          
            Test.startTest();
            CWP_IdeasCornerLightningController.getChatterGroups();   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void obtainPopularIdeasTest() {     
        system.debug('test 4');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_IdeasCornerLightningController.obtainPopularIdeas(true, 'searchText');   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void obtainPopularIdeasTest2() {     
        system.debug('test 4.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_IdeasCornerLightningController.obtainPopularIdeas(null, null);   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void obtainPopularIdeasTest3() {     
        system.debug('test 4.2');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_IdeasCornerLightningController.obtainPopularIdeas(false, 'searchText');   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void obtenerIdeasRecientesTest() {     
        system.debug('test 5');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();
            CWP_IdeasCornerLightningController.obtenerIdeasRecientes(true,'searchText');   
            Test.stopTest();
        }       
        
    } 
    
    
     @isTest
    private static void obtenerIdeasRecientesTest2() {     
        system.debug('test 5.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_IdeasCornerLightningController.obtenerIdeasRecientes(null,null);   
            Test.stopTest();
        }       
        
    } 
    
    
     @isTest
    private static void obtenerIdeasRecientesTest3() {     
        system.debug('test 5.2');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_IdeasCornerLightningController.obtenerIdeasRecientes(false,'searchText');   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void obtainMyIdeasTest() {     
        system.debug('test 6');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.obtainMyIdeas(true, 'searchText');   
            Test.stopTest();
        }       
        
    } 
    
     
     @isTest
    private static void obtainMyIdeasTest2() {     
        system.debug('test 6.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.obtainMyIdeas(null, null);   
            Test.stopTest();
        }       
        
    } 
    
     
     @isTest
    private static void obtainMyIdeasTest3() {     
        system.debug('test 6.2');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.obtainMyIdeas(false, 'searchText');   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getRecoverySolutionsTest() {     
        system.debug('test 7');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.getRecoverySolutions(true, true, 'searchText');   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getRecoverySolutionsTest2() {     
        system.debug('test 7.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.getRecoverySolutions(null, null, null);   
            Test.stopTest();
        }       
        
    } 
     @isTest
    private static void getAllCommentsTest() {     
        system.debug('test 8');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
        
        Idea idea3 = [SELECT AttachmentBody,AttachmentContentType,AttachmentLength,AttachmentName,Body,Categories,Community.Name,CommunityId,CreatedBy.Name,CreatedById,CreatedDate,CreatorFullPhotoUrl,CreatorName,CreatorSmallPhotoUrl,CurrencyIsoCode,Id,IsDeleted,IsHtml,IsMerged,LastCommentDate,LastCommentId,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,NumComments,ParentIdeaId,RecordTypeId,Status,SystemModstamp,Title,VoteScore,VoteTotal FROM Idea WHERE Title =: 'prueba']; 

        
        CWP_IdeasCornerLightningController.ideasWrapper  ideas = new CWP_IdeasCornerLightningController.ideasWrapper();
        ideas.Title='Title';
        ideas.VoteTotal='VoteTotal';
        ideas.createdDate='createdDate';
        ideas.parsedDate='parsedDate';
        ideas.id=idea3.id;
        ideas.Body='Body';
        ideas.Category='Category';
        ideas.Community='Community';
        ideas.IdeaInAction='IdeaInAction';
        ideas.CreatedBy='CreatedBy';
        
        list < CWP_IdeasCornerLightningController.ideasWrapper> lista3 = new list < CWP_IdeasCornerLightningController.ideasWrapper>();
        lista3.add(ideas);
		String test5 = JSON.serialize(lista3);
        
       
        
        System.runAs(usuario){                          
            Test.startTest();      
            CWP_IdeasCornerLightningController.getAllComments(test5);   
            Test.stopTest();
        }       
        
    } 
     @isTest
    private static void voteIdeaTest() {     
        system.debug('test 9');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();
            CWP_IdeasCornerLightningController.voteIdea('OK','selectedIdea');   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void voteIdeaTest2() {     
        system.debug('test 9.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
      
        
        CWP_IdeasCornerLightningController.ideasWrapper  ideas = new CWP_IdeasCornerLightningController.ideasWrapper();
        ideas.Title='Title';
        ideas.VoteTotal='VoteTotal';
        ideas.createdDate='createdDate';
        ideas.parsedDate='parsedDate';
        ideas.Body='Body';
        ideas.Category='Category';
        ideas.Community='Community';
        ideas.IdeaInAction='IdeaInAction';
        ideas.CreatedBy='CreatedBy';
        
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.voteIdea('OK',ideas.id);   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void voteIdeaTest3() {     
        system.debug('test 9.2');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
      
        
        CWP_IdeasCornerLightningController.ideasWrapper  ideas = new CWP_IdeasCornerLightningController.ideasWrapper();
        ideas.Title='Title';
        ideas.VoteTotal='VoteTotal';
        ideas.createdDate='createdDate';
        ideas.parsedDate='parsedDate';
        ideas.Body='Body';
        ideas.Category='Category';
        ideas.Community='Community';
        ideas.IdeaInAction='IdeaInAction';
        ideas.CreatedBy='CreatedBy';
        
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.voteIdea('KO',ideas.id);   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void createCommentTest() {     
        system.debug('test 10');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1']; 
     
		  Idea idea1 = [SELECT Id FROM Idea LIMIT 1];
			String ideaComent = idea1.Id;        
       
        System.runAs(usuario){                          
            Test.startTest();      
            CWP_IdeasCornerLightningController.createComment('commentBody',ideaComent);   
            Test.stopTest();
        }       
        
    } 
    
       @isTest
    private static void createCommentTest2() {     
        system.debug('test 10.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
          CWP_IdeasCornerLightningController.ideasWrapper  ideas = new CWP_IdeasCornerLightningController.ideasWrapper();
        ideas.Title='Title';
        ideas.VoteTotal='VoteTotal';
        ideas.createdDate='createdDate';
        ideas.parsedDate='parsedDate';
        ideas.Body='Body';
        ideas.Category='Category';
        ideas.Community='Community';
        ideas.IdeaInAction='IdeaInAction';
        ideas.CreatedBy='CreatedBy';
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.createComment('commentBody',ideas.id);   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void createCommentTest3() {     
        system.debug('test 10.2');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
          CWP_IdeasCornerLightningController.ideasWrapper  ideas = new CWP_IdeasCornerLightningController.ideasWrapper();
        ideas.Title='Title';
        ideas.VoteTotal='VoteTotal';
        ideas.createdDate='createdDate';
        ideas.parsedDate='parsedDate';
        ideas.Body='Body';
        ideas.Category='Category';
        ideas.Community='Community';
        ideas.IdeaInAction='IdeaInAction';
        ideas.CreatedBy='CreatedBy';
       
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_IdeasCornerLightningController.createComment(null,ideas.id);   
            Test.stopTest();
        }       
        
    } 

    
     @isTest
    private static void insertIdeaTest() {     
        system.debug('test 11');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();        
            CWP_IdeasCornerLightningController.insertIdea('title','body');   
            Test.stopTest();
        }       
        
    } 
    
      @isTest
    private static void insertIdeaTest2() {     
        system.debug('test 11.1');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();      
            CWP_IdeasCornerLightningController.insertIdea(null,'body');   
            Test.stopTest();
        }       
        
    } 
    
   
          @isTest
    private static void buildIdeasWrapperTest() {     
        system.debug('test 12');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
        
        
		Idea idea1 = [SELECT AttachmentBody,AttachmentContentType,AttachmentLength,AttachmentName,Body,Categories,Community.Name,CommunityId,CreatedBy.Name,CreatedById,CreatedDate,CreatorFullPhotoUrl,CreatorName,CreatorSmallPhotoUrl,CurrencyIsoCode,Id,IsDeleted,IsHtml,IsMerged,LastCommentDate,LastCommentId,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,NumComments,ParentIdeaId,RecordTypeId,Status,SystemModstamp,Title,VoteScore,VoteTotal FROM Idea WHERE Title =: 'prueba']; 
      
        
        List<Idea> toRet = new List<Idea>();
        toRet.add(idea1);
       
       
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_IdeasCornerLightningController.buildIdeasWrapper(toRet);   
            Test.stopTest();
        }       
        
    } 
    
  
    
     @isTest
    private static void getRSSDataTest() {     
        system.debug('test 13');
         BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();  
            CWP_IdeasCornerLightningController.getRSSData(null);   
            Test.stopTest();
        }       
        
    } 
    
   
}