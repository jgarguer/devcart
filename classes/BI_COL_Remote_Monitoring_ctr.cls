/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       24-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_Remote_Monitoring_ctr 
{
    public static void crearNovedad( Id idMs )
	{
		List<BI_COL_Modificacion_de_Servicio__c> lstMs = null;
		string strInterfaz = 'DAVOXPLUS'; 
		String strTransaccion = 'ALTA EQUIPO'; 
		String strNombreObjeto = 'BI_COL_Modificacion_de_Servicio__c'; 
		String strDireccion = 'SALIENTE'; 
		String strNovedad = 'ALTA EQUIPO';
		boolean blnConfigCargada = false, blnProcesada = false, blnRegistrada = false;
		BI_COL_Configuracion_campos_interfases__c configuracionInterfaz = new BI_COL_Configuracion_campos_interfases__c();
		list<BI_COL_Configuracion_campos_interfases__c> Interfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>();
		list<BI_COL_Configuracion_campos_interfases__c> camposConfigInterfaz_list = new list<BI_COL_Configuracion_campos_interfases__c>();
		list<id> Registros_list = new list<id>();
		set<id> idsSet = new set<id>();
		map<id, BI_COL_Modificacion_de_Servicio__c> DS_map = new map<id, BI_COL_Modificacion_de_Servicio__c>(); 
		map<id, string> cadenas_map = new map<id, string>();
		map<id, string> errores_map = new map<id, string>();
		map<id, string> novedades_map = new map<id, string>();
		map<id,id> registrosAsociados = new map<id,id>();
		integer TotalRegistros = 0;
		
		lstMs = [ select Id from BI_COL_Modificacion_de_Servicio__c where Id =: idMs ];
		
		for( integer i = 0; i < lstMs.size() ; i++ )
		{
			Registros_list.add( lstMs[i].id );
			novedades_map.put( lstMs[i].id, strNovedad );
			registrosAsociados.put( lstMs[i].id, lstMs[i].id ); 
		}
		
		if( Registros_list.size() > 0 )
		{
			blnConfigCargada = BI_COL_Basic_pck_cls.cargarConfiguracion( Interfaz_list, camposConfigInterfaz_list, 
				strInterfaz, 'SERV. CORPORATIVO - NOVEDADES', 'SALIENTE');
			configuracionInterfaz = Interfaz_list.get(0);
			
			if( blnConfigCargada )
			{
				blnProcesada = BI_COL_Basic_pck_cls.procesarRegistros( configuracionInterfaz, camposConfigInterfaz_list, 
					Registros_list, cadenas_map, errores_map, novedades_map,strNovedad);
				blnRegistrada = BI_COL_Basic_pck_cls.registrarLog( cadenas_map, errores_map, strInterfaz, 
					strNombreObjeto, strTransaccion, strDireccion, novedades_map, registrosAsociados);
				List<BI_Log__c> lstNovedadFact = BI_COL_Basic_pck_cls.lstNf;
				enviarInfo( lstMs[0], lstNovedadFact );
			}
			else
				system.debug('ERROR: No se encontró la configuración para la Interfaz con DavoxPlus. Transacción: '+strTransaccion); 
		}       
	}
	
	public static void enviarInfo( BI_COL_Modificacion_de_Servicio__c ms,List<BI_Log__c> novedadFact)
	{
		BI_COL_Modificacion_de_Servicio__c[] lstMs = new BI_COL_Modificacion_de_Servicio__c[]{ms};
		String trama = armarXml( 'SERV. CORPORATIVO - NOVEDADES', novedadFact );
		if( !test.isRunningTest() )
			GenerarTramaEnviarDavox( novedadFact[0].Id, trama, ms.Id );
	}
	
	@Future (callout=true)
	public static void GenerarTramaEnviarDavox( Id idNovedad, String trama, Id idMs )
	{
		BI_Log__c novedadFac = [
			select 	Id, BI_COL_Causal_Error__c, BI_COL_Estado__c, BI_COL_Informacion_recibida__c 
			from 	BI_Log__c 
			where 	Id =: idNovedad 
			limit 1];
		BI_COL_Modificacion_de_Servicio__c ms = [ select Id from BI_COL_Modificacion_de_Servicio__c where Id =: idMs limit 1];
		String strRespuestaServicio = '';
		List<String> lstIdNF = new List<String>();
		lstIdNF.add( novedadFac.Id );
		try
		{
			BI_COL_Davox_wsdl.BasicServiceWSSOAP ws = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
			ws.timeout_x = 12000;
			strRespuestaServicio = ws.add( trama );
			BI_COL_GenerateNoveltyBilling_ctr.ActualizaNovedades( lstIdNF, strRespuestaServicio );
		}
		catch( Exception ex )
		{
			novedadFac.BI_COL_Causal_Error__c = 'Error enviando la Novedad a DAVOX: ' + ex.getMessage();
			novedadFac.BI_COL_Estado__c = 'Error Conexion';
			novedadFac.BI_COL_Informacion_recibida__c = ex.getMessage();         
			update novedadFac;
			actualizarMsFallo( ms );
			system.Debug('Error del XML: ' + ex.getMessage());
		}
	}
	
	public static void actualizarMsFallo( BI_COL_Modificacion_de_Servicio__c ms )
	{
		ms.BI_COL_Fecha_de_facturacion__c = null;
		ms.BI_COL_Autorizacion_facturacion__c = false;       
		update ms;
	}

	public static String armarXml( String typeX, List<BI_Log__c> novedades)
	{
		if( test.isRunningTest() && novedades == null )
			novedades = new List<BI_Log__c>();
		
		XmlStreamWriter xmlWriter = new XmlStreamWriter();
		xmlWriter.writeStartDocument( 'utf-8', '1.0' );
		xmlWriter.writeStartElement( null, 'WS', null );

		xmlWriter.writeStartElement( null, 'USER', null );
		xmlWriter.writeCharacters( 'USER' );
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement( null, 'PASSWORD', null );
		xmlWriter.writeCharacters('PASSWORD');
		xmlWriter.writeEndElement();
		xmlWriter.writeStartElement( null, 'DATEPROCESS', null );
		xmlWriter.writeCharacters( String.valueOf( System.Today() ) );
		xmlWriter.writeEndElement();
		xmlWriter.writeStartElement( null, 'PACKCODE', null );
		xmlWriter.writeCharacters( 'ASD654AS' + System.now() );
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement( null, 'SEPARATOR', null );
		xmlWriter.writeCharacters( 'Õ' );
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'TYPE', null);
		xmlWriter.writeCharacters(typeX);
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'VALUES', null);

		for( BI_Log__c n : novedades )
		{
			xmlWriter.writeStartElement(null, 'VALUE', null);
			xmlWriter.writeCharacters( n.BI_COL_Informacion_Enviada__c );
			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();

		xmlWriter.writeStartElement(null, 'COUNT', null);
		xmlWriter.writeCharacters( String.valueOf( novedades.size() ) );
		xmlWriter.writeEndElement();

		xmlWriter.writeEndElement();

		string tramaDavox = xmlWriter.getXmlString();
		tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
		xmlWriter = new XmlStreamWriter();

		return tramaDavox;
	}
}