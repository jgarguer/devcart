/***********************************************************************************************
* Avanxo Colombia
 * @author              Carlos A. Rodriguez Bejarano.
 * @project             Telefonica
 * @description         Test class of BI_COL_ContractExcel_ctr.
 * 
 *Changes (Version)
 *-----------------------------------------------------------------------------
 *          No.     Date            Autor                                   Description
 *          -----   ----------      --------------------                    -------------------
 *@version  1.0     30/06/2015      Carlos Rodriguez (CR)                   New test created.
 *@version  1.1     2015-24-07      Manuel Esthiben Mendez Devia (MEMD)     Modificated test class 
**************************************************************************************************/
@isTest
private class BI_COL_ContractExcel_tst  
{

    Public static list <UserRole>                               lstRoles;
    Public static User                                          objUsuario;
    public static Contact                                       objContacto;
    public static BI_Col_Ciudades__c                            objCiudad;
    public static BI_Sede__c                                    objSede;
    public static BI_Punto_de_instalacion__c                    objPuntosInsta;
    public static BI_COL_Anexos__c                              objAnexos;
    public static BI_COL_Modificacion_de_Servicio__c            objModSer;
    public static BI_COL_Descripcion_de_servicio__c             objDesSer;
    public static Account                                       objCuenta;
    public static Opportunity                                   objOpp;
    public static List<RecordType>                              rtBI_FUN;
    public static List<RecordType>                              rtOpp;
    public static list<BI_COL_ContractExcel_ctr.WrapperLst>     lstGlobal;
    public static List<BI_COL_Modificacion_de_Servicio__c>      lstMS;
    public static List<User>                                    lstUsuarios;
    
    static void crearData()
    {
        //List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        ////list<User> lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

        ////lstRol
        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        //insert objUsuario;
        
        //lstUsuarios = new list<User>();
        //lstUsuarios.add(objUsuario);

        //System.runAs(lstUsuarios[0])
        //{
            objCuenta                                       = new Account();
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
            objCuenta.CurrencyIsoCode                       = 'GTQ';
            insert objCuenta;
            System.debug('\n\n\n Sosalida objCuenta '+ objCuenta );
            
            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
        	objContacto.FirstName 							= 'fisrtname';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '57123456';
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //objContacto.BI_Tipo_de_contacto__c              = 'Autorizado';
            objContacto.BI_Tipo_de_contacto__c              = 'General';
       		//REING_FIN
            
            Insert objContacto;

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);

            objOpp                                          = new Opportunity();
            objOpp.Name                                     = 'prueba opp';
            objOpp.AccountId                                = objCuenta.Id;
            objOpp.BI_Country__c                            = 'Colombia';
            objOpp.CloseDate                                = System.today().addDays(+5);
            objOpp.StageName                                = 'F6 - Prospecting';
            objOpp.CurrencyIsoCode                          = 'GTQ';
            objOpp.Certa_SCP__contract_duration_months__c   = 12;
            objOpp.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
            objOpp.BI_COL_Autoconsumo__c                    = false;
            insert objOpp;
            System.debug('\n\n\n Sosalida objOpp '+ objOpp );

            rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
//Contrato
            Contract objContrato                                     = new Contract ();

            objContrato.AccountId                           = objCuenta.Id;
            objContrato.StartDate                           =  System.today().addDays(+5);
            objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
            objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
            objContrato.ContractTerm                        = 12;
            objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
            objContrato.BI_Indefinido__c                    = 'Si';
            objContrato.BI_COL_Cuantia_Indeterminada__c     = true;
            objContrato.BI_COL_Duracion_Indefinida__c       = true;
            insert objContrato;
            System.debug('datos Contratos ===>'+objContrato);

            objAnexos               = new BI_COL_Anexos__c();
            objAnexos.Name          = 'FUN-0041414';
            objAnexos.RecordTypeId  = rtBI_FUN[0].Id;
            objAnexos.BI_COL_Contrato__c=objContrato.id;
            insert objAnexos;
            System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);

            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOpp.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
            System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);
            List<BI_COL_Descripcion_de_servicio__c> lstqry = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
            System.debug('\n\n\n Sosalida lstqry '+ lstqry);

            objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
            objModSer.BI_COL_FUN__c                         = objAnexos.Id;
            objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModSer.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModSer.BI_COL_Bloqueado__c                   = false;
            objModSer.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            insert objModSer;
            System.debug('\n\n\n Sosalida objModSer '+ objModSer);
        //} 

    }
    
    static testmethod void myTestMethod()
    {

        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('idFUN', objAnexos.Id);            
            ApexPages.currentPage().getParameters().put('CFM', 'true');
            ApexPages.currentPage().getParameters().put('CNX', 'true');
            ApexPages.currentPage().getParameters().put('nombreSucursal', 'true');
            ApexPages.currentPage().getParameters().put('ciudadDestino', 'true');
            ApexPages.currentPage().getParameters().put('bwPorExceso', 'true');
            ApexPages.currentPage().getParameters().put('costoBwPorExceso', 'true');
            ApexPages.currentPage().getParameters().put('demo', 'true');
            ApexPages.currentPage().getParameters().put('anexoServicios', '[1],[2],[3]');
            BI_COL_ContractExcel_ctr ctr = new BI_COL_ContractExcel_ctr();
            List<BI_COL_Modificacion_de_Servicio__c>  lstMod=new List<BI_COL_Modificacion_de_Servicio__c>();
            lstMod.add(objModSer);
            ctr.lstMS=lstMod;
            ctr.fun = objAnexos;
            String str = ctr.numContrato;
            String rzn = ctr.razonSocial;
            String nt = ctr.nit;
            String dir = ctr.direccion;
            String tel = ctr.telefono;
            String eml = ctr.email;
            Decimal tot = ctr.totalInstalacion;
            Decimal total = ctr.totalServicio;
            Decimal totalOr= ctr.totalOrden;
            String mon = ctr.tipoMoneda;
            Integer dur = ctr.duracionContrato;
            String ase = ctr.asesorVtas;
            Integer cant = ctr.cantidadColumnas;
            boolean logo = ctr.logoMovistar;
            boolean logotel =ctr.logoTelefonica;
            String tipoOr= ctr.tipoOrden;
            String dire = ctr.direcc;
            ctr.lstMS = new List<BI_COL_Modificacion_de_Servicio__c>();
            ctr.lstMS.add(objModSer);
            String anexo = ctr.anexoServicios;
            String  cf = ctr.CFM;
            String  cn = ctr.CNX;
            String  nombre = ctr.nombreSucursal;
            String  cuidad = ctr.ciudadDestino;
            String  bwPor = ctr.bwPorExceso;
            String  costoBwP = ctr.costoBwPorExceso;
            String  dem = ctr.demo;
            String numor = ctr.numOrdenServicio;
            lstGlobal = ctr.lstGlobal;
            List<BI_COL_Modificacion_de_Servicio__c> lst = ctr.getMSSolicitudServicio('idFUN');
            ctr.calcularTotales(lst);
            System.debug(ctr.anexoServicios);
            Test.stopTest();
       
        }
    }

    static testmethod void myTestMethod1()
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            crearData();
            Test.startTest();
            
            BI_COL_ContractExcel_ctr ctr = new BI_COL_ContractExcel_ctr();
            ctr.cantidadColumnas=12;
            
            List<BI_COL_Modificacion_de_Servicio__c>  lstMod=new List<BI_COL_Modificacion_de_Servicio__c>();
            for(integer i=0;i<14;i++)
                lstMod.add(objModSer);
            ctr.lstMS=lstMod;
            System.debug('\n\n'+ctr.lstGlobal);
            
            Test.stopTest();
       
        }
    }
    
}