/*-------------------------------------------------------------------------------------
 Author:         -
 Company:        New Energy Aborda
 Description:    Methods that tests TGS_CaseMethods

 History
 <Date>      <Author>              <Description>
 27/7/2016  Juan Carlos Terrón      Second Version of the class, previous one commented
                                    on the top of the class.
 07/08/2016	Jorge Galindo			Changes for avoiding too many queries error
-------------------------------------------------------------------------------------*/
@isTest(seeAllData=false)
public class TGS_Billing_Date_TEST {
    /*static testMethod void testBillingDate(){
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        //Se escoge el perfil dentro de Salesforce
        System.runAs(new User(Id = Userinfo.getUserId())) {		
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
        // Se genera una user organization TGS para que el trigger salte.
           
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        
       //Se genera un usuario con permisos TGS y profile TGS System Administrator
        
        System.runAs(userTest){
            //Se genera un Order que tenga un Case de tipo "Order Management"
          	NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfigurationOrder();     //Con este comando se genera un order item con una order y case relacionados
            NE__OrderItem__c testAssetItem = TGS_Dummy_Test_Data.dummyConfigurationOrder();     //Con este comando se genera un order item con una order y case relacionados
            Test.StartTest();
            testOrderItem.TGS_billing_start_date__c=System.today();
            testOrderItem.TGS_Flag_BSD__c=false;
            testOrderItem.TGS_Change_MRC__c=true;
            testOrderItem.NE__ProdId__c=testAssetItem.NE__ProdId__c;
            update testOrderItem;
            Case testCase = [Select Id,asset__c,status,Order__c FROM Case WHERE Order__c=:testOrderItem.NE__OrderId__c];
            testCase.Asset__c = testAssetItem.NE__OrderId__c; 													//Se añade el asset al case.
			testCase.status = 'Closed';															//Se cierra el case
            update testCase;
            TGS_Billing_Date.setCiBillingDate(testCase);
			Test.StopTest();
		}
    }*/

    /*-------------------------------------------------------------------------------------
     Author:         -
     Company:        New Energy Aborda
     Description:    Tests TGS_CaseMethods.caseEmailSend()
    
     History
     <Date>      <Author>              <Description>
     21/07/2016  Juan Carlos Terrón    Second Version of the method commented above in 
                                        order to adapt it to the changes made in TGS_Billing_Date_c
     06/08/2016  Humberto Nunes        Se coloco el NETriggerHelper.setTriggerFired('BI_Case'); para que no se ejecutara ya que da too many querys... 
	 07/10/2016  Sara Nuñez				Añadido parametro en case para pasar custom validations
    -------------------------------------------------------------------------------------*/
    static testMethod void testBillingDate()
    {
        String profile = 'TGS System Administrator';
        Profile tgsSysAdmin = [SELECT Id FROM Profile WHERE Name = :profile];

        //Se escoge el perfil dentro de Salesforce
        System.runAs(new User(Id = Userinfo.getUserId())) {     
            insert new TGS_User_Org__c(TGS_Is_TGS__c = true, SetupOwnerId = tgsSysAdmin.Id);
        }
        // Se genera una user organization TGS para que el trigger salte.
        
        BI_TestUtils.throw_exception = false;
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        BI_MigrationHelper.setSkippedTrigger('User');
        insert userTest;
        BI_MigrationHelper.cleanSkippedTriggers();
        
       //Se genera un usuario con permisos TGS y profile TGS System Administrator
        
        System.runAs(userTest) {
			// JGL 07/08/2016
            //Test.startTest();
           
            NETriggerHelper.setTriggerFired('BI_Case');
            NETriggerHelper.setTriggerFired('BI_FVIBorrar');
            NETriggerHelper.setTriggerFired('BI_FVI_Autosplit');
            NETriggerHelper.setTriggerFired('NECheckDeltaQuantity');
            NETriggerHelper.setTriggerFired('NECheckOrderItems');
            NETriggerHelper.setTriggerFired('NESetDefaultOIFields');
            NETriggerHelper.setTriggerFired('BI_OpportunityMethods.createTask');
            // END JGL 07/08/2016
            NE__OrderItem__c oi_term = TGS_Dummy_Test_Data.dummyConfigurationOrder();    
            Test.startTest(); 
            NE__OrderItem__c ai_term = TGS_Dummy_Test_Data.dummyConfigurationOrder();
            NE__Asset__c commAsset = new NE__Asset__c(
                TGS_RFB_date__c = Date.today(),
                TGS_RFS_date__c = Date.today(),
                TGS_Billing_end_date__c = Date.today()
            );
            insert commAsset;
            
            update new NE__Order__c[] {
                new NE__Order__c(Id = oi_term.NE__OrderId__c, NE__Asset__c = commAsset.Id),
                new NE__Order__c(Id = ai_term.NE__OrderId__c, NE__Asset__c = commAsset.Id)
            };
            

            //JGL 07/08/2016 moved
            
            List<NE__OrderItem__c> list_orderItems_toProcess = new List<NE__OrderItem__c>();
            List<Case> list_Case_toProcess = new List<Case>();

            
            /*--Termination--*/
                oi_term.TGS_billing_start_date__c = null;
                oi_term.TGS_Flag_BSD__c = false;
                oi_term.TGS_Change_MRC__c = true;
                oi_term.NE__Action__c = 'Add';
                oi_term.NE__ProdId__c = ai_term.NE__ProdId__c;

                ai_term.NE__AssetItemEnterpriseId__c = oi_term.Id;
                list_orderItems_toProcess.add(ai_term);

                Case cas_term = [SELECT Id, Asset__c, Status, Order__c, TGS_RFS_date__c FROM Case WHERE Order__c=:oi_term.NE__OrderId__c];
                cas_term.Asset__c = ai_term.NE__OrderId__c;//Se añade el asset al case.
                cas_term.TGS_RFS_date__c = Date.today();
                cas_term.status = 'Closed';
            	cas_term.TGS_Casilla_Desarrollo__c = true;//SNM 07/10/2016
                cas_term.Type = 'Disconnect';
                list_Case_toProcess.add(cas_term);
            /*--Termination--*/
           
            //update list_orderItems_toProcess;
            update list_Case_toProcess;

            
            TGS_Billing_Date.setCiBillingDate(cas_term);
            Test.stopTest();
        }
    }
}