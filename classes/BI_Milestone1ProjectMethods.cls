public class BI_Milestone1ProjectMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Milestone1_Project_Trigger 
    Test Class:    BI_Milestone1ProjectMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    08/07/2014              Ignacio Llorca          Initial Version
    05/07/2016              Jose Miguel Fierro      Method preventDeleteTemplate and isProfileSysAdmin added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History:       Method that creates a record of BI_Recurso__c related to the OpportunityTeamMember when inserting a Milestone1_Project__c
    
    <Date>                  <Author>                <Change Description>
    08/07/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void addOTM(List <Milestone1_Project__c> news){
        Set <Id> set_mpOpp = new Set <Id>();
        
        for (Milestone1_Project__c mpOpp:news){
            set_mpOpp.add(mpOpp.BI_Oportunidad_asociada__c);
        }
        
        List<OpportunityTeamMember> lst_otm = [SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMember WHERE OpportunityId IN :set_mpOpp];
        
        List<BI_Recurso__c> lst_rec = new List<BI_Recurso__c>();
        
        for (Milestone1_Project__c mpro:news){
            for (OpportunityTeamMember otm:lst_otm){
                BI_Recurso__c res = new BI_Recurso__c (BI_Funcion__c = otm.TeamMemberRole,
                                                       BI_Activo__c = true,
                                                       BI_Miembro_de_equipo__c = otm.UserId,
                                                       BI_Proyecto__c = mpro.Id);
                lst_rec.add(res);
            }
        }
        insert lst_rec;
    }   
    
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History:       Method that check the mandatory attachment were inserted before change the status
    
    <Date>                  <Author>                <Change Description>
    22/09/2014              Diego Arenas          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void checkMandatoryAttachment(List <Milestone1_Project__c> news, List <Milestone1_Project__c> olds, map<Id,Milestone1_Project__c> mapsNews){
        try{
            list<string> listCountry = new list<string>();
            list<string> listComplex = new list<string>();
            list<string> listStage = new list<string>();
            list<string> listprojectId = new list<string>();
            list<Milestone1_Project__c> listProjectOk = new list<Milestone1_Project__c>();
            integer counter = 0;
            for(Milestone1_Project__c item : news){
                if(olds[counter].BI_Etapa_del_proyecto__c != item.BI_Etapa_del_proyecto__c){
                    listCountry.add(olds[counter].BI_Country__c);
                    listComplex.add(olds[counter].BI_Complejidad_del_proyecto__c);
                    listStage.add(olds[counter].BI_Etapa_del_proyecto__c);
                    listprojectId.add(olds[counter].Id);
                    listProjectOk.add(olds[counter]);
                }
                counter++;
            }
            
            //Obtain the attachments and put in to Map the proyectId with the description the attachment insert
            map<string, set<string>> mapProjectIdDescriptioAtt = new map<string, set<string>>();
            list<Attachment> listAtta = [Select Id, Description, ParentId From Attachment where parentId IN : listprojectId order by parentId];
            if(!listAtta.isEmpty()){
                for (Attachment item : listAtta){
                    set<string> setDescriptionAux = mapProjectIdDescriptioAtt.get(item.ParentId);
                    if(setDescriptionAux == null){
                        setDescriptionAux = new set<string>();
                    }
                    setDescriptionAux.add(item.Description);
                    mapProjectIdDescriptioAtt.put(item.ParentId, setDescriptionAux);
                }
            }
            
            Schema.DescribeFieldResult fieldResult = Milestone1_Project__c.BI_Etapa_del_proyecto__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            map<string,integer> mapEtapas = new map<string, integer>();
            integer iteration=0;
            for(Schema.PicklistEntry pcklst :ple){
                 
                mapEtapas.put(pcklst.getValue(), iteration);
                iteration++;
            }           

            if(!listCountry.isEmpty() || !listComplex.isEmpty() || !listStage.isEmpty()){       
                string stringCountries = getStringParse(listCountry);
                string stringComplex = getStringParse(listComplex);
                string stringStage = getStringParse(listStage);
            
            //we need the description for the attachments for each project
            String queryStr = 'Select Id, BI_Nombre_del_documento__c, BI_Objeto__c, BI_Obligatoriedad__c, BI_Country__c, BI_Etapa_del_proyecto__c, BI_Complejidad_del_proyecto__c';
            queryStr = queryStr + ' From BI_Configuracion_de_documentacion__c'; 
            queryStr = queryStr + ' where BI_Obligatoriedad__c = \'Sí\' and BI_Objeto__c = \'Proyecto\' and BI_Country__c IN (' +  stringCountries + ')';
            queryStr = queryStr + ' and BI_Complejidad_del_proyecto__c IN (' +  stringComplex + ')';
            queryStr = queryStr + ' and BI_Etapa_del_proyecto__c INCLUDES (' + stringStage +  ')';
            system.debug(queryStr);
            
            list<BI_Configuracion_de_documentacion__c> listConfDocument = Database.query(queryStr);
            if(mapEtapas.get(news[0].BI_Etapa_del_proyecto__c) > mapEtapas.get(olds[0].BI_Etapa_del_proyecto__c)){
            if(!listConfDocument.isEmpty()){
                for(Milestone1_Project__c item : listProjectOk){
                    for(BI_Configuracion_de_documentacion__c item2 : listConfDocument){
                        if(item2.BI_Etapa_del_proyecto__c.contains(item.BI_Etapa_del_proyecto__c)
                        && item.BI_Complejidad_del_proyecto__c == item2.BI_Complejidad_del_proyecto__c
                        && item.BI_Country__c == item2.BI_Country__c){
                            set<string> setDescriptionAux2 = mapProjectIdDescriptioAtt.get(item.id);
                            if(setDescriptionAux2 == null || (setDescriptionAux2 != null && !setDescriptionAux2.contains(item2.BI_Nombre_del_documento__c))){
                                Milestone1_Project__c MPAux = mapsNews.get(item.id);
                                MPAux.addError(Label.BI_FaltanAdjuntosProyecto);
                                break;
                            }
                        }
                    }
                }
            
            }
        }
        }
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_Milestone1ProjectMethods.checkMandatoryAttachment', 'BI_EN', Exc, 'Trigger');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Prevents the all users from deleting a template-project, except for System Administrators
     History:
     
     <Date>            <Author>              <Description>
     05/07/2016        Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventDeleteTemplate(List<Milestone1_Project__c> triggerList) {
        try {
            for(Milestone1_Project__c project  : triggerList) {
                if(project.BI_O4_Template__c && !isProfileSysAdmin(UserInfo.getProfileId())) {
                    project.addError('You cannot delete template projects');
                }
            }
        } catch (Exception exc) {
            BI_LogHelper.generate_BILog('Milestone1_Project_Trigger_Utility.preventDeleteTemplate', 'BI_O4', exc, 'Apex Class');
        }
    }

        @testVisible
    private static Map<String, Profile> mapProfiles;


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Retruns if the current user is a 'System Administrator'
     History:
     
     <Date>            <Author>              <Description>
     05/07/2016        Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static boolean isProfileSysAdmin(Id profileId) { //UserInfo.getProfileId();
        if(mapProfiles == null) {
            mapProfiles = new Map<String, Profile>([SELECT Id, Name FROM Profile]);
        }

        return mapProfiles.get(profileId).Name == Label.BI_Administrador_Sistema;
    }

    static string getStringParse (list<string> listStringToChange){
        string res = '';
        for(string item : listStringToChange){
            res = res + '\'' + item + '\'' + ','; 
        }
        system.debug('res: '+res);
        res = res.substring(0, res.length()-1);
        system.debug('res after substring: '+res);
        return res;
    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jaime Regidor Morales
     Company:       Accenture
     Description:   Change owner of project
     History:
     
     <Date>          <Author>                 <Description>
     30/05/2017      Jaime Regidor Morales    Initial version
     20/06/2017      Jaime Regidor Morales    version 1.0
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


        public static void setProjectOwner(List <Milestone1_Project__c> news,Map <Id,Milestone1_Project__c> olds){
            try{

                

                List <Milestone1_Project__c> toProcess = new List <Milestone1_Project__c>();
                for(Milestone1_Project__c proSteady :news){
                        if(proSteady.BI_O4_Head_of_Project_Management__c == 'Customer Relationship Executive' && (olds == null || proSteady.BI_O4_Head_of_Project_Management__c != olds.get(proSteady.id).BI_O4_Head_of_Project_Management__c)){
                                toProcess.add(proSteady);
                        }
                }            

                    if(!toProcess.isEmpty()){
                        Group grupo = [Select Id from Group where DeveloperName = 'BI_O4_TNA_CustomerRelationshipExecutives' limit 1];
                        for(Milestone1_Project__c proSteady :toProcess){
                            proSteady.OwnerId = grupo.Id;
                        }
                    }    
                } catch (Exception exc) {
                        BI_LogHelper.generate_BILog('BI_Milestone1ProjectMethods.setProjectOwner', 'BI_O4', exc, 'Apex Class');
                    }
        }
}