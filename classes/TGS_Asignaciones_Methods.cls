/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Juan Carlos Terrón
    Company:       New Energy Aborda
    Description:   Trigger to handle TGS_Asignaciones__c custom object events.
    
    History:   
    <Date>                  <Author>                <Change Description>   
    14/11/2016				Juan Carlos Terrón		Initial Version.
    23/11/2016				Juan Carlos Terrón		Added null TGS_Status_Reason__c field checking to avoid null pointer exception
    												on certain case status casuistry.     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class TGS_Asignaciones_Methods {
	public static void generate_TGS_AssignId(List<TGS_Asignaciones__c> news){
		String str_concat = '';
		Set<Id> set_AccountID = new Set<Id>();
		Map<Id,Account> map_Accounts;
		for(TGS_Asignaciones__c assign : news)
		{
			if(!set_AccountID.contains(assign.TGS_Account__c) && assign.TGS_Account__c != null)
			{
				set_AccountID.add(assign.TGS_Account__c);
			}
		}
		map_Accounts =  new Map<Id,Account>(
							[
							SELECT 	Id, TGS_Aux_Holding__c, RecordType.DeveloperName
							FROM 	Account
							WHERE 	Id IN : set_AccountID AND
									(TGS_Aux_Holding__c != null OR
									RecordType.DeveloperName = :Constants.RECORD_TYPE_TGS_HOLDING)
							]
						);

		if(map_Accounts!=null && !map_Accounts.isEmpty())
		{
			for(TGS_Asignaciones__c assign : news)
			{
				if(assign.TGS_Account__c!=null && map_Accounts.containsKey(assign.TGS_Account__c))
				{
					Boolean holding = map_Accounts.get(assign.TGS_Account__c).TGS_Aux_Holding__c!=null;
					if(holding)
					{
						str_concat += map_Accounts.get(assign.TGS_Account__c).TGS_Aux_Holding__c+'-';
						str_concat += String.valueOf(assign.TGS_Service__c).deleteWhitespace().toLowerCase()+'-';
						str_concat += String.valueOf(assign.TGS_Status__c).deleteWhitespace().toLowerCase()+'-';
						if(assign.TGS_Status_Reason__c!=null)
						{
							str_concat += String.valueOf(assign.TGS_Status_Reason__c).deleteWhitespace().toLowerCase()+'-';
						}
						else
						{
							str_concat += '-';
						}
						str_concat += String.valueOf(assign.BI_Tipo_de_Caso__c).deleteWhitespace().toLowerCase()+'-';
						if(assign.TGS_CWP_Case__c){str_concat += 'true';} else {str_concat += 'false';}

						assign.TGS_AssignId__c = str_concat;
						str_concat = '';
					}
					else if(map_Accounts.get(assign.TGS_Account__c).RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_HOLDING)
					{
						str_concat += map_Accounts.get(assign.TGS_Account__c).Id+'-';
						str_concat += String.valueOf(assign.TGS_Service__c).deleteWhitespace().toLowerCase()+'-';
						str_concat += String.valueOf(assign.TGS_Status__c).deleteWhitespace().toLowerCase()+'-';
						if(assign.TGS_Status_Reason__c!=null)
						{
							str_concat += String.valueOf(assign.TGS_Status_Reason__c).deleteWhitespace().toLowerCase()+'-';
						}
						else
						{
							str_concat += '-';
						}
						str_concat += String.valueOf(assign.BI_Tipo_de_Caso__c).deleteWhitespace().toLowerCase()+'-';
						if(assign.TGS_CWP_Case__c){str_concat += 'true';} else {str_concat += 'false';}

						assign.TGS_AssignId__c = str_concat;
						str_concat = '';
					}
				}
			}
		}
	}
	public static void modify_TGS_AssignId(Map<Id,TGS_Asignaciones__c> news, Map<Id,TGS_Asignaciones__c> olds){
		Map<Id,TGS_Asignaciones__c> map_NewAsignaciones = new Map<Id,TGS_Asignaciones__c>([SELECT Id,TGS_Account__c,TGS_AssignId__c,TGS_Cola__c,TGS_CWP_Case__c,TGS_Service__c,TGS_Status__c,TGS_Status_Reason__c,BI_Tipo_de_Caso__c FROM TGS_Asignaciones__c WHERE Id IN :news.keyset()]);
		generate_TGS_AssignId(map_NewAsignaciones.values());
	}
}