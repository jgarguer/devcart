@isTest
public class VE_InputLookupController_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Test class for VE_InputLookupController
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    27/09/2017        Jaime Regidor      Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void testGetCurrentValue1(){
        Test.startTest();
     	String result = VE_InputLookupController.getCurrentValue(null, null);
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testGetCurrentValue2(){
        Test.startTest();
     	String result = VE_InputLookupController.getCurrentValue('no-null', null);
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testGetCurrentValue3(){
        
        Account a = new Account();
        a.name='test';
        a.BI_Segment__c = 'test'; //27/09/2017
        a.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        a.BI_Territory__c = 'test'; //27/09/2017
        insert a;
        
        String value = a.Id;
        
        Test.startTest();
     	String result = VE_InputLookupController.getCurrentValue('no-null', value);
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testGetCurrentValue4(){
        Test.startTest();
     	String result = VE_InputLookupController.getCurrentValue('no-null', '');
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testGetCurrentValue5(){
        
        Account a = new Account();
        a.name='test';
        a.BI_Segment__c = 'test'; //27/09/2017
        a.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        a.BI_Territory__c = 'test'; //27/09/2017
        insert a;
        String value = a.Id;
        
        Test.startTest();
     	String result = VE_InputLookupController.getCurrentValue('Account', value);
        test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testGetCurrentValue6(){
                        
        Test.startTest();
     	String result = VE_InputLookupController.getCurrentValue('Account', '0018E000007LvvD');
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testSearchSObject1(){        
        Test.startTest();
     	String result = VE_InputLookupController.searchSObject(null, null);
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testSearchSObject2(){        
        Test.startTest();
     	String result = VE_InputLookupController.searchSObject('no-null', null);
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testSearchSObject6(){        
        Test.startTest();
     	String result = VE_InputLookupController.searchSObject('no-null', 'no-null');
        test.stopTest();
        
        System.assert(result==null);
    }
    
    static testMethod void testSearchSObject3(){  
        
        Account a = new Account();
        a.name='test';
        a.BI_Segment__c = 'test'; //27/09/2017
        a.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        a.BI_Territory__c = 'test'; //27/09/2017
        insert a;
        
        Test.startTest();
     	String result = VE_InputLookupController.searchSObject('Account', 'test');
        test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testSearchSObject4(){  
        
        Account a = new Account();
        a.name='test';
        a.BI_Segment__c = 'test'; //27/09/2017
        a.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        a.BI_Territory__c = 'test'; //27/09/2017
        insert a;
        
        Test.startTest();
     	String result = VE_InputLookupController.searchSObject('Account', 'pero');
        test.stopTest();
        
        System.assert(result!=null);
    }
    
    static testMethod void testSearchSObject5(){  
        
        Account a = new Account();
        a.name='test';
        a.BI_Segment__c = 'test'; //27/09/2017
        a.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        a.BI_Territory__c = 'test'; //27/09/2017
        insert a;
        
        Test.startTest();
     	String result = VE_InputLookupController.searchSObject('Opportunity', 'pero');
        test.stopTest();
        
        System.assert(result!=null);
    }
    
}