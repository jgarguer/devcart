/***********************************************************************************************
* Avanxo Colombia
 * @author              Carlos A. Rodriguez Bejarano.
 * @project             Telefonica
 * @description         Test class of BI_COL_Address_standard_ctr.
 * 
 *Changes (Version)
 *-----------------------------------------------------------------------------
 *          No.     Date            Autor                   Description
 *          -----   ----------      --------------------    -------------------
 *@version  1.0     30/06/2015      Carlos Rodriguez (CR)   Class created.
**************************************************************************************************/
@isTest
private class BI_COL_Address_standard_tst
{
	static list<Profile> 											lstPerfil;
	static list <UserRole> 											lstRoles;
	static User 													objUsuario = new User();
	static BI_COL_Opcion_callejero__c 								objOpCallejeroOne = new BI_COL_Opcion_callejero__c();
	static BI_COL_Opcion_callejero__c 								objOpCallejeroTwo = new BI_COL_Opcion_callejero__c();
	static List<BI_COL_Opcion_callejero__c> 						lstOpcionCalle = new List<BI_COL_Opcion_callejero__c>();
	static BI_Sede__c 												objSede = new BI_Sede__c();
	static BI_COL_Address_standard_ctr.WrpSucOpcCallejero 			objWrpSucOpcCalle;
	static List<BI_COL_Address_standard_ctr.WrpSucOpcCallejero> 	lstWrpSucOpcCalle = new List<BI_COL_Address_standard_ctr.WrpSucOpcCallejero>();
	static BI_COL_Address_standard_ctr.WrpOpcCallejero  			objWrpOpCallejero;

	private static void createData()
	{
		////lstPerfil
		//lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

		////lstRol
		//lstRoles = new list <UserRole>();
		//lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

		////ObjUsuario
		//objUsuario = new User();
		//objUsuario.Alias = 'standt';
		//objUsuario.Email ='pruebas@test.com';
		//objUsuario.EmailEncodingKey = '';
		//objUsuario.LastName ='Testing';
		//objUsuario.LanguageLocaleKey ='en_US';
		//objUsuario.LocaleSidKey ='en_US'; 
		//objUsuario.ProfileId = lstPerfil.get(0).Id;
		//objUsuario.TimeZoneSidKey ='America/Los_Angeles';
		//objUsuario.UserName ='pruebas@test.com';
		//objUsuario.EmailEncodingKey ='UTF-8';
		//objUsuario.UserRoleId = lstRoles.get(0).Id;
		//objUsuario.BI_Permisos__c ='Sucursales';
		//objUsuario.Pais__c='Colombia';
		
		//list<User> lstUsuarios = new list<User>();
		//lstUsuarios.add(objUsuario);
		//insert lstUsuarios;

		//System.runAs(lstUsuarios[0])
		//{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			
			//objSede.BI_COL_Ciudad_Departamento__c = objCiudad.Id;
			objSede.BI_Direccion__c = 'Test Street 123 Number 321';
			objSede.BI_Localidad__c = 'Test Local';
			objSede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c = 'Libre';
			objSede.BI_Country__c = 'Colombia';
			objSede.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c = '12356';
			insert objSede;

			//objOpCallejeroOne.Name = 'test name';
			objOpCallejeroOne.BI_COL_Direccion_Sede__c = 'Test Direccion';
			objOpCallejeroOne.BI_COL_Direccion__c = objSede.Id;
			insert objOpCallejeroOne;

			//objOpCallejeroTwo.Name = 'test name dos';
			objOpCallejeroTwo.BI_COL_Direccion_Sede__c = 'Test Direccion2';
			insert objOpCallejeroTwo;
		//}

		
	}
	
	@isTest static void test_method_one()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) 
		{
			createData();
			lstOpcionCalle.add(objOpCallejeroOne);
			lstOpcionCalle.add(objOpCallejeroTwo);
			objWrpOpCallejero = new BI_COL_Address_standard_ctr.WrpOpcCallejero (lstOpcionCalle);
			objWrpOpCallejero.strCheck = objOpCallejeroOne.Id;
			objWrpSucOpcCalle = new BI_COL_Address_standard_ctr.WrpSucOpcCallejero(objSede, lstOpcionCalle);
			objWrpSucOpcCalle.OpcCallejero = objWrpOpCallejero;
			lstWrpSucOpcCalle.add(objWrpSucOpcCalle);
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(objSede);
			Test.startTest();
			BI_COL_Address_standard_ctr objAddressStandart = new BI_COL_Address_standard_ctr(controller);
			objAddressStandart.lstSucOpCa = lstWrpSucOpcCalle;
			objAddressStandart.IdDireccion = 'a2Dg00000006ZENEA2';
			objAddressStandart.Save();
			Test.stopTest();
		}
	}

		@isTest static void test_method_two()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) 
		{
			createData();
			lstOpcionCalle.add(objOpCallejeroOne);
			lstOpcionCalle.add(objOpCallejeroTwo);
			objWrpOpCallejero = new BI_COL_Address_standard_ctr.WrpOpcCallejero (lstOpcionCalle);
			objWrpOpCallejero.strCheck = objOpCallejeroOne.Id;
			objWrpSucOpcCalle = new BI_COL_Address_standard_ctr.WrpSucOpcCallejero(objSede, lstOpcionCalle);
			objWrpSucOpcCalle.OpcCallejero = objWrpOpCallejero;
			lstWrpSucOpcCalle.add(objWrpSucOpcCalle);
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(objSede);
			Test.startTest();
			BI_COL_Address_standard_ctr objAddressStandart = new BI_COL_Address_standard_ctr(controller);
			objAddressStandart.lstSucOpCa = lstWrpSucOpcCalle;
			objAddressStandart.Save();
			Test.stopTest();
		}
	}
	
	@isTest static void test_method_three()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) 
		{
			createData();
			objWrpSucOpcCalle = new BI_COL_Address_standard_ctr.WrpSucOpcCallejero(objSede, lstOpcionCalle);
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(objSede);
			Test.startTest();
			BI_COL_Address_standard_ctr objAddressStandart = new BI_COL_Address_standard_ctr(controller);
			objAddressStandart.lblError = '';
			objAddressStandart.Draw();
			BI_COL_Address_standard_ctr.WrpSucOpcCallejero objWrpSucOpcCalleTwo = new BI_COL_Address_standard_ctr.WrpSucOpcCallejero();
			BI_COL_Address_standard_ctr.WrpOpcCallejero objWrpOpCallejeroTwo = new BI_COL_Address_standard_ctr.WrpOpcCallejero();
			Test.stopTest();
		}
	}
	
}