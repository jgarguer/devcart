public class Constants 
{
    /* Case */
    
    // Status field values
    public static final String CASE_STATUS_ASSIGNED = 'Assigned';
    public static final String CASE_STATUS_RESOLVED = 'Resolved';
    public static final String CASE_STATUS_PENDING = 'Pending';
    public static final String CASE_STATUS_CANCELLED = 'Cancelled';
    public static final String CASE_STATUS_REJECTED = 'Rejected';
    public static final String CASE_STATUS_CLOSED = 'Closed';
    public static final String CASE_STATUS_IN_PROGRESS = 'In Progress';
    // Status reason field values    
    public static final String CASE_STATUS_REASON_FINANCIAL_RISK = 'Financial Risk';
    public static final String CASE_STATUS_REASON_IN_ACTIVATION = 'In Activation';
    public static final String CASE_STATUS_REASON_IN_CANCELLATION = 'In Cancellation';
    public static final String CASE_STATUS_REASON_IN_CANCELLATION_CLOSE = 'In Cancellation - Close';
    public static final String CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE = 'In Cancellation - Customer Acceptance';
    public static final String CASE_STATUS_REASON_IN_CANCELLATION_NOT_PAID = 'In Cancellation – Not Paid';
    public static final String CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET = 'In Cancellation - Off-Net';
    public static final String CASE_STATUS_REASON_IN_CONFIGURATION = 'In Configuration';
    public static final String CASE_STATUS_REASON_IN_ORDER_HANDLING = 'In Order Handling';
    public static final String CASE_STATUS_REASON_IN_PROVISION = 'In Provision';
    public static final String CASE_STATUS_REASON_PROVISION_START = 'Provision Start'; //Fixed Voice status reason
    public static final String CASE_STATUS_REASON_IN_SD_VALIDATION = 'In SD Validation';
    public static final String CASE_STATUS_REASON_IN_TEST = 'In Test';
    public static final String CASE_STATUS_REASON_OFF_NET_SUPPLIER_DATA_CHECKED = 'Off-Net Supplier Data Checked';        
    public static final String CASE_STATUS_REASON_PENDING_TASK_RESOLUTION = 'Pending Task Resolution';
    public static final String CASE_STATUS_REASON_POF_SIGNED = 'POF Signed';    
    public static final String CASE_STATUS_REASON_SO_ACCEPTED = 'SO Accepted';
    public static final String CASE_STATUS_REASON_SO_LAUNCHED_POF_REQUIRED = 'SO Launched (& POF Required)';
    public static final String CASE_STATUS_REASON_WAITING_FOR_QUOTATION_ACCEPTANCE = 'Waiting For Quotation Acceptance';
    // Record type values
    public static final String CASE_RTYPE_ORDER_MNGMNT = 'Order Management Case';
    public static final String CASE_RTYPE_DEVNAME_ORDER_MNGMNT = 'Order_Management_Case';
    
    // DEPRECATED Acting as values
    public static final String CASE_ACTING_AS_ON_BEHALF = 'On Behalf';
    public static final String CASE_ACTING_AS_AUTHORIZED_USER = 'Authorized User';
    public static final String CASE_ACTING_AS_SUPPORT_AGENT = 'Support Agent';
    //Tier 2
    public static final String CASE_TIER2_MPLS_VPN = 'MPLS VPN';
    public static final String CASE_TIER2_INTERNET_ACCESS_TG_SOLUTIONS = 'Managed Internet Access';
    
    /* Configuration Item */
    
    //Status field values
    public static final String CI_STATUS_ACTIVE = 'Active';
    public static final String CI_STATUS_IN_PROGRESS = 'In Progress';
    public static final String CI_STATUS_PENDING = 'Pending';
    //TGS_SERVICE_STATUS field values
    public static final String CI_TGS_SERVICE_STATUS_DELETED = 'Deleted';
    public static final String CI_TGS_SERVICE_STATUS_DEPLOYED = 'Deployed';
    public static final String CI_TGS_SERVICE_STATUS_RECEIVED = 'Received';
    public static final String CI_TGS_SERVICE_STATUS_DISPOSED = 'Disposed';

    
    /* Configuration */
    
    public static final String CONFIGURATION_STATUS_ACTIVE = 'Active';
    // NE 26/11/2015 - Added Channel constant for Bulk context
    public static final String CONFIGURATION_CHANNEL_BULK = 'Bulk request';
    
    /* Profile */
    public static final String PROFILE_TGS_PARTNER_COMMUNITY = 'TGS_Partner Community';
    public static final String PROFILE_TGS_CUSTOMER_COMMUNITY_PLUS = 'TGS Customer Community Plus';
    public static final String PROFILE_TGS_INTEGRATION_USER = 'TGS Integration User';
    
    /*Type*/
    
    public static final String TYPE_DISCONNECT = 'Disconnect';
    public static final String TYPE_CHANGE = 'Change';
    public static final String TYPE_NEW = 'New';
    

    /* Record Type */
    
    public static final String RECORD_TYPE_ASSET = 'Asset';
    public static final String RECORD_TYPE_ORDER = 'Order';
    
    // GAB 27/03/2015 - CONSTANTS FROM TGSDEV, PLEASE MERGE THEM WITH THE ONES BELOW
    public static final String RECORD_TYPE_TGS_HOLDING ='TGS_Holding';
    public static final String RECORD_TYPE_TGS_CUSTOMER_COUNTRY = 'TGS_Customer_Country';
    public static final String RECORD_TYPE_TGS_LEGAL_ENTITY = 'TGS_Legal_Entity';
    public static final String RECORD_TYPE_TGS_BUSINESS_UNIT = 'TGS_Business_Unit';
    public static final String RECORD_TYPE_TGS_COST_CENTER = 'TGS_Cost_Center';
    public static final String RECORD_TYPE_TGS_INCIDENT = 'TGS_Incident';
    public static final String RECORD_TYPE_TGS_CHANGE = 'TGS_Change';
    public static final String RECORD_TYPE_TGS_QUERY = 'TGS_Query';
    public static final String RECORD_TYPE_OPTY = 'Opty';
    
    // GAB 09/04/2015 - CONSTANTS FROM INTDEV, PLEASE MERGE THEM WITH THE ONES ABOVE
    public static final String RECORD_TYPE_HOLDING_ACCOUNT = 'TGS_Holding';
    public static final String RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT = 'TGS_Customer_Country';
    public static final String RECORD_TYPE_LEGAL_ENTITY_ACCOUNT = 'TGS_Legal_Entity';
    public static final String RECORD_TYPE_BUSINESS_UNIT_ACCOUNT = 'TGS_Business_Unit';
    public static final String RECORD_TYPE_COST_CENTER_ACCOUNT = 'TGS_Cost_Center';
    
    public static final String RECORD_TYPE_HOLDING_ACCOUNT_LABEL = '1.Holding';
    public static final String RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT_LABEL = '2.Customer Country';
    public static final String RECORD_TYPE_LEGAL_ENTITY_ACCOUNT_LABEL = '3.Legal Entity';
    public static final String RECORD_TYPE_BUSINESS_UNIT_ACCOUNT_LABEL = '4.Business Unit';
    public static final String RECORD_TYPE_COST_CENTER_ACCOUNT_LABEL = '5.Cost Center';

    //Opportunity 
     public static final String RECORD_TYPE_CICLO_COMPLETO_OPP = 'BI_FVI_Ciclo_completo';
    
    /* Product */
    
    public static final String PRODUCT_SEC_MOB_DEV_APPS_AND_CONT_MNGMNT = 'Secure Mobile Device, Apps and Content Management';
    public static final String PRODUCT_SMDM_INVENTORY = 'SMDM Inventory';
    public static final String PRODUCT_SMDM_MANAGMENT = 'SMDM Management';
    public static final String PRODUCT_SMART_M2M = 'Smart M2M';
    public static final String PRODUCT_M2M_JASPER = 'M2M Jasper';
    /* Colocation/Housing */
    public static final String PRODUCT_HOUS_COLOCATION = 'Colocation/Housing';
    public static final String PRODUCT_HOUS_GENERAL = 'General Information';
    public static final String PRODUCT_HOUS_DEDICATED = 'Dedicated room';
    public static final String PRODUCT_HOUS_SHARED = 'Shared room';
    public static final String PRODUCT_HOUS_AC = 'AC Plug';
    public static final String PRODUCT_HOUS_DC = 'DC Plug';
    public static final String PRODUCT_HOUS_CROSS_WITH = 'Cross Connection with MMR Service';
    public static final String PRODUCT_HOUS_CROSS_WITHOUT = 'Cross Connection without MMR Service';
    public static final String PRODUCT_HOUS_HANDS = 'Hands and Eyes';
    /* MWAN */
    public static final String PRODUCT_MWAN_SU = 'mWan';
    public static final String PRODUCT_MWAN_INTERNET = 'mWan - Internet';
    //GMN 18/05/2017 - Add PRODUCT_MWAN_DATATEMPLATE constant
    public static final String PRODUCT_MWAN_DATATEMPLATE = 'mWAN-Data Template';
    public static final String PRODUCT_MWAN_OPT = 'mWan SU - Optimization';
    public static final String PRODUCT_MWAN_INT = 'mWan SU - Internet';
    public static final String PRODUCT_MWAN_TUNNELING = 'Split Tunneling';
    public static final String PRODUCT_MWAN_TEMPLATE = 'Data_Template';
    public static final String PRODUCT_MWAN_VPN = 'VPN';
    public static final String PRODUCT_MWAN_CPE = 'CPE';
    public static final String PRODUCT_MWAN_ADDRESS = 'IP_Address';
    public static final String PRODUCT_MWAN_WAN = 'IP WAN';
    public static final String PRODUCT_MWAN_CUSTOMER = 'IP Customer';
    public static final String PRODUCT_MWAN_LOOP = 'IP Loopback';
    public static final String PRODUCT_MWAN_LAN = 'IP Lan';
    public static final String PRODUCT_MWAN_HSRP = 'IP HSRP';
    public static final String PRODUCT_MWAN_ADD_EQ = 'Additional Equipment';
    public static final String PRODUCT_MWAN_SLA = 'mWan SU SLA';
    public static final String PRODUCT_MWAN_COS = 'CoS';
    public static final String PRODUCT_MWAN_SLA_COS = 'mWAN SU Technical SLA per CoS';
    public static final String PRODUCT_MWAN_ACCESS = 'Access';
    public static final String PRODUCT_MWAN_GLOBAL_P = 'Global Price';
    public static final String PRODUCT_MWAN_BANDWIDTH_P = 'IP Bandwidth price';
    public static final String PRODUCT_MWAN_MANAGEMENT_P = 'Service Management price';
    public static final String PRODUCT_MWAN_MULTICAST_P = 'Multicast price';
    public static final String PRODUCT_MWAN_ADDITIONAL_P = 'Additional Features price';
    // FAR 13/06/2016 - Add PRODUCT_MWAN_ADDITIONAL_EQ_P constant
    public static final String PRODUCT_MWAN_ADDITIONAL_EQ_P = 'Additional Equipment price';
    public static final String PRODUCT_MWAN_MAINTENANCE_P = 'Service Maintenance price';
    public static final String PRODUCT_MWAN_WAAS_DEVICE_P = 'WAAS Device Price';
    public static final String PRODUCT_MWAN_WAN_OPTIM_P = 'WAN Optimization Price';
    public static final String PRODUCT_MWAN_VISIBILITY_P = 'Application Visibility Price';
    public static final String PRODUCT_MWAN_ACCELERATION_P = 'Application Acceleration Price';
    public static final String PRODUCT_MWAN_WAAS_MANAG_P = 'WAAS.Management Price';
    public static final String PRODUCT_MWAN_WAAS_FEATU_P = 'WAAS.Additional Features Price';
    public static final String PRODUCT_MWAN_INTERET_CPE_P = 'Internet CPE Price';
    public static final String PRODUCT_MWAN_INTERNET_BW_P = 'Internet BW Price';
    // FAR 21/06/2016 - Add Internet Access Price
    public static final String PRODUCT_MWAN_INTERNET_ACC_P = 'Internet Access Price';
    public static final String PRODUCT_MWAN_INET_MANAG_P = 'INET.Management Price';
    public static final String PRODUCT_MWAN_INET_ADDIT_P = 'INET.Additional Features Price';
    /* HCS */
    public static final String PRODUCT_HCS_VIDEO = 'HCS Video';
    public static final String PRODUCT_HCS_TOIP = 'HCS ToIP';
    public static final String PRODUCT_HCS_LICENSE = 'HCS License';
    public static final String PRODUCT_HCS_OPEN_ORDER = 'HCS Open order';
    public static final String PRODUCT_HCS_LOCATIONS = 'HCS Locations';
    public static final String PRODUCT_HCS_USERS = 'HCS Users';
    public static final String PRODUCT_CISCO_HCS_SERVICE = 'Cisco HCS Service';
    public static final String PRODUCT_CISCO_HCS_AUTOATTENDANT = 'HCS AutoAttendant VM';
    public static final String PRODUCT_CISCO_HCS_CALL_PICKUP = 'HCS Call Pick-Up';
    public static final String PRODUCT_CISCO_HCS_HUNT_GROUP = 'HCS Hunt Group';
    /* UC MOBILE */
    public static final String PRODUCT_UC_LICENSE = 'UC Mobile Licenses';
    public static final String PRODUCT_UC_USER = 'UC Mobile Users';
    public static final String PRODUCT_UC_OPEN_ORDER = 'UC Mobile Open Order';
    public static final String PRODUCT_UC_SERVICE = 'UC Mobile Service';
    /* MSIP */
    public static final String PRODUCT_MSIP_CCA = 'mSIP Site CCA';
    public static final String PRODUCT_MSIP_ADMIN = 'mSIP Site ADMIN';
    public static final String PRODUCT_MSIP_HOSTED_UC = 'mSIP Site Hosted UC';
    public static final String PRODUCT_MSIP_NO_TRUNK = 'mSIP Site No Trunk';
    public static final String PRODUCT_MSIP_TRUNK = 'mSIP Site Trunk';
    public static final String PRODUCT_MSIP_DVCE = 'DVCE';
    public static final String PRODUCT_MSIP_DVCE_PRICE = 'DVCE.Type.Price';
    public static final String PRODUCT_MSIP_UPBX = 'UPBX';
    public static final String PRODUCT_MSIP_SPECIAL_NUMBER = 'Special Number';
    public static final String PRODUCT_MSIP_PNP = 'PNP';
    public static final String PRODUCT_MSIP_DDI_RESALE = 'DDI Resale';
    public static final String PRODUCT_MSIP_DDI_STANDARD = 'DDI Standard';
    //INI - everis - 21/12/2016 - DSS218 - new automatic notifications in Infinity for mSIP service
    public static final String PRODUCT_MSIP_BUNDLE = 'Bundle';   
    public static final String PRODUCT_MSIP_FLAT_RATE = 'FLAT RATE';
    public static final String PRODUCT_MSIP_DESCUENTO_POR_VOLUMEN = 'Descuento Por Volumen';
    //FIN - everis - 21/12/2016 - DSS218 - new automatic notifications in Infinity for mSIP service

    /* Otros */
    public static final String PRODUCT_INTERNATIONAL_NUMBER = 'International Number';
    public static final String PRODUCT_DOMESTIC_NUMBER = 'Domestic Number';
    public static final String PRODUCT_DDI_STANDARD = 'DDI Standard';
    public static final String PRODUCT_DDI_RESALE = 'DDI Resale';
    
    public static final String PRODUCT_WEBEX = 'WebEx';
    
    /* Catalog */
    
    public static final String CATALOG_TGSOL_CATALOG = 'TGSol Catalog';
    
    /* Assignment rules */
    
    public static final String ASSIGNMENT_RULE_BI = 'BI_Redireccionamiento grupo de trabajo';
   
    
    /* Notifications */
    
    public static final String SEND_ALL_NOTIFICATIONS = 'All Notifications';
    public static final String SEND_BILLING_INQUIRY = 'Billing Inquiry';
    public static final String SEND_CHANGE = 'Change';
    public static final String SEND_COMPLAINT = 'Complaint';
    public static final String SEND_INCIDENT = 'Incident';
    public static final String SEND_ORDER_MANAGMENT_CASE = 'Order Management Case';
    public static final String SEND_QUERY = 'Query';
    
        
    /* Site */
    
    public static final String SITE_DEFAULT = 'No Site';
    
    
    /* Reponse UDO */
    
    public static final String UDO_CREATED = 'CREATED';
    public static final String UDO_CREATE = 'CREATE';
    public static final String UDO_OK = 'OK';
    
    
    /* Status machine */
    
    //Profile values
    public static final String STATUS_MACHINE_AU_PROFILE = 'TGS Customer Community Plus';
    public static final String STATUS_MACHINE_PROFILE_TGS_ORDER_HANDLING = 'TGS Order Handling';
    //Prior status field values
    public static final String STATUS_MACHINE_PRIOR_STATUS_RESOLVED = 'Resolved';
    //Status field values
    public static final String STATUS_MACHINE_STATUS_CLOSED = 'Closed';
    //Prior status reason field values
    public static final String STATUS_MACHINE_PRIOR_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE = 'In Cancellation - Customer Acceptance';
    //Status reason field values
    public static final String STATUS_MACHINE_STATUS_REASON_IN_CANCELLATION = 'In Cancellation';
    
    /* Profiles */
    public static final String PROFILE_DEFAULT_TICKET_MASTER = 'Default Ticket Master';
    public static final String PROFILE_DEFAULT_TICKET_USER = 'Default Ticket User';
    public static final String PROFILE_ORDER_HANDLING = 'Order handling';
    public static final String PROFILE_PROVISION = 'Provision';
    public static final String PROFILE_CUSTOMER_CARE =  'Customer care';
    public static final String PROFILE_TGS_PERFIL_1 = 'TGS_Perfil 1';
    public static final String PROFILE_TGS_ORDER_HANDLING = 'TGS Order Handling';
    public static final String PROFILE_TGS_PROVISION = 'TGS Provision';
    // FAR 06/10/2016
    public static final String PROFILE_BI_O4_TGS_STANDARD = 'BI_O4_TGS_Standard';
    public static final String PROFILE_BI_O4_TNA_STANDARD_TIWS = 'BI_O4_TNA_Standard_TIWS';
    
    /* Others */
    public static final String NONE = '--None--';
    public static boolean firstRunAttribute = true;
    public static boolean firstRunLookup = true;
    public static boolean firstRunBilling = true;
    public static boolean isQueueableContext = false;
    
    /* Global Mobility */
    public static final String GLOBAL_MOBILITY_FOR_MNCS = 'Global Mobility for MNCs';
    
    /* Ferrovial Services */
    /* 20-07-2017 -  Added Ferrovial Services -  Manuel Ochoa */
    public static final List<String> FERROVIAL_SERVICES = new list<String> {'Regional' , 'Local' , 'Corporativo','Data Center','RNAS WAN','Herramienta Trazas y Auditoría','Aceleración','PT','Branch','Site Survey LAN','SFP','RNAS LAN','LAN type','Site Survey WLAN','RNAS WLAN','IAP','Temporal','Sucursal','DDI','RNAS Fixed Voice','Terminal','PRI Backup','Related DDI'};
       
    /* Ci Action */
    public static final String CI_ACTION_ADD = 'Add';
    public static final String CI_ACTION_REMOVE = 'Remove'; // PCP 07/07/2016
    public static final String CI_ACTION_CHANGE = 'Change'; //JCT 15/07/2016
    public static final String CI_ACTION_NONE   = 'None';
    
    //INI - everis - 21/12/2016 - DSS218 - new automatic notifications in Infinity for mSIP service
    public static final String EMAIL_NOTIFICATION_MSIP = 'TGS_EmailNotificationMsip';
    public static final String MSIP_TRUNKING_SITE = 'Managed SIP Trunking Site';
    
    /* Account Team Role Rols */
    public static final String ROLE_GPM = 'GPM';
    public static final String ROLE_PM_IT = 'PM IT';
    public static final String ROLE_SERVICE_ARCHITECT = 'Service Architect';
    public static final String ROLE_IP_ITFS = 'SIP_ITFS';
    public static final String ROLE_NEGOCIO = 'Negocio';
    //FIN - everis - 21/12/2016 - DSS218 - new automatic notifications in Infinity for mSIP service

    //INI - everis - 09/01/2017 - DSS224
    public static final String HCS_ATT_NAME_MACADDRESS = 'MAC Address';
    public static final String HCS_ATT_NAME_MAC_ADDRESS = 'MAC_Address';
    //FIN - everis - 09/01/2017 - DSS224

    //INI - everis - 10/03/2017 - GME1501
	public static final String ENTERPRISE_MANAGED_MOBILITY = 'Enterprise Managed Mobility';    
    public static final String GLOBAL_MANAGEMENT_ELITE = 'Global Management Elite (GME)';
    public static final String MOBILE_LINE = 'Mobile Line';
    public static final String BILLABLE_MOBILE_LINE = 'Billable Mobile Line';
    public static final String MONTHLY = 'Monthly';
    public static final String ATTACHMENT_DESCRIPTION = 'Completed updates details';
    public static final String ATTACHMENT_NAME = 'Billing_Mobile_Line_order_';
    public static final String ATTACHMENT_EXTENSION = '.csv';
    public static final String BILLING_MOBILE_LINE_OIA = 'Q';
    public static final String TYPE_DISCONNECTED = 'Disconnected';
    public static final String PRODUCT = 'Product';
    public static final String TGSOL_CATALOG = 'TGSOL Catalog';
    public static final String IN_ORDER = 'InOrder';
    public static final String BILLING_MOBILE_LINE_FAMILY = 'Billable Mobile Line Family';
    public static final String CURRENCY_EUR = 'EUR';   
    public static final String COUNTRY_SPAIN = 'Spain';   
    //FIN - everis - 10/03/2017 - GME1501
    //
    /* RoD Services */
    public static final String ROD_MWAN = 'mWAN';
    public static final String ROD_MWAN_INTERNET = 'mWAN - Internet';
    public static final String ROD_MSIP_TRUNK = 'mSIP Site TRUNK';
    public static final String ROD_MSIP_NO_TRUNK = 'mSIP Site NO TRUNK';
    public static final String ROD_BUNDLES_WITH_POOLING = 'Bundles With Pooling';
}