/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Test class for BI_O4_NewOpportunityQualify
    Test Class:    BI_O4_NewOpportunityQualify_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Fernando Arteaga        Initial version
    17/10/2016              Fernando Arteaga        Changes made to increase code coverage and cover new methods in BI_O4_Utils
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_NewOpportunityQualify_TEST {

	static{
        BI_TestUtils.throw_exception = false;
  	}
	
	@isTest static void test_method_one() {

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
	        CloseDate = Date.today(),
	        StageName = Label.BI_F5DefSolucion,
	       // AccountId = lst_acc[0].Id,
	        BI_Ciclo_ventas__c = Label.BI_Completo,
	        BI_Opportunity_Type__c = 'Alta',
	        BI_Country__c = 'Argentina'
	        );
		Insert opp;
		
		Test.startTest();
		
		ApexPages.StandardController stdControl = new ApexPages.StandardController(opp);
		BI_O4_NewOpportunityQualify controller = new BI_O4_NewOpportunityQualify(stdControl);
		controller.newOpportunityQualify();

		BI_O4_Opportunity_Qualify__c oq = new BI_O4_Opportunity_Qualify__c(BI_O4_Opportunity__c = opp.Id);
		insert oq;

		controller = new BI_O4_NewOpportunityQualify(stdControl);
		controller.newOpportunityQualify();

		Test.stopTest();
	}
}