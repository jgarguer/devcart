@isTest
private class BI_LoadAllKindQueable_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Accenture
        Description:   Test class to manage the code coverage from BI_LoadAllKindQueueable
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        26/04/2017                      Guillermo Muñoz             Initial version
        18/08/2017                      Guillermo Muñoz             Changed to use new BI_MigrationHelper functionality
        20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c   
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static{
		BI_TestUtils.throw_exception = false;
	}
	
	@isTest static void insert_TEST() {
		
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        Map <Integer, BI_bypass__C> mapa =BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
        BI_MigrationHelper.skipAllTriggers();

		List<Account> lst_acc = new List <Account>();

        Account acc1 = new Account(
        	Name = 'test1',
            Sector__c = 'Private',
            BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
            BI_Activo__c = Label.BI_Si,
            BI_Country__c = Label.BI_Argentina,
            BI_Segment__c = 'test',
            BI_Subsegment_Regional__c = 'test',
            BI_Territory__c = 'test'
        );
        lst_acc.add(acc1);

         Account acc2 = new Account(
        	Name = 'test2',
            Sector__c = 'Private',
            BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
            BI_Activo__c = Label.BI_Si,
            BI_Country__c = Label.BI_Argentina,
            BI_Subsegment_Local__c = 'Mayoristas',
            BI_Segment__c = 'test',
            BI_Subsegment_Regional__c = 'test',
            BI_Territory__c = 'test'
        );
        lst_acc.add(acc2);

        Test.startTest();
        System.enqueueJob(new BI_LoadAllKindQueable(lst_acc, 1, 'INSERT', 'Test insert'));
        Test.stopTest();

		BI_MigrationHelper.disableBypass(mapa);
        BI_MigrationHelper.cleanSkippedTriggers();
	}

	@isTest static void update_TEST() {
	
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        Map <Integer, BI_bypass__C> mapa =BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
        BI_MigrationHelper.skipAllTriggers();

		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        lst_acc[0].BI_Country__c = Label.BI_Peru;

        Test.startTest();
        System.enqueueJob(new BI_LoadAllKindQueable(lst_acc, 1, 'Update', 'Test Update'));
        Test.stopTest();

        System.assertEquals([SELECT BI_Country__c FROM Account][0].BI_Country__c, Label.BI_Peru);

		BI_MigrationHelper.disableBypass(mapa);
        BI_MigrationHelper.cleanSkippedTriggers();
	}

	@isTest static void delete_TEST() {
	
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        Map <Integer, BI_bypass__C> mapa =BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

		Test.startTest();
        System.enqueueJob(new BI_LoadAllKindQueable(lst_acc, 1, 'Delete', 'Test Delete'));
        Test.stopTest();

        System.assertEquals([SELECT Id FROM Account].isEmpty(),true);

        BI_MigrationHelper.disableBypass(mapa);
	}

	@isTest static void undelete_TEST() {
	
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        Map <Integer, BI_bypass__C> mapa =BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        delete lst_acc;

		Test.startTest();
        System.enqueueJob(new BI_LoadAllKindQueable(lst_acc, 1, 'Undelete', 'Test Undelete'));
        Test.stopTest();

        System.assertEquals([SELECT Id FROM Account].isEmpty(),false);

        BI_MigrationHelper.disableBypass(mapa);
	}

	@isTest static void exceptions_TEST() {
	
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        Map <Integer, BI_bypass__C> mapa =BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        try{

            BI_LoadAllKindQueable obj = new BI_LoadAllKindQueable(lst_acc, 1, 'Update', null);
        }catch(BI_Exception exc){

            System.assertEquals(exc.getMessage(), 'The arguments cannot be null');
        }

        try{

            BI_LoadAllKindQueable obj = new BI_LoadAllKindQueable(lst_acc, 1, 'bad DML type', 'Test');
        }catch(BI_Exception exc){
            
            System.assertEquals(exc.getMessage(), 'Invalid DML type');
        }

        try{

            BI_LoadAllKindQueable obj = new BI_LoadAllKindQueable(lst_acc, 0, 'Update', 'Test');
        }catch(BI_Exception exc){
            
            System.assertEquals(exc.getMessage(), 'The batch size cannot be 0 or less than 0');
        }

        BI_TestUtils.throw_exception = true;

        Test.startTest();
        System.enqueueJob(new BI_LoadAllKindQueable(lst_acc, 1, 'Update', 'Test exception'));
        Test.stopTest();

        System.assertEquals([SELECT BI_Asunto__c FROM BI_Log__c WHERE BI_Asunto__c Like '%Test exception'].size(), 2);

		BI_MigrationHelper.disableBypass(mapa);
	}
	
	
	
}