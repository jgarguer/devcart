/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for Lightning Component CWP_InvoicesComponent
    Test Class:    CWP_InvoicesTreeViewControllerTest
    
    History:
     
    <Date>                  <Author>                <Change Description>
    21/04/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class CWP_InvoicesTreeViewController {

    private static Map <String, String> monthNames = new Map <String, String> {'01'=>'Enero', '02'=>'Febrero', '03'=>'Marzo', '04'=>'Abril', '05'=>'Mayo', '06'=>'Junio', 
                                                                         '07'=>'Julio', '08'=>'Agosto', '09'=>'Septiembre', '10'=>'Octubre', '11'=>'Noviembre', '12'=>'Diciembre'};

    public static list<string> initialLevelsToQuery = new list<string>{'Level0', 'Level1', 'Level2', 'Level3'};
	
	@AuraEnabled
	public static Account init() {
		User currentUser = new User();
		Boolean isPortalUser = false;
		Account accountRecord = new Account();
		Id accId;
		String selectedDisplayType;
		
        try{
            User u = [select LocaleSidKey, AccountId from User where id = :UserInfo.getUserId()];
            currentUser = u;
            isPortalUser = u.AccountId != null;

            if (u.AccountId != null) {
                accId = BI_AccountHelper.getCurrentAccountId();
            }

            if (accId != null) {
                accountRecord = [select Name, CurrencyIsoCode from Account where id = :accId];
            }
            
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_InvoicesTreeViewController.BI_InvoicesTreeViewController', 'BI_EN', Exc, 'Class');
        }

        generateDisplayTypes();
        selectedDisplayType = 'FCON';

        fetchInvoices(accId, selectedDisplayType);
        return accountRecord;
    }
	
	@AuraEnabled
    public static List<SelectItem> generateDisplayTypes() {  	
        List<SelectItem> displayTypes = new List<SelectItem>();
        displayTypes.add(new SelectItem('FCIC', 'Recurrente'));
        displayTypes.add(new SelectItem('FCON', 'No recurrente'));
        return displayTypes;
    }
	
	@AuraEnabled
	public static String fetchInvoices(Id accId, String selectedDisplayType){
		
            List<Level0> topLevelItems = new List<Level0>();

            Map<String, Level0> level0Map = new Map<String, Level0>();
            Map<String, Level1> level1Map = new Map<String, Level1>();
            Map<String, Level2> level2Map = new Map<String, Level2>();
            Map<String, Level3> level3Map = new Map<String, Level3>();

            Map<String, Level0> level0DateMap = new Map<String, Level0>();
            Map<String, Level1> level1DateMap = new Map<String, Level1>();
            Map<String, Level2> level2DateMap = new Map<String, Level2>();
            Map<String, Level3> level3DateMap = new Map<String, Level3>();

            List<BI_Cobranza__c> invoiceList = [Select  Id, Aggregation_Level__c, BI_YearMonth__c, 
                                                    BI_ID_Fiscal__c, BI_Rama__c, BI_Familia_1__c, 
                                                    BI_Familia_2__c, BI_Tipo_Facturacion__c, BI_FCIC_Amount__c, BI_FCON_Amount__c,
                                                    BI_Codigo_concepto__c, BI_Descripcion_concepto__c,
                                                    Aggregation_Type__c
                                             from   BI_Cobranza__c 
                                            where   BI_Clientes__c=:accId and Aggregation_Level__c in:initialLevelsToQuery and Active__c=true and 
                                                    Aggregation_Type__c IN ('1-3M', '4-13M') and
                                                    BI_Tipo_Facturacion__c = :selectedDisplayType
                                         order by   BI_YearMonth__c Asc, BI_Rama__c Asc, BI_Familia_1__c Asc, BI_Familia_2__c Asc, BI_Tipo_Facturacion__c Asc LIMIT 1000];
			
        
            for(BI_Cobranza__c invoiceRecord : invoiceList){
    
                String invoiceDate = getDateKey(invoiceRecord.BI_YearMonth__c);
                String invoiceDatem1 = getPreviousMonthKey(invoiceRecord.BI_YearMonth__c, -1);
                String invoiceDatem2 = getPreviousMonthKey(invoiceRecord.BI_YearMonth__c, -2);
                String invoiceRama = invoiceRecord.BI_Rama__c;
                String invoiceFamilia1 = invoiceRecord.BI_Familia_1__c;
                String invoiceFamilia2 = invoiceRecord.BI_Familia_2__c;

                if(invoiceRecord.Aggregation_Level__c=='Level0'){

                    Level0 l = new Level0(invoiceRecord, invoiceDate, level0DateMap.get(invoiceDatem1), level0DateMap.get(invoiceDatem2));
                    level0Map.put(invoiceDate, l);
                    level0DateMap.put(invoiceRecord.BI_YearMonth__c, l);
                }

                if(invoiceRecord.Aggregation_Level__c=='Level1'){
                    Level0 l = level0Map.get(invoiceDate);

                    if (l != null) {
                        Level1 lOne = new Level1(invoiceRecord, invoiceRama, level1DateMap.get(invoiceRama+invoiceDatem1), level1DateMap.get(invoiceRama+invoiceDatem2));
                        l.addChild(lOne);
                        level1Map.put(invoiceDate+invoiceRama, lOne);
                        level1DateMap.put(invoiceRama+invoiceRecord.BI_YearMonth__c, lOne);
                    }
                }

                if(invoiceRecord.Aggregation_Level__c=='Level2'){
                    Level1 l = level1Map.get(invoiceDate+invoiceRama);

                    if (l != null) {
                        Level2 lTwo = new Level2(invoiceRecord, invoiceFamilia1, level2DateMap.get(invoiceRama+invoiceFamilia1+invoiceDatem1), level2DateMap.get(invoiceRama+invoiceFamilia1+invoiceDatem2));
                        l.addChild(lTwo);
                        level2Map.put(invoiceDate+invoiceRama+invoiceFamilia1, lTwo);
                        level2DateMap.put(invoiceRama+invoiceFamilia1+invoiceRecord.BI_YearMonth__c, lTwo);
                    }
                }

                if(invoiceRecord.Aggregation_Level__c=='Level3'){
                    Level2 l = level2Map.get(invoiceDate+invoiceRama+invoiceFamilia1);

                    if (l != null) {
                        Level3 lThree = new Level3(invoiceRecord, invoiceFamilia2, level3DateMap.get(invoiceRama+invoiceFamilia1+invoiceFamilia2+invoiceDatem1), level3DateMap.get(invoiceRama+invoiceFamilia1+invoiceFamilia2+invoiceDatem2));
                        l.addChild(lThree);
                        level3Map.put(invoiceDate+invoiceRama+invoiceFamilia1+invoiceFamilia2, lThree);
                        level3DateMap.put(invoiceRama+invoiceFamilia1+invoiceFamilia2+invoiceRecord.BI_YearMonth__c, lThree);
                    }
                }                              
            }  

            
            
         	for (String level0ListKey : level0Map.keySet()){
            	topLevelItems.add(level0Map.get(level0ListKey)); 
	        }
            
            topLevelItems.sort();

            return JSON.serialize(topLevelItems);                                    
    }
	
	private static String getDateKey(String DWHDate){
        try {
            return(monthNames.get(DWHDate.substring(4,6)) + ' - ' + DWHDate.substring(0,4));

        }catch (exception Exc){
           BI_LogHelper.generate_BILog('CWP_InvoicesTreeViewController.getDateKey', 'BI_EN', Exc, 'Class');
           return null;
        } 
    }
    
    @AuraEnabled
     public static String getPreviousMonthKey(string dateString, integer months) {
        try {
            Date d = Date.newinstance(Integer.valueOf(dateString.substring(0,4)), Integer.valueOf(dateString.substring(4,6)), 1);
            d = d.addMonths(months);

            String sMonth;    
            if (d.month() > 9) {
                sMonth = String.valueOf(d.month());
            } else {
                sMonth = '0'+String.valueOf(d.month());
            }
            return ''+d.year()+sMonth;

        }catch (exception Exc){
           BI_LogHelper.generate_BILog('CWP_InvoicesTreeViewController.getDateKey', 'BI_EN', Exc, 'Class');
           return null;
        } 
    }
	
    
    @AuraEnabled
    public static List<Level4> getDetails(string accountID, string invoiceDate, string invoiceRama, string invoiceFamilia1, string invoiceFamilia2, string tipoFacturacion) {
        List<Level4> returnList = new List<Level4>();


        list<string> tList = new list<string>();
        if (tipoFacturacion != null) {
            tList.add(tipoFacturacion);
        }

        string prev1 = CWP_InvoicesTreeViewController.getPreviousMonthKey(invoiceDate, -1);
        string prev2 = CWP_InvoicesTreeViewController.getPreviousMonthKey(invoiceDate, -2);

        List<BI_Cobranza__c> tempList = [Select     Aggregation_Level__c, BI_YearMonth__c, 
                                                    BI_ID_Fiscal__c, BI_Rama__c, BI_Familia_1__c, 
                                                    BI_Familia_2__c, BI_FCIC_Amount__c, BI_FCON_Amount__c,
                                                    BI_Codigo_concepto__c, BI_Descripcion_concepto__c,
                                                    BI_Currency_ISO__c
                                             from   BI_Cobranza__c 
                                            where   BI_Clientes__c=:accountID and Aggregation_Level__c = 'Level4' and Active__c=true and 
                                                    BI_Tipo_Facturacion__c IN :tList and
                                                    Aggregation_Type__c IN ('1-3M', '4-13M') and
                                                    BI_Rama__c = :invoiceRama and
                                                    BI_Familia_1__c = :invoiceFamilia1 and
                                                    BI_Familia_2__c = :invoiceFamilia2 and
                                                    (BI_YearMonth__c = :invoiceDate or BI_YearMonth__c = :prev1 or  BI_YearMonth__c = :prev2)
                                         order by   Aggregation_Level__c Asc, BI_YearMonth__c Asc, BI_Rama__c Asc, BI_Familia_1__c Asc, BI_Familia_2__c Asc, BI_Tipo_Facturacion__c Asc LIMIT 150];
                                         

        map<string, Level4> levelMap = new map<string, Level4>();

        for (BI_Cobranza__c bic : tempList) {
            Level4 lFour = new Level4(bic, '', null, null);
            levelMap.put(bic.BI_Codigo_concepto__c+bic.BI_YearMonth__c, lFour);
        }

        for (BI_Cobranza__c bic : tempList) {
            if (bic.BI_YearMonth__c == invoiceDate) {
                returnList.add(new Level4(bic, '', levelMap.get(bic.BI_Codigo_concepto__c+prev1), levelMap.get(bic.BI_Codigo_concepto__c+prev2)));    
            }
        }

        return returnList;
    }
    
	 public virtual class LineItem {
	 		@AuraEnabled
	        public List<BI_Cobranza__c> records;
	        @AuraEnabled
	        public BI_Cobranza__c record;
	        @AuraEnabled
	        public String levelName;
	        
	        public LineItem(){ 
	        }
	    }

   
    public virtual class Level0 extends LineItem implements Comparable {
    	@auraEnabled
        public List<Level1> children;
        @auraEnabled
        public Decimal totalFCIC;
        @auraEnabled
        public Decimal totalFCON;
        @auraEnabled
        public Decimal totalBOTH;
        @auraEnabled
        public Level0 minus1Amount;
        @auraEnabled
        public Level0 minus2Amount;

        public Level0(BI_Cobranza__c r, String lt, Level0 m1, Level0 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            levelName = lt;
            children = new List<Level1>();
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
            
        }
        
        public Level0(){
        }

        public void addChild(Level1 l) {
            children.add(l);
        }
        
        public integer compareTo(Object level0Comparable) {
        	CWP_InvoicesTreeViewController.Level0 level0ToCompare = (CWP_InvoicesTreeViewController.Level0) level0Comparable;
        	if (record.BI_YearMonth__c < level0ToCompare.record.BI_YearMonth__c) {
        		return 1;
        	} else {
        		return -1;
        	}
        }
    }

    public virtual class Level1 extends Level0  {
    	@auraEnabled
        public List<Level2> children;
        @auraEnabled
        public Level1 minus1Amount;
        @auraEnabled
        public Level1 minus2Amount;
         @auraEnabled
        public Decimal totalFCIC;
        @auraEnabled
        public Decimal totalFCON;
        @auraEnabled
        public Decimal totalBOTH;
		
		public Level1(BI_Cobranza__c r, String lt, Level1 m1, Level1 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            levelName = lt;
            children = new List<Level2>();
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
        } 
        
        public Level1(){
        }

        public void addChild(Level2 l) {
            children.add(l);
        }
    }

    public virtual class Level2 extends Level1  {
    	@auraEnabled
        public List<Level3> children;
        @auraEnabled
        public Level2 minus1Amount;
        @auraEnabled
        public Level2 minus2Amount;
        public Decimal totalFCIC;
        @auraEnabled
        public Decimal totalFCON;
        @auraEnabled
        public Decimal totalBOTH;

        public Level2(BI_Cobranza__c r, String lt, Level2 m1, Level2 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            levelName = lt;
            children = new List<Level3>();
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
        }
        
        public Level2(){
        }

        public void addChild(Level3 l) {
            children.add(l);
        }
    }

    public virtual class Level3 extends Level2  {
    	@auraEnabled
        public List<Level4> children;
        @auraEnabled
        public String codigoConcepto;
        @auraEnabled
        public String descripcionConcepto;
        @auraEnabled
        public String yearMonth;
        @auraEnabled
        public String rama;
        @auraEnabled
        public String famOne;
        @auraEnabled
        public String famTwo;
        @auraEnabled
        public String aggregationType;
        @auraEnabled
        public Level3 minus1Amount;
        @auraEnabled
        public Level3 minus2Amount;
        public Decimal totalFCIC;
        @auraEnabled
        public Decimal totalFCON;
        @auraEnabled
        public Decimal totalBOTH;
        @AuraEnabled
	    public String levelName;
	    @AuraEnabled
	    public BI_Cobranza__c record;

        public Level3(BI_Cobranza__c r, String lt, Level3 m1, Level3 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            codigoConcepto = r.BI_Codigo_concepto__c;
            descripcionConcepto = r.BI_Descripcion_concepto__c;
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            children = new List<Level4>();
            yearMonth = r.BI_YearMonth__c;
            rama = r.BI_Rama__c;
            famOne = r.BI_Familia_1__c;
            famTwo = r.BI_Familia_2__c;
            aggregationType = r.Aggregation_Type__c;
            minus1Amount = m1;
            minus2Amount = m2;

            levelName = lt;
        }
        
        public Level3(){
        }
    }

    public class Level4 extends Level3  {
    	@auraEnabled
        public Level4 minus1Amount;
        @auraEnabled
        public Level4 minus2Amount;
        @auraEnabled
        public String currencyType;
        @auraEnabled
        public Decimal totalFCIC;
        @auraEnabled
        public Decimal totalFCON;
        @auraEnabled
        public Decimal totalBOTH;
        @AuraEnabled
	    public String levelName;
	    @AuraEnabled
	    public BI_Cobranza__c record;

        public Level4(BI_Cobranza__c r, String lt, Level4 m1, Level4 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            codigoConcepto = r.BI_Codigo_concepto__c;
            descripcionConcepto = r.BI_Descripcion_concepto__c;
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
            currencyType = r.BI_Currency_ISO__c;

            levelName = lt;
        }
    }
   
}