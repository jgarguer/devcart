public class BIIN_UNICA_Utils
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González Beltrán
    Company:       HPE
    Description:   Utils for UNICA integrations
    
    <Date>             <Author>                        <Change Description>
    20/05/2016         José Luis González Beltrán      Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public static String translateFromSalesForce(String tableType, String value)
  {
    String result = null;
    Map <String, List<BIIN_Tabla_Correspondencia__c>> mapaTraducciones = BIIN_Helper.map_tc;

    if(mapaTraducciones.containsKey(tableType))
    {
      for(BIIN_Tabla_Correspondencia__c tc : mapaTraducciones.get(tableType))
      {
        if(tc.BIIN_Valor_Salesforce__c == value)
        {
          result = tc.BIIN_Valor_Remedy__c;
          break;
        }
      }
    }
    return result;
  }

  public static String translateFromSalesForceToID(String tableType, String value)
  {
    String result = null;
    Map <String, List<BIIN_Tabla_Correspondencia__c>> mapaTraducciones = BIIN_Helper.map_tc;

    if(mapaTraducciones.containsKey(tableType))
    {
      for(BIIN_Tabla_Correspondencia__c tc : mapaTraducciones.get(tableType))
      {
        if(tc.BIIN_Valor_Salesforce__c == value)
        {
          result = tc.BIIN_RoD_ID__c;
          break;
        }
      }
    }
    return result;
  }

  public static String translateFromRemedyIDToRemedyValue(String tableType, Integer remedyId)
  {
    String result = null;
    Map <String, List<BIIN_Tabla_Correspondencia__c>> mapaTraducciones = BIIN_Helper.map_tc;

    if(mapaTraducciones.containsKey(tableType))
    {
      for(BIIN_Tabla_Correspondencia__c tc : mapaTraducciones.get(tableType))
      {
        if(tc.BIIN_RoD_ID__c == String.valueOf(remedyID))
        {
          result = tc.BIIN_Valor_Remedy__c;
          break;
        }
      }
    }
    return result;
  }

 public static String translateFromRemedyValueToRemedyID(String tableType, String remedyValue)
  {
    String result = null;
    Map <String, List<BIIN_Tabla_Correspondencia__c>> mapaTraducciones = BIIN_Helper.map_tc;

    if(mapaTraducciones.containsKey(tableType))
    {
      for(BIIN_Tabla_Correspondencia__c tc : mapaTraducciones.get(tableType))
      {
        if(tc.BIIN_Valor_Remedy__c == remedyValue)
        {
          result = tc.BIIN_RoD_ID__c;
          break;
        }
      }
    }
    return result;
  }

  public static String translateFromRemedy(String tableType, String remedyValue)
  {
    String result = null;
    Map <String, List<BIIN_Tabla_Correspondencia__c>> mapaTraducciones = BIIN_Helper.map_tc;

    if(mapaTraducciones.containsKey(tableType))
    {
      for(BIIN_Tabla_Correspondencia__c tc : mapaTraducciones.get(tableType))
      {
        if(tc.BIIN_Valor_Remedy__c == remedyValue)
        {
          result = tc.BIIN_Valor_Salesforce__c;
          break;
        }
      }
    }
    return result;
  }

  public static String translateFromRemedyByID(String tableType, Integer remedyID)
  {
    String result = null;
    Map <String, List<BIIN_Tabla_Correspondencia__c>> mapaTraducciones = BIIN_Helper.map_tc;

    if(mapaTraducciones.containsKey(tableType))
    {
      for(BIIN_Tabla_Correspondencia__c tc : mapaTraducciones.get(tableType))
      {
        if(tc.BIIN_RoD_ID__c == String.valueOf(remedyID))
        {
          result = tc.BIIN_Valor_Salesforce__c;
          break;
        }
      }
    }
    return result;
  }

  public static Map<String, String> additionalDataToMap(List<BIIN_UNICA_Pojos.KeyValueType> additionalData)
  {
    Map<String, String> mapa = new Map<String, String>();
    for(BIIN_UNICA_Pojos.KeyValueType keyValueType : additionalData)
    {
      mapa.put(keyValueType.key, keyValueType.value);
    }
    return mapa;
  }

  public static String addParameterAdditional(String url, String name, String value)
  {
    return addParameter(url, 'ad_' + name, value);
  }

  public static String addParameter(String url, String name, String value)
  {
    if (url == null || name == null || value == null)
    {
      return url;
    }

    String link = '&';
    if (url.indexOf('?') == -1)
    {
      link = '?';
    }

    // Encoding
    value= EncodingUtil.urlEncode(value, 'UTF-8');

    url = url + link + name + '=' + value;

    return url;
  }

  public static String dateTimeToSeconds(DateTime fecha)
  {
    return fecha != null ? String.valueOf(fecha.getTime()/1000) : null;
  }

  public static BIIN_UNICA_Pojos.TicketStatusType stringToEnumValueForTicketStatusType(String value)
  {
    if (value != null)
    {
      // Reserved words
      value.replaceAll('NEW', 'NEW_STATUS');

      for (BIIN_UNICA_Pojos.TicketStatusType element : BIIN_UNICA_Pojos.TicketStatusType.values())
      {
        if (element.name() == value) 
        {
          return element;
        }
      }
    }
    return BIIN_UNICA_Pojos.TicketStatusType.new_status;
  }

  public static BIIN_UNICA_Pojos.ContactStatusType stringToEnumValueForContactStatusType(String value)
  {
    if (value != null)
    {
      for (BIIN_UNICA_Pojos.ContactStatusType element : BIIN_UNICA_Pojos.ContactStatusType.values())
      {
        if (element.name() == value) 
        {
          return element;
        }
      }
    }
    return BIIN_UNICA_Pojos.ContactStatusType.active;
  }
}