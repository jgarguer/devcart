@isTest(seeAllData = false)

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        José María Martín
Company:       Deloitte
Description:   Test method to manage the code coverage for BIIN_Actualizar_Ticket_WS

<Date>               <Author>                               <Change Description>
11/2015              José María Martín                      Initial Version
17/06/2016           José Luis González Beltrán             Adapt test to UNICA modifications
06/02/2017           Pedro Párraga                          Increase coverage
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class BIIN_Actualizar_Ticket_TEST 
{
    static testMethod void test_BIIN_Actualizar_Ticket_TEST() 
    {
        //BI_TestUtils.throw_exception=false;

        // Add test users
        List<User> users=new List<User>();
        User actualUser = BIIN_Test_Data_Load.createUSer('BI_Standard_PER', 'Asesor');
        User bi_admin = BIIN_Test_Data_Load.createUSer('BI_Administrator', 'Administrador del Sistema');
        users.add(actualUser);
        users.add(bi_admin);
        insert users;

        Account account;
        List<Case> lst_case = new List<Case>();
        Case caso;
        Case casoDos;
        System.runAs(bi_admin)
        {
            account=BIIN_Test_Data_Load.createAccount('Pachamama');
            insert account;

            caso = new Case(Status='1', Description='Descripcion test', accountId=account.id, OwnerId=ActualUser.id,BI_Status_Reason__c='27000');
            lst_case.add(caso);
            

            casoDos = new Case(Status='Assigned', Description='Descripcion test', accountId=account.id, OwnerId=ActualUser.id,BI_Status_Reason__c='27000');
            lst_case.add(casoDos);

            insert lst_case;

            
        }

        System.runAs(ActualUser)
        {
            BIIN_Tabla_Correspondencia__c tabla=new BIIN_Tabla_Correspondencia__c(Name='TestTabla',BIIN_RoD_ID__c='27000',BIIN_Valor_Salesforce__c='SFDC27000',BIIN_Valor_Remedy__c='ROD27000',BIIN_Tipo_Tabla__c = 'Subestado');
            insert tabla;

            Test.startTest(); 

            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ActualizarTicket'));  

            BIIN_Actualizar_Ticket_WS controller = new BIIN_Actualizar_Ticket_WS();
            controller.actualizarTicket('', caso, '');
            controller.actualizarTicket('', casoDos, '');

            Test.stopTest();
        }
    }
}