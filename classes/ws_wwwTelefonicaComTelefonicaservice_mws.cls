/**
* Avanxo Colombia
* @author 			OJCB
* Project:			Telefonica
* Description:		Clase tipo WebServiceMock del servicio NotificacionesSalesForceWSDL.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2015-08-27		OJCB      				Definicion de la clase implments WebServiceMock
*************************************************************************************************************/
@isTest
global class ws_wwwTelefonicaComTelefonicaservice_mws implements WebServiceMock
{

   global Map<String, Object> mapWSResponseBySOAPAction	= new Map<String, Object>();

    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		
		//ActivateOperation
		ws_wwwTelefonicaComTelefonicaservices.ActivateOperationResponse_element objActivateOperationResponse_element =new ws_wwwTelefonicaComTelefonicaservices.ActivateOperationResponse_element(); 
		objActivateOperationResponse_element.returnCode='returnCode';
		//ActivateOperation
		//SetUserSupervisionOIMResponse
		ws_wwwTelefonicaComTelefonicaservices.SetUserSupervisionOIMResponse_element objSetUserSupervisionOIMResponse_element =new ws_wwwTelefonicaComTelefonicaservices.SetUserSupervisionOIMResponse_element();
		objSetUserSupervisionOIMResponse_element.out='out';
		//SetUserSupervisionOIMResponse
		
		ws_wwwTelefonicaComTelefonicaservices.TechnicalViabilityResponse_element objTechnicalViabilityResponseElement = new ws_wwwTelefonicaComTelefonicaservices.TechnicalViabilityResponse_element();
		objTechnicalViabilityResponseElement.error='error';
        objTechnicalViabilityResponseElement.errorMessage='errorMessage';
        objTechnicalViabilityResponseElement.response='response';
        objTechnicalViabilityResponseElement.distributor='distributor';
        objTechnicalViabilityResponseElement.closet='closet';
        objTechnicalViabilityResponseElement.box='box';
        objTechnicalViabilityResponseElement.boxPair='boxPair';
        objTechnicalViabilityResponseElement.boxType='boxType';
        objTechnicalViabilityResponseElement.boxDescription='boxDescription';
        objTechnicalViabilityResponseElement.initialHouse='initialHouse';
        objTechnicalViabilityResponseElement.initialBox='initialBox';
        objTechnicalViabilityResponseElement.finalHouse='finalHouse';
        objTechnicalViabilityResponseElement.finalBox='finalBox';
        objTechnicalViabilityResponseElement.rangeZoneId='rangeZoneId';
        objTechnicalViabilityResponseElement.errorCode='errorCode';
        objTechnicalViabilityResponseElement.disponibilty='disponibilty';
        objTechnicalViabilityResponseElement.maxSpeed='maxSpeed';
        objTechnicalViabilityResponseElement.speedDown='speedDown';
        objTechnicalViabilityResponseElement.technology='technology';
        objTechnicalViabilityResponseElement.centralCode='centralCode';
        ws_wwwTelefonicaComTelefonicaservices.ProvisioningSVAResponse_element objProvisioningSVAResponse_element =new ws_wwwTelefonicaComTelefonicaservices.ProvisioningSVAResponse_element();
        objProvisioningSVAResponse_element.provider='provider';
        objProvisioningSVAResponse_element.clientDocument='clientDocument';
        objProvisioningSVAResponse_element.phoneNumber='phoneNumber';
        objProvisioningSVAResponse_element.responseCode='responseCode';
        objProvisioningSVAResponse_element.responseDescription='responseDescription';

        ws_wwwTelefonicaComTelefonicaservices.RemoveUserProfileOIMResponse_element objRemoveUserProfileOIMResponse_element = new ws_wwwTelefonicaComTelefonicaservices.RemoveUserProfileOIMResponse_element();
		objRemoveUserProfileOIMResponse_element.out='out';

		ws_wwwTelefonicaComTelefonicaservices.ConfirmCollectionResponse_element objConfirmCollectionResponse_element = new ws_wwwTelefonicaComTelefonicaservices.ConfirmCollectionResponse_element();
		objConfirmCollectionResponse_element.AtisRequestNumber=1;
        objConfirmCollectionResponse_element.AtiempoRequestNumber=1;
        ws_wwwTelefonicaComTelefonicaservices.ErrorType objError=new ws_wwwTelefonicaComTelefonicaservices.ErrorType();
        objError.Code=1;
        objError.Description='Description';
        objConfirmCollectionResponse_element.Error=objError;

        ws_wwwTelefonicaComTelefonicaservices.CancellationResponse_element objCancellationResponse_element =new ws_wwwTelefonicaComTelefonicaservices.CancellationResponse_element();
        objCancellationResponse_element.ttAccountId='ttAccountId';

        ws_wwwTelefonicaComTelefonicaservices.AddUserSkillOIMResponse_element objAddUserSkillOIMResponse_element = new ws_wwwTelefonicaComTelefonicaservices.AddUserSkillOIMResponse_element();
        objAddUserSkillOIMResponse_element.out='out';

        ws_wwwTelefonicaComTelefonicaservices.RemoveUserSkillOIMResponse_element objRemoveUserSkillOIMResponse_element =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserSkillOIMResponse_element();
        objRemoveUserSkillOIMResponse_element.out='out';

		ws_wwwTelefonicaComTelefonicaservices.CreateUserOIMResponse_element objCreateUserOIMResponse_element = new ws_wwwTelefonicaComTelefonicaservices.CreateUserOIMResponse_element();
		objCreateUserOIMResponse_element.out='out';
		
		ws_wwwTelefonicaComTelefonicaservices.RemoveUserRoleOIMResponse_element objRemoveUserRoleOIMResponse_element =new ws_wwwTelefonicaComTelefonicaservices.RemoveUserRoleOIMResponse_element();
		objRemoveUserRoleOIMResponse_element.out='out';
		
		ws_wwwTelefonicaComTelefonicaservices.SetupMobileInternetResponse_element objSetupMobileInternetResponse_element = new ws_wwwTelefonicaComTelefonicaservices.SetupMobileInternetResponse_element();
		
		ws_wwwTelefonicaComTelefonicaservices.AddUserProfileOIMResponse_element objAddUserProfileOIMResponse_element = new ws_wwwTelefonicaComTelefonicaservices.AddUserProfileOIMResponse_element();
		objAddUserProfileOIMResponse_element.out='out';
		
		ws_wwwTelefonicaComTelefonicaservices.AddUserRoleOIMResponse_element objAddUserRoleOIMResponse_element = new ws_wwwTelefonicaComTelefonicaservices.AddUserRoleOIMResponse_element();
		objAddUserRoleOIMResponse_element.out='out';
		
		ws_wwwTelefonicaComTelefonicaservices.CheckStatusSVAResponse_element objCheckStatusSVAResponse_element= new  ws_wwwTelefonicaComTelefonicaservices.CheckStatusSVAResponse_element();
		objCheckStatusSVAResponse_element.SxRxId=1;
        objCheckStatusSVAResponse_element.clientId='ClientId';
        objCheckStatusSVAResponse_element.accountId='accountid';
        objCheckStatusSVAResponse_element.idpc='idpc';
        objCheckStatusSVAResponse_element.isSVA=true;
        objCheckStatusSVAResponse_element.status=true;
        objCheckStatusSVAResponse_element.svaDescription='scaDescription';
		
		ws_wwwTelefonicaComTelefonicaservices.LegalizeMobileInternetResponse_element objLegalizeMobileInternetResponse_element = new ws_wwwTelefonicaComTelefonicaservices.LegalizeMobileInternetResponse_element();
		
		ws_wwwTelefonicaComTelefonicaservices.PutPurchaseOrderResponse_element objPutPurchaseOrderResponse_element = new ws_wwwTelefonicaComTelefonicaservices.PutPurchaseOrderResponse_element();
		objPutPurchaseOrderResponse_element.returnCode='returnCode';
		
		ws_wwwTelefonicaComTelefonicaservices.GetUserExternalLoginResponse_element objGetUserExternalLoginResponse_element = new ws_wwwTelefonicaComTelefonicaservices.GetUserExternalLoginResponse_element();
		objGetUserExternalLoginResponse_element.urlRedirection='urRedireccion';
        objGetUserExternalLoginResponse_element.tokenId='tokenId';
        objGetUserExternalLoginResponse_element.returnCode='returnCode';
		
		mapWSResponseBySOAPAction.put( 'ActivateOperation', objActivateOperationResponse_element);
		mapWSResponseBySOAPAction.put( 'SetUserSupervisionOIM', objSetUserSupervisionOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'TechnicalViability', objTechnicalViabilityResponseElement);
		mapWSResponseBySOAPAction.put( 'ProvisioningSVA', objProvisioningSVAResponse_element);
		mapWSResponseBySOAPAction.put( 'RemoveUserProfileOIM', objRemoveUserProfileOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'ConfirmCollection', objConfirmCollectionResponse_element);
		mapWSResponseBySOAPAction.put( 'Cancellation', objCancellationResponse_element);
		mapWSResponseBySOAPAction.put( 'AddUserSkillOIM', objAddUserSkillOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'RemoveUserSkillOIM', objRemoveUserSkillOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'CreateUserOIM', objCreateUserOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'RemoveUserRoleOIM', objRemoveUserRoleOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'SetupMobileInternet', objSetupMobileInternetResponse_element);
		mapWSResponseBySOAPAction.put( 'AddUserProfileOIM', objAddUserProfileOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'AddUserRoleOIM', objAddUserRoleOIMResponse_element);
		mapWSResponseBySOAPAction.put( 'CheckStatusSVA', objCheckStatusSVAResponse_element);
		mapWSResponseBySOAPAction.put( 'LegalizeMobileInternet', objLegalizeMobileInternetResponse_element);
		mapWSResponseBySOAPAction.put( 'PutPurchaseOrder', objPutPurchaseOrderResponse_element);
		mapWSResponseBySOAPAction.put( 'GetUserExternalLogin', objGetUserExternalLoginResponse_element);
		
		System.debug('\n\n####requestName'+requestName);
		System.debug('\n\n####objTechnicalViabilityResponseElement=>'+objTechnicalViabilityResponseElement);
		System.debug('\n\n ### objSetUserSupervisionOIMResponse_element=>'+objSetUserSupervisionOIMResponse_element);
		response.put( 'response_x', mapWSResponseBySOAPAction.get(requestName) );
		System.debug('\n\n####objTechnicalViabilityResponseElement=>'+objTechnicalViabilityResponseElement+' response=>'+response);
	}
}