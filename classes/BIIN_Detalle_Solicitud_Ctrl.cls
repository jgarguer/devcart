public class BIIN_Detalle_Solicitud_Ctrl {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
	 Company:       Deloitte
	 Description:   Clase para ver el detalle de la solicitud de incidencia guardada en Salesforce
	    
	 History:
	    
	 <Date>            <Author>        											                      <Description>
	 10/12/2015        Ignacio Morales Rodríguez, Jose María Martín Castaño   		Initial version
   17/06/2016        José Luis González Beltrán                                 Adapt to UNICA
   30/06/2016        José Luis González Beltrán                                 Retrieve BI2_PER_Descripcion_de_producto__c case field
   12/07/2016        José Luis González Beltrán                                 Fix case status when saving
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public user casoName                      {get; set;}
    public Account AccountName                {get; set;}
    public Contact nombreContact              {get; set;}  
    public case caso                          {get; set;}
    public String  casoNota                   {get; set;}
    public boolean displayPopup               {get; set;} 
    public String caseId;
    public Attachment attach                  {get; set;}
    public boolean modBoolean                 {get; set;}
    public List <Attachment> ListaAdjuntos    {get; set;} 
    public String attach_Id                   {get; set;}
    public attachment adjunto                 {get; set;}
    public boolean displayPopUp2    		      {get; set;}
    public boolean displayPopUp3    	        {get; set;}
    public String StringWSCreacion 			      {get; set;}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Constructor, donde se definen las variables globales y se obtienen todos los campos del caso.
	    
	    History:
	    
	    <Date>            <Author>          			<Description>
	    12/12/2015     Ignacio Morales Rodríguez    	Initial version
      15/02/2016     Micah Burgos García              Add ParentId field for case query on variable 'caso'. eHelp 01519963
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


        public BIIN_Detalle_Solicitud_Ctrl(ApexPages.StandardController controller) {  
            StringWSCreacion = '';
            displayPopUp = false;
            displayPopUp2 = false;
            displayPopUp2 = false;
            ListaAdjuntos  = new List <Attachment> ();
            modBoolean = true;
            casoNota = '';
            caso = new case();    
            adjunto = new Attachment();   
            caseId = System.currentPagereference().getParameters().get('Idcaso');         
            system.debug('idcaso--->' + caseId );         
            ListaAdjuntos = [SELECT Id, Name, Body FROM Attachment WHERE ParentId  =: caseId];
            caso = [ SELECT  toLabel(Type), Reason,BI_Product_Categorization_Tier_1__c, ParentId, BI_Product_Categorization_Tier_2__c, BI_Product_Categorization_Tier_3__c, CaseNumber, AccountId, ContactId, toLabel(Status),BIIN_Form_Name__c,TGS_Ticket_Site__c, OwnerId, BI_COL_Fecha_Radicacion__c, BI_Codigo_del_producto__c,  BIIN_Descripcion_error_integracion__c,  BI2_Nombre_Contacto_Final__c, toLabel(Origin), BI2_Apellido_Contacto_Final__c, TGS_Impact__c, BI2_Email_Contacto_Final__c, TGS_Urgency__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c, RecordTypeId, BI_COL_Codigo_CUN__c, toLabel(BI_Line_of_Business__c), BI_Product_Service__c, BIIN_Site__c, BIIN_Id_Producto__c, Subject,  description, id, BI2_PER_Descripcion_de_producto__c FROM case WHERE id =: caseId];    
            casoName =  [ SELECT Name FROM User WHERE Id =: caso.OwnerId ];
            AccountName = [SELECT Name, BI2_asesor__c FROM Account WHERE Id =: caso.AccountId];
            nombreContact =  [SELECT Name FROM Contact WHERE Id =: caso.ContactId];
            system.debug('BIIN_Descripcion_error_integracion__c  ---->' + caso.BIIN_Descripcion_error_integracion__c);
        }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para mostrar la vista del adjunto asociado a la solicitud. 
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public PageReference showAttachment(){
        adjunto = [SELECT Id FROM Attachment WHERE Id =: attach_Id];            
        
        return new ApexPages.Standardcontroller(adjunto).view();  
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Funciones para modificar las Categorías Operacionales.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    Public void Modificar(){
        modBoolean = false; 
    }
    
    Public void GuardarModif(){
        Relanzar();
        modBoolean = true;
    }
    
    Public void Cancelar(){        
        modBoolean = true;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martin Castaño
	    Company:       Deloitte
	    Description:   Función para Relanzar la Solicitud.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015      Jose María Martin Castaño 			Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     Public void Relanzar()
     {
        BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
        BIIN_Creacion_Ticket_WS ct=new BIIN_Creacion_Ticket_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        Integer nIntentos=0;
        Boolean exito=false;
        system.debug('CASO' + caso);


        // Mount attachment list
        List<BIIN_Creacion_Ticket_WS.Adjunto> la = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
        for (Attachment att: ListaAdjuntos)
        {
            system.debug('Adjunto nombre: '+ att);
            BIIN_Creacion_Ticket_WS.Adjunto a = new BIIN_Creacion_Ticket_WS.Adjunto(att.Name, att.Body, att.Id);
            system.debug('Adjunto nombre: '+ a);
            la.add(a);                
        } 
        // Make rest callout
        do
        {
            exito=ct.crearTicket(authorizationToken, caso, la, null);
            nIntentos++;
        }
        while((nIntentos<3)&&(exito==false));
         
        if(Test.isRunningTest())
        {
        	exito = false;
        }

        StringWSCreacion = ct.StringWS;        
        system.debug('BooleanWSCreacion----->' + StringWSCreacion); 
        if(!exito){//lanzar la tarea porque no hemos podido contactar con el bao
            caso.Status='Confirmation pending';
            StringWSCreacion = 'PENDING';
            try{
                update caso;
                }catch(DmlException e){
                    system.debug('Error al insertar el estado del caso: '+e);
                }
                Task tarea = new Task();
                tarea.WhatId = caso.Id;
                tarea.WhoId = caso.ContactId;
                tarea.Subject = 'Other';
                tarea.OwnerId = Userinfo.getUserId();
                tarea.Description = 'Esto es una tarea de prueba';
                tarea.Status = 'Not Started';
                tarea.Priority = 'High';
                insert tarea;          
                }else{
                    system.debug('DETALLE SOLICITUD OK');
                }
            }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para almacenar la descripción del error que ha sucedido en la integración .
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void MostrarPopUpEstado()
     {  
       if(StringWSCreacion == 'PENDING')
       {
           displayPopUp = true;
           caso.BIIN_Descripcion_error_integracion__c = 'Es necesario relanzar manualmente el ticket,pulse el botón Relanzar';
           update caso;
       }   
       else if(StringWSCreacion == 'OK')
       {
           displayPopUp2 = true;
           caso.Status = 'Being processed';
           caso.BIIN_Descripcion_error_integracion__c = 'Petición en curso. Mientras el ticket no se haya procesado podrá consultar su ticket 	como Solicitud Incidencia Técnica. En breves momentos recibirá una notificación y podrá consultar su ticket como una Incidencia Técnica.';
           update caso;
       }  
       else
       {
           displayPopUp3 = true;
           caso.BIIN_Descripcion_error_integracion__c = StringWSCreacion;
           update caso;
       }  
   }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Cierre y Apertura  de PopUp. 
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void NOOKdetalle(){   
      displayPopUp = false;
      displayPopUp2 = false;  
      displayPopUp3 = false;    
  }

  public pageReference OKdetalle(){   
      return new PageReference('/500/o');
  }

  public pageReference TimeOUT(){  
      return new ApexPages.Standardcontroller(caso).view();
  }

}