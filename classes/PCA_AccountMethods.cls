public class PCA_AccountMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Class with account methods to Portal Platino 
    
    History:
    
    <Date>            <Author>          	<Description>
     24/06/2014       Micah Burgos       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   When change the sector in to account if not exist a chatter group create this chatter group with the Sector Name
    
    History:
    
    <Date>            <Author>          	<Description>
     02/07/2014       Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void PCA_CreateChatterGroup(List<Account> news, List<Account> olds){
		try{
			system.debug('news: '+news);
			system.debug('olds: '+olds);
			String industryName ='';
			String publicStr = 'Public';
			List<CollaborationGroup> listCG = new List<CollaborationGroup>();
			
			//Network ID
			list<Network> Net = [select Id from Network where Name = :Label.PCA_Name_Comm limit 1];
			if(!Net.isEmpty()){
				set<string> setSector = new set<string>();
				integer i = 0;
				for(Account items : news){
					system.debug('items: ' +items);
					if((olds == null && items.BI_Sector__c != null) || (olds != null && items.BI_Sector__c != null && items.BI_Sector__c != olds[i].BI_Sector__c )){
						system.debug('Entra');
						setSector.add(items.BI_Sector__c);
					}
					i++;
				}
				
				
				if(!setSector.isEmpty()){
					/*list<BI_Pickup_Option__c> listPickUpOption = [Select Id, Name From BI_Pickup_Option__c where id IN : setSector];
					setSector = new set<string>();
					for(BI_Pickup_Option__c item : listPickUpOption){
						setSector.add(item.Name);
					}*/
					system.debug('setSector: '+setSector);
					set<string> groupToCreate = new set<string>();
					List<CollaborationGroup> cGList = [SELECT Id, Name FROM CollaborationGroup where name IN : setSector];
					if(!cGList.isEmpty()){
						for(CollaborationGroup item: cGList){
							if(!setSector.contains(item.name)){
								groupToCreate.add(item.name);
							}
						}
					}else{
						groupToCreate = setSector;
					}
					if(!groupToCreate.isEmpty()){
						for(string item : groupToCreate){
							CollaborationGroup cG = new CollaborationGroup(Name=item, /*NetworkId='0DBi0000000GmiSGAS'*/ NetworkId = Net[0].Id, CollaborationType=publicStr);
							listCG.add(cG);
						}
						//try{
							// TO DO: Use Database to depure de errors and asign with is object.
							insert listCG;				
/*						}catch(System.Dmlexception dmlE){
							
							System.debug('CollaborationGroup error generation: ' + dmlE.getMessage());
						}	*/
					}
				}
			}
			/*
		    for(Account myAccount : news){
		    	if(myAccount.Industry!=null){
					industryName = myAccount.Industry; 
			    	System.debug('industryName: ' + industryName);    	
					// TO DO: Check the field Network on CollaborationGroup select, could be more than one record with the same name but diferent Network
					// Para siguiente fase recuperar del id de registro de account un nuevo campo con el nombre de las comunities a donde aplica (Network)  
					List<CollaborationGroup> cGList = [SELECT Id, Name FROM CollaborationGroup where name = : industryName limit 1];
					// If not a group create it.
					// TO DO: recuperar el network id no hardcode
					if(cGList.size()<1 || Test.isRunningTest()){					
						CollaborationGroup cG = new CollaborationGroup(Name=industryName, NetworkId = Net.Id, CollaborationType='Public');
						listCG.add(cG);
					}	    	
		    	}				
			}
			
			if(listCG!=null && listCG.size()>0){
				try{
					// TO DO: Use Database to depure de errors and asign with is object.
					insert listCG;				
				}catch(System.Dmlexception dmlE){
					
					System.debug('CollaborationGroup error generation: ' + dmlE.getMessage());
				}		
			}
			*/
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_AccountMethods.PCA_Account', 'Portal Platino', Exc, 'Class');
		 }
	}

}