/*---------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Eduardo Ventura
Company:        Everis España
Description:    RFG-INT-CHI-01: Validación del identificador fiscal.

History:

<Date>                      <Author>                        <Change Description>
07/10/2016                  Eduardo Ventura                 Versión Inicial
17/05/2017                  Eduardo Ventura                 Actualización de estructuras.
24/10/2017                                                  Ever01:Inclusion de logs
---------------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class FS_CHI_Account_Record_Controller {
    /* DECLARACIÓN DE VARIABLES */
    /* Vacío */
    
    @AuraEnabled
    public static String createAccount(String country, String documentType, String documentValue, String name,  String currencyCode, boolean manual) {
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: createAccount   Comienzo del método');
        
        /* Declaración de variables */
        LightningResponse response = new LightningResponse();
        String status;
        if(!manual) status = 'Validado';
        else status = 'No validado';
        Account account = new Account();
        try{                       
            if(country != 'undefined') account.BI_Country__c = country;
            if(documentType != 'undefined') account.BI_Tipo_de_identificador_fiscal__c = documentType;
            if(documentValue != 'undefined') account.BI_No_Identificador_fiscal__c = documentValue;
            
            /* Información de contacto */
            if (name != 'undefined') account.name = name;          
            if (currencyCode != 'undefined') account.CurrencyIsoCode = currencyCode;
            
            /* Extra */
            account.FS_CHI_Validacion_Fiscal__c = status;                        
            insert account;
            response.id = account.Id;
        }catch(Exception exc){
            //Ever01-INI
            BI_LogHelper.generate_BILog_Generic('FS_CHI_Account_Record_Controller.createAccount', 'BI_EN', exc, 'Web Service',account);
            response.error = exc.getMessage();
            //Ever01-FIN
        }
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: createAccount   Finalización del método    Resultado: ' + JSON.serialize(response));
        return JSON.serialize(response);
    }
    
    webservice static void reintentar(String accountId) {
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: reintentar   Comienzo del método');
        
        //Ever01-INI
        Account account;
        try{
            //Ever01-FIN
            /* Variable Configuration */
            account = [SELECT Id, FS_CHI_Validacion_Fiscal__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Name, Phone, Fax FROM Account WHERE Id = :accountId LIMIT 1][0];
            if(account.FS_CHI_Validacion_Fiscal__c != 'Validado'){
                BI_RestWrapper.CustomersListType returnValue = FS_CHI_Account_Management.Sync(account.BI_No_Identificador_fiscal__c);
                if(returnValue != null && returnValue.totalResults == 1){
                    /* JSON */
                    if(returnValue.customersList.customerList[0].customerDetails != null && returnValue.customersList.customerList[0].customerDetails.customerName != null)                         
                        account.Name = returnValue.customersList.customerList[0].customerDetails.customerName;
                    if(returnValue.customersList.customerList[0].contactingModes != null){                        
                        for(BI_RestWrapper.ContactingModeType element : returnValue.customersList.customerList[0].contactingModes){
                        } 
                    }                    
                    account.FS_CHI_Validacion_Fiscal__c = 'Validado';
                } else account.FS_CHI_Validacion_Fiscal__c = 'No validado';               
                update account;
            }
            //Ever01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog_Generic('FS_CHI_Account_Record_Controller.reintentar', 'BI_EN', exc, 'Web Service',account);
        }
        //Ever01-FIN
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: reintentar   Finalización del método');
    }
    
    public static LightningResponse reintentarL(String accountId) {
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: reintentarL   Comienzo del método');
        
        //Ever01-INI
        LightningResponse l= new LightningResponse();
        Account account;
        boolean process= true;
        try{
            //Ever01-FIN
            /* Variable Configuration */
            account = [SELECT Id, BI_Country__c, FS_CHI_Validacion_Fiscal__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, Name, Phone, Fax FROM Account WHERE Id = :accountId LIMIT 1][0];            
            if(String.isEmpty(account.BI_No_Identificador_fiscal__c)){
                l.id= '300';
                l.error= 'Para poder validar el cliente debe informar el número de identificación fiscal.';
                process=false;
            }            
            if(account.BI_Country__c!='Chile'){
                l.id= '500';
                l.error= 'No es posible recuperar la información del registro para el país seleccionado.';
                process=false;
            }                        
            if(account.FS_CHI_Validacion_Fiscal__c != 'Validado' && process){
                BI_RestWrapper.CustomersListType returnValue = FS_CHI_Account_Management.Sync(account.BI_No_Identificador_fiscal__c); 
                System.debug('returnValue: '+ returnValue);
                if(returnValue != null && returnValue.totalResults == 1){
                    /* JSON */
                    if(returnValue.customersList.customerList[0].customerDetails != null && returnValue.customersList.customerList[0].customerDetails.customerName != null){
                        account.Name = returnValue.customersList.customerList[0].customerDetails.customerName;                        
                        account.FS_CHI_Validacion_Fiscal__c = 'Validado';
                        l.id= '200';
                        l.error= 'Cliente validado correctamente';                        
                    } else {
                        l.id= '300';
                        l.error= 'No es posible validar el cliente';
                        account.FS_CHI_Validacion_Fiscal__c = 'No validado';
                    }                                                           
                }else if (returnValue.totalResults == 0){
                    l.id= '300';
                    l.error= 'No es posible validar el cliente';
                    account.FS_CHI_Validacion_Fiscal__c = 'No validado';
                }else {
                    l.id= '500';
                    l.error= 'Error al validar el cliente. Contacte con su Administrador';
                    account.FS_CHI_Validacion_Fiscal__c = 'No validado';
                }                
                update account;
            }else{
                if (account.FS_CHI_Validacion_Fiscal__c == 'Validado'){
                    l.id= '200';
                    l.error= 'El cliente ya está validado.';  
                }                
            }
            //Ever01-INI
        }catch(Exception exc){    
            String aux = exc.getMessage();
   
            if (aux.contains('INSUFFICIENT_ACCESS_OR_READONLY')){
               l.id= '500';
               l.error= 'Error al validar el cliente. No tiene permiso para editar el registro o el registro es de solo lectura';            
               BI_LogHelper.generate_BILog_Generic('FS_CHI_Account_Record_Controller.reintentar', 'BI_EN', exc, 'Web Service',account);     
            }else if (aux.contains('No es posible modificar la razón social del cliente para Chile')){
                  l.id= '500';
                  l.error= 'Error al validar el cliente. No es posible modificar la razón social del cliente para Chile';            
                  BI_LogHelper.generate_BILog_Generic('FS_CHI_Account_Record_Controller.reintentar', 'BI_EN', exc, 'Web Service',account);   
            	  }else{
                    l.id= '500';
                    l.error= 'Error al validar el cliente. Contacte con su Administrador';            
                    BI_LogHelper.generate_BILog_Generic('FS_CHI_Account_Record_Controller.reintentar', 'BI_EN', exc, 'Web Service',account); 
                   }
                        
           //    account.FS_CHI_Validacion_Fiscal__c = 'No validado';
           //    update account;
            
           
        }
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: reintentarL   Finalización del método    Resultado: ' + l);
        return l;
        //Ever01-FIN
    }    
    
    @AuraEnabled
    public static LightningResponse doInitController(String accountId){
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: doInitController   Comienzo del método');        
        LightningResponse l=FS_CHI_Account_Record_Controller.reintentarL(accountId);
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: doInitController   Finalización del método    Resultado: ' + l);
        return l;
    }       
    
    @AuraEnabled
    public static String retrieveJSON(String documentType, String documentValue){
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: retrieveJSON   Comienzo del método');      
        /* Declaración de variables */
        /* Vacío */
        String response;
        try{
            response  = JSON.serialize(FS_CHI_Account_Management.Sync(documentValue));  
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('FS_CHI_Account_Record_Controller.retrieveJSON', 'BI_EN', exc, 'Web Service');        
        } 
        System.debug('Nombre de clase: FS_CHI_Account_Record_Controller   Nombre de método: retrieveJSON   Finalización del método    Resultado: ' + response);
        return response; 
    }
    
    public class LightningResponse {
        @auraEnabled
        public string id;
        @auraEnabled
        public string error;
    }
    
    @AuraEnabled
    public static List <String> getSelectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
 	}
}