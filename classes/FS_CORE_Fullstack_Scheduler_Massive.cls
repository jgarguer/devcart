/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Lanzador para sincronización de la cartera de clientes por Data Loader.

History:
<Date>							<Author>						<Change Description>
26/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class FS_CORE_Fullstack_Scheduler_Massive implements Schedulable {
    /* SALESFORCE VARIABLE DEFINITION */
    FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
    
    global void Execute(SchedulableContext ctx) {
        /* Variable Definition */
        Set<Account> resegmentacion = new Set<Account>();
        Set<Account> recarterizacion = new Set<Account>();
        
        /* Query Search */        
        List<Account> Accounts = [SELECT Id, FS_CORE_Account_Massive__c FROM Account WHERE FS_CORE_Account_Massive__c = True LIMIT :(Integer) ORGConfiguration.FS_CORE_Resegmentacion_Limit__c];
        
        List<Account> ATMAccounts = [SELECT Id, FS_CORE_ATM_Massive__c, OwnerId, BI2_asesor__c,BI2_service_manager__c FROM Account WHERE FS_CORE_ATM_Massive__c = True LIMIT :(Integer) ORGConfiguration.FS_CORE_Recarterizacion_Limit__c];
        system.debug('ATMAccounts '+ATMAccounts);
        Map<Id, Account> ATMAccountMap = new Map<Id, Account>(ATMAccounts);
        system.debug('ATMAccountMap '+ATMAccountMap);
        List<AccountTeamMember> ATMs = [SELECT Id, AccountId FROM AccountTeamMember WHERE AccountId IN :ATMAccounts];
        system.debug('ATMs '+ATMs);
        
        /* Process Accounts */
        for(Account item : Accounts){
            item.FS_CORE_Account_Massive__c = False;
            resegmentacion.add(item);
            
            /* Invoke */
            FS_CORE_Fullstack_Manager.callout(item.Id, 1, false, null);
        }
        
        /* Process ATMs */
        Account temp;
        for(AccountTeamMember item : ATMs){
            temp = ATMAccountMap.get(item.AccountId);
            temp.FS_CORE_ATM_Massive__c = False;
            recarterizacion.add(temp);
            
            if(!Test.isRunningTest()) {
            	FS_CORE_Fullstack_ATM.process(temp);
            }
        }
        
        /* Commit */
        if(!resegmentacion.isEmpty()) update new List<Account>(resegmentacion);
        if(!recarterizacion.isEmpty()) update new List<Account>(recarterizacion);
    }
}