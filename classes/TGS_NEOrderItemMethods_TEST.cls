/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Pedro Párraga
Company:       Aborda
Description:   Test class to manage the coverage code for TGS_NEOrderItemMethods class 
  
History: 
<Date>                  <Author>                <Change Description>
21/12/2016              Pedro Párraga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class TGS_NEOrderItemMethods_TEST {


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Pedro Párraga
        Company:       Aborda
        Description:   Set the value of TGS_Service__c field
        
        History: 
        <Date>                          <Author>                    <Change Description>
        21/12/2016                      Pedro Párraga           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @isTest static void setCaseService_TEST(){

        User userTest = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
       
        insert userTest;
              
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_TGS__c = true;
        insert userTGS;

        System.runAs(userTest){


         List<RecordType> rt = [SELECT Id, DeveloperName from RecordType where DeveloperName IN ('TGS_Holding', 'TGS_Customer_Country', 'TGS_Legal_Entity',
                                                                                                 'TGS_Business_Unit', 'TGS_Cost_Center')];

        Map<String, id> mrt  = new Map<String, Id>();

        for(RecordType r : rt){
            mrt.put(r.DeveloperName, r.Id);
        }

        List<Account> acc = new List<Account>();

        Account accH = new Account(Name = 'accH',BI_Country__c = 'Colombia', TGS_Region__c = 'América', BI_Tipo_de_identificador_fiscal__c  = 'NIT',
            CurrencyIsoCode = 'GTQ', RecordTypeId = mrt.get('TGS_Holding'), TGS_Es_MNC__c = true);
        insert accH;

        Account accCustC = new Account(ParentId = accH.Id, Name = 'accCustC',BI_Country__c = 'Colombia', TGS_Region__c = 'América', BI_Tipo_de_identificador_fiscal__c  = 'NIT',
            CurrencyIsoCode = 'GTQ', RecordTypeId = mrt.get('TGS_Customer_Country'), TGS_Es_MNC__c = true);
        insert accCustC;

        Account accLE = new Account(ParentId = accCustC.Id, Name = 'accLE',BI_Country__c = 'Colombia', TGS_Region__c = 'América', BI_Tipo_de_identificador_fiscal__c  = 'NIT',
            CurrencyIsoCode = 'GTQ', RecordTypeId = mrt.get('TGS_Legal_Entity'), TGS_Es_MNC__c = true);
        insert accLE;

        Account accBU = new Account(ParentId = accLE.Id, Name = 'accBU',BI_Country__c = 'Colombia', TGS_Region__c = 'América', BI_Tipo_de_identificador_fiscal__c  = 'NIT',
            CurrencyIsoCode = 'GTQ', RecordTypeId = mrt.get('TGS_Business_Unit'), TGS_Es_MNC__c = true);
        insert accBU;

        Account accCostC = new Account(ParentId = accBU.Id, Name = 'accCostC',BI_Country__c = 'Colombia', TGS_Region__c = 'América', BI_Tipo_de_identificador_fiscal__c  = 'NIT',
            CurrencyIsoCode = 'GTQ', RecordTypeId = mrt.get('TGS_Cost_Center'), TGS_Es_MNC__c = true);
       insert accCostC;

            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
            NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Configuration_Type__c = 'Disconnect');
            insert testOrder;

            Id rtIdA = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Asset');
            NE__Order__c testOrderAsset = new NE__Order__c(RecordTypeId = rtIdA);
            insert testOrderAsset;

            NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
            insert testCatalog;

            NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
            insert testCatalogCategory;

            NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(Name='Test SubCategory', NE__CatalogId__c = testCatalog.Id,
             NE__Parent_Category_Name__c = testCatalogCategory.Id);
            insert testCatalogSubCategory;

            NE__Product__c testProduct = new NE__Product__c(Name='Bundle');
            insert testProduct;

            NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Type__c = 'Product', NE__Catalog_Category_Name__c = testCatalogSubCategory.Id,
             NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
            insert testCatalogItem;

    Test.startTest();

            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
            Case testCase = new Case(RecordTypeId = rtId, Subject = 'Test Case', Status = 'Assigned', Order__c = testOrder.Id, TGS_Service__c = 'test service');
            insert testCase;

            NE__OrderItem__c orderRoot = new NE__OrderItem__c(NE__OrderId__c = testOrderAsset.Id,
             NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1, NE__Root_Order_Item__c = null);
            insert orderRoot;

            List<RecordType> rcOI = [SELECT Id from RecordType where  SobjectType = 'NE__OrderItem__c' and DeveloperName = 'Service' Limit 1];

            NE__OrderItem__c testOrderItemAsset = new NE__OrderItem__c(RecordTypeId = rcOI[0].Id, NE__Account__c = accLE.Id, NE__Billing_Account__c = accCostC.Id,
             NE__Service_Account__c = accBU.Id, NE__OrderId__c = testOrderAsset.Id,
            NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1, NE__Root_Order_Item__c = orderRoot.Id);
            insert testOrderItemAsset;

            NE__OrderItem__c testOrderItemOrder = new NE__OrderItem__c(RecordTypeId = rcOI[0].Id, NE__Account__c = accLE.Id, NE__Billing_Account__c = accCostC.Id,
             NE__Service_Account__c = accBU.Id, NE__OrderId__c = testOrder.Id,
            NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1, NE__Root_Order_Item__c = orderRoot.Id);
            insert testOrderItemOrder;

            testOrder.Case__c = testCase.Id;
            update testOrder;

            testOrderAsset.Case__c = testCase.Id;
            update testOrderAsset;

            testOrderItemAsset.TGS_billing_start_date__c = Date.newInstance(date.today().year()+1, 12, 22);
            testOrderItemAsset.TGS_billing_end_date__c = Date.newInstance(date.today().year()+1, 12, 25);
            testOrderItemAsset.NE__Root_Order_Item__c = null;
            update testOrderItemAsset;

            /*testOrderItemOrder.TGS_billing_start_date__c = Date.newInstance(date.today().year()+1, 12, 22);
            testOrderItemOrder.TGS_billing_end_date__c = Date.newInstance(date.today().year()+1, 12, 25);
            testOrderItemOrder.NE__Root_Order_Item__c = null;
            update testOrderItemOrder;*/

        Test.stopTest();

    }

    }

    @isTest static void validateMSISDN_TEST(){

        Account a = new Account();
        a = TGS_Dummy_Test_data.dummyHierarchy();
        //Database.insert(a);   Originally in the method; not neccesary since dummyHierachy() does the insert call      
        Contact c = new Contact(AccountId = a.id,
                        lastname = 'lastname',
                        Email = 'contactTest@telefonica.com',
                        BI_Activo__c = true,
                        BI_Country__c = a.BI_Country__c
                        );
        insert c;
        Test.startTest();
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS('TGS Customer Community Plus');
        userTest.BI_Permisos__c = 'TGS';
        userTest.ContactId = c.Id;
       
        insert userTest;
              
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_TGS__c = true;
        insert userTGS;

        System.runAs(userTest){

            NE__OrderItem__C oi = TGS_Dummy_Test_Data.dummyConfiguration(userTest.Id, 'New');
            NE__OrderItem__C oi2 = TGS_Dummy_Test_Data.dummyConfiguration(userTest.Id, 'New');

            oi.NE__Parent_Order_Item__c = null;
            oi.NE__AssetItemEnterpriseId__c = oi2.Id;
            update oi;
            Set<Id> set_ois = new Set<Id>();
            set_ois.add(oi.Id);
            set_ois.add(oi2.Id);
            Test.stopTest();
            NE__Order__c ord = new NE__Order__c(Id = oi.NE__OrderId__c, NE__Channel__c='Bulk request', NE__Type__c = 'Disconnection');
            BI_MigrationHelper.skipAllTriggers();
            update ord;
            BI_MigrationHelper.cleanSkippedTriggers();

            List<NE__Order_Item_Attribute__c> lst_atts = [SELECT Id, Name, NE__Value__c, NE__Order_Item__c FROM NE__Order_Item_Attribute__c WHERE NE__Order_Item__c IN :set_ois];

            for(NE__Order_Item_Attribute__c att :lst_atts){
                att.Name = 'MSISDN (Telephone No)';
                att.NE__Value__c = '2233';
            }
            NE__Order_Item_Attribute__c cia = new NE__Order_Item_Attribute__c(
                Name = 'MSISDN (Telephone No)',
                NE__Value__c = '46577',
                NE__Order_Item__c = oi.Id
            );
            insert cia;
            lst_atts.add(cia);
            System.debug('lst_atts: ' + lst_atts);
            update lst_atts;

        }
    }
}