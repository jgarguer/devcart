@RestResource(urlMapping='/customerinfo/v1/customers/*/accounts/*')
global class BI_CustomerAccountsRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for accounts related to a customer Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains the list of accounts linked to a given customer.
    
    IN:            Void
    OUT:           BI_RestWrapper.AccountsListType structure
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.AccountsListType getAccounts() {
		
		BI_RestWrapper.AccountsListType response;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				response = BI_RestHelper.getAccountsForCustomer(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				RestContext.response.statuscode = (response == null)?404:200;//404 NOT_FOUND, 200 OK
				
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerAccountsRest.getAccounts', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return response;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Links an existing account to a customer.
    
    IN:            Void
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static void updateAccounts() {
		
		try{
				
			String customerId = String.valueOf(RestContext.request.requestURI).split('/')[4];
			String accountId = String.valueOf(RestContext.request.requestURI).split('/')[6];
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				String message = BI_RestHelper.updateAccountForCustomer(customerId, accountId, RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				if(message == Label.BI_UpdateOk){
				
					RestContext.response.statuscode = 200;
				
				}else if(message == Label.BI_NotFound){
					
					RestContext.response.statuscode = 404;//NOT_FOUND
					
				}else{
					
					throw new BI_Exception(message);
					
				}
				
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerAccountsRest.updateAccounts', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Removes the link between an existing account and a customer
    
    IN:            Void
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpDelete
	global static void deleteAccounts() {
		
		try{
				
			String customerId = String.valueOf(RestContext.request.requestURI).split('/')[4];
			String accountId = String.valueOf(RestContext.request.requestURI).split('/')[6];
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				String message = BI_RestHelper.deleteAccountForCustomer(customerId, accountId, RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				if(message == Label.BI_UpdateOk){
					
					RestContext.response.statuscode = 200;
					
				}else if(message == Label.BI_NotFound){
					
					RestContext.response.statuscode = 404;//NOT_FOUND
					
				}else{
					
					throw new BI_Exception(message);
					
				}
				
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerAccountsRest.deleteAccounts', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}
	
}