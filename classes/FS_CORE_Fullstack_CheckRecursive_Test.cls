/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                         Pablo Bordas
Company:                        Everis 
Description:                    Test para la clase FS_CORE_Fullstack_CheckRecursive

History:
<Date>                          <Author>                        <Change Description>
14/05/2017                      Pablo Bordas            		Versión inicial
<
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CORE_Fullstack_CheckRecursive_Test {
	
    
    static testMethod void runOnceTrue() {
        boolean result = FS_CORE_Fullstack_CheckRecursive.runOnce();
        System.assert(result);
    }
    
    static testMethod void runOnceFalse() {
        boolean resultF = FS_CORE_Fullstack_CheckRecursive.runOnce();
        boolean result = FS_CORE_Fullstack_CheckRecursive.runOnce();
        System.assert(!result);
    }
}