/**
* Avanxo Colombia
* @author           Dolly Fierro href=<dfierro@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-12      Dolly Fierro (DF)                       New Test Class
* @version   2.0    2015-08-27      Oscar Angel  (OA)                       Corrección de errores y aumento de cobertura 
*************************************************************************************************************/
@isTest
private class WS_ContactTask_tst  
{
    public static list<Profile> lstPerfil;
    public static list <UserRole> lstRoles;
    public static list<User> lstUsuarios;
    public static list <Campaign> lstCampana;
    public static List<RecordType> lstRecTyp;
    public static list<BI_COL_Modificacion_de_Servicio__c> lstMSSel;
    
    public static User objUsuario;
    public static Account objCuenta;
    public static Opportunity objOportunidad;
    public static Campaign objCampana;
    public static BI_COL_cup_Selectds_ctr objCupSelectds;
    public static BI_COL_Modificacion_de_Servicio__c objMS;
    public static BI_COL_Anexos__c objAnexos;
    public static BI_COL_Descripcion_de_servicio__c objDesSer;
    public static BI_Col_Ciudades__c objCiudad;

    
    public static void crearData()
    {
        BI_bypass__c objBibypass = new BI_bypass__c();
        objBibypass.BI_migration__c=true;
        insert objBibypass;
            
        objCuenta                                                   = new Account();
        objCuenta.Name                                              = 'prueba';
        objCuenta.BI_Country__c                                     = 'Colombia';
        objCuenta.TGS_Region__c                                     = 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c                = '';
        objCuenta.CurrencyIsoCode                                   = 'COP';
        objCuenta.BI_No_Identificador_fiscal__c                     = '1234';
        insert objCuenta;
        
        objOportunidad                                              = new Opportunity();
        objOportunidad.Name                                         = 'prueba opp';
        objOportunidad.AccountId                                    = objCuenta.Id;
        objOportunidad.BI_Country__c                                = 'Colombia';
        objOportunidad.CloseDate                                    = System.today().addDays(+5);
        objOportunidad.StageName                                    = 'F5 - Solution Definition';
        objOportunidad.CurrencyIsoCode                              = 'COP';
        objOportunidad.Certa_SCP__contract_duration_months__c       = 12;
        objOportunidad.BI_Plazo_estimado_de_provision_dias__c       = 0 ;
        insert objOportunidad;
        
        objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
        objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
        objDesSer.CurrencyIsoCode               = 'COP';
        insert objDesSer;
        
        objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
        //objMS.BI_COL_FUN__c                         = objAnexos.Id;
        objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
        objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
        objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
        objMS.BI_COL_Bloqueado__c                   = false;
        objMS.BI_COL_Estado_orden_trabajo__c        = 'Ejecutado';
        //objMS.BI_COL_Cuenta_facturar_davox__c       = '1'; //10/11/2016 GSPM: Se comentó el campo antiguo
        objMS.BI_COL_Cuenta_facturar_davox1__c       = '1'; //10/11/2016 GSPM: Campo nuevo facturar davox1 
        
        lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
        lstMSSel.add(objMS); 
        insert lstMSSel;
        System.debug('\n\n\n Modificación de Servicio '+ objMS);
        
        objCiudad = new BI_Col_Ciudades__c();
        objCiudad.Name = 'BOGOTA DC / CUNDINAMARCA';
        objCiudad.BI_COL_Codigo_DANE__c = '11001000';
        insert objCiudad;              
    }
    static testMethod void myUnitTestOne() {
        crearData();
        WS_ContactTask.notificacionRequest clsNotifReq = new WS_ContactTask.notificacionRequest();
        
        clsNotifReq.id = objCuenta.Id;
        clsNotifReq.estado = 'OK';
        clsNotifReq.mensaje = 'Mensaje'; 
        clsNotifReq.sistema = 'Sistema';
      
       WS_ContactTask.NotificacionTRSSucursal(new List<WS_ContactTask.notificacionRequest>{clsNotifReq});
       WS_ContactTask.NotificacionTRSCliente(new List<WS_ContactTask.notificacionRequest>{clsNotifReq});
       WS_ContactTask.NotificacionTRSContacto(new List<WS_ContactTask.notificacionRequest>{clsNotifReq});
      
       WS_ContactTask.respCambioSucursalRequest cambioSuc=new WS_ContactTask.respCambioSucursalRequest();
       //cambioSuc.cuenta=objMS.BI_COL_Cuenta_facturar_davox__c;
       cambioSuc.cuenta=objMS.BI_COL_Cuenta_facturar_davox1__c; //10/11/2016 GSPM: Se comentó el campo antiguo
       cambioSuc.idCliente=objCuenta.BI_No_Identificador_fiscal__c; //10/11/2016 GSPM: Campo nuevo facturar davox1 
       cambioSuc.IdDs=objDesSer.Id;
       cambioSuc.IdMs=objMS.Id;
       
       WS_ContactTask.respCambioSucursal(new List<WS_ContactTask.respCambioSucursalRequest>{cambioSuc},'Davox');  
       WS_ContactTask.getDatosSucursal('','');
       WS_ContactTask.getDatosClientes('NIT','IdSFDC','Sistema','NombContacto','Apellidos','Representante','Teloficina','Telefono','Movil','Tipoid','NumId',
        new List<String> {'TipoContacto'},'email@avx.com','DirOficina','ciudad',objCuenta.BI_Country__c,'IdContacto',objCuenta.BI_No_Identificador_fiscal__c,'TipoDocCliente','Estado');
       WS_ContactTask.notificacionRequest nreq=new WS_ContactTask.notificacionRequest();
       WS_ContactTask.Response res=new WS_ContactTask.Response();
       WS_ContactTask.respCambioSucursalRequest resCaSuc=new WS_ContactTask.respCambioSucursalRequest();
    }
}