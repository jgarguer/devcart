/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Infinity W4+5 methods executed by NE__OrderItem__c trigger
 Test Class:    BI_O4_OrderItemMethods_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/07/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_OrderItemMethods
{
	// Map to hold pair <RecordType.DeveloperName, Id>
	final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
	
    static {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Opportunity']){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Checks several conditions on order items related to the opportunity, and if apply, updates BI_Oferta_Economica__c field for approval

    In:            newOpps: Trigger.new
    Out:           void
    
    History: 
    
    <Date>          <Author>                <Change Description>
    23/08/2016      Fernando Arteaga        Initial Version
    28/10/2016      Fernando Arteaga        Get only TNA opportunities
    03/02/2017      Alvaro Garcia		Added condition to check if set is empty
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Map<Id, Opportunity> updateOpptyForApproval(List<NE__OrderItem__c> newOrderItems, List<NE__OrderItem__c> oldOrderItems)
	{
		Set<Id> setOrderId = new Set<Id>();
		Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
		Map<Id, String> mapOppApproval = new Map<Id, String>();
		//Map<Id, String> mapOppOfertaEconomica = new Map<Id, String>();
		
		Map<Id, List<NE__OrderItem__c>> mapOrderItems = new Map<Id, List<NE__OrderItem__c>>();
		
		for (NE__OrderItem__c oi: newOrderItems)
			setOrderId.add(oi.NE__OrderId__c);
			
		try
		{
			//if (BI_TestUtils.isRunningTest())
			//	throw new BI_Exception('test');
			if (!setOrderId.isEmpty()) {

				for (NE__OrderItem__c oi: [SELECT Id, BI_O4_Gross_margin_P__c, BI_O4_Acquisition_Margin__c, BI_O4_Retention_Margin__c, BI_O4_Growth_Margin__c, BI_FCV_Producto__c,
									    	      NE__OrderId__c, NE__OrderId__r.NE__OptyId__c, NE__OrderId__r.NE__OptyId__r.BI_O4_Tipo_de_OportunidadTNA__c, NE__OrderId__r.NE__OptyId__r.BI_Oferta_economica__c
	                                       FROM NE__OrderItem__c
	                                       WHERE NE__OrderId__c IN :setOrderId
	                                       AND NE__OrderId__r.NE__OptyId__r.BI_O4_Tipo_de_OportunidadTNA__c != null])
				{

					if (!mapOrderItems.containsKey(oi.NE__OrderId__c))
						mapOrderItems.put(oi.NE__OrderId__c, new List<NE__OrderItem__c>{oi});
					else
						mapOrderItems.get(oi.NE__OrderId__c).add(oi);
				}
				
				if (!mapOrderItems.isEmpty())
				{
					mapOppApproval = economicValidationTNA(mapOrderItems);
					System.debug(LoggingLevel.ERROR, mapOppApproval);

					for (Id oppId : mapOppApproval.keySet())
					{
						Opportunity newOpp = new Opportunity(Id = oppId, BI_Oferta_economica__c = mapOppApproval.get(oppId));
						mapOpp.put(oppId, newOpp);
					}
				}
			}	
			
			return mapOpp;
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderItemMethods.updateOpptyForApproval', 'BI_EN', exc, 'Trigger');
			return mapOpp;
		}
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Checks if certains conditions apply for the economic fields, in order to set the opportunity for approval or not

    In:            Map<Id, List<NE__OrderItem__c>>: map of order and list of related order items
    Out:           Map<Id, String>: map of opportunity id and value to fill BI_Oferta_economica__c field
    
    History: 
    
    <Date>          <Author>                <Change Description>
    19/09/2016      Fernando Arteaga        Initial Version
    26/01/2017      Alvaro Garcia	        Change > to >= in the condition
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static Map<Id, String> economicValidationTNA(Map<Id, List<NE__OrderItem__c>> mapOI)
	{
		String ofertaEconomica = '';
		Boolean needsApproval;
		Map<Id, String> mapOppEconomicApproval = new Map<Id, String>();
		Id oppId;
	
		try
		{	
			//if (BI_TestUtils.isRunningTest())
			//	throw new BI_Exception('test');
				
			for (Id orderId: mapOI.keySet())
			{
				oppId = null;
				needsApproval = false;
				
				for (NE__OrderItem__c oi: mapOI.get(orderId))
				{
					if (oppId == null)
						oppId = oi.NE__OrderId__r.NE__OptyId__c;
					
					System.debug(LoggingLevel.INFO, 'gross margin:' + oi.BI_O4_Gross_margin_P__c);
					System.debug(LoggingLevel.INFO, 'acquisition margin:' + oi.BI_O4_Acquisition_Margin__c);
					System.debug(LoggingLevel.INFO, 'retention margin:' + oi.BI_O4_Retention_Margin__c);
					System.debug(LoggingLevel.INFO, 'growth margin:' + oi.BI_O4_Growth_Margin__c);
					System.debug(LoggingLevel.INFO, 'FCV Producto:' + oi.BI_FCV_Producto__c);
					System.debug(LoggingLevel.INFO, 'tipo oportunidad TNA:' + oi.NE__OrderId__r.NE__OptyId__r.BI_O4_Tipo_de_OportunidadTNA__c);
					
					if ((oi.BI_O4_Gross_margin_P__c < oi.BI_O4_Acquisition_Margin__c && oi.NE__OrderId__r.NE__OptyId__r.BI_O4_Tipo_de_OportunidadTNA__c == 'Acquisition') ||
						(oi.BI_O4_Gross_margin_P__c < oi.BI_O4_Retention_Margin__c && oi.NE__OrderId__r.NE__OptyId__r.BI_O4_Tipo_de_OportunidadTNA__c == 'Retention') ||
						(oi.BI_O4_Gross_margin_P__c < oi.BI_O4_Growth_Margin__c && oi.NE__OrderId__r.NE__OptyId__r.BI_O4_Tipo_de_OportunidadTNA__c == 'Growth') ||
						(oi.BI_FCV_Producto__c >= 400000))
					{
						needsApproval = true;
						break;
					}	 
				}
				if (needsApproval)
					mapOppEconomicApproval.put(oppId, Label.BI_EnviarAprobacion);
				else
					mapOppEconomicApproval.put(oppId, Label.BI_SinDescuento);
			}
		
			return mapOppEconomicApproval;
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderItemMethods.economicValidationTNA', 'BI_EN', exc, 'Trigger');
			return mapOppEconomicApproval;	
		}
	}
	
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Checks if Local opportunity is related to a Holding account and has a parent opportunity, then it bypasses economic approval

 In:            oppty: opportunity from Trigger.new
 Out:           Boolean: if bypass applies   
 
 History:
  
 <Date>              <Author>                   <Change Description>
 19/09/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Boolean bypassEconomicApproval(Opportunity oppty)
	{
		try
		{	
			//if (BI_TestUtils.isRunningTest())
			//	throw new BI_Exception('test');
					
			if (oppty.RecordTypeId == MAP_NAME_RT.get('BI_Ciclo_completo') &&
				oppty.Account.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_HOLDING &&
				oppty.BI_Oportunidad_padre__c != null)
				return true;
			else
				return false;
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderItemMethods.bypassEconomicApproval', 'BI_EN', exc, 'Trigger');
			return false;
		}
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Checks if Local opportunity is related to a Holding account and has a parent opportunity, then it bypasses economic approval

 In:            listOrderItems: order items from Trigger.new or passed from BI_O4_OpportunityMethods.updateOrderItemsInvoicingModelUnit method
 Out:               
 
 History:
  
 <Date>              <Author>                   <Change Description>
 19/09/2016          Fernando Arteaga           Initial Version
 20/01/2017			 Alvaro García 		 		Añadido un boolean para evitar posible overwrite
 03/02/2017          Alvaro Garcia		        Added condition to check if set is empty
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void fillInvoicingModelAndUnit(List<NE__OrderItem__c> listOrderItems, Boolean overwrite)
	{
		Set<Id> setOrderId = new Set<Id>();
		Set<Id> setOppId = new Set<Id>();
		Map<Id, List<String>> mapOrderOppty = new Map<Id, List<String>>();
		Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
		
		try
		{	
			for (NE__OrderItem__c oi: listOrderItems)
			{
				setOrderId.add(oi.NE__OrderId__c);
			}

			if(!setOrderId.isEmpty()) {
		
				for (NE__Order__c ord: [SELECT Id, NE__OptyId__c, NE__OptyId__r.BI_O4_Invoicing_Model__c, NE__OptyId__r.BI_O4_Invoicing_Unit__c
									    FROM NE__Order__c
									    WHERE Id IN :setOrderId])
				{
					mapOrderOppty.put(ord.Id, new List<String>{ord.NE__OptyId__r.BI_O4_Invoicing_Model__c, ord.NE__OptyId__r.BI_O4_Invoicing_Unit__c});
				}

				for (NE__OrderItem__c oi: listOrderItems)
				{
					if (mapOrderOppty.containsKey(oi.NE__OrderId__c))
					{
						List<String> listStrOpp = mapOrderOppty.get(oi.NE__OrderId__c);
						oi.BI_O4_Invoicing_Model__c = listStrOpp[0];
						oi.BI_O4_Invoicing_Unit__c = listStrOpp[1];
					}
				}
				
				if (overwrite && !listOrderItems.isEmpty()){
					update listOrderItems;
				}
			}
			
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderItemMethods.fillInvoicingModelAndUnit', 'BI_EN', exc, 'Trigger');
		}
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Sets BI_O4_Total_cost__c and BI_O4_Total_price__c fields (insert trigger)

 In:            listOrderItems: order items from Trigger.new
 Out:               
 
 History:
  
 <Date>              <Author>                   <Change Description>
 19/09/2016          Fernando Arteaga           Initial Version
 03/02/2017          Alvaro Garcia		        Added condition to check if set is empty
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setTotalPriceAndCost(List<NE__OrderItem__c> newOrderItems)
	{
		Set<Id> setOrderId = new Set<Id>();
		Map<Id, Decimal> mapOrderDuracionMeses = new Map<Id, Decimal>();
		
		try
		{
				
			for (NE__OrderItem__c oi: newOrderItems)
			{
				if (oi.NE__Qty__c != null && ((oi.NE__Recurring_Cost__c != null && oi.NE__One_Time_Cost__c != null) || (oi.NE__RecurringChargeOv__c != null && oi.NE__OneTimeFeeOv__c != null))) 
					setOrderId.add(oi.NE__OrderId__c);
			}
			
			if (!setOrderId.isEmpty()) {
				
				for (NE__Order__c ord : [SELECT Id, NE__OptyId__r.BI_Duracion_del_contrato_Meses__c
										 FROM NE__Order__c
										 WHERE Id = :setOrderId])
				{
					mapOrderDuracionMeses.put(ord.Id, ord.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c);
				}
			
				for (NE__OrderItem__c oi: newOrderItems)
				{
					//if (oi.NE__Recurring_Cost__c != null && oi.NE__One_Time_Cost__c != null && mapOrderDuracionMeses.get(oi.NE__OrderId__c) != null)
					oi.BI_O4_Total_cost__c = (convertNullToZero((Double)oi.NE__One_Time_Cost__c)  + (convertNullToZero((Double)oi.NE__Recurring_Cost__c) * mapOrderDuracionMeses.get(oi.NE__OrderId__c))) * oi.NE__Qty__c;
					
					//if (oi.NE__RecurringChargeOv__c != null && oi.NE__OneTimeFeeOv__c != null && mapOrderDuracionMeses.get(oi.NE__OrderId__c) != null)
					oi.BI_O4_Total_price__c = (convertNullToZero((Double)oi.NE__OneTimeFeeOv__c) + (convertNullToZero((Double)oi.NE__RecurringChargeOv__c) * mapOrderDuracionMeses.get(oi.NE__OrderId__c))) * oi.NE__Qty__c;
				}
			}	

			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderItemMethods.setTotalPriceAndCost', 'BI_EN', exc, 'Trigger');
		}
	}


/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Sets BI_O4_Total_cost__c and BI_O4_Total_price__c fields (update trigger)

 In:            listOrderItems: order items from Trigger.new
                mapOldOrderItems: order items from Trigger.oldMap
 Out:               
 
 History:
  
 <Date>              <Author>                   <Change Description>
 19/09/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/    
	public static void setTotalPriceAndCost(List<NE__OrderItem__c> newOrderItems, Map<Id, NE__OrderItem__c> mapOldOrderItems)
	{
		Set<Id> setOrderId = new Set<Id>();
		Map<Id, Decimal> mapOrderDuracionMeses = new Map<Id, Decimal>();
		
		try
		{				
			for (NE__OrderItem__c oi: newOrderItems)
			{
				if ((oi.NE__Qty__c != null && ((oi.NE__Recurring_Cost__c != null && oi.NE__One_Time_Cost__c != null) || (oi.NE__RecurringChargeOv__c != null && oi.NE__OneTimeFeeOv__c != null))) &&
					((oi.NE__Recurring_Cost__c != mapOldOrderItems.get(oi.Id).NE__Recurring_Cost__c) ||
					 (oi.NE__One_Time_Cost__c != mapOldOrderItems.get(oi.Id).NE__One_Time_Cost__c) ||
					 (oi.NE__RecurringChargeOv__c != mapOldOrderItems.get(oi.Id).NE__RecurringChargeOv__c) ||
					 (oi.NE__OneTimeFeeOv__c != mapOldOrderItems.get(oi.Id).NE__OneTimeFeeOv__c) ||
					 (oi.NE__Qty__c != mapOldOrderItems.get(oi.Id).NE__Qty__c)))
					setOrderId.add(oi.NE__OrderId__c);
			}
	
			if (!setOrderId.isEmpty())
			{
				for (NE__Order__c ord : [SELECT Id, NE__OptyId__r.BI_Duracion_del_contrato_Meses__c
										 FROM NE__Order__c
										 WHERE Id = :setOrderId])
				{
					mapOrderDuracionMeses.put(ord.Id, ord.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c);
				}
				
				for (NE__OrderItem__c oi: newOrderItems)
				{
					//if (oi.NE__Recurring_Cost__c != null && oi.NE__One_Time_Cost__c != null && mapOrderDuracionMeses.get(oi.NE__OrderId__c) != null)
					oi.BI_O4_Total_cost__c = (convertNullToZero((Double)oi.NE__One_Time_Cost__c)  + (convertNullToZero((Double)oi.NE__Recurring_Cost__c) * mapOrderDuracionMeses.get(oi.NE__OrderId__c))) * oi.NE__Qty__c;
					
					//if (oi.NE__RecurringChargeOv__c != null && oi.NE__OneTimeFeeOv__c != null && mapOrderDuracionMeses.get(oi.NE__OrderId__c) != null)
					oi.BI_O4_Total_price__c = (convertNullToZero((Double)oi.NE__OneTimeFeeOv__c) + (convertNullToZero((Double)oi.NE__RecurringChargeOv__c) * mapOrderDuracionMeses.get(oi.NE__OrderId__c))) * oi.NE__Qty__c;
				}
			}
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OrderItemMethods.setTotalPriceAndCost', 'BI_EN', exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Cristina Rodríguez
    Company:       Accenture
    Description:   Refactor from NECheckOrderItems.trigger
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    23/05/2017                      Cristina Rodríguez          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void setEconomicFeasibility(List<NE__OrderItem__c> news, List<NE__OrderItem__c> olds, Map<Id, Opportunity> mapOppsToUpd, list<Opportunity> listOfOptyToUpd)
    {
        Map<Id, Opportunity> myMapAux = BI_O4_OrderItemMethods.updateOpptyForApproval(news, olds);
            
        for (Opportunity opp: myMapAux.values()){

            if (mapOppsToUpd.containsKey(opp.Id)){

                mapOppsToUpd.get(opp.Id).BI_Oferta_economica__c = opp.BI_Oferta_economica__c;
            }
            else {

                mapOppsToUpd.put(opp.Id, opp);
            }
        }
        
        // FAR 19/09/2016: If Local opportunity is related to a Holding account and has a parent opportunity, it should bypass economic approval
        for (Opportunity opp: mapOppsToUpd.values()){

            if (BI_O4_OrderItemMethods.bypassEconomicApproval(opp) && mapOppsToUpd.get(opp.Id).BI_Oferta_economica__c == Label.BI_EnviarAprobacion) {

                mapOppsToUpd.get(opp.Id).BI_Oferta_economica__c = Label.BI_SinDescuento;
            }
        }
        
        if (!mapOppsToUpd.isEmpty()){

            listOfOptyToUpd.addAll(mapOppsToUpd.values());
        }

        System.debug(LoggingLevel.ERROR, 'listOfOptyToUpd:' + listOfOptyToUpd);
    }

    public static Double convertNullToZero(Double value){

		if(value == null){
			value = 0;
			return value;
		}
		else{
			return value;
		}
	}

}