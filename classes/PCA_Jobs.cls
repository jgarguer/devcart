global class PCA_Jobs implements Schedulable{

	global void execute(SchedulableContext ctx){ 
		System.abortJob(ctx.getTriggerId());
		sendEmails();
	}
	//07/12/2015        Guillermo Muñoz      Prevent the email send in Test class 
	public static void sendEmails(){
		try{
			if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

			//Profile profile = [select Id from Profile where Name = 'Customer Community User Clone' limit 1];
			List<Event> events = [select  Id, Description, Subject, StartDateTime, EndDateTime, DurationInMinutes, ActivityDateTime, ActivityDate from Event where BI_Correo_electronico_enviado__c=false and LastModifiedDate=today and BI_ParentId__c=null limit 100];
			//Set<String> emailAddresses = new Set<String>();
			Map<Id, Event> eventsMap = new Map<Id, Event>(); 	
			for(Event item: events){
				eventsMap.put(item.Id, item);
			}
			List<EventRelation> contacts = [SELECT EventId,Id,RelationId FROM EventRelation WHERE EventId in :eventsMap.keySet()];
			Map<Id, List<Id>> eventRelMap = new Map<Id, List<Id>>();
			Set<Id> contactIds = new Set<Id>();
			for(EventRelation item : contacts){
				if(eventRelMap.containsKey(item.EventId)){
					eventRelMap.get(item.EventId).add(item.RelationId);
				}
				else{
					eventRelMap.put(item.EventId, new List<Id> {item.RelationId});
				}
				if(!contactIds.contains(item.RelationId)){
					contactIds.add(item.RelationId);
				}
			}
			List<BI_Contact_Customer_Portal__c> contactEmails = [select Id, BI_Contacto__r.Email,BI_Contacto__c from BI_Contact_Customer_Portal__c where BI_Contacto__c in :contactIds];
			List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='BI_Comunicacion_nuevo_evento'];
			//system.debug('templates: '+template);
			Map<String, String> emails = new Map<String, String>();
			for(BI_Contact_Customer_Portal__c item : contactEmails){
				emails.put(item.BI_Contacto__c, item.BI_Contacto__r.Email);
			}
			List<Messaging.SingleEmailMessage> emailSend = new List<Messaging.SingleEmailMessage>();
			List<Event> eventsToUpdate = new List<Event>();
			for(Event item: events){
				String[] emailAddresses = new String[]{};
				if(eventRelMap.containsKey(item.Id)){
					if(!eventRelMap.get(item.Id).isEmpty()){
						List<Id> eventRelationIds = eventRelMap.get(item.Id); 
						for(Id itemId : eventRelationIds){
							if(emails.containsKey(itemId)){
								emailAddresses.add(emails.get(itemId));
							}
						}
						if(!emailAddresses.isEmpty()){
							if(!templates.isEmpty()){
								Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
								
								mail.setToAddresses(emailAddresses);
								string body = templates[0].Body;
								system.debug('body: '+body);
								body = body.replace('{!Event.Subject}', item.Subject);
								body = body.replace('{!Event.ActivityDate}', ''+item.ActivityDate);
								//body = body.replace('{!Event.Subject}', item.Subject);
								mail.setHtmlBody(body);
								mail.setSubject(templates[0].Subject);
								mail.setPlainTextBody(body);
					    		emailSend.add(mail);
					    		system.debug('emailAddresses: '+emailAddresses);	
							}
						}
					}
				}
				item.BI_Correo_electronico_enviado__c = true;
				eventsToUpdate.add(item);
			}
			if(!Test.isRunningTest()){
				Messaging.sendEmail(emailSend);
			}
			update eventsToUpdate;
			Datetime currentDate= Datetime.now();
			currentDate=currentDate.addSeconds(300); 
			
			string NombreProceso = 'Send emails';
			String strProg = currentDate.second() + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' + currentDate.day() + ' ' +currentDate.month() + ' ? ' + currentDate.year();
			System.schedule(NombreProceso , strProg, new PCA_Jobs()); 
		
		}catch(Exception Exc){
			BI_LogHelper.generate_BILog('PCA_Jobs.sendEmails', 'Portal Platino', Exc, 'Schedule Class');
		}
	}
}