/************************************************************************************
* New Energy Aborda
* @author           Geraldine Perez
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2016-08-23     Geraldine Perez    Create Class
*************************************************************************************/

@isTest
private class BI_COL_CargarMS_tst {


	Public static User 											objUsuario;
	public static BI_COL_Copy_MS_cls							objCopyMS;
	public static Account                                   	objCuenta;
	public static Account                                   	objCuenta2;
	public static Contact                                   	objContacto;
	public static Campaign                                  	objCampana;
	public static Contract                                  	objContrato;
	public static BI_COL_Anexos__c                          	objAnexos;
	public static Opportunity                               	objOportunidad;
	public static BI_Col_Ciudades__c                        	objCiudad;
	public static BI_Sede__c                                	objSede;
	public static BI_Punto_de_instalacion__c                	objPuntosInsta;
	public static BI_COL_Descripcion_de_servicio__c         	objDesSer;
	public static BI_COL_Modificacion_de_Servicio__c        	objMS;
	public static BI_COL_Modificacion_de_Servicio__c        	objMS1;
	public static BI_COL_Cobro_Parcial__c                   	objCobroParcial;
	public static BI_COL_Btn_Generate_MS_ctr.WrapperMS			objWrapperMS;
	
	public static List <Profile>                            	lstPerfil;
	public static List <User>                               	lstUsuarios;
	public static List <UserRole>                           	lstRoles;
	public static List<Campaign>                            	lstCampana;
	public static List<BI_COL_manage_cons__c >              	lstManege;
	public static List<BI_COL_Modificacion_de_Servicio__c>  	lstMSALTA;
	public static List<BI_COL_Modificacion_de_Servicio__c>  	lstMSPOST;
	public static List<BI_COL_Descripcion_de_servicio__c>   	lstDS;
	public static List<recordType>                          	lstRecTyp;
	public static list<NE__OrderItem__c>						lstOItem;
	public static list<NE__OrderItem__c> 						lstOI;
	public static List<BI_COL_BtnGenerateMSDown_ctr.WrapperMS>     lstWrapper = new List<BI_COL_BtnGenerateMSDown_ctr.WrapperMS>();


 // 	public static void CrearData()
	//{
			
	//		//Cuentas
	//		objCuenta                                       = new Account();
	//		objCuenta.BI_Segment__c                       	= 'Empresas';
 //         	objCuenta.BI_Subsegment_Regional__c           	= 'Mayoristas';
 //         	objCuenta.BI_Subsector__c                     	= 'Banca';
 //         	objCuenta.BI_Sector__c                        	= 'Industria';      
	//		objCuenta.Name                                  = 'prueba';
	//		objCuenta.BI_Country__c                         = 'Colombia';
	//		objCuenta.TGS_Region__c                         = 'América';
	//		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
	//		objCuenta.CurrencyIsoCode                       = 'COP';
	//		objCuenta.BI_Fraude__c                          = false;
	//		insert objCuenta;
	//		system.debug('datos Cuenta '+objCuenta);
			
	//		objCuenta2                                       = new Account();
	//		objCuenta2.BI_Segment__c                       	 = 'Empresas';
 //         	objCuenta2.BI_Subsegment_Regional__c           	 = 'Mayoristas';
 //         	objCuenta2.BI_Subsector__c                     	 = 'Banca';
 //         	objCuenta2.BI_Sector__c                        	 = 'Industria';
	//		objCuenta2.Name                                  = 'prueba';
	//		objCuenta2.BI_Country__c                         = 'Colombia';
	//		objCuenta2.TGS_Region__c                         = 'América';
	//		objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
	//		objCuenta2.CurrencyIsoCode                       = 'COP';
	//		objCuenta2.BI_Fraude__c                          = true;
	//		insert objCuenta2;
			

	//		//Oportunidad
	//		objOportunidad                                          = new Opportunity();
	//		objOportunidad.Name                                     = 'prueba opp';
	//		objOportunidad.AccountId                                = objCuenta.Id;
	//		objOportunidad.BI_Country__c                            = 'Colombia';
	//		objOportunidad.CloseDate                                = System.today().addDays(+5);
	//		objOportunidad.StageName                                = 'F5 - Solution Definition';
	//		objOportunidad.CurrencyIsoCode                          = 'COP';
	//		objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
	//		objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
	//		insert objOportunidad;
	//		system.debug('Datos Oportunidad'+objOportunidad);
			
			
	//		//Ciudad
	//		objCiudad = new BI_Col_Ciudades__c ();
	//		objCiudad.Name                      = 'Test City';
	//		objCiudad.BI_COL_Pais__c            = 'Test Country';
	//		objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
	//		insert objCiudad;
	//		System.debug('Datos Ciudad ===> '+objSede);
			
	//		//Contactos
	//		objContacto                                     = new Contact();
	//		objContacto.LastName                            = 'Test';
	//		objContacto.BI_Country__c                       = 'Colombia';
	//		objContacto.CurrencyIsoCode                     = 'COP'; 
	//		objContacto.AccountId                           = objCuenta.Id;
	//		objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
	//		objContacto.Phone 								= '12345678';							
	//		objContacto.Email 								= 'correo@email.com';	
	//		objContacto.MobilePhone 						= '0987654321';
	//		objContacto.BI_COL_Direccion_oficina__c 		= 'nsdihweufhcfdnvldfk';
	//		objContacto.BI_COL_Ciudad_Depto_contacto__c 	= objCiudad.id;
	//		objContacto.BI_Tipo_de_contacto__c 				= 'Autorizado';
	//		Insert objContacto;
	//		//Direccion
	//		objSede                                 = new BI_Sede__c();
	//		objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
	//		objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
	//		objSede.BI_Localidad__c                 = 'Test Local';
	//		objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
	//		objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
	//		objSede.BI_Country__c                   = 'Colombia';
	//		objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
	//		objSede.BI_Codigo_postal__c             = '12356';
	//		objSede.Estado__c 		 				= 'Validado por callejero';	
	//		insert objSede;
	//		System.debug('Datos Sedes ===> '+objSede);
			
	//		//Sede
	//		objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
	//		objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
	//		objPuntosInsta.BI_Sede__c          = objSede.Id;
	//		objPuntosInsta.BI_Contacto__c      = objContacto.id;
	//		objPuntosInsta.Name 				='Prueba Charra';
	//		insert objPuntosInsta;
	//		System.debug('Datos Sucursales ===> '+objPuntosInsta);

	//		Factura__c Factura = new Factura__c ();
	//		Factura.Activo__c = true;
	//		Factura.Cliente__c = objCuenta.Id;
	//		Factura.Fecha_Inicio__c = Date.today();
	//		Factura.Fecha_Fin__c = Date.today() + 20;
	//		Factura.Inicio_de_Facturaci_n__c = Date.today();
	//		Factura.IVA__c = '16%';
	//		Factura.Sociedad_Facturadora__c = 'GTM';
	//		Factura.Requiere_Anexo__c = 'SI';
	//		RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente'];
	//		Factura.RecordTypeId = tiporegistro.Id;
	//		insert Factura;

	//		//Producto COmercial
	//		NE__Product__c ProCom = new NE__Product__c();
	//      	ProCom.Offer_SubFamily__c = 'a-test;';
	//      	ProCom.Offer_Family__c = 'b-test';
	//      	ProCom.BI_COL_CodigoP__c = 'P - 000000000';
	      	
	//      	insert ProCom;
	//		//Order
	//		NE__Order__c NEORDER =  new NE__Order__c(); 
	//		NEORDER.NE__OptyId__c = objOportunidad.Id;
	//		NEORDER.NE__OrderStatus__c = 'Active';
	//		insert NEORDER;
	//		system.debug('####OOOrder#####' +NEORDER);

	//		lstOI = new list<NE__OrderItem__c>();
	//		//OrderItem1
	//		NE__OrderItem__c objOrderItem1  = new NE__OrderItem__c ();
	//		objOrderItem1.NE__OrderId__c = NEORDER.id; 
	//		objOrderItem1.NE__ProdId__c = ProCom.id;
	//		objOrderItem1.Factura__c = Factura.Id;
	//		objOrderItem1.NE__Qty__c = 4;
	//		lstOI.add(objOrderItem1);
	//		//insert objOrderItem1;
	//		system.debug('####order item#####' +objOrderItem1);

	//		//OrderItem2
	//		NE__OrderItem__c objOrderItem2  = new NE__OrderItem__c ();
	//		objOrderItem2.NE__OrderId__c = NEORDER.id; 
	//		objOrderItem2.Factura__c = Factura.Id;
	//		objOrderItem2.NE__ProdId__c = ProCom.id;
	//		objOrderItem2.NE__Qty__c = 1;
	//		lstOI.add(objOrderItem2);			
	//		//insert objOrderItem2;
	//		system.debug('####order item#####' +objOrderItem2);

	//		//OrderItem3
	//		NE__OrderItem__c objOrderItem3  = new NE__OrderItem__c ();
	//		objOrderItem3.NE__OrderId__c = NEORDER.id; 
	//		objOrderItem3.Factura__c = Factura.Id;
	//		objOrderItem3.NE__Qty__c = 12;
	//		objOrderItem3.NE__ProdId__c = ProCom.id;
	//		lstOI.add(objOrderItem3);			

	//		insert lstOI;
	//		system.debug('####order item#####' + lstOI);
			
	//		//DS
	//		lstDS 												= new List<BI_COL_Descripcion_de_servicio__c>(); 
	//		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
	//		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;		
	//		objDesSer.BI_COL_Producto_Telefonica__c				= lstOI[0].Id;             
 //           objDesSer.BI_COL_Sede_Origen__c 				    = objPuntosInsta.Id;  
	//		System.debug('Data obj DS ====> '+ objDesSer);
	//		lstDS.add(objDesSer);
	//		insert lstDS;   
	//		System.debug('Data list DS ====> '+ lstDS);
			
			//MS
			//System.debug('Data DS ID====> '+ objDesSer.Id);
			//lstMSALTA 									= new List<BI_COL_Modificacion_de_Servicio__c>();
			//objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
			//objMS.BI_COL_Codigo_unico_servicio__c       = lstDS[0].Id;
			//objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
			//objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
			//objMS.BI_COL_Bloqueado__c                   = false;
			//objMS.BI_COL_Estado__c                      = 'Activa';
			//objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			//objMs.BI_COL_Duracion_meses__c = 12;
			//lstMSALTA.add(objMS);
			//insert lstMSALTA;
			////MS
			//lstMSPOST									= new List<BI_COL_Modificacion_de_Servicio__c>();
			//objMS1   									= new BI_COL_Modificacion_de_Servicio__c();
			//objMS1.BI_COL_Codigo_unico_servicio__c       = lstDS[0].Id;
			//objMS1.BI_COL_Clasificacion_Servicio__c      = 'CAMBIO DE PLAN';
			//objMS1.BI_COL_Oportunidad__c                 = objOportunidad.Id;
			//objMS1.BI_COL_Bloqueado__c                   = false;
			//objMS1.BI_COL_Estado__c                      = 'Pendiente';
			//objMs1.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			//objMs1.BI_COL_Duracion_meses__c = 1;
			//lstMSPOST.add(objMS1);
			//insert lstMSPOST;
			//system.debug('Data objMs ===>'+objMs);



			
		

	//}
	
	static testMethod void ReadFile_TEST() {
		
		Test.StartTest();		

			Opportunity objOppty = new Opportunity();
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(objOppty);
			BI_COL_CargarMS_ctr Controller = new BI_COL_CargarMS_ctr(stdCtrl);

			blob b = [select body from staticResource where name = 'BI_COL_Plantilla_CargaMs_TEST'].body;
			Controller.bloContentFile = b;
			Controller.ReadFile();
		Test.StopTest();
	}

	static testMethod void ReadFile_TEST1() {
		
		Test.StartTest();	

			Opportunity objOppty = new Opportunity();
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(objOppty);
			BI_COL_CargarMS_ctr Controller = new BI_COL_CargarMS_ctr(stdCtrl);

			blob b = [select body from staticResource where name = 'BI_COL_Plantilla_CargaMs_TEST_Error'].body;
			Controller.bloContentFile = b;
			Controller.ReadFile();
		Test.StopTest();
	}


	
	
}