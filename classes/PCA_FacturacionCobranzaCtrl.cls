public with sharing class PCA_FacturacionCobranzaCtrl extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Controller of Facturación & Cobranza page
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    29/08/2016        Jose Miguel Fierro   Modified "loadInfo"
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String lastModification {get;set;}
    public String lastModificationFact {get;set;}
    
    public String nameFS;
    public String nameObject                    {get; set;}
    public String accountFieldName;
    
    public String nameFS1;
    public String nameObject1                   {get; set;}
    public String accountFieldName1;
    
    public String nameFS2;
    public String nameObject2                   {get; set;}
    public String accountFieldName2;
    
    public String searchFinal;
    public map<string,string>  mapVisibility {get;set;}
    public Id AccountId;
    
    public String searchText                    {get; set;}
    public String searchField                   {get; set;}
    public String firstHeader                   {get; set;}
    
    public Integer index                        {get; set;}
    public Integer pageSize                     {get; set;}
    public Integer totalRegs                    {get; set;}
    
    public Boolean OptionA                      {get; set;}
    
    public List<SelectOption> searchFields      {get; set;}
    
    public List<FieldSetHelper.FieldSetRecord> viewRecords  {get; set;}
    public FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    
    public String fieldImage {get;set;}
    public String Mode {get;set;}
    
    public String FieldOrder {get;set;}
    public String Field;    
    public String PageValue {get;set;}
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_FacturacionCobranzaCtrl(){}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of facturas and cobranzas
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    29/08/2016        Jose Miguel Fierro   Separar la comprobación del Estado de Deudas de la comprobación del estado de Facturación
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.index = 1;
            this.pageSize = 10;
            this.searchFinal = null;        
            this.AccountId = BI_AccountHelper.getCurrentAccountId();
            if(AccountId == null){
                return;
            }
            string countryAccount = BI_AccountHelper.getCountryAccount(AccountId);
            
            //List<BI_Actualizaciones_integracion__c> regionsFact = [select Id, BI_Entidad__c, BI_Country__c, BI_UltimaModificacion__c from BI_Actualizaciones_integracion__c where BI_Country__c= : countryAccount AND BI_Entidad__c=:'Facturas'];
            //List<BI_Actualizaciones_integracion__c> regions = [select Id, BI_Entidad__c, BI_Country__c, BI_UltimaModificacion__c from BI_Actualizaciones_integracion__c where BI_Country__c= : countryAccount AND BI_Entidad__c=:'Cobranza'];
            List<BI_Facturas__c> FactDate = [Select Id, BI_Fecha_de_carga__c FROM BI_Facturas__c where BI_Fecha_de_carga__c != null AND BI_Nombre_del_cliente__c = :BI_AccountHelper.getCurrentAccountId() ORDER BY BI_Fecha_de_carga__c DESC LIMIT 1];
            List<BI_Facturacion__c> FactDate2 = [Select Id, BI_Fecha_de_carga__c FROM BI_Facturacion__c where BI_Fecha_de_carga__c != null AND BI_Cliente__c = :BI_AccountHelper.getCurrentAccountId() ORDER BY BI_Fecha_de_carga__c DESC LIMIT 1];
            
            
            if(!FactDate.isEmpty()){
                lastModificationFact = FactDate[0].BI_Fecha_de_carga__c.format(); //Last modified Facturas
                //system.debug('regions' +regions);
            }
            // START JMF 29/08/2016 - Separar Estado de deudas del estado de facturas
            if(!FactDate2.isEmpty()) {
                lastModification = FactDate2[0].BI_Fecha_de_carga__c.format(); //Last modified Estado de deuda
            }
            // END JMF 29/08/2016

            /*if(!regionsFact.isEmpty()){
                lastModificationFact = FactDate[0].BI_Fecha_de_carga__c.format();
            }*/
            
            this.OptionA = true;
            this.nameObject1 = 'BI_Facturas__c';
            this.nameFS1 = 'PCA_MainTableFacturas';
            this.accountFieldName1 = 'BI_Nombre_del_cliente__c';
            
            this.nameObject2 = 'BI_Facturacion__c';
            this.nameFS2 = 'PCA_MainTableFacturacion';
            this.accountFieldName2 = 'BI_Cliente__c';
            
            this.nameFS = this.nameFS1;
            this.nameObject = this.nameObject1;
            this.accountFieldName = this.accountFieldName1;
            mapVisibility = PCA_ProfileHelper.getPermissionSet();
            defineSearch();
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of facturas and cobranzas
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void changeTab(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.searchText = null;
            this.searchFinal = null;
            this.OptionA = this.OptionA ? false : true;
            this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
            this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
            this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
    
            defineSearch();
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.changeTab', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search records
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineRecords(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.searchFinal, accountValue,null);
            
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search views
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineViews(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
            Integer topValue = ((this.index + this.pageSize) > this.totalRegs) ? this.totalRegs : (this.index + this.pageSize - 1);
            
            for (Integer i = (this.index - 1); i < topValue; i++)
            {
                FieldSetHelper.FieldSetRecord iRecord = this.fieldSetRecords.regs[i]; 
                this.viewRecords.add(iRecord);
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineViews', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search criteria
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineSearch(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
        this.searchFields = FieldSetHelper.defineSearchFields(this.nameFS, this.nameObject);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineSearch', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search 
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void searchRecords(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.searchFinal = this.searchField + ';' + this.searchText;
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.searchRecords', 'Portal Platino', Exc, 'Class');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Retrieves true if index of search is not the first one, false otherwise
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasPrevious(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return (this.index != 1);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.getHasPrevious', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Retrieves true if index of search is not the last one, false otherwise
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasNext(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return !((this.index + this.pageSize) > this.totalRegs);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.getHasNext', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Adds a position to the index of search pages
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Next(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.index += this.pageSize;
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.Next', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Decreases a position to the index of search pages
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Previous(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.pageSize = 10;
            this.index -= this.pageSize;
            defineViews();  
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.Previous', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines records in page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public List<SelectOption> getItemPage() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        options.add(new SelectOption('200','200'));
        system.debug('itemPage: '+ options);
        return options;
    }   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show more values in a page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void valuesInPage(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
        this.pageSize = Integer.valueOf(PageValue);
        system.debug('pageSize*: '+this.pageSize);
        defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.valuesInPage', 'Portal Platino', Exc, 'Class');
        }
    }    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Order table with field
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void Order(){
        
        try{/*
            this.searchText = null;
            this.searchFinal = null;
            this.OptionA = this.OptionA ? false : true;
            this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
            this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
            this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
    */          
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            Mode = '';
            defineSearch();
            fieldImage = FieldOrder;
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            //if(this.nameObject == this.nameObject2){
            //this.searchFinal = '::NE__OrderId__r.RecordType.DeveloperName;Asset;'+FieldOrder;
            //}
            if(Field == FieldOrder){
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'DESC');
                FieldOrder = '';
                Mode = 'DESC';
            }else{
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'ASC');
                Mode = 'ASC';
            }
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
            
            Field = this.FieldOrder;
            defineViews();
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', Exc, 'Class');
        }
        
    }   
    /*
    public void createNewCase()
    {
        Case newCase = new Case();
        newCase.AccountId = AccountId;
    }
    */
    
}