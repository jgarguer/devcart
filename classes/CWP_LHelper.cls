public without sharing class CWP_LHelper {
    /*
    Developer Everis
    Function: helper method for portal lightning pages - returns the id of the image which should be displayed in the main page
    *///
    public static map<string, string> getPortalInfo(string forTab, user currentUser, Account currentAccount){
        BI_ImageHome__c RelationImageMovi;
        BI_ImageHome__c RelationImageTel;
        BI_ImageHome__c finalImageTel;
        map<string, string> retMap = new map<string, string>();
        list<BI_ImageHome__c> RelImg = new list<BI_ImageHome__c>();
        system.debug('usuario recibido  ' + currentUser);
        system.debug('con perfil  ' + currentUser.Profile.Name);
        if(currentAccount.BI_Segment__c!=null){
            system.debug('segmento  ' + currentAccount.BI_Segment__c);
            RelImg = getImageHome(forTab, 'General', currentAccount.BI_Segment__c, currentUser);
           /* if(currentUser.Profile.Name.startsWith('BI')){
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c =: forTab AND  RecordType.Name= 'General' AND BI_Segment__c=: currentAccount.BI_Segment__c LIMIT 1];
            }else if(currentUser.Profile.Name.startsWith('TGS')){
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c =: forTab AND  RecordType.Name= 'General' AND BI_Segment__c=: currentAccount.BI_Segment__c AND CWP_ManagedByTGS__c= true LIMIT 1];
            }*/
            system.debug('valor del objeto que imagenes relacionadas 1  '+ relImg);
            if(!RelImg.isEmpty()) {
                if(RelImg[0].BI_Segment__c!=null){
                    if(RelImg[0].BI_Segment__c=='Empresas'){
                        RelationImageTel = RelImg[0];
                    }else if(RelImg[0].BI_Segment__c=='Negocios'){
                        RelationImageMovi = RelImg[0];
                    }
                }
            }
        }else{
           // RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c =: forTab AND  RecordType.Name= 'General'  AND BI_Segment__c='Empresas' LIMIT 1];
            RelImg =  getImageHome(forTab, 'General', 'Empresas', currentUser);
            system.debug('valor del objeto que imagenes relacionadas 2  '+ relImg);
            if(!RelImg.isEmpty()){
                RelationImageTel = RelImg[0];
            }else{
               RelImg =  getImageHome(forTab, 'General', 'Negocios', currentUser);
               // RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c =: forTab AND  RecordType.Name= 'General'  AND BI_Segment__c='Negocios' LIMIT 1];
                system.debug('valor del objeto que imagenes relacionadas 3  '+ relImg);
                if(!RelImg.isEmpty()){
                    RelationImageMovi = RelImg[0];
                }
            }
        }
        system.debug('relationImageTel  ' + relationImageTel);
        system.debug('RelationImageMovi  ' + RelationImageMovi);
        list<Attachment> AttTarget;
        if(RelationImageTel!=null){
            finalImageTel = RelationImageTel;
            AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageTel.Id Limit 1];
            system.debug('att target  ' + attTarget);
            if(AttTarget!=null){
                retMap.put('imgId', attTarget[0].id);
            }
        }else if(RelationImageMovi!=null){
            finalImageTel = RelationImageMovi;
            AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageMovi.Id Limit 1];
            system.debug('att target  ' + attTarget);
            if(AttTarget!=null){
                retMap.put('imgId', attTarget[0].id);
            }
        }
        if(finalImageTel != null){
            if(finalImageTel.BI_Titulo__c != null){
                retMap.put('title', finalImageTel.BI_Titulo__c);
            }
            if(finalImageTel.BI_Informacion_Menu__c != null){
                retMap.put('informationMenu', finalImageTel.BI_Informacion_Menu__c);
            }
        }
    system.debug('retMap de va de vuelta desde lHelper  ' + retMap);
        return retMap;
    }
    
    private static list<BI_ImageHome__c> getImageHome(string forTab, string rtName, string bySegment, user forWhom){
        list<BI_ImageHome__c> retList = new list<BI_ImageHome__c>();
        if(forWhom.Profile.Name.startsWith('BI')){
            retList = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c =: forTab AND  RecordType.Name=: rtName AND BI_Segment__c=: bySegment LIMIT 1];
        }else if(forWhom.Profile.Name.startsWith('TGS')){
            retList = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c =: forTab AND  RecordType.Name=: rtName AND BI_Segment__c=: bySegment AND CWP_ManagedByTGS__c= true LIMIT 1];
        }
        return retList;
    }
    
    /*
    Developer Everis
    Function: method used to get the information of the currently logged user.
    */
    public static User getCurrentUser(user rcvUser){
        if(rcvUser == null){
            rcvUser = [SELECT Id, AccountId, ProfileId, Profile.Name, Pais__c, ContactId, Contact.BI_Cuenta_activa_en_portal__c FROM User WHERE Id =: userInfo.getUserId()];
        }
        return rcvUser;
    }
    
    /*
    Developer Everis
    Function: method used to get information related to the account of the current user. This method is needed due to issues encontered when using __r relation fields
    */
    public static Account getCurrentAccount(id accountId){
        Account retAcc = [SELECT Id, OwnerId, BI_Segment__c, BI_Country__c FROM Account WHERE Id =: accountId];
        return retAcc;
    }
    
    /*
    Developer Everis
    Function: method to get the list of days with its events for each month for the calendar
    */
    public static list<list<calendarDayWrap>> getDayList(integer month, integer year, integer firstWeekDay, map<date, list<Event>> eventByDateMap){
        list<list<calendarDayWrap>> result;
        map<string, integer> dayNumberMap = new map<string, integer>{
            'Sun'=>0,
            'Mon'=>1,
            'Tue'=>2,
            'Wed'=>3,
            'Thu'=>4,
            'Fri'=>5,
            'Sat'=>6
        };
        integer priorMonth = month-1;
        integer nextMonth = month+1;
        integer yearPriorMonth = year;
        integer yearNextMonth = year;
        
        if(month==1){
            priorMonth=12;
            yearPriorMonth--;
        }
        if(month==12){
            nextMonth=1;
            yearNextMonth++;
        }
        Integer daysPriorMonth = Date.daysInMonth(yearPriorMonth, priorMonth);
        Integer daysThisMonth = Date.daysInMonth(year, month);
        Integer daysNextMonth = Date.daysInMonth(yearNextMonth, nextMonth);
        system.debug('mes anterior  ' + daysPriorMonth + '  este mes    ' + daysThisMonth+' mes siguiente   ' + daysNextMonth);
        
        Date firstDayOfMonth = Date.newInstance(year, month, 1);
        DateTime firstDateTimeOfMonth = (DateTime)firstDayOfMonth;
        String dayOfWeek = firstDateTimeOfMonth.format('E');
        system.debug('dia de la semana  ' + dayOfWeek);
        
        integer weekDay = dayNumberMap.get(dayOfWeek);
        integer offset;
        if(firstWeekDay<=weekDay){
            offSet = weekDay-firstWeekDay;
        }else{
            offSet = 7-firstWeekDay+weekDay;
        }
        system.debug('dias de avance de offset  '+ offSet);
        integer priorMonthStart = daysPriorMonth-offSet+1;
        list<calendarDayWrap> w1 = new list<calendarDayWrap>();
        list<calendarDayWrap> w2 = new list<calendarDayWrap>();
        list<calendarDayWrap> w3 = new list<calendarDayWrap>();
        list<calendarDayWrap> w4 = new list<calendarDayWrap>();
        list<calendarDayWrap> w5 = new list<calendarDayWrap>();
        list<calendarDayWrap> w6 = new list<calendarDayWrap>();
        w1.addAll(fillWeek(priorMonthStart, daysPriorMonth, priorMonth, yearPriorMonth, false, eventByDateMap));
        integer restDays = 7-w1.size();
        w1.addAll(fillWeek(1, restDays, month, year, true, eventByDateMap));
        system.debug('semana 1  ' + w1);
        
        w2.addAll(fillWeek(w1[6].day+1, w1[6].day+7, month, year, true, eventByDateMap));
        system.debug('semana 2  ' + w2);
        w3.addAll(fillWeek(w2[6].day+1, w2[6].day+7, month, year, true, eventByDateMap));
        system.debug('semana 3  ' + w3);
        w4.addAll(fillWeek(w3[6].day+1, w3[6].day+7, month, year, true, eventByDateMap));
        system.debug('semana 4  ' + w4);
        integer remainingDays = daysThisMonth-w4[6].day;
        if(remainingDays>7){
            w5.addAll(fillWeek(w4[6].day+1, w4[6].day+7, month, year, true, eventByDateMap));
            w6.addAll(fillWeek(w5[6].day+1, daysThisMonth, month, year, true, eventByDateMap));
            w6.addAll(fillWeek(1, 7-w6.size(), nextMonth, yearNextMonth, false, eventByDateMap));
            result = new list<list<calendarDayWrap>>{w1, w2, w3, w4, w5, w6};
        }else{
            w5.addAll(fillWeek(w4[6].day+1, daysThisMonth, month, year, true, eventByDateMap));
            w5.addAll(fillWeek(1, 7-w5.size(), nextMonth, yearNextMonth, false, eventByDateMap));
            result = new list<list<calendarDayWrap>>{w1, w2, w3, w4, w5};
            system.debug('semana 5  ' + w5);
        }
        
        return result;
    }
    
    private static list<calendarDayWrap> fillWeek(integer inicio, integer fin, integer month, integer year, boolean isCurrent, map<date, list<Event>> eventByDateMap){
        system.debug('mapa de eventos clasificados por fecha    ' + eventByDateMap);
        list<calendarDayWrap> retList = new list<calendarDayWrap>();
        User usuarioPortal= [Select id, FullPhotoUrl, name,Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c, ContactId, MobilePhone, Usuario_BO__c, pass_BO__c, Contact.Name  from User where id=:UserInfo.getUserId()];
        for(integer i = inicio; i<=fin; i++){
            Date newDay = Date.NewInstance(year, month, i);
            calendarDayWrap calendarEntry = new calendarDayWrap(i, newDay, isCurrent);
            if (newDay < DateTime.now().Date()) {
                calendarEntry.isEnabled = false;
            } else {
                calendarEntry.isEnabled = true;
            }
            if(eventByDateMap.containskey(newDay)){
                for(Event j : eventByDateMap.get(newDay)){
                    if(j.createdById ==usuarioPortal.Id )
                    calendarEntry.addEvent(new calendarEventWrap(j,true));
                    else calendarEntry.addEvent(new calendarEventWrap(j,false));
                }
            }
            retList.add(calendarEntry);
        }
        return retList;
    }
    
    public static map<date, list<Event>> sortEventsByDay(list<Event> eventList){
        map<date, list<Event>> retMap = new map<date, list<Event>>();
        for(Event i : eventList){
            if(retMap.containskey(i.ActivityDate)){
                retMap.get(i.ActivityDate).add(i);
            }else{
                retMap.put(i.ActivityDate, new list<Event>{i});
            }
        }
        return retMap;
    }
    
    /*
    Developer Everis
    Function: wrapper defined to days and events of the calendar of relationship management tab in CWP
    */
    public class calendarDayWrap{
        @auraEnabled
        public integer day{get; set;}
        @auraEnabled
        public date dayDate{get; set;}
        @auraEnabled
        public boolean isCurrent{get; set;}
        @auraEnabled
        public boolean isToday{get; set;}
        @auraEnabled
        public boolean isEnabled{get; set;}
        @auraEnabled
        public list<calendarEventWrap> eventList{get; set;}
        
        public calendarDayWrap(integer day, date dayDate, boolean isCurrent){
            this.day = day;
            this.dayDate = dayDate;
            this.isCurrent = isCurrent;
            this.eventList = new list<calendarEventWrap>();
            this.isToday = date.today().isSameDay(dayDate);
        }
        
        public void addEvent(calendarEventWrap newEvent){
            eventList.add(newEvent);
        }
    
    }
    /*
    Developer Everis
    Function: wrapper defined to display events in the calendar
    */
    public class calendarEventWrap{
        @auraEnabled
        public id eventId{get; set;}
        @auraEnabled
        public string interval{get; set;}
        @auraEnabled
        public string eventName{get; set;}
        @auraEnabled
        public boolean  isPublic{get; set;}
        
        public calendarEventWrap(id Eventid, string interval, string eventName){
            this.eventId = eventId;
            this.interval = interval;
            this.eventName = eventName;
        }
        
        public calendarEventWrap(Event theEvent, boolean p){
            this.eventId = theEvent.id;
            this.interval = theEvent.startDateTime.format('HH:mm')+ ' - ' +theEvent.endDateTime.format('HH:mm');
            this.eventName = theEvent.subject;
              this.isPublic = p;
        }
    }
    
    /*
    Developer Everis
    Function: wrapper defined to display the elements of the table in the services page for portal platino
  */
    public class inventoryWrap{
        @auraEnabled
        public string Name{get; set;}
        @auraEnabled
        public string Id{get; set;}
        @auraEnabled
        public string Description{get; set;}
        @auraEnabled
        public integer amount{get; set;}
        
        public inventoryWrap(string Name, string Id, string Description, integer amount){
            this.Name=Name;
            this.Id=Id;
            this.Description=Description;
            this.amount=amount;
        }
        
        public void addAmount(){
            this.amount++;
        }
       
        
    }
    
    /*
    Developer Everis
    Function: wrapper defined to display home page images
    */
    public class imageHomeWrapper{
        @auraEnabled
        public Boolean externalURL{get; set;}
        @auraEnabled
        public BI_ImageHome__c ihome{get; set;}
        @auraEnabled
        public List<String> attachments{get; set;}
        @auraEnabled
        public list<string> attachUrl{get; set;}
        
        public ImageHomeWrapper(BI_ImageHome__c ihome, List<String> attachments, string url){
            this.ihome = ihome;
            this.attachments = attachments;
            this.externalURL = false;
            this.attachUrl = new list<string>();
            for(string i : attachments){
                attachUrl.add(url + i);
            }
            if (ihome.BI_Url__c != null){
                if ((ihome.BI_Url__c.startsWith('www.')) ||
                    (ihome.BI_Url__c.startsWith('http:')) ||
                    (ihome.BI_Url__c.startsWith('https:'))){
                    this.externalURL = true;
                }
            }
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Belén Coronado
    *
    * Company:       everis
    *
    * Description:   Build select options for product categorization in tickets and orders filter
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 09/05/2017                      Belén Coronado                                              Initial version
    */  
    public static map<string, map<string, set<CWP_CasesController.PickListItem>>> getFilterFromCatProd(list<TGS_GME_GM_Product_Categorization__c> rcvList){
        map<string, map<string, set<CWP_CasesController.PickListItem>>> retMap = new map<string, map<string, set<CWP_CasesController.PickListItem>>>();
        set<string> pl1 = new set<string>();
        set<string> pl2 = new set<string>();
        set<string> pl3 = new set<string>();
        map<string, set<CWP_CasesController.PickListItem>> lvl1 = new map<string, set<CWP_CasesController.PickListItem>>{userInfo.getUserId() => new set<CWP_CasesController.PickListItem>{}};//contiene id del usuario (porque alguno hay que poner) y lista de opciones de de padre
        map<string, set<CWP_CasesController.PickListItem>> lvl2 = new map<string, set<CWP_CasesController.PickListItem>>();//contiene catalog category parent y lista de catalog category child
        map<string, set<CWP_CasesController.PickListItem>> lvl3 = new map<string, set<CWP_CasesController.PickListItem>>();//contiene catalog category child y lista de productos
        for(TGS_GME_GM_Product_Categorization__c i : rcvList){
            system.debug('getFilterFromTierDep --> i.TGS_Categorization_tier_1__c: ' + i.TGS_Categorization_tier_1__c);
            //rellenado del mapa que contiene el id del usuario y lista de opciones de catalog category parent (poner la id del usuario es porque hay que poner alguna id, funcionalmente no tiene sentido)
            if(i.TGS_Categorization_tier_1__c != null && !pl1.contains(i.TGS_Categorization_tier_1__c)){
                system.debug('TGS_Product_Tier_1__c != null && !pl1.contains(i.TGS_Product_Tier_1__c) antes: ' + lvl1);
                lvl1.get(UserInfo.getUserId()).add(new CWP_CasesController.PickListItem(i.TGS_Categorization_tier_1__c, i.TGS_Categorization_tier_1__c));
                pl1.add(i.TGS_Categorization_tier_1__c);
                system.debug('TGS_Categorization_tier_1__c != null && !pl1.contains(i.TGS_Categorization_tier_1__c) después: ' + lvl1);
            }
            
            //rellenado del mapa que contiene opciones de catalog category parent y lista de opciones de catalog category child
            if(!string.isBlank(i.TGS_Categorization_tier_1__c) && lvl2.containskey(i.TGS_Categorization_tier_1__c)){
                if(i.TGS_Categorization_tier_2__c!= null && !pl2.contains(i.TGS_Categorization_tier_2__c)){
                    lvl2.get(i.TGS_Categorization_tier_1__c).add(new CWP_CasesController.PickListItem(i.TGS_Categorization_tier_2__c, i.TGS_Categorization_tier_2__c));
                    pl2.add(i.TGS_Categorization_tier_2__c);
                }
            }else{
                if(!string.isBlank(i.TGS_Categorization_tier_1__c) && !string.isBlank(i.TGS_Categorization_tier_2__c)){
                    lvl2.put(i.TGS_Categorization_tier_1__c, new set<CWP_CasesController.PickListItem>{new CWP_CasesController.PickListItem(i.TGS_Categorization_tier_2__c, i.TGS_Categorization_tier_2__c)});
                    pl2.add(i.TGS_Categorization_tier_2__c);
                }
            }
            
            //rellenado de mapa que contiene opciones de catalog category child y lista de opciones de productos
            if(!string.isBlank(i.TGS_Categorization_tier_2__c) && lvl3.containskey(i.TGS_Categorization_tier_2__c)){
                if(!string.isBlank(i.TGS_Categorization_tier_3__c) && !pl3.contains(i.TGS_Categorization_tier_3__c)){
                    lvl3.get(i.TGS_Categorization_tier_2__c).add(new CWP_CasesController.PickListItem(i.TGS_Categorization_tier_3__c, i.TGS_Categorization_tier_3__c));
                    pl3.add(i.TGS_Categorization_tier_3__c);
                }
            }else{
                if(!string.isBlank(i.TGS_Categorization_tier_2__c) && !string.isBlank(i.TGS_Categorization_tier_3__c)){
                    lvl3.put(i.TGS_Categorization_tier_2__c, new set<CWP_CasesController.PickListItem>{new CWP_CasesController.PickListItem(i.TGS_Categorization_tier_3__c, i.TGS_Categorization_tier_3__c)});
                    pl3.add(i.TGS_Categorization_tier_3__c);
                }
            }
        }   
        retMap.put('lvl1',lvl1);
        retMap.put('lvl2',lvl2);
        retMap.put('lvl3',lvl3);
        return retMap;
    }
    
}