<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>BI_EN: Esta aplicación contiene todas los objetos de configuración del portal platino de BI_EN.</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Portal platino</label>
    <tabs>BI_ImageHome__c</tabs>
    <tabs>Market_Place_PP__c</tabs>
    <tabs>BI_Catalogo_PP__c</tabs>
    <tabs>BI_Multimarca_PP__c</tabs>
    <tabs>BI_Contact_Customer_Portal__c</tabs>
    <tabs>BI_Permisos_PP__c</tabs>
    <tabs>Hierarchy__c</tabs>
    <tabs>BI_Portal_local__c</tabs>
    <tabs>Reportes_PP__c</tabs>
    <tabs>BI_Videoteca__c</tabs>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Solution</tabs>
    <tabs>standard-Idea</tabs>
    <tabs>Certa_SCP__SCP_Criteria_DCM__c</tabs>
    <tabs>BI_Service_Tracking_URL_PP__c</tabs>
    <tabs>Monitor_CSB_2</tabs>
    <tabs>E24P_Account_Plan__c</tabs>
    <tabs>E3_Matriz_de_Complejidad__c</tabs>
    <tabs>E3_MC_Configuracion__c</tabs>
    <tabs>E3_Big_Deal__c</tabs>
    <tabs>E24P_Aplicabilidad__c</tabs>
    <tabs>E24P_Contacto_Account_Planning__c</tabs>
    <tabs>E24P_DFE__c</tabs>
    <tabs>E24P_Competidor__c</tabs>
    <tabs>E24P_Share_of_Wallet__c</tabs>
</CustomApplication>
