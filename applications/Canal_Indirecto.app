<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Canal Indirecto</label>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Case</tabs>
    <tabs>standard-report</tabs>
    <tabs>BI_FVI_Trafico__c</tabs>
    <tabs>Monitor_CSB_2</tabs>
    <tabs>E24P_Account_Plan__c</tabs>
    <tabs>E3_Matriz_de_Complejidad__c</tabs>
    <tabs>E3_MC_Configuracion__c</tabs>
    <tabs>E3_Big_Deal__c</tabs>
    <tabs>E24P_Aplicabilidad__c</tabs>
    <tabs>E24P_Contacto_Account_Planning__c</tabs>
    <tabs>E24P_DFE__c</tabs>
    <tabs>E24P_Competidor__c</tabs>
    <tabs>E24P_Share_of_Wallet__c</tabs>
</CustomApplication>
