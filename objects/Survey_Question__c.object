<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>BI_FVI_CSI_Question__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indica si es la pregunta para CSI (ISC Indice de satisfacción de cliente) se utiliza en los Desempeños del FVI, Solo puede estar 1 activo por encuesta.</description>
        <externalId>false</externalId>
        <label>CSI Question</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Choices__c</fullName>
        <externalId>false</externalId>
        <label>Choices</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>OrderNumber__c</fullName>
        <description>Field to specify in which position the question is in the survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Meter la posición de la pregunta en el estudio aquí</inlineHelpText>
        <label>OrderNumber</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Question__c</fullName>
        <externalId>false</externalId>
        <label>Question</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Required__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Is the question required?</description>
        <externalId>false</externalId>
        <inlineHelpText>Marque esta casilla si desea que el tomador a de estar obligado a tomar la pregunta</inlineHelpText>
        <label>Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Survey__c</fullName>
        <externalId>false</externalId>
        <label>Survey</label>
        <referenceTo>Survey__c</referenceTo>
        <relationshipLabel>SurveyQuestions</relationshipLabel>
        <relationshipName>SurveyQuestions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Total_Responses__c</fullName>
        <externalId>false</externalId>
        <label>Total Responses</label>
        <summaryForeignKey>SurveyQuestionResponse__c.Survey_Question__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Single Select--Vertical</fullName>
                    <default>false</default>
                    <label>Single Select--Vertical</label>
                </value>
                <value>
                    <fullName>Single Select--Horizontal</fullName>
                    <default>false</default>
                    <label>Single Select--Horizontal</label>
                </value>
                <value>
                    <fullName>Multi-Select--Vertical</fullName>
                    <default>false</default>
                    <label>Multi-Select--Vertical</label>
                </value>
                <value>
                    <fullName>Free Text</fullName>
                    <default>false</default>
                    <label>Free Text</label>
                </value>
                <value>
                    <fullName>Hidden</fullName>
                    <default>false</default>
                    <label>Hidden</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Survey Question</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Encuesta_de_cierre_de_caso</fullName>
        <columns>OrderNumber__c</columns>
        <columns>NAME</columns>
        <columns>Question__c</columns>
        <columns>Type__c</columns>
        <columns>Choices__c</columns>
        <columns>Required__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Survey__c</field>
            <operation>equals</operation>
            <value>BI2 :: Encuesta de cierre de caso</value>
        </filters>
        <label>Encuesta de cierre de caso</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Survey Questions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>BI_FVI_Question_Without_Choises</fullName>
        <active>true</active>
        <errorConditionFormula>AND ( OR (ISPICKVAL( Type__c , &apos;Single Select--Vertical&apos;), 
 ISPICKVAL( Type__c , &apos;Single Select--Horizontal&apos;), 
 ISPICKVAL( Type__c , &apos;Multi-Select--Vertical&apos;)) , OR(  ISNULL(Choices__c ),Choices__c =&quot;&quot;  ))</errorConditionFormula>
        <errorDisplayField>Choices__c</errorDisplayField>
        <errorMessage>Debe Incluir valores en la Opciones de respuesta</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
