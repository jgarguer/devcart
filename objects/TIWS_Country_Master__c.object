<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Objeto creado para poder incluir los datos de segmentacion de TIWS por cada pais.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>TIWS_ARPU__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Average revenue per user (ARPU). Total recurring (service) revenue generated per connection per month in the period. 

Despite the acronym, the metric is strictly average revenue per connection, not per subscriber.</inlineHelpText>
        <label>ARPU</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TIWS_GDP__c</fullName>
        <description>Producto Interior Bruto del país</description>
        <externalId>false</externalId>
        <label>GDP (U.S.D)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TIWS_GDPpercapita__c</fullName>
        <description>Producto Interior Bruto del país por habitantes</description>
        <externalId>false</externalId>
        <label>GDP per capita (U.S.D)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TIWS_GSMA_Information__c</fullName>
        <externalId>false</externalId>
        <label>GSMA Information</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>TIWS_GSMA_Website__c</fullName>
        <externalId>false</externalId>
        <label>GSMA Website</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>TIWS_Group__c</fullName>
        <externalId>false</externalId>
        <label>Group</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                    <label>1</label>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                    <label>2</label>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                    <label>3</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>TIWS_Human_Development_Index__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The HDI was created to emphasize that people and their capabilities should be the ultimate criteria for assessing the development of a country, not economic growth alone.</inlineHelpText>
        <label>Human Development Index</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>VERY HIGH HUMAN DEVELOPMENT</fullName>
                    <default>false</default>
                    <label>VERY HIGH HUMAN DEVELOPMENT</label>
                </value>
                <value>
                    <fullName>HIGH HUMAN DEVELOPMENT</fullName>
                    <default>false</default>
                    <label>HIGH HUMAN DEVELOPMENT</label>
                </value>
                <value>
                    <fullName>MEDIUM HUMAN DEVELOPMENT</fullName>
                    <default>false</default>
                    <label>MEDIUM HUMAN DEVELOPMENT</label>
                </value>
                <value>
                    <fullName>LOW HUMAN DEVELOPMENT</fullName>
                    <default>false</default>
                    <label>LOW HUMAN DEVELOPMENT</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>TIWS_ISO_3_Code__c</fullName>
        <externalId>true</externalId>
        <label>ISO-3 Code</label>
        <length>3</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TIWS_Minutos_de_uso__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Number of annual voice minutes per country</inlineHelpText>
        <label>Minutes of Use</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TIWS_N_de_conexiones_moviles__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Number of mobile connections per country</inlineHelpText>
        <label>Mobile Conexions</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TIWS_OB_Footprint__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Country with TEF footprint</inlineHelpText>
        <label>OB Footprint</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TIWS_Political_Risk_Index__c</fullName>
        <description>El Índice de Riesgo Político es un índice propietario que mide el nivel de riesgo que plantean a los gobiernos, corporaciones e inversionistas, basado en una miríada de factores políticos y económicos. 
El riesgo político 
El índice se calcula utilizando una metodología establecida basada en diversos criterios *, incluyendo la siguiente consideración: estabilidad política, representación política, responsabilidad democrática, libertad de expresión, seguridad y delincuencia, riesgo de conflicto, desarrollo humano, jurisprudencia y transparencia normativa, riesgo económico, Consideraciones de inversión, posibilidad de impago soberano y corrupción. Los puntajes se asignan de 0 a 10 usando los criterios antes mencionados. Una puntuación de 0 marca el riesgo político más alto, mientras que una puntuación de 10 marca el riesgo político más bajo.</description>
        <externalId>false</externalId>
        <inlineHelpText>El Índice de Riesgo Político es un índice propietario que mide el nivel de riesgo que plantean a los gobiernos, corporaciones e inversionistas, basado en una miríada de factores políticos y económicos.</inlineHelpText>
        <label>Political Risk Index</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TIWS_Population__c</fullName>
        <externalId>false</externalId>
        <label>Population</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TIWS_Region__c</fullName>
        <externalId>false</externalId>
        <label>Region</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TIWS_Subregion__c</fullName>
        <externalId>false</externalId>
        <label>Subregion</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <gender>Masculine</gender>
    <label>Country Master</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>Todos</label>
    </listViews>
    <nameField>
        <label>Country Master</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Country Masters</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
